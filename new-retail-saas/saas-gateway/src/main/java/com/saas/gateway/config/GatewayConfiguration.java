//package com.pig4cloud.pig.gateway.config;
//
//import com.dogame.common.redis.bean.RedisClient;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.pig4cloud.pig.gateway.filter.PasswordDecoderGatewayFilterFactory;
//import com.pig4cloud.pig.gateway.filter.PigRequestGlobalFilter;
//import com.pig4cloud.pig.gateway.filter.ValidateCodeGatewayFilterFactory;
//import com.pig4cloud.pig.gateway.handler.GlobalExceptionHandler;
//import com.pig4cloud.pig.gateway.handler.ImageCodeHandler;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * 网关配置
// *
// * @author L.cm
// */
//@Configuration(proxyBeanMethods = false)
//@EnableConfigurationProperties(GatewayConfigProperties.class)
//// @AutoConfigureAfter 自定义的配置类(@Configuration和@ComponentScan扫描加载的配置类)是不会生效的 自动配置类上才会生效
////@AutoConfigureAfter(RedisConfig.class)
//public class GatewayConfiguration {
//
//	@Bean
//	public PasswordDecoderGatewayFilterFactory passwordDecoderFilter(GatewayConfigProperties configProperties,
//																	 RedisClient redisClient) {
//		return new PasswordDecoderGatewayFilterFactory(configProperties,redisClient);
//	}
//
//	@Bean
//	public PigRequestGlobalFilter pigRequestGlobalFilter() {
//		return new PigRequestGlobalFilter();
//	}
//
//	@Bean
//	public ValidateCodeGatewayFilterFactory validateCodeGatewayFilter(GatewayConfigProperties configProperties,
//																	  ObjectMapper objectMapper, RedisClient redisClient) {
//		return new ValidateCodeGatewayFilterFactory(configProperties, objectMapper, redisClient);
//	}
//
//	@Bean
//	public GlobalExceptionHandler globalExceptionHandler(ObjectMapper objectMapper) {
//		return new GlobalExceptionHandler(objectMapper);
//	}
//
//	@Bean
//	public ImageCodeHandler imageCodeHandler(RedisClient redisClient) {
//		return new ImageCodeHandler(redisClient);
//	}
//
//}
