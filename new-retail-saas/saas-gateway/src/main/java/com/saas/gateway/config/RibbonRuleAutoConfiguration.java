//package com.pig4cloud.pig.gateway.config;
//
//import com.pig4cloud.pig.gateway.filter.LocalHostLoadBalancerRule;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.config.ConfigurableBeanFactory;
//import org.springframework.boot.autoconfigure.AutoConfigureBefore;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//
///**
// * @Description Ribbon路由规则自动配置器
// * @Author chengang
// * @Date 2021/11/23
// */
//
//@Configuration
//@RequiredArgsConstructor
////启用自定义的配置文件
//@EnableConfigurationProperties(RibbonRuleProperties.class)
////一定要在ribbon默认的配置器之前加载
////@AutoConfigureBefore(RibbonClientConfiguration.class)
////当ribbon.rule.enabled为false时不启用自定义过滤规则
//@ConditionalOnProperty(value = "ribbon.rule.enabled", matchIfMissing = false)
//public class RibbonRuleAutoConfiguration {
//
//	@Bean
//	@ConditionalOnMissingBean
//	//这里一定要配置为多例
//	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//	public PredicateBasedRule localHostAwareRule() {
//		return new LocalHostLoadBalancerRule();
//	}
//
//}
