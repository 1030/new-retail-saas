/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author lengleng
 * @date 2018年06月21日
 * <p>
 * 网关应用
 */
@EnableDiscoveryClient
@SpringBootApplication
public class PanguGatewayApplication {

	//升级遇见的问题：
	//1. 无法加载nacos配置，提示redis.nodes为null
	// action： 由于版本的问题，bootstrap无法加载bootstrap文件 需要依赖：spring-cloud-starter-bootstrap
	//2. Unable to find GatewayFilterFactory with name RequestRateLimiter
	// action1 : 需要响应试的限流，gateway暂时在nacos配置中写前缀
	// action2：https://www.jianshu.com/p/4972f248d24d ReactiveRedisConnectionFactory 这个Redis连接工厂的实现类是LettuceConnectionFactory或RedissonConnectionFactory
	// 3. Unable to start ServletWebServerApplicationContext due to missing ServletWebServerFactory bean.
	// action 排除 redission 依赖的 spring-boot-starter-web
	// 4.Unable to find GatewayFilterFactory with name ValidateCodeGatewayFilter
	// 自定义的GatewayFilterFactory没有注入到容器 ，看源码得知配置的名称不能带 GatewayFilterFactory

	public static void main(String[] args) {
		SpringApplication.run(PanguGatewayApplication.class, args);
	}

}
