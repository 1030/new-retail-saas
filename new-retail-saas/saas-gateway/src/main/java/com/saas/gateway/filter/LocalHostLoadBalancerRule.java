//package com.pig4cloud.pig.gateway.filter;
//
//import cn.hutool.core.util.ObjectUtil;
//import cn.hutool.system.HostInfo;
//import cn.hutool.system.SystemUtil;
//import lombok.extern.slf4j.Slf4j;
//import java.util.Collections;
//import java.util.List;
//
///**
// * @Description 开发环境实现指定服务的选择
// * 				参考网址：https://blog.csdn.net/jxysgzs/article/details/117460241
// * @Author chengang
// * @Date 2021/11/23
// */
//@Slf4j
//public class LocalHostLoadBalancerRule extends PredicateBasedRule {
//
////	@Autowired
////	private LoadBalancerClient loadBalancerClient;
//
//	@Override
//	public AbstractServerPredicate getPredicate() {
//		//断言总是返回TRUE
//		return AbstractServerPredicate.alwaysTrue();
//	}
//
//	@Override
//	public Server choose(Object key) {
//		ILoadBalancer lb = getLoadBalancer();
//		//获取所有服务列表
//		List<Server> allServers = lb.getAllServers();
//		List<Server> serverList = this.filterServers(allServers);
//		return getPredicate().chooseRoundRobinAfterFiltering(serverList, key).orNull();
//	}
//
//
//	/**
//	 * 这种方法需要启动GateWay服务
//	 * @param serverList
//	 * @return
//	 */
//	private List<Server> filterServers(List<Server> serverList) {
//		//获取本机IP
//		HostInfo hostInfo = SystemUtil.getHostInfo();
//		String localIp = hostInfo.getAddress();
//		for (Server server : serverList) {
//			//提供服务的host
//			String serverHost = server.getHost();
//			if (ObjectUtil.equal(localIp, serverHost) || ObjectUtil.equal(hostInfo.getName(), serverHost)) {
//				log.info("{}的请求转发到指定路由{}",localIp,serverHost);
//				return Collections.singletonList(server);
//			}
//		}
//		return Collections.unmodifiableList(serverList);
//	}
//
//}
