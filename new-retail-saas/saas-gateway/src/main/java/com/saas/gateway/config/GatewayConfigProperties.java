package com.saas.gateway.config;

import com.pig4cloud.pig.gateway.filter.PasswordDecoderGatewayFilterFactory;
import com.pig4cloud.pig.gateway.filter.ValidateCodeGatewayFilterFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * 网关配置文件
 */
@Data
@RefreshScope
@Component
@ConfigurationProperties("gateway")
public class GatewayConfigProperties {

	/**
	 * 网关解密登录前端密码 秘钥 {@link PasswordDecoderGatewayFilterFactory}
	 */
	public String encodeKey;

	/**
	 * 网关不需要校验验证码的客户端 {@link ValidateCodeGatewayFilterFactory}
	 */
	public List<String> ignoreClients;

}