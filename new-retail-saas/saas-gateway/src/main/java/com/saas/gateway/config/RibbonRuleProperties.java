package com.saas.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author chengang
 * @Date 2021/11/23
 */
@Data
@ConfigurationProperties("ribbon.rule")
@Component
public class RibbonRuleProperties {

	/**
	 * 是否开启，默认：FALSE 仅开发环境开启
	 */
	private boolean enabled = Boolean.FALSE;

}
