package com.saas.gateway.filter;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.http.HttpUtil;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.gateway.config.GatewayConfigProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *  密码解密工具类
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PasswordDecoderGatewayFilterFactory extends AbstractGatewayFilterFactory<Object> {

	private static final String PASSWORD = "password";

	private static final String USERNAME = "username";

	private static final String KEY_ALGORITHM = "AES";

	private final GatewayConfigProperties configProperties;

	private final RedisTemplate redisTemplate;

	/**
	 * common-redis 和 reactive-redis暂时不能兼容，问题还未解决。暂时在gateway项目中配置前缀
	 * @param data
	 * @param pass
	 * @return
	 */
	@Value("${redis.key.prefix:dongyou}")
	private String gatewayRedisPrefix;

	private static String decryptAES(String data, String pass) {
		AES aes = new AES(Mode.CBC, Padding.NoPadding, new SecretKeySpec(pass.getBytes(), KEY_ALGORITHM),
				new IvParameterSpec(pass.getBytes()));
		byte[] result = aes.decrypt(Base64.decode(data.getBytes(StandardCharsets.UTF_8)));
		return new String(result, StandardCharsets.UTF_8);
	}

	@Override
	public GatewayFilter apply(Object config) {
		return (exchange, chain) -> {
			ServerHttpRequest request = exchange.getRequest();

			// 不是登录请求，直接向下执行
			if (!StrUtil.containsAnyIgnoreCase(request.getURI().getPath(), SecurityConstants.OAUTH_TOKEN_URL)) {
				return chain.filter(exchange);
			}

			URI uri = exchange.getRequest().getURI();
			String queryParam = uri.getRawQuery();
			Map<String, String> paramMap = HttpUtil.decodeParamMap(queryParam, CharsetUtil.CHARSET_UTF_8);

			String userName = paramMap.get(USERNAME);
			String password = paramMap.get(PASSWORD);
			//是否需要验证AES 1--不验证  传递该参数则不验证aes
			String aesState = paramMap.get("aes_state");
			//跨平台调用无需解密（密码为明文）
			if (StrUtil.isNotBlank(password) && aesState == null) {
				try {
					password = decryptAES(password, configProperties.getEncodeKey());
				} catch (Exception e) {
					log.error("密码解密失败:{}", password);
					return Mono.error(e);
				}
				paramMap.put(PASSWORD, password.trim());
			}

			// 解密成功后将明文密码存入redis,便于后台模拟登录,暂不存密文后解密
			// 每次 set 过期时间会重置
			// 解密后的密码是16位会补空格,trim()能够去除从编码’\u0000′ 至 ‘\u0020’ 的所有字符
			//需要配置 StringRedisSerializer 不然默认是JDK序列化方式会导致切换租户获取不到
			redisTemplate.setKeySerializer(new StringRedisSerializer());
			redisTemplate.setValueSerializer(new StringRedisSerializer());
			redisTemplate.opsForValue().set(gatewayRedisPrefix.concat(SecurityConstants.CHANGE_PREFIX) + userName, "\""+password.trim()+"\"",1L, TimeUnit.DAYS);
			URI newUri = UriComponentsBuilder.fromUri(uri).replaceQuery(HttpUtil.toParams(paramMap)).build(true)
					.toUri();

			ServerHttpRequest newRequest = exchange.getRequest().mutate().uri(newUri).build();
			return chain.filter(exchange.mutate().request(newRequest).build());
		};
	}

}
