package com.pig4cloud.pig.common.log.filter;

import com.alibaba.fastjson.serializer.ValueFilter;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description 将文件类型的属性转为空字符串
 * @Author chengang
 * @Date 2021/11/9
 */
public class FieldToEmptyStringFilter implements ValueFilter {

	/**
	 * 解释 SerializerFeature
	 * https://blog.csdn.net/u010246789/article/details/52539576/
	 *
	 * @param obj
	 * @param name
	 * @param value
	 * @return
	 */
	@Override
	public Object process(Object obj, String name, Object value) {
		if (obj instanceof MultipartFile) {
			return null;
		} else {
			return value;
		}
	}
}
