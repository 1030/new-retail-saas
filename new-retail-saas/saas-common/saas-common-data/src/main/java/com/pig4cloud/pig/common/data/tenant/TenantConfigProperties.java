package com.pig4cloud.pig.common.data.tenant;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * 多租户配置
 *
 * @author oathsign
 */
@Data
@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "tenant")
public class TenantConfigProperties {

	/**
	 * 是否开启租户模式
	 */
	private Boolean enable = true;

	/**
	 * tenant.column=tenant_id
	 * 维护租户列名称
	 */
	private String column = "tenant_id";

	/**
	 * tenant.tables[0]=sys_role
	 * tenant.tables[1]=sys_log
	 * tenant.tables[N]=xxx_xxx
	 * 多租户的数据表集合
	 */
	private List<String> tables = new ArrayList<>();

	private Integer cpsTenantId;
}
