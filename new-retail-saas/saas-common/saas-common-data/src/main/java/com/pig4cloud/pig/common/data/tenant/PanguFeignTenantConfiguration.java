package com.pig4cloud.pig.common.data.tenant;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 /**
 * @author chengang
 * @date 2021/9/11
 * feign 租户信息拦截
 */
@Configuration
public class PanguFeignTenantConfiguration {
	@Bean
	public RequestInterceptor panguFeignTenantInterceptor() {
		return new PanguFeignTenantInterceptor();
	}
}
