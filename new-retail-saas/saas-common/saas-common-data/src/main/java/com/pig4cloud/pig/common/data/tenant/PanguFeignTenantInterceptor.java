
package com.pig4cloud.pig.common.data.tenant;

import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.security.tenant.TenantContextHolder;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chengang
 * @date 2021/9/11
 */
@Slf4j
public class PanguFeignTenantInterceptor implements RequestInterceptor {
	/**
	 * feign在发送请求之前都会调用该接口的apply方法
	 * 即：远程调用带上租户ID
	 * @param requestTemplate
	 */
	@Override
	public void apply(RequestTemplate requestTemplate) {
		if (TenantContextHolder.getTenantId() == null) {
			log.error("feign拦截器 >> 定时器执行时租户ID为空,已设为默认值1");
			TenantContextHolder.setTenantId(SecurityConstants.TENANT_ID_1);
		}
		requestTemplate.header(SecurityConstants.TENANT_ID, TenantContextHolder.getTenantId().toString());
	}
}
