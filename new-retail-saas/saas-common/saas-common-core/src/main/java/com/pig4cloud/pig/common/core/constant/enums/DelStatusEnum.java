package com.pig4cloud.pig.common.core.constant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum DelStatusEnum {
	DEL("1"),NODEL("0");
	private final String value;

}
