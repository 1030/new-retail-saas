/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.common.core.constant;

/**
 * @author lengleng
 * @date 2019/2/1
 */
public interface CommonConstants {

	/**
	 * 删除
	 */
	String STATUS_DEL = "1";

	/**
	 * 正常
	 */
	String STATUS_NORMAL = "0";

	/**
	 * 锁定
	 */
	String STATUS_LOCK = "9";

	/**
	 * 菜单树根节点
	 */
	Integer MENU_TREE_ROOT_ID = -1;

	/**
	 * 菜单
	 */
	String MENU = "0";

	/**
	 * 编码
	 */
	String UTF8 = "UTF-8";

	/**
	 * JSON 资源
	 */
	String CONTENT_TYPE = "application/json; charset=utf-8";

	/**
	 * 前端工程名
	 */
	String FRONT_END_PROJECT = "pig-ui";

	/**
	 * 后端工程名
	 */
	String BACK_END_PROJECT = "pig";

	/**
	 * 成功标记
	 */
	Integer SUCCESS = 0;
	/**
	 * 成功标记mag
	 */
	String SUCCESS_MSG = "操作成功";

	/**
	 * 失败标记
	 */
	Integer FAIL = 1;
	/**
	 * 失败标记
	 */
	String FAIL_MSG = "操作失败";

	/**
	 * 验证码前缀
	 */
	String DEFAULT_CODE_KEY = "DEFAULT_CODE_KEY_";

	/**
	 * 当前页
	 */
	String CURRENT = "current";

	/**
	 * size
	 */
	String SIZE = "size";


	Integer OPERATE_1=1;
	Integer OPERATE_2=2;
	Integer OPERATE_3=3;
	Integer OPERATE_4=4;
	Integer OPERATE_5=5;
	Integer OPERATE_6=6;

	Integer TYPE_1=1;
	Integer TYPE_2=2;
	Integer TYPE_3=3;
	Integer TYPE_4=4;



	Integer HIDE_0=0;
	Integer HIDE_1=1;


	/**
	 * 默认存储bucket
	 */
	String BUCKET_NAME = "avatar";

	String AD_HIDE_COLS_ = "ad_hide_cols_";


	String AD_ORDER_COLS_ = "dynamic_table_order_cols_";

	String AD_DYNAMIC_NONE = "NONE#";

	/**
	 * 是否删除 ：0未删除 1 删除
	 */
	Integer DELETED_FALSE=0;
	Integer DELETED_TRUE=1;


	/**
	 *是否发生变更 ：1 已变更  2 未变更
	 */
	 Integer CHANGE_1=1;
	 Integer CHANGE_2=2;

}
