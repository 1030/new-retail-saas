package com.pig4cloud.pig.common.core.util;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description
 * @Author chengang
 * @Date 2021/10/27
 */
public class CommonUtils {

	/**
	 * 有效邮件正则表达式
	 */
	private static Pattern emailPattern = Pattern
			.compile("[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}");

	/**
	 * 有效ip正则表达式
	 */
	private static Pattern ipPattern = Pattern
			.compile("([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}");

	/**
	 * 手机号正则表达式
	 */
	private static Pattern mobilePattern = Pattern
			.compile("(13[0-9]|15[0|1|3|5|6|8|9])\\d{8}");

	/**
	 * 数字政策表达式
	 */
	private static Pattern numberPattern = Pattern
			.compile("[0-9]{1,}[.][0-9]{1,}|[0-9]{1,}");

	/**
	 * 正整数
	 */
	private static Pattern positiveNumPattern = Pattern
			.compile("[1-9][0-9]{0,}");


	/**
	 * 浮点数，包含负数小数
	 */
	private static Pattern decimalsMinusNumPattern = Pattern
			.compile("^(-?\\d+)(\\.\\d+)?$");

	public static String[] domains = {".com.cn", ".net.cn", ".cn", ".com",
			".net" /** ,".org",".edu",".mil",".gov" */
	};

	/**
	 * 校验字母和数字正则表达式
	 */
	private static Pattern lettersAndNumberPattern = Pattern
			.compile("^[A-Za-z0-9]+$");

	/**
	 * 校验字母正则表达式
	 */
	private static Pattern lettersPattern = Pattern
			.compile("^([A-Za-z])+$");


	/**
	 * 密码正则校验
	 */
	private static Pattern isPassword = Pattern
			.compile("^(?![a-zA-z]+$)(?!\\d+$)(?![!@#$%^&*(),.]+$)[a-zA-Z\\d/\\\\!@#$%^&*(),.=+-_'|{}:;\"~`]{6,16}$");


	/**
	 * 校验浮点数，包含负数小数
	 *
	 * @param val
	 * @return
	 */
	public static boolean checkNumber(String val) {
		if (val == null) {
			return false;
		}
		Matcher m = decimalsMinusNumPattern.matcher(val);
		return m.matches();
	}

	/**
	 * 判断是否包含字母
	 *
	 * @param userName
	 */
	public static boolean isLetters(String userName) {
		Matcher m = lettersPattern.matcher(userName);
		return m.matches();
	}

	/**
	 * 判断是否有效邮件
	 *
	 * @param email
	 */
	public static boolean isValidEmail(String email) {
		if (email == null) {
			return false;
		}
		Matcher m = emailPattern.matcher(email);
		return m.matches();
	}

	/**
	 * 判断是否有效ip
	 *
	 * @param ip
	 */
	public static boolean isValidip(String ip) {
		if (ip == null) {
			return false;
		}
		Matcher m = ipPattern.matcher(ip);
		return m.matches();
	}

	/**
	 * 判断是否是包含数字和字母
	 *
	 * @param Mobile
	 */
	public static boolean isLettersAndNumber(String Mobile) {
		if (Mobile == null) {
			return false;
		}
		Matcher m = lettersAndNumberPattern.matcher(Mobile);
		return m.matches();
	}

	/**
	 * 判断是否有效手机号
	 *
	 * @param mobile
	 */
	public static boolean isValidMobile(String mobile) {

		Matcher m = mobilePattern.matcher(mobile);
		return m.matches();
	}

	/**
	 * 判断是否有效手机号 ,1开头，校验位数为11为
	 *
	 * @param mobile
	 */
	public static boolean isValidMobileNew(String mobile) {
		String regExp = "^(1)\\d{10}$";
		Pattern p = Pattern.compile(regExp);
		Matcher m = p.matcher(mobile);
		return m.matches();
	}

	/**
	 * 给定字符传是否是数字
	 *
	 * @return
	 */
	public static boolean isNumber(String targetStr) {
		Matcher m = numberPattern.matcher(targetStr);
		return m.matches();
	}

	/**
	 * 密码正则校验
	 *
	 * @return
	 */
	public static boolean isPassword(String targetStr) {
		Matcher m = isPassword.matcher(targetStr);
		return m.matches();
	}

	/**
	 * 给定字符串是否正整数
	 *
	 * @param targetStr
	 * @return
	 */
	public static boolean isPositiveNumber(String targetStr) {
		if (StringUtils.isBlank(StringUtils.trim(targetStr))) {
			return false;
		}
		Matcher m = positiveNumPattern.matcher(targetStr);
		return m.matches();
	}

	/**
	 * 给定字符串是否为金额类型
	 *
	 * @param targetStr
	 * @return
	 */
	public static boolean isMoneyNumber(String targetStr, int floatLen) {
		Pattern moneyPattern = Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0," + floatLen + "})?$");
		Matcher m = moneyPattern.matcher(targetStr);
		return m.matches();
	}

	/**
	 * 去掉所有空格
	 *
	 * @param str
	 */
	public static String ignoreSpaces(String str) {

		StringBuffer sb = new StringBuffer("");
		String[] temp = str.split(" ");
		for (int i = 0; i < temp.length; i++) {
			sb.append(temp[i]);
		}
		return sb.toString();
	}

	/**
	 * 是否空字符串
	 *
	 * @param targetStr
	 */
	public static boolean isEmpty(String targetStr) {
		return targetStr == null || "".equals(targetStr.trim());
	}

	/**
	 * 去掉字符串前后的空字符，包括全角的
	 *
	 * @return
	 */
	public static String trim(String str, String Flag) {

		if (str == null || "".equals(str)) {
			return str;
		} else {
			str = "" + str;
			if ("l".equals(Flag) || "L".equals(Flag)) {// 去掉首空格
				String RegularExp = "^[\u00a0|\u0020]+";
				return str.replaceAll(RegularExp, "");
			} else if ("r".equals(Flag) || "R".equals(Flag)) { // 去掉尾空格
				String RegularExp = "[\u00a0|\u0020]+$";
				return str.replaceAll(RegularExp, "");
			} else { // 去掉首和尾空格
				String RegularExp = "^[\u00a0|\u0020]+|[\u00a0|\u0020]+$";
				return str.replaceAll(RegularExp, "");
			}
		}
	}

	/**
	 * 计算两个Date类型的日期相差的天数
	 *
	 * @param beginDate
	 * @param endDate
	 * @return
	 * @author jinxingshan
	 */
	public static int dateGap(Date beginDate, Date endDate) {
		long time = endDate.getTime() - beginDate.getTime();
		int days = (int) (time / (24 * 60 * 60 * 1000)) + 1;

		return days;
	}


	/**
	 * 金额校验
	 * @param str
	 * @param digit 小数位数
	 * @return
	 */
	public static boolean isMoney(String str, int digit) {
		Pattern pattern = Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0," + digit + "})?$");
		Matcher match = pattern.matcher(str);
		return match.matches();
	}

}
