package com.pig4cloud.pig.common.core.util;

import com.alibaba.fastjson.JSONObject;



/**
 *  uc接口报文返回 
 *  @author zxm
 */
public class UcResponseBean {
	
    protected JSONObject header;
    
    protected JSONObject body;

	public JSONObject getHeader() {
		return header;
	}

	public void setHeader(JSONObject header) {
		this.header = header;
	}

	public JSONObject getBody() {
		return body;
	}

	public void setBody(JSONObject body) {
		this.body = body;
	}
}
