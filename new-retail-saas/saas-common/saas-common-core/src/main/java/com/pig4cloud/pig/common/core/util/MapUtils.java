package com.pig4cloud.pig.common.core.util;



import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.baomidou.mybatisplus.core.toolkit.StringUtils.underlineToCamel;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName MapUtils.java
 * @createTime 2021年06月10日 11:24:00
 */
public class MapUtils {



	/**
	 * 将map对于的key转换为驼峰格式
	 * @param map
	 * @return
	 */

	public static Map<String, Object> toReplaceKeyLow(Map<String, Object> map) {
		Map reMap = new HashMap();

		if (map != null) {
			Iterator var2 = map.entrySet().iterator();

			while (var2.hasNext()) {
				Map.Entry<String, Object> entry = (Map.Entry) var2.next();
				reMap.put(underlineToCamel((String) entry.getKey()), map.get(entry.getKey()));
			}

			map.clear();
		}

		return reMap;
	}


}
