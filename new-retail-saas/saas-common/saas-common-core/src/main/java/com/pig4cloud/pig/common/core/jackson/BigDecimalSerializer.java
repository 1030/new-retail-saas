package com.pig4cloud.pig.common.core.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * TODO
 *
 * @Author: hejiale
 * @Date: 2022/4/9 11:40
 */
public class BigDecimalSerializer extends JsonSerializer<BigDecimal>/* implements ContextualSerializer*/ {

	// 默认格式化方案, 项目中添加了 BigDecimal 的格式化配置后,
	// 所有未添加 @BigDecimalFormat 注解的 BigDecimal 数据都会变成这个格式;
	private String format = "#.00";
	private int scale = 2;

	@Override
	public void serialize(BigDecimal bigDecimal, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
		if (bigDecimal != null) {
			/** 在这里定制我们需要格式化的逻辑 */
			// 留scale位小数，四舍五入
			BigDecimal number = bigDecimal.setScale(scale, RoundingMode.HALF_UP);
//			jsonGenerator.writeString(new DecimalFormat(format).format(number));
			jsonGenerator.writeString(number.toString());
		}
	}

//	@Override
//	public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty beanProperty) throws JsonMappingException {
//		if(beanProperty !=null ){
//			if(Objects.equals(beanProperty.getType().getRawClass(),BigDecimal.class)){
//				BigDecimalFormat bigDecimalFormat = beanProperty.getAnnotation((BigDecimalFormat.class));
//				if(bigDecimalFormat == null){
//					bigDecimalFormat = beanProperty.getContextAnnotation(BigDecimalFormat.class);
//				}
//				BigDecimalSerializer bigDecimalSerializer = new BigDecimalSerializer();
//				if(bigDecimalFormat != null){
//					bigDecimalSerializer.format = bigDecimalFormat.value();
//				}
//				return bigDecimalSerializer;
//			}
//			return serializerProvider.findValueSerializer(beanProperty.getType(),beanProperty);
//		}
//		return serializerProvider.findNullValueSerializer(beanProperty);
//	}
}
