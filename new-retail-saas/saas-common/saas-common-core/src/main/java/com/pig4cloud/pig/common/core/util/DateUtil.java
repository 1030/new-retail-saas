package com.pig4cloud.pig.common.core.util;


import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtil {

	private static ThreadLocal<DateFormat> threadLocal = new ThreadLocal<DateFormat>();
	public static final String simple = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 获取一个去处格式到秒时间
	 */
	public static String dateToString() {
		String f = "yyyyMMddHHmmss";
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat(f);
		String Time = formatter.format(new Date());
		return Time;
	}
	/**
	 * 获取一个去处格式到秒时间 
	 */
	public static String timeToString() {
		String f = "HHmmss";
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat(f);
		String Time = formatter.format(new Date());
		return Time;
	}
	/**
	 * 时间戳
	 * @return
	 */
	public static String timeFilName(){
		SimpleDateFormat sdf = new SimpleDateFormat("", Locale.SIMPLIFIED_CHINESE);
		sdf.applyPattern("yyyyMMddHHmmssSS");
		return sdf.format(System.currentTimeMillis());
		
	}
	
	/** 
	* 根据当前日期获得所在周的日期区间（周一和周日日期） 
	*  
	* @return 
	* @author zhaoxuepu 
	* @throws ParseException 
	*/  
	public static String getTimeInterval(Date date) {  
	     Calendar cal = Calendar.getInstance();  
	     cal.setTime(date);  
	     // 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了  
	     int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天  
	     if (1 == dayWeek) {  
	        cal.add(Calendar.DAY_OF_MONTH, -1);  
	     }  
	     // System.out.println("要计算日期为:" + sdf.format(cal.getTime())); // 输出要计算日期  
	     // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一  
	     cal.setFirstDayOfWeek(Calendar.MONDAY);  
	     // 获得当前日期是一个星期的第几天  
	     int day = cal.get(Calendar.DAY_OF_WEEK);  
	     // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值  
	     cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);  
	     String imptimeEnd = dateToString(cal.getTime(), "yyyy-MM-dd HH:mm:ss");  
	     // System.out.println("所在周星期一的日期：" + imptimeBegin);  
	     cal.add(Calendar.DATE, -6);  
	     String imptimeBegin = dateToString(cal.getTime(), "yyyy-MM-dd HH:mm:ss"); 
	     // System.out.println("所在周星期日的日期：" + imptimeEnd);  
	     return imptimeBegin + "," + imptimeEnd;  
	}  
	  
	  
	/** 
	* 根据当前日期获得上周的日期区间（上周周一和周日日期） 
	*  
	* @return 
	* @author zhaoxuepu 
	*/  
	public String getLastTimeInterval() { 
		SimpleDateFormat sdf = new SimpleDateFormat("", Locale.SIMPLIFIED_CHINESE);
	     Calendar calendar1 = Calendar.getInstance();  
	     Calendar calendar2 = Calendar.getInstance();  
	     int dayOfWeek = calendar1.get(Calendar.DAY_OF_WEEK) - 1;  
	     int offset1 = 1 - dayOfWeek;  
	     int offset2 = 7 - dayOfWeek;  
	     calendar1.add(Calendar.DATE, offset1 - 7);  
	     calendar2.add(Calendar.DATE, offset2 - 7);  
	     // System.out.println(sdf.format(calendar1.getTime()));// last Monday  
	     String lastBeginDate = sdf.format(calendar1.getTime());  
	     // System.out.println(sdf.format(calendar2.getTime()));// last Sunday  
	     String lastEndDate = sdf.format(calendar2.getTime());  
	     return lastBeginDate + "," + lastEndDate;  
	} 
	
	/**
	 * 获取当前日日期返回
	 */
	public static String getDay() {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("d");
		String day = formatter.format(new Date());
		return day;
	}

	/**
	 * 获取月份
	 */
	public static String getMonth() {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("M");
		String month = formatter.format(new Date());
		return month;
	}

	/**
	 * 获取年
	 */
	public static String getYear() {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyy");
		String year = formatter.format(new Date());
		return year;
	}

	/**
	 * 获取秒
	 */
	public static String getsecond() {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("ss");
		return formatter.format(new Date());
	}
	/**
	 * 将java.util.Date 格式转换为字符串格式'yyyy-MM-dd HH:mm:ss a'(12小时制) 如Sat May 11
	 * 17:23:22 CST 2002 to '2002-05-11 05:23:22 下午'
	 * 
	 * @param time
	 *            Date 日期
	 * @return String 字符串 默认yyyy-MM-dd HH:mm:ss
	 */
	public static String dateToString(Date time, String format) {
		String f = "yyyy-MM-dd HH:mm:ss";
		if (format != null) {
			f = format;
		}
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat(f);
		String Time = formatter.format(time);
		return Time;
	}

	/**
	 * 取系统当前时间:返回只值为如下形式 2002-10-30 20:24:39
	 * 
	 * @return String
	 */
	public static String getTime() {
		return dateToString(new Date(), null);
	}

	/**
	 * 取系统当前日期:返回只值为如下形式 2002-10-30
	 * 
	 * @return String
	 */
	public static String getDate() {
		return dateToString(new Date(), "yyyy-MM-dd");
	}
	/**
	 * 取系统当前日期:返回只值为如下形式 10-30
	 * 
	 * @return String
	 */
	public static String getCurrentDay(Date date) {
		return dateToString(date, "MM-dd");
	}
	/**
	 * 取系统当前日期:返回只值为如下形式 2002-10-30
	 * 
	 * @return String
	 */
	public static String getDates() {
		return dateToString(new Date(), "yyyyMMdd");
	}
	/**
	 * 字符串转为日期
	 * @param date
	 * @param format 默认yyyy-MM-dd
	 * @return
	 */
	public static Date stringToDate(String date, String format){
		if(format == null){
			format = "yyyy-MM-dd";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}
	
	/**
	 * 传入日期值加减去天数后的日期
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date getDate(Date date, Integer day){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, day);
		return calendar.getTime();
	}
	
	/**
	 * 传入日期值加减去分钟数后的日期
	 * @param date
	 * @param minute
	 * @return
	 */
	public static Date getMinuteDate(Date date, Integer minute){
		Date now = new Date(date.getTime() - minute*60000); //5分钟前的时间
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//可以方便地修改日期格式
		String nowTime = dateFormat.format(now);
		return stringToDate(nowTime, "yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * 传入日期值加减去分钟数后的日期
	 * @param date
	 * @param minute
	 * @return
	 */
	public static Date getMinuteDate(Date date, String type,Integer time){
		Date now = null;
		if("minute".equals(type)) {
			now = new Date(date.getTime() - time*60000); //多少分钟前的时间
		}else if("second".equals(type)) {
			now = new Date(date.getTime() - time*1000); //多少秒前的时间
		}else if("hour".equals(type)) {
			now = new Date(date.getTime() - time*60*60*1000); //多少秒前的时间
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//可以方便地修改日期格式
		String nowTime = dateFormat.format(now);
		return stringToDate(nowTime, "yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * 将日期转换为客户端支持类型，
	 * @param date
	 * @return 返回字符串格式("yyyy/MM/dd HH:mm")
	 */
	public static String dateToString(Date date){
		return new SimpleDateFormat("yyyy/MM/dd HH:mm").format(date).toString();
	}
	public static String getDateFirst(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
		//获取前月的第一天
        Calendar   cal_1=Calendar.getInstance();//获取当前日期 
        cal_1.add(Calendar.MONTH, -1);
        cal_1.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天 
        String  firstDay = format.format(cal_1.getTime());
        return firstDay;
	}
	public static String getDateLast(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
		//获取前月的最后一天
        Calendar cale = Calendar.getInstance();   
        cale.set(Calendar.DAY_OF_MONTH,0);//设置为1号,当前日期既为本月第一天 
        String lastDay = format.format(cale.getTime());
        return lastDay;
	}
	/**
	 * 两日期相减获取相减后天数
	 * @param d1 用于减去的时间
	 * @param d2 被减的时间
	 * @return
	 */
	public static long dateMinus(Date d1,Date d2){
		//Calendar nowDate=Calendar.getInstance(),oldDate=Calendar.getInstance();
		//nowDate.setTime(d1); 
		//DateUtil.stringToDate("2015-1-11","yyyy-MM-dd")
//		oldDate.setTime(d2); 
//		long timeNow=nowDate.getTimeInMillis();
//		long timeOld=oldDate.getTimeInMillis();
		long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别 
		long minuteCount=diff/(1000* 60);//化为分钟
		return minuteCount;
	}
	
	/**
	 * 两日期相减获取相减后小时
	 * @param d1 用于减去的时间
	 * @param d2 被减的时间
	 * @return
	 */
	public static long dateHours(Date d1,Date d2){
		//Calendar nowDate=Calendar.getInstance(),oldDate=Calendar.getInstance();
		//nowDate.setTime(d1); 
		 long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别 
		//DateUtil.stringToDate("2015-1-11","yyyy-MM-dd")
//		oldDate.setTime(d2); 
//		long timeNow=nowDate.getTimeInMillis();
//		long timeOld=oldDate.getTimeInMillis();
		long dayCount=diff/(1000*60*60);//化为小时
		return dayCount;
	}
	
	public static long minuteDate(Date d1, Date d2) {
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 long seconds = 0l;
		// long MM=0l;
	    try  
	    {  
	      long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别  
	     // long days = diff / (1000 * 60 * 60 * 24);  
	       seconds=diff/(1000); //共计秒数
	       //MM = seconds/60;   //共计分钟数
	       //int hh=(int)ss/3600;  //共计小时数
	      //int dd=(int)hh/24;   //共计天数
	      //long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);  
	     // long minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);  
	     // seconds = diff/1000*1000;//(diff-days*(1000 * 60 * 60 *24)-hours*(1000* 60 * 60))/(1000* 60*60);  
	     // System.out.println(""+days+"天"+hours+"小时"+minutes+"分");  
	    }catch (Exception e) {  
	    }  
	    return seconds;
	}

	/**
	 * 获取两个日期相隔的天数（小数点后面的全舍掉）
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static long DayDate(Date d1, Date d2) {
		long days = 0l;
		try
		{
			long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
			  days = diff / (1000 * 60 * 60 * 24);
		}catch (Exception e) {
		}
		return days;
	}
	/**
	* 字符串转换成日期
	* @param datestr
	* @param fomart
	* @return date
	*/
	public static Date StrToDate(String datestr,String fomart) {
	   SimpleDateFormat format = new SimpleDateFormat(fomart);
	   Date date = null;
	   try {
	    date = format.parse(datestr);
	   } catch (ParseException e) {
	    e.printStackTrace();
	   }
	   return date;
	}
	
	/**
	* 毫秒字符串转换成日期格式
	* @param datestr
	* @param fomart
	* @return date
	*/
	public static String StringToDateStr(String datestr,String fomart) {
	    Date date = new Date(Long.parseLong(datestr));
	   return dateToString(date, fomart);
	}
	
	/**
	* 毫秒字符串转换成日期格式
	* @param datestr
	* @param fomart
	* @return date
	*/
	public static String getHHMMsss(String datestr,String fomart) {
	   return dateToString(StrToDate(datestr,"yyyy-MM-dd HH:mm:ss"), "HHmmsss");
	}
	
	public static String dayForWeek(String pTime) throws Exception {  
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		 Calendar c = Calendar.getInstance();  
		 c.setTime(format.parse(pTime));  
		 int dayForWeek = 0;  
		 if(c.get(Calendar.DAY_OF_WEEK) == 1){  
		  dayForWeek = 7;  
		 }else{  
		  dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;  
		 }  
		 if(dayForWeek==1){
			 return "星期一";
		 }else if(dayForWeek==2){
			 return "星期二";
		 }else if(dayForWeek==3){
			 return "星期三";
		 }else if(dayForWeek==4){
			 return "星期四";
		 }else if(dayForWeek==5){
			 return "星期五";
		 }else if(dayForWeek==6){
			 return "星期六";
		 }else {
			 return "周日";
		 }
	} 
	
	public static List<Map<String,Object>> twoDateFormatList(Date oneDate, Date twoDate, List<Map<String,Object>> list) {
		int day = 0;
		Date currentDate = new Date();
		boolean flag = false;
		;
		if(Integer.parseInt(DateUtil.dateToString(currentDate, "dd")) <= Integer.parseInt(DateUtil.dateToString(oneDate, "dd"))) {
			day = Integer.parseInt(DateUtil.dateToString(twoDate, "dd"))-Integer.parseInt(DateUtil.dateToString(oneDate, "dd"));
		}else{
			flag = true;
			day = Integer.parseInt(DateUtil.dateToString(twoDate, "dd"))-Integer.parseInt(DateUtil.dateToString(currentDate, "dd"));
		}
		for (int i = 0; i <= day; i++) {
			try {
			if(flag) {
				Map<String,Object> map = dayForDayAndWeek(DateUtil.dateToString(DateUtil.getDate(currentDate, i), "yyyy-MM-dd HH:mm:ss"));
					if(!list.contains(map)) {
						list.add(map);
					}
			}else{
				Map<String,Object> map = dayForDayAndWeek(DateUtil.dateToString(DateUtil.getDate(oneDate, i), "yyyy-MM-dd HH:mm:ss"));
				if(!list.contains(map)) {
					list.add(map);
				}
			}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	/***
	 * 转换日期为"号数日星期 "
	 * @param pTime
	 * @return
	 * @throws Exception
	 */
	public static Map<String,Object> dayForDayAndWeek(String pTime) throws Exception {  
		 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		 Calendar c = Calendar.getInstance();
		 Calendar current = Calendar.getInstance();
		 int currentWeek = 0;
		 c.setTime(format.parse(pTime));  
		 int dayForWeek = 0;  
		 if(c.get(Calendar.DAY_OF_WEEK) == 1){  
		  dayForWeek = 7;  
		 }else{  
		  dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;  
		 }  
		 current.setTime(format.parse(getDate()));  
		 if(current.get(Calendar.DAY_OF_WEEK) == 1){  
			 currentWeek = 7;  
			}else{  
			currentWeek = current.get(Calendar.DAY_OF_WEEK) - 1;  
		 } 
		 String yearMonth = dateToString(StrToDate(pTime,"yyyy-MM-dd HH:mm:ss"), "dd");
		 String currentMonth = dateToString(new Date(), "yyyy-MM-dd");
		 String recordMonth = dateToString(StrToDate(pTime,"yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd");
		 Map<String,Object> map = new HashMap<String,Object>();
		 if(currentWeek==dayForWeek) {
			 if(currentMonth.equals(recordMonth)) {
				 map.put("day", yearMonth+"日");
				 map.put("week", "今天");
				 return map;
			 }else{
				 if(dayForWeek==1){
					 map.put("day", yearMonth+"日");
					 map.put("week", "星期一");
					 return map;
				 }else if(dayForWeek==2){
					 map.put("day", yearMonth+"日");
					 map.put("week", "星期二");
					 return map;
				 }else if(dayForWeek==3){
					 map.put("day", yearMonth+"日");
					 map.put("week", "星期三 ");
					 return map;
				 }else if(dayForWeek==4){
					 map.put("day", yearMonth+"日");
					 map.put("week", "星期四 ");
					 return map;
				 }else if(dayForWeek==5){
					 map.put("day", yearMonth+"日");
					 map.put("week", "星期五");
					 return map;
				 }else if(dayForWeek==6){
					 map.put("day", yearMonth+"日");
					 map.put("week", "星期六");
					 return map;
				 }else {
					 map.put("day", yearMonth+"日");
					 map.put("week", "星期日 ");
					 return map;
				 }
			 }
		 }else if(dayForWeek==1){
			 map.put("day", yearMonth+"日");
			 map.put("week", "星期一  ");
			 return map;
		 }else if(dayForWeek==2){
			 map.put("day", yearMonth+"日");
			 map.put("week", "星期二 ");
			 return map;
		 }else if(dayForWeek==3){
			 map.put("day", yearMonth+"日");
			 map.put("week", "星期三 ");
			 return map;
		 }else if(dayForWeek==4){
			 map.put("day", yearMonth+"日");
			 map.put("week", "星期四  ");
			 return map;
		 }else if(dayForWeek==5){
			 map.put("day", yearMonth+"日");
			 map.put("week", "星期五 ");
			 return map;
		 }else if(dayForWeek==6){
			 map.put("day", yearMonth+"日");
			 map.put("week", "星期六  ");
			 return map;
		 }else {
			 map.put("day", yearMonth+"日");
			 map.put("week", "星期日 ");
			 return map;
		 }
		 
	}
	
	/***
	 * 转换日期为"星期 yyyy-mm"
	 * @param pTime
	 * @return
	 * @throws Exception
	 */
	public static String dayForWeekAndYearMonth(String pTime) throws Exception {  
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		 Calendar c = Calendar.getInstance();
		 Calendar current = Calendar.getInstance();
		 int currentWeek = 0;
		 c.setTime(format.parse(pTime));  
		 int dayForWeek = 0;  
		 if(c.get(Calendar.DAY_OF_WEEK) == 1){  
		  dayForWeek = 7;  
		 }else{  
		  dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;  
		 }  
		 current.setTime(format.parse(getDate()));  
		 if(current.get(Calendar.DAY_OF_WEEK) == 1){  
			 currentWeek = 7;  
			}else{  
			currentWeek = current.get(Calendar.DAY_OF_WEEK) - 1;  
		 } 
		 String yearMonth = dateToString(StrToDate(pTime,"yyyy-MM-dd HH:mm:ss"), "HH:mm");
		 String currentMonth = dateToString(new Date(), "yyyy-MM-dd");
		 String recordMonth = dateToString(StrToDate(pTime,"yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd");
		 if(currentWeek==dayForWeek) {
			 if(currentMonth.equals(recordMonth)) {
				 return "今天 "+yearMonth;
			 }else{
				 if(dayForWeek==1){
					 return "周一 "+yearMonth;
				 }else if(dayForWeek==2){
					 return "周二 "+yearMonth;
				 }else if(dayForWeek==3){
					 return "周三 "+yearMonth;
				 }else if(dayForWeek==4){
					 return "周四 "+yearMonth;
				 }else if(dayForWeek==5){
					 return "周五 "+yearMonth;
				 }else if(dayForWeek==6){
					 return "周六 "+yearMonth;
				 }else {
					 return "周日 "+yearMonth;
				 }
			 }
		 }else if(dayForWeek==1){
			 return "周一 "+yearMonth;
		 }else if(dayForWeek==2){
			 return "周二 "+yearMonth;
		 }else if(dayForWeek==3){
			 return "周三 "+yearMonth;
		 }else if(dayForWeek==4){
			 return "周四 "+yearMonth;
		 }else if(dayForWeek==5){
			 return "周五 "+yearMonth;
		 }else if(dayForWeek==6){
			 return "周六 "+yearMonth;
		 }else {
			 return "周日 "+yearMonth;
		 }
		 
	}
	
	public static boolean isDateFormat(String pTime) throws Exception { 
		 String datePattern1 = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}";
		 Pattern pattern = Pattern.compile(datePattern1);
        Matcher match = pattern.matcher(pTime);
        if (match.matches()) {
       	 return true;
        }else{
       	 return false;
        }
	}
	
	public static List<Map<String,Object>> returnWeekAndDay() {
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		for(int i = 0; i < 7; i++) {
			Map<String,Object> map = new HashMap<String, Object>();
			try {
				if(i==0) {
						map.put("week", dayForWeek(getTime()));
						map.put("day", getCurrentDay(new Date()));
				}else{
						map.put("week", dayForWeek(dateToString(getDate(new Date(),i),null)));
						map.put("day", getCurrentDay(getDate(new Date(),i)));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			list.add(map);
		}
		return list;
	}
	
	/**
	 * 获取某一日期之后几天的时间
	 * 
	 * @param d	
	 * @param day
	 * @return
	 */
	private static Date getDateAfter(Date d, int day) {
		Calendar now = Calendar.getInstance();
		now.setTime(d);
		now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
		return now.getTime();
	}
	
	/**
	 * 获取某一日期之前几天的时间
	 * 
	 * @param d	
	 * @param day
	 * @return
	 */
	private static Date getDateBefore(Date d, int day) {
		Calendar now = Calendar.getInstance();
		now.setTime(d);
		now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
		return now.getTime();
	}

	/**
	 * todo  关系线程安全问题
	 * 利用threadlocal创建每个线程独有的变量
	 * @param dateFormat
	 * @return
     */
	public static DateFormat getDateFormat(String dateFormat)
	{

		if (StringUtils.isBlank(dateFormat)) {
			dateFormat = simple;
		}
		SimpleDateFormat df = new SimpleDateFormat(dateFormat);
//		DateFormat df = threadLocal.get();
//		if(df==null){
//			df = new SimpleDateFormat(dateFormat);
//			threadLocal.set(df);
//		}
		return df;
	}


	public static String formatDate(Date date,String dateFormat) throws ParseException {
		return getDateFormat(dateFormat).format(date);
	}

	public static String formatDate(Object date,String dateFormat) throws ParseException {
		return getDateFormat(dateFormat).format(date);
	}

	public static Date parse(String strDate,String dateFormat) throws ParseException {
		return getDateFormat(dateFormat).parse(strDate);
	}
	
	/**
	 * 根据日期获取当月天数
	 * @param date
	 * @return
	 */
	public static int getDaysOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}



	public static Date timeToBeginDay(Date date) {
		if (date != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			return calendar.getTime();
		}
		return null;
	}


	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date date = sdf.parse("2018-12-01 00:00:00");
			System.out.println(sdf.format(getDate(date, 1)));
			
			
			
			int days = getDaysOfMonth(date);
			System.out.println(sdf.format(getDate(date, days)));
			Long d=DayDate(new Date(),parse("2021-04-20 15:27:00","yyyy-MM-dd"));
			System.out.println(d);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		//Date d = new Date(Long.parseLong("1489155370306"));
		//String now = DateUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss");
//		System.out.println(DateUtil.getMinuteDate(new Date(), -1));
//		Date nows =DateUtil.StrToDate(DateUtil.dateToString(new Date(), "yyyy-MM-dd")+" 10:00:00", "yyyy-MM-dd HH:mm:ss");
//		//Date d1 = DateUtil.StrToDate(DateUtil.dateToString(new Date(), "yyyy-MM-dd")+" 10:00:00", "yyyy-MM-dd HH:mm:ss");
//		//String url ="http://shop-manager.test.800uz.com:80/upload/01/20170719102507499.jpg";
//		System.out.println(DateUtil.getMinuteDate(new Date(), "second", 5));
//		List<Map<String,Object>> list = twoDateFormatList(new Date(), DateUtil.getDate(new Date(), 3), new ArrayList<Map<String,Object>>());
//		System.out.println(list);
//		try {
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		//System.out.println(Integer.parseInt("09"));
		/*
		String lastPeriod = "";
		int time = Integer.parseInt("11");
		if(time<9) {
			lastPeriod="0"+(time+1);
		}else if(time==9) {
			lastPeriod=(time+1)+"";
		}else if(time>=10 && time<24) {
			lastPeriod=(time+1)+"";
		}else if(time==24) {
			lastPeriod="01";
		}
		lastPeriod=lastPeriod+":00:00";
		
		Date p = DateUtil.stringToDate(now.substring(0, 10)+" "+lastPeriod, "yyyy-MM-dd HH:mm:ss");
		Date n = DateUtil.stringToDate(now, "yyyy-MM-dd HH:mm:ss");
		System.out.println(DateUtil.dateToString(DateUtil.getDate(new Date(), 30), "yyyy-MM-dd HH:mm:ss"));
		System.out.println(new BigDecimal(3.012445).setScale(2, RoundingMode.UP).doubleValue());
		System.out.println(DateUtil.stringToDate(DateUtil.minuteDate(p, new Date())+"", "HH:mm:ss"));*/
		//System.out.println(StrToDate("2017-02-28 22:03:20", "yyyy-MM-dd HH:mm:ss"));
		//System.out.println(dateHours(StrToDate("2017-02-27 23:03:20", "yyyy-MM-dd HH:mm:ss"),StrToDate("2017-02-27 19:04:20", "yyyy-MM-dd HH:mm:ss")));
	}
}