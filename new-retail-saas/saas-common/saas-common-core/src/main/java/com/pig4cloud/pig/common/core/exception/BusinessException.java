package com.pig4cloud.pig.common.core.exception;

import com.pig4cloud.pig.common.core.constant.CommonConstants;

/**
 * 业务异常
 * @author hjl
 * @date 2019/11/26
 */
public class BusinessException extends AbstractException {

	private static final long serialVersionUID = -6497844145928891090L;

    public BusinessException(int code, String message, Object... params) {
        super(code, message, params);
    }
	public BusinessException(String message) {
		super(CommonConstants.FAIL, message);
	}
}
