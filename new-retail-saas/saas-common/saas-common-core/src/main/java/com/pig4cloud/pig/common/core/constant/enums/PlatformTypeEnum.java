/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.common.core.constant.enums;

import org.apache.commons.lang3.StringUtils;


public enum PlatformTypeEnum {
	P3367("0", "3367平台", "3367Platform"),
	TT("1", "今日头条", "tt"),
	XS("3", "新数", "xs"),
	GH("4", "工会", "gh"),
	RY("5", "热云", "ry"),
	UC("7", "UC", "uc"),
	GDT("8", "广点通", "gdt"),
	BD("9", "百度", "bd"),
	KS("10", "快手", "ks"),
	CPS("11", "CPS", "CPS");
	/**
	 * 平台ID
	 */
	private final String value;
	/**
	 * 描述
	 */
	private final String description;

	private final String code;

	//构造方法
	PlatformTypeEnum(String value, String description, String code) {
		this.value = value;
		this.description = description;
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * 通过value    取type
	 *
	 * @return
	 */
	public static String codeByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (PlatformTypeEnum item : PlatformTypeEnum.values()) {
			if (item.getValue().equals(value) ) {
				return item.getCode();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 *
	 * @return
	 */
	public static String valueByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}

		for (PlatformTypeEnum item : PlatformTypeEnum.values()) {
			if (item.getCode().equals(code)) {
				return item.getValue();
			}
		}
		return null;

	}

	public static String descByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (PlatformTypeEnum item : PlatformTypeEnum.values()) {
			if (item.getValue().equals(value)) {
				return item.getDescription();
			}
		}
		return null;

	}

	public static boolean containsType(String value) {
		if (StringUtils.isBlank(value)) {
			return false;
		}

		for (PlatformTypeEnum item : PlatformTypeEnum.values()) {
			if (item.getValue().equals(value)) {
				return true;
			}
		}
		return false;
	}

	public static PlatformTypeEnum getByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}
		for (PlatformTypeEnum item : PlatformTypeEnum.values()) {
			if (item.getValue().equals(value)) {
				return item;
			}
		}
		return null;
	}

	public static PlatformTypeEnum getEnumByKey(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (PlatformTypeEnum item : PlatformTypeEnum.values()) {
			if (item.getValue().equals(value)) {
				return item;
			}
		}
		return null;

	}
}
