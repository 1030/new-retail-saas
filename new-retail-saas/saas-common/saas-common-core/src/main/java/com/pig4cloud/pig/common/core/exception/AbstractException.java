package com.pig4cloud.pig.common.core.exception;

@SuppressWarnings("serial")
public class AbstractException extends RuntimeException{

	protected int code;
	
	protected Object[] params;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Object[] getParams() {
		return params;
	}
	public void setParams(Object[] params) {
		this.params = params;
	}
	public AbstractException(int code, String message, Object... params) {
		super(message);
		this.code = code;
	}
	public AbstractException(int code, String message, Throwable e, Object... params) {
		super(message,e);
		this.code = code;
	}
	
}
