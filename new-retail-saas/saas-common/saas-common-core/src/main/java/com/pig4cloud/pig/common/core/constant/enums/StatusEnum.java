package com.pig4cloud.pig.common.core.constant.enums;

/**
 * Created by hma on 2020/11/6.
 * 对应类型，状态枚举值
 */
public enum  StatusEnum {
	JOB_GROUP_ZERO(0,""),
	JOB_GROUP_ONE(1,"示例执行器"),

	JOB_STATUS_ONE(1,"正常"),


	AD_TYPE_ONE(1,"头条"),
	AD_TYPE_TWO(2,"广电通"),

	OPERATE_TYPE_ONE(1,"修改广告账号预算"),
	OPERATE_TYPE_TWO(2,"修改推广组预算"),
	OPERATE_TYPE_THREE(3,"修改广告计划预算"),
	OPERATE_TYPE_FOUR(4,"修改广点通广告组预算"),
	OPERATE_TYPE_FIVE(5,"修改广点通推广计划预算"),


	TIME_TYPE_ONE(1,"大于当前时间的判断"),

	OPERATE_ONE(1,"操作1"),
	OPERATE_TWO(2,"操作2"),
	OPERATE_THREE(3,"操作3"),
	OPERATE_FOUR(4,"操作4"),

	TYPE_ONE(1,"类型1"),
	TYPE_TWO(2,"类型2"),
	TYPE_THREE(3,"类型3"),
	TYPE_FOUR(4,"类型4"),
	TYPE_FIVE(5,"类型5");













	private Integer  status;
	private String defaultMsg;

	StatusEnum(Integer status, String defaultMsg) {
		this.status = status;
		this.defaultMsg = defaultMsg;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDefaultMsg() {
		return defaultMsg;
	}

	public void setDefaultMsg(String defaultMsg) {
		this.defaultMsg = defaultMsg;
	}

}
