package com.pig4cloud.pig.common.core.util;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Title null.java
 * @Package com.pig4cloud.pigx.common.core.util
 * @Author 马嘉祺
 * @Date 2021/6/24 19:24
 * @Description
 */
public final class ECollectionUtil {

	private ECollectionUtil() {
	}

	/**
	 * 将使用逗号分隔的字符串转换为泛型为String的Set
	 *
	 * @param str
	 * @return
	 */
	public static Set<String> stringToStringSet(String str) {
		return Optional.ofNullable(str).filter(StringUtils::isNotEmpty).map(e -> Arrays.stream(e.split(",")).filter(StringUtils::isNotEmpty).collect(Collectors.toSet())).orElse(Collections.emptySet());
	}

	/**
	 * 将使用特殊分隔的字符串转换为泛型为String的Set
	 *
	 * @param str
	 * @param delimiter
	 * @return
	 */
	public static Set<String> stringToStringSet(String str, String delimiter) {
		return Optional.ofNullable(str).filter(StringUtils::isNotEmpty).map(e -> Arrays.stream(e.split(delimiter)).filter(StringUtils::isNotEmpty).collect(Collectors.toSet())).orElse(Collections.emptySet());
	}

	/**
	 * 将使用逗号分隔的字符串转换为泛型为Integer的Set
	 *
	 * @param str
	 * @return
	 */
	public static Set<Integer> stringToIntSet(String str) {
		return Optional.ofNullable(str).filter(StringUtils::isNotEmpty).map(e -> Arrays.stream(e.split(",")).filter(StringUtils::isNotEmpty).map(Integer::valueOf).collect(Collectors.toSet())).orElse(Collections.emptySet());
	}

	/**
	 * 将使用特殊分隔的字符串转换为泛型为Integer的Set
	 *
	 * @param str
	 * @param delimiter
	 * @return
	 */
	public static Set<Integer> stringToIntSet(String str, String delimiter) {
		return Optional.ofNullable(str).filter(StringUtils::isNotEmpty).map(e -> Arrays.stream(e.split(delimiter)).filter(StringUtils::isNotEmpty).map(Integer::valueOf).collect(Collectors.toSet())).orElse(Collections.emptySet());
	}

	/**
	 * 将使用逗号分隔的字符串转换为泛型为Long的Set
	 *
	 * @param str
	 * @return
	 */
	public static Set<Long> stringToLongSet(String str) {
		return Optional.ofNullable(str).filter(StringUtils::isNotEmpty).map(e -> Arrays.stream(e.split(",")).filter(StringUtils::isNotEmpty).map(Long::valueOf).collect(Collectors.toSet())).orElse(Collections.emptySet());
	}

	/**
	 * 将使用特殊分隔的字符串转换为泛型为Long的Set
	 *
	 * @param str
	 * @param delimiter
	 * @return
	 */
	public static Set<Long> stringToLongSet(String str, String delimiter) {
		return Optional.ofNullable(str).filter(StringUtils::isNotEmpty).map(e -> Arrays.stream(e.split(delimiter)).filter(StringUtils::isNotEmpty).map(Long::valueOf).collect(Collectors.toSet())).orElse(Collections.emptySet());
	}

}
