package com.pig4cloud.pig.common.core.mybatis;

import lombok.Data;


@Data
public class Page<T> extends com.baomidou.mybatisplus.extension.plugins.pagination.Page {

	/**
	 * 分页以为的数据
	 */
	private T row;
	public Page() {

	}
	public Page(long current, long size) {
		super(current, size);
	}

}

