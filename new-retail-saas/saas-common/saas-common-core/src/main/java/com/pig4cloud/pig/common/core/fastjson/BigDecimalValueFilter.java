package com.pig4cloud.pig.common.core.fastjson;

import com.alibaba.fastjson.serializer.ValueFilter;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * TODO
 *
 * @Author: hejiale
 * @Date: 2022/4/9 18:04
 */
public class BigDecimalValueFilter implements ValueFilter {

	// 默认格式化方案, 项目中添加了 BigDecimal 的格式化配置后,
	// 所有未添加 @BigDecimalFormat 注解的 BigDecimal 数据都会变成这个格式;
	private String format = "#.00";
	private int scale = 2;

	public Object process(Object object, String name, Object value) {
		if (null != value && value instanceof BigDecimal) {
			BigDecimal bigDecimal = (BigDecimal) value;
			// 留六位小数，四舍五入
			BigDecimal number = bigDecimal.setScale(scale, RoundingMode.HALF_UP);
			return number.toString();
		}
		return value;
	}

}