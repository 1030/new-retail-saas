package com.pig4cloud.pig.common.core.constant.enums;

import org.apache.commons.lang3.StringUtils;

/**
	 * 计划属性统计分析类别
	 */
	public enum PlanAttrStatTypeEnum {
		//客户端操作系统
		ADID("adid"),
		//主游戏
		PGID("pgid"),
		//客户端操作系统
		GAMEID("gameid"),
		//主渠道
		PARENTCHL("parentchl"),
		//分包
		APPCHL("appchl"),
		//广告账户
		ADVERTISERID("advertiserid");
		private String type;
		private PlanAttrStatTypeEnum(String v){
			this.type=v;
		}
		public String V(){
			return this.type;
		}
		public static boolean contain(String tv){
			PlanAttrStatTypeEnum[] values= PlanAttrStatTypeEnum.values();
			for (PlanAttrStatTypeEnum t: values){
				if(t.type.equals(tv)){
					return true;
				}
			}
			return false;
		}
		public static PlanAttrStatTypeEnum get(String tv){
			if(StringUtils.isBlank(tv)){
				return ADID;
			}
			PlanAttrStatTypeEnum[] values= PlanAttrStatTypeEnum.values();
			for (PlanAttrStatTypeEnum t: values){
				if(t.type.equals(tv)){
					return t;
				}
			}
			return ADID;
		}
	}
