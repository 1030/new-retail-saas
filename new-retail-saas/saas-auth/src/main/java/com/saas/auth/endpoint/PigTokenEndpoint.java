package com.saas.auth.endpoint;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.constant.CacheConstants;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.tenant.TenantContextHolder;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import com.sjda.framework.common.utils.HttpUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 删除token端点
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/token")
public class PigTokenEndpoint {

	private final ClientDetailsService clientDetailsService;

	private final TokenStore tokenStore;

	private final RedisTemplate redisTemplate;

	private final StringRedisTemplate stringRedisTemplate;

	private final CacheManager cacheManager;

	@Value(value = "${loginUrl}")
	private String loginUrl;

	/**
	 * 认证页面
	 * @param modelAndView
	 * @param error 表单登录失败处理回调的错误信息
	 * @return ModelAndView
	 */
	@GetMapping("/login")
	public ModelAndView require(ModelAndView modelAndView, @RequestParam(required = false) String error) {
		modelAndView.setViewName("ftl/login");
		modelAndView.addObject("error", error);
		return modelAndView;
	}

	/**
	 * 确认授权页面
	 * @param request
	 * @param session
	 * @param modelAndView
	 * @return
	 */
	@GetMapping("/confirm_access")
	public ModelAndView confirm(HttpServletRequest request, HttpSession session, ModelAndView modelAndView) {
		Map<String, Object> scopeList = (Map<String, Object>) request.getAttribute("scopes");
		modelAndView.addObject("scopeList", scopeList.keySet());

		Object auth = session.getAttribute("authorizationRequest");
		if (auth != null) {
			AuthorizationRequest authorizationRequest = (AuthorizationRequest) auth;
			ClientDetails clientDetails = clientDetailsService.loadClientByClientId(authorizationRequest.getClientId());
			modelAndView.addObject("app", clientDetails.getAdditionalInformation());
			modelAndView.addObject("user", SecurityUtils.getUser());
		}

		modelAndView.setViewName("ftl/confirm");
		return modelAndView;
	}

	/**
	 * 切换租户
	 * @return
	 */
	@PostMapping("/change")
	public R changeTenant(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String authHeader,
						  @RequestHeader(value = SecurityConstants.TENANT_ID) String tenantId){
//		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
//		String tenantId = request.getHeader(SecurityConstants.TENANT_ID);
//		if (StringUtils.isBlank(token)) {
//			return R.failed("缺少token");
//		}
//		if (StringUtils.isBlank(tenantId)) {
//			return R.failed("目标租户不能为空");
//		}
		try {
			String token = authHeader.replace(OAuth2AccessToken.BEARER_TYPE, StrUtil.EMPTY).trim();
			OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
			if (accessToken == null || StrUtil.isBlank(accessToken.getValue())) {
				return R.failed(null,-1,"Token失效,请重新登录");
			}

			OAuth2Authentication auth2Authentication = tokenStore.readAuthentication(accessToken);
			PigUser pigUser = (PigUser) auth2Authentication.getUserAuthentication().getPrincipal();
			String key = SecurityConstants.CHANGE_PREFIX + pigUser.getUsername();
			Object password = stringRedisTemplate.opsForValue().get(key);
			if (!stringRedisTemplate.hasKey(key) || password == null) {
				return R.failed(null,-1,"Token失效,请重新登录");
			}
			// 暂时不模拟退出，之前的token仍然可用,有效期12h
			// aes_state = 1 不验证AES故从redis中获取 password明文,当然可以采用加密模拟登录
			// http://localhost:8080/auth/oauth/token?username={0}&password={1}&grant_type={2}&scope={3}&login_type={4}&aes_state={5}
			// URL = http://localhost:8080/auth/oauth/token?username=admin&password=123456&grant_type=password&scope=server&login_type=login_after&aes_state=1
			String url = MessageFormat.format(loginUrl,pigUser.getUsername(),password.toString(),"password","server","login_after","1");
			Map<String,String> headers = new HashMap<>();
			headers.put("TENANT-ID",tenantId);
			//cGlnOnBpZw = base64(pig:pig )
			headers.put("Authorization","Basic cGlnOnBpZw==");
			String result = HttpUtils.doPost(url,"",headers);
			return R.ok(JSON.parse(result));
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 退出并删除token
	 * @param authHeader Authorization
	 */
	@DeleteMapping("/logout")
	public R<Boolean> logout(@RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String authHeader) {
		if (StrUtil.isBlank(authHeader)) {
			return R.ok();
		}

		String tokenValue = authHeader.replace(OAuth2AccessToken.BEARER_TYPE, StrUtil.EMPTY).trim();
		return removeToken(tokenValue);
	}

	/**
	 * 令牌管理调用
	 * @param token token
	 */
	@Inner
	@DeleteMapping("/{token}")
	public R<Boolean> removeToken(@PathVariable("token") String token) {
		OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
		if (accessToken == null || StrUtil.isBlank(accessToken.getValue())) {
			return R.ok();
		}

		OAuth2Authentication auth2Authentication = tokenStore.readAuthentication(accessToken);
		// 清空用户信息
		cacheManager.getCache(CacheConstants.USER_DETAILS).evict(auth2Authentication.getName());

		// 清空access token
		tokenStore.removeAccessToken(accessToken);

		// 清空 refresh token
		OAuth2RefreshToken refreshToken = accessToken.getRefreshToken();
		tokenStore.removeRefreshToken(refreshToken);

		//清除用于切换租户用的密码
		stringRedisTemplate.delete(SecurityConstants.CHANGE_PREFIX + auth2Authentication.getName());
		return R.ok();
	}

	/**
	 * 查询token
	 * @param params 分页参数
	 * @return
	 */
	@Inner
	@PostMapping("/page")
	public R<Page> tokenList(@RequestBody Map<String, Object> params) {
		// 根据分页参数获取对应数据
		String key = String.format("%sauth_to_access:*:%s", CacheConstants.PROJECT_OAUTH_ACCESS,TenantContextHolder.getTenantId());
		List<String> pages = findKeysForPage(key, MapUtil.getInt(params, CommonConstants.CURRENT),
				MapUtil.getInt(params, CommonConstants.SIZE));

		redisTemplate.setKeySerializer(new StringRedisSerializer());
		redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
		Page result = new Page(MapUtil.getInt(params, CommonConstants.CURRENT),
				MapUtil.getInt(params, CommonConstants.SIZE));
		result.setRecords(redisTemplate.opsForValue().multiGet(pages));
		result.setTotal(redisTemplate.keys(key).size());
		return R.ok(result);
	}

	private List<String> findKeysForPage(String patternKey, int pageNum, int pageSize) {
		ScanOptions options = ScanOptions.scanOptions().count(1000L).match(patternKey).build();
		RedisSerializer<String> redisSerializer = (RedisSerializer<String>) redisTemplate.getKeySerializer();
		Cursor cursor = (Cursor) redisTemplate.executeWithStickyConnection(
				redisConnection -> new ConvertingCursor<>(redisConnection.scan(options), redisSerializer::deserialize));
		List<String> result = new ArrayList<>();
		int tmpIndex = 0;
		int startIndex = (pageNum - 1) * pageSize;
		int end = pageNum * pageSize;

		assert cursor != null;
		while (cursor.hasNext()) {
			if (tmpIndex >= startIndex && tmpIndex < end) {
				result.add(cursor.next().toString());
				tmpIndex++;
				continue;
			}
			if (tmpIndex >= end) {
				break;
			}
			tmpIndex++;
			cursor.next();
		}

		try {
			cursor.close();
		}
		catch (IOException e) {
			log.error("关闭cursor 失败");
		}
		return result;
	}

}
