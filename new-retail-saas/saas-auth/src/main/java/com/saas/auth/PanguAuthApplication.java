package com.saas.auth;

import com.pig4cloud.pig.common.security.annotation.EnablePigFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * 认证授权中心
 */
@SpringCloudApplication
@EnablePigFeignClients
public class PanguAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(PanguAuthApplication.class, args);
	}

}
