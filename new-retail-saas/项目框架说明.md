## 系统说明
- 基于 Spring Cloud 、Spring Boot、 OAuth2 的 RBAC **企业快速开发平台**， 同时支持微服务架构和单体架构
- 提供对 Spring Authorization Server 生产级实践，支持多种安全授权模式
- 提供对常见容器化方案支持 Kubernetes、Rancher2 、Kubesphere、EDAS、SAE 支持
- 配置maven(3.9.8),local repository不要放C盘

### 核心依赖及版本说明
| 依赖                   | 版本            |
| ---------------------- | ------------- |
| Spring Boot            | 3.3.RELEASE   |
| Spring Cloud           | 2023.0.1      |
| Spring Cloud Alibaba   | 2022.0.0.0 |
| Spring Authorization Server | 1.2.4      |
| Mybatis Plus           | 3.5.6         |
| hutool                 | 5.4.4         |
| Avue                   | 2.6.16        |
| JDK                    | 17        |
| Vue                         | 3.4        |
| Element Plus                | 2.6        |


### 模块说明
```lua
pig-ui  -- https://gitee.com/log4j/pig-ui

pig
├── pig-auth -- 授权服务提供[3000]
└── pig-common -- 系统公共模块
     ├── pig-common-core -- 公共工具类核心包
     ├── pig-common-datasource -- 动态数据源包
     ├── pig-common-job -- xxl-job 封装
     ├── pig-common-log -- 日志服务
     ├── pig-common-mybatis -- mybatis 扩展封装
     ├── pig-common-security -- 安全工具类
     ├── pig-common-swagger -- 接口文档
     ├── pig-common-sentinel -- sentinel 扩展封装
     └── pig-common-test -- oauth2.0 单元测试扩展封装
├── pig-register -- Nacos Server[8848]
├── pig-gateway -- Spring Cloud Gateway网关[9999]
└── pig-upms -- 通用用户权限管理模块
     └── pig-upms-api -- 通用用户权限管理系统公共api模块
     └── pig-upms-biz -- 通用用户权限管理系统业务处理模块[4000]
└── pig-visual
     └── pig-monitor -- 服务监控 [5001]
     ├── pig-codegen -- 图形化代码生成 [5002]
     ├── pig-sentinel-dashboard -- 流量高可用 [5003]
     └── pig-xxl-job-admin -- 分布式定时任务管理台 [5004]
```

## 文档视频

[文档视频 wiki.pig4cloud.com](https://wiki.pig4cloud.com)

[PIGX 在线体验 pigx.pig4cloud.com](http://pigx.pig4cloud.com)

[产品白皮书 paper.pig4cloud.com](https://paper.pig4cloud.com)

## 快速开始

### 本地开发 运行

pig 提供了详细的[部署文档 wiki.pig4cloud.com](https://www.yuque.com/pig4cloud/pig/vsdox9)，包括开发环境安装、服务端代码运行、前端代码运行等。

请务必**完全按照**文档部署运行章节 进行操作，减少踩坑弯路！！

### Docker 运行

```
# 下载并运行服务端代码
git clone https://gitee.com/log4j/pig.git

cd pig && mvn clean install && docker-compose up -d

# 下载并运行前端UI
git clone https://gitee.com/log4j/pig-ui.git

cd pig-ui && npm install -g cnpm --registry=https://registry.npm.taobao.org

npm run build:docker && docker-compose up -d
```

### 快速构架微服务

```xml
<!-- pig-gen archetype -->
# 在空文件夹执行以下命令，注意 windows 下  \ 修改成 ^
mvn archetype:generate \
       -DgroupId=com.pig4cloud \
       -DartifactId=demo \
       -Dversion=1.0.0-SNAPSHOT \
       -Dpackage=com.pig4cloud.pig.demo \
       -DarchetypeGroupId=com.pig4cloud.archetype \
       -DarchetypeArtifactId=pig-gen \
       -DarchetypeVersion=2.10.0 \
       -DarchetypeCatalog=local
```



