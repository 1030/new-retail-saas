package com.pig4cloud.pig.codegen.test;


public enum SeasonEnum {
    CAR_TYPE_BMW("bmw", "宝马"),
    CAR_TYPE_BC("bc", "奔驰"),
    CAR_TYPE_AUDI("audi", "奥迪");

    private String type;

    private String desc;

    private SeasonEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static String getValue(String type) {
        SeasonEnum[] carTypeEnums = values();
        for (SeasonEnum carTypeEnum : carTypeEnums) {
            if (carTypeEnum.type().equals(type)) {
                return carTypeEnum.desc();
            }
        }
        return null;
    }

    public static String getType(String desc) {
        SeasonEnum[] carTypeEnums = values();
        for (SeasonEnum carTypeEnum : carTypeEnums) {
            if (carTypeEnum.desc().equals(desc)) {
                return carTypeEnum.type();
            }
        }
        return null;
    }

    private String type() {
        return this.type;
    }

    private String desc() {
        return this.desc;
    }

    public static void main(String[] args) {
        // 根据类型获取描述
//        String desc = SeasonEnum.getValue("bmw");
//        System.out.println("根据类型获取描述：" + desc);
//        // 根据描述获取类型
//        String type = SeasonEnum.getType("宝马");
//        System.out.println("根据描述获取类型：" + type);
//        SqlTypeEnum desc = SqlTypeEnum.getTypeValue("active_device");
//        System.out.println("根据类型获取描述：" + desc);

    }
}
