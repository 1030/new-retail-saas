package com.pig4cloud.pig.codegen.test;

import lombok.Data;

@Data
public class Person {

    String name;

    String gender;

    String nickname;

    String phone;

}