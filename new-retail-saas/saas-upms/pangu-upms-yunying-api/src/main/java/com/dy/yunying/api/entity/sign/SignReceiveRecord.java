package com.dy.yunying.api.entity.sign;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;


/**
 * 奖品领取记录表
 *
 * @author chengang
 * @version 2021-12-01 10:14:36
 * table: sign_receive_record
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sign_receive_record")
public class SignReceiveRecord extends Model<SignReceiveRecord> {

	//columns START
	//主键id
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;


	//活动ID
	@TableField(value = "activity_id")
	private Long activityId;


	//业务ID
	@TableField(value = "business_id")
	private Long businessId;


	//类型  1签到 2充值任务
	@TableField(value = "business_type")
	private Integer businessType;


	//业务code
	@TableField(value = "business_code")
	private String businessCode;


	// 签到类型  1补签 2正常签到
	@TableField(value = "sign_type")
	private Integer signType;

	//活动名称
	@TableField(value = "activity_name")
	private String activityName;


	//奖品名称
	@TableField(value = "prize_name")
	private String prizeName;


	//类型  1游戏物品 2代金券
	@TableField(value = "prize_type")
	private Integer prizeType;


	//领取序号
	@TableField(value = "receive_sn")
	private Integer receiveSn;


	//用户ID
	@TableField(value = "user_id")
	private Long userId;


	//用户名
	@TableField(value = "user_name")
	private String userName;


	//父游戏ID
	@TableField(value = "parent_game_id")
	private Long parentGameId;


	//父游戏名称
	@TableField(value = "parent_game_name")
	private String parentGameName;


	//子游戏ID
	@TableField(value = "sub_game_id")
	private Long subGameId;


	//区服ID
	@TableField(value = "area_id")
	private Long areaId;


	//区服名称
	@TableField(value = "area_name")
	private String areaName;


	//角色ID
	@TableField(value = "role_id")
	private String roleId;


	//角色名称
	@TableField(value = "role_name")
	private String roleName;


	//领取时间
	@TableField(value = "receive_time")
	private Date receiveTime;


	//是否删除  0否 1是
	@TableField(value = "deleted")
	private Integer deleted;


	//创建时间
	@TableField(value = "create_time")
	private Date createTime;


	//修改时间
	@TableField(value = "update_time")
	private Date updateTime;


	//创建人
	@TableField(value = "create_id")
	private Long createId;


	//修改人
	@TableField(value = "update_id")
	private Long updateId;


	//columns END 数据库字段结束


}

	

	
	

