package com.dy.yunying.api.vo.hbdata;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/18 16:24
 * @description：
 * @modified By：
 */
@Data
public class HbActiveCashVo implements Serializable {

	// 活动类型
	private Integer activityType;

	// 活动类型名称
	private String activityTypeName;

	// 活动名称
	private String activityName;

	// 一次提现点击
	private Integer oneClickNum;

	// 提现点击
	private Integer cashClickNum;
}
