package com.dy.yunying.api.vo.usergroup;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class DeviceVo implements Serializable {

	private static final long serialVersionUID = -5923220684483044377L;

	//主键ID
	private Long id;


	//父ID
	private Long pid;


	private String name;

}
