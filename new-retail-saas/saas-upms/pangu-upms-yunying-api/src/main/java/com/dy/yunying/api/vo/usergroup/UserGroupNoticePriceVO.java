package com.dy.yunying.api.vo.usergroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * hezz 2022-05-17 17:00:00
 * */
@Data
public class UserGroupNoticePriceVO implements Serializable {

	private static final long serialVersionUID = 3799186434822397683L;

	/**用户群组ID*/
	private Long groupId;

	@ApiModelProperty(value = "公告IDS")
	private List<Long> noticeList;

	@ApiModelProperty(value = "奖励IDS")
	private List<Long> prizeList;

	public UserGroupNoticePriceVO() {
	}

	public UserGroupNoticePriceVO(Long groupId) {
		this.groupId = groupId;
	}

	public UserGroupNoticePriceVO(Long groupId, List<Long> noticeList) {
		this.groupId = groupId;
		this.noticeList = noticeList;
	}

	public UserGroupNoticePriceVO(Long groupId, List<Long> noticeList, List<Long> prizeList) {
		this.groupId = groupId;
		this.noticeList = noticeList;
		this.prizeList = prizeList;
	}
}
