package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 红包活动动态类型表
 *
 * @author zhuxm
 * @date 2022-06-06 14:37:18
 */
@Data
@TableName("hb_activity_dynamic_type")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "红包活动动态类型表")
public class HbActivityDynamicType extends HbBaseEntity {

	/**
	 * 活动ID
	 */
	@TableId(value = "id",type = IdType.AUTO)
	@ApiModelProperty(value = "活动动态类型Id")
	private Long id;


	/**
	 * 动态展示名称
	 */
	@ApiModelProperty(value = "动态展示名称")
	private String showName;

	/**
	 * 活动类型(1等级 2充值 3邀请)
	 */
	@ApiModelProperty(value = "活动类型(1等级 2充值 3邀请)")
	private Integer activityType;



	/**
	 * 排序值,值越大越靠后
	 */
	@ApiModelProperty(value = "排序值,值越大越靠后")
	private Integer sort;
}
