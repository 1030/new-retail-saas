package com.dy.yunying.api.vo;

import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

@Data
@Accessors(chain = true)
public class HbActivityKvpicVO extends Page<Object> implements Serializable {

	/**
	 * 活动中心活动ID
	 */
	private String searchContent;


}
