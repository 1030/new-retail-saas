package com.dy.yunying.api.resp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 转化目标字典表
 * @author  chenxiang
 * @version  2022-07-04 13:58:21
 * table: ad_external_dict
 */
@Data
@Accessors(chain = true)
public class AdExternalDictRes {
	/**
	 * 平台ID：1头条，8广点通，10快手
	 */
	private Integer platformId;
	/**
	 * 转化名称
	 */
	private String externalName;
	/**
	 * 转化目标
	 */
	private String externalAction;

}

	

	
	

