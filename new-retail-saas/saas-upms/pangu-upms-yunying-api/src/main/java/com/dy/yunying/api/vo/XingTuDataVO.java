package com.dy.yunying.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Data
@Accessors(chain = true)
public class XingTuDataVO extends Page<Object> {

	/**
	 * 开始日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate startDate;

	/**
	 * 结束日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate endDate;

	/**
	 * 任务名称
	 */
	private String demandName;

	/**
	 * 达人名称
	 */
	private String authorName;

	/**
	 * 视频ID
	 */
	private Long itemId;

	/**
	 * 主游戏ID，多个ID使用逗号分隔
	 */
	private String pgidArr;

	/**
	 * 主游戏ID列表
	 */
	private Set<Long> pgids;

	/**
	 * 子游戏ID，多个ID使用逗号分隔
	 */
	private String gameidArr;

	/**
	 * 子游戏ID列表
	 */
	private Set<Long> gameids;

	/**
	 * 主渠道编码
	 */
	private String parentchl;

	/**
	 * 分包渠道编码，多个使用逗号分隔
	 */
	private String appchlArr;

	/**
	 * 分包渠道编码列表
	 */
	private Set<String> appchls;

	/**
	 * 系统标识
	 */
	private Integer os;

	/**
	 * 部门ID，多个ID使用逗号分隔
	 */
	private String deptIdArr;

	/**
	 * 部门ID列表
	 */
	private Set<Long> deptIds;

	/**
	 * 组别ID，多个ID使用逗号分隔
	 */
	private String userGroupIdArr;

	/**
	 * 组别ID列表
	 */
	private Set<Long> userGroupIds;

	/**
	 * 投放人ID，多个ID使用逗号分隔
	 */
	private String investorIdArr;

	/**
	 * 投放人ID列表
	 */
	private Set<Long> investorIds;

	/**
	 * 是否分成前，0-否；1-是；
	 */
	private Integer showRatio;

	/**
	 * 分组周期：day-按日；week-按周；month-按月；year-按年；collect-汇总；
	 */
	private Integer cycleType;

	/**
	 * 分组字段列表，多个使用逗号分隔：settle_type-推广方式；author_id-达人ID；investor_id-投放人ID；appchl-分包渠道编码；parentchl-主渠道编码；dept_id-部门ID；pgid-主游戏ID；gameid-子游戏ID；os-系统标识；
	 */
	private String groupByArr;

	/**
	 * 分组字段列表：settle_type-推广方式；author_id-达人ID；investor_id-投放人ID；appchl-分包渠道编码；parentchl-主渠道编码；dept_id-部门ID；pgid-主游戏ID；gameid-子游戏ID；os-系统标识；
	 */
	private List<String> groupBys;

	/**
	 * 排序字段名称
	 */
	private String orderByName;

	/**
	 * 排序顺序：0-升序（ASC）；1-降序（DESC）
	 */
	private Integer orderByAsc;

	/**
	 * 导出列名称，多个使用逗号分隔
	 */
	private String exportTitles;

	/**
	 * 导出字段名称，多个使用逗号分隔
	 */
	private String exportColumns;

	/**
	 * 是否为系统管理员
	 */
	private Boolean isAdmin;

	/**
	 * 权限相关投放人条件
	 */
	private Set<Long> prvInvestorIds;

}
