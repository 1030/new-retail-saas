package com.dy.yunying.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sunyq
 * @date 2022/8/22 14:29
 */
@Data
public class AccountInfoVO implements Serializable {
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 注册时间
	 */
	private String regTime;

	private String phoneNumber;

	private String account;

	private String nickName;

	private String regSrc;

	private String regIp;

	private String regArea;

	private String lastActiveTime;

	private String taptap;

	private String qq;

	private String realName;

}
