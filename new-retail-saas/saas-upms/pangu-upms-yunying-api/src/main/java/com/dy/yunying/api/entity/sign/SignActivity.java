/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity.sign;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dy.yunying.api.entity.hongbao.HbBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 签到活动表
 *
 * @author yuwenfeng
 * @date 2021-11-30 19:46:19
 */
@Data
@TableName("sign_activity")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "签到活动表")
public class SignActivity extends HbBaseEntity {

	/**
	 * 签到活动ID
	 */
	@TableId(value = "id",type = IdType.AUTO)
	@ApiModelProperty(value = "签到活动ID")
	private Long id;

	/**
	 * 签到活动名称
	 */
	@ApiModelProperty(value = "签到活动名称")
	private String activityName;

	/**
	 * 父游戏ID
	 */
	@ApiModelProperty(value = "父游戏ID")
	private Long parentGameId;

	/**
	 * 开始时间
	 */
	@ApiModelProperty(value = "开始时间")
	private Date startTime;

	/**
	 * 结束时间
	 */
	@ApiModelProperty(value = "结束时间")
	private Date finishTime;

	/**
	 * 签到天数
	 */
	@ApiModelProperty(value = "签到天数")
	private Integer signDays;

	/**
	 * 活动状态(1待上线 2活动中 3已下线)
	 */
	@ApiModelProperty(value = "活动状态(1待上线 2活动中 3已下线)")
	private Integer activityStatus;

	/**
	 * 活动规则
	 */
	@ApiModelProperty(value = "活动规则")
	private String activityRule;

}
