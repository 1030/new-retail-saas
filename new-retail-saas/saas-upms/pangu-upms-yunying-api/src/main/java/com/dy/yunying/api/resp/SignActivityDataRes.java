package com.dy.yunying.api.resp;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * @author chenxiang
 * @className SignActivityDataRes
 * @date 2021/12/2 20:34
 */
@Data
public class SignActivityDataRes implements Serializable {
	/**
	 * 日期
	 */
	private String date;
	/**
	 * 活动ID
	 */
	private String activityId;
	/**
	 * 活动名称
	 */
	private String activityName;
	/**
	 * PV
	 */
	private Integer servicePV;
	/**
	 * UV
	 */
	private Integer serviceUV;
	/**
	 * IP
	 */
	private Integer serviceIP;
	/**
	 * 绑定角色数
	 */
	private Integer bindRoleNum;
	/**
	 * 签到角色数
	 */
	private Integer signRoleNum;
	/**
	 * 签到次数
	 */
	private Integer signTotal;
	/**
	 * 补签角色数
	 */
	private Integer repairRoleNum;
	/**
	 * 人均签到次数
	 */
	private BigDecimal averageSignNum;
	/**
	 * 完成任务角色数
	 */
	private Integer doneTaskNum;
	/**
	 * 领取奖励角色数
	 */
	private Integer receiveRewardNum;
	/**
	 * 充值总金额
	 */
	private BigDecimal totalRechargeAmount;
	/**
	 * 角色ARPPU
	 */
	private BigDecimal roleARPPU;

	public Integer getServicePV() {
		if (Objects.nonNull(servicePV)){
			return servicePV;
		}
		return 0;
	}

	public Integer getServiceUV() {
		if (Objects.nonNull(serviceUV)){
			return serviceUV;
		}
		return 0;
	}

	public Integer getServiceIP() {
		if (Objects.nonNull(serviceIP)){
			return serviceIP;
		}
		return 0;
	}

	public Integer getBindRoleNum() {
		if (Objects.nonNull(bindRoleNum)){
			return bindRoleNum;
		}
		return 0;
	}

	public Integer getSignRoleNum() {
		if (Objects.nonNull(signRoleNum)){
			return signRoleNum;
		}
		return 0;
	}

	public Integer getSignTotal() {
		if (Objects.nonNull(signTotal)){
			return signTotal;
		}
		return 0;
	}

	public Integer getRepairRoleNum() {
		if (Objects.nonNull(repairRoleNum)){
			return repairRoleNum;
		}
		return 0;
	}

	public Integer getDoneTaskNum() {
		if (Objects.nonNull(doneTaskNum)){
			return doneTaskNum;
		}
		return 0;
	}

	public Integer getReceiveRewardNum() {
		if (Objects.nonNull(receiveRewardNum)){
			return receiveRewardNum;
		}
		return 0;
	}

	public BigDecimal getTotalRechargeAmount() {
		if (Objects.nonNull(totalRechargeAmount)){
			return totalRechargeAmount;
		}
		return new BigDecimal("0");
	}

	public BigDecimal getAverageSignNum() {
		if (Objects.nonNull(signRoleNum) && 0 != signRoleNum && Objects.nonNull(signTotal)){
			averageSignNum =  new BigDecimal(signTotal).divide(new BigDecimal(signRoleNum),0, RoundingMode.HALF_UP);
			return averageSignNum;
		}
		return new BigDecimal("0");
	}

	public BigDecimal getRoleARPPU() {
		if (Objects.nonNull(receiveRewardNum) && 0 != receiveRewardNum && Objects.nonNull(totalRechargeAmount)){
			roleARPPU =  totalRechargeAmount.divide(new BigDecimal(receiveRewardNum),2, RoundingMode.HALF_UP);
			return roleARPPU;
		}
		return new BigDecimal("0");
	}
}
