package com.dy.yunying.api.req.hongbao;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * wan_game_version
 *
 * @author hjl
 * @date 2020-07-21 14:17:19
 */
@Data
public class FileUploadReq {

	private static final long serialVersionUID = 3604499146246680448L;

	/**
	 * 文件夹路径
	 */
	private String dirPath;

}