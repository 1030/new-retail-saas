package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @ClassName AdmonitorRuleDto
 * @Description todo
 * @Author yangyh
 * @Time 2021/6/22 14:27
 * @Version 1.0
 **/
@Setter
@Getter
@ApiModel("监控规则查询对象")
public class MonitorRuleManagerReq extends Page {


	/**
	 * 规则名称
	 */
	@NotNull(message = "规则名称不能为空")
	@ApiModelProperty(value="规则名称")
	private String monitorName;

	/**创建时间开始*/
	@ApiModelProperty(value = "创建时间开始")
	private String rsTime;

	/**创建时间终止*/
	@ApiModelProperty(value = "创建时间终止")
	private String reTime;



	/**
	 * 监控渠道平台类型
	 */
	@ApiModelProperty(value="监控渠道")
	private Integer monitorChannelType;

	/**
	 * 监控账号
	 */
	@ApiModelProperty(value="监控账号，多个用逗号分隔")
	private String advertiserIdArr;



	/**
	 * 监控指标:   1.账号余额 2：0注册的消耗  3 注册成本 4 新增付费成本 5 首日roi 6:7日roi 7:15日roi
	 */
	@ApiModelProperty(value="监控项")
	private Integer monitorTarget;

	/**
	 * 监控状态：0关闭，1开启
	 */
	@ApiModelProperty(value="监控状态：0关闭，1开启")
	private Integer monitorStatus;

	@ApiModelProperty(value="可查看创建的用户")
	private String userArr;
}
