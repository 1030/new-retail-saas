package com.dy.yunying.api.req.yyz;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment
 */
@Data
public class GameAppointmentReq implements Serializable {

	/***
	 *   主键id
	 */
	private String id;

	/***
	 *   手机号
	 */
	private String mobile;

	/**
	 * 预约来源（1：PC  2：H5）
	 */
	private Integer codeType;

	/***
	 * 图形验证码
	 */
	private String captcha;

	/***
	 * 验证码
	 */
	private String yzm;

	/**
	 * 预约设备（1：IOS  2：安卓）
	 */
	private Integer device;

	/**
	 * 发送方式（1.无参数模板 2.有参数模板固定参数 3.有参数模板动态参数）
	 */
	private Integer sendType;

	/***
	 * 随机码证码
	 */
	private String random;


	@ApiModelProperty(value = "预约类型：1幻兽仓角预约  2 烛光勇士预约")
	private Integer type;

	@ApiModelProperty(value = "预约开始时间" )
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date appointStartTime;
	@ApiModelProperty(value = "预约结束时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date appointEndTime;




}
