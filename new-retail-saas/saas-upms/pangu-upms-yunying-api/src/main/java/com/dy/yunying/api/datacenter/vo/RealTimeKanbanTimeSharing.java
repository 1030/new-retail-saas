package com.dy.yunying.api.datacenter.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author sunyq
 * @date 2022/8/19 14:10
 */
@Data
public class RealTimeKanbanTimeSharing implements Serializable {
	private String time;

	private String today;

	private String yesterday;

	private String lastWeekToday;
}
