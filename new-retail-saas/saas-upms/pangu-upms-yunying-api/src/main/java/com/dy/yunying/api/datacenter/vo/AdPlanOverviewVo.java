package com.dy.yunying.api.datacenter.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Objects;

@Setter
@Getter
@ApiModel("广告数据分析")
public class AdPlanOverviewVo {

	// 广告名称
	@ExcelProperty("广告账号名称")
	@ApiModelProperty("广告账号名称")
	private String adname;


	@ExcelProperty("广告计划ID")
	@ApiModelProperty("广告计划ID")
	// 广告计划ID
	private String adid;



	@ApiModelProperty("部门主键")
	// 广告计划ID
	private String deptId;

	@ApiModelProperty("广告主键id")
	// 广告计划ID
	private String adgroupId;


	@ExcelProperty("部门名称")
	@ApiModelProperty("部门名称")
	// 广告计划ID
	private String deptName;

	@ExcelProperty("广告计划名称")
	@ApiModelProperty("广告计划名称")
	// 广告计划名称
	private String adidName;


	@ExcelProperty("广告状态描述")
	@ApiModelProperty("广告状态描述")
	private String status;
	@ExcelProperty("广告状态描述")

	@ApiModelProperty("广告操作状态")
	private String optStatus;


	@ApiModelProperty("投放开始时间")
	private String startTime;
	@ApiModelProperty("投放结束时间")
	private String endTime;

	// 广告位名称
	@ApiModelProperty("广告账号ID")
	private String advertid;

	@ExcelProperty("渠道名称")
	// 渠道名称
	@ApiModelProperty("渠道名称")
	private String chlname;

	@ExcelProperty("投放人")
	// 投放人
	@ApiModelProperty("投放人")
	private String investor;


	@ExcelProperty("投放人姓名")
	// 投放人
	@ApiModelProperty("投放人")
	private String investorname;



	@ExcelProperty("游戏")
	// 游戏
	@ApiModelProperty("游戏")
	private String gname;

	// 子游戏id
	@ApiModelProperty("子游戏id")
//	@JsonProperty("gid")
	private String gid;

	@ApiModelProperty("父游戏id")
	private String pgid;



	//新增设备数
	@ApiModelProperty("系统类型")
//	@JsonProperty("os")
	private String os;


	@ExcelProperty("父游戏")
	// 父游戏
	@ApiModelProperty("父游戏")
	private String pgame;

	@ExcelProperty("渠道类型")
	// 渠道类型
	@ApiModelProperty("渠道类型")
//	@JsonProperty("parentChlName")
	private String parentchlname;


	@ExcelProperty("分包编码")
	@ApiModelProperty("分包编码")
//	@JsonProperty("packCode")
	private String packCode;

	@ExcelProperty("父渠道类型")
	// 渠道类型
	@ApiModelProperty("父渠道类型")
	private String parentchl;


	@ExcelProperty("出价(元)")
	@ApiModelProperty("出价(元)")
	private BigDecimal pricing;

	@ExcelProperty("计划预算(元)")
	@ApiModelProperty("计划预算(元)")
	private BigDecimal budget;


	//todo

//todo   投放方式 投放范围 投放时间  转化目标  投放位置 广告组id,广告组id

	@ExcelProperty("投放方式")
	@ApiModelProperty("投放方式")
	private String unionVideoType;
	/** 投放范围 ： 通投智选(UNIVERSAL)  默认(DEFAULT) 穿山甲(UNION) */

	@ExcelProperty("投放范围")
	@ApiModelProperty("投放范围")
	private String deliveryRange;

	@ExcelProperty("投放时间")
	@ApiModelProperty("投放时间")
	private String scheduleType;



	@ApiModelProperty("转化目标")
	private String convertName;

	@ExcelProperty("转化目标")
	@ApiModelProperty("转化目标")
	private String convertDescri;


	@ApiModelProperty("深化目标")
	private String deepConvert;

	@ApiModelProperty("深度转化目标描述")
	private String deepConvertDescri;

	@ApiModelProperty("转化目标id")
	private String convertId;

	@ExcelProperty("投放位置")
	@ApiModelProperty("投放位置")
	private String inventoryType;

	@ApiModelProperty("广告组id")
	private String campaignId;

	@ExcelProperty("广告组名称")
	@ApiModelProperty("广告组名称")
	private String campaignName;


	@ApiModelProperty("平台类型：1头条 7 uc  8 广点通 9 ")
	private String ctype;

	@ApiModelProperty("创建时间")
	private String createTime;
	@ApiModelProperty("修改时间")
	private String updateTime;

	// 活跃设备数
	// 活跃ARPU= 活跃充值金额/活跃设备数
	// 当周(连续7天)充值金额
	// 当月(连续30天)充值金额
	// 当周ROI(连续7天)=	当周充值金额/返点后消耗
	// 当月ROI(连续30天)= 当月充值金额/返点后消耗




	@ExcelProperty("当周ROI")
	@ApiModelProperty("当周ROI ")
	private BigDecimal weekRoi=new BigDecimal(0);


	@ExcelProperty("当月ROI")
	@ApiModelProperty("当月ROI ")
	private BigDecimal monthRoi=new BigDecimal(0);



	@ExcelProperty("活跃用户")
	@ApiModelProperty("活跃用户(活跃设备数) ")
	private Integer actuuidnums=0;

	@ExcelProperty("活跃ARPU")
	//	活跃ARPU= 活跃充值金额/活跃设备数
	@ApiModelProperty("活跃ARPU")
	private BigDecimal actarpu=new BigDecimal(0);


	@ExcelProperty("新增设备注册数")
	//新增设备注册数(新用户注册)
	@ApiModelProperty("新增设备注册数")
	private Integer usrnamenums=0;


	@ExcelProperty("新增充值金额")
	// 新增充值金额
	@ApiModelProperty("新增充值金额")
	private BigDecimal newuuidfees=new BigDecimal(0);


	@ExcelProperty("活跃充值金额")
	// 充值金额
	@ApiModelProperty("活跃充值金额")
	private BigDecimal payuuidfees=new BigDecimal(0);

	@ExcelProperty("首日新增充值金额")
	@ApiModelProperty("首日新增充值金额")
	@JsonProperty("worth1")
	private BigDecimal worth1=new BigDecimal(0); // 首日 新增充值金额



	@ExcelProperty("当周充值金额")
	@ApiModelProperty("当周充值金额")
	private BigDecimal worth7=new BigDecimal(0); //7天累计 新增充值金额



	@ExcelProperty("当月充值金额")
	@ApiModelProperty("当月充值金额")
	private BigDecimal worth30=new BigDecimal(0); //30天累计 新增充值金额

	@ExcelProperty("设备次日留存数")
	@ApiModelProperty("设备次日留存数")
	@JsonProperty("num2")
	private Integer num2=0; //设备次日留存数

	@ExcelProperty("消耗")

	@ApiModelProperty("消耗后消耗（总成本）")
	@JsonProperty("cost")
	private BigDecimal cost=new BigDecimal(0);
	// 返点后消耗（总成本）
	@ExcelProperty("返点前消耗")
	@ApiModelProperty("返点前消耗")
	private BigDecimal rudeCost=new BigDecimal(0);

	@ApiModelProperty("展示数")
	@JsonProperty("showNums")
	// 展示数
	private Integer shownums=0;
	// 点击数
	@ApiModelProperty("点击数")
	private Integer clicknums=0;


	@ExcelProperty("点击率")
	@ApiModelProperty("点击率")
	// 点击率   点击数/展示数
	private BigDecimal clickratio=new BigDecimal(0);


	@ExcelProperty("点击激活率")
	@ApiModelProperty("点击激活率")
	// 点击激活率   新增设备数/点击数
	private BigDecimal clickactiveratio=new BigDecimal(0);


	@ExcelProperty("设备成本")
	@ApiModelProperty("设备成本")
	// 激活成本  消耗/新增设备数
	private BigDecimal activationcostratio=new BigDecimal(0);




	@ExcelProperty("新增付费率")
	@ApiModelProperty("新增付费率 ")
	//新增付费率  新增充值人数/新增设备数
	private BigDecimal payratio=new BigDecimal(0);


	@ExcelProperty("新增arpu")
	@ApiModelProperty("新增arpu ")
	//注册arpu  新增充值金额/新增设备数
	private BigDecimal regarpu=new BigDecimal(0);

	@ExcelProperty("累计充值ROI")
	@ApiModelProperty("累计充值ROI ")
	//总roi 累计充值金额/总消耗
	private BigDecimal roiratio=new BigDecimal(0);


	@ExcelProperty("累计充值金额")
	@ApiModelProperty("累计充值金额")
	private BigDecimal uuidsumfees=new BigDecimal(0);


	@ExcelProperty("次留")
	@ApiModelProperty("次留 ")
	//次留
	private BigDecimal retention1=new BigDecimal(0);

	@ExcelProperty("首日ROI")
	@ApiModelProperty("首日ROI  ")
	// roi1
	private BigDecimal roi1=new BigDecimal(0);


	private String ratio;

	//-----------------------------------------------------保留字段
	@ApiModelProperty("新增付费成本")
	//新增付费成本 消耗/新增充值人数
	private BigDecimal paycostratio=new BigDecimal(0);

	@ApiModelProperty("总注册成本")
	// 总注册成本  消耗/总注册数
	private BigDecimal regcostratio=new BigDecimal(0);


	@ApiModelProperty("点击注册率")
	// 点击注册率  注册数/点击数
	private BigDecimal regRatio=new BigDecimal(0);


	@ApiModelProperty("新增设备注册成本")
	// 新增设备注册成本  消耗/新增设备注册数
	private BigDecimal uuidcostratio=new BigDecimal(0);



	@ApiModelProperty("总注册转化率")
	// 总注册转化率  总注册数/总注册设备数
	private BigDecimal reghangeratio=new BigDecimal(0);

	@ApiModelProperty("新增设备注册转化率")
//	@JsonProperty("uuidChangeRatio")
	// 新增设备注册转化率  新增注册设备数/新增设备数
	private BigDecimal uuidchangeratio=new BigDecimal(0);



	@ApiModelProperty("新增充值人数")
//	@JsonProperty("newUuidNums")
	//新增充值人数
	private Integer newuuidnums=0;

	// 充值人数
	@ApiModelProperty("充值人数")
	private Integer payuuidnums=0;



	//总注册数
	@ApiModelProperty("总注册数")
//	@JsonProperty("usrNameSums")
	private Integer usrnamesums=0;

	//总注册设备数
	@ApiModelProperty("总注册设备数")
//	@JsonProperty("uuidSums")
	private Integer uuidsums=0;

	//新增设备数
	@ApiModelProperty("新增设备数")
//	@JsonProperty("uuidNums")
	private Integer uuidnums=0;


	//日期
	@ApiModelProperty("日期")
//	@JsonProperty("day")
	private String day;

    //总LTV 累计充值金额/新增设备数
	@ApiModelProperty("总LTV 累计充值金额/新增设备数")
	private BigDecimal ltvratio=new BigDecimal(0);

	//新增arppu  新增充值金额/新增充值人数
	@ApiModelProperty("新增arppu  新增充值金额/新增充值人数")
	private BigDecimal arppuratio=new BigDecimal(0);



	public  BigDecimal getMonthRoi(){
		if (worth30.compareTo(new BigDecimal(0)) == 1 &&  rudeCost.compareTo(new BigDecimal(0)) == 1) {
			BigDecimal monthR =worth30.divide(rudeCost, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return monthR;
		}
		return BigDecimal.ZERO;
	}

	public  BigDecimal getWeekRoi(){
		if (worth7.compareTo(new BigDecimal(0)) == 1 &&  rudeCost.compareTo(new BigDecimal(0)) == 1) {
			BigDecimal weekR = worth7.divide(rudeCost, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return weekR;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getRegRatio(){

		if (Objects.nonNull(clicknums) && Objects.nonNull(usrnamesums) && 0 != usrnamesums &&clicknums > 0) {
			BigDecimal regRatio = new BigDecimal(usrnamesums).divide(new BigDecimal(clicknums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return regRatio;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getActarpu() {
				if (Objects.nonNull(payuuidfees) && Objects.nonNull(actuuidnums) && 0 != actuuidnums &&payuuidfees.compareTo(new BigDecimal(9)) != 0) {
			BigDecimal arpu = payuuidfees.divide(new BigDecimal(actuuidnums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return arpu;
		}
		return BigDecimal.ZERO;
	}



	public BigDecimal getClickratio() {
		if (Objects.nonNull(shownums) && Objects.nonNull(clicknums) && 0 != shownums && 0 != clicknums) {
			BigDecimal ret = new BigDecimal(clicknums).divide(new BigDecimal(shownums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getClickactiveratio() {
		if (Objects.nonNull(clicknums) && Objects.nonNull(uuidnums) && 0 != clicknums && 0 != uuidnums) {
			BigDecimal ret = new BigDecimal(uuidnums).divide(new BigDecimal(clicknums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getUuidchangeratio() {
		if (Objects.nonNull(uuidnums) && Objects.nonNull(usrnamenums) && 0 != usrnamenums && 0 != uuidnums) {
			BigDecimal ret = new BigDecimal(usrnamenums).divide(new BigDecimal(uuidnums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}


	public BigDecimal getRegchangeratio() {
		if (Objects.nonNull(uuidsums) && Objects.nonNull(usrnamesums) && 0 != usrnamesums && 0 != uuidsums) {
			BigDecimal ret = new BigDecimal(usrnamesums).divide(new BigDecimal(uuidsums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}



	public BigDecimal getActivationcostratio() {
		if (Objects.nonNull(cost) && Objects.nonNull(uuidnums) && BigDecimal.ZERO.compareTo(cost) == -1 && 0 != uuidnums) {
			BigDecimal ret = cost.divide(new BigDecimal(uuidnums), 2, BigDecimal.ROUND_HALF_UP);
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getUuidcostratio() {
		if (Objects.nonNull(cost) && Objects.nonNull(usrnamenums) && BigDecimal.ZERO.compareTo(cost) == -1 && 0 != usrnamenums) {
			BigDecimal ret = cost.divide(new BigDecimal(usrnamenums), 2, BigDecimal.ROUND_HALF_UP);
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getRegcostratio() {
		if (Objects.nonNull(cost) && Objects.nonNull(usrnamesums) && BigDecimal.ZERO.compareTo(cost) == -1 && 0 != usrnamesums) {
			BigDecimal ret = cost.divide(new BigDecimal(usrnamesums), 2, BigDecimal.ROUND_HALF_UP);
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getPaycostratio() {
		if (Objects.nonNull(cost) && Objects.nonNull(newuuidnums) && BigDecimal.ZERO.compareTo(cost) == -1 && 0 != newuuidnums) {
			BigDecimal ret = cost.divide(new BigDecimal(newuuidnums), 2, BigDecimal.ROUND_HALF_UP);
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getPayratio() {
		if (Objects.nonNull(uuidnums) && Objects.nonNull(newuuidnums) && 0 != uuidnums && 0 != newuuidnums) {
			BigDecimal ret = new BigDecimal(newuuidnums).divide(new BigDecimal(uuidnums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getArppuratio() {
		if (Objects.nonNull(newuuidnums) && Objects.nonNull(newuuidfees) && BigDecimal.ZERO.compareTo(newuuidfees) == -1 && 0 != newuuidnums) {
			BigDecimal ret = newuuidfees.divide(new BigDecimal(newuuidnums), 2, BigDecimal.ROUND_HALF_UP);
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getRegarpu() {
		if (Objects.nonNull(uuidnums) && Objects.nonNull(newuuidfees) && BigDecimal.ZERO.compareTo(newuuidfees) == -1 && 0 != uuidnums) {
			BigDecimal ret = newuuidfees.divide(new BigDecimal(uuidnums), 2, BigDecimal.ROUND_HALF_UP);
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getRoiratio() {
		if (Objects.nonNull(cost) && Objects.nonNull(uuidsumfees) && BigDecimal.ZERO.compareTo(uuidsumfees) == -1 && BigDecimal.ZERO.compareTo(cost) == -1) {
			BigDecimal ret = uuidsumfees.divide(cost, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getLtvratio() {
		if (Objects.nonNull(uuidnums) && Objects.nonNull(uuidsumfees) && BigDecimal.ZERO.compareTo(uuidsumfees) == -1 && 0 != uuidnums) {
			BigDecimal ret = uuidsumfees.divide(new BigDecimal(uuidnums), 2, BigDecimal.ROUND_HALF_UP);
			return ret;
		}
		return BigDecimal.ZERO;
	}



	public BigDecimal getRoi1() {
		if (Objects.nonNull(worth1) && Objects.nonNull(cost) && BigDecimal.ZERO.compareTo(worth1) == -1 && BigDecimal.ZERO.compareTo(cost) == -1) {
			BigDecimal ret = worth1.divide(cost, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}





}
