package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.annotation.TableId;
import com.dy.yunying.api.entity.DmSearchTmplate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.Date;
@ApiModel(value = "编辑搜索模板")
@Data
public class SearchTmplateEditReq {
	/**
	 * 唯一标识
	 */
	@NotNull
	@ApiModelProperty(value="唯一标识")
	private Long id;
	/**
	 * 模板名称
	 */
	@ApiModelProperty(value="模板名称")
	private String name;
	/**
	 * 模板元素json信息
	 */
	@ApiModelProperty(value="模板元素json信息",example="\"{filter:{cycle:[],types:[],function:[]},customColumn:[]}\"")
	private String info;
	/**
	 * 模板类型，0:广告数据分析表搜索模板
	 */
	@ApiModelProperty(value="模板类型，0:广告数据分析表搜索模板")
	private Integer type;

	public String getInfo() {
		if(StringUtils.isBlank(info)){
			return "{}";
		}
		return info;
	}

}
