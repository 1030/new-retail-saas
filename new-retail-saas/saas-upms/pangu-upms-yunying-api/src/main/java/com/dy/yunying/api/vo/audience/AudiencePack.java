package com.dy.yunying.api.vo.audience;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 迁移并重写实现逻辑，故出入参数保持不变
 * @Description
 * @Author chengang
 * @Date 2022/7/18
 */
@Data
public class AudiencePack implements Serializable {

	private static final long serialVersionUID = 7283156119641711609L;

	public AudiencePack(List<AudiencePackVo> list, Long total,Long size,Long current){
		this.list = list;
		this.total = total;
		this.size = size;
		this.current = current;
	}

	private List<AudiencePackVo> list;
	//总数
	private Long total = 0L;

	//每页显示条数，默认 10
	private Long size = 10L;

	//当前页
	private Long current = 1L;
}
