package com.dy.yunying.api.dto;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName AdKpiDto
 * @Description todo
 * @Author nieml
 * @Time 2021/7/8 11:19
 * @Version 1.0
 **/
@Data
@ApiModel("Kpi报表查询对象")
public class AdKpiDto {

	@ApiModelProperty("年月")
	private Integer yearMonth;

	@ApiModelProperty("父游戏")
	private String pgidArr;

	@ApiModelProperty("部门id数组,多个用逗号分隔")
	private String deptIdArr;
	public List<Long> deptIdList;

	@ApiModelProperty("投放人数组")
	private String  investorArr;
	private List<Integer>  investorList;

	@ApiModelProperty("客户端操作系统0=android；1=IOS；3=OTHERS或为空；4=H5; 5=PC")
	private Integer os;


	@ApiModelProperty("广告账号数组")
	private String advertiserIdArr;
	private List<String> advertiserIdList;




	public List<String> getAdvertiserIdList() {
		List<String> list = new ArrayList<>();
		return StringUtils.isNotEmpty(advertiserIdArr)? Arrays.asList(advertiserIdArr.split(",")): list;
	}

	public void setAdvertiserIdList(List<String> advertiserIdList) {
		this.advertiserIdList = advertiserIdList;
	}

	public List<Long> getDeptIdList(){
		List<Long> list = new ArrayList<>();
		return StringUtils.isNotEmpty(deptIdArr)? Arrays.stream( Arrays.stream(deptIdArr.split(",")).mapToLong(Long::parseLong).toArray()).boxed().collect(Collectors.toList()): list;
	}

	public List<Integer> getInvestorList(){
		List<Integer> list = new ArrayList<>();
		return StringUtils.isNotEmpty(investorArr)? Arrays.stream( Arrays.stream(investorArr.split(",")).mapToInt(Integer::parseInt).toArray()).boxed().collect(Collectors.toList()): list;
	}

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	@ApiModelProperty("是否系统管理员")
	private int isSys;

	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	public String getAdAccounts() {
		String adAccounts = "'NO'";  // 默认不查询任何广告账户
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			adAccounts = "'" + StringUtils.join(roleAdAccountList, "','") + "'";
		}
		return adAccounts;
	}

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}


}
