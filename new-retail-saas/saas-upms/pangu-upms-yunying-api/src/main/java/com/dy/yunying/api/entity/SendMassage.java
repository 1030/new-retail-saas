package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 短信发送日志表
 * @author  lsw
 * @version  2022-05-06 17:14:29
 * table: send_massage
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "send_massage")
public class SendMassage extends Model<SendMassage>{
	/**
	 * 主键id
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	@TableField(value = "type")
	private String type;
	/**
	 * 发送手机号
	 */
	@TableField(value = "mobile")
	private String mobile;
	/**
	 * 发送手机号
	 */
	@TableField(value = "mobile_list")
	private List<String> mobileList;
	/**
	 * 发送时间
	 */
	@TableField(value = "send_time")
	private Date sendTime;
	/**
	 * 是否成功发送
	 */
	@TableField(value = "is_success")
	private Integer isSuccess;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
}






