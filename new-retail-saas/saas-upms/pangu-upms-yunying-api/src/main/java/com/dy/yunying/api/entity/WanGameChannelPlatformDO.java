package com.dy.yunying.api.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * wan_game_channel_platform
 *
 * @author hjl
 * @date 2020-07-27 18:29:27
 */
@Getter
@Setter
public class WanGameChannelPlatformDO {
	/**
	 *
	 */
	private Integer id;

	/**
	 *
	 */
	private Integer platformid;

	/**
	 *
	 */
	private String name;

	/**
	 *
	 */
	private Long appid;

	/**
	 *
	 */
	private String appname;

	/**
	 *
	 */
	private Integer gameid;
	/**
	 *
	 */
	private String gdtAppid;
	/**
	 *
	 */
	private String gname;
	/**
	 *
	 */
	private String appCnName;
	/**
	 *
	 */
	private String gameEnName;

}