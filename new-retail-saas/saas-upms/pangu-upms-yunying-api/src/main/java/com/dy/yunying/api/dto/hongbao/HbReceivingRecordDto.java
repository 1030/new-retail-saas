package com.dy.yunying.api.dto.hongbao;

import lombok.Data;
import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 奖品领取记录
 * @author  chenxiang
 * @version  2021-10-27 13:55:13
 * table: hb_receiving_record
 */
@Data
public class HbReceivingRecordDto implements Serializable {

	/**
	 * 主键ID
	 */
	private Long id;
	/**
	 * 活动ID
	 */
	private Long activityId;
	/**
	 * 活动ID
	 */
	private String activityName;
	/**
	 * 类型：1红包，2提现
	 */
	private Integer objectType;
	/**
	 * 红包ID,提现档次id
	 */
	private Long objectId;
	/**
	 * 领取类型：1红包，2礼包码，3实物礼品，4代金券
	 */
	private Integer type;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 用户名
	 */
	private String userName;
	/**
	 * 父游戏ID
	 */
	private Integer parentGameId;
	/**
	 * 父游戏名称
	 */
	private String parentGameName;
	/**
	 * 子游戏ID
	 */
	private Integer gameId;
	/**
	 * 子游戏名称
	 */
	private String gameName;
	/**
	 * 区服ID
	 */
	private Integer areaId;
	/**
	 * 角色ID
	 */
	private String roleId;
	/**
	 * 角色名称
	 */
	private String roleName;
	/**
	 * 礼包ID：如礼包主键，代金券主键
	 */
	private String giftId;
	/**
	 * 礼包码
	 */
	private String giftCode;
	/**
	 * 领取时间
	 */
	private Date receiveTime;
	/**
	 * 领取时间
	 */
	private String receiveTimeStr;
	/**
	 * 驳回原因
	 */
	private String forbidReason;
	/**
	 * 状态：1已到账，2待填写地址，3待审核，4待发货，5已发货，6已收货，7已驳回
	 */
	private Integer status;
	/**
	 * 是否删除：0否 1是
	 */
	private Integer deleted;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

	/**
	 * 修改时间
	 */
	private String updateTimeStr;
	/**
	/**
	 * 创建人
	 */
	private Long createId;
	/**
	 * 修改人
	 */
	private Long updateId;
	/**
	 * 修改昵称
	 */
	private String updateRealName;
	/**
	 * 红包名称
	 */
	private String redpackName;
	/**
	 * 红包金额
	 */
	private BigDecimal money;
	/**
	 * 红包价值
	 */
	private BigDecimal cashValues;
	/**
	 * 接收人姓名
	 */
	private String receiveUserName;
	/**
	 * 接收人电话
	 */
	private String receiveUserMobile;
	/**
	 * 接收人地址
	 */
	private String receiveUserAddress;
	/**
	 * 发货时间
	 */
	private Date sendTime;
	/**
	 * 发货时间
	 */
	private String sendTimeStr;
	/**
	 * 快递公司
	 */
	private String expressName;
	/**
	 * 快递单号
	 */
	private String expressCode;
}


