package com.dy.yunying.api.vo.usergroup;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class UserGroupExportVo implements Serializable {

	private static final long serialVersionUID = 6185421481781860269L;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("角色ID")
	private String roleId;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("角色名")
	private String roleName;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("账号")
	private String username;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("主渠道")
	@ApiModelProperty(value = "主渠道")
	private String parentchl;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("子渠道")
	@ApiModelProperty(value = "子渠道")
	private String chl;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("父游戏")
	private String parentGameName;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("子游戏")
	private String gameName;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("区服")
	private String areaid;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("累计充值金额")
	private BigDecimal payAmountTotal;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("注册时间")
	private String accountRegTime;

	//创角时间
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("创角时间")
	private String roleRegTime;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("最后登录时间")
	private String lastLoginTime;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("最后充值时间")
	private String lastPayTime;

	//地区
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("地区")
	private String area;

	//手机号
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("手机号")
	private String mobile;

	//设备型号
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("设备型号")
	private String device;

}
