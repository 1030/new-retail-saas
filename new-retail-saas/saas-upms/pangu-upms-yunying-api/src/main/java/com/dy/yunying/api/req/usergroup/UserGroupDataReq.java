package com.dy.yunying.api.req.usergroup;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Data
public class UserGroupDataReq extends Page implements Serializable {

	private static final long serialVersionUID = -1572324353914510716L;


	@ApiModelProperty(value = "主键ID")
	private Long id;

	@ApiModelProperty(value = "主键IDs")
	private String ids;


	@ApiModelProperty(value = "群组ID")
	private Long groupId;

	@ApiModelProperty(value = "筛选维度：1账号，2角色")
	private Integer dimension;

	@ApiModelProperty(value = "筛选方式：1规则筛选，2ID筛选")
	private Integer type;


	private MultipartFile multipartFile;

}
