package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName HbActivityTypeEnum.java
 * @createTime 2021年10月26日 10:35:00
 */
public enum HbActivityTypeEnum {
	LEVEL(1, "等级红包"),
	RECHARGE(2, "充值红包"),
	INVITATION(3, "邀请红包"),
	CUSTOM(4, "定制红包");

	private Integer type;
	private String name;
	public Integer getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private HbActivityTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(Integer type) {
		if (type == null){
			return null;
		}
		for (HbActivityTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return null;
	}
}
