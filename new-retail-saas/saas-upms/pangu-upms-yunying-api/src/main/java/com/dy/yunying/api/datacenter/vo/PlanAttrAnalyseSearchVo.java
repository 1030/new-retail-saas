package com.dy.yunying.api.datacenter.vo;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ApiModel(value = "计划属性分析报表查询参数")
@Data
public class PlanAttrAnalyseSearchVo {

	//排序
	private String sort;

	//排序 指标字段值
	private String kpiValue;

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	public String getAdAccounts() {
		String adAccounts = "'NO'";  // 默认不查询任何广告账户
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			adAccounts = "'" + StringUtils.join(roleAdAccountList, "','") + "'";
		}
		return adAccounts;
	}


	private Integer cycleType = 4;

	/**
	 * 是否系统管理员
	 */
	private int isSys = 0;

	@ApiModelProperty(value = "查询时间，格式为YYYYMMDD")
	private String date;
	@ApiModelProperty(value = "主渠道编码")
	@NotBlank(message = "主渠道编码不能为空")
	private String parentchlArr;
	@ApiModelProperty(value = "渠道类型/平台")
	@NotNull(message = "渠道类型不能为空")
	private Integer ctype;
	@ApiModelProperty(value = "主游戏id列表")
	private String pgidArr;
	@ApiModelProperty(value = "子游戏id列表")
	private String gameidArr;
	@ApiModelProperty(value = "素材名称")
	private String adMaterialName;
	@ApiModelProperty("转化/深化目标")
	private String convertArr;
	@ApiModelProperty("深度转化目标")
	private String deepConvertArr;
	@ApiModelProperty("深度转化目标出价方式")
	private String convertDataTypeArr;
	@ApiModelProperty(value = "客户端操作系统")
	private Integer os;
	@ApiModelProperty(value = "分包编码列表")
	private String appchlArr;
	@ApiModelProperty(value = "部门ID列表")
	private String deptIdArr;
	@ApiModelProperty(value = "用户组Id列表")
	private String userGroupIdArr;
	@ApiModelProperty(value = "广告主ID列表")
	private String advertiserIdArr;
	@ApiModelProperty(value = "投放人ID列表")
	private String investorArr;
	@ApiModelProperty(value = "广告状态列表")
	private String adStatusArr;
	@ApiModelProperty(value = "分类列表")
	private String queryColumn;
	@ApiModelProperty(value = "当前页")
	private Long current = 1L;
	@ApiModelProperty(value = "页数据条数")
	private Long size = 10000L;
	@ApiModelProperty(value = "导出excel表头名称")
	private String titles;
	@ApiModelProperty(value = "导出excel数据列标识名")
	private String columns;
	private List<Long> gameIds;

	public List<String> getQueryColumn() {
		if (StringUtils.isBlank(queryColumn)) {
			return ListUtil.empty();
		} else {
			return Arrays.stream(queryColumn.split(Constants.COMMA)).collect(Collectors.toList());
		}
	}

	private Boolean enableTest = false;
}
