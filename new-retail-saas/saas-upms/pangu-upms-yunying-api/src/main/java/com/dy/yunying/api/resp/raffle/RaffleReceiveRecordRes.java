package com.dy.yunying.api.resp.raffle;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 领取记录信息表
 * @author  chenxiang
 * @version  2022-11-08 16:01:24
 * table: raffle_receive_record
 */
@Data
public class RaffleReceiveRecordRes implements Serializable {

	@ApiModelProperty(value = "主键id")
	private Long id;

	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	@ApiModelProperty(value = "领取类型：1-抽奖中奖记录；2-积分兑换记录；")
	private Integer objectType;

	@ApiModelProperty(value = "奖品配置ID")
	private Long objectId;

	@ApiModelProperty(value = "奖品名称")
	private String objectName;

	@ApiModelProperty(value = "用户ID")
	private Long userId;

	@ApiModelProperty(value = "用户账号")
	private String username;

	@ApiModelProperty(value = "主游戏ID")
	private Long parentGameId;

	@ApiModelProperty(value = "子游戏ID")
	private Long subGameId;

	@ApiModelProperty(value = "区服ID")
	private String areaId;

	@ApiModelProperty(value = "角色ID")
	private String roleId;

	@ApiModelProperty(value = "角色名称")
	private String roleName;

	@ApiModelProperty(value = "主渠道")
	private String mainChl;

	@ApiModelProperty(value = "子渠道")
	private String subChl;

	@ApiModelProperty(value = "分包渠道")
	private String pkgCode;

	@ApiModelProperty(value = "奖品类型：1-礼包码；2-代金券；3-游豆；4-实物礼品；")
	private Integer giftType;

	@ApiModelProperty(value = "奖品发放数量")
	private Integer giftNum;

	@ApiModelProperty(value = "外部奖品ID")
	private Long giftId;

	@ApiModelProperty(value = "礼包码编码/代金券编码(逗号分隔)")
	private String giftCode;

	@ApiModelProperty(value = "代金券名称/实物礼品名称")
	private String giftName;

	@ApiModelProperty(value = "代金券金额/折扣券比例/游豆数量")
	private BigDecimal giftAmount;

	@ApiModelProperty(value = "代金券使用限制金额")
	private BigDecimal giftUseLimit;

	@ApiModelProperty(value = "奖品使用范围：1-不限；2-游戏；3-角色；")
	private Integer giftUseScope;

	@ApiModelProperty(value = "奖品过期类型：1-永久有效；2-临时奖品；")
	private Integer giftExpireType;

	@ApiModelProperty(value = "奖品固定生效时间")
	private Date giftStartTime;

	@ApiModelProperty(value = "奖品固定过期时间")
	private Date giftEndTime;

	@ApiModelProperty(value = "领取时间")
	private Date receiveTime;

	@ApiModelProperty(value = "领取记录状态：1-未填写地址；2-待审核；3-审核驳回；4-审核通过/已发货；")
	private Integer recordStatus;

	@ApiModelProperty(value = "驳回原因")
	private String forbidReason;

	@ApiModelProperty(value = "收货人姓名")
	private String receiptUsername;

	@ApiModelProperty(value = "收货人电话")
	private String receiptPhone;

	@ApiModelProperty(value = "receipt_province_code")
	private String receiptProvinceCode;

	@ApiModelProperty(value = "receipt_province_name")
	private String receiptProvinceName;

	@ApiModelProperty(value = "receipt_city_code")
	private String receiptCityCode;

	@ApiModelProperty(value = "receipt_city_name")
	private String receiptCityName;

	@ApiModelProperty(value = "receipt_area_code")
	private String receiptAreaCode;

	@ApiModelProperty(value = "receipt_area_name")
	private String receiptAreaName;

	@ApiModelProperty(value = "receipt_address")
	private String receiptAddress;

	@ApiModelProperty(value = "发货时间")
	private Date deliverTime;

	@ApiModelProperty(value = "快递名称")
	private String expressName;

	@ApiModelProperty(value = "快递订单号")
	private String expressCode;

	@ApiModelProperty(value = "是否删除：0-否；1-是；")
	private Integer deleted;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

	@ApiModelProperty(value = "创建人")
	private Long createId;

	@ApiModelProperty(value = "修改人")
	private Long updateId;

	/**
	 * 活动名称
	 */
	private String activityName;
	/**
	 * 用户地址
	 */
	private String userAddress;
	/**
	 * 操作人名称
	 */
	private String updateName;
	/**
	 * 父游戏名称
	 */
	private String parentGameName;;
	/**
	 * 子游戏名称
	 */
	private String gameName;
}


