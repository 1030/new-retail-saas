package com.dy.yunying.api.constant;

/**
 * @ClassName RetentionKpiEnum
 * @Description 周期类型 ：1 按天 2 按周  3  按月 4 汇总（默认）
 * @Author nieml
 * @Time 2021/6/21 17:12
 * @Version 1.0
 **/
public enum CycleTypeEnum {

	DAY(1,"day"),
	WEEK(2,"week"),
	MONTH(3,"month"),
	ALL(4,"all");

	private CycleTypeEnum(Integer type,String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(String type) {
		for (CycleTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return "未知";
	}

	private Integer type;

	private String name;

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
