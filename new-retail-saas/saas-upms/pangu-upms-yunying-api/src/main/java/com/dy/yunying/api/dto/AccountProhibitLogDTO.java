package com.dy.yunying.api.dto;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * wan_prohibit_log
 *
 * @author leisw
 * @date 2022-08-22 19:57:35
 */
@Getter
@Setter
public class AccountProhibitLogDTO {
    /**
     * 自增ID
     */
	@TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 类型：1：账号；2：IP；3：OAID；4：IMEI
     */
    private Integer type;

    /**
     * 封禁内容
     */
    private String content;

    /**
     * 备注，封禁原因
     */
    private String remark;

    /**
     * 封禁时长，单位：分钟。0：永久封禁
     */
    private Integer time;

    /**
     * 封禁状态，1：封禁；2：解封
     */
    private Integer status;

    /**
     * 创建者
     */
    private Long creator;

    /**
     * 创建时间
     */
    private Date createTime;
}