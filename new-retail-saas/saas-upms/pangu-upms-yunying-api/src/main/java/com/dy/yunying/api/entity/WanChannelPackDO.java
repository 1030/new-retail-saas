package com.dy.yunying.api.entity;

import lombok.Data;

import java.util.Date;

/**
 * wan_channel_pack
 *
 * @author hjl
 * @date 2020-08-25 17:20:39
 */
@Data
public class WanChannelPackDO {
	/**
	 * 分包渠道ID
	 */
	private Long packId;

	/**
	 * 游戏ID
	 */
	private Long gameId;

	/**
	 * 母包类型：1-运营母包；2-云微端；
	 */
	private Integer packType;

	/**
	 * 版本包ID
	 */
	private Long versionId;

	/**
	 * 分包类型：1、渠道分包；2：子游戏基础包
	 */
	private Integer type;

	/**
	 * 分包编码（游戏编码）
	 */
	private String code;

	/**
	 * 所属渠道编码（子渠道）
	 */
	private String chlCode;

	/**
	 * 游戏包路径
	 */
	private String path;

	/**
	 * 状态：1：生成中，2：已生成；3：失效
	 */
	private Integer status;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	private String app_file_url_root;

	/**
	 * 推广类型（1信息流 2短视频）
	 */
	private Integer spreadType;

	/**
	 * 结算类型(KOL(一口价) KOL(CPM按次))
	 */
	private String settleType;

}