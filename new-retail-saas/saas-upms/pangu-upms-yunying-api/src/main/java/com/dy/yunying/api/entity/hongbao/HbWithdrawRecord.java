package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 提现记录表
 *
 * @author zxm
 * @date 2021-10-28 20:14:35
 */
@Data
@TableName("hb_withdraw_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "提现记录表")
public class HbWithdrawRecord extends HbBaseEntity {

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "提现记录ID")
	private Long id;

	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	@ApiModelProperty(value = "提现方式：1游戏货币，2微信，3支付宝  4  代金券  5 游豆")
	private Integer type;

	@ApiModelProperty(value = "提现金额")
	private BigDecimal money;

	@ApiModelProperty(value = "档次ID")
	private Long levelId;

	@ApiModelProperty(value = "礼包ID")
	private Long bagId;

	@ApiModelProperty(value = "礼包码")
	private String giftCode;

	@ApiModelProperty(value = "代金券编码")
	private String cdkCode;

	@ApiModelProperty(value = "昵称")
	private String nickname;

	@ApiModelProperty(value = "openid")
	private String openid;

	@ApiModelProperty(value = "用户ID")
	private Long userId;

	@ApiModelProperty(value = "用户名称")
	private String username;

	@ApiModelProperty(value = "主游戏ID")
	private Integer parentGameId;

	@ApiModelProperty(value = "游戏ID")
	private Integer gameId;

	@ApiModelProperty(value = "区服ID")
	private Integer areaId;

	@ApiModelProperty(value = "角色ID")
	private String roleId;

	@ApiModelProperty(value = "角色名称")
	private String roleName;

	@ApiModelProperty(value = "状态：0审核中，1审核通过，2提现中 3提现失败 4提现成功 5交易成功（游戏货币使用）")
	private Integer status;

	@ApiModelProperty(value = "提现时间")
	private Date withdrawTime;


	@ApiModelProperty(value = "审核时间")
	private Date auditingTime;

	@ApiModelProperty(value = "审核人")
	private Long auditingId;

	@ApiModelProperty(value = "到账时间")
	private Date payedTime;

	@ApiModelProperty(value = "驳回原因")
	private String rejectReason;

	@ApiModelProperty(value = "是否删除：0否 1是")
	private Integer deleted;


	@TableField(exist = false)
	@ApiModelProperty(value = "父游戏名称")
	private String parentGameName;

	@TableField(exist = false)
	@ApiModelProperty(value = "子游戏名称")
	private String gameName;

	@TableField(exist = false)
	@ApiModelProperty(value = "活动名称")
	private String activityName;

	@ApiModelProperty(value = "商户订单号")
	private String partnerTradeNo;

	@ApiModelProperty(value = "微信付款单号")
	private String paymentNo;

	@ApiModelProperty(value = "付款成功时间")
	private String paymentTime;

	@ApiModelProperty(value = "错误码信息")
	private String errCode;


	@ApiModelProperty(value = "错误代码描述")
	private String errCodeDes;

	@ApiModelProperty(value = "主渠道")
	private String parentchl;

	@ApiModelProperty(value = "子渠道")
	private String chl;

	@ApiModelProperty(value = "分包渠道")
	private String appchl;

	@ApiModelProperty(value = "游豆数额")
	private BigDecimal currencyAmount;

	@ApiModelProperty(value = "提现申请单号")
	private String withdrawNo;

	@TableField(exist = false)
	@ApiModelProperty(value = "状态：0审核中，1审核通过，2提现中 3提现失败 4提现成功 5交易成功（游戏货币使用）")
	private String statusName;
	@TableField(exist = false)
	@ApiModelProperty(value = "提现方式：1游戏货币，2微信，3支付宝")
	private String typeName;

}
