package com.dy.yunying.api.datacenter.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * dogame分时游戏注册
 * @author hma
 * @date 2022/8/19 15:14
 */
@Data
@ApiModel("dogame分时游戏注册数据")
public class TimeGameDeviceRegVO implements Serializable {
	/**
	 * 日期
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("日期")
	private String day;
	/**
	 * 新增账号
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("新增设备")
	private Integer accountNums;

	/**
	 *  00-01:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("01:00")
	private Integer accountNums1;


	/**
	 *  01-02:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("02:00")
	private Integer accountNums2;

	/**
	 *  02-03:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("03:00")
	private Integer accountNums3;

	/**
	 *  03-04:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("04:00")
	private Integer accountNums4;
	/**
	 *  04-05:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("05:00")
	private Integer accountNums5;
	/**
	 *  05-06:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("06:00")
	private Integer accountNums6;
	/**
	 *  06-07:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("07:00")
	private Integer accountNums7;
	/**
	 *  07-08:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("08:00")
	private Integer accountNums8;
	/**
	 *  08-09:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("09:00")
	private Integer accountNums9;
	/**
	 * 09-10:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("10:00")
	private Integer accountNums10;
	/**
	 * 10-11:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("11:00")
	private Integer accountNums11;
	/**
	 * 11-12:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("12:00")
	private Integer accountNums12;
	/**
	 * 12-13:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("13:00")
	private Integer accountNums13;

	/**
	 * 13-14:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("14:00")
	private Integer accountNums14;

	/**
	 * 14-15:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("15:00")
	private Integer accountNums15;

	/**
	 * 15-16:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("16:00")
	private Integer accountNums16;

	/**
	 * 16-17:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("17:00")
	private Integer accountNums17;


	/**
	 * 17-18:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("18:00")
	private Integer accountNums18;

	/**
	 *  18-19:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("19:00")
	private Integer accountNums19;



	/**
	 *  19-20:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("20:00")
	private Integer accountNums20;
	/**
	 *  20-21:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("21:00")
	private Integer accountNums21;
	/**
	 * 21-22:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("22:00")
	private Integer accountNums22;
	/**
	 * 22-23:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("23:00")
	private Integer accountNums23;
	/**
	 *23-24:00新增用户
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("24:00")
	private Integer accountNums24;


}
