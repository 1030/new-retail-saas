package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @ClassName AdmonitorRule
 * @Description 广告监控情形表
 * @Author yangyh
 * @Time 2021/6/18 11:39
 * @Version 1.0
 **/
@Data
@TableName("ad_monitor_rule_situation")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "广告监控情形表")
public class AdmonitorSituation extends Model<AdmonitorSituation> {
	/**
	 * 主键
	 */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value="主键")
	private Integer id;

	/**
	 * 监控规则ID
	 */
	@ApiModelProperty(value="监控规则ID")
	private Integer adMonitorRuleId;

	/**
	 * 监控项
	 */
	@ApiModelProperty(value="监控项")
	private String monitorAditem;

	/**
	 * 监控指标
	 */
	@ApiModelProperty(value="监控指标")
	private String monitorTarget;

	/**
	 * 比较运算符
	 */
	@ApiModelProperty(value="比较运算符")
	private String compare;


	/**
	 * 比较值
	 */
	@ApiModelProperty(value="比较值")
	private String compareValue;

	/**
	 * 结果数
	 */
	private Integer resultNo;

	/**
	 * 删除状态：0正常，1删除
	 */
	@ApiModelProperty(value="删除状态：0正常，1删除")
	private Integer delFlag;
	/**
	 * 创建人
	 */
	@ApiModelProperty(value="创建人")
	private Integer createBy;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value="创建时间")
	private Date createTime;
	/**
	 * 修改人
	 */
	@ApiModelProperty(value="修改人")
	private Integer updateBy;
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value="修改时间")
	private Date updateTime;
}
