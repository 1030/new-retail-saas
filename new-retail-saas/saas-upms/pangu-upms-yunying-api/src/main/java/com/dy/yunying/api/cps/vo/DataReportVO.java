package com.dy.yunying.api.cps.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author leisw
 * @date 2022/9/15 15:14
 */
@Data
@ApiModel("cps数据报表")
public class DataReportVO implements Serializable {
	/**
	 * 周期
	 */
	private String period;
	// 子游戏id
	@ApiModelProperty("子游戏id")
	private Long appCode;

	@ApiModelProperty("子游戏")
	private String gameName;

	// 渠道名称
	@ApiModelProperty("子渠道")
	private String chlSub;


	@ApiModelProperty("子渠道名称")
	@ExcelProperty("子渠道")
	private String chlName;

	// 渠道类型
	@ApiModelProperty("分包渠道")
	private String chlApp;
	/**
	 * 主渠道名称
	 */
	@ApiModelProperty("分包渠道名称")
	@ExcelProperty("分包渠道")
	private String chlAppName;
	/**
	 * 激活数
	 */
	private Integer accountNums;
	/**
	 * 新增设备注册数
	 */
	private Integer accountRegNums;
	/**
	 * 激活率
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private String accountNumRate;
	/**
	 * 新账号充值人数
	 */
	private Integer newPayNums;

	/**
	 * 新增付费率
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private String newpayRate;
	/**
	 * 新账号充值金额
	 */
	private BigDecimal newPayAmounts;
	/**
	 * 新账号实付充值金额
	 */
	private BigDecimal newPayFeeAmounts;
	/**
	 * 期内充值实付金额
	 */
	private BigDecimal periodTotalPayFeeAmount;
	/**
	 * 新账号代金券充值金额
	 */
	private BigDecimal newPayGiveMoney;
	/**
	 * 活跃用户数
	 */
	private Integer activeaccounts;

	/**
	 * 活跃付费数
	 */
	private Integer activeFeeAccounts;
	/**
	 * 活跃付费率
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private String accountpayRate;
	/**
	 * 活跃充值金额
	 */
	private BigDecimal activePayAmounts;
	/**
	 * 活跃充值实付金额
	 */
	private BigDecimal activePayFeeAmounts;
	/**
	 * 活跃充值代金券金额
	 */
	private BigDecimal activeGiveAmounts;
	/**
	 * 累计付费数
	 */
	private Integer totalpaynums;
	/**
	 * 累计充值金额
	 */
	private BigDecimal totalpayamounts;
	/**
	 * 累计充值实付金额
	 */
	private BigDecimal totalpayfeeamounts;
	/**
	 * 累计充值代金券金额
	 */
	private BigDecimal totalpaygivemoneys;


}

