package com.dy.yunying.api.req.prize;
import io.swagger.annotations.ApiModelProperty;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

/**
 * 奖励与游戏关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:59
 * table: prize_push_game
 */
@Data
public class PrizePushGameReq extends Page {

	@ApiModelProperty(value = "主键ID")
	private String id;

	@ApiModelProperty(value = "奖励推送ID")
	private String prizePushId;

	@ApiModelProperty(value = "主游戏ID")
	private String parentGameId;

	@ApiModelProperty(value = "子游戏ID")
	private String childGameId;

	@ApiModelProperty(value = "游戏层级(1主游戏 2子游戏)")
	private String gameRange;
}
