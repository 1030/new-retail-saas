package com.dy.yunying.api.datacenter.export;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pig4cloud.pig.common.core.util.BigDecimalUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/6/17 15:12
 * @description：
 * @modified By：
 */
@Data
@Accessors(chain = true)
public class AdExportDataVo  implements Serializable {

	private static final long serialVersionUID = 8701826231231117248L;

	private static final String DEFAULT_VALUE = "-";

	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	private String period;

	@ExcelProperty("系统")
	@ApiModelProperty("系统")
	private String osStr;

	@ExcelProperty("主游戏")
	@ApiModelProperty("主游戏")
	private String parentGameName;

	@ExcelProperty("部门")
	@ApiModelProperty("部门")
	private String deptName;


	@ExcelProperty("组别")
	@ApiModelProperty("组别")
	private String userGroupName;

	@ExcelProperty("投放人")
	@ApiModelProperty("投放人")
	private String investorName;

	// 返点后消耗（总成本）
	@ExcelProperty("消耗")
	@ApiModelProperty("消耗")
	private BigDecimal rudeCost = new BigDecimal(0);

	@ExcelProperty("返点后消耗")
	@ApiModelProperty("消耗后消耗（总成本）")
	@JsonProperty("cost")
	private BigDecimal cost = new BigDecimal(0);

	@ExcelProperty("新增设备注册数")
	@ApiModelProperty("新增设备注册数")
	private Integer usrnamenums = 0;

	// 设备成本  消耗/新增设备注册数
	@ExcelProperty("设备成本")
	@ApiModelProperty("设备成本")
	private BigDecimal deviceCose = new BigDecimal(0);

	// 新增注册创角数
	@ExcelProperty("创角数")
	@ApiModelProperty("新增注册创角数")
	private Integer createRoleCount;

	// 新增创角率
	@ExcelProperty("注册创角率")
	@ApiModelProperty("注册创角率")
	private String createRoleRate;

	// 新增注册实名数
	@ExcelProperty("新增注册实名数")
	@ApiModelProperty("新增注册实名数")
	private Integer certifiedCount;

	// 注册未实名数
	@ExcelProperty("注册未实名数")
	@ApiModelProperty("注册未实名数")
	private Integer notCertifiedCount;

	// 新增实名制转化率
	@ExcelProperty("注册实名率")
	@ApiModelProperty("注册实名率")
	private String certifiedRate;

	//注册arpu  新增充值金额/新增设备数
	@ExcelProperty("新增ARPU")
	@ApiModelProperty("新增ARPU")
	private BigDecimal regarpu = new BigDecimal(0);

	// 新增付费ARPPU
	@ExcelProperty("新增付费ARPPU")
	@ApiModelProperty("新增付费ARPPU")
	private BigDecimal newPayArppu;


	//新增付费率  新增设备付费数/新增设备注册数
	@ExcelProperty("新增付费率")
	@ApiModelProperty("新增付费率")
	private String regPayRatio;

	@ExcelProperty("新增充值实付金额")
	@ApiModelProperty("新增充实付值金额")
	private BigDecimal newPayAmount = new BigDecimal(0);

	@ExcelProperty("新增充值代金券金额")
	@ApiModelProperty("新增充代金券值金额")
	private BigDecimal newPayGivemoney = new BigDecimal(0);

	@ExcelProperty("活跃充值实付金额")
	@ApiModelProperty("活跃充值实付金额")
	private BigDecimal activetotalfee = new BigDecimal(0);

	@ExcelProperty("活跃充值代金券金额")
	@ApiModelProperty("活跃充值代金券金额")
	private BigDecimal activePayGivemoney = new BigDecimal(0);

	@ExcelProperty("当周充值实付金额")
	@ApiModelProperty("当周充值实付金额")
	private BigDecimal weektotalfee = new BigDecimal(0);

	@ExcelProperty("当月充值实付金额")
	@ApiModelProperty("当月充值实付金额")
	private BigDecimal monthtotalfee = new BigDecimal(0);

	// 活跃付费设备数
	@ExcelProperty("活跃付费数")
	@ApiModelProperty("活跃付费数")
	private Integer activePayCount;


	// 活跃ARPU= 活跃充值金额/活跃设备数
	@ExcelProperty("活跃ARPU")
	@ApiModelProperty("活跃ARPU")
	private BigDecimal actarpu = new BigDecimal(0);

	// 活跃付费ARPPU
	@ExcelProperty("活跃ARPPU")
	@ApiModelProperty("活跃ARPPU")
	private BigDecimal activePayArppu;

	@ExcelProperty("活跃设备数")
	@ApiModelProperty("活跃设备数")
	private Integer activedevices = 0;

	// 活跃付费率
	@ExcelProperty("活跃付费率")
	@ApiModelProperty("活跃付费率")
	private String activePayRate;

	// roi1
	@ExcelProperty("首日ROI")
	@ApiModelProperty("首日ROI")
	private String roi1;

	@ExcelProperty("当周ROI")
	@ApiModelProperty("当周ROI ")
	private String weekRoi;

	@ExcelProperty("当月ROI")
	@ApiModelProperty("当月ROI ")
	private String monthRoi;

	// 期内付费设备数
	@ExcelProperty("期内充值人数")
	@ApiModelProperty("期内充值人数")
	private Integer periodPayCount;

	// 期内充值实付金额
	@ExcelProperty("期内充值金额")
	@ApiModelProperty("期内充值金额")
	private BigDecimal periodPayFee;

	@ExcelProperty("期内付费成本")
	@ApiModelProperty("期内付费成本")
	private BigDecimal periodPayCose = new BigDecimal(0);

	// 期内付费率
	@ExcelProperty("期内付费率")
	@ApiModelProperty("期内付费率")
	private String periodPayRate;

	//总roi 累计充值金额/总消耗
	@ExcelProperty("累计充值ROI")
	@ApiModelProperty("累计充值ROI")
	private String allRoi;

	// 累计充值金额
	@ExcelProperty("累计充值实付金额")
	@ApiModelProperty("累计充值实付金额")
	private BigDecimal totalPayfee = new BigDecimal(0);

	@ExcelProperty("累计充值代金券金额")
	@ApiModelProperty("累计充值代金券金额")
	private BigDecimal totalPayGivemoney = new BigDecimal(0);


	@ExcelProperty("次留")
	@ApiModelProperty("次留")
	@JsonProperty("retention2Ratio")
	private String retention2Ratio;

	@ExcelProperty("设备重复率")
	@ApiModelProperty("设备重复率")
	private String duplicateDeviceRatio;


	@ExcelProperty("父游戏id")
	@ApiModelProperty("父游戏id")
	private Long pgid;

	@ExcelProperty("系统类型")
	@ApiModelProperty("系统类型")
	private Integer os;

	@ExcelProperty("父渠道")
	@ApiModelProperty("父渠道")
	private String parentchl;

	@ExcelProperty("主渠道名称")
	@ApiModelProperty("主渠道名称")
	private String parentchlName;

	@ExcelProperty("部门主键")
	@ApiModelProperty("部门主键")
	private String deptId;

	@ExcelProperty("组别主键")
	@ApiModelProperty("组别主键")
	private String userGroupId;

	@ExcelProperty("投放人")
	@ApiModelProperty("组别名称")
	private String investor;

	// 未成年人数
	@ExcelProperty("未成年人数")
	@ApiModelProperty("未成年人数")
	private Integer youngCount;

	@ExcelProperty("新增设备付费数")
	@ApiModelProperty("新增设备付费数")
	private Integer newPayCount = 0;


	@ExcelProperty("当周充值代金券金额")
	@ApiModelProperty("当周充值代金券金额")
	private BigDecimal weekPayGivemoney = new BigDecimal(0);


	@ExcelProperty("当月充值代金券金额")
	@ApiModelProperty("当月充值代金券金额")
	private BigDecimal monthPayGivemoney = new BigDecimal(0);

	@ExcelProperty("设备次日留存数")
	@ApiModelProperty("设备次日留存数")
	private Integer num1 = 0; //设备次日留存数

	// 重复设备数
	@ExcelProperty("重复设备数")
	private Integer duplicateDeviceCount;

	// 期内充值代金券金额
	@ExcelProperty("期内充值代金券金额")
	@ApiModelProperty("期内充值代金券金额")
	private BigDecimal periodPayGivemoney;

	/**
	 * 四舍五入，保留两位有效数字
	 *
	* */
	public BigDecimal getNewPayAmount() {
		return BigDecimalUtils.round(newPayAmount, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getNewPayGivemoney() {
		return BigDecimalUtils.round(newPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getWeektotalfee() {
		return BigDecimalUtils.round(weektotalfee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getWeekPayGivemoney() {
		return BigDecimalUtils.round(weekPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getMonthtotalfee() {
		return BigDecimalUtils.round(monthtotalfee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getMonthPayGivemoney() {
		return BigDecimalUtils.round(monthPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivetotalfee() {
		return BigDecimalUtils.round(activetotalfee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivePayGivemoney() {
		return BigDecimalUtils.round(activePayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActarpu() {
		return BigDecimalUtils.round(actarpu, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivePayArppu() {
		return BigDecimalUtils.round(activePayArppu, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getNewPayArppu() {
		return BigDecimalUtils.round(newPayArppu, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getTotalPayfee() {
		return BigDecimalUtils.round(totalPayfee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getTotalPayGivemoney() {
		return BigDecimalUtils.round(totalPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getPeriodPayFee() {
		return BigDecimalUtils.round(periodPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getPeriodPayGivemoney() {
		return BigDecimalUtils.round(periodPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public String getOsStr() {
		if (null == this.osStr) {
			this.osStr = null == this.os ? DEFAULT_VALUE : OsEnum.getName(this.os);
		}
		return this.osStr;
	}
}
