package com.dy.yunying.api.dto;

import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
public class RoiTopTenDto {
	/**
	 * 周期
	 */
	private Integer topType;
	/**
	 * 开始日期，格式为：20220322
	 */
	private String sdate;

	/**
	 * 结束日期，格式为：20220322
	 */
	private String edate;

	/**
	 * 主渠道编码
	 */
	private String parentchl;

	/**
	 * 1：头条体验版数据
	 */
	private String experience;

}
