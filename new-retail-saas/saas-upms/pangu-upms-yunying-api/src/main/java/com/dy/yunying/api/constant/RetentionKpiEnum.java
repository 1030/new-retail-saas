package com.dy.yunying.api.constant;

/**
 * @ClassName RetentionKpiEnum
 * @Description todo
 * @Author nieml
 * @Time 2021/6/21 17:12
 * @Version 1.0
 **/
public enum RetentionKpiEnum {

	REG("reg","注册留存"),
	PAY("pay","付费留存");

	private RetentionKpiEnum(String type,String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(String type) {
		for (RetentionKpiEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return "未知";
	}

	private String type;

	private String name;

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
