package com.dy.yunying.api.constant;

/**
 * 订单常量配置
 */
public class RechargeConstants {

    // 待兑换订单号列表
    public final static String REDIS_KEY_RECHARGE_ORDERNO_LIST = "3367v2_recharge_orderno_list";
    // 正式订单信息
    public final static String REDIS_KEY_RECHARGE_ = "3367v2_recharge_";
    // 临时订单信息
    public final static String REDIS_KEY_RECHARGE_TMP_API_ = "3367v2_api_recharge_tmp_";

}
