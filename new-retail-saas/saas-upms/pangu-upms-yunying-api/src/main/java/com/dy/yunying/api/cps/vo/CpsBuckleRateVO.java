package com.dy.yunying.api.cps.vo;

import cn.hutool.core.date.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author sunyq
 * @date 2022/10/12 11:31
 */
@Data
public class CpsBuckleRateVO implements Serializable {
	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 账号ID
	 */
	private Integer userId;

	/**
	 * 账号名称
	 */
	private String username;
	/**
	 * 父游戏id
	 */
	private Long pgid;

	/**
	 * 扣量比例0~99
	 */
	private BigDecimal buckleRate;
	/**
	 * 开始时间
	 */
	private Date startTime;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 生效时间
	 */
	private String effectiveTime;

	public String getEffectiveTime() {
		return DateUtil.format(startTime, "yyyy-MM-dd") +"~"+DateUtil.format(endTime, "yyyy-MM-dd");
	}

	public BigDecimal getBuckleRate() {
		return buckleRate.stripTrailingZeros();
	}
}
