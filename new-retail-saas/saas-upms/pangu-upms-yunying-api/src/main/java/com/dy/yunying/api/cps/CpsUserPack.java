package com.dy.yunying.api.cps;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * cps用户分包授权表
 * @TableName cps_user_pack
 */
@TableName(value ="cps_user_pack")
@Data
public class CpsUserPack implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 账号ID
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 主渠道
     */
    @TableField(value = "parent_chl")
    private String parentChl;

    /**
     * 子渠道
     */
    @TableField(value = "chl")
    private String chl;

    /**
     * 分包渠道
     */
    @TableField(value = "app_chl")
    private String appChl;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 创建人
     */
    @TableField(value = "create_id")
    private Long createId;

    /**
     * 修改人
     */
    @TableField(value = "update_id")
    private Long updateId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}