package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: monitor_rule_manager
 * @Author: hma
 * @Date:   2023-03-21
 * @Version: V1.0
 */
@Data
@TableName("monitor_rule_manager")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="monitor_rule_manager对象", description="monitor_rule_manager")
public class MonitorRuleManager implements Serializable {

	private static final long serialVersionUID = 4825831200472514327L;
	/**主键id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "主键id")
    private Integer id;
	/**规则名称*/
    @ApiModelProperty(value = "规则名称")
    private String monitorName;
	/**监控范围*/
    @ApiModelProperty(value = "监控范围:主渠道")
    private Integer monitorChannelType;
	/**广告账户*/
    @ApiModelProperty(value = "广告账户")
    private String adAccount;
	/**监控游戏ID,多个用逗号分隔，0为不限*/
    @ApiModelProperty(value = "监控游戏ID,多个用逗号分隔，0为不限")
    private String monitorGameId;
	/**监控项：1广告计划  2 广告账号*/
    @ApiModelProperty(value = "监控项：1广告计划  2 广告账号")
    private Integer monitorItem;
	/**监控指标:   1.账号余额 2：0注册的消耗  3 注册成本 4 新增付费成本 5 首日roi 6:7日roi 7:15日roi */
    @ApiModelProperty(value = "监控指标:   1.账号余额 2：0注册的消耗  3 注册成本 4 新增付费成本 5 首日roi 6:7日roi 7:15日roi ")
    private Integer monitorTarget;
	/**比较运算符：=，!=,>,< ,>=,<= */
    @ApiModelProperty(value = "比较运算符：=，!=,>,< ,>=,<= ")
    private String compare;

	/**比较值*/
    @ApiModelProperty(value = "比较值")
    private String compareValue;
	/**通知类型：飞书消息*/
    @ApiModelProperty(value = "通知类型：飞书消息")
    private Integer notifyType;
	/**通知人员*/
    @ApiModelProperty(value = "通知人员")
    private String notifytor;
	/**广告计划*/
    @ApiModelProperty(value = "广告计划")
    private String exceptPlan;
	/**监控状态：0关闭，1开启*/
    @ApiModelProperty(value = "监控状态：0关闭，1开启")
    private Integer monitorStatus;

	@ApiModelProperty(value = "规则是否变更：1 变更  2未变更  ")
	private Integer ruleChange;


	/**广告计划*/
	@ApiModelProperty(value = "最新推送的广告计账号(多个用逗号分隔)")
	private String lastPushAccount;


	/**广告计划*/
	@ApiModelProperty(value = "最新推送的广告计划id(多个用逗号分隔)")
	private String lastPushPlan;


	/**是否删除  0否 1是*/
    @ApiModelProperty(value = "是否删除  0否 1是")
    private Integer deleted;
	/**创建时间*/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**修改时间*/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private Integer createId;
	/**修改人*/
    @ApiModelProperty(value = "修改人")
    private Integer updateId;


	/**
	 * 监控游戏名称
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "监控游戏名称")
	private String monitorGame;



	/**监控账户名称*/
	@TableField(exist = false)
	@ApiModelProperty(value = "监控账户名称")
	private String monitorAccountName;

	@TableField(exist = false)
	/**监控目标名称*/
	@ApiModelProperty(value = "监控目标名称")
	private String monitorTargetName;

	@TableField(exist = false)
	@ApiModelProperty(value = "创建人")
	private String createName;

}
