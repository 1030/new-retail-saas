/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.dy.yunying.api.feign;

import com.dy.yunying.api.entity.AdRoleUser;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/28
 */
@FeignClient(contextId = "remoteAdRoleUserService", value = ServiceNameConstants.UPMS_YUNYING_SERVICE)
public interface RemoteAdRoleUserService {
	/**
	 * 根据用户列表查询绑定的的广告账号列表
	 * @return
	 */
	@PostMapping("/select/roleUserList")
	R getOwnerRoleUsers();

	/**
	 * 获取当前登录用户对应角色集合所关联的投放人集合
	 * @param from
	 * @return
	 */
	@PostMapping("/select/getUserListByRole")
	R<List<AdRoleUser>> getUserListByRole(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody List<Integer> roles);





}
