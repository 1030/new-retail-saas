package com.dy.yunying.api.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class AdMaterialDataDTO {

	private static final String DEFAULT_VALUE = "-";

	/**
	 * 周期
	 */
	private String period;
	/**
	 * 总播放数
	 */
	private Long playTotalNum;

	/**
	 * 完播数
	 */
	private Long playOverNum;

	/**
	 * 完成播放率(%)
	 */
	private BigDecimal playOverRate;

	/**
	 * 有效播放数
	 */
	private Long playValidNum;

	/**
	 * 有效播放率(%)
	 */
	private BigDecimal playValidRate;
	/**
	 * 转化数
	 */
	private Long convertNum;

	/**
	 * 转化率(%)
	 */
	private BigDecimal convertRate;

	/**
	 * 转化成本
	 */
	private BigDecimal convertCost;

	/**
	 * 展示数
	 */
	private Long showNum;

	/**
	 * 点击数
	 */
	private Long clickNum;

	/**
	 * 原始消耗
	 */
	private BigDecimal rudeCost;

	/**
	 * 返点后消耗
	 */
	private BigDecimal cost;

	/**
	 * 展示点击率(%)
	 */
	private BigDecimal showClickRate;

	/**
	 * 注册设备数
	 */
	private Long registerNum;

	/**
	 * 注册成本
	 */
	private BigDecimal registerCost;

	/**
	 * 新增付费设备数
	 */
	private Long newPayNum;

	/**
	 * 新增付费成本
	 */
	private BigDecimal newPayCost;

	/**
	 * 累计付费实付金额
	 */
	private BigDecimal totalPayFee;

	/**
	 * 累计ROI(%)
	 */
	private BigDecimal totalRoi;

	/**
	 * 次留(%)
	 */
	private BigDecimal retention2;

	/**
	 * 7留(%)
	 */
	private BigDecimal retention7;

	/**
	 * 30留(%)
	 */
	private BigDecimal retention30;
}
