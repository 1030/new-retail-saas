package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum BdTransTypeEnum {
	TRANS1("1", "咨询按钮点击"),
	TRANS2("2", "电话按钮点击"),
	TRANS3("3", "表单提交成功"),
	TRANS4("4", "激活"),
	TRANS5("5", "表单按钮点击"),
	TRANS6("6", "下载（预约）按钮点击（小流量）"),
	TRANS10("10", "购买成功"),
	TRANS14("14", "订单提交成功"),
	TRANS17("17", "三句话咨询"),
	TRANS18("18", "留线索"),
	TRANS19("19", "一句话咨询"),
	TRANS20("20", "关键页面浏览"),
	TRANS25("25", "注册（小流量）"),
	TRANS26("26", "付费（小流量）"),
	TRANS27("27", "客户自定义（小流量）"),
	TRANS28("28", "次日留存（小流量）"),
	TRANS30("30", "电话拨通"),
	TRANS35("35", "微信复制按钮点击（小流量）"),
	TRANS41("41", "申请（小流量）"),
	TRANS42("42", "授信（小流量）"),
	TRANS45("45", "商品下单成功"),
	TRANS46("46", "加入购物车"),
	TRANS47("47", "商品收藏"),
	TRANS48("48", "商品详情页到达"),
	TRANS53("53", "订单核对成功"),
	TRANS54("54", "收货成功"),
	TRANS56("56", "到店（小流量）"),
	TRANS57("57", "店铺调起"),
	TRANS67("67", "微信调起按钮点击"),
	TRANS68("68", "粉丝关注成功"),
	TRANS71("71", "应用调起"),
	TRANS72("72", "聊到相关业务（小流量）"),
	TRANS73("73", "回访-电话接通（小流量）"),
	TRANS74("74", "回访-信息确认（小流量）"),
	TRANS75("75", "回访-发现意向（小流量）"),
	TRANS76("76", "回访-高潜成交（小流量）"),
	TRANS77("77", "回访-成单客户（小流量）"),
	TRANS93("93", "付费阅读(小流量)");

	//枚举值所包含的属性
	/**
	 * 名称
	 */
	private String type;


	private String name;

	//构造方法
	BdTransTypeEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}


	/**
	 *
	 * @param type
	 * @return
	 */
	public static String getName(String type){
		if (StringUtils.isBlank(type)){
			return "";
		}

		for (BdTransTypeEnum item : BdTransTypeEnum.values()) {
			if (String.valueOf(item.getType()).equals(type)) {
				return item.getName();
			}
		}
		return "";

	}


}
