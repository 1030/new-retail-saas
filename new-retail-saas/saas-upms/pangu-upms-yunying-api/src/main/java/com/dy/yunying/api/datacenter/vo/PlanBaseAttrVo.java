package com.dy.yunying.api.datacenter.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PlanBaseAttrVo {
	//===========公共属性，根据统计类别，业务含义不同，例如广告计划，则为广告id，广告名称

	// 数据唯一标识
	private String day;

	// 数据唯一标识
	private String id;

	@ExcelProperty("名称")
	private String name;

	@ExcelProperty("广告账户名称")
	private String adAccountName;

	@ExcelProperty("广告账户名称ID")
	private String adAccountId;

	// 目标转化名称
	private String convertName;

	// 目标转化描述
	@ExcelProperty("目标转化")
	private String convertDescri;

	// 深度转化目标
	private String deepConvert;

	// 转化统计方式
	private String convertDataType;

	//===========公共指标属性
	@ExcelProperty("返点后消耗（总成本）")
	private BigDecimal cost;

	/*public BigDecimal getCost() {
		return Objects.isNull(cost) ? BigDecimal.ZERO : cost;
	}*/

	// 返点后消耗（总成本）
	@ExcelProperty("消耗")
	private BigDecimal rudeCost;

	/*public BigDecimal getRudeCost() {
		return Objects.isNull(rudeCost) ? BigDecimal.ZERO : rudeCost;
	}*/

	//新增设备注册数(新用户注册)
	@ExcelProperty("新增设备注册数")
	private Integer usrnamenums; //usrnamenums

	/*public Integer getUsrnamenums() {
		return Objects.isNull(usrnamenums) ? 0 : usrnamenums;
	}*/

	// 设备成本: 返点后消耗 / 新增设备注册数
	@ExcelProperty("设备成本")
	private BigDecimal deviceCose = BigDecimal.ZERO;

	// 首日LTV: 首日充值金额 / 新增设备注册数
	@ExcelProperty("首日LTV")
	private BigDecimal firstDayLtv = BigDecimal.ZERO;

	@JsonIgnore
	@ApiModelProperty("新增设备付费数")
	private Integer paydevice1 = 0;

	@ApiModelProperty("新增充值实付金额（分成前）")
	private BigDecimal newdevicefees;

	@ApiModelProperty("新增充值实付金额（分成后）")
	private BigDecimal worth1 = BigDecimal.ZERO;

	@ApiModelProperty("新增充值代金券金额（分成前）")
	private BigDecimal newdevicegivemoney = BigDecimal.ZERO;

	@ApiModelProperty("新增充值代金券金额（分成后）")
	private BigDecimal newdeviceshargivemoney = BigDecimal.ZERO;

	@ApiModelProperty("累计充值实付金额（分成前）")
	private BigDecimal totalPayfee = BigDecimal.ZERO;

	@ApiModelProperty("累计充值实付金额（分成后）")
	private BigDecimal userfeeAll = BigDecimal.ZERO;

	@ApiModelProperty("累计充值代金券金额（分成前）")
	private BigDecimal totalPaygivemoney = BigDecimal.ZERO;

	@ApiModelProperty("累计充值代金券金额（分成后）")
	private BigDecimal totalPayshargivemoney = BigDecimal.ZERO;

	// 新增设备付费数/新增设备注册数
	// paydeviceAll usrnamenums
	@ExcelProperty("新增付费率")
	private BigDecimal regPayRatio = BigDecimal.ZERO;

	// 新增充值金额 / 返点后消耗
	// worth1 cost
	@ExcelProperty("首日ROI")
	private BigDecimal roi1 = BigDecimal.ZERO;

	// 次留: 新增设备在次日有登录行为的设备数/新增设备注册数
	@ExcelProperty("次留")
	@ApiModelProperty("次留")
	@JsonProperty("retention2Ratio")
	private BigDecimal retention2Ratio = BigDecimal.ZERO;

	// 设备次日留存数
	private Integer retention2 = 0;

	//总roi 累计充值金额/总消耗
	@ExcelProperty("累计充值ROI")
	@ApiModelProperty("累计充值ROI")
	private BigDecimal allRoi = BigDecimal.ZERO;

	// 新增充值金额
	/*@ApiModelProperty("新增充值金额（代金券）")
	private BigDecimal givemoneyAll = BigDecimal.ZERO;*/

	// 新增充值金额
	/*@ExcelProperty("累计充值金额")
	@ApiModelProperty("累计充值金额")
	private BigDecimal allTopUpAmount = BigDecimal.ZERO;*/

	// 新增付费ARPPU
	private BigDecimal newPayArppu;

	// 新增注册创角数
	private Integer createRoleCount;

	// 新增注册创角率
	private BigDecimal createRoleRate;

	// 新增注册实名数
	private Integer certifiedCount;

	// 注册未实名数
	private Integer notCertifiedCount;

	// 未成年人数
	private Integer youngCount;

	// 新增实名转化率
	private BigDecimal certifiedRate;

	/*public BigDecimal getAllTopUpAmount() {
		allTopUpAmount = userfeeAll;
		return allTopUpAmount;
	}*/
}
