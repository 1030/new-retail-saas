package com.dy.yunying.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sunyq
 * @date 2022/8/22 16:13
 */
@Data
public class AccountOperateInfoVO implements Serializable {


	/**
	 * 类型：1：账号；2：IP；3：OAID；4：IMEI
	 */
	private Short type;

	/**
	 * 封禁内容
	 */

	private String content;

	/**
	 * 备注，封禁原因
	 */

	private String remark;

	/**
	 * 封禁状态，1：封禁；2：正常
	 */

	private Short status;

	/**
	 * 创建者
	 */private Long creator;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 创建人名称
	 */
	private String creatorName;
}
