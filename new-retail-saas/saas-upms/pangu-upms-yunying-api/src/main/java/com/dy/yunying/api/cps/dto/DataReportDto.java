package com.dy.yunying.api.cps.dto;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 *
 * @author leisw
 * @date 2022/9/15 10:30
 */
@Data
@ApiModel("cps数据报表查询对象")
public class DataReportDto {
	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

	@ApiModelProperty("是否系统管理员")
	private int isSys;

	@ApiModelProperty("查询开始时间")
	@NotNull(message = "查询开始日期不允许为空")
	private String startTime;

	@ApiModelProperty("查询结束时间")
	@NotNull(message = "查询结束日期不允许为空")
	private String endTime;


	@ApiModelProperty("查询充值开始时间")
	private String payStartTime;

	@ApiModelProperty("查询充值结束时间")
	private String payEndTime;


	@ApiModelProperty("子游戏")
	private String gameIdArr;

	@ApiModelProperty("主渠道列表")
	private String parentChlArr;

	@ApiModelProperty("子渠道编码列表")
	private String chlArr;

	@ApiModelProperty("分包渠道")
	private String appchlArr;

	@ApiModelProperty("查询的字段 多个用逗号分隔  ")
	private String columns;

	@ApiModelProperty("导出的时候对应的表名")
	private String titles;

	@ApiModelProperty("当前页")
	private Long current;

	@ApiModelProperty("每页的size")
	private Long size;

	@ApiModelProperty("排序  desc / asc")
	private String sort;

	@ApiModelProperty("排序字段")
	private String kpiValue;



	@ApiModelProperty("周期类型 ：1 按天 2 按周  3  按月 4 按年 5汇总（默认）")
	private Integer cycleType = 5;


	public String getPeriod() {
		String period =" '汇总' ";
		switch (cycleType) {
			case 1:
				period = "day";
				break; //可选
			case 2:
				period = "week";
				//语句
				break; //可选
			case 3:
				period = "month";
				break; //可选
			case 4:
				period = "year";
				break; //可选
			case 5:
				period = " '汇总' ";
				break; //可选
			//你可以有任意数量的case语句
			default: //可选
				//语句
		}
		return period;
	}




	@ApiModelProperty("类别名称 逗号分割  app_code(子游戏),chl_sub（子渠道）,chl_app(分包)")
	private String queryColumn;

	/**
	 * 汇总字段标识
	 */
	private String collect;
}

