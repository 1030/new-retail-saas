package com.dy.yunying.api.entity.znfx;

import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: zhuxm
 * @time: 2023/3/21 17:53
 */
@Data
public class ChartDataDetail implements Serializable {

	private static final long serialVersionUID = 6111441733277571944L;

	private String name;

	private Long value;

	public ChartDataDetail(){}

	public ChartDataDetail(String name, Long value) {
		this.name = name;
		this.value = value;
	}
}
