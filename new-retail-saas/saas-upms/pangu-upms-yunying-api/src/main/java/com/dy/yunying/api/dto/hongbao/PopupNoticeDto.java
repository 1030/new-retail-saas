package com.dy.yunying.api.dto.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dy.yunying.api.req.hongbao.AreaReq;
import com.dy.yunying.api.req.hongbao.ChannelReq;
import com.dy.yunying.api.req.hongbao.GameReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author ：lile
 * @date ：2021/10/27 10:43
 * @description：
 * @modified By：
 */
@Data
public class PopupNoticeDto {

	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	@ApiModelProperty(value = "菜单id  如 红包、礼包、消息、账户等")
	private Long menuId;

	@ApiModelProperty(value = "菜单编码")
	private String menuCode;

	@ApiModelProperty(value = "公告类型  1、普通公告 2、红包活动公告")
	private Integer sourceType;

	@ApiModelProperty(value = "如:红包活动公告所属类型 1：等级红包、2：充值红包、3：邀请红包")
	private Integer sourceBelongType;

	@ApiModelProperty(value = "来源id, 如：红包活动id")
	private Long sourceId;

	@ApiModelProperty(value = "通知弹窗事件")
	private String popupEvent;

	@ApiModelProperty(value = "通知类型, 1、活动开始弹窗 2、活动结束弹窗 3、消息 4、浮标")
	private Integer popupType;

	@ApiModelProperty(value = "通知弹窗内容,图片的话就是url")
	private String popupContent;

	@ApiModelProperty(value = "公告通知发布时间")
	private Date popupTime;

	@ApiModelProperty(value = "图片相对路劲")
	private String imgUrl;

	private MultipartFile file;

	@NotNull(message = "渠道范围集合不能为空")
	@ApiModelProperty(value = "渠道范围集合")
	private List<String[]> channelList;

	@NotNull(message = "游戏范围集合不能为空")
	@ApiModelProperty(value = "游戏范围集合")
	private List<String[]> gameList;

	@NotNull(message = "区服范围集合不能为空")
	@ApiModelProperty(value = "区服范围集合")
	private List<String[]> areaList;

	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	private String finishTime;

	@ApiModelProperty(value = "事件编码")
	private String eventCode;
}
