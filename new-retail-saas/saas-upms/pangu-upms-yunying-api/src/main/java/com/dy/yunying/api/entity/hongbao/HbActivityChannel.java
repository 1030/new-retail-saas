/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 活动渠道范围关系表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:19
 */
@Data
@TableName("hb_activity_channel")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "活动渠道范围关系表")
public class HbActivityChannel extends HbBaseEntity {

	/**
	 * 活动渠道关系ID
	 */
	@TableId(value = "id",type = IdType.AUTO)
	@ApiModelProperty(value = "活动渠道关系ID")
	private Long id;

	/**
	 * 活动类型：1-红包活动；3-外部链接活动；
	 */
	@ApiModelProperty(value = "活动类型")
	private Integer sourceType;

	/**
	 * 活动ID
	 */
	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	/**
	 * 主渠道ID
	 */
	@ApiModelProperty(value = "主渠道ID")
	private String channelCode;

	/**
	 * 子渠道ID
	 */
	@ApiModelProperty(value = "子渠道ID")
	private String childChannelCode;

	/**
	 * 分包渠道ID
	 */
	@ApiModelProperty(value = "分包渠道ID")
	private String subChannelCode;

	/**
	 * 渠道层级(1主渠道 2子渠道 3分包渠道)
	 */
	@ApiModelProperty(value = "渠道层级(1主渠道 2子渠道 3分包渠道)")
	private int channelRange;
}
