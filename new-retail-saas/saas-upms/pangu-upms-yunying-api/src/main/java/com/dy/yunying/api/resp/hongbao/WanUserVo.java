package com.dy.yunying.api.resp.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 玩家用户
 * @date 2021/11/5 14:03
 */
@Data
public class WanUserVo implements Serializable {

	@ApiModelProperty(value = "主键id")
	private Long userId;

	@ApiModelProperty(value = "用户名")
	private String userName;

}
