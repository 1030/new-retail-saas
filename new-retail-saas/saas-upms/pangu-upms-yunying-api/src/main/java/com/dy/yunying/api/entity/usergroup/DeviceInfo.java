package com.dy.yunying.api.entity.usergroup;


import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * 用户群组数据
 * @author zhuxm
 * @date 2022-04-27 17:50:21
 */
@ApiModel(value = "用户群组数据")
@TableName(value ="device_info")
@Data
public class DeviceInfo {

	/**
	 * 主键ID
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;


	/**
	 * 群组数据
	 */
	@TableField(value = "devicebrand")
	private String devicebrand;


	/**
	 * 群组数据
	 */
	@TableField(value = "devicemodel")
	private String devicemodel;

	/**
	 * 是否删除：0否 1是
	 */
	@TableField(value = "deleted")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建时间
	 */

	@TableField(value = "create_time", fill = FieldFill.INSERT)
	private Date createTime;


}
