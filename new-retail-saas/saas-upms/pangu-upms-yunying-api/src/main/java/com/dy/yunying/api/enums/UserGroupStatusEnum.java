package com.dy.yunying.api.enums;

/**
 * hezz
 * */
public enum UserGroupStatusEnum {

	// update_mark  '更新标识 0 不需要更新  1需要更新'
	UPDATE_MARK0(10, "更新标识--不需要更新"),
	UPDATE_MARK1(11, "更新标识--需要更新"),

	// update_mode  更新方式：1自动更新，2不更新
	UPDATE_MODE1(21,"更新方式--自动更新"),
	UPDATE_MODE2(22,"更新方式--不更新"),

	;

	private Integer type;
	private String name;
	public Integer getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private UserGroupStatusEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static UserGroupStatusEnum getOneByType(Integer type){
		for(UserGroupStatusEnum dimensionEnum: UserGroupStatusEnum.values()){
			if(dimensionEnum.getType() == type){
				return dimensionEnum;
			}
		}
		return null;
	}
}
