package com.dy.yunying.api.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 游戏截图
 * wan_game_imgs
 * @author kongyanfang
 * @date 2020-07-21 19:11:40
 */
@Getter
@Setter
public class WanGameImgsDO {
    /**
     */
    private Long imgid;

    /**
     * 游戏id
     */
    private Long gid;

    /**
     * 图片链接
     */
    private String imgurl;

    /**
     * 序号
     */
    private Integer idx;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 修改时间
     */
    private Date updatetime;

    /**
     * 图片类型1-游戏专享页背景图 2-游戏专享页游戏截图
     */
    private Byte imgtype;

    /**
     * 图片描述
     */
    private String imgalt;
}