package com.dy.yunying.api.vo.hongbao;

import com.dy.yunying.api.entity.hongbao.HbGiftBag;
import lombok.Data;

/**
 * 红包礼包码
 * @author  chenxiang
 * @version  2021-10-25 14:20:24
 * table: hb_gift_bag
 */
@Data
public class HbGiftBagVo  extends HbGiftBag {
	
	
	
}
