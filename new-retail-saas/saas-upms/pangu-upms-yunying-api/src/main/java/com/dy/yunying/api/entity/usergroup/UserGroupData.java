package com.dy.yunying.api.entity.usergroup;


import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * 用户群组数据
 * @author zhuxm
 * @date 2022-04-27 17:50:21
 */
@ApiModel(value = "用户群组数据")
@TableName(value ="user_group_data")
@Data
public class UserGroupData {

	/**
	 * 主键ID
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;

	/**
	 * 筛选方式：1规则筛选，2ID筛选
	 */
	@TableField(value = "type")
	private Integer type;


	/**
	 * 筛选维度：1账号，2角色
	 */
	@TableField(value = "dimension")
	private Integer dimension;

	/**
	 * 群组ID
	 */
	@TableField(value = "group_id")
	private Long groupId;


	/**
	 * 群组数据
	 */
	@TableField(value = "group_data")
	private String groupData;

	/**
	 * 创建时间
	 */

	@TableField(value = "create_time", fill = FieldFill.INSERT)
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time", fill = FieldFill.UPDATE)
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

	@TableField(exist = false)
	private String createName;


	/**
	 * 群组人数
	 */
	@TableField(exist = false)
	private Long groupUserNum;
}
