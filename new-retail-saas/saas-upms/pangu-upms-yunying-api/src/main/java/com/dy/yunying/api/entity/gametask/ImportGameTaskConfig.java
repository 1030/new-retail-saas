package com.dy.yunying.api.entity.gametask;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class ImportGameTaskConfig {
	/**
	 * 游戏app_id
	 */
	@ExcelProperty(index = 0)
	private String appId;

	/**
	 * 任务id
	 */
	@ExcelProperty(index = 1)
	private String taskId;

	/**
	 * 任务描述
	 */
	@ExcelProperty(index = 2)
	private String taskDescription;
}
