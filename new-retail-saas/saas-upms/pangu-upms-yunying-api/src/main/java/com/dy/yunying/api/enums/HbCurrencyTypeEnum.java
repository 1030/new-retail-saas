package com.dy.yunying.api.enums;

/**
 *  游豆种类
 */
public enum HbCurrencyTypeEnum {
	COMMON(1, "普通"),
	TEMP(2, "临时");

	private int type;
	private String name;
	public int getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private HbCurrencyTypeEnum(int type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(Integer type) {
		if (type == null){
			return null;
		}
		for (HbCurrencyTypeEnum ele : values()) {
			if(ele.getType() == type) {
				return ele.getName();
			}
		}
		return null;
	}


	public static HbCurrencyTypeEnum getOneByType(int type){
		for(HbCurrencyTypeEnum typeEnum : HbCurrencyTypeEnum.values()){
			if(typeEnum.getType() == type){
				return typeEnum;
			}
		}
		return null;
	}
}
