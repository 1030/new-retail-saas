package com.dy.yunying.api.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import java.math.BigDecimal;
import java.util.Objects;

@Setter
@Getter
public class AdOverviewSumVo {

    private String gameid;

    private String parentchl;

    private String chl;

    private String appchl;
    // 广告位名称
    private String adname;
    // 渠道名称
    private String chlname;
    // 投放人
    private String investor;
    // 游戏
    private String sgame;
    // 广告计划id
    private String adcreateid;
    // 父游戏
    private String pgame;
    // 渠道类型
    private String parentchlname;
    //日期
    private String day;
    //新增设备数
    private Integer uuidnums;
    //新增设备注册数
    private Integer usrnamenums;
    // 次留 汇总设备数
    private Integer uuidnums1;
    private Integer uuidnums2;
    private Integer uuidnums3;
    private Integer uuidnums4;
    private Integer uuidnums5;
    private Integer uuidnums6;
    //新增设备创角数
    private Integer rolenums;
    //总注册数
    private Integer usrnamesums;
    //总注册设备数
    private Integer uuidsums;
    //总创角数
    private Integer rolesums;
    //新增充值人数
    private Integer newuuidnums;
    // 新增充值金额
    private BigDecimal newuuidfees;
    // 充值人数
    private Integer payuuidnums;
    // 充值金额
    private BigDecimal payuuidfees;
    // 累计充值金额
    private BigDecimal uuidsumfees;
    // 新增设备1-7天累计充值
    private BigDecimal worth1;
    private BigDecimal worth2;
    private BigDecimal worth3;
    private BigDecimal worth4;
    private BigDecimal worth5;
    private BigDecimal worth6;
    private BigDecimal worth7;
    //  1-7 留存数
    private Integer num1;
    private Integer num2;
    private Integer num3;
    private Integer num4;
    private Integer num5;
    private Integer num6;
    private Integer num7;
    // 消耗（总成本） 1-7的广告计划消耗
    private BigDecimal cost;
    // 消耗（总成本） 1-7的广告计划消耗
    private BigDecimal rudecost;
    // 汇总消耗
    private BigDecimal cost1;
    private BigDecimal cost2;
    private BigDecimal cost3;
    private BigDecimal cost4;
    private BigDecimal cost5;
    private BigDecimal cost6;
    private BigDecimal cost7;
    // 下载IP
    private Integer downloadip;
    // 下载pv
    private Integer downloadpv;
    // 展示数
    private Integer shownums;
    // 点击数
    private Integer clicknums;

    // 点击率   点击数/展示数
    private BigDecimal clickratio;

    // 点击激活率   新增设备数/点击数
    private BigDecimal clickactiveratio;

    // 新增设备注册转化率  新增注册设备数/新增设备数
    private BigDecimal uuidchangeratio;

    // 新增设备创角率  新增设备创角数/新增设备注册数
    private BigDecimal rolechangeratio;

    // 总注册转化率  总注册数/总注册设备数
    private BigDecimal regchangeratio;

    // 总创角率  总创角数/总注册数
    private BigDecimal totalrolechangeratio;

    // 激活成本  消耗/新增设备数
    private BigDecimal activationcostratio;

    // 新增设备注册成本  消耗/新增设备注册数
    private BigDecimal uuidcostratio;

    // 总注册成本  消耗/总注册数
    private BigDecimal regcostratio;

    //新增付费成本 消耗/新增充值人数
    private BigDecimal paycostratio;

    //新增付费率  新增充值人数/新增设备数
    private BigDecimal payratio;

    //新增arppu  新增充值金额/新增充值人数
    private BigDecimal arppuratio;

    //注册arpu  新增充值金额/新增设备数
    private BigDecimal regarpu;

    //总roi 累计充值金额/总消耗
    private BigDecimal roiratio;

    //总LTV 累计充值金额/新增设备数
    private BigDecimal ltvratio;


    //次留
    private BigDecimal retention1;
    //3留
    private BigDecimal retention2;
    //4留
    private BigDecimal retention3;
    // 5留
    private BigDecimal retention4;
    // 6留
    private BigDecimal retention5;
    // 7留
    private BigDecimal retention6;

    // roi1
    private BigDecimal roi1;
    // roi2
    private BigDecimal roi2;
    // roi3
    private BigDecimal roi3;
    // roi4
    private BigDecimal roi4;
    // roi5
    private BigDecimal roi5;
    // roi6
    private BigDecimal roi6;
    // roi7
    private BigDecimal roi7;


    public String getDay() {
        if (StringUtils.isNotEmpty(day) && !"汇总".equals(day) && !"0".equals(day)) {
            return String.format("%s-%s-%s", day.substring(0, 4), day.substring(4, 6), day.substring(6, 8));
        }
        return day;
    }



    public BigDecimal getClickratio() {
        if (Objects.nonNull(shownums) && Objects.nonNull(clicknums) && 0 != shownums && 0 != clicknums) {
            BigDecimal ret = new BigDecimal(clicknums).divide(new BigDecimal(shownums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getClickactiveratio() {
        if (Objects.nonNull(clicknums) && Objects.nonNull(uuidnums) && 0 != clicknums && 0 != uuidnums) {
            BigDecimal ret = new BigDecimal(uuidnums).divide(new BigDecimal(clicknums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getUuidchangeratio() {
        if (Objects.nonNull(uuidnums) && Objects.nonNull(usrnamenums) && 0 != usrnamenums && 0 != uuidnums) {
            BigDecimal ret = new BigDecimal(usrnamenums).divide(new BigDecimal(uuidnums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRolechangeratio() {
        if (Objects.nonNull(rolenums) && Objects.nonNull(usrnamenums) && 0 != usrnamenums && 0 != rolenums) {
            BigDecimal ret = new BigDecimal(rolenums).divide(new BigDecimal(usrnamenums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRegchangeratio() {
        if (Objects.nonNull(uuidsums) && Objects.nonNull(usrnamesums) && 0 != usrnamesums && 0 != uuidsums) {
            BigDecimal ret = new BigDecimal(usrnamesums).divide(new BigDecimal(uuidsums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getTotalrolechangeratio() {
        if (Objects.nonNull(usrnamesums) && Objects.nonNull(rolesums) && 0 != usrnamesums && 0 != rolesums) {
            BigDecimal ret = new BigDecimal(rolesums).divide(new BigDecimal(usrnamesums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getActivationcostratio() {
        if (Objects.nonNull(cost) && Objects.nonNull(uuidnums) && BigDecimal.ZERO.compareTo(cost) == -1 && 0 != uuidnums) {
            BigDecimal ret = cost.divide(new BigDecimal(uuidnums), 2, BigDecimal.ROUND_HALF_UP);
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getUuidcostratio() {
        if (Objects.nonNull(cost) && Objects.nonNull(usrnamenums) && BigDecimal.ZERO.compareTo(cost) == -1 && 0 != usrnamenums) {
            BigDecimal ret = cost.divide(new BigDecimal(usrnamenums), 2, BigDecimal.ROUND_HALF_UP);
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRegcostratio() {
        if (Objects.nonNull(cost) && Objects.nonNull(usrnamesums) && BigDecimal.ZERO.compareTo(cost) == -1 && 0 != usrnamesums) {
            BigDecimal ret = cost.divide(new BigDecimal(usrnamesums), 2, BigDecimal.ROUND_HALF_UP);
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getPaycostratio() {
        if (Objects.nonNull(cost) && Objects.nonNull(newuuidnums) && BigDecimal.ZERO.compareTo(cost) == -1 && 0 != newuuidnums) {
            BigDecimal ret = cost.divide(new BigDecimal(newuuidnums), 2, BigDecimal.ROUND_HALF_UP);
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getPayratio() {
        if (Objects.nonNull(uuidnums) && Objects.nonNull(newuuidnums) && 0 != uuidnums && 0 != newuuidnums) {
            BigDecimal ret = new BigDecimal(newuuidnums).divide(new BigDecimal(uuidnums), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getArppuratio() {
        if (Objects.nonNull(newuuidnums) && Objects.nonNull(newuuidfees) && BigDecimal.ZERO.compareTo(newuuidfees) == -1 && 0 != newuuidnums) {
            BigDecimal ret = newuuidfees.divide(new BigDecimal(newuuidnums), 2, BigDecimal.ROUND_HALF_UP);
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRegarpu() {
        if (Objects.nonNull(uuidnums) && Objects.nonNull(newuuidfees) && BigDecimal.ZERO.compareTo(newuuidfees) == -1 && 0 != uuidnums) {
            BigDecimal ret = newuuidfees.divide(new BigDecimal(uuidnums), 2, BigDecimal.ROUND_HALF_UP);
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRoiratio() {
        if (Objects.nonNull(cost) && Objects.nonNull(uuidsumfees) && BigDecimal.ZERO.compareTo(uuidsumfees) == -1 && BigDecimal.ZERO.compareTo(cost) == -1) {
            BigDecimal ret = uuidsumfees.divide(cost, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getLtvratio() {
        if (Objects.nonNull(uuidnums) && Objects.nonNull(uuidsumfees) && BigDecimal.ZERO.compareTo(uuidsumfees) == -1 && 0 != uuidnums) {
            BigDecimal ret = uuidsumfees.divide(new BigDecimal(uuidnums), 2, BigDecimal.ROUND_HALF_UP);
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRetention1() {
        if (Objects.nonNull(uuidnums1) && Objects.nonNull(num1) && 0 != num1 && 0 != uuidnums1) {
            BigDecimal ret = new BigDecimal(num1).divide(new BigDecimal(uuidnums1), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRetention2() {
        if (Objects.nonNull(uuidnums2) && Objects.nonNull(num2) && 0 != num2 && 0 != uuidnums2) {
            BigDecimal ret = new BigDecimal(num2).divide(new BigDecimal(uuidnums2), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRetention3() {
        if (Objects.nonNull(uuidnums3) && Objects.nonNull(num3) && 0 != num3 && 0 != uuidnums3) {
            BigDecimal ret = new BigDecimal(num3).divide(new BigDecimal(uuidnums3), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRetention4() {
        if (Objects.nonNull(uuidnums4) && Objects.nonNull(num4) && 0 != num4 && 0 != uuidnums4) {
            BigDecimal ret = new BigDecimal(num4).divide(new BigDecimal(uuidnums4), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRetention5() {
        if (Objects.nonNull(uuidnums5) && Objects.nonNull(num5) && 0 != num5 && 0 != uuidnums5) {
            BigDecimal ret = new BigDecimal(num5).divide(new BigDecimal(uuidnums5), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRetention6() {
        if (Objects.nonNull(uuidnums6) && Objects.nonNull(num6) && 0 != num6 && 0 != uuidnums6) {
            BigDecimal ret = new BigDecimal(num6).divide(new BigDecimal(uuidnums6), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }


    public BigDecimal getRoi1() {
        if (Objects.nonNull(worth1) && Objects.nonNull(cost) && BigDecimal.ZERO.compareTo(worth1) == -1 && BigDecimal.ZERO.compareTo(cost) == -1) {
            BigDecimal ret = worth1.divide(cost, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRoi2() {
        if (Objects.nonNull(worth2) && Objects.nonNull(cost1) && BigDecimal.ZERO.compareTo(worth2) == -1 && BigDecimal.ZERO.compareTo(cost1) == -1) {
            BigDecimal ret = worth2.divide(cost1, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRoi3() {
        if (Objects.nonNull(worth3) && Objects.nonNull(cost2) && BigDecimal.ZERO.compareTo(worth3) == -1 && BigDecimal.ZERO.compareTo(cost2) == -1) {
            BigDecimal ret = worth3.divide(cost2, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRoi4() {
        if (Objects.nonNull(worth4) && Objects.nonNull(cost3) && BigDecimal.ZERO.compareTo(worth4) == -1 && BigDecimal.ZERO.compareTo(cost3) == -1) {
            BigDecimal ret = worth4.divide(cost3, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRoi5() {
        if (Objects.nonNull(worth5) && Objects.nonNull(cost4) && BigDecimal.ZERO.compareTo(worth5) == -1 && BigDecimal.ZERO.compareTo(cost4) == -1) {
            BigDecimal ret = worth5.divide(cost4, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRoi6() {
        if (Objects.nonNull(worth6) && Objects.nonNull(cost5) && BigDecimal.ZERO.compareTo(worth6) == -1 && BigDecimal.ZERO.compareTo(cost5) == -1) {
            BigDecimal ret = worth6.divide(cost5, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }

    public BigDecimal getRoi7() {
        if (Objects.nonNull(worth7) && Objects.nonNull(cost6) && BigDecimal.ZERO.compareTo(cost6) == -1 && BigDecimal.ZERO.compareTo(worth7) == -1) {
            BigDecimal ret = worth7.divide(cost6, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            return ret;
        }
        return BigDecimal.ZERO;
    }


}
