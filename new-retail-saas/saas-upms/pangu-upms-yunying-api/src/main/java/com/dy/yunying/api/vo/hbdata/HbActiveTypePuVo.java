package com.dy.yunying.api.vo.hbdata;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/18 15:54
 * @description：
 * @modified By：
 */
@Data
public class HbActiveTypePuVo implements Serializable {
	// 活动类型
	private Integer activityType;

	// 活动类型名称
	private String activityTypeName;

	// 活动名称
	private String activityName;

	// PV
	private Integer servicePV;

	// UV
	private Integer serviceUV;

	public String getActivityTypeName() {
		String activityTypeName = null;
		switch (activityType) {
			case 1:
				activityTypeName = "等级红包活动";
				break;
			case 2:
				activityTypeName = "充值红包活动";
				break;
			case 3:
				activityTypeName = "邀请红包活动";
				break;
			default:
				activityTypeName = "-";
		}
		return activityTypeName;
	}
}
