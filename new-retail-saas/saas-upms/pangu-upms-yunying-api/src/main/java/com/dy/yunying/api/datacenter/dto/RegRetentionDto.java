package com.dy.yunying.api.datacenter.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *
 * @author hma
 * @date 2022/8/18 13:49
 */
@Data
@ApiModel("注册留存率查询对象")
public class RegRetentionDto {

	@ApiModelProperty("查询开始时间")
	@NotNull(message = "查询开始日期不允许为空")
	private String startTime;

	@ApiModelProperty("查询结束时间")
	@NotNull(message = "查询结束日期不允许为空")
	private String endTime;

	@ApiModelProperty("父游戏")
	private String pgidArr;

	@ApiModelProperty("子游戏")
	private String gameidArr;

	@ApiModelProperty("主渠道列表")
	private String parentchlArr;

	@ApiModelProperty("子渠道编码列表")
	private String chlArr;

	@ApiModelProperty("分包编码列表")
	private String appchlArr;


	@ApiModelProperty("当前页")
	private Long current;

	@ApiModelProperty("每页的size")
	private Long size;

	@ApiModelProperty("排序  desc / asc")
	private String sort;

	@ApiModelProperty("排序字段")
	private String kpiValue;


}
