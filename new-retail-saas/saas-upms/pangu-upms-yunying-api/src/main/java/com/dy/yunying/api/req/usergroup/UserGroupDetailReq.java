package com.dy.yunying.api.req.usergroup;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class UserGroupDetailReq implements Serializable {


	private static final long serialVersionUID = 6296976379567355787L;


	@NotNull(message = "群组ID不能为空")
	@ApiModelProperty(value = "群组ID")
	private Long groupId;

	@ApiModelProperty(value = "账号")
	private String username;

	@ApiModelProperty(value = "分包渠道")
	private String chlApp;

	@ApiModelProperty(value = "子渠道")
	private String chlSub;

	@ApiModelProperty(value = "主渠道")
	private String chlMain;

	@ApiModelProperty(value = "筛选维度：1账号，2角色")
	private Integer dimension;

	@ApiModelProperty(value = "筛选方式：1规则筛选，2ID筛选")
	private Integer type;


	@ApiModelProperty(value = "子游戏ID")
	private Integer appCode;

	@ApiModelProperty(value = "主游戏ID")
	private Integer appMain;

	@ApiModelProperty(value = "区服ID")
	private String areaid;

	@ApiModelProperty(value = "角色ID")
	private String roleId;

	private Long current = 1L;
	private Long size = 10000L;


	//排序
	private String sort;
	//排序 字段值
	private String detailValue;

	/**
	 * 导出配置
	 */
	private String accountTitles = "账号,主渠道,子渠道,父游戏,子游戏,累计充值金额,注册时间,最后登录事件,最后充值时间,地区,手机号,设备型号";
	private String accountColumns = "username,parentchl,chl,parentGameName,gameName,payAmountTotal,accountRegTime,lastLoginTime,lastPayTime,area,mobile,device";


	private String roleTitles = "角色ID,角色名,主渠道,子渠道,父游戏,子游戏,区服,账号,累计充值金额,注册时间,创角时间,最后登录事件,最后充值时间,地区,手机号,设备型号";
	private String roleColumns = "roleId,roleName,parentchl,chl,parentGameName,gameName,areaid,username,payAmountTotal,accountRegTime,roleRegTime,lastLoginTime,lastPayTime,area,mobile,device";

}
