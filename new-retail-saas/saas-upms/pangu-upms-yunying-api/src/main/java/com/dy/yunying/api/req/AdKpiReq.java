package com.dy.yunying.api.req;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import java.util.List;

/**
 * @ClassName AdKpiReq
 * @Description
 * @Author nieml
 * @Time 2021/7/8 11:15
 * @Version 1.0
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class AdKpiReq extends Page {

	@ApiModelProperty("层级名称")
	private String hierarchyName;

	@ApiModelProperty("年月")
	private Integer yearMonth;

	@ApiModelProperty("客户端操作系统0=android；1=IOS；3=OTHERS或为空；4=H5; 5=PC")
	private Integer os;

	@ApiModelProperty("父游戏")
	private String pgidArr;

	@ApiModelProperty("部门id数组,多个用逗号分隔")
	private String deptIdArr;

	@ApiModelProperty("组别id数组,多个用逗号分隔")
	private String groupIdArr;

	@ApiModelProperty("投放人数组")
	private String  investorArr;


	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	@ApiModelProperty("是否系统管理员")
	private int isSys;

	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	public String getAdAccounts() {
		String adAccounts = "'NO'";  // 默认不查询任何广告账户
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			adAccounts = "'" + StringUtils.join(roleAdAccountList, "','") + "'";
		}
		return adAccounts;
	}

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

}
