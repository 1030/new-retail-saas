package com.dy.yunying.api.entity.usergroup;


import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

/**
 * 设备信息
 * @author zhuxm
 * @date 2022-05-17 17:50:21
 */
@ApiModel(value = "设备信息")
@TableName(value ="device")
@Data
public class Device implements  Comparable{

	/**
	 * 主键ID
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;


	/**
	 * 父ID
	 */
	@TableField(value="pid")
	private Long pid;


	/**
	 * 类型：1设备品牌 2设备机型
	 */
	@TableField(value = "level")
	private Integer level;


	/**
	 * 名称
	 */
	@TableField(value = "name")
	private String name;

	/**
	 * 是否删除：0否 1是
	 */
	@TableField(value = "deleted")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time", fill = FieldFill.INSERT)
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time", fill = FieldFill.UPDATE)
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;


	@TableField(exist = false)
	private Set<Device> children;

	@Override
	public int compareTo(Object o) {
		if (!(o instanceof Device))
			throw new RuntimeException("不是Device对象");
		Device p = (Device) o;

		return this.id.compareTo(p.id);
	}
}
