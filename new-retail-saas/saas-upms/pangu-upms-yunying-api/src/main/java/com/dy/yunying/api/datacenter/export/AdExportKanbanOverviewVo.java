package com.dy.yunying.api.datacenter.export;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.constant.OsEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Setter
@Getter
@ApiModel("广告数据分析")
public class AdExportKanbanOverviewVo  implements Serializable{

	private static final long serialVersionUID = 4701855235401117248L;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("父游戏id")
	@ApiModelProperty("父游戏id")
	private Long pgid;
	// 父游戏
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("主游戏")
	@ApiModelProperty("主游戏")
	private String pgame;

	// 子游戏id
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("子游戏id")
	@ApiModelProperty("子游戏id")
	private Long gid;
	// 游戏

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("子游戏")
	@ApiModelProperty("子游戏")
	private String sgame;

	// 分成比例
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("分成比例")
	@ApiModelProperty("分成比例")
	private BigDecimal sharing;


	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("消耗")
	@ApiModelProperty("消耗")
	private BigDecimal cost;
	// 消耗

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("返点后消耗（总成本）")
	@ApiModelProperty("返点后消耗（总成本）")
	private BigDecimal rudecost;

	//总roi 累计充值金额/总消耗

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("累计充值ROI")
	@ApiModelProperty("累计充值ROI ")
	private BigDecimal roiratio;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("累计充值金额")
	@ApiModelProperty("累计充值金额")
	private BigDecimal uuidsumfees;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("累计充值金额（分成后）")
	@ApiModelProperty("累计充值金额（分成后）")
	private BigDecimal dividefees;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("利润(分成后)")
	@ApiModelProperty("利润(分成后)")
	private BigDecimal profit;

	//新增设备数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("系统类型")
	@ApiModelProperty("系统类型")
	private Long os;

	//新增设备数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("系统名称")
	@ApiModelProperty("系统名称")
	private String osName;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("部门主键")
	@ApiModelProperty("部门主键")
	// 广告计划ID
	private Long deptId;

	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("部门")
	@ApiModelProperty("部门")
	private String deptName;

	public BigDecimal getRoiratio() {
		if (Objects.nonNull(uuidsumfees) && Objects.nonNull(rudecost) && 0 != rudecost.intValue()) {
			BigDecimal ret = uuidsumfees.divide(rudecost, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getSharing() {
		if (Objects.nonNull(sharing)) {
			BigDecimal ret = sharing.multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getProfit() {
		if (Objects.nonNull(dividefees) && Objects.nonNull(rudecost)) {
			BigDecimal ret = dividefees.subtract(rudecost);
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public String getOsName() {
		if (Objects.nonNull(os)) {
			return OsEnum.getName(os.intValue());
		}
		return "-";
	}

	public String getPgame() {
		if (StringUtils.isBlank(pgame)) {
			return "-";
		}
		return pgame;
	}

	public String getSgame() {
		if (StringUtils.isBlank(sgame)) {
			return "-";
		}
		return sgame;
	}

	public String getDeptName() {
		if (StringUtils.isBlank(deptName)) {
			return "-";
		}
		return deptName;
	}

}
