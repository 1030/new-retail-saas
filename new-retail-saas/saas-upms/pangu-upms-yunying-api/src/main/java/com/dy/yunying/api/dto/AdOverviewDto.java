package com.dy.yunying.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@ApiModel("广告报表查询对象")
public class AdOverviewDto  {

	@ApiModelProperty("是否系统管理员")
	private int isSys = 0;

	@ApiModelProperty("设备激活时间（开始）/广告统计查询时间")
	@NotNull(message = "查询开始日期不允许为空")
	private Long   rsTime;
	@ApiModelProperty("设备激活时间（结束）/广告统计查询时间")
	@NotNull(message = "查询结束日期不允许为空")
	private Long reTime;

	@ApiModelProperty("渠道主键id")
	private String channelId;

	@ApiModelProperty("渠道编码")
	private String chnCode;

	@ApiModelProperty("广告账号名称")
	private String adName;


	@ApiModelProperty("广告计划名称")
	private String adidName;

	@ApiModelProperty("部门id数组,多个用逗号分隔")
	private String deptIdArr;
	@ApiModelProperty("客户端操作系统0=android；1=IOS；3=OTHERS或为空；4=H5; 5=PC")
	private Integer os;


	@ApiModelProperty("转化/深化目标")
	private String  convert;

	@ApiModelProperty("投放人数组")
	private String  investorArr;
	//	@ApiModelProperty("游戏对象信息")
//	private List<WanGameDto> wanGameDtoList;
	@ApiModelProperty("子游戏")
	private String gIdArr;
	@ApiModelProperty("父游戏")
	private String pgIdArr;
	@ApiModelProperty("周期类型 ：1 按天 2 按周  3  按月 4 汇总（默认）")
	private Integer cycleType=4;

	@ApiModelProperty("类别 多个用逗号分隔【系统 os,主游戏 pgid，子游戏 gid ,部门 deptId,渠道编码 parentchl，分包编码：packCode,广告计划 adId,  投放人investor   】")
	private String  queryColumn;


	@ApiModelProperty("搜索名称")
	private String  searchName;

	@ApiModelProperty("是否显示分成前的数据 ：0 否 1：是")
	private Integer  showRatio= 0;

	@ApiModelProperty("广告账号数组")
	private String advertiserIdArr;

	public List<Long> deptIdList;
	private List<Integer>  investorList;
	private List<Long>  gIdList;
	private List<Long>  pgIdList;
	private List<String> advertiserIdList;


	private String titles;
	private String columns;

	private Long current;
	private Long size;


	public List<String> getAdvertiserIdList() {
		List<String> list = new ArrayList<>();
		return StringUtils.isNotEmpty(advertiserIdArr)? Arrays.asList(advertiserIdArr.split(",")): list;
	}

	public void setAdvertiserIdList(List<String> advertiserIdList) {
		this.advertiserIdList = advertiserIdList;
	}

	public List<Long> getDeptIdList(){
		List<Long> list = new ArrayList<>();
		return StringUtils.isNotEmpty(deptIdArr)? Arrays.stream( Arrays.stream(deptIdArr.split(",")).mapToLong(Long::parseLong).toArray()).boxed().collect(Collectors.toList()): list;
	}

	public List<Integer> getInvestorList(){
		List<Integer> list = new ArrayList<>();
		return StringUtils.isNotEmpty(investorArr)? Arrays.stream( Arrays.stream(investorArr.split(",")).mapToInt(Integer::parseInt).toArray()).boxed().collect(Collectors.toList()): list;
	}
	public List<Long> getGIdList(){
		List<Long> list = new ArrayList<>();
		return StringUtils.isNotEmpty(gIdArr)? Arrays.stream( Arrays.stream(gIdArr.split(",")).mapToLong(Long::parseLong).toArray()).boxed().collect(Collectors.toList()): list;
	}
	public List<Long> getPgIdList(){
		List<Long> list = new ArrayList<>();
		return StringUtils.isNotEmpty(pgIdArr)? Arrays.stream( Arrays.stream(pgIdArr.split(",")).mapToLong(Long::parseLong).toArray()).boxed().collect(Collectors.toList()): list;
	}


}
