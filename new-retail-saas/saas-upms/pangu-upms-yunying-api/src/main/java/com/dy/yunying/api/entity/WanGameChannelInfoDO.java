package com.dy.yunying.api.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * wan_game_channel_info
 *
 * @author hjl
 * @date 2020-07-24 09:44:28
 */
@Data
@Accessors(chain = true)
@TableName(value = "wan_game_channel_info")
public class WanGameChannelInfoDO {

	/**
	 *
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 游戏id
	 */
	private Integer gameid;

	/**
	 * 游戏包渠道编码（第三级）
	 */
	private String chl;

	/**
	 * 游戏包渠道编码名称（第三级）
	 */
	private String codeName;

	/**
	 * 渠道应用id
	 */
	private String appid;

	/**
	 * 渠道应用名称
	 */
	private String appname;

	/**
	 * 推广渠道编码（第二级）
	 */
	private String channel;

	/**
	 * 平台，0,3367平台，1头条，2百度,3 新数，4.公会
	 */
	private Integer platform;

	/**
	 * 是否上报0不上报1上报
	 */
	private Integer isup;

	/**
	 * 缓存状态0未缓存，1已经缓存
	 */
	private Integer cachestatus;

	/**
	 * 创建时间
	 */
	private Date createtime;

	/**
	 * 对应渠道包内容描述
	 */
	private String content;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 0 sdk支付，1苹果内置支付
	 */
	private Integer paytype;

	/**
	 * 广告主账户
	 */
	private String adaccount;

	/**
	 * 渠道计划数
	 */
	private Integer plannum;

	/**
	 * 渠道创意数
	 */
	private Integer ideanum;

	/**
	 * 1.api   2.sdk
	 */
	private Integer rtype;

	/**
	 * 推广类型（1信息流 2短视频）
	 */
	private Integer spreadType;

	/**
	 * 结算类型(KOL(一口价) KOL(CPM按次))
	 */
	private String settleType;

}