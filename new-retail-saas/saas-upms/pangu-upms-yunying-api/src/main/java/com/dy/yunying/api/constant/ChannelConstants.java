package com.dy.yunying.api.constant;

/**
 * 渠道常量配置
 */
public class ChannelConstants {

    /**
     * 渠道分包间隔符
     */
    public static final String CHANNEL_PACK_SEP = "x";

}
