package com.dy.yunying.api.req.prize;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 奖励推送表
 * @author  chenxiang
 * @version  2022-04-26 10:21:34
 * table: prize_push
 */
@Data
public class PrizePushReq extends Page {

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "用户群组ID(多个逗号分割)")
	private String userGroupId;

	@ApiModelProperty(value = "标题文本")
	private String title;

	@ApiModelProperty(value = "说明文本")
	private String description;

	@ApiModelProperty(value = "icon图")
	private String icon;

	@ApiModelProperty(value = "有效开始时间")
	private String startTime;

	@ApiModelProperty(value = "有效结束时间")
	private String endTime;

	@ApiModelProperty(value = "发放频率：1每天，2隔天，3每周，4仅一次")
	private String pushType;

	@ApiModelProperty(value = "发放频率(天：为间隔天数，周：逗号分割)")
	private String pushInterval;

	@ApiModelProperty(value = "推送节点：1首次登录成功，2每次登录成功，3首次进入游戏，4每次进入游戏")
	private String pushNode;

	@ApiModelProperty(value = "推送目标：1-账户，2-角色")
	private Integer pushTarget;

	@ApiModelProperty(value = "推送类型：1单个奖励，2组合奖励")
	private String pushPrizeType;

	@ApiModelProperty(value = "弹框形式：1单个奖励，2弹框，3图文，4通栏，5消息")
	private String popupMold;

	@ApiModelProperty(value = "打开类型：0不跳转，1SDK面板，2内置浏览器，3外部链接")
	private String openType;

	@ApiModelProperty(value = "菜单Id")
	private String menuId;

	@ApiModelProperty(value = "菜单名称")
	private String menuTitle;

	@ApiModelProperty(value = "菜单编码")
	private String menuCode;

	@ApiModelProperty(value = "ios 菜单编码地址")
	private String menuCodeIos;
	/**
	 * 二级菜单编码(安卓)
	 */
	@ApiModelProperty(value = "二级菜单编码(安卓)")
	private String secondMenuCode;
	/**
	 * 二级菜单编码(苹果)
	 */
	@ApiModelProperty(value = "二级菜单编码(苹果)")
	private String secondMenuCodeIos;

	@ApiModelProperty(value = "来源类型(1红包活动 2签到活动)")
	private String skipMold;

	@ApiModelProperty(value = "跳转位置 如：红包类型 1：等级红包、2：充值红包、3：邀请红包、4：定制红包")
	private String skipBelongType;

	@ApiModelProperty(value = "来源id, 如：红包活动id")
	private String skipId;

	@ApiModelProperty(value = "跳转地址")
	private String skipUrl;

	@ApiModelProperty(value = "跳转地址IOS")
	private String skipUrlIOS;

	@ApiModelProperty(value = "展示方式(1横屏展示，2竖屏展示)")
	private String showType;

	@ApiModelProperty(value = "展示顺序：默认为1")
	private String showSort;


	@ApiModelProperty(value = "状态：1正常，2停用")
	private String status;

	@ApiModelProperty(value = "奖品配置集合")
	private List<PrizeConfigReq> prizeConfigList;

	@NotNull(message = "渠道范围集合不能为空")
	@ApiModelProperty(value = "渠道范围集合")
	private List<String[]> channelList;

	@NotNull(message = "游戏范围集合不能为空")
	@ApiModelProperty(value = "游戏范围集合")
	private List<String[]> gameList;

	@NotNull(message = "区服范围集合不能为空")
	@ApiModelProperty(value = "区服范围集合")
	private List<String[]> areaList;

	@ApiModelProperty(value = "父游戏范围集合")
	private String parentGameArr;

	@ApiModelProperty(value = "子游戏范围集合")
	private String gameArr;

	@ApiModelProperty(value = "区服范围集合")
	private String areaArr;

	@ApiModelProperty(value = "排序方式(DESC:倒序 ASC：正序)")
	private String sortType;

}
