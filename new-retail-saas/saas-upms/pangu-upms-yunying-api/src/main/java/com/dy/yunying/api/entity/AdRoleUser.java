package com.dy.yunying.api.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色平台用户表
 * @author zhuxm
 *
 */
@Data
@TableName(value="ad_role_user")
public class AdRoleUser implements Serializable {

	private static final long serialVersionUID = -3195283480297995545L;

	/**
	 * 角色ID
	 */
	@ApiModelProperty(value = "角色id")
	private Integer roleId;

	/**
	 * 用户ID
	 */
	@ApiModelProperty(value = "用户id")
	private Integer userId;

	
}