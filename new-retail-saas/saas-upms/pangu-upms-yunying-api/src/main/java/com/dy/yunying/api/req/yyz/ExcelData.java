package com.dy.yunying.api.req.yyz;

import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName ExcelData.java
 * @createTime 2021年03月12日 14:32:00
 */
@Data
public class ExcelData {
    private static final long serialVersionUID = 4454016249210520899L;
    /**
     * 表头
     */
    private List<String> titles;
    /**
     * 数据
     */
    private List<List<Object>> rows;
    /**
     * 页签名称
     */
    private String name;
	/**
	 * 导出数据总页数
	 */
	private int totalItem;
	/**
	 *
	 */
	private boolean needQueryAll;
	/**
	 * 每页显示多少条数据
	 */
	private int pageSize;
	/**
	 * 设置第几页
	 */
	private int currentPage;


}
