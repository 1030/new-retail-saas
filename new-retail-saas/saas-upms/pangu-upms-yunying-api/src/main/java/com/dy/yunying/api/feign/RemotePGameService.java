/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.dy.yunying.api.feign;

import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.entity.ParentGameVersionDO;
import com.dy.yunying.api.feign.factory.RemotePGameServiceFallbackFactory;
import com.dy.yunying.api.req.ParentGameReq;
import com.dy.yunying.api.resp.ParentGameDataRes;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/22
 */
@FeignClient(contextId = "remotePGameService",value = ServiceNameConstants.UPMS_YUNYING_SERVICE,fallbackFactory = RemotePGameServiceFallbackFactory.class)
public interface RemotePGameService {

	/**
	 * 获取可缓存的主游戏列表
	 *
	 * @param req
	 * @return R
	 */
	@PostMapping("/parentgame/getCacheablePGameList")
	R<List<ParentGameDO>> getCacheablePGameList(@RequestBody ParentGameReq req);

	@PostMapping("/parentGame/version/add")
	R add(@RequestBody ParentGameVersionDO req);

}
