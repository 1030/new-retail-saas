package com.dy.yunying.api.cps;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author sunyq
 * @date 2022/10/11 17:15
 */
@Data
public class CpsUserAuthPackReq implements Serializable {
	/**
	 * cps用户id
	 */
	private Integer userId;
	/**
	 * 授权渠道
	 */
	private List<String> channelList;
}
