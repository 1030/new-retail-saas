package com.dy.yunying.api.datacenter.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/6/22 11:07
 * @description：
 * @modified By：
 */
@Data
public class AdRecoveryVo {

	//日期
	private String day;

	//周
	private String week;

	//月
	private String month;

	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	// 周期
	private String period;

	//主渠道
	private String parentchl;

	//分包渠道
	private String appchl;

	//部门id
	private String deptId;

	//部门id
	private String deptName;
	//组别
	private String userGroupId;
	private String userGroupName;

	@ApiModelProperty("父游戏id")
	private Long pgid;

	// 父游戏名称
	private String parentGameName;

	@ApiModelProperty("子游戏id")
	private Long gameid;

	// 子游戏名称
	private String gname;

	//系统类型
	private Integer os;

	//投放人 id
	private String investor;

	//投放人名称
	private String investorName;

	//主渠道名称
	private String parentchlName;

	//分包渠道名称
	private String appchlName;

	//系统类型名称
	private String osName;

	//广告计划
	private String adid;

	/**
	 * 广告计划名称
	 */
	private String adidName;

	/**
	 * 广告账户ID
	 */
	private String advertid;


	/**
	 * 广告计账户名称
	 */
	private String adAccountName;


	/**
	 * 转化类型
	 */
	private String converType;


	/**
	 * 转化类型名称
	 */
	private String converTypeName;



	//消耗
	private BigDecimal rudeCost;
	//返点后消耗
	private BigDecimal cost;
	//新增设备注册数
	private Integer usrnamenums;
	//新增设备成本
	private BigDecimal deviceCose;
	//累计充值ROI
	private BigDecimal roiRatio;
	//总LTV
	private BigDecimal ltvRatio;

	private BigDecimal roiRatioNum;
	private BigDecimal ltvRatioNum;


	public BigDecimal ltv1;
	public BigDecimal ltv2;
	public BigDecimal ltv3;
	public BigDecimal ltv4;
	public BigDecimal ltv5;
	public BigDecimal ltv6;
	public BigDecimal ltv7;
	public BigDecimal ltv8;
	public BigDecimal ltv9;
	public BigDecimal ltv10;
	public BigDecimal ltv11;
	public BigDecimal ltv12;
	public BigDecimal ltv13;
	public BigDecimal ltv14;
	public BigDecimal ltv15;
	public BigDecimal ltv16;
	public BigDecimal ltv17;
	public BigDecimal ltv18;
	public BigDecimal ltv19;
	public BigDecimal ltv20;
	public BigDecimal ltv21;
	public BigDecimal ltv22;
	public BigDecimal ltv23;
	public BigDecimal ltv24;
	public BigDecimal ltv25;
	public BigDecimal ltv26;
	public BigDecimal ltv27;
	public BigDecimal ltv28;
	public BigDecimal ltv29;
	public BigDecimal ltv30;
	public BigDecimal ltv45;
	public BigDecimal ltv60;
	public BigDecimal ltv75;
	public BigDecimal ltv90;
	public BigDecimal ltv120;
	public BigDecimal ltv150;
	public BigDecimal ltv180;

	public BigDecimal roi1;
	public BigDecimal roi2;
	public BigDecimal roi3;
	public BigDecimal roi4;
	public BigDecimal roi5;
	public BigDecimal roi6;
	public BigDecimal roi7;
	public BigDecimal roi8;
	public BigDecimal roi9;
	public BigDecimal roi10;
	public BigDecimal roi11;
	public BigDecimal roi12;
	public BigDecimal roi13;
	public BigDecimal roi14;
	public BigDecimal roi15;
	public BigDecimal roi16;
	public BigDecimal roi17;
	public BigDecimal roi18;
	public BigDecimal roi19;
	public BigDecimal roi20;
	public BigDecimal roi21;
	public BigDecimal roi22;
	public BigDecimal roi23;
	public BigDecimal roi24;
	public BigDecimal roi25;
	public BigDecimal roi26;
	public BigDecimal roi27;
	public BigDecimal roi28;
	public BigDecimal roi29;
	public BigDecimal roi30;
	public BigDecimal roi45;
	public BigDecimal roi60;
	public BigDecimal roi75;
	public BigDecimal roi90;
	public BigDecimal roi120;
	public BigDecimal roi150;
	public BigDecimal roi180;


	public BigDecimal ltvtimes1;
	public BigDecimal ltvtimes2;
	public BigDecimal ltvtimes3;
	public BigDecimal ltvtimes4;
	public BigDecimal ltvtimes5;
	public BigDecimal ltvtimes6;
	public BigDecimal ltvtimes7;
	public BigDecimal ltvtimes8;
	public BigDecimal ltvtimes9;
	public BigDecimal ltvtimes10;
	public BigDecimal ltvtimes11;
	public BigDecimal ltvtimes12;
	public BigDecimal ltvtimes13;
	public BigDecimal ltvtimes14;
	public BigDecimal ltvtimes15;
	public BigDecimal ltvtimes16;
	public BigDecimal ltvtimes17;
	public BigDecimal ltvtimes18;
	public BigDecimal ltvtimes19;
	public BigDecimal ltvtimes20;
	public BigDecimal ltvtimes21;
	public BigDecimal ltvtimes22;
	public BigDecimal ltvtimes23;
	public BigDecimal ltvtimes24;
	public BigDecimal ltvtimes25;
	public BigDecimal ltvtimes26;
	public BigDecimal ltvtimes27;
	public BigDecimal ltvtimes28;
	public BigDecimal ltvtimes29;
	public BigDecimal ltvtimes30;
	public BigDecimal ltvtimes45;
	public BigDecimal ltvtimes60;
	public BigDecimal ltvtimes75;
	public BigDecimal ltvtimes90;
	public BigDecimal ltvtimes120;
	public BigDecimal ltvtimes150;
	public BigDecimal ltvtimes180;

	public BigDecimal ltv1Num;
	public BigDecimal ltv2Num;
	public BigDecimal ltv3Num;
	public BigDecimal ltv4Num;
	public BigDecimal ltv5Num;
	public BigDecimal ltv6Num;
	public BigDecimal ltv7Num;
	public BigDecimal ltv8Num;
	public BigDecimal ltv9Num;
	public BigDecimal ltv10Num;
	public BigDecimal ltv11Num;
	public BigDecimal ltv12Num;
	public BigDecimal ltv13Num;
	public BigDecimal ltv14Num;
	public BigDecimal ltv15Num;
	public BigDecimal ltv16Num;
	public BigDecimal ltv17Num;
	public BigDecimal ltv18Num;
	public BigDecimal ltv19Num;
	public BigDecimal ltv20Num;
	public BigDecimal ltv21Num;
	public BigDecimal ltv22Num;
	public BigDecimal ltv23Num;
	public BigDecimal ltv24Num;
	public BigDecimal ltv25Num;
	public BigDecimal ltv26Num;
	public BigDecimal ltv27Num;
	public BigDecimal ltv28Num;
	public BigDecimal ltv29Num;
	public BigDecimal ltv30Num;
	public BigDecimal ltv45Num;
	public BigDecimal ltv60Num;
	public BigDecimal ltv75Num;
	public BigDecimal ltv90Num;
	public BigDecimal ltv120Num;
	public BigDecimal ltv150Num;
	public BigDecimal ltv180Num;

	public BigDecimal roi1Num;
	public BigDecimal roi2Num;
	public BigDecimal roi3Num;
	public BigDecimal roi4Num;
	public BigDecimal roi5Num;
	public BigDecimal roi6Num;
	public BigDecimal roi7Num;
	public BigDecimal roi8Num;
	public BigDecimal roi9Num;
	public BigDecimal roi10Num;
	public BigDecimal roi11Num;
	public BigDecimal roi12Num;
	public BigDecimal roi13Num;
	public BigDecimal roi14Num;
	public BigDecimal roi15Num;
	public BigDecimal roi16Num;
	public BigDecimal roi17Num;
	public BigDecimal roi18Num;
	public BigDecimal roi19Num;
	public BigDecimal roi20Num;
	public BigDecimal roi21Num;
	public BigDecimal roi22Num;
	public BigDecimal roi23Num;
	public BigDecimal roi24Num;
	public BigDecimal roi25Num;
	public BigDecimal roi26Num;
	public BigDecimal roi27Num;
	public BigDecimal roi28Num;
	public BigDecimal roi29Num;
	public BigDecimal roi30Num;
	public BigDecimal roi45Num;
	public BigDecimal roi60Num;
	public BigDecimal roi75Num;
	public BigDecimal roi90Num;
	public BigDecimal roi120Num;
	public BigDecimal roi150Num;
	public BigDecimal roi180Num;


	public BigDecimal ltvtimes1Num;
	public BigDecimal ltvtimes2Num;
	public BigDecimal ltvtimes3Num;
	public BigDecimal ltvtimes4Num;
	public BigDecimal ltvtimes5Num;
	public BigDecimal ltvtimes6Num;
	public BigDecimal ltvtimes7Num;
	public BigDecimal ltvtimes8Num;
	public BigDecimal ltvtimes9Num;
	public BigDecimal ltvtimes10Num;
	public BigDecimal ltvtimes11Num;
	public BigDecimal ltvtimes12Num;
	public BigDecimal ltvtimes13Num;
	public BigDecimal ltvtimes14Num;
	public BigDecimal ltvtimes15Num;
	public BigDecimal ltvtimes16Num;
	public BigDecimal ltvtimes17Num;
	public BigDecimal ltvtimes18Num;
	public BigDecimal ltvtimes19Num;
	public BigDecimal ltvtimes20Num;
	public BigDecimal ltvtimes21Num;
	public BigDecimal ltvtimes22Num;
	public BigDecimal ltvtimes23Num;
	public BigDecimal ltvtimes24Num;
	public BigDecimal ltvtimes25Num;
	public BigDecimal ltvtimes26Num;
	public BigDecimal ltvtimes27Num;
	public BigDecimal ltvtimes28Num;
	public BigDecimal ltvtimes29Num;
	public BigDecimal ltvtimes30Num;
	public BigDecimal ltvtimes45Num;
	public BigDecimal ltvtimes60Num;
	public BigDecimal ltvtimes75Num;
	public BigDecimal ltvtimes90Num;
	public BigDecimal ltvtimes120Num;
	public BigDecimal ltvtimes150Num;
	public BigDecimal ltvtimes180Num;

	public String getOsStr() {
		String osStr = OsEnum.getName(os);
		return osStr;
	}

	public String getPeriod() {
		if (StringUtils.isNotBlank(period)) {
			if (period.contains("周")) {
				String periodYear = period.substring(0, 4);
				String periodWeek = period.substring(4, 7);
				return periodYear + "-" + periodWeek;
			}
		}
		return period;
	}
}
