package com.dy.yunying.api.datacenter.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/6/17 15:12
 * @description：
 * @modified By：
 */
@Data
@Accessors(chain = true)
public class AdDataVo {

	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	private String period;

	@ApiModelProperty("父游戏id")
	private Long pgid;
	private String parentGameName;

	@ApiModelProperty("系统类型")
	private Integer os;

	public String getOsStr() {
		return OsEnum.getName(os);
	}

	@ExcelProperty("父渠道")
	@ApiModelProperty("父渠道")
	private String parentchl;

	//主渠道名称
	private String parentchlName;

	@ApiModelProperty("部门主键")
	private String deptId;

	@ExcelProperty("部门名称")
	@ApiModelProperty("部门名称")
	private String deptName;

	@ApiModelProperty("组别主键")
	private String userGroupId;

	@ExcelProperty("组别名称")
	@ApiModelProperty("组别名称")
	private String userGroupName;

	private String investor;

	@ExcelProperty("投放人姓名")
	@ApiModelProperty("投放人")
	private String investorName;

	@ExcelProperty("新增设备注册数")
	@ApiModelProperty("新增设备注册数")
	private Integer usrnamenums = 0;

	// 新增注册创角数
	private Integer createRoleCount;

	// 新增创角率
	private BigDecimal createRoleRate;

	// 新增注册实名数
	private Integer certifiedCount;

	// 注册未实名数
	private Integer notCertifiedCount;

	// 未成年人数
	private Integer youngCount;

	// 新增实名制转化率
	private BigDecimal certifiedRate;

	@ExcelProperty("新增设备付费数")
	@ApiModelProperty("新增设备付费数")
	private Integer newPayCount = 0;

	@ExcelProperty("新增充值实付金额")
	@ApiModelProperty("新增充实付值金额")
	private BigDecimal newPayAmount = new BigDecimal(0);

	@ExcelProperty("新增充值代金券金额")
	@ApiModelProperty("新增充代金券值金额")
	private BigDecimal newPayGivemoney = new BigDecimal(0);

	@ExcelProperty("当周充值实付金额")
	@ApiModelProperty("当周充值实付金额")
	private BigDecimal weektotalfee = new BigDecimal(0);

	@ExcelProperty("当周充值代金券金额")
	@ApiModelProperty("当周充值代金券金额")
	private BigDecimal weekPayGivemoney = new BigDecimal(0);

	@ExcelProperty("当月充值实付金额")
	@ApiModelProperty("当月充值实付金额")
	private BigDecimal monthtotalfee = new BigDecimal(0);

	@ExcelProperty("当月充值代金券金额")
	@ApiModelProperty("当月充值代金券金额")
	private BigDecimal monthPayGivemoney = new BigDecimal(0);

	@ExcelProperty("活跃设备数")
	@ApiModelProperty("活跃设备数")
	private Integer activedevices = 0;

	// 活跃付费设备数
	private Integer activePayCount;

	@ExcelProperty("活跃充值实付金额")
	@ApiModelProperty("活跃充值实付金额")
	private BigDecimal activetotalfee = new BigDecimal(0);

	@ExcelProperty("活跃充值代金券金额")
	@ApiModelProperty("活跃充值代金券金额")
	private BigDecimal activePayGivemoney = new BigDecimal(0);

	// 活跃付费率
	private BigDecimal activePayRate;

	// 活跃ARPU= 活跃充值金额/活跃设备数
	@ExcelProperty("活跃设备ARPU")
	@ApiModelProperty("活跃设备ARPU")
	private BigDecimal actarpu = new BigDecimal(0);

	// 活跃付费ARPPU
	private BigDecimal activePayArppu;

	private Integer num1 = 0; //设备次日留存数

	// 次留: 新增设备在次日有登录行为的设备数/新增设备数
	@ExcelProperty("次留")
	@ApiModelProperty("次留")
	@JsonProperty("retention2Ratio")
	private BigDecimal retention2Ratio = new BigDecimal(0);

	@ExcelProperty("消耗")
	@ApiModelProperty("消耗后消耗（总成本）")
	@JsonProperty("cost")
	private BigDecimal cost = new BigDecimal(0);

	// 返点后消耗（总成本）
	@ExcelProperty("返点前消耗")
	@ApiModelProperty("返点前消耗")
	private BigDecimal rudeCost = new BigDecimal(0);

	// 设备成本  消耗/新增设备注册数
	@ExcelProperty("设备成本")
	@ApiModelProperty("设备成本")
	private BigDecimal deviceCose = new BigDecimal(0);


	@ExcelProperty("期内付费成本")
	@ApiModelProperty("期内付费成本")
	private BigDecimal periodPayCose = new BigDecimal(0);


	// 重复设备数
	@ExcelProperty("重复设备数")
	private Integer duplicateDeviceCount;

	@ExcelProperty("设备重复率")
	@ApiModelProperty("设备重复率")
	private BigDecimal duplicateDeviceRatio = new BigDecimal(0);

	//注册arpu  新增充值金额/新增设备数
	@ExcelProperty("新增arpu")
	@ApiModelProperty("新增arpu")
	private BigDecimal regarpu = new BigDecimal(0);

	// 新增付费ARPPU
	private BigDecimal newPayArppu;

	//新增付费率  新增设备付费数/新增设备注册数
	@ExcelProperty("新增付费率")
	@ApiModelProperty("新增付费率")
	private BigDecimal regPayRatio = new BigDecimal(0);

	// roi1
	@ExcelProperty("首日ROI")
	@ApiModelProperty("首日ROI")
	private BigDecimal roi1 = new BigDecimal(0);

	@ExcelProperty("当周ROI")
	@ApiModelProperty("当周ROI ")
	private BigDecimal weekRoi = new BigDecimal(0);

	@ExcelProperty("当月ROI")
	@ApiModelProperty("当月ROI ")
	private BigDecimal monthRoi = new BigDecimal(0);

	// 累计充值金额
	@ExcelProperty("累计充值实付金额")
	@ApiModelProperty("累计充值实付金额")
	private BigDecimal totalPayfee = new BigDecimal(0);

	@ExcelProperty("累计充值代金券金额")
	@ApiModelProperty("累计充值代金券金额")
	private BigDecimal totalPayGivemoney = new BigDecimal(0);

	//总roi 累计充值金额/总消耗
	@ExcelProperty("累计充值ROI")
	@ApiModelProperty("累计充值ROI")
	private BigDecimal allRoi = new BigDecimal(0);

	// 期内付费设备数
	private Integer periodPayCount;

	// 期内充值实付金额
	private BigDecimal periodPayFee;

	// 期内充值代金券金额
	private BigDecimal periodPayGivemoney;

	// 期内付费率
	private BigDecimal periodPayRate;

}
