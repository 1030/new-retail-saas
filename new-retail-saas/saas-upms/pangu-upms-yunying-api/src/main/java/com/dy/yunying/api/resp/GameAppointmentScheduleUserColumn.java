package com.dy.yunying.api.resp;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 游戏预约进度档位表
 * @author  leisw
 * @version  2022-08-18 17:14:29
 * table: game_appointment_schedule_user_num
 */
@Data
public class GameAppointmentScheduleUserColumn{

	/**
	 * 标签来源/规则名称和预约人数的
	 */
	@TableField(value = "appoint_convention")
	private String appointConvention;

}






