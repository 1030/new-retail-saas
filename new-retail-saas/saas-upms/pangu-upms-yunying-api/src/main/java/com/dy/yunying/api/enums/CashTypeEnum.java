package com.dy.yunying.api.enums;

/**
 * @description: 提现类型
 * @author yuwenfeng
 * @date 2021/11/4 15:13
 */
public enum CashTypeEnum {

	GameMoney(1,"游戏币"),
	WeChat(2,"微信"),
	AliPay(3,"支付宝"),
	Cdk(4,"代金券"),
	BALANCE(5,"游豆");


	CashTypeEnum(Integer type, String desc){
		this.type =type;
		this.desc =desc;
	}

	private Integer type;

	private String desc;

	public Integer getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}
}
