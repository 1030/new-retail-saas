package com.dy.yunying.api.constant;

/**
 * 分包常量配置
 */
public class PackConstants {

    /**
     * 分包线程池并发度
     */
    public static final int CHANNEL_PACK_THREAD_FIXED_NUM = 1;

    /**
     * 渠道分包分批数量
     * 每？个一批
     */
    public static final int CHANNEL_PACK_BATCH_NUM = 10;

}
