package com.dy.yunying.api.enums;

public enum ChartTypeEnum {

	PIE ("饼图","pie"),
	LINE("折线图","line"),
	BAR("柱状图","bar"),
	;
	ChartTypeEnum(String name, String code) {
		this.name = name;
		this.code = code;
	}

	private String name;

	private String code;

	public static ChartTypeEnum getOneByCode(String code) {
		for (ChartTypeEnum codeEnum : ChartTypeEnum.values()) {
			if (codeEnum.getCode().equals(code)) {
				return codeEnum;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}
}
