package com.dy.yunying.api.req;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class AdRoleGameReq implements Serializable {

	private static final long serialVersionUID = -1010882849358386367L;
	@NotNull(message = "角色ID不能为空")
	private Integer roleId;

	@NotNull(message = "主游戏id不能为空")
	private Long pgid;

	private Long gameid;

	@NotNull(message = "类别不能为空")
	private Integer type;
}
