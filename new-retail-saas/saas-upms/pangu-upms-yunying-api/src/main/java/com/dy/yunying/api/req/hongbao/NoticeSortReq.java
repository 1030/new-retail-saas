package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @description: 公告排序
 * @author yuwenfeng
 * @date 2022/3/7 20:40
 */
@Data
public class NoticeSortReq implements Serializable {

	@ApiModelProperty(value = "ID")
	@NotNull(message = "ID不能为空")
	private Long id;

	@ApiModelProperty(value = "排序值")
	@NotNull(message = "排序值不能为空")
	@Min(1)
	@Max(999999)
	private Integer sort;

}
