package com.dy.yunying.api.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


@Data
@Accessors(chain = true)
public class FutureDaoVo {

	/**
	 * 第三方素材ID
	 */
	private String cid;

	private BigDecimal cost;

	private BigDecimal value;

}
