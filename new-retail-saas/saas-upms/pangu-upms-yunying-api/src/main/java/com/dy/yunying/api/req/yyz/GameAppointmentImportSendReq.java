package com.dy.yunying.api.req.yyz;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * 游戏预约表
 * @author  kongyanfang
 * @version  2022-01-17 17:14:29
 * table: game_appointment
 */
@Data
public class GameAppointmentImportSendReq implements Serializable {

	@ApiModelProperty(value = "上传文件流")
	private MultipartFile file;

	@ApiModelProperty(value = "发送类型,匹配数据库")
	private String code;

}
