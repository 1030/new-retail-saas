package com.dy.yunying.api.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author yuwenfeng
 * @description: 父游戏下拉信息
 * @date 2022/7/6 15:05
 */
@Data
@ApiModel(value = "父游戏下拉信息")
public class ParentGameDataRes {

	@ApiModelProperty(value = "父游戏ID")
	private Long pgId;

	@ApiModelProperty(value = "游戏名称")
	private String gname;

	@ApiModelProperty(value = "包名")
	private String pkName;

	@ApiModelProperty(value = "版本编码，数字，如56")
	private Integer code;

}
