package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 */
@Data
public class DynamicTypeReq implements Serializable {

	private Long id;

	@NotBlank(message = "名称不能为空")
	private String showName;

	@NotNull(message = "类型不能为空")
	private Integer activityType;

	private Integer sort;

	private Long create_id;

	private Long update_id;

}
