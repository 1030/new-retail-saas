package com.dy.yunying.api.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 游戏
 * wan_game
 * @author kongyanfang
 * @date 2020-07-22 15:21:57
 */
@Getter
@Setter
public class WanGameVO {
    /**
     * 游戏ID
     */
    private Long id;

    /**
     * 游戏名称
     */
    private String gname;

    /**
     * 游戏版本ID
     */
    private Long versionId;

    /**
     * 版本编码，数字，如56
     */
    private Integer versionCode;

    /**
     * 版本名称，字符串，如：v3.2.5
     */
    private String versionName;

    /**
     * 游戏类型，对应字典表里的游戏类型
     */
    private Short gtype;

    /**
     */
    private String gtypes;

    /**
     * 游戏题材
     */
    private Short gtheme;

    /**
     */
    private String gthemes;

    /**
     * 游戏战斗模式
     */
    private Short gpattern;

    /**
     * 首字母
     */
    private String firstletter;

    /**
     * 游戏特征
     */
    private String features;

    /**
     * 全部游戏(80*80)
     */
    private String icon;

    /**
     * 头部热门游戏图片
     */
    private String picture;

    /**
     * 首页推荐展示图片(218*250)
     */
    private String rcmdimg;

    /**
     * 首页顶部游戏(400*262) 作用待定
     */
    private String indextopimg;

    /**
     * 游戏大厅(顶部)(480*310)
     */
    private String halltopimg;

    /**
     * 栏目热门(95*68)
     */
    private String hotimg;

    /**
     * 大厅预览图 88*82
     */
    private String preimg;

    /**
     * 官方网站链接
     */
    private String officialurl;

    /**
     * 开始游戏链接
     */
    private String playurl;

    /**
     * flash小视频链接
     */
    private String flashurl;

    /**
     * 论坛地址
     */
    private String forumurl;

    /**
     * 礼包地址
     */
    private String bagurl;

    /**
     * 游戏汇率
     */
    private String rate;

    /**
     * 分成比例
     */
    private String ratio;

    /**
     * 充值积分兑换比例
     */
    private Integer integralrate;

    /**
     * 游戏渠道ID
     */
    private Long gcid;

    /**
     * 点赞次数
     */
    private Long likes;

    /**
     * 评分
     */
    private BigDecimal score;

    /**
     * 默认评论次数
     */
    private Long comments;

    /**
     * 实际评论次数
     */
    private Long realcomments;

    /**
     * 开区数量
     */
    private Long opennum;

    /**
     * 注册用户数
     */
    private Long registernum;

    /**
     * 当前在线人数
     */
    private Long onlinenum;

    /**
     * 充值金额
     */
    private BigDecimal recharge;

    /**
     * 状态  30：正常，40：停运
     */
    private Short status;

    /**
     * 是否热门 0:否  1:是
     */
    private Short ishot;

    /**
     * 是否推荐  0:否  1:是
     */
    private Short isrecommend;

    /**
     * 游戏币单位
     */
    private String unit;

    /**
     * 游戏简介
     */
    private String remark;

    /**
     * 是否删除0、否，1、是
     */
    private Short isdelete;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 更新时间
     */
    private Date updatetime;

    /**
     * 第三方游戏ID
     */
    private String mcode;

    /**
     * 推荐排序
     */
    private Integer rmdorder;

    /**
     * 热门排序
     */
    private Integer hotorder;

    /**
     * 全部游戏排序
     */
    private Integer allorder;

    /**
     * 游戏大厅顶部排序
     */
    private Integer gtoporder;

    /**
     * 顶部热门游戏排序
     */
    private Integer htoporder;

    /**
     * 热门推荐列表排序
     */
    private Integer lhrdorder;

    /**
     * 导量区服id
     */
    private Long guideaid;

    /**
     * 导量区服名称
     */
    private String guideaname;

    /**
     * 终端类型,0：pc网页端  1：h5   2:安卓手游 3:IOS手游
     */
    private Short terminaltype;

    /**
     * H5游戏推荐列表排序
     */
    private Integer h5order;

    /**
     * H5游戏二维码图片地址
     */
    private String qrcodeimg;

    /**
     * H5游戏列表排序
     */
    private Integer h5listorder;

    /**
     * 微信公众平台礼包置顶游戏设置 0-否，1-是
     */
    private Short giftbagIstop;

    /**
     * 是否在前端显示，用来游戏上线前测试，页面上不显示游戏，0 表示显示，1 表示不显示
     */
    private Short display;

    /**
     * 包名
     */
    private String pkName;

    /**
     * 包签名串
     */
    private String pkNameSign;

    /**
     * 大小
     */
    private Double size;

    /**
     * 下载地址
     */
    private String downloadurl;

    /**
     * h5下载地址
     */
    private String downloadurlH5;

    /**
     * sdk下载地址
     */
    private String downloadurlSdk;

    /**
     * 3367平台下载地址
     */
    private String downloadurl3367;

    /**
     * 公众号下载地址
     */
    private String downloadurlGzh;

    /**
     * 回调地址
     */
    private String exchangeUrl;

    /**
     * 英文名称
     */
    private String enname;

    /**
     * 游戏详情横幅图
     */
    private String bannerimg;

    /**
     */
    private String shareurl;

    /**
     * 1是，2否
     */
    private Integer isweb;

    /**
     * 1开，2关
     */
    private Integer isrecharge;

    /**
     */
    private String bundleid;

    /**
     */
    private String seoKeywords;

    /**
     */
    private String seoDescription;
}