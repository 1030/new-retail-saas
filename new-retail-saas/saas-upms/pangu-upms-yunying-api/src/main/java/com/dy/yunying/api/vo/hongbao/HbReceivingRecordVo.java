package com.dy.yunying.api.vo.hongbao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

import java.util.Date;

/**
 * 奖品领取记录
 * @author  chenxiang
 * @version  2021-10-25 14:21:04
 * table: hb_receiving_record
 */
@Data
public class HbReceivingRecordVo extends Page {
	/**
	 * 主键ID
	 */
	private String id;
	/**
	 * 活动ID
	 */
	private String activityId;
	/**
	 * 活动ID
	 */
	private String activityName;
	/**
	 * 类型：1红包，2提现
	 */
	private String objectType;
	/**
	 * 红包ID,提现档次id
	 */
	private String objectId;
	/**
	 * 领取类型：1红包，2礼包码，3实物礼品，4代金券
	 */
	private String type;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 用户名
	 */
	private String userName;
	/**
	 * 父游戏ID
	 */
	private String parentGameId;
	/**
	 * 子游戏ID
	 */
	private String gameId;
	/**
	 * 区服ID
	 */
	private String areaId;
	/**
	 * 角色ID
	 */
	private String roleId;
	/**
	 * 角色名称
	 */
	private String roleName;
	/**
	 * 礼包ID：如礼包主键，代金券主键
	 */
	private String giftId;
	/**
	 * 礼包码
	 */
	private String giftCode;
	/**
	 * 领取时间
	 */
	private String receiveTime;
	/**
	 * 状态：1已到账，2待发货，3已发货，4已收货
	 */
	private String status;
	/**
	 * 驳回原因
	 */
	private String forbidReason;
	/**
	 * 快递名称
	 */
	private String expressName;
	/**
	 * 快递订单号
	 */
	private String expressCode;
	/**
	 * 来源类型：1-红包活动；2-签到活动；3-外部链接；4-抽奖活动；
	 */
	private String sourceType;
	/**
	 * 领取时间开始端
	 */
	private String 	 receiveTimeStart;

	/**
	 * 领取时间结束端
	 */
	private String 	 receiveTimeEnd;


	/**
	 * 修改时间开始端
	 */
	private String updateTimeStart;

	/**
	 * 修改时间结束端
	 */
	private String updateTimeEnd;

}
