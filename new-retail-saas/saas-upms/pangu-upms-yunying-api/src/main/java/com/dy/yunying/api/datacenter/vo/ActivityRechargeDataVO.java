package com.dy.yunying.api.datacenter.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hma
 * @date 2022/7/25 15:14
 */
@Data
@ApiModel("每日运营数据")
public class ActivityRechargeDataVO implements Serializable {

	//todo  调整 需要动态支持导出数据，动态支持字段顺序调整
	/**
	 * 激活主渠道名称
	 */
	@ApiModelProperty("激活主渠道名称")
	@ExcelProperty("激活主渠道名称")
	private String parentChlName;


	@ApiModelProperty("激活子渠道名称")
	@ExcelProperty("激活子渠道名称")
	private String chlName;


	@ApiModelProperty("广告计划")
	@ExcelProperty("广告计划")
	private String adName;


	@ApiModelProperty("计划ID")
	@ExcelProperty("计划ID")
	private String adId;


	@ApiModelProperty("账号")
	@ExcelProperty("账号")
	private String userName;





	@ApiModelProperty("角色名")
	@ExcelProperty("角色名")
	private String roleName;



	@ApiModelProperty("角色ID")
	@ExcelProperty("角色ID")
	private String roleId;



	@ApiModelProperty("充值IP")
	@ExcelProperty("充值IP")
	private String ip;



	@ApiModelProperty("省份/地区")
	@ExcelProperty("省份/地区")
	private String ccName;



	@ApiModelProperty("订单号")
	@ExcelProperty("订单号")
	private String orderNo;


	@ApiModelProperty("交易号")
	@ExcelProperty("交易号")
	private String tradeNo;



	@ApiModelProperty("注册时间")
	@ExcelProperty("注册时间")
	private String regTime;




	@ApiModelProperty("充值时间")
	@ExcelProperty("充值时间")
	private String payTime;



	@ApiModelProperty("总金额")
	@ExcelProperty("总金额")
	private BigDecimal totalFee;



	@ApiModelProperty("现金")
	@ExcelProperty("现金")
	private BigDecimal fee;


	@ApiModelProperty("代金券")
	@ExcelProperty("代金券")
	private BigDecimal giveMoney;





	@ApiModelProperty("父游戏")
	@ExcelProperty("父游戏")
	private String parentGameName;



	@ApiModelProperty("子游戏")
	@ExcelProperty("父游戏")
	private String gameName;


	/**
	 * 区服
	 */
	@ExcelProperty("区服")
	@ApiModelProperty("区服")
	private String areaId;



	@ApiModelProperty("物品ID")
	@ExcelProperty("物品ID")
	private String productId;


	@ApiModelProperty("物品名称")
	@ExcelProperty("物品名称")
	private String productName;


	@ApiModelProperty("支付状态")
	@ExcelProperty("支付状态")
	private String statusName;


	@ApiModelProperty("充值方式")
	@ExcelProperty("充值方式")
	private String rgTypeName;

	@ApiModelProperty("UCID")
	@ExcelProperty("UCID")
	private String ucId;



	@ApiModelProperty("支付状态")
	private Integer status;





	@ApiModelProperty("充值方式")
	private Integer rType;








}
