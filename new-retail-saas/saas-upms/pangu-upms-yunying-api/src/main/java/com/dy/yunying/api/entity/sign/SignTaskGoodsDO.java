package com.dy.yunying.api.entity.sign;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务奖品关联信息表
 *
 * @TableName sign_task_goods
 */
@Data
@Accessors(chain = true)
@TableName(value = "sign_task_goods")
public class SignTaskGoodsDO implements Serializable {

	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 活动ID
	 */
	@TableField(value = "activity_id")
	private Long activityId;

	/**
	 * 活动任务ID
	 */
	@TableField(value = "task_id")
	private Long taskId;

	/**
	 * 物品ID
	 */
	@TableField(value = "goods_id")
	private Long goodsId;

	/**
	 * 物品数量
	 */
	@TableField(value = "goods_num")
	private Integer goodsNum;

	/**
	 * 是否删除：0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

	/*
	 * 非数据库字段
	 */

	/**
	 * 物品编码
	 */
	@TableField(exist = false)
	private String goodsCode;

	/**
	 * 物品名称
	 */
	@TableField(exist = false)
	private String goodsName;

	/**
	 * 物品图片地址
	 */
	@TableField(exist = false)
	private String goodsUrl;

}