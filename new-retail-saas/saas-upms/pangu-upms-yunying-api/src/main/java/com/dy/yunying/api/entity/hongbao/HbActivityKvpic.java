package com.dy.yunying.api.entity.hongbao;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * kv图库表(hb_activity_kvpic)实体类
 *
 * @author zjz
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("hb_activity_kvpic")
public class HbActivityKvpic extends Model<HbActivityKvpic> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value = "id",type = IdType.AUTO)
	private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * kv图片
     */
    private String kvpic;

	private Integer deleted;
    /**
     * createUser
     */
    private Long createUser;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * updateUser
     */
    private Long updateUser;
    /**
     * 更新时间
     */
    @TableField(update = "now()")
	private Date updateTime;

}