package com.dy.yunying.api.resp.hongbao;

import lombok.Getter;
import lombok.Setter;

/**
 * wan_game_version
 *
 * @author hjl
 * @date 2020-07-21 14:17:19
 */
@Getter
@Setter
public class FileUploadRes {

	private static final long serialVersionUID = 3604499146246680448L;

	/**
	 * 文件路径
	 */
	private String path;

	/**
	 * 文件路链接
	 */
	private String url;

}