package com.dy.yunying.api.constant;

/**
 * 分包类型
 * 1：渠道分包，2：主渠道默认包
 * 
 * @author hjl
 * @date 2020-5-7 20:00:47
 */
public enum ChannelPackTypeEnum {

	CHANNEL(1, "渠道分包"),
	DEFAULT(2, "主渠道默认包");

	private ChannelPackTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}
	public static String getName(Integer type) {
		for (ChannelPackTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return "未知";
	}

	private Integer type;

	private String name;

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
