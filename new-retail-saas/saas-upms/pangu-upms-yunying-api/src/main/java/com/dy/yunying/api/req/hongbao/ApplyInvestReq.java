package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: ID
 * @date 2021/10/23 11:04
 */
@Data
public class ApplyInvestReq implements Serializable {

	@ApiModelProperty(value = "ID")
	@NotNull(message = "ID不能为空")
	private Long id;

	@ApiModelProperty(value = "注资状态(2已到账 3不通过)")
	@NotNull(message = "ID不能为空")
	private Integer applyStatus;

	@ApiModelProperty(value = "审核备注")
	private String applyRemarke;
}
