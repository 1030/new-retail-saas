package com.dy.yunying.api.enums;

/**
 * 监控规则：监控指标最新的枚举值
 * @author hma
 */
public enum MonitorTargetTypeEnum
{
	/**
	 * 监控指标:   1.账号余额 2：0注册的消耗  3 注册成本 4 新增付费成本 5 首日roi 6:7日roi 7:15日roi
	 */
	ACCOUNT_BALANCE(1, "accountBalance", "账号余额"),
	USER_NAME_NUM_0(2, "cost", "0注册的消耗"),
	REG_COST(3, "regCost", "注册成本"),
	TOTAL_PAY_COST(4, " totalPayCost ", " 新增付费成本"),
	ROI1(5, "roi1", "首日ROI"),
	ROI7(6, "roi7", "7日ROI"),
	ROI15(7,"roi15", "15日ROI");

	/**
	 * 监控类型code
	 */
	private int monitorType;
	/**
	 * 监控类型字段
	 */
	private String monitorField;
	/**
	 * 监控指标名称
	 */
	private String name;


    MonitorTargetTypeEnum(int monitorType, String monitorField, String name) {
        this.monitorType = monitorType;
        this.monitorField = monitorField;
        this.name = name;
    }


	public static MonitorTargetTypeEnum getOneByType(Integer monitorType){
		for(MonitorTargetTypeEnum typeEnum : MonitorTargetTypeEnum.values()){
			if(typeEnum.getMonitorType() == monitorType){
				return typeEnum;
			}
		}
		return null;
	}


	public int getMonitorType() {
		return monitorType;
	}

	public String getMonitorField() {
		return monitorField;
	}

	public String getName() {
		return name;
	}
}
