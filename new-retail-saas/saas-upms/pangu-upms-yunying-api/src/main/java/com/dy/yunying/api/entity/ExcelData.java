package com.dy.yunying.api.entity;

import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName ExcelData.java
 * @createTime 2021年03月12日 14:32:00
 */
@Data
public class ExcelData {
    private static final long serialVersionUID = 4454016249210520899L;
    /**
     * 表头
     */
    private List<String> titles;
    /**
     * 数据
     */
    private List<List<Object>> rows;
    /**
     * 页签名称
     */
    private String name;
}
