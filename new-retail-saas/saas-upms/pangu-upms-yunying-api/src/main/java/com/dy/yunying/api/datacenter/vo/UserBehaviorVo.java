package com.dy.yunying.api.datacenter.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * @author ：leisw
 * @date ：2022/8/19 11:07
 * @description：
 * @modified By：
 */
@Data
public class UserBehaviorVo {

	//行为时间
	private String behaviorTime;

	//用户行为
	private String userBehavior;

	//用户行为名称
	private String userBehaviorName;

	//手机号
	private String mobile;

	//账号id
	private String username;

	//设备id
	private String uuid;

	//来源端口
	private String osname;

	//主渠道
	private String parentchl;

	//主渠道名称
	private String parentchlName;

	//子渠道
	private String chl;

	//分包渠道
	private String appchl;

	//分包渠道名称
	private String appchlName;

	//父游戏id
	private Long pgid;

	// 父游戏名称
	private String parentGameName;

	@ApiModelProperty("子游戏id")
	private Long gameid;

	// 子游戏名称
	private String gname;

	//区服id
	private String areaid;

	//区服名称
	private String serverName;

	//角色id
	private String roleid;

	//角色名称
	private String rolename;

	//等级
	private String rolelevel;

	//登录IP
	private String ip;

	//登录的地区
	private String area;

	//机型
	private String deviceBrand;

	//系统
	private String deviceModel;

}
