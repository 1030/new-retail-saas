package com.dy.yunying.api.datacenter.export;


import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @ClassName RetentionVo
 * @Description todo
 * @Author nieml
 * @Time 2021/6/21 16:09
 * @Version 1.0
 **/

@Data
public class ExportRetentionVo implements Serializable {

	private static final long serialVersionUID = 7701827235400187248L;

	/**
	 * 日期，如：2020/02/02
	 * 周，如：2020年20周
	 * 月，如：2020年12月
	 * 汇总，如：汇总
	 */
	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	private String period;

	@ExcelProperty("系统")
	@ApiModelProperty("系统")
	private String osStr;

	// 父游戏
	@ExcelProperty("父游戏")
	@ApiModelProperty("父游戏")
	private Long pgid;

	@ExcelProperty("主游戏")
	@ApiModelProperty("主游戏")
	private String parentGameName;

	@ExcelProperty("子游戏id")
	@ApiModelProperty("子游戏id")
	private Long gameid;

	// 子游戏名称
	@ExcelProperty("子游戏")
	@ApiModelProperty("子游戏")
	private String gname;

	// 部门
	@ExcelProperty("部门")
	@ApiModelProperty("部门")
	private String deptId;

	@ExcelProperty("部门")
	@ApiModelProperty("部门")
	private String deptName;

	// 组别
	@ExcelProperty("组别")
	@ApiModelProperty("组别")
	private String userGroupId;

	@ExcelProperty("组别")
	@ApiModelProperty("组别")
	private String userGroupName;

	@ExcelProperty("主渠道")
	@ApiModelProperty("主渠道")
	private String parentchl;

	@ExcelProperty("渠道名称")
	@ApiModelProperty("渠道名称")
	private String parentchlName;

	@ExcelProperty("分包编码")
	@ApiModelProperty("分包编码")
	private String appchl;

	// 操作系统
	@ExcelProperty("操作系统")
	@ApiModelProperty("操作系统")
	private Integer os;


	// 投放人
	@ExcelProperty("投放人")
	@ApiModelProperty("投放人")
	private String investor;

	@ExcelProperty("投放人名称")
	@ApiModelProperty("投放人名称")
	private String investorName;

	// 广告计划
	@ExcelProperty("广告计划")
	@ApiModelProperty("广告计划")
	private String adid;

	@ExcelProperty("消耗")
	@ApiModelProperty("消耗")
	private BigDecimal rudeCost;

	@ExcelProperty("返点后消耗")
	@ApiModelProperty("返点后消耗")
	private BigDecimal cost;

	@ExcelProperty("新增设备注册数")
	@ApiModelProperty("新增设备注册数")
	private Integer usrNameNums;

	// 计算公式：返点后消耗/新增设备注册数
	@ExcelProperty("新增设备成本")
	@ApiModelProperty("新增设备成本")
	private BigDecimal usrNameNumsCost;


	// 计算公式：新增设备中次日登录的设备数/新增设备注册数
	@ExcelProperty("次留")
	@ApiModelProperty("次留")
	private String retention2;

	@ExcelProperty("3留")
	@ApiModelProperty("3留")
	private String retention3;

	@ExcelProperty("4留")
	@ApiModelProperty("4留")
	private String retention4;

	@ExcelProperty("5留")
	@ApiModelProperty("5留")
	private String retention5;

	@ExcelProperty("6留")
	@ApiModelProperty("6留")
	private String retention6;

	@ExcelProperty("7留")
	@ApiModelProperty("7留")
	private String retention7;

	@ExcelProperty("8留")
	@ApiModelProperty("8留")
	private String retention8;

	@ExcelProperty("9留")
	@ApiModelProperty("9留")
	private String retention9;

	@ExcelProperty("10留")
	@ApiModelProperty("10留")
	private String retention10;

	@ExcelProperty("11留")
	@ApiModelProperty("11留")
	private String retention11;

	@ExcelProperty("12留")
	@ApiModelProperty("12留")
	private String retention12;

	@ExcelProperty("13留")
	@ApiModelProperty("13留")
	private String retention13;

	@ExcelProperty("14留")
	@ApiModelProperty("14留")
	private String retention14;

	@ExcelProperty("15留")
	@ApiModelProperty("15留")
	private String retention15;

	@ExcelProperty("16留")
	@ApiModelProperty("16留")
	private String retention16;

	@ExcelProperty("17留")
	@ApiModelProperty("17留")
	private String retention17;

	@ExcelProperty("18留")
	@ApiModelProperty("18留")
	private String retention18;

	@ExcelProperty("19留")
	@ApiModelProperty("19留")
	private String retention19;

	@ExcelProperty("20留")
	@ApiModelProperty("20留")
	private String retention20;

	@ExcelProperty("21留")
	@ApiModelProperty("21留")
	private String retention21;

	@ExcelProperty("22留")
	@ApiModelProperty("22留")
	private String retention22;

	@ExcelProperty("23留")
	@ApiModelProperty("23留")
	private String retention23;

	@ExcelProperty("24留")
	@ApiModelProperty("24留")
	private String retention24;

	@ExcelProperty("25留")
	@ApiModelProperty("25留")
	private String retention25;

	@ExcelProperty("26留")
	@ApiModelProperty("26留")
	private String retention26;

	@ExcelProperty("27留")
	@ApiModelProperty("27留")
	private String retention27;

	@ExcelProperty("28留")
	@ApiModelProperty("28留")
	private String retention28;

	@ExcelProperty("29留")
	@ApiModelProperty("29留")
	private String retention29;

	@ExcelProperty("30留")
	@ApiModelProperty("30留")
	private String retention30;

	private BigDecimal retention2Num;
	private BigDecimal retention3Num;
	private BigDecimal retention4Num;
	private BigDecimal retention5Num;
	private BigDecimal retention6Num;
	private BigDecimal retention7Num;
	private BigDecimal retention8Num;
	private BigDecimal retention9Num;
	private BigDecimal retention10Num;
	private BigDecimal retention11Num;
	private BigDecimal retention12Num;
	private BigDecimal retention13Num;
	private BigDecimal retention14Num;
	private BigDecimal retention15Num;
	private BigDecimal retention16Num;
	private BigDecimal retention17Num;
	private BigDecimal retention18Num;
	private BigDecimal retention19Num;
	private BigDecimal retention20Num;
	private BigDecimal retention21Num;
	private BigDecimal retention22Num;
	private BigDecimal retention23Num;
	private BigDecimal retention24Num;
	private BigDecimal retention25Num;
	private BigDecimal retention26Num;
	private BigDecimal retention27Num;
	private BigDecimal retention28Num;
	private BigDecimal retention29Num;
	private BigDecimal retention30Num;


	public String getOsStr() {
		String osStr = OsEnum.getName(os);
		return osStr;
	}



	public String getUserGroupName() {
		return StringUtils.isBlank(userGroupName) ? "-" : userGroupName;
	}

}
