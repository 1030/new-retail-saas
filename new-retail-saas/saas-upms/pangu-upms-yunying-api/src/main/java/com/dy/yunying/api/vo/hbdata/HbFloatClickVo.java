package com.dy.yunying.api.vo.hbdata;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/18 15:11
 * @description：
 * @modified By：
 */
@Data
public class HbFloatClickVo implements Serializable {

	//活动ID
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活动ID")
	private Long activityId;

	// 活动名称
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("活动名称")
	private String activityName;

	// 浮标内容
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("弹窗信息")
	private String floatContent;

	// 浮标点击数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("点击数")
	private Long clickNum;
}
