package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum KsConvertTargetEnum {
	ZERO("0", "无"),
	ONE("1", "表单提交"),
	TWO("2", "行为数"),
	THREE("3", "付费"),
	SEVEN("7", "次日留存"),
	EIGHT("8", "7日留存"),
	TEN("10", "完件"),
	ELEVEN("11", "授信"),
	THIRTEEN("13", "添加购物车"),
	FOURTEEN("14", "提交订单"),
	FIFTEEN("15", "购买"),
	FORTYFOUR("44", "有效线索"),
	NINETYTWO("92", "付费roi"),
	ONEEIGHZERO("180", "激活"),
	ONEEIGHTONE("181", "激活后24H次日留存"),
	ONENINETYERO("190", "付费"),
	ONENINETYONE("191", "首日ROI（24h）");


	//枚举值所包含的属性
	/**
	 * 名称
	 */
	private String type;


	private String name;

	//构造方法
	KsConvertTargetEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}


	/**
	 *
	 * @param type
	 * @return
	 */
	public static String getName(String type){
		if (StringUtils.isBlank(type)){
			return "";
		}

		for (KsConvertTargetEnum item : KsConvertTargetEnum.values()) {
			if (String.valueOf(item.getType()).equals(type)) {
				return item.getName();
			}
		}
		return "";

	}


}
