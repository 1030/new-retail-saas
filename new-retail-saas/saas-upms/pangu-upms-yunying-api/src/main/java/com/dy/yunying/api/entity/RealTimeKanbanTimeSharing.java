package com.dy.yunying.api.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author sunyq
 * @date 2022/8/16 17:45
 */
@Data
public class RealTimeKanbanTimeSharing implements Serializable {
	/**
	 * 今日
	 */
	private String today;
	/**
	 * 昨日
	 */
	private String yesterday;
	/**
	 * 上周当日
	 */
	private String sameDayLastWeek;
}
