package com.dy.yunying.api.enums;

/**
 * 监控规则：监控项：1广告计划  2 广告账号
 * @author hma
 */

public enum MonitorItemEnum {


	PLAN(1,"广告计划"),
	ACCOUNT(2,"广告账户"),
	;
	MonitorItemEnum(Integer type, String desc){
		this.type =type;
		this.desc =desc;
	}


	/**
	 * 通过type   取desc
	 * @return
	 */
	public static String descByType(Integer type){
		if (null == type){
			return null;
		}
		for (MonitorItemEnum item : MonitorItemEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getDesc();
			}
		}
		return null;

	}

	/**
	 * 类型
	 */
	private final Integer type;

	/**
	 * 描述
	 */
	private final String desc;

	public Integer getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}
}
