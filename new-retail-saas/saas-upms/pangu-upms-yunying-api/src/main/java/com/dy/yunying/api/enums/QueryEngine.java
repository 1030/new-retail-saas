package com.dy.yunying.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum QueryEngine {

	/**
	 * presto
	 */
	PRESTO("presto", "presto"),

	/**
	 * impala
	 */
	IMPALA("impala", "impala"),

	/**
	 * clickHouses
	 */
	CLICK_HOUSES("clickHouses", "clickHouses"),

	/**
	 * doris
	 * */
	DORIS("doris","doris");

	/**
	 * 类型
	 */
	private final String type;

	/**
	 * 描述
	 */
	private final String description;

}
