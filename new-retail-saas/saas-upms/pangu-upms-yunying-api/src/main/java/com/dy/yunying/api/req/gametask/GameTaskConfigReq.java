package com.dy.yunying.api.req.gametask;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class GameTaskConfigReq extends Page {

	@ApiModelProperty(value = "主键id")
	private Long id;

	@ApiModelProperty(value = "主游戏id")
	@NotNull(message = "主游戏不能为空")
	private Long parentGameId;

	@ApiModelProperty(value = "游戏app_id")
	@Size(min = 1,max = 32,message = "appId长度必须在1~32之间")
	private String appId;

	@ApiModelProperty(value = "任务id")
	@NotNull(message = "任务id不能为空")
	@Size(min = 1,max = 18,message = "taskId长度必须在1~18之间")
	private String taskId;

	@ApiModelProperty(value = "任务描述")
	@NotNull(message = "任务描述不能为空")
	@Size(min = 1,max = 50,message = "任务描述长度必须在1~50之间")
	private String taskDescription;


}
