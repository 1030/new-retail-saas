package com.dy.yunying.api.resp.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yuwenfeng
 * @description: CDK信息
 * @date 2021/11/5 14:03
 */
@Data
public class CdkVo implements Serializable {

	@ApiModelProperty(value="CDK码")
	private String cdkCode;

	@ApiModelProperty(value="CDK名称")
	private String cdkName;

	@ApiModelProperty(value="金额")
	private BigDecimal money;

	@ApiModelProperty(value="创建时间")
	private Date createTime;

	@ApiModelProperty(value="使用时间")
	private Date useTime;

	@ApiModelProperty(value="创建者名称")
	private String createUserName;

	@ApiModelProperty(value="有效期类型 1:长期有效 2:指定有效期")
	private Integer expiryType;

	@ApiModelProperty(value="有效开始时间")
	private Date startTime;

	@ApiModelProperty(value="有效结束时间")
	private Date endTime;

	@ApiModelProperty(value="备注")
	private String remark;

	@ApiModelProperty(value="使用状态：10、未使用；20、已使用")
	private Integer useStatus;

	@ApiModelProperty(value="金额限制类型 1:无门槛 2:指定金额")
	private Integer limitMoneyType;

	@ApiModelProperty(value="限制金额(使用时金额超过该金额才可以使用)")
	private BigDecimal limitMoney;

	@ApiModelProperty(value = "子游戏id")
	private Long gameid;

	@ApiModelProperty(value = "父游戏ID")
	private Long pgid;

	@ApiModelProperty(value="所属者id")
	private Long belongsId;

	@ApiModelProperty(value="所属者名称")
	private String belongsName;

	@ApiModelProperty(value="子游戏名称")
	private String gameName;

	@ApiModelProperty(value="父游戏名称")
	private String pGameName;

	@ApiModelProperty(value="发放人id")
	private Long giveId;

	@ApiModelProperty(value="发放者名称")
	private String giveName;

	@ApiModelProperty(value = "代金券限制类型 1:无门槛 2:指定游戏 3:指定角色")
	private Integer cdkLimitType;

	@ApiModelProperty(value = "来源ID")
	private Long sourceId;

	@ApiModelProperty(value = "来源类型")
	private Integer sourceType;

	@ApiModelProperty(value = "来源业务ID")
	private Integer sourceBusiness;

	@ApiModelProperty(value = "区服ID")
	private Long areaId;

	@ApiModelProperty(value = "角色ID")
	private String roleId;

	@ApiModelProperty(value = "角色名称")
	private String roleName;
}
