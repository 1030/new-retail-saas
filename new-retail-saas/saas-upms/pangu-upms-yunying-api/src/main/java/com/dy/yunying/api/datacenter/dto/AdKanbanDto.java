package com.dy.yunying.api.datacenter.dto;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

@Setter
@Getter
@ApiModel("广告报表查询对象")
public class AdKanbanDto {


	@ApiModelProperty("设备激活时间（开始）/广告统计查询时间")
	@NotNull(message = "查询开始日期不允许为空")
	private Long rsTime;
	@ApiModelProperty("设备激活时间（结束）/广告统计查询时间")
	@NotNull(message = "查询结束日期不允许为空")
	private Long reTime;

	@ApiModelProperty("部门id数组")
	private String deptIdArr;
	@ApiModelProperty("客户端操作系统")
	private String os;

	@ApiModelProperty("子游戏")
	private String gameIdArr;
	@ApiModelProperty("父游戏")
	private String pgIdArr;


	@ApiModelProperty("类别 多个用逗号分隔【系统 os,主游戏 pgid，子游戏 gid ,部门 deptId】")
	private String queryColumn;
	@ApiModelProperty("周期类型 ：1 按天 2 按周  3  按月 4 汇总（默认）")
	//查询汇总数据
	private Integer cycleType = 4;
	@ApiModelProperty("是否系统管理员")
	private int isSys = 0;
	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	public String getUserIds() {
		// 默认不查询任何投放账号
		String userIds = "-1";
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	public String getAdAccounts() {
		// 默认不查询任何广告账户
		String adAccounts = "'NO'";
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			adAccounts = "'" + StringUtils.join(roleAdAccountList, "','") + "'";
		}
		return adAccounts;
	}

	private String titles;
	private String columns;

	private String querySql;
	private String groupSql;

	private Long size;
	private Long current;

	public String getPeriod() {
		String period = "collect";
		switch (cycleType) {
			case 1:
				period = "day";
				break; //可选
			case 2:
				period = "week";
				//语句
				break; //可选
			case 3:
				period = "month";
				break; //可选
			case 4:
				period = "collect";
				break; //可选
			//你可以有任意数量的case语句
			default: //可选
				//语句
		}
		return period;
	}

}
