package com.dy.yunying.api.req.sign;

import lombok.Data;

/**
 * @author chenxiang
 * @className SignActivityDataReq
 * @date 2021/12/2 20:50
 */
@Data
public class SignActivityDataReq {
	/**
	 * 搜索
	 */
	private String searchName;
	/**
	 * 活动ID
	 */
	private String activityId;
	/**
	 * 名称
	 */
	private String activityName;
	/**
	 * 开始时间
	 */
	private String startTime;
	/**
	 * 结束时间
	 */
	private String endTime;
}
