package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @ClassName AdmonitorRule
 * @Description 广告监控规则表
 * @Author yangyh
 * @Time 2021/6/18 11:39
 * @Version 1.0
 **/
@Data
@TableName("ad_monitor_rule")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "广告监控规则表")
public class AdmonitorRule extends Model<AdmonitorRule> {
	/**
	 * 主键
	 */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value="主键")
	private Integer id;

	/**
	 * 规则名称
	 */
	@NotNull(message = "规则名称不能为空")
	@ApiModelProperty(value="规则名称")
	private String monitorName;

	/**
	 * 监控游戏ID
	 */
	@ApiModelProperty(value="监控游戏ID")
	private Long monitorGameId;

	/**
	 * 监控渠道平台类型
	 */
	@ApiModelProperty(value="监控渠道平台类型")
	private Integer monitorChannelType;

	/**
	 * 监控操作类型
	 */
	@NotNull(message = "监控操作类型不能为空")
	@ApiModelProperty(value="监控操作类型")
	private String monitorOperatorType;


	/**
	 * 通知人员
	 */
	@NotNull(message = "通知人员不能为空")
	@ApiModelProperty(value="通知人员")
	private String notifytor;

	/**
	 * 监控范围类型
	 */
	@ApiModelProperty(value="监控范围类型")
	private Integer scopeType;

	/**
	 * 广告账户
	 */
	@ApiModelProperty(value="广告账户")
	private String adAccount;

	/**
	 * 广告计划
	 */
	@ApiModelProperty(value="广告计划")
	private String adPlan;

	/**
	 * 监控状态：0关闭，1开启
	 */
	@ApiModelProperty(value="监控状态：0关闭，1开启")
	private Integer monitorStatus;

	/**
	 * 删除状态：0正常，1删除
	 */
	@ApiModelProperty(value="删除状态：0正常，1删除")
	private Integer delFlag;
	/**
	 * 创建人
	 */
	@ApiModelProperty(value="创建人")
	private Integer createBy;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value="创建时间")
	private Date createTime;
	/**
	 * 修改人
	 */
	@ApiModelProperty(value="修改人")
	private Integer updateBy;
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value="修改时间")
	private Date updateTime;


	/**
	 * 监控情形列表
	 */
	@TableField(exist = false)
	private List<AdmonitorSituation> admonitorSituations;

	/**
	 * 创建者姓名
	 */
	@TableField(exist = false)
	private String createByName;

	/**
	 * 监控游戏
	 */
	@TableField(exist = false)
	@ApiModelProperty(value="监控游戏")
	private String monitorGame;

	/**
	 * 监控渠道平台类型
	 */
	@TableField(exist = false)
	@ApiModelProperty(value="监控渠道")
	private String monitorChannelTypeName;

	/**
	 * 监控项
	 */
	@TableField(exist = false)
	@ApiModelProperty(value="监控项")
	private String monitorAditem;
}
