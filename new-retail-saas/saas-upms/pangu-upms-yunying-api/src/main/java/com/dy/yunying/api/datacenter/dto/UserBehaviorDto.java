package com.dy.yunying.api.datacenter.dto;

import cn.hutool.core.collection.CollectionUtil;
import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName UserBehaviorDto
 * @Description
 * @Author leisw
 * @Time 2021/8/19 14:24
 * @Version 1.0
 **/

@Data
public class UserBehaviorDto{

	@ApiModelProperty("查询开始时间")
	private String startTime;

	@ApiModelProperty("查询结束时间")
	private String endTime;

	@ApiModelProperty("父游戏")
	private String pgidArr;

	@ApiModelProperty("子游戏")
	private String gameidArr;

	@ApiModelProperty("主渠道列表")
	private String parentchlArr;

	@ApiModelProperty("子渠道编码列表")
	private String chlArr;

	@ApiModelProperty("分包编码列表")
	private String appchlArr;

	@ApiModelProperty("用户行为")
	private String userBehavior;

	@ApiModelProperty("登录端口")
	private String os;

	@ApiModelProperty("手机号")
	private String mobile;

	@ApiModelProperty("账号id")
	private String accountId;

	@ApiModelProperty("角色id")
	private String playerId;

	@ApiModelProperty("设备id")
	private String uuid;

	@ApiModelProperty("当前页")
	private Long current;

	@ApiModelProperty("每页的size")
	private Long size;

	@ApiModelProperty("排序  desc / asc")
	private String sort;

	@ApiModelProperty("排序字段")
	private String kpiValue;

	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	@ApiModelProperty("是否系统管理员")
	private int isSys;

}
