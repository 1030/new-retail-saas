package com.dy.yunying.api.enums;

public enum MonitorOperatorTypeEnum {

	EMAIL(1,"邮件通知"),
	;
	MonitorOperatorTypeEnum(Integer type, String desc){
		this.type =type;
		this.desc =desc;
	}

	/**
	 * 类型
	 */
	private final Integer type;

	/**
	 * 描述
	 */
	private final String desc;
}
