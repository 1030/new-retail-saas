package com.dy.yunying.api.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class HbActivityCenterVO extends Page<Object> implements Serializable {

	/**
	 * 活动中心活动ID
	 */
	@NotNull(message = "活动中心活动ID不能为空", groups = {Edit.class})
	private Long id;

	/**
	 * 用户群组ID(多个逗号分割)
	 */
	private String userGroupId;

	/**
	 * 活动来源：1-红包活动；2-签到活动；3-外部链接；4-抽奖活动
	 */
	@Range(min = 1, max = 4, message = "活动来源无效", groups = {Save.class, Edit.class})
	private Integer sourceType;

	/**
	 * 活动类型：1-等级活动；2-充值活动；3-邀请活动；4-定制活动；，sourceType=1时必传
	 */
	//@Range(min = 1, max = 4, message = "活动类型只能为1、2、3、4", groups = {Save.class, Edit.class})
	private Integer activityType;

	/**
	 * 打开链接方式：1-刷新当前页面；2-APP打开全屏新窗口；3-APP外部打开链接；
	 */
	private Integer openType;
	/**
	 * 活动动态类型ID
	 */
	//@NotNull(message = "活动动态类型ID不能为空", groups = {Save.class, Edit.class})
	private Long dynamicTypeId;

	/**
	 * 活动ID
	 */
	private Long activityId;

	/**
	 * 活动名称
	 */
	private String activityName;

	/**
	 * 活动顺序
	 */
	@NotNull(message = "活动顺序不能为空", groups = {Save.class, Edit.class})
	@Range(min = 1, max = 999999, message = "活动顺序值范围必须在 1 ~ 999999 之间", groups = {Save.class, Edit.class})
	private Integer activityOrder;

	/**
	 * 推广图片
	 */
	@NotBlank(message = "推广图片不能为空", groups = {Save.class})
	private String promoteImageUri;
	/**
	 * 打开地址
	 */
	private String openUrl;

	/**
	 * 开始时间
	 */
	private String startTime;

	/**
	 * 结束时间
	 */
	private String finishTime;

	@ApiModelProperty(value = "游戏范围集合")
	private List<String[]> gameDataList;

	@ApiModelProperty(value = "区服范围集合")
	private List<String[]> areaDataList;

	@ApiModelProperty(value = "渠道范围集合")
	private List<String[]> channelDataList;

	/**
	 * 排序字段
	 */
	private String orderByName;

	/**
	 * 是否升序：1-升序；0-降序；
	 */
	private Integer orderByAsc;

	public interface Save {
	}

	public interface Edit {
	}

}
