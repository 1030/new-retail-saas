package com.dy.yunying.api.req.raffle;
import io.swagger.annotations.ApiModelProperty;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 领取记录信息表
 * @author  chenxiang
 * @version  2022-11-08 16:01:24
 * table: raffle_receive_record
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RaffleReceiveRecordReq extends Page {

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "活动ID")
	private String activityId;

	@ApiModelProperty(value = "领取类型：1-抽奖中奖记录；2-积分兑换记录；")
	private String objectType;

	@ApiModelProperty(value = "奖品配置ID")
	private String objectId;

	@ApiModelProperty(value = "奖品名称")
	private String objectName;

	@ApiModelProperty(value = "用户ID")
	private String userId;

	@ApiModelProperty(value = "用户账号")
	private String username;

	@ApiModelProperty(value = "主游戏ID")
	private String parentGameId;

	@ApiModelProperty(value = "子游戏ID")
	private String subGameId;

	@ApiModelProperty(value = "区服ID")
	private String areaId;

	@ApiModelProperty(value = "角色ID")
	private String roleId;

	@ApiModelProperty(value = "角色名称")
	private String roleName;

	@ApiModelProperty(value = "主渠道")
	private String mainChl;

	@ApiModelProperty(value = "子渠道")
	private String subChl;

	@ApiModelProperty(value = "分包渠道")
	private String pkgCode;

	@ApiModelProperty(value = "奖品类型：1-礼包码；2-代金券；3-游豆；4-实物礼品；")
	private String giftType;

	@ApiModelProperty(value = "奖品发放数量")
	private String giftNum;

	@ApiModelProperty(value = "外部奖品ID")
	private String giftId;

	@ApiModelProperty(value = "礼包码编码/代金券编码(逗号分隔)")
	private String giftCode;

	@ApiModelProperty(value = "代金券名称/实物礼品名称")
	private String giftName;

	@ApiModelProperty(value = "代金券金额/折扣券比例/游豆数量")
	private String giftAmount;

	@ApiModelProperty(value = "代金券使用限制金额")
	private String giftUseLimit;

	@ApiModelProperty(value = "奖品使用范围：1-不限；2-游戏；3-角色；")
	private String giftUseScope;

	@ApiModelProperty(value = "奖品过期类型：1-永久有效；2-临时奖品；")
	private String giftExpireType;

	@ApiModelProperty(value = "奖品固定生效时间")
	private String giftStartTime;

	@ApiModelProperty(value = "奖品固定过期时间")
	private String giftEndTime;

	@ApiModelProperty(value = "领取时间")
	private String receiveTime;

	@ApiModelProperty(value = "领取记录状态：1-未填写地址；2-待审核；3-审核驳回；4-审核通过/已发货；")
	private String recordStatus;

	@ApiModelProperty(value = "驳回原因")
	private String forbidReason;

	@ApiModelProperty(value = "收货人姓名")
	private String receiptUsername;

	@ApiModelProperty(value = "收货人电话")
	private String receiptPhone;

	@ApiModelProperty(value = "receipt_province_code")
	private String receiptProvinceCode;

	@ApiModelProperty(value = "receipt_province_name")
	private String receiptProvinceName;

	@ApiModelProperty(value = "receipt_city_code")
	private String receiptCityCode;

	@ApiModelProperty(value = "receipt_city_name")
	private String receiptCityName;

	@ApiModelProperty(value = "receipt_area_code")
	private String receiptAreaCode;

	@ApiModelProperty(value = "receipt_area_name")
	private String receiptAreaName;

	@ApiModelProperty(value = "receipt_address")
	private String receiptAddress;

	@ApiModelProperty(value = "发货时间")
	private String deliverTime;

	@ApiModelProperty(value = "快递名称")
	private String expressName;

	@ApiModelProperty(value = "快递订单号")
	private String expressCode;

	/**
	 * 扩展字段
	 */
	@ApiModelProperty(value = "活动名称")
	private String activityName;

	@ApiModelProperty(value = "领取开始时间")
	private String receiveStartTime;

	@ApiModelProperty(value = "领取结束时间")
	private String receiveEndTime;

	@ApiModelProperty(value = "修改开始时间")
	private String updateStartTime;

	@ApiModelProperty(value = "修改结束时间")
	private String updateEndTime;

}
