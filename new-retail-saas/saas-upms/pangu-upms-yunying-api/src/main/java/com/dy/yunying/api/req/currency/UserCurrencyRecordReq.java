package com.dy.yunying.api.req.currency;

import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName UserCurrencyRecordReq
 * @Description
 * @Author hejiale
 * @Time 2022-3-25 09:42:58
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class UserCurrencyRecordReq extends Page<Object> implements Serializable {

	/**
	 * 开始时间
	 */
	private Date startTime;

	/**
	 * 结束时间
	 */
	private Date endTime;

	/**
	 * 父游戏ID
	 */
	private String pgids;

	/**
	 * 子游戏ID
	 */
	private String gameIds;

	/**
	 * 区服ID
	 */
	private String areaIds;

	/**
	 * 用户ID
	 */
	private Long userid;

	/**
	 * 用户账号
	 */
	private String username;

	/**
	 * 角色ID或角色名称
	 */
	private String roleName;

	/**
	 * 游豆类型。1：普通；2：临时
	 * CurrencyTypeEnum
	 */
	private Integer currencyType;

	/**
	 * 类型。1：获得；2：消耗
	 * CurrencyChangeTypeEnum
	 */
	private Integer type;

	/**
	 * CurrencySourceTypeEnum
	 */
	private Integer sourceType;

	/**
	 * IP
	 */
	private String ip;

	/**
	 * 来源业务ID
	 */
	private String sourceId;

	/**
	 * 来源业务名称
	 */
	private String sourceName;

	/**
	 * 来源业务ID1
	 */
	private String sourceId1;

	/**
	 * 来源业务名称1
	 */
	private String sourceName1;

	/**
	 * 来源业务ID2
	 */
	private String sourceId2;

	/**
	 * 来源业务名称2
	 */
	private String sourceName2;

	/**
	 * 状态
	 */
	private Integer status;

	/**
	 * 导出配置
	 */
	private String titles = "账号,类型,游豆,更新时间,登录IP,来源,来源标识,标识ID,标识名称,主游戏,子游戏,区服,角色名称,角色ID";
	private String columns = "username,typeStr,amount,updateTime,ip,sourceTypeStr,sourceId1,sourceId2,sourceNameStr,parentGameName,gameName,areaId,roleName,roleId";

}
