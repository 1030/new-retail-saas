package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yuwenfeng
 * @description: 公共实体类
 * @date 2021/10/23 10:35
 */
@Data
public class HbBaseEntity implements Serializable {

	/**
	 * 是否删除(0否 1是)
	 */
	@ApiModelProperty(value = "是否删除(0否 1是)")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建者
	 */
	@TableField(fill = FieldFill.INSERT)
	private Long createId;

	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 更新者
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updateId;

	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;
}
