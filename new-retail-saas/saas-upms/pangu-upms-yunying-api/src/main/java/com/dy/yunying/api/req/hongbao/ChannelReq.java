package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 新增/编辑对象
 * @date 2021/10/23 11:04
 */
@Data
public class ChannelReq implements Serializable {

	@NotBlank(message = "主渠道ID不能为空")
	@ApiModelProperty(value = "主渠道ID")
	private String channelCode;

	@ApiModelProperty(value = "子渠道ID")
	private String childChannelCode;

	@ApiModelProperty(value = "分包渠道ID")
	private String subChannelCode;
}
