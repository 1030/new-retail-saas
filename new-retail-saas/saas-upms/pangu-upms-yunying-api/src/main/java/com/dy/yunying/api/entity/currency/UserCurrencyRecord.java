package com.dy.yunying.api.entity.currency;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 账号平台币明细
 * @TableName user_currency_record
 *
 * @Author: hjl
 * @Date: 2022-3-24 15:52:14
 */
@ApiModel(value = "账号平台币明细")
@TableName(value ="user_currency_record")
@Data
public class UserCurrencyRecord implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "userid")
    private Long userid;

    /**
     * 用户账号
     */
    @TableField(value = "username")
    private String username;

    /**
     * 游豆类型。1：普通；2：临时
     */
    @TableField(value = "currency_type")
    private Integer currencyType;

    /**
     * 类型。1：获得；2：消耗
     */
    @TableField(value = "type")
    private Integer type;

    /**
     * 来源。11：游豆充值；21：游戏订单付费
     */
    @TableField(value = "source_type")
    private Integer sourceType;

    /**
     * 来源业务ID1
     */
    @TableField(value = "source_id1")
    private String sourceId1;

    /**
     * 来源业务名称1
     */
    @TableField(value = "source_name1")
    private String sourceName1;

    /**
     * 来源业务ID2
     */
    @TableField(value = "source_id2")
    private String sourceId2;

    /**
     * 来源业务名称2
     */
    @TableField(value = "source_name2")
    private String sourceName2;

    /**
     * 父游戏ID
     */
    @TableField(value = "pgid")
    private Long pgid;

    /**
     * 子游戏ID
     */
    @TableField(value = "game_id")
    private Long gameId;

    /**
     * 区服ID
     */
    @TableField(value = "area_id")
    private String areaId;

    /**
     * 角色ID
     */
    @TableField(value = "role_id")
    private String roleId;

    /**
     * 金额
     */
    @TableField(value = "amount")
    private BigDecimal amount;

    /**
     * 变动前游豆
     */
    @TableField(value = "before_balance")
    private BigDecimal beforeBalance;

    /**
     * 变动后游豆
     */
    @TableField(value = "after_balance")
    private BigDecimal afterBalance;

    /**
     * 详情，JSON字符串
     */
    @TableField(value = "detail")
    private String detail;

    /**
     * 删除标识，1：删除；0：未删除
     */
    @TableField(value = "deleted")
    private Integer deleted;

    /**
     * IP
     */
    @TableField(value = "ip")
    private String ip;

    /**
     * 创建人
     */
    @TableField(value = "create_id")
    private Long createId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "update_id")
    private Long updateId;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}