package com.dy.yunying.api.vo.usergroup;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class UserGroupDataVo implements Serializable {

	private static final long serialVersionUID = 8790975819318182338L;

	//主键ID
	private Long id;

	/**
	 * 群组名称
	 */
	@NotNull(message = "群组ID不能为空")
	private Long groupId;


	@NotBlank(message = "角色/账号不能为空")
	@Length(min = 1, max = 50, message = "角色/账号长度不能超过50位")
	private String groupData;

}
