package com.dy.yunying.api.req;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class AdRoleUserReq implements Serializable {
	private static final long serialVersionUID = 4307477112098541594L;

	@NotNull(message = "角色ID不能为空")
	private Integer roleId;

//	@NotBlank(message = "用户ID不能为空")
	private String userIds;
}
