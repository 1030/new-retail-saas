package com.dy.yunying.api.datacenter.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * dogame注册留存率数据
 * @author hma
 * @date 2022/8/19 15:14
 */
@Data
@ApiModel("dogame注册留存率数据")
public class RegRetentionVO implements Serializable {
	/**
	 * 日期
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("日期")
	private String day;
	/**
	 * 新增账号
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("注册数")
	private Integer accountNums;

	/**
	 * 次留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("次留")
	private String retentionRate;


	/**
	 * 3留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("3留")
	private String retentionRate3;

	/**
	 * 4留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("4留")
	private String retentionRate4;

	/**
	 * 5留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("5留")
	private String retentionRate5;
	/**
	 * 6留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("6留")
	private String retentionRate6;
	/**
	 * 7留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("7留")
	private String retentionRate7;
	/**
	 * 8留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("8留")
	private String retentionRate8;
	/**
	 * 9留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("9留")
	private String retentionRate9;
	/**
	 * 10留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("10留")
	private String retentionRate10;
	/**
	 * 11留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("11留")
	private String retentionRate11;
	/**
	 * 12留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("12留")
	private String retentionRate12;
	/**
	 * 13留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("13留")
	private String retentionRate13;
	/**
	 * 14留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("14留")
	private String retentionRate14;

	/**
	 * 30留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("30留")
	private String retentionRate30;

	/**
	 * 60留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("60留")
	private String retentionRate60;

	/**
	 * 90留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("90留")
	private String retentionRate90;

	/**
	 * 120留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("120留")
	private String retentionRate120;


	/**
	 * 150留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("150留")
	private String retentionRate150;

	/**
	 * 180留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("180留")
	private String retentionRate180;



	/**
	 * 360留
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("360留")
	private String retentionRate360;


}
