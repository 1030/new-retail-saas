package com.dy.yunying.api.req.znfx;

import lombok.Data;

import java.io.Serializable;

@Data
public class AdAccountDownReq implements Serializable {

	private static final long serialVersionUID = -1736341849811934428L;

	private String isAll;

	private String mediaCode;
}

