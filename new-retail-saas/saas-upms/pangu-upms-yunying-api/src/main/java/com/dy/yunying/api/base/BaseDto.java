package com.dy.yunying.api.base;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

@Setter
@Getter
public abstract class BaseDto implements Serializable, Cloneable {

    /**
     * (non-Javadoc)
     * 
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

    /**
     * (non-Javadoc)
     *
     * @see Object#clone()
     */
    @Override
    public Object clone() {
        return SerializationUtils.clone(this);
    }

    /**
     * Object to JSON String
     * 
     * @param object
     * @return
     */
    public static String toString(Object object) {
        return JSON.toJSONString(object);
    }

    /**
     * JSON String to Object
     * 
     * @param json
     * @param classOfT
     * @return
     */
    public static <T> T fromJson(String json, Class<T> classOfT) {
        return JSON.parseObject(json, classOfT);
    }

    /**
     * JSON String to Object
     * 
     * @param json
     * @param typeOfT
     * @return
     */
    public static <T> T fromJson(String json, Type typeOfT) {
        return JSON.parseObject(json, typeOfT);
    }

    /**
     * JSON String to JSONArray
     *
     * @param json
     * @param classOfT
     * @return
     */
    public static <T> List<T> fromJsonArray(String json, Class<T> classOfT) {
        return JSONArray.parseArray(json, classOfT);
    }
}
