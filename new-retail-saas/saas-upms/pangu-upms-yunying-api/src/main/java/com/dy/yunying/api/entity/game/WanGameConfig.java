package com.dy.yunying.api.entity.game;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 子游戏
 * @TableName wan_game_config
 */
@TableName(value ="wan_game_config")
@Data
public class WanGameConfig implements Serializable {
    /**
     * 子游戏ID
     */
    @TableId(value = "game_id")
    private Long gameId;

    /**
     * 热云开关。1：开启；0：关闭
     */
    @TableField(value = "reyun_switch")
    private Integer reyunSwitch;

    /**
     * 热云APPKEY
     */
    @TableField(value = "reyun_app_key")
    private String reyunAppKey;

    /**
     * 热云appsecret
     */
    @TableField(value = "reyun_app_secret")
    private String reyunAppSecret;

    /**
     * 创建人
     */
    @TableField(value = "create_id")
    private Long createId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "update_id")
    private Long updateId;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}