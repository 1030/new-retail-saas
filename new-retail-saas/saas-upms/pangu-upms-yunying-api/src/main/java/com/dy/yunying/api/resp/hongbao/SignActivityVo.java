package com.dy.yunying.api.resp.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 签到活动列表
 * @date 2021/11/30 20:00
 */

@Data
public class SignActivityVo implements Serializable {

	@ApiModelProperty(value = "签到活动ID")
	private Long id;

	@ApiModelProperty(value = "签到活动名称")
	private String activityName;

	@ApiModelProperty(value = "父游戏ID")
	private Long parentGameId;

	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	private String finishTime;

	@ApiModelProperty(value = "签到天数")
	private Integer signDays;

	@ApiModelProperty(value = "活动状态(1待上线 2活动中 3已下线)")
	private Integer activityStatus;

	@ApiModelProperty(value = "活动规则")
	private String activityRule;

	@ApiModelProperty(value = "活动范围")
	private String rangeName;

	@ApiModelProperty(value = "区服数据集合")
	private Long[] areaDataList;

	@ApiModelProperty(value = "结束日期")
	private long finishDate;

	@ApiModelProperty(value = "签到活动公告地址")
	private String noticeSignActivityUrl;

	@ApiModelProperty(value = "签到活动微信地址")
	private String wechatSignActivityUrl;
}
