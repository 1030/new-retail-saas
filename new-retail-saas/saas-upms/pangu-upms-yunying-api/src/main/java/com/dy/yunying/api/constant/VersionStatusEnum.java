package com.dy.yunying.api.constant;

/**
 * 游戏版本状态
 * 0：待生成  1：已生成（有效）  2:删除
 * 
 * @author hjl
 * @date 2020-5-7 20:00:47
 */
public enum VersionStatusEnum {

	INIT(0, "待生成"),
	VALID(1, "有效"),
	DISABLED(2, "失效"),
	DELETE(3, "删除");

	private VersionStatusEnum(Integer status, String name) {
		this.status = status;
		this.name = name;
	}
	public static String getName(Integer status) {
		for (VersionStatusEnum ele : values()) {
			if(ele.getStatus().equals(status)) {
				return ele.getName();
			}
		}
		return "未知";
	}

	private Integer status;

	private String name;

	public Integer getStatus() {
		return status;
	}

	public String getName() {
		return name;
	}
}
