package com.dy.yunying.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 父游戏
 * parent_game
 *
 * @author kongyanfang
 * @date 2021-01-05 14:22:57
 */
@Data
@TableName(value = "parent_game")
public class ParentGameDO {
	/**
	 *
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 游戏名称
	 */
	private String gname;

	/**
	 * 游戏币汇率
	 */
	private BigDecimal exchangeRate;

	/**
	 * 游戏分成，0到100
	 */
	private BigDecimal sharing;

	/**
	 * 子游戏数量
	 */
	private Long sgnum;

	/**
	 * 玩家人数
	 */
	private Long playernum;

	/**
	 * 充值金额
	 */
	private BigDecimal recharge;

	/**
	 * 游戏厂商
	 */
	private String gmanufacturer;

	/**
	 * 登录地址
	 */
	private String loginaddress;

	/**
	 * 充值回调
	 */
	private String rechargecallback;

	/**
	 * 上架时间
	 */
	private Date launchtime;

	/**
	 * 包名
	 */
	private String pkName;

	/**
	 * 请求接口秘钥
	 */
	private String queryKey;

	/**
	 * 兑换接口密钥
	 */
	private String exchangeKey;

	/**
	 * 对接、联调游戏ID
	 */
	private Long jointGameId;

	/**
	 * 导出接口密钥
	 */
	private String reportKey;

	/**
	 * 兑换任务类
	 */
	private String exchangeClass;

	/**
	 * 登录接口地址
	 */
	private String loginUrl;

	/**
	 * 兑换接口地址
	 */
	private String exchangeUrl;

	/**
	 * 兑换结果查询接口地址
	 */
	private String exchangeQueryUrl;

	/**
	 * 游戏等级查询url 地址
	 */
	private String roleQueryUrl;


	/**
	 * 登录图地址
	 */
	private String loginMapUrl;

	/**
	 * loging图地址
	 */
	private String logingMapUrl;

	/**
	 * 状态，1：正常
	 */
	private Integer status;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * 游戏LOGO地址
	 */
	private String gameLogo;
}