package com.dy.yunying.api.enums;

public enum GroupClassEnum {

	ALL_GAME(1, "全部游戏"),
	PART_GAME(2, "部分游戏"),
	APPOINT_GAME (3, "指定游戏");

	private Integer type;
	private String name;
	public Integer getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private GroupClassEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static GroupClassEnum getOneByType(Integer type){
		for(GroupClassEnum classEnum: GroupClassEnum.values()){
			if(classEnum.getType() == type){
				return classEnum;
			}
		}
		return null;
	}
}
