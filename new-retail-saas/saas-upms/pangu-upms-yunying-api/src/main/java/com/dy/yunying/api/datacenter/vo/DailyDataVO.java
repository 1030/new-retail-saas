package com.dy.yunying.api.datacenter.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author sunyq
 * @date 2022/6/21 15:14
 */
@Data
@ApiModel("每日运营数据")
public class DailyDataVO implements Serializable {
	/**
	 * 日期
	 */
	private String day;
	/**
	 * 新增设备
	 */
	private Integer uuidNums;
	/**
	 * 新增账号
	 */
	private Integer accountNums;
	/**
	 * 注册转换率
	 */
	private String regRate;
	/**
	 * 次留
	 */
	private String retentionRate;
	/**
	 * 活跃账号
	 */
	private Integer activeAccounts;

	/**
	 * 充值金额
	 */
	private BigDecimal activePayAmounts;
	/**
	 * 充值人数
	 */
	private Integer activeFeeAccounts;
	/**
	 * 付费率
	 */
	private String payFeeRate;
	/**
	 *
	 */
	private BigDecimal arpu;
	/**
	 *
	 */
	private BigDecimal arppu;

	/**
	 * 新账号充值金额
	 */
	private BigDecimal newPayAmounts;
	/**
	 * 新账号充值人数
	 */
	private Integer newPayNums;
	/**
	 * 新账号付费率
	 */
	private String newPayFeeRate;
	/**
	 * 新账号ARPU
	 */
	private BigDecimal newArpu;

	/**
	 * 新账号ARPPU
	 */
	private BigDecimal newArppu;
	/**
	 * 老账号数
	 */
	private Integer oldActiveAccounts;
	/**
	 * 老帐号充值金额
	 */
	private BigDecimal oldActivePayAmounts;
	/**
	 * 老帐号充值人数
	 */
	private Integer oldActiveFeeAccounts;
	/**
	 * 老帐号付费率
	 */
	private String oldPayFeeRate;
	/**
	 * 老帐号ARPU
	 */
	private BigDecimal oldArpu;

	/**
	 * 老账号ARPPU
	 */
	private BigDecimal oldArppu;
}
