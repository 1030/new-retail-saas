package com.dy.yunying.api.resp.yyz;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 游戏预约表
 * @author  kongyanfang
 * @version  2022-01-17 17:14:29
 * table: game_appointment
 */
@Data
public class GameAppointmentRes implements Serializable {
	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 父游戏ID
	 */
	private Long pgid;
	/**
	 * 预约手机号
	 */
	private String mobile;
	/**
	 * 预约设备（1：IOS  2：安卓）
	 */
	private Integer appointDevice;
	/**
	 * 预约来源（1：PC  2：H5）
	 */
	private Integer appointSource;
	/**
	 * 预约时间
	 */
	private Date appointTime;
	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	private Integer type;
	/**
	 * 是否删除  0否 1是
	 */
	private Integer deleted;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 创建人
	 */
	private Long createId;
	/**
	 * 修改人
	 */
	private Long updateId;
}


