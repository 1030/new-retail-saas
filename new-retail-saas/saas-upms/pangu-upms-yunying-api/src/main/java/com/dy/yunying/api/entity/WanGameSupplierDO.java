package com.dy.yunying.api.entity;

import lombok.Data;

import java.util.Date;

/**
 * wan_game_supplier
 *
 * @author cx
 * @date 2020-09-03 15:48:23
 */
@Data
public class WanGameSupplierDO {
	/**
	 * 游戏ID
	 */
	private Integer gid;

	/**
	 * 终端类型,0：pc网页端  1：h5   2:安卓手游 3:IOS手游
	 */
	private Short terminalType;

	/**
	 * 游戏名称
	 */
	private String name;

	/**
	 * 游戏币兑换签名key
	 */
	private String exchangeKey;

	/**
	 * 游戏查询类接口签名key
	 */
	private String queryKey;

	/**
	 * 数据上报类接口签名key
	 */
	private String reportKey;

	/**
	 * 兑换任务类
	 */
	private String exchangeClass;

	/**
	 * 进入游戏接口地址
	 */
	private String loginGameUrl;

	/**
	 *
	 */
	private String exchangeUrl;

	/**
	 * 兑换结果查询接口地址
	 */
	private String exchangeQueryUrl;

	/**
	 *
	 */
	private String roleQueryUrl;

	/**
	 * 参数加密状态 0:启用 1:禁用
	 */
	private Short status;

	/**
	 * 更新时间
	 */
	private Date updatetime;

	/**
	 * 添加时间
	 */
	private Date createtime;

	/**
	 *
	 */
	private String remark;

	/**
	 * 扩展参数，接口地址栏位或签名不足可在此字段中扩展，结构为json字符串
	 */
	private String extinfo;
}