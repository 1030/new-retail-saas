package com.dy.yunying.api.req;

import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 抽奖活动配置表(raffle_activity)实体类
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class FutureMaterialReq extends Page {
    private static final long serialVersionUID = 1L;
    /**
     * 潜力类型
     */
	private Integer futureType;

	private String sdate;

	/**
	 * 结束日期，格式为：20220322
	 */
	private String edate;

}