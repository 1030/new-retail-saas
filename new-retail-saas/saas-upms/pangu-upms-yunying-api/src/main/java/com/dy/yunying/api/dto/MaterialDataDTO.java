package com.dy.yunying.api.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import com.dy.yunying.api.enums.MakeTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class MaterialDataDTO {

	private static final String DEFAULT_VALUE = "-";

	/**
	 * 周期
	 */
	private String period;

	/**
	 * 素材ID
	 */
	private Long materialId;

	/**
	 * 素材名称
	 */
	private String materialName;

	/**
	 * 素材文件URL
	 */
	private String materialFileUrl;

	/**
	 * 素材视频封面URL
	 */
	private String materialCoverUrl;

	/**
	 * 落地页ID
	 */
	private String landingPageId;

	/**
	 * 落地页名称
	 */
	private String landingPageName;

	/**
	 * 落地页缩略图URL
	 */
	private String landingPageUrl;

	/**
	 * 主渠道编码
	 */
	private String parentchl;

	/**
	 * 主渠道名称
	 */
	private String parentchlName;

	/**
	 * 系统类型
	 */
	private Integer os;

	/**
	 * 系统名称
	 */
	private String osStr;

	/**
	 * 素材类型
	 */
	private Integer materialType;

	/**
	 * 素材类型名称
	 */
	private String materialTypeStr;

	/**
	 * 落地页类型：TT_GROUP-头条程序化落地页；TT_ORANGE-头条橙子落地页；…
	 */
	private String landingPageType;

	/**
	 * 落地页类型名称
	 */
	private String landingPageTypeStr;

	/**
	 * 创意者ID
	 */
	private Integer creatorId;

	/**
	 * 创意者姓名
	 */
	private String creatorName;

	/**
	 * 制作者ID
	 */
	private Integer makerId;

	/**
	 * 制作者姓名
	 */
	private String makerName;

	/**
	 * 素材宽度
	 */
	private String materialWidth;

	/**
	 * 素材高度
	 */
	private String materialHeight;

	/**
	 * 素材尺寸/像素
	 */
	private String materialPixel;

	/**
	 * 主游戏ID
	 */
	private Long pgid;

	/**
	 * 主游戏名称
	 */
	private String pgname;

	/**
	 * 制作类型
	 */
	private Integer makeType;

	/**
	 * 制作类型名称
	 */
	private String makeTypeStr;

	/**
	 * 卖点ID
	 */
	private Long sellingPointId;

	/**
	 * 卖点名称
	 */
	private String sellingPointName;

	/**
	 * 素材持续使用天数
	 */
	private Long materialUsageDays;

	/**
	 * 素材被使用次数
	 */
	private Long materialUsageNum;

	/**
	 * 素材使用率(%)
	 */
	private BigDecimal materialUsageRate;

	/**
	 * 10%播放数
	 */
	private Long play10perNum;

	/**
	 * 10%播放率(%)
	 */
	private BigDecimal play10perRate;

	/**
	 * 25%播放数
	 */
	private Long play25perNum;

	/**
	 * 25%播放率(%)
	 */
	private BigDecimal play25perRate;

	/**
	 * 50%播放数
	 */
	private Long play50perNum;

	/**
	 * 50%播放率(%)
	 */
	private BigDecimal play50perRate;

	/**
	 * 75%播放数
	 */
	private Long play75perNum;

	/**
	 * 75%播放率(%)
	 */
	private BigDecimal play75perRate;

	/**
	 * 95%播放数
	 */
	private Long play95perNum;

	/**
	 * 95%播放率(%)
	 */
	private BigDecimal play95perRate;

	/**
	 * 99%播放数
	 */
	private Long play99perNum;

	/**
	 * 99%播放率(%)
	 */
	private BigDecimal play99perRate;

	/**
	 * 总播放数
	 */
	private Long playTotalNum;

	/**
	 * 完播数
	 */
	private Long playOverNum;

	/**
	 * 完成播放率(%)
	 */
	private BigDecimal playOverRate;

	/**
	 * 有效播放数
	 */
	private Long playValidNum;

	/**
	 * 有效播放率(%)
	 */
	private BigDecimal playValidRate;

	/**
	 * 有效播放成本
	 */
	private BigDecimal playValidCost;

	/**
	 * 展示数
	 */
	private Long showNum;

	/**
	 * 点击数
	 */
	private Long clickNum;

	/**
	 * 原始消耗
	 */
	private BigDecimal rudeCost;

	/**
	 * 返点后消耗
	 */
	private BigDecimal cost;

	/**
	 * 展示点击率(%)
	 */
	private BigDecimal showClickRate;

	/**
	 * 激活数
	 */
	private Long activeNum;

	/**
	 * 点击激活率(%)
	 */
	private BigDecimal clickActiveRate;

	/**
	 * 注册设备数
	 */
	private Long registerNum;

	/**
	 * 点击注册率(%)
	 */
	private BigDecimal clickRegisterRate;

	/**
	 * 注册成本
	 */
	private BigDecimal registerCost;

	/**
	 * 新增付费设备数
	 */
	private Long newPayNum;

	// 重复设备数
	@ExcelProperty("重复设备数")
	private Integer duplicateDeviceCount;

	/**
	 * 新增付费次留(%)
	 */
	private BigDecimal newPayRetention2;

	/**
	 * 新增付费率(%)
	 */
	private BigDecimal newPayRate;

	/**
	 * 新增付费成本
	 */
	private BigDecimal newPayCost;

	/**
	 * 新增付费实付金额
	 */
	private BigDecimal newPayFee;

	/**
	 * 新增付费代金券金额
	 */
	private BigDecimal newPayGivemoney;

	/**
	 * 累计付费实付金额
	 */
	private BigDecimal totalPayFee;

	/**
	 * 累计付费代金券金额
	 */
	private BigDecimal totalPayGivemoney;

	/**
	 * 累计ROI(%)
	 */
	private BigDecimal totalRoi;

	/**
	 * 首日ROI(%)
	 */
	private BigDecimal firstRoi;

	/**
	 * 次留(%)
	 */
	private BigDecimal retention2;

	/**
	 * 7留(%)
	 */
	private BigDecimal retention7;

	/**
	 * 30留(%)
	 */
	private BigDecimal retention30;
	
	/**
	 * 是否存在素材收集箱
	 * 0.不存在  1.存在
	 */
	private Integer inBox = 0;

	public String getMaterialName() {
		return StringUtils.defaultIfBlank(this.materialName, DEFAULT_VALUE);
	}

	public String getLandingPageName() {
		return StringUtils.defaultIfBlank(this.landingPageName, DEFAULT_VALUE);
	}

	public String getParentchlName() {
		return StringUtils.defaultIfBlank(this.parentchlName, DEFAULT_VALUE);
	}

	public String getOsStr() {
		if (null == this.osStr) {
			this.osStr = null == this.os ? DEFAULT_VALUE : OsEnum.getName(this.os);
		}
		return this.osStr;
	}

	public String getMaterialTypeStr() {
		if (null == this.materialTypeStr) {
			if (null == this.materialType) {
				this.materialTypeStr = DEFAULT_VALUE;
			} else if (1 == this.materialType) {
				this.materialTypeStr = "视频";
			} else if (2 == this.materialType) {
				this.materialTypeStr = "图片";
			} else {
				this.materialTypeStr = DEFAULT_VALUE;
			}
		}
		return this.materialTypeStr;
	}

	public String getLandingPageTypeStr() {
		return StringUtils.defaultIfBlank(this.landingPageTypeStr, DEFAULT_VALUE);
	}

	public String getCreatorName() {
		return StringUtils.defaultIfBlank(this.creatorName, DEFAULT_VALUE);
	}

	public String getMakerName() {
		return StringUtils.defaultIfBlank(this.makerName, DEFAULT_VALUE);
	}

	public String getMaterialPixel() {
		if (null == this.materialPixel) {
			this.materialPixel = StringUtils.isAnyBlank(this.materialWidth, this.materialHeight) ? DEFAULT_VALUE : String.format("%sx%s", this.materialWidth, this.materialHeight);
		}
		return this.materialPixel;
	}

	public String getPgname() {
		return StringUtils.defaultIfBlank(this.pgname, DEFAULT_VALUE);
	}

	public String getMakeTypeStr() {
		return null == this.makeType ? DEFAULT_VALUE : MakeTypeEnum.getName(this.makeType);
	}

	public String getSellingPointName() {
		return StringUtils.defaultIfBlank(this.sellingPointName, DEFAULT_VALUE);
	}

}
