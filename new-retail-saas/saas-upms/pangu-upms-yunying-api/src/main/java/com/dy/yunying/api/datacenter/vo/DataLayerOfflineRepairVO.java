package com.dy.yunying.api.datacenter.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName DataLayerOfflineRepairVO
 * @Description todo
 * @Author nieml
 * @Time 2023/1/10 17:52
 * @Version 1.0
 **/
@Data
@ApiModel("数据分层离线修复程序信息记录表")
public class DataLayerOfflineRepairVO {

	private String tableName;

	private Date day;

	private int hour;

}
