package com.dy.yunying.api.enums;

public enum MonitorAdChannelType {

	ALL(0,"所有"),
	TOUTIAO(1,"今日头条"),
	GDT(8,"广点通"),
	;
	MonitorAdChannelType(Integer type, String desc){
		this.type =type;
		this.desc =desc;
	}

	public static MonitorAdChannelType getByType(Integer type){
		MonitorAdChannelType[] values = MonitorAdChannelType.values();
		for (MonitorAdChannelType value : values) {
			if(value.type.equals(type))return value;
		}
		return null;
	}

	/**
	 * 类型
	 */
	private final Integer type;

	/**
	 * 描述
	 */
	private final String desc;

	public Integer getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}
}
