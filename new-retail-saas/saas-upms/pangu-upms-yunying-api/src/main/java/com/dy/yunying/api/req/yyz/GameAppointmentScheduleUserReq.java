package com.dy.yunying.api.req.yyz;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 游戏预约进度人数表表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_user
 */
@Data
public class GameAppointmentScheduleUserReq extends Page {

	/***
	 *   主键id
	 */
	private String id;

	@ApiModelProperty(value = "预约类型：1幻兽仓角预约  2 烛光勇士预约")
	private String type;

	@ApiModelProperty(value = "预约人数类型：1实际预约人数  2 标签预约人数 3 规则预约人数")
	private String conventionTypeId;

	/**
	 * 标签来源或规则名称的id
	 */
	private String conventionId;

	/**
	 * 标签来源或规则名称
	 */
	private String conventionName;

	/**
	 * 人数
	 */
	private Long number;

	/**
	 * 生成小时类型(1.全天 2.早5晚12 3.早8晚10 4.自定义)
	 */
	private String scheduleType;


	/**
	 *生成时间小时(1-24)
	 */
	private String scheduleHour;

	@ApiModelProperty(value = "预约开始时间" )
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date startTime;
	@ApiModelProperty(value = "预约结束时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date endTime;

	/**
	 * 生成频率
	 */
	private String frequency;

	/**
	 * 实际单位
	 */
	@ApiModelProperty(value = "1:表示分, 2:表示小时, 3:表示天,4:表示月")
	private String timeUnit;

	/**
	 * 最小生成人数
	 */
	private String minNum;
	/**
	 * 最大生成人数
	 */
	private String maxNum;
	/**
	 * 预计增长人数
	 */
	private String expectNum;

	/**
	 * 是否启动(1:表示不启用, 2:表示启用)
	 */
	private String isStart;
	/**
	 * 是否删除(1:表示不删除, 2:表示删除)
	 */
	private String isDelete;

}
