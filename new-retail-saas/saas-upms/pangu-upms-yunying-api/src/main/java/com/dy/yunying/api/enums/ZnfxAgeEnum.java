package com.dy.yunying.api.enums;

public enum ZnfxAgeEnum {

	AGE_LESS_18 ("0-17","AGE_LESS_18","17"),
	AGE_BETWEEN_18_23 ("18-23","AGE_BETWEEN_18_23","18"),
	AGE_BETWEEN_24_30("24-30","AGE_BETWEEN_24_30","24"),
	AGE_BETWEEN_31_40("31-40","AGE_BETWEEN_31_40","31"),
	AGE_BETWEEN_41_49("41-49","AGE_BETWEEN_41_49","41"),
	AGE_ABOVE_50("50以上","AGE_ABOVE_50","50"),
	;
	ZnfxAgeEnum(String name, String desc, String age) {
		this.name = name;
		this.desc = desc;
		this.age = age;
	}

	private String name;

	private String desc;

	private String age;

	public static ZnfxAgeEnum getOneByage(String age) {
		for (ZnfxAgeEnum codeEnum : ZnfxAgeEnum.values()) {
			if (codeEnum.getAge().equals(age)) {
				return codeEnum;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public String getAge() {
		return age;
	}
}
