package com.dy.yunying.api.entity.hongbao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 实物奖品领取地址记录
 * @author  chenxiang
 * @version  2021-10-25 14:20:46
 * table: hb_receiving_address
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "hb_receiving_address")
public class HbReceivingAddress extends Model<HbReceivingAddress>{

	//主键ID
	@TableId(value="id",type= IdType.AUTO)
	private Long id;

	//领取记录ID
	@TableField(value = "record_id")
	private Long recordId;
	//收货姓名
	@TableField(value = "receive_user_name")
	private String receiveUserName;

	//收货电话
	@TableField(value = "receive_user_mobile")
	private String receiveUserMobile;

	//收货地址
	@TableField(value = "receive_user_address")
	private String receiveUserAddress;

	//省编码
	@TableField(value = "address_province_code")
	private String addressProvinceCode;

	//省名称
	@TableField(value = "address_province_name")
	private String addressProvinceName;

	//市编码
	@TableField(value = "address_city_code")
	private String addressCityCode;

	//市名称
	@TableField(value = "address_city_name")
	private String addressCityName;

	//区编码
	@TableField(value = "address_area_code")
	private String addressAreaCode;

	//区名称
	@TableField(value = "address_area_name")
	private String addressAreaName;

	//发货时间
	@TableField(value = "send_time")
	private Date sendTime;

	//快递名称
	@TableField(value = "express_name")
	private String expressName;

	//快递订单号
	@TableField(value = "express_code")
	private String expressCode;

	//是否删除：0否 1是
	@TableField(value = "deleted")
	private Integer deleted;

	//创建时间
	@TableField(value = "create_time")
	private Date createTime;

	//修改时间
	@TableField(value = "update_time")
	private Date updateTime;

	//创建人
	@TableField(value = "create_id")
	private Long createId;

	//修改人
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

