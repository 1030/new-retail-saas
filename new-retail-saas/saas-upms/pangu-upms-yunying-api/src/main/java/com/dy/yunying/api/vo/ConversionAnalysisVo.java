package com.dy.yunying.api.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description 转化分析报表视图渲染对象
 * @Author chengang
 * @Date 2022/2/28
 */
@Data
@Accessors(chain = true)
public class ConversionAnalysisVo implements Serializable {

	private static final long serialVersionUID = -2071825419492268293L;

	/**
	 * 周期
	 */
	private String cycleType;

	/**
	 * 投放人ID
	 */
	private Long investorId;

	/**
	 * 投放人姓名
	 */
	private String investorName;

	/**
	 * 主渠道编码
	 */
	private String parentchl;

	/**
	 * 主渠道名称
	 */
	private String parentchlName;

	/**
	 * 部门主键
	 */
	private Long deptId;

	/**
	 * 部门名称
	 */
	private String deptName;

	/**
	 * 主游戏ID
	 */
	private Long pgid;

	/**
	 * 主游戏名称
	 */
	private String pgname;

	/**
	 * 子游戏ID
	 */
	private Long gameid;

	/**
	 * 子游戏名称
	 */
	private String gname;

	/**
	 * 系统标识
	 */
	private Integer os;

	/**
	 * 系统名称
	 */
	private String osName;

	/**
	 * 展示数
	 */
	private Long shownums = 0L;

	/**
	 * 点击数
	 */
	private Long clicknums = 0L;

	/**
	 * 展示点击率
	 */
	private BigDecimal clickRatio = new BigDecimal(0);

	/**
	 * 激活设备数
	 */
	private Long uuidnums = 0L;

	/**
	 * 点击激活率
	 */
	private BigDecimal regRatio = new BigDecimal(0);

	/**
	 * 启动SDK数
	 */
	private Long startupSdkNums = 0L;

	/**
	 * 注册设备数
	 */
	private Long usrnamenums = 0L;

	/**
	 * 完成注册率
	 */
	private BigDecimal completeRegRatio = new BigDecimal(0);

	/**
	 * 实名设备数
	 */
	private Long certifiedCount = 0L;

	/**
	 * 注册实名率
	 */
	private BigDecimal certifiedRegRatio = new BigDecimal(0);

	/**
	 * 未实名设备数
	 */
	private Long notCertifiedCount = 0L;

	/**
	 * 注册未实名率
	 */
	private BigDecimal noCertifiedRegRatio = new BigDecimal(0);

	/**
	 * 创角设备数
	 */
	private Long createRoleCount = 0L;

	/**
	 * 升级设备数
	 */
	private Long upgradeNums = 0L;

	/**
	 * 创角升级率
	 */
	private BigDecimal createRoleUpgradeRatio = new BigDecimal(0);

	/**
	 * 付费设备数
	 */
	private Long usrpaynamenums = 0L;

	/**
	 * 设备付费率
	 */
	private BigDecimal payRatio = new BigDecimal(0);

}
