package com.dy.yunying.api.enums;

public enum DimensionEnum {

	ACCOUNT(1, "账号"),
	ROLEID(2, "角色");

	private Integer type;
	private String name;
	public Integer getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private DimensionEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static DimensionEnum getOneByType(Integer type){
		for(DimensionEnum dimensionEnum: DimensionEnum.values()){
			if(dimensionEnum.getType() == type){
				return dimensionEnum;
			}
		}
		return null;
	}
}
