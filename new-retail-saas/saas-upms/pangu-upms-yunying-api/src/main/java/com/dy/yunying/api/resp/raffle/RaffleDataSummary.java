package com.dy.yunying.api.resp.raffle;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description
 * @Author chengang
 * @Date 2022/11/9
 */
@Data
public class RaffleDataSummary implements Serializable {

	private static final long serialVersionUID = 3856240198806525569L;

	private Long pv;
	private Long uv;
	private Long ip;

	/**
	 * 绑定角色数
	 */
	private Long bindRoleNum;

	/**
	 * 抽奖角色数
	 */
	private Long lotteryRoleNum;

	/**
	 * 兑换角色数
	 */
	private Long exchangeRoleNum;

	/**
	 * 人均抽奖次数
	 */
	private Long perLotteryNum;

	/**
	 * 完成任务角色数
	 */
	private Long taskRoleNum;

	/**
	 * 角色ARPPU
	 */
	private BigDecimal roleArppu;


}
