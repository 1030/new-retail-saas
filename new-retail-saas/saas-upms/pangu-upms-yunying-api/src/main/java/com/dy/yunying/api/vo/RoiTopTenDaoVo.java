package com.dy.yunying.api.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


@Data
@Accessors(chain = true)
public class RoiTopTenDaoVo {

	private Integer sort;
	/**
	 * 周期
	 */
	private BigDecimal roi;
	/**
	 * 开始日期，格式为：20220322
	 */
	private BigDecimal cost;

	private String cid;

	private String adid;

}
