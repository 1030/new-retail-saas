package com.dy.yunying.api.enums;

import java.util.Objects;

/**
 * @author chenxiang
 * @className PushTypeEnum
 * @date 2022-4-25 17:37
 */
public enum PushNodeEnum {

	FIRST_LOGIN(1, "首次登录成功"),
	EVERY_LOGIN(2, "每次登录成功"),
	FIRST_ENTER_GAME(3, "首次进入游戏"),
	EVERY_ENTER_GAME(4, "每次进入游戏"),
	FIRST_PAY_SUCCESS(6, "首次支付成功"),
	EVERY_PAY_SUCCESS(7, "每次支付成功"),
	FIRST_PAY_CANCEL(8, "首次支付取消"),
	EVERY_PAY_CANCEL(9, "每次支付取消");

	private Integer type;

	private String name;

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	private PushNodeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(Integer type) {
		if (Objects.isNull(type)) {
			return null;
		}
		for (PushNodeEnum ele : values()) {
			if (ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return null;
	}

}
