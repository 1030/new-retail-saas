package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author yuwenfeng
 * @description: 提现档次
 * @date 2021/10/23 11:04
 */
@Data
public class CashConfigReq implements Serializable {

	@ApiModelProperty(value = "提现档次ID")
	private Long id;

	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	@ApiModelProperty(value = "提现类型(1游戏货币 2微信 3支付宝 4代金券)")
	private Integer cashType;

	@ApiModelProperty(value = "提现档次")
	@NotBlank(message = "提现档次不能为空")
	@Length(min = 1, max = 20, message = "提现档次字符长度不合法")
	private String cashName;

	@ApiModelProperty(value = "现金价值(元)")
	@NotNull(message = "现金价值不能为空")
	@Max(value = 9999999, message = "现金价值必须小于等于9999999")
	@Min(value = 0, message = "现金价值必须大于等于0")
	private BigDecimal cashMoney;

	@ApiModelProperty(value = "角标开关(1开 0关)")
	@NotNull(message = "角标开关不能为空")
	private Integer subscriptOff;

	@ApiModelProperty(value = "角标内容")
	private String subscriptName;

	@ApiModelProperty(value = "礼包码类型：1唯一码，2通用码")
	private Integer giftType;

	@ApiModelProperty(value = "礼包通用码")
	private String giftCode;

	@ApiModelProperty(value = "总数量（-1不限制）")
	private String giftAmount;

	@ApiModelProperty(value = "礼包码文件")
	private MultipartFile file;

	/**
	 * 代金券名称
	 */
	private String cdkName;
	/**
	 * 代金券金额
	 */
	private String cdkAmount;
	/**
	 * 限制金额
	 */
	private String cdkLimitAmount;
	/**
	 * 有效规则类型  1长期游戏 2固定时间 3领券后指定天数
	 */
	private Integer expiryType;
	/**
	 * 有效开始时间
	 */
	private String startTime;
	/**
	 * 有效结束时间
	 */
	private String endTime;
	/**
	 * 有效天数
	 */
	private Integer days;

	//提现游豆
	private String currencyAmount;

	@ApiModelProperty(value = "代金券限制类型 1:无门槛 2:指定游戏 3:指定角色")
	private Integer cdkLimitType;
}
