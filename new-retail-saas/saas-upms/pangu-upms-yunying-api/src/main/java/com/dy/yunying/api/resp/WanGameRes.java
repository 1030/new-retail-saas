package com.dy.yunying.api.resp;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 游戏
 * wan_game
 * @author hma
 * @date 2021-06-02 14:22:57
 */
@Getter
@Setter
@ApiModel(value = "渠道管理")
public class WanGameRes {
    /**
     * 游戏ID
     */
	@ApiModelProperty(value = "游戏ID")
	@JSONField(name="gameId")
    private Long id;

    /**
     * 游戏ID
     */
	@ApiModelProperty(value = "游戏名称")
    private String gname;


	/**
	 * 游戏ID
	 */
	@ApiModelProperty(value = "父游戏id")
	private Long pgid;


	/**
	 * 游戏类别
	 */
	@ApiModelProperty(value = "游戏类别")
	private Integer type;
	/**
	 * 包名
	 */
	@ApiModelProperty(value = "包名")
	private String pkName;

	/**
	 * 中文首字母名称
	 */
	@ApiModelProperty(value = "中文首字母名称")
	private String firstWordsName;

	private List<WanGameRes> sonlist;

}