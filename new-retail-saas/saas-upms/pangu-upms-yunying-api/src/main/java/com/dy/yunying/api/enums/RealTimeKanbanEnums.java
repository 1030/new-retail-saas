package com.dy.yunying.api.enums;

/**
 * @author sunyq
 * @date 2022/8/17 10:52
 */
public enum RealTimeKanbanEnums {

	ACCOUNT_NUM("新增账号","bg1","今日首次注册的账号数"),
	DIVICE_NUM("新增设备","bg2","今日首次完成初始化的设备数"),
	ACTIVE_PAY_AMOUNTS("充值金额","bg3","今日的成功充值的金额总值"),
	ACTIVE_FEE_ACCOUNTS("充值人数","bg4","今日充值的总人数"),
	ACTIVE_ACCOUNTS("活跃账号","bg5","今日登陆的账号数"),
	PAY_FEE_RATE("付费率","bg6","今日充值人数占活跃账号的比例"),
	;

	RealTimeKanbanEnums(String name, String color, String tips) {
		this.name = name;
		this.color = color;
		this.tips = tips;
	}

	public String getName() {
		return name;
	}

	public String getColor() {
		return color;
	}

	public String getTips() {
		return tips;
	}

	private String name;

	private String color;

	private String tips;
}
