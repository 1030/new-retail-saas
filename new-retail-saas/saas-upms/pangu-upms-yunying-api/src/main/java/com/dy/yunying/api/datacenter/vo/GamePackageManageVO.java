package com.dy.yunying.api.datacenter.vo;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class GamePackageManageVO {

    /**
     * 分包渠道ID
     */
    private Long packId;

    /**
     * 子游戏ID
     */
    private Long gameId;

    /**
    *  子游戏名称
     */
    private String gname;

    /**
     * 主渠道
     */
    private String chnname;
    /**
     * 主渠道ID
     */
    private Integer platformId;

    /**
     * 第三方APPID
     */
    private String appId;

    /**
     * 第三方APPNAME
     */
    private String appName;

    /**
     * 上报方式
     */
    private Integer rtype;
    /**
     * 广点通appid
     */
    private String gdtAppid;

    /**
     * 母包版本
     */
    private String pName;

    /**
     * 基础包包版本
     */
    private String sName;
    /**
     * 母包code
     */
    private Integer pCode;

    /**
     * 基础包code
     */
    private Integer sCode;

    /**
     * 状态
     */
    private Integer status;

    private Long pgid;

    public String getRtypeStr () {
        String rtypeStr = "";
        switch (rtype) {
            case 0 :
                rtypeStr = "不上报";
                break;
            case 1 :
                rtypeStr = "API上报";
                break;
            case 2 :
                rtypeStr = "SDK上报";
                break;
            default:
                rtypeStr = "";
        }
        return rtypeStr;
    }

    /**
     *
     */
    private String path;

    private String url;

    public String getSCodeStr() {
        String sCodeStr = null;
        if (sCode != null && StringUtils.isNotBlank(sName)) {
            sCodeStr = sName + "【" + sCode + "】";
        }
        return sCodeStr;
    }

    public String getPCodeStr() {
        String pCodeStr = null;
        if (pCode != null && StringUtils.isNotBlank(pName)) {
            pCodeStr = pName + "【" + pCode + "】";
        }
        return pCodeStr;
    }

}
