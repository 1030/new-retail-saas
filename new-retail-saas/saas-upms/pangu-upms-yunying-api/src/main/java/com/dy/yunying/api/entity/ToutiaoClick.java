package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yuwenfeng
 * @description: 头条点击表
 * @date 2022/3/2 15:50
 */
@TableName("toutiao_click")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ToutiaoClick {
	private String callbackUrl;
}
