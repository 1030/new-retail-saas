package com.dy.yunying.api.datacenter.dto;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author hma
 * @date 2022/7/25 15:14
 */
@Data
@ApiModel("每日运营查询对象")
public class ActivityRechargeDataDto {

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	public String getUserIds() {
		String userIds =null;  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	public String getAdAccounts() {
		String adAccounts =null;  // 默认不查询任何广告账户
		if (CollectionUtils.isNotEmpty(roleAdAccountList)) {
			adAccounts = "'" + StringUtils.join(roleAdAccountList, "','") + "'";
		}
		return adAccounts;
	}

	@ApiModelProperty("是否系统管理员")
	private int isSys = 0;


	@ApiModelProperty("查询开始时间")
	@NotNull(message = "查询开始日期不允许为空")

	private String startTime;

	@ApiModelProperty("查询结束时间")
	@NotNull(message = "查询结束日期不允许为空")
	private String endTime;


	@ApiModelProperty("查询开始时间")
	@NotNull(message = "查询注册开始日期不允许为空")

	private String regStartTime;

	@ApiModelProperty("查询结束时间")
	@NotNull(message = "查询注册结束日期不允许为空")
	private String regEndTime;


	@ApiModelProperty("主渠道列表")
	private String parentChlArr;



	@ApiModelProperty("渠道编码列表")
	private String chlArr;



	@ApiModelProperty("分包编码列表")
	private String appChlArr;




	@ApiModelProperty("广告计划")
	private String adidArr;



	@ApiModelProperty("父游戏")
	private String pgIdArr;

	@ApiModelProperty("子游戏")
	private String gameIdArr;





	@ApiModelProperty("投放人数组")
	private String investorArr;

	@ApiModelProperty("广告账号数组")
	private String advertiserIdArr;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * UCID
	 */
	private String ucId;


	private List<Integer> investorList;
	private List<String> advertiserIdList;


	private String titles;
	private String columns;

	private Long current = 1L;
	private Long size = 10000L;


	public List<String> getAdvertiserIdList() {
		return StringUtils.isNotEmpty(advertiserIdArr) ? Arrays.asList(advertiserIdArr.split(",")) : new ArrayList<>();
	}

	public void setAdvertiserIdList(List<String> advertiserIdList) {
		this.advertiserIdList = advertiserIdList;
	}


	//排序
	private String sort;

	//排序 指标字段值
	private String kpiValue;






}
