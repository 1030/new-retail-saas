package com.dy.yunying.api.req.usergroup;

import com.baomidou.mybatisplus.annotation.TableField;
import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserGroupReq extends Page implements Serializable {

	private static final long serialVersionUID = 3799186434822397683L;

	@ApiModelProperty(value = "群组名称")
	private String groupName;

	@ApiModelProperty(value = "群组归属：1.平台，2.用户，3.产品，4.渠道，10.其他")
	private Integer groupType;

	@ApiModelProperty(value = "群组分类：1.全部游戏、2.部分游戏、3.指定游戏")
	private Integer groupClass;

	@ApiModelProperty(value = "指定游戏id")
	private Long classGameid;

	@ApiModelProperty(value = "筛选维度：1账号，2角色")
	private Integer dimension;

	@ApiModelProperty(value = "筛选方式：1规则筛选，2ID筛选")
	private Integer type;
}
