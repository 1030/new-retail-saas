package com.dy.yunying.api.entity.znfx;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * @author: zhuxm
 * @time: 2023/3/21 17:53
 */
@Data
public class ChartData implements Serializable {

	private static final long serialVersionUID = -6049461051729226011L;

	//图形类型 pie:饼 line:折线图 bar:柱状图
	private String type;

	private List<ChartDataDetail> data;
}
