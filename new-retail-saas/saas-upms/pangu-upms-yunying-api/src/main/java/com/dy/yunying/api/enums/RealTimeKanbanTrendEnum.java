package com.dy.yunying.api.enums;

/**
 * @author sunyq
 * @date 2022/8/17 11:26
 */
public enum RealTimeKanbanTrendEnum {
	UP("up"),
	DOWN("down"),
	FLAT("-")
	;

	RealTimeKanbanTrendEnum(String trend) {
		this.trend = trend;
	}

	public String getTrend() {
		return trend;
	}

	private String trend;



}
