package com.dy.yunying.api.datacenter.vo;

import com.dy.yunying.api.entity.ChannelManageDO;
import org.apache.commons.lang3.StringUtils;
import java.math.BigDecimal;

public class ChannelManageVo extends ChannelManageDO {

	private String operate; // 操作

	private String soncode; // 子渠道编码

	private String sonname; // 子渠道编码  Investor

	private String agentname; // 代理商名称

	private String leadperson; // 负责人

	private String effectDate; // 生效时间

	private Integer cid; // 子渠道id

	private String platformname; // 平台类型名称

	private String appchl; // 分包渠道编码

	private String chl; // 子渠道编码

	private BigDecimal rebate;

	/**
	 * 负责人或者投放人
	 */
	//private String manage;

	/**
	 * 广告账号
	 */
	private String toutiaoAccount;

	/**
	 * 广告账号
	 */
	private String adAccountValue;

	/**
	 * 1.api   2.sdk 上报方式
	 */
	private Integer rtype;

	/**
	 * 平台类型 平台，0,3367平台，1头条，2百度,3 新数，4.公会
	 */
	private Integer platform;

	/**
	 * 是否上报0不上报1上报
	 */
	private Integer isup;

	/**
	 * 0 sdk支付，1苹果内置支付
	 */
	private Integer paytype;

	public void setOperate(String operate) {
		this.operate = operate;
	}

	public void setSoncode(String soncode) {
		this.soncode = soncode;
	}

	public void setSonname(String sonname) {
		this.sonname = sonname;
	}

	public void setAgentname(String agentname) {
		this.agentname = agentname;
	}

	public void setLeadperson(String leadperson) {
		this.leadperson = leadperson;
	}

	public void setEffectDate(String effectDate) {
		this.effectDate = effectDate;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public void setPlatformname(String platformname) {
		this.platformname = platformname;
	}

	public void setAppchl(String appchl) {
		this.appchl = appchl;
	}

	public void setChl(String chl) {
		this.chl = chl;
	}

	@Override
	public void setToutiaoAccount(String toutiaoAccount) {
		this.toutiaoAccount = toutiaoAccount;
	}

	public void setAdAccountValue(String adAccountValue) {
		this.adAccountValue = adAccountValue;
	}

	@Override
	public void setRtype(Integer rtype) {
		this.rtype = rtype;
	}

	@Override
	public void setPlatform(Integer platform) {
		this.platform = platform;
	}

	@Override
	public void setIsup(Integer isup) {
		this.isup = isup;
	}

	@Override
	public void setPaytype(Integer paytype) {
		this.paytype = paytype;
	}

	public String getOperate() {
		return operate;
	}

	public String getSoncode() {
		return soncode;
	}

	public String getSonname() {
		return sonname;
	}

	public String getAgentname() {
		return agentname;
	}

	public String getLeadperson() {
		return leadperson;
	}

	public String getEffectDate() {
		if (StringUtils.isNotEmpty(effectDate)) {
			return String.format("%s-%s-%s", effectDate.substring(0, 4), effectDate.substring(4, 6), effectDate.substring(6, 8));
		}
		return effectDate;
	}

	public Integer getCid() {
		return cid;
	}

	public String getPlatformname() {
		return platformname;
	}

	public String getAppchl() {
		return appchl;
	}

	public String getChl() {
		return chl;
	}

	@Override
	public String getToutiaoAccount() {
		return toutiaoAccount;
	}

	public String getAdAccountValue() {
		return adAccountValue;
	}

	@Override
	public Integer getRtype() {
		return rtype;
	}

	@Override
	public Integer getPlatform() {
		return platform;
	}

	@Override
	public Integer getIsup() {
		return isup;
	}

	@Override
	public Integer getPaytype() {
		return paytype;
	}

	public BigDecimal getRebate() {
		return rebate;
	}

	public void setRebate(BigDecimal rebate) {
		this.rebate = rebate;
	}
}
