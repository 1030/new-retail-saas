package com.dy.yunying.api.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MaterialDaoVo {

	private String materialName;

	private String materialUrl;

	private String materialId;

	private String imageUrl;
	private String format;
	/**
	 * 三方素材ID
	 */
	private String mId;

	private String advertiserId;

	private String tmId;

}
