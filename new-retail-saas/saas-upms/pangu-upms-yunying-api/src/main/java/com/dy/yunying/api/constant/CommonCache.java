package com.dy.yunying.api.constant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 公用缓存数据
 *
 * @Author: hjl
 * @Date: 2020/4/27 9:51
 */
public class CommonCache {

    private static final Map<String, Object> cache = new ConcurrentHashMap<>();

    public static final String KEY_GAME_INFO = "game_info";

    public static Object get(String key) {
        return cache.get(key);
    }

    public static void put(String key, Object value) {
        cache.put(key, value);
    }

}