package com.dy.yunying.api.resp.hongbao;

import lombok.Data;

import java.io.Serializable;

/**
 * @description: 下拉数据通用
 * @author yuwenfeng
 * @date 2022/3/14 16:02
 */
@Data
public class OptionData implements Serializable {

	private Long id;

	private String name;

}
