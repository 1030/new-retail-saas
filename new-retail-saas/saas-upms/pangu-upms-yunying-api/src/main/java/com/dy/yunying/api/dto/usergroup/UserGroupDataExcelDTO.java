package com.dy.yunying.api.dto.usergroup;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Date;

@Slf4j
@Data
public class UserGroupDataExcelDTO {

	/**
	 * 角色/账号
	 */
	@ExcelProperty(index = 0)
	private BigDecimal groupData;

}
