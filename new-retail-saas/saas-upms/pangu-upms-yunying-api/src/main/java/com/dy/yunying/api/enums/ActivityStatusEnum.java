package com.dy.yunying.api.enums;

public enum ActivityStatusEnum {

	READY(1,"待上线"),
	ACTIVITING(2,"活动中"),
	CLOSE(3,"已下线");

	ActivityStatusEnum(Integer status, String desc){
		this.status =status;
		this.desc =desc;
	}


	private Integer status;

	private String desc;

	public Integer getStatus() {
		return status;
	}

	public String getDesc() {
		return desc;
	}
}
