package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum TtDeepExternalActionEnum {

	//深度转化目标
	AD_CONVERT_TYPE_PAY   ("AD_CONVERT_TYPE_PAY", "付费", 1),
	AD_CONVERT_TYPE_NEXT_DAY_OPEN("AD_CONVERT_TYPE_NEXT_DAY_OPEN", "次留", 2),
	AD_CONVERT_TYPE_GAME_ADDICTION  ("AD_CONVERT_TYPE_GAME_ADDICTION","关键行为", 3),
	AD_CONVERT_TYPE_PURCHASE_ROI  ("AD_CONVERT_TYPE_PURCHASE_ROI","付费ROI", 4),
	AD_CONVERT_TYPE_LT_ROI  ("AD_CONVERT_TYPE_LT_ROI","广告变现ROI", 5),
	AD_CONVERT_TYPE_UG_ROI  ("AD_CONVERT_TYPE_UG_ROI","内广ROI", 6),
	DEEP_BID_DEFAULT  ("DEEP_BID_DEFAULT","无", null);

	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;


	private Integer value;

	//构造方法
	TtDeepExternalActionEnum(String type, String name, Integer value){
		this.type = type;
		this.name = name;
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public Integer getValue() {
		return value;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (TtDeepExternalActionEnum item : TtDeepExternalActionEnum.values()) {
			if (String.valueOf(item.getValue()).equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static Integer valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (TtDeepExternalActionEnum item : TtDeepExternalActionEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getValue();
			}
		}
		return null;

	}


	/**
	 * 通过type   取name
	 * @return
	 */
	public static String nameByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (TtDeepExternalActionEnum item : TtDeepExternalActionEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	/**
	 * value 取name
	 * @return
	 */
	public static String valueByName(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}
		for (TtDeepExternalActionEnum item : TtDeepExternalActionEnum.values()) {
			if (String.valueOf(item.getValue()).equals(value)) {
				return item.getName();
			}
		}
		return null;

	}




	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (TtDeepExternalActionEnum item : TtDeepExternalActionEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
