package com.dy.yunying.api.enums;

public enum DataReportEnum {
	ADANALYSIS("AdAnalysis","广告数据分析报表"),
	ADDATA("AdData","广告数据报表"),
	HOURDATA("HourData","分时报表"),
	;

	private String type;

	private String name;

	DataReportEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
