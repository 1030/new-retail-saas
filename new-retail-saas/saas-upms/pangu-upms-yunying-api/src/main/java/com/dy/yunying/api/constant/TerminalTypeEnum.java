package com.dy.yunying.api.constant;

/**
 * 终端类型（游戏端口）
 * 0：pc网页端  1：h5   2:安卓手游 3:IOS手游
 *
 * @author hjl
 * @date 2020-5-7 20:00:47
 */
public enum TerminalTypeEnum {

	PC(0, "PC"),
	H5(1, "H5"),
	ANDROID(2, "安卓"),
	IOS(3, "ios");

	private TerminalTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}
	public static String getName(Integer type) {
		for (TerminalTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return "未知";
	}

	private Integer type;

	private String name;

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
