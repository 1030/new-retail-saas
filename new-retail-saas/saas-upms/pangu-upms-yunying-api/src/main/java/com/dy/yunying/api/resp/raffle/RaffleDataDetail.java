package com.dy.yunying.api.resp.raffle;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description
 * @Author chengang
 * @Date 2022/11/9
 */
@Data
public class RaffleDataDetail implements Serializable {

	private static final long serialVersionUID = -8976820643389238402L;

	private String reportDate;

	private Long activityId;

	private String activityName;

	private Long pv;
	private Long uv;
	private Long ip;

	/**
	 * 绑定角色数
	 */
	private Long bindRoleNum;

	/**
	 * 抽奖角色数
	 */
	private Long lotteryRoleNum;

	/**
	 * 兑换角色数
	 */
	private Long exchangeRoleNum;

	/**
	 * 人均抽奖次数
	 */
	private Long perLotteryNum;

	/**
	 * 完成任务角色数
	 */
	private Long taskRoleNum;

	/**
	 * 角色ARPPU
	 */
	private BigDecimal roleArppu;

}
