/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 广告域名管理
 *
 * @author pigx code generator
 * @date 2021-06-04 15:53:15
 */
@Data
@TableName("advertiser_domain")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "广告域名管理")
public class AdvertiserDomain extends Model<AdvertiserDomain> {
private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
	@TableId(value = "domain_id", type = IdType.AUTO)
    @ApiModelProperty(value="自增ID")
    private Integer domainId;
    /**
     * 域名地址
     */
    @ApiModelProperty(value="域名地址")
    private String domainAddr;
    /**
     * 是否默认：0：否 1：是 
     */
    @ApiModelProperty(value="是否默认：0：否 1：是 ")
    private Integer isDefault;
    /**
     * 状态：0：禁用 1：启用  2：删除
     */
    @ApiModelProperty(value="状态：0：禁用 1：启用  2：删除")
    private Integer status;
    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;
    /**
     * 创建者ID
     */
    @ApiModelProperty(value="创建者ID")
    private Integer createBy;
    /**
     * 创建者名称
     */
    @ApiModelProperty(value="创建者名称")
    private String createUser;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    /**
     * 更新者ID
     */
    @ApiModelProperty(value="更新者ID")
    private Integer updateBy;
    /**
     * 更新者名称
     */
    @ApiModelProperty(value="更新者名称")
    private String updateUser;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private Date updateTime;
    }
