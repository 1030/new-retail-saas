package com.dy.yunying.api.vo.hbdata;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/17 18:31
 * @description：
 * @modified By：
 */
@Data
public class HbDataNoticeVo implements Serializable {

	/**
	 * 活动名称
	 */
	private String sourceName;

	/**
	 * 通知类型
	 */
	private String popupType;

	/**
	 * 通知类型名称
	 */
	private String popupTypeName;


	/**
	 * 达到数
	 */
	private Integer serviceNum;

	/**
	 * PV
	 */
	private Integer servicePV;

	/**
	 * UV
	 */
	private Integer serviceUV;

	public String getPopupTypeName() {
		String popupTypeName = null;
		switch (popupType) {
			case "1":
				popupTypeName = "活动开启弹窗";
				break;
			case "2":
				popupTypeName = "活动关闭提醒";
				break;
			case "3":
				popupTypeName = "消息";
				break;
			case "4":
				popupTypeName = "浮标";
				break;
			default:
				popupTypeName = "-";
		}
		return popupTypeName;
	}
}
