package com.dy.yunying.api.vo.hbdata;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/18 17:10
 * @description：
 * @modified By：
 */
@Data
public class HbActiveFinishVo implements Serializable {

	// 活动名称
	private String activityName;

	// 红包名称
	private String hbName;

	// 完成数
	private Integer finishNum;
}
