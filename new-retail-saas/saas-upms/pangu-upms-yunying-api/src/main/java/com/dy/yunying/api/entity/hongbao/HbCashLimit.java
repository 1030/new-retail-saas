/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 提现规则限制表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:19
 */
@Data
@TableName("hb_cash_limit")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "提现规则限制表")
public class HbCashLimit extends HbBaseEntity {

	/**
	 * 提现规则限制ID
	 */
	@TableId(value = "id",type = IdType.AUTO)
	@ApiModelProperty(value = "提现规则限制ID")
	private Long id;

	/**
	 * 活动ID
	 */
	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	/**
	 * 提现类型(1游戏货币 2微信 3支付宝)
	 */
	@ApiModelProperty(value = "提现类型(1游戏货币 2微信 3支付宝)")
	private Integer cashType;

	/**
	 * 规则类型(1每天次数 2累计次数 3累计金额)
	 */
	@ApiModelProperty(value = "规则类型(1每天次数 2累计次数 3累计金额)")
	private Integer ruleType;

	/**
	 * 数量，-1则表示无限制
	 */
	@ApiModelProperty(value = "数量，-1则表示无限制")
	private BigDecimal ruleLimit;

}
