package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 礼包码类型：1唯一码，2通用码
 *
 * @author sunyouquan
 * @date 2022/5/5 14:22
 */
public enum GiftTypeEnum {

	UNIQUE(1, "唯一码"), COMMON(2, "通用码");

	private Integer type;

	private String name;

	GiftTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonValue
	public Integer getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}

	@JsonCreator
	public static GiftTypeEnum getGiftTypeEnum(Integer value) {
		for (GiftTypeEnum giftTypeEnum : values()) {
			if (giftTypeEnum.type.equals(value)) {
				return giftTypeEnum;
			}
		}
		return null;
	}

}
