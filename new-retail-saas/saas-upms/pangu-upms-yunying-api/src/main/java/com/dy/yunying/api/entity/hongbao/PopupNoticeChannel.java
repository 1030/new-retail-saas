package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ：lile
 * @date ：2021/10/27 11:26
 * @description：
 * @modified By：
 */
@Data
@TableName("popup_notice_channel")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "公告渠道范围关系表")
public class PopupNoticeChannel extends HbBaseEntity {

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "公告渠道关系ID")
	private Long id;

	@ApiModelProperty(value = "公告ID")
	private Long noticeId;

	@ApiModelProperty(value = "主渠道")
	private String parentchl;

	@ApiModelProperty(value = "子渠道")
	private String chl;

	@ApiModelProperty(value = "分包渠道")
	private String appchl;

	@ApiModelProperty(value = "渠道层级(1主渠道 2子渠道 3分包渠道)")
	private int channelRange;
}
