package com.dy.yunying.api.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author sunyq
 * @date 2022/8/17 9:36
 */
@Data
public class RealTimeKanbanDO implements Serializable {
	/**
	 * 时间
	 */
	private String dataTime;
	/**
	 * 新增账号数
	 */
	private BigDecimal accountNums = BigDecimal.ZERO;
	/**
	 * 新增设备
	 */
	private BigDecimal deviceNums = BigDecimal.ZERO;
	/**
	 * 充值金额
	 */
	private BigDecimal activePayAmounts = BigDecimal.ZERO;
	/**
	 * 充值人数
	 */
	private BigDecimal activeFeeAccounts = BigDecimal.ZERO;
	/**
	 * 活跃账号
	 */
	private BigDecimal activeAccounts = BigDecimal.ZERO;
	/**
	 * 付费率
	 */
	private BigDecimal payFeeRate = BigDecimal.ZERO;
}
