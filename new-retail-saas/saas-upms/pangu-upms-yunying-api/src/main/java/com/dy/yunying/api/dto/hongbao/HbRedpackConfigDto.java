package com.dy.yunying.api.dto.hongbao;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 红包配置表
 * @author  chenxiang
 * @version  2021-10-25 20:36:34
 * table: hb_redpack_config
 */
@Data
public class HbRedpackConfigDto implements Serializable {
	
	/**
	 * 主键ID
	 */
	private Long id;
	/**
	 * 活动ID
	 */
	private Long activityId;
	/**
	 * 红包内容：1现金红包，2礼包码，3实物礼品，4代金券
	 */
	private Integer type;
	/**
	 * 领取条件类型：1角色等级，2角色等级和创角时间，3角色累计充值，4角色首次充值，5角色单笔充值，6邀请好友成功注册游戏，7邀请好友首次提现成功，8邀请好友累计充值，9邀请好友创角N天内达到N级
	 */
	private Integer conditionType;
	/**
	 * 领取条件：角色等级
	 */
	private Integer conditionRoleLevel;
	/**
	 * 领取条件：创角天数
	 */
	private Integer conditionRoleDays;
	/**
	 * 领取条件：角色充值金额
	 */
	private BigDecimal conditionRoleRecharge;
	/**
	 * 红包数值
	 */
	private BigDecimal money;
	/**
	 * 使用门槛（0为不限制）
	 */
	private BigDecimal limitMoney;
	/**
	 * 现金价值
	 */
	private BigDecimal cashValues;
	/**
	 * 礼包总数量
	 */
	private Integer giftAmount;
	/**
	 * 礼包已领取数量
	 */
	private Integer giftGetcounts;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 用户可领数量
	 */
	private Integer useAmount;
	/**
	 * 角标：0无，1推荐
	 */
	private Integer angleMark;
	/**
	 * 详情状态：0关闭，1开启
	 */
	private Integer detailsStatus;
	/**
	 * 详情
	 */
	private String details;
	/**
	 * 状态：0停用，1启用
	 */
	private Integer status;
	/**
	 * 礼包码类型：1唯一码，2通用码
	 */
	private Integer giftType;
	/**
	 * 礼包通用码
	 */
	private String giftCode;
	/**
	 * 代金券有效期类型：1永久有效，2固定时间，3指定天数
	 */
	private Integer voucherType;
	/**
	 * 代金券领取后有效天数
	 */
	private Integer voucherDays;
	/**
	 * 代金券有效开始时间
	 */
	private Date voucherStartTime;
	/**
	 * 代金券有效结束时间
	 */
	private Date voucherEndTime;
	/**
	 * 代金券限制类型 1:无门槛 2:指定游戏 3:指定角色
	 */
	private Integer cdkLimitType;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 红包icon地址
	 */
	private String iconUrl;
	/**
	 * 红包总数是否显示：0不显示，1显示
	 */
	private String amountShow;
	/**
	 * 是否删除：0否 1是
	 */
	private Integer deleted;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 创建人
	 */
	private Long createId;
	/**
	 * 修改人
	 */
	private Long updateId;


	/**
	 * 余额类型 1，普通 2，临时
	 */
	@TableField(value = "currency_type")
	private Integer currencyType;

	/**
	 * 游豆
	 */
	@TableField(value = "currency_amount")
	private BigDecimal currencyAmount;

	//是否自动领取，1-是；2-否
	private Integer autoReceive;

	//游戏任务ID
	private Long conditionTaskId;

	/**
	 * 代金券类型  1 满减  2 折扣
	 */
	private Integer cdkType;
}


