package com.dy.yunying.api.datacenter.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * TODO
 *
 * @Author: hjl
 * @Date: 2020/8/8 11:08
 */
@Getter
@Setter
public class SonGameDto implements Serializable {

	private static final long serialVersionUID = -8445662897144500482L;
	/**
	 * 子游戏ID
	 */
	private Long gameId;

	/**
	 * 子游戏应用名称
	 */
	private String gameAppName;

	/**
	 * 子游戏包名
	 */
	private String packageName;

	/**
	 * 子游戏ICON路径
	 */
	private String iconPath;

	/**
	 * 子游戏反编译路径
	 */
	private String decodePath;

	/**
	 * 游戏LOGO地址
	 */
	private String gameLogo;

	/**
	 * 游戏登录图
	 */
	private String loginMap;

	/**
	 * 游戏LOGING图
	 */
	private String logingMap;
}