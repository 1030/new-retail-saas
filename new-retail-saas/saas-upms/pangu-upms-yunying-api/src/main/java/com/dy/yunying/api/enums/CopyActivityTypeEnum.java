package com.dy.yunying.api.enums;

/**
 * @description: 活动复制配置选项
 * @author yuwenfeng
 * @date 2022/1/11 16:17
 */
public enum CopyActivityTypeEnum {

	HB("HB","红包"),
	TXDC("TXDC","提现档次"),
	ZJ("ZJ","资金"),
	GG("GG","公告"),
	BD("BD","榜单"),
	TZPZ("TZPZ","通知配置");

	CopyActivityTypeEnum(String type, String desc){
		this.type =type;
		this.desc =desc;
	}

	private String type;

	private String desc;

	public String getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}
}
