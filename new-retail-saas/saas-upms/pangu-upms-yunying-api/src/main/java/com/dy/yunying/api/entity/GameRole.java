package com.dy.yunying.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 角色信息表
 * @author  chengang
 * @version  2021-10-28 13:35:09
 * table: game_role
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "game_role")
public class GameRole extends Model<GameRole>{

	//columns START
			//主键id
			@TableId(value = "id",type = IdType.AUTO)
			private Long id;

			@TableField(value = "user_id")
			private Long userId;

			//用户名
			@TableField(value = "username")
			private String username;
			

			//父游戏ID
			@TableField(value = "pgame_id")
			private Long pgameId;
			

			//子游戏ID
			@TableField(value = "game_id")
			private Long gameId;
			

			//服务器ID
			@TableField(value = "server_id")
			private Long serverId;
			

			//区服ID
			@TableField(value = "area_id")
			private Long areaId;
			

			//角色ID
			@TableField(value = "role_id")
			private String roleId;
			

			//角色等级
			@TableField(value = "role_level")
			private Integer roleLevel;
			

			//角色名称
			@TableField(value = "role_name")
			private String roleName;
			

			//创角时间
			@TableField(value = "create_role_time")
			private Date createRoleTime;
			

			//最后登录时间
			@TableField(value = "login_time")
			private Date loginTime;

			//最后登录时间
			@TableField(value = "login_ip")
			private String loginIp;
			

			//是否删除(0否 1是)
			@TableField(value = "deleted")
			private Integer deleted;
			

			//创建时间
			@TableField(value = "create_time")
			private Date createTime;
			

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;
			

			//创建人
			@TableField(value = "create_id")
			private Long createId;
			

			//修改人
			@TableField(value = "update_id")
			private Long updateId;
			

	//columns END 数据库字段结束
	

	
}

	

	
	

