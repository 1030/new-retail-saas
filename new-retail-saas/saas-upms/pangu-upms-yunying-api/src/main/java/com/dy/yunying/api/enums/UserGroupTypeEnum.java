package com.dy.yunying.api.enums;

public enum UserGroupTypeEnum {

	RULE(1, "规则"),
	ID_FILTER(2, "ID筛选");

	private Integer type;
	private String name;
	public Integer getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private UserGroupTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static UserGroupTypeEnum getOneByType(Integer type){
		for(UserGroupTypeEnum typeEnum: UserGroupTypeEnum.values()){
			if(typeEnum.getType() == type){
				return typeEnum;
			}
		}
		return null;
	}
}
