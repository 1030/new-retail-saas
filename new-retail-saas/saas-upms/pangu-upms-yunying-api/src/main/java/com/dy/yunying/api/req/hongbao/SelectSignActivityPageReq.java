package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 活动列表入参对象
 * @date 2021/10/23 11:04
 */
@Data
public class SelectSignActivityPageReq implements Serializable {

	@ApiModelProperty(value = "父游戏ID")
	private Long parentGameId;

	@ApiModelProperty(value = "活动名称")
	private String activityName;

	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	private String finishTime;

	@ApiModelProperty(value = "活动状态(1待上线 2活动中 3已下线)")
	private Integer activityStatus;

}
