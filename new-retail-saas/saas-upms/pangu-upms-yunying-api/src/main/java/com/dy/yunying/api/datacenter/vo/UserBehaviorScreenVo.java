package com.dy.yunying.api.datacenter.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ：leisw
 * @date ：2022/8/19 11:07
 * @description：
 * @modified By：
 */
@Data
public class UserBehaviorScreenVo {

	//用户行为
	private String userBehavior;

	//手机号
	private String userBehaviorName;

}
