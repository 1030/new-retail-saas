package com.dy.yunying.api.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * cms配置
 * wan_cms_config
 * @author kongyanfang
 * @date 2020-07-21 19:11:40
 */
@Getter
@Setter
public class WanCmsConfigDO {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 改变状态 0未改变, 1更新栏目，2更新页面
     */
    private String cfgstatus;

    /**
     * 状态 :1 可用 0 删除
     */
    private Short status;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 创建者id
     */
    private Integer createuser;

    /**
     * 更新时间
     */
    private Date updatetime;

    /**
     * 栏目类型（index-首页，games-游戏，news-新闻，activity-活动，gift-礼包，service-客服,reg-注册）
     */
    private String cfgtype;

    /**
     * 类型id(数据id)
     */
    private Integer typeid;

    /**
     * 网页title
     */
    private String title;

    /**
     * 网页keywords
     */
    private String keywords;

    /**
     * 网页描述
     */
    private String description;
}