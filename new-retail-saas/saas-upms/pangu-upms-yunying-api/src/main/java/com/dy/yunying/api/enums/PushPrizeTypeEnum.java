package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * class info
 *
 * @author sunyouquan
 * @date 2022/5/6 10:28
 */
public enum PushPrizeTypeEnum {

	SINGLE(1, "单个奖励"),

	MUTI(2, "组合奖励");

	private Integer type;

	private String name;

	PushPrizeTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonCreator
	public static PushPrizeTypeEnum getPushPrizeTypeEnum(Integer value) {
		for (PushPrizeTypeEnum pushPrizeTypeEnum : values()) {
			if (pushPrizeTypeEnum.type.equals(value)) {
				return pushPrizeTypeEnum;
			}
		}
		return null;
	}

	@JsonValue
	public Integer getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}

}
