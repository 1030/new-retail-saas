package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 渠道包请求
 *
 * @Author: hjl
 * @Date: 2020/6/11 20:59
 */
@Getter
@Setter
public class WanChannelPackReq extends Page implements Serializable {

	/**
	 * 分包号起始
	 */
	private Long packNumFrom;

	/**
	 * 分包号终止
	 */
	private Long packNumTo;

	private String stime; //查询时间(开始)

	private String etime; //查询时间（结束）

	/**
	 * 分包渠道ID
	 */
	private Long packId;

	/**
	 * 游戏ID
	 */
	private Integer gameId;

	/**
	 * 游戏名称
	 */
	private String gameName;

	/**
	 * 游戏包名
	 */
	private String pkName;

	/**
	 *
	 */
	private Long versionId;

	/**
	 * 分包编码（游戏编码）
	 */
	private String code;

	/**
	 * 所属渠道编码（子渠道）
	 */
	private String chlCode;


	//主渠道
	private String parentCode;
	//主渠道ID
	private String pid;

	/**
	 * 游戏包路径
	 */
	private String path;

	/**
	 * 状态：1：生成中，2：已生成；3：失效
	 */
	private Short status;

	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 分包用户id
	 */
	private String packUserid;

	/**
	 * 创建人名称
	 */
	private String creatorName;
	/**
	 * 筛选分包
	 */
	private String isPack;
	/**
	 * 平台ID
	 */
	private String platformId;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	private List<Integer> userList;

	/**
	 * 推广类型（1信息流 2短视频）
	 */
	private Integer spreadType;

	/**
	 * 结算类型(1KOL(一口价) 2KOL(CPM按次))
	 */
	private String settleType;

	@ApiModelProperty("分包渠道")
	private String[] appchlArr;

	private List<String> appchlList;

	/**
	 * 母包类型：1-运营母包；2-云微端；
	 */
	private Integer packType;
}