package com.dy.yunying.api.constant;

public class DistributedLock {

	public static final int TIMEOUT_ONE_SEC = 1;

	public static final int TIMEOUT_THREE_SEC = 3;

	public static final int TIMEOUT_FIVE_SEC = 5;

	public static final int TIMEOUT_TEN_SEC = 10;

	public static final int TIMEOUT_FIFTEEN_SEC = 15;

	public static final int TIMEOUT_TWENTY_SEC = 20;

	public static final int TIMEOUT_THIRTY_SEC = 30;

	public static final int TIMEOUT_SIXTY_SEC = 60;

	public static final String UPSERT_CURRENCY_RECHARGE_CONFIG_KEY = "distributed:lock:upsert_currency_recharge_config";

	/**
	 * 用户平台币（游豆）变动分布式锁
	 * KEY: USER_CURRENCY_KEY_PREFIX + userId
	 */
	public static final String USER_CURRENCY_KEY_PREFIX = "distributed:lock:user_currency:userid:";
	/**
	 * 素材推送分布式锁key
	 * key :
	 */
	public static final String AD_MATERIAL_PUSH_LOCK_PRIX = "distributed:lock:ad_material_push_lock_prix";


	/**
	 *
	 * 提现记录审核分布式锁，放置重复提交
	 */
	public static final String WITHDRAW_RECORD_ID_KEY_PREFIX = "distributed:lock:withdraw_record:id:";

}
