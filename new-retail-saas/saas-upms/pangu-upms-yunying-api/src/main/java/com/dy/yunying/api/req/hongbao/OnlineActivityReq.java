package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 活动上下线
 * @date 2021/10/23 11:04
 */
@Data
public class OnlineActivityReq implements Serializable {

	@ApiModelProperty(value = "活动ID")
	@NotNull(message = "活动ID不能为空")
	private Long id;

	@ApiModelProperty(value = "活动状态(2上线 3下线)")
	@NotNull(message = "活动状态不能为空")
	private Integer activityStatus;

}
