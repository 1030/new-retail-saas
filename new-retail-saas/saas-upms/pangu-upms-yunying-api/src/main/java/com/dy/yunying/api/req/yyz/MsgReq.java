package com.dy.yunying.api.req.yyz;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString(callSuper = true)
public class MsgReq implements Serializable {

	private static final long serialVersionUID = 2475374455806708599L;

	/***
	 *   手机号
	 */
	private String mobile;

	/***
	 * "预约类型：1幻兽仓角预约  2 烛光勇士预约"
	 */
	private Integer type;

	/***
	 * 预约来源（1：PC  2：H5）
	 */
	private Integer codeType;

	/***
	 * 图形验证码
	 */
	private String captcha;

	/***
	 * 随机码证码
	 */
	private String random;


}
