package com.dy.yunying.api.resp.hongbao;

import com.dy.yunying.api.entity.hongbao.HbInvestRecord;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yuwenfeng
 * @description: 注资记录
 * @date 2021/11/5 14:03
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class InvestRecordVo extends HbInvestRecord {
	@ApiModelProperty(value = "申请人")
	private String applyUserName;
}
