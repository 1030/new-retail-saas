package com.dy.yunying.api.req.sign;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 通知公告新增/编辑对象
 * @date 2021/12/1 18:24
 */
@Data
public class CommonNoticeReq implements Serializable {

	@ApiModelProperty(value = "公告ID")
	private Long id;

	@ApiModelProperty(value = "通知弹窗内容,图片的话就是url")
	private String popupContent;

	@NotNull(message = "跳转方式不能为空")
	@ApiModelProperty(value = "跳转方式(1SDK面板 2内置浏览器 3外部浏览器 4不跳转)")
	private Integer openType;

	@ApiModelProperty(value = "展示方式(1横屏展示，2竖屏展示)")
	private Integer showType;

	@ApiModelProperty(value = "跳转链接")
	private String imgUrl;

	@ApiModelProperty(value = "SDK菜单")
	private String menuTitle;

	@ApiModelProperty(value = "红包类型")
	private Integer sourceBelongType;

	@ApiModelProperty(value = "来源类型(1红包活动 2签到活动)")
	private Integer sourceMold;

	@ApiModelProperty(value = "活动id")
	private Long sourceId;

	@NotNull(message = "关闭方式不能为空")
	@ApiModelProperty(value = "关闭方式(1手动关闭 2倒计时关闭)")
	private Integer closeType;

	@ApiModelProperty(value = "关闭倒计时(秒)")
	private Integer closeTimes;

	@ApiModelProperty(value = "公告标题")
	private String popupTitle;
	@ApiModelProperty(value = "公告内容")
	private String popupMessage;

	@NotNull(message = "公告形式不能为空")
	@ApiModelProperty(value = "公告形式(1弹窗图 2全屏图 3图文 4通栏消息)")
	private Integer popupMold;

	@NotNull(message = "公告屏幕方向不能为空")
	@ApiModelProperty(value = "公告屏幕方向(1横屏 2竖屏)")
	private Integer popupOrient;

	@NotNull(message = "推送频次不能为空")
	@Range(min = 1, max = 4, message = "请提供正确的推送频次值")
	@ApiModelProperty(value = "推送频次: 1-每天; 2-隔天; 3-每周; 4-仅一次;")
	private Integer pushFrequency;

	@ApiModelProperty(value = "每天推送间隔时为天数；或者每周推送间隔时: 1 ~ 7 -> 周一 ~ 周日，使用逗号分隔")
	private String pushInterval;

//	@Range(min = 1, max = 4, message = "请提供正确的推送节点值")
	@ApiModelProperty(value = "推送节点: 1-首次登录成功; 2-每次登录成功; 3-首次进入游戏; 4-每次进入游戏;")
	private Integer pushNode;

	@ApiModelProperty(value = "用户群组ID，多个使用逗号分隔")
	private String userGroup;

	@NotBlank(message = "开始时间不能为空")
	@ApiModelProperty(value = "开始时间(格式：yyyy-MM-dd HH:mm:ss)")
	private String popupStartTime;

	@NotBlank(message = "结束时间不能为空")
	@ApiModelProperty(value = "结束时间(格式：yyyy-MM-dd HH:mm:ss)")
	private String popupEndTime;

	@NotNull(message = "渠道范围集合不能为空")
	@ApiModelProperty(value = "渠道范围集合")
	private List<String[]> channelList;

	@NotNull(message = "游戏范围集合不能为空")
	@ApiModelProperty(value = "游戏范围集合")
	private List<String[]> gameList;

	@NotNull(message = "区服范围集合不能为空")
	@ApiModelProperty(value = "区服范围集合")
	private List<String[]> areaList;

}
