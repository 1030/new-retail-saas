package com.dy.yunying.api.req.currency;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @ClassName UserCurrencyReq
 * @Description
 * @Author hejiale
 * @Time 2022-3-23 16:39:17
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class UserCurrencyReq extends Page {

	/**
	 * 父游戏ID，0：通用
	 */
	@ApiModelProperty("父游戏ID")
	private Long pgid;

	/**
	 * 子游戏ID，0：通用
	 */
	@ApiModelProperty("子游戏ID")
	private Long gameId;

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

}
