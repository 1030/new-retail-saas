package com.dy.yunying.api.entity.prize;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 奖励与渠道关系表
 *
 * @author chenxiang
 * @version 2022-04-26 10:20:48 table: prize_push_channel
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "prize_push_channel")
public class PrizePushChannel extends Model<PrizePushChannel> {

	/**
	 * 主键ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 奖励推送ID
	 */
	@TableField(value = "prize_push_id")
	private Long prizePushId;

	/**
	 * 主渠道编码
	 */
	@TableField(value = "parent_chl")
	private String parentChl;

	/**
	 * 子渠道编码
	 */
	@TableField(value = "chl")
	private String chl;

	/**
	 * 分包渠道编码
	 */
	@TableField(value = "app_chl")
	private String appChl;

	/**
	 * 渠道层级(1主渠道 2子渠道 3分包渠道)
	 */
	@TableField(value = "channel_range")
	private Integer channelRange;

	/**
	 * 是否删除(0否 1是)
	 */
	@ApiModelProperty(value = "是否删除(0否 1是)")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建者
	 */
	@TableField(fill = FieldFill.INSERT)
	private Long createId;

	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 更新者
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updateId;

	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}
