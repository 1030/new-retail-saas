package com.dy.yunying.api.datacenter.vo;

import lombok.Data;

/**
 * @author ：lile
 * @date ：2021/9/14 14:08
 * @description：
 * @modified By：
 */
@Data
public class AdAgentAccountVo {
	private String advertiserId;
}
