package com.dy.yunying.api.dto.currency;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pig4cloud.pig.common.core.jackson.BigDecimalSerializer;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * 游豆充值订单
 *
 * @author zhuxm
 * @version 2022-01-13 16:02:25
 * table: wan_recharge_order
 */
@Data
@Accessors(chain = true)
public class CurrencyOrderDTO implements Serializable {

	/**
	 * 主键
	 */
	private Long id;

	/**
	 * 充值账号
	 */
	private String username;

	/**
	 * 充值ip
	 */
	private String ip;

	/**
	 * 省份名称
	 */
	private String provinceName;

	/**
	 * 城市名称
	 */
	private String cityName;

	/**
	 * 订单编号
	 */
	private String orderno;

	/**
	 * 第三方交易流水号
	 */
	private String tradeno;

	/**
	 * 注册时间，格式：2022-03-30 12:20:32
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date registerTime;

	/**
	 * 充值时间，格式：2022-03-30 12:20:32
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date rechargeTime;

	/**
	 * 金额
	 */
	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal rechargeAmount;

	/**
	 * 游豆
	 */
	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal currencyAmount;

	/**
	 * 充值状态：2-成功；127-失败；
	 */
	private Integer rechargeStatus;

	/**
	 * 充值状态
	 */
	private String rechargeStatusStr;

	/**
	 * 充值方式：1-支付宝；2-微信；
	 */
	private Integer rechargeType;

	/**
	 * 充值方式
	 */
	private String rechargeTypeStr;

	/**
	 * 到账状态：1-已到账；127-未到账；
	 */
	private Integer shippingStatus;

	/**
	 * 到账状态
	 */
	private String shippingStatusStr;

	/**
	 * 主游戏ID
	 */
	private Long pgid;

	/**
	 * 主游戏名称
	 */
	private String pgname;

	/**
	 * 子游戏ID
	 */
	private Long gameid;

	/**
	 * 子游戏名称
	 */
	private String gname;

	/**
	 * 区服ID
	 */
	private String areaId;

	/**
	 * 角色ID
	 */
	private String roleId;

	/**
	 * 角色名称
	 */
	private String roleName;

	// 查询条件字段

	/**
	 * 充值开始时间，格式：2022-03-30 12:21:32
	 */
	private Date rechargeStartTime;

	/**
	 * 充值结束时间，格式：2022-03-30 12:21:32
	 */
	private Date rechargeEndTime;

	/**
	 * 注册开始时间，格式：2022-03-30 12:21:32
	 */
	private Date registerStartTime;

	/**
	 * 注册结束时间，格式：2022-03-30 12:21:32
	 */
	private Date registerEndTime;

	/**
	 * 主游戏ID
	 */
	private Collection<Long> pgids;

	/**
	 * 子游戏ID
	 */
	private Collection<Long> gameids;

	public Integer getRechargeStatus() {
		if (null == rechargeStatus) {
			return null;
		} else if (2 != rechargeStatus) {
			return 127;
		}
		return rechargeStatus;
	}

	public String getRechargeStatusStr() {
		Integer rechargeStatus = this.getRechargeStatus();
		if (null == rechargeStatus) {
			rechargeStatusStr = "-";
		} else if (2 == rechargeStatus) {
			rechargeStatusStr = "已支付";
		} else if (127 == rechargeStatus) {
			rechargeStatusStr = "未支付";
		} else {
			rechargeStatusStr = "-";
		}
		return rechargeStatusStr;
	}

	public String getRechargeTypeStr() {
		if (null == rechargeType) {
			rechargeTypeStr = "-";
		} else if (14 == rechargeType) {
			rechargeTypeStr = "支付宝";
		} else if (15 == rechargeType) {
			rechargeTypeStr = "微信";
		} else {
			rechargeTypeStr = "-";
		}
		return rechargeTypeStr;
	}

	public Integer getShippingStatus() {
		if (null == shippingStatus) {
			return null;
		} else if (1 != shippingStatus) {
			return 127;
		}
		return shippingStatus;
	}

	public String getShippingStatusStr() {
		if (null == shippingStatus) {
			shippingStatusStr = "-";
		} else if (1 == shippingStatus) {
			shippingStatusStr = "已到账";
		} else if (127 == shippingStatus) {
			shippingStatusStr = "未到账";
		} else {
			shippingStatusStr = "-";
		}
		return shippingStatusStr;
	}

}

	

	
	

