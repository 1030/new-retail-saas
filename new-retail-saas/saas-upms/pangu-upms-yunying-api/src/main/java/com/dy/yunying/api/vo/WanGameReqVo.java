package com.dy.yunying.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.constant.VersionStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * WanGamePo Po
 *
 * @author system
 */

public class WanGameReqVo implements Serializable {
	/**
	 * @author cx
	 * @date 2020年7月24日12:20:25
	 */
	private static final long serialVersionUID = 7980528629348181946L;
	/**
	 * 游戏ID
	 */
	private BigInteger id;
	/**
	 * 父游戏id
	 */
	private BigInteger pgid;
	/**
	 * 父游戏名称
	 */
	private String pgname;
	/**
	 * 游戏名称
	 */
	private String gname;
	/**
	 * 游戏类型
	 */
	private Integer gtype;
	/**
	 * 游戏类型
	 */
	private String gtypes;
	/**
	 * 游戏题材
	 */
	private Integer gtheme;
	/**
	 * 游戏题材
	 */
	private String gthemes;
	/**
	 * 游戏战斗模式
	 */
	private Integer gpattern;
	/**
	 * 首字母
	 */
	private String firstletter;
	/**
	 * 游戏特征
	 */
	private String features;
	/**
	 * 全部游戏(80*80)
	 */
	private String icon;
	/**
	 * 首页热门/游戏大厅(250*134)
	 */
	private String picture;
	/**
	 * 推荐展示图片(218*250)
	 */
	private String rcmdimg;
	/**
	 * 首页顶部游戏(400*262)
	 */
	private String indextopimg;
	/**
	 * 游戏大厅(顶部)(480*310)
	 */
	private String halltopimg;
	/**
	 * 栏目热门(95*68)
	 */
	private String hotimg;
	/**
	 * 游戏大厅预览图 88*82
	 */
	private String preimg;
	/**
	 * 官方网站链接
	 */
	private String officialurl;
	/**
	 * 开始游戏链接
	 */
	private String playurl;
	/**
	 * flash小视频链接
	 */
	private String flashurl;
	/**
	 * 论坛地址
	 */
	private String forumurl;
	/**
	 * 礼包地址
	 */
	private String bagurl;
	/**
	 * 游戏汇率
	 */
	private String rate;
	/**
	 * 分成比例
	 */
	private String ratio;
	/**
	 * 游戏渠道ID
	 */
	private BigInteger gcid;
	/**
	 * 点赞次数
	 */
	private BigInteger likes;
	/**
	 * 评分
	 */
	private BigDecimal score;
	/**
	 * 默认评论次数
	 */
	private BigInteger comments;
	/**
	 * 实际评论次数
	 */
	private BigInteger realcomments;
	/**
	 * 开区数量
	 */
	private BigInteger opennum;
	/**
	 * 注册用户数
	 */
	private BigInteger registernum;
	/**
	 * 当前在线人数
	 */
	private BigInteger onlinenum;
	/**
	 * 充值金额
	 */
	private BigDecimal recharge;
	/**
	 * 状态 30：正常，40：停运
	 */
//	@DataStatus("{'10':'推荐','20':'热门','30':'正常','40':'停运'}")
	private Integer status;
	/**
	 * 是否热门 0:否  1:是
	 */
	private Integer ishot;
	/**
	 * 是否推荐  0:否  1:是
	 */
	private Integer isrecommend;
	/**
	 * 游戏币单位
	 */
	private String unit;
	/**
	 * 游戏简介
	 */
	private String remark;
	/**
	 * 是否删除0、否，1、是
	 */
	private Integer isdelete;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createtime;
	/**
	 * 更新时间
	 */
	private Date updatetime;

	/**
	 * 充值积分兑换比例
	 */
	private Integer integralrate;

	/**
	 * 导量区服id
	 */
	private BigInteger guideaid;

	/**
	 * 导量区服名称
	 */
	private String guideaname;

	//微信公众平台礼包置顶游戏设置 0-否，1-是
	private int giftbagIstop;

	//终端类型,0：pc网页端，1：h5, 2:手游andr， 3-手游iPhone
	private Integer terminaltype;
	//终端类型,0：pc网页端，1：h5
	private Integer h5order;
	//终端类型,0：pc网页端，1：h5
	private String qrcodeimg;
	//H5游戏列表排序
	private Integer h5listorder;
	//大小
	private Double size;
	//下载地址
	private String downloadurl;
	//英文名称
	private String enname;

	//详情横幅图片
	private String bannerimg;

	private String shareurl;//分享url
	//seo信息
	private String seo_keywords;
	private String seo_description;
	/**
	 * IOS bundleid
	 */
	private String bundleid;
	/**
	 * 充值开关 1开，2关
	 */
	private String isrecharge;
	/**
	 * 包名
	 */
	private String pk_name;
	/**
	 * 充值回调
	 */
	private String exchange_url;
	/**
	 * 是否展示到网站
	 */
	private int display;
	/**
	 * 游戏描述
	 *
	 * @return
	 */
	private String description;
	/**
	 * 图片路径
	 *
	 * @return
	 */
	private String imgpath;
	/**
	 * appicon路径
	 *
	 * @return
	 */
	private String appiconpath;
	/**
	 * 游戏详情图片（多图）
	 */
	private String detailsimg;
	/**
	 * 域名
	 */
	private String domain;
	/**
	 * APP包版本id
	 */
	private BigInteger version_id;
	/**
	 * APP包版本code
	 */
	private Integer versioncode;
	/**
	 * APP包版本name
	 */
	private String versionname;

	/***
	 * APP包版本更新时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date versiontime;

	/**
	 * app应用名称
	 */
	private String app_name;
	/**
	 * app游戏图标
	 */
	private String app_icon;
	/**
	 * 子游戏版本状态
	 */
	private Integer sgv_status;
	/**
	 * 父游戏版本名称
	 */
	private String pgv_name;

	/**
	 * 父游戏版本code
	 */
	private Integer pgv_code;

	/**
	 * 是否修改
	 */
	private Integer is_update;

	/**
	 * 旧的app包
	 */
	private String oldPkName;

	/**
	 * 游戏LOGO地址
	 */
	private String gameLogo;

	/**
	 * 游戏登录图
	 */
	private String loginMap;

	/**
	 * 游戏LOGING图
	 */
	private String logingMap;

	private List<Map<String, String>> parentGameVersionList;
	private List<Map<String, String>> sonGameVersionList;

	public List<Map<String, String>> getParentGameVersionList() {
		return parentGameVersionList;
	}

	public void setParentGameVersionList(List<Map<String, String>> parentGameVersionList) {
		this.parentGameVersionList = parentGameVersionList;
	}

	public List<Map<String, String>> getSonGameVersionList() {
		return sonGameVersionList;
	}

	public void setSonGameVersionList(List<Map<String, String>> sonGameVersionList) {
		this.sonGameVersionList = sonGameVersionList;
	}

	public String getOldPkName() {
		return oldPkName;
	}

	public void setOldPkName(String oldPkName) {
		this.oldPkName = oldPkName;
	}

	public String getParentVersionStr() {
		String parentVersionStr = null;
		if (pgv_code != null && StringUtils.isNotBlank(pgv_name)) {
			parentVersionStr = pgv_name + "【" + pgv_code + "】";
		}
		return parentVersionStr;
	}

	public String getVersionStr() {
		String versionStr = null;
		if (versioncode != null && StringUtils.isNotBlank(versionname)) {
			versionStr = versionname + "【" + versioncode + "】";
		}
		return versionStr;
	}

	public String getVersionStatusStr() {
		String versionStatusStr = "";
		if (sgv_status != null) {
			if (pgv_code != null && sgv_status == VersionStatusEnum.VALID.getStatus() && versioncode < pgv_code) {
				versionStatusStr = "版本过低";
			} else {
				versionStatusStr = VersionStatusEnum.getName(sgv_status);
			}
		}
		return versionStatusStr;
	}

	public Integer getIs_update() {
		return is_update;
	}

	public void setIs_update(Integer is_update) {
		this.is_update = is_update;
	}

	public Integer getSgv_status() {
		return sgv_status;
	}

	public void setSgv_status(Integer sgv_status) {
		this.sgv_status = sgv_status;
	}

	public String getPgv_name() {
		return pgv_name;
	}

	public void setPgv_name(String pgv_name) {
		this.pgv_name = pgv_name;
	}

	public String getAppiconpath() {
		return appiconpath;
	}

	public void setAppiconpath(String appiconpath) {
		this.appiconpath = appiconpath;
	}

	public String getApp_name() {
		return app_name;
	}

	public void setApp_name(String app_name) {
		this.app_name = app_name;
	}

	public String getApp_icon() {
		return app_icon;
	}

	public void setApp_icon(String app_icon) {
		this.app_icon = app_icon;
	}

	public BigInteger getVersion_id() {
		return version_id;
	}

	public void setVersion_id(BigInteger version_id) {
		this.version_id = version_id;
	}

	public Integer getVersioncode() {
		return versioncode;
	}

	public void setVersioncode(Integer versioncode) {
		this.versioncode = versioncode;
	}

	public String getVersionname() {
		return versionname;
	}

	public void setVersionname(String versionname) {
		this.versionname = versionname;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getDetailsimg() {
		return detailsimg;
	}

	public void setDetailsimg(String detailsimg) {
		this.detailsimg = detailsimg;
	}

	public String getImgpath() {
		return imgpath;
	}

	public void setImgpath(String imgpath) {
		this.imgpath = imgpath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	public String getExchange_url() {
		return exchange_url;
	}

	public void setExchange_url(String exchange_url) {
		this.exchange_url = exchange_url;
	}

	public String getPk_name() {
		return pk_name;
	}

	public void setPk_name(String pk_name) {
		this.pk_name = pk_name;
	}

	public BigInteger getPgid() {
		return pgid;
	}

	public void setPgid(BigInteger pgid) {
		this.pgid = pgid;
	}

	public String getIsrecharge() {
		return isrecharge;
	}

	public void setIsrecharge(String isrecharge) {
		this.isrecharge = isrecharge;
	}

	public String getBundleid() {
		return bundleid;
	}

	public void setBundleid(String bundleid) {
		this.bundleid = bundleid;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getGname() {
		return gname;
	}

	public void setGname(String gname) {
		this.gname = gname;
	}

	public Integer getGtype() {
		return gtype;
	}

	public void setGtype(Integer gtype) {
		this.gtype = gtype;
	}

	public Integer getGtheme() {
		return gtheme;
	}

	public void setGtheme(Integer gtheme) {
		this.gtheme = gtheme;
	}

	public Integer getGpattern() {
		return gpattern;
	}

	public void setGpattern(Integer gpattern) {
		this.gpattern = gpattern;
	}

	public String getFirstletter() {
		return firstletter;
	}

	public void setFirstletter(String firstletter) {
		this.firstletter = firstletter;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getOfficialurl() {
		return officialurl;
	}

	public void setOfficialurl(String officialurl) {
		this.officialurl = officialurl;
	}

	public String getPlayurl() {
		return playurl;
	}

	public void setPlayurl(String playurl) {
		this.playurl = playurl;
	}

	public String getFlashurl() {
		return flashurl;
	}

	public void setFlashurl(String flashurl) {
		this.flashurl = flashurl;
	}

	public String getForumurl() {
		return forumurl;
	}

	public void setForumurl(String forumurl) {
		this.forumurl = forumurl;
	}

	public String getBagurl() {
		return bagurl;
	}

	public void setBagurl(String bagurl) {
		this.bagurl = bagurl;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRatio() {
		return ratio;
	}

	public void setRatio(String ratio) {
		this.ratio = ratio;
	}

	public BigInteger getGcid() {
		return gcid;
	}

	public void setGcid(BigInteger gcid) {
		this.gcid = gcid;
	}

	public BigInteger getLikes() {
		return likes;
	}

	public void setLikes(BigInteger likes) {
		this.likes = likes;
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public BigInteger getComments() {
		return comments;
	}

	public void setComments(BigInteger comments) {
		this.comments = comments;
	}

	public BigInteger getOpennum() {
		return opennum;
	}

	public void setOpennum(BigInteger opennum) {
		this.opennum = opennum;
	}

	public BigInteger getRegisternum() {
		return registernum;
	}

	public void setRegisternum(BigInteger registernum) {
		this.registernum = registernum;
	}

	public BigInteger getOnlinenum() {
		return onlinenum;
	}

	public void setOnlinenum(BigInteger onlinenum) {
		this.onlinenum = onlinenum;
	}

	public BigDecimal getRecharge() {
		return recharge;
	}

	public void setRecharge(BigDecimal recharge) {
		this.recharge = recharge;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIshot() {
		return ishot;
	}

	public void setIshot(Integer ishot) {
		this.ishot = ishot;
	}

	public Integer getIsrecommend() {
		return isrecommend;
	}

	public void setIsrecommend(Integer isrecommend) {
		this.isrecommend = isrecommend;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getRcmdimg() {
		return rcmdimg;
	}

	public void setRcmdimg(String rcmdimg) {
		this.rcmdimg = rcmdimg;
	}

	public String getIndextopimg() {
		return indextopimg;
	}

	public void setIndextopimg(String indextopimg) {
		this.indextopimg = indextopimg;
	}

	public String getHalltopimg() {
		return halltopimg;
	}

	public void setHalltopimg(String halltopimg) {
		this.halltopimg = halltopimg;
	}

	public String getHotimg() {
		return hotimg;
	}

	public void setHotimg(String hotimg) {
		this.hotimg = hotimg;
	}

	public Integer getIntegralrate() {
		return integralrate;
	}

	public void setIntegralrate(Integer integralrate) {
		this.integralrate = integralrate;
	}

	public BigInteger getRealcomments() {
		return realcomments;
	}

	public void setRealcomments(BigInteger realcomments) {
		this.realcomments = realcomments;
	}

	public String getPreimg() {
		return preimg;
	}

	public void setPreimg(String preimg) {
		this.preimg = preimg;
	}

	public BigInteger getGuideaid() {
		return guideaid;
	}

	public void setGuideaid(BigInteger guideaid) {
		this.guideaid = guideaid;
	}

	public String getGuideaname() {
		return guideaname;
	}

	public void setGuideaname(String guideaname) {
		this.guideaname = guideaname;
	}

	public int getGiftbagIstop() {
		return giftbagIstop;
	}

	public void setGiftbagIstop(int giftbagIstop) {
		this.giftbagIstop = giftbagIstop;
	}

	public Integer getTerminaltype() {
		return terminaltype;
	}

	public void setTerminaltype(Integer terminaltype) {
		this.terminaltype = terminaltype;
	}

	public Integer getH5order() {
		return h5order;
	}

	public void setH5order(Integer h5order) {
		this.h5order = h5order;
	}

	public String getQrcodeimg() {
		return qrcodeimg;
	}

	public void setQrcodeimg(String qrcodeimg) {
		this.qrcodeimg = qrcodeimg;
	}

	public Integer getH5listorder() {
		return h5listorder;
	}

	public void setH5listorder(Integer h5listorder) {
		this.h5listorder = h5listorder;
	}

	public String getDownloadurl() {
		return downloadurl;
	}

	public void setDownloadurl(String downloadurl) {
		this.downloadurl = downloadurl;
	}

	public String getShareurl() {
		return shareurl;
	}

	public void setShareurl(String shareurl) {
		this.shareurl = shareurl;
	}

	public String getEnname() {
		return enname;
	}

	public void setEnname(String enname) {
		this.enname = enname;
	}

	public String getBannerimg() {
		return bannerimg;
	}

	public void setBannerimg(String bannerimg) {
		this.bannerimg = bannerimg;
	}

	public String getSeo_keywords() {
		return seo_keywords;
	}

	public void setSeo_keywords(String seo_keywords) {
		this.seo_keywords = seo_keywords;
	}

	public String getSeo_description() {
		return seo_description;
	}

	public void setSeo_description(String seo_description) {
		this.seo_description = seo_description;
	}

	public String getGtypes() {
		return gtypes;
	}

	public void setGtypes(String gtypes) {
		this.gtypes = gtypes;
	}

	public String getGthemes() {
		return gthemes;
	}

	public void setGthemes(String gthemes) {
		this.gthemes = gthemes;
	}

	public String getPgname() {
		return pgname;
	}

	public void setPgname(String pgname) {
		this.pgname = pgname;
	}

	public Integer getPgv_code() {
		return pgv_code;
	}

	public void setPgv_code(Integer pgv_code) {
		this.pgv_code = pgv_code;
	}

	public Date getVersiontime() {
		return versiontime;
	}

	public void setVersiontime(Date versiontime) {
		this.versiontime = versiontime;
	}

	public String getGameLogo() {
		return gameLogo;
	}

	public void setGameLogo(String gameLogo) {
		this.gameLogo = gameLogo;
	}

	public void setSize(Double size) {
		this.size = size;
	}

	public Double getSize() {
		return size;
	}

	public String getLoginMap() {
		return loginMap;
	}

	public void setLoginMap(String loginMap) {
		this.loginMap = loginMap;
	}

	public String getLogingMap() {
		return logingMap;
	}

	public void setLogingMap(String logingMap) {
		this.logingMap = logingMap;
	}
}