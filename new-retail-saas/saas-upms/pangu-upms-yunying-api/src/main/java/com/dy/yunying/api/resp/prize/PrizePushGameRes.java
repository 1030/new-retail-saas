package com.dy.yunying.api.resp.prize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 奖励与游戏关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:59
 * table: prize_push_game
 */
@Data
public class PrizePushGameRes implements Serializable {

	@ApiModelProperty(value = "主键ID")
	private Long id;

	@ApiModelProperty(value = "奖励推送ID")
	private Long prizePushId;

	@ApiModelProperty(value = "主游戏ID")
	private Long parentGameId;

	@ApiModelProperty(value = "子游戏ID")
	private Long childGameId;

	@ApiModelProperty(value = "游戏层级(1主游戏 2子游戏)")
	private Integer gameRange;
}


