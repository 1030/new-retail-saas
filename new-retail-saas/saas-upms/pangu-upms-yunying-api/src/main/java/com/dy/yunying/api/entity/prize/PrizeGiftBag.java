package com.dy.yunying.api.entity.prize;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 奖品礼包码
 *
 * @author chenxiang
 * @version 2022-04-25 16:25:54 table: prize_gift_bag
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "prize_gift_bag")
public class PrizeGiftBag extends Model<PrizeGiftBag> {

	/**
	 * 主键ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 类型：1礼包，2自定义
	 */
	@TableField(value = "source_type")
	private Integer sourceType;

	/**
	 * 来源关联ID。如：奖品ID
	 */
	@TableField(value = "source_id")
	private Long sourceId;

	/**
	 * 礼包码
	 */
	@TableField(value = "gift_code")
	private String giftCode;

	/**
	 * 礼包码类型：1：唯一码，2：通用码
	 */
	@TableField(value = "usable")
	private Integer usable;

	/**
	 * 状态：0可用，1已领取
	 */
	@TableField(value = "status")
	private Integer status;

	/**
	 * 领取时间
	 */
	@TableField(value = "receive_time")
	private Date receiveTime;

	/**
	 * 礼包码总数量
	 */
	@TableField(value = "gift_amount")
	private Long giftAmount;

	/**
	 * 已领取数量
	 */
	@TableField(value = "gift_getcounts")
	private Long giftGetcounts;

	/**
	 * 是否删除：0否 1是
	 */
	@TableLogic
	@TableField(value = "deleted")
	private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

}
