package com.dy.yunying.api.enums;

/**
 * @author sunyq
 * @date 2022/8/30 10:45
 */
public enum ProhibitTypeEnum {
	//1：账号；2：IP；3：OAID；4：IMEI
	ACCOUNT(1,"账号"),
	IP(2,"IP"),
	OAID(3,"OAID"),
	IMEI(4,"IMEI");

	ProhibitTypeEnum(Integer type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	private Integer type;

	private String desc;

	public Integer getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

	public static ProhibitTypeEnum getProhibitTypeEnum(Integer type) {
		for (ProhibitTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele;
			}
		}
		return null;
	}

}
