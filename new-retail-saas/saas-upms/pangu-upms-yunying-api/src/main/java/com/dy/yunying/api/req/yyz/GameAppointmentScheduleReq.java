package com.dy.yunying.api.req.yyz;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import com.pig4cloud.pig.common.core.mybatis.Page;
import java.util.Date;

/**
 * 游戏预约进度表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_schedule
 */
@Data
public class GameAppointmentScheduleReq{

	/***
	 *   主键id
	 */
	private String id;

	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	private String type;


	/**
	 * 开始时间
	 */
	private String startTime;

	/**
	 * 结束时间
	 */
	private String endTime;

}
