package com.dy.yunying.api.datacenter.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * @ClassName HourDataVo
 * @Description todo
 * @Author nieml
 * @Time 2021/6/24 14:31
 * @Version 1.0
 **/
@Data
public class HourDataVo {

	private Long day;
	//小时
	private Long hour;
	//父游戏
	private Long pgid;
	private String parentGameName;
	//子游戏
	private Long gameid;
	private String gameName;
	//主渠道
	private String parentchl;
	private String parentchlName;
	//渠道分包
	private String appchl;
	//部门
	private String deptId;
	private String deptName;
	//组别
	private String userGroupId;
	private String userGroupName;
	//投放人
	private String investor;
	private String investorName;

	//新增设备数
	private Integer newRegNums = 0;

	//付费设备数
	private Integer payAmount = 0;

	//新增充值金额
	private BigDecimal newRegPayAmount;

	public BigDecimal getNewRegPayAmount() {
		if (Objects.isNull(newRegPayAmount)){
			return new BigDecimal("0.00");
		}
		return newRegPayAmount;
	}

	//ltv
	//计算公式：新增充值金额/新增设备
	private BigDecimal ltv;

	public BigDecimal getLtv() {
		Integer newRegNums = this.newRegNums;
		BigDecimal newRegPayAmount = this.newRegPayAmount;
		BigDecimal res = new BigDecimal("0.00");
		if (newRegNums != null && newRegNums != 0) {
			if (newRegPayAmount == null || res.equals(newRegPayAmount)) {
				return res;
			}
			//计算公式：新增充值金额/新增设备
			return newRegPayAmount.divide(new BigDecimal(newRegNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}


}
