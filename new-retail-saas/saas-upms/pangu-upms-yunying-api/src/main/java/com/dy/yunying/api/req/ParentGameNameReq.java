package com.dy.yunying.api.req;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

public class ParentGameNameReq extends Page {
	private String gname; //父游戏名称

	public String getGname() {
		return gname;
	}

	public void setGname(String gname) {
		this.gname = gname;
	}
}
