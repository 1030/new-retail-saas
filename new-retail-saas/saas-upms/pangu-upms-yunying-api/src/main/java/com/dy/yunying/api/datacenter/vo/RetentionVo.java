package com.dy.yunying.api.datacenter.vo;


import com.dy.yunying.api.constant.OsEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @ClassName RetentionVo
 * @Description todo
 * @Author nieml
 * @Time 2021/6/21 16:09
 * @Version 1.0
 **/

@Data
public class RetentionVo implements Serializable {

	private static final long serialVersionUID = 7701827235400187248L;

	/**
	 * 日期，如：2020/02/02
	 * 周，如：2020年20周
	 * 月，如：2020年12月
	 * 汇总，如：汇总
	 */
	@ApiModelProperty("周期")
	private String period;

	@ApiModelProperty("主渠道")
	private String parentchl;
	private String parentchlName;

	@ApiModelProperty("渠道分包")
	private String appchl;

	// 父游戏
	private Long pgid;
	private String parentGameName;

	@ApiModelProperty("子游戏id")
	private Long gameid;

	// 子游戏名称
	private String gname;

	// 操作系统
	private Integer os;

	public String getOsStr() {
		return OsEnum.getName(os);
	}

	// 部门
	private String deptId;
	private String deptName;
	// 组别
	private String userGroupId;
	private String userGroupName;

	public String getUserGroupName() {
		return StringUtils.isBlank(userGroupName) ? "-" : userGroupName;
	}

	// 投放人
	private String investor;
	private String investorName;

	// 广告计划
	private String adid;

	@ApiModelProperty("消耗")
	private BigDecimal rudeCost;

	@ApiModelProperty("返点后消耗")
	private BigDecimal cost;

	// 计算公式：返点后消耗/新增设备注册数
	@ApiModelProperty("新增设备成本")
	private BigDecimal usrNameNumsCost;

	@ApiModelProperty("新增设备注册数")
	private Integer usrNameNums;

	// 计算公式：新增设备中次日登录的设备数/新增设备注册数
	@ApiModelProperty("次留")
	private BigDecimal retention2;
	private BigDecimal retention3;
	private BigDecimal retention4;
	private BigDecimal retention5;
	private BigDecimal retention6;
	private BigDecimal retention7;
	private BigDecimal retention8;
	private BigDecimal retention9;
	private BigDecimal retention10;
	private BigDecimal retention11;
	private BigDecimal retention12;
	private BigDecimal retention13;
	private BigDecimal retention14;
	private BigDecimal retention15;
	private BigDecimal retention16;
	private BigDecimal retention17;
	private BigDecimal retention18;
	private BigDecimal retention19;
	private BigDecimal retention20;
	private BigDecimal retention21;
	private BigDecimal retention22;
	private BigDecimal retention23;
	private BigDecimal retention24;
	private BigDecimal retention25;
	private BigDecimal retention26;
	private BigDecimal retention27;
	private BigDecimal retention28;
	private BigDecimal retention29;
	private BigDecimal retention30;

	private BigDecimal retention2Num;
	private BigDecimal retention3Num;
	private BigDecimal retention4Num;
	private BigDecimal retention5Num;
	private BigDecimal retention6Num;
	private BigDecimal retention7Num;
	private BigDecimal retention8Num;
	private BigDecimal retention9Num;
	private BigDecimal retention10Num;
	private BigDecimal retention11Num;
	private BigDecimal retention12Num;
	private BigDecimal retention13Num;
	private BigDecimal retention14Num;
	private BigDecimal retention15Num;
	private BigDecimal retention16Num;
	private BigDecimal retention17Num;
	private BigDecimal retention18Num;
	private BigDecimal retention19Num;
	private BigDecimal retention20Num;
	private BigDecimal retention21Num;
	private BigDecimal retention22Num;
	private BigDecimal retention23Num;
	private BigDecimal retention24Num;
	private BigDecimal retention25Num;
	private BigDecimal retention26Num;
	private BigDecimal retention27Num;
	private BigDecimal retention28Num;
	private BigDecimal retention29Num;
	private BigDecimal retention30Num;

	/*public BigDecimal getRetention2Num() {
		return this.retention2;
	}

	public BigDecimal getRetention3Num() {
		return this.retention3;
	}

	public BigDecimal getRetention4Num() {
		return this.retention4;
	}

	public BigDecimal getRetention5Num() {
		return this.retention5;
	}

	public BigDecimal getRetention6Num() {
		return this.retention6;
	}

	public BigDecimal getRetention7Num() {
		return this.retention7;
	}

	public BigDecimal getRetention8Num() {
		return this.retention8;
	}

	public BigDecimal getRetention9Num() {
		return this.retention9;
	}

	public BigDecimal getRetention10Num() {
		return this.retention10;
	}

	public BigDecimal getRetention11Num() {
		return this.retention11;
	}

	public BigDecimal getRetention12Num() {
		return this.retention12;
	}

	public BigDecimal getRetention13Num() {
		return this.retention13;
	}

	public BigDecimal getRetention14Num() {
		return this.retention14;
	}

	public BigDecimal getRetention15Num() {
		return this.retention15;
	}

	public BigDecimal getRetention16Num() {
		return this.retention16;
	}

	public BigDecimal getRetention17Num() {
		return this.retention17;
	}

	public BigDecimal getRetention18Num() {
		return this.retention18;
	}

	public BigDecimal getRetention19Num() {
		return this.retention19;
	}

	public BigDecimal getRetention20Num() {
		return this.retention20;
	}

	public BigDecimal getRetention21Num() {
		return this.retention21;
	}

	public BigDecimal getRetention22Num() {
		return this.retention22;
	}

	public BigDecimal getRetention23Num() {
		return this.retention23;
	}

	public BigDecimal getRetention24Num() {
		return this.retention24;
	}

	public BigDecimal getRetention25Num() {
		return this.retention25;
	}

	public BigDecimal getRetention26Num() {
		return this.retention26;
	}

	public BigDecimal getRetention27Num() {
		return this.retention27;
	}

	public BigDecimal getRetention28Num() {
		return this.retention28;
	}

	public BigDecimal getRetention29Num() {
		return this.retention29;
	}

	public BigDecimal getRetention30Num() {
		return this.retention30;
	}*/

	/*public BigDecimal getRetention2() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention2 = this.retention2;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention2 == null || res.equals(retention2)) {
				return res;
			}
			return retention2.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention3() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention3 = this.retention3;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention3 == null || res.equals(retention3)) {
				return res;
			}
			return retention3.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention4() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention4 = this.retention4;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention4 == null || res.equals(retention4)) {
				return res;
			}
			return retention4.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention5() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention5 = this.retention5;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention5 == null || res.equals(retention5)) {
				return res;
			}
			return retention5.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention6() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention6 = this.retention6;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention6 == null || res.equals(retention6)) {
				return res;
			}
			return retention6.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention7() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention7 = this.retention7;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention7 == null || res.equals(retention7)) {
				return res;
			}
			return retention7.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention8() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention8 = this.retention8;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention8 == null || res.equals(retention8)) {
				return res;
			}
			return retention8.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention9() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention9 = this.retention9;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention9 == null || res.equals(retention9)) {
				return res;
			}
			return retention9.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention10() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention10 = this.retention10;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention10 == null || res.equals(retention10)) {
				return res;
			}
			return retention10.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention11() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention11 = this.retention11;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention11 == null || res.equals(retention11)) {
				return res;
			}
			return retention11.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention12() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention12 = this.retention12;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention12 == null || res.equals(retention12)) {
				return res;
			}
			return retention12.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention13() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention13 = this.retention13;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention13 == null || res.equals(retention13)) {
				return res;
			}
			return retention13.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention14() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention14 = this.retention14;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention14 == null || res.equals(retention14)) {
				return res;
			}
			return retention14.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention15() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention15 = this.retention15;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention15 == null || res.equals(retention15)) {
				return res;
			}
			return retention15.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention16() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention16 = this.retention16;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention16 == null || res.equals(retention16)) {
				return res;
			}
			return retention16.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention17() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention17 = this.retention17;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention17 == null || res.equals(retention17)) {
				return res;
			}
			return retention17.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention18() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention18 = this.retention18;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention18 == null || res.equals(retention18)) {
				return res;
			}
			return retention18.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention19() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention19 = this.retention19;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention19 == null || res.equals(retention19)) {
				return res;
			}
			return retention19.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention20() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention20 = this.retention20;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention20 == null || res.equals(retention20)) {
				return res;
			}
			return retention20.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention21() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention21 = this.retention21;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention21 == null || res.equals(retention21)) {
				return res;
			}
			return retention21.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention22() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention22 = this.retention22;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention22 == null || res.equals(retention22)) {
				return res;
			}
			return retention22.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention23() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention23 = this.retention23;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention23 == null || res.equals(retention23)) {
				return res;
			}
			return retention23.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention24() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention24 = this.retention24;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention24 == null || res.equals(retention24)) {
				return res;
			}
			return retention24.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention25() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention25 = this.retention25;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention25 == null || res.equals(retention25)) {
				return res;
			}
			return retention25.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention26() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention26 = this.retention26;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention26 == null || res.equals(retention26)) {
				return res;
			}
			return retention26.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention27() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention27 = this.retention27;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention27 == null || res.equals(retention27)) {
				return res;
			}
			return retention27.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention28() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention28 = this.retention28;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention28 == null || res.equals(retention28)) {
				return res;
			}
			return retention28.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention29() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention29 = this.retention29;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention29 == null || res.equals(retention29)) {
				return res;
			}
			return retention29.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}

	public BigDecimal getRetention30() {
		Integer usrNameNums = this.usrNameNums;
		BigDecimal retention30 = this.retention30;
		BigDecimal res = new BigDecimal(0);
		if (usrNameNums != null && usrNameNums != 0) {
			if (retention30 == null || res.equals(retention30)) {
				return res;
			}
			return retention30.multiply(new BigDecimal(100)).divide(new BigDecimal(usrNameNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}*/

	/*public String getPeriod() {
		if (StringUtils.isNotBlank(period)) {
			if (period.contains("周")) {
				String periodYear = period.substring(0, 4);
				String periodWeek = period.substring(4, 7);
				return periodYear + "-" + periodWeek;
			}
		}
		return period;
	}*/

}
