package com.dy.yunying.api.enums;

public enum WithdrawTypeEnum {

	COINS(1, "游戏货币"),
	WECHAT(2, "微信"),
	ALIPAY(3, "支付宝"),
	VOUCHER(4, "代金券"),
	BALANCE(5, "游豆"),
	;

	private Integer type;
	private String desc;

	WithdrawTypeEnum(Integer type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public static WithdrawTypeEnum getOneByType(Integer type) {
		for (WithdrawTypeEnum typeEnum : WithdrawTypeEnum.values()) {
			if (typeEnum.getType().equals(type)) {
				return typeEnum;
			}
		}
		return null;
	}

	public Integer getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}
}
