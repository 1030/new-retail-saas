package com.dy.yunying.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @ClassName HourDataVo
 * @Description todo
 * @Author nieml
 * @Time 2021/6/24 14:31
 * @Version 1.0
 **/
@Data
public class HourDataVo {

	//小时
	private Long hour;
	//父游戏
	private Long pgid;
	private String parentGameName;
	//父游戏
	private Long gameid;
	private String gName;
	//主渠道
	private String parentchl;
	private String parentchlName;
	//渠道分包
	private String appchl;
	//部门
	private String deptId;
	private String deptName;
	//组别
	private String userGroupId;
	private String userGroupName;
	//投放人
	private String investor;
	private String investorName;

	//组别 todo

	//新增设备数
	private Integer newRegNums;
	//新增充值金额
	private BigDecimal newRegPayAmount = BigDecimal.ZERO;
	//ltv
	//计算公式：新增充值金额/新增设备
	private BigDecimal ltv;

	public BigDecimal getLtv() {
		Integer newRegNums = this.newRegNums;
		BigDecimal newRegPayAmount = this.newRegPayAmount;
		BigDecimal res = new BigDecimal("0.00");
		if (newRegNums != null && newRegNums != 0) {
			if (newRegPayAmount == null || res.equals(newRegPayAmount)) {
				return res;
			}
			//计算公式：新增充值金额/新增设备
			return newRegPayAmount.divide(new BigDecimal(newRegNums), 2, RoundingMode.HALF_UP);
		}
		return res;
	}


}
