package com.dy.yunying.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collection;
import java.util.Set;

@Data
@Accessors(chain = true)
public class MaterialDataVO extends Page<Object> {

	/**
	 * 开始日期，格式为：20220307
	 */
	private String sdate;

	/**
	 * 结束日期，格式为：20220307
	 */
	private String edate;

	/**
	 * 主渠道编码
	 */
	private String parentchl;

	/**
	 * 系统类型
	 */
	private Integer os;

	/**
	 * 素材类型
	 */
	private Integer materialType;

	/**
	 * 创意者ID
	 */
	private Integer creatorId;

	/**
	 * 制作者ID
	 */
	private Integer makerId;

	/**
	 * 主游戏ID
	 */
	private Collection<Long> pgids;
	private Long pgid;

	/**
	 * 制作类型
	 */
	private Integer makeType;

	/**
	 * 素材ID
	 */
	private Long materialId;

	/**
	 * 素材名称
	 */
	private String materialName;

	/**
	 * 卖点名称
	 */
	private String sellingPointName;

	/**
	 * 视频宽度
	 */
	private String materialWidth;

	/**
	 * 视频高度
	 */
	private String materialHeight;

	/**
	 * 落地页ID
	 */
	private Long landingPageId;

	/**
	 * 周期
	 */
	private Integer period;

	/**
	 * 组别
	 */
	private Collection<String> groupBys;

	/**
	 * 排序字段
	 */
	private String orderByName;

	/**
	 * 排序顺序
	 */
	private String orderByDesc;

	/**
	 * 导出列名标题
	 */
	private String titles;

	/**
	 * 要到处列名称
	 */
	private String columns;
	/**
	 * 1：头条体验版数据
	 */
	private String experience;

	/**
	 * 标签id，多个用逗号分隔
	 */

	private String tagIds;


	/**
	 * 投放人ID列表
	 */

	private String investorArr;
	/**
	 * 素材ID集合
	 */
	private String materialIds;
	/**
	 * 数据类型: 1-素材数据，2-标签数据
	 */
	private Integer dataType;
}
