package com.dy.yunying.api.resp.hongbao;

import com.dy.yunying.api.entity.hongbao.PopupNotice;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author yuwenfeng
 * @description: 通用公告
 * @date 2021/11/30 20:00
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CommonNoticeVo extends PopupNotice {

	@ApiModelProperty(value = "渠道集合")
	private List<String[]> channelList;

	@ApiModelProperty(value = "游戏集合")
	private List<Long[]> gameList;

	@ApiModelProperty(value = "区服集合")
	private List<Long[]> areaList;

	@ApiModelProperty(value = "渠道数据集合")
	private List<String[]> channelDataList;

	@ApiModelProperty(value = "游戏数据集合")
	private List<Long[]> gameDataList;

	@ApiModelProperty(value = "区服数据集合")
	private List<Long[]> areaDataList;

	@ApiModelProperty(value = "游戏范围")
	private String gameNameStr;

	@ApiModelProperty(value = "区服范围")
	private String areaNameStr;

	@ApiModelProperty(value = "渠道范围")
	private String channelNameStr;
}
