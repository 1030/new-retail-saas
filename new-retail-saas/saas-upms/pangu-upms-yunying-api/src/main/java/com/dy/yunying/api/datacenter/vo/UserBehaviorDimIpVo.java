package com.dy.yunying.api.datacenter.vo;

import lombok.Data;

/**
 * @author ：leisw
 * @date ：2022/8/19 11:07
 * @description：
 * @modified By：
 */
@Data
public class UserBehaviorDimIpVo {

	//用户行为
	private String ip;

	//手机号
	private String area;

}
