package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 活动角色信息表
 *
 * @author chengang
 * @version 2021-10-28 12:02:13
 * table: hb_activity_role_info
 */
@Data
@Accessors(chain = true)
@TableName(value = "hb_activity_role_info")
public class HbActivityRoleInfo implements Serializable {

	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 来源类型：1-红包活动；2-签到活动；3-外部链接；4-抽奖活动；
	 */
	@TableField(value = "source_type")
	private Integer sourceType;

	/**
	 * 活动ID
	 */
	@TableField(value = "activity_id")
	private Long activityId;

	/**
	 * 游戏角色ID
	 */
	@TableField(value = "role_id")
	private String roleId;

	/**
	 * 主渠道编码
	 */
	@TableField(value = "parentchl")
	private String parentchl;

	/**
	 * 子渠道编码
	 */
	@TableField(value = "chl")
	private String chl;

	/**
	 * 分包渠道编码
	 */
	@TableField(value = "appchl")
	private String appchl;

	/**
	 * 主游戏ID
	 */
	@TableField(value = "pgame_id")
	private Long pgameId;

	/**
	 * 子游戏ID
	 */
	@TableField(value = "game_id")
	private Long gameId;

	/**
	 * 游戏区服ID
	 */
	@TableField(value = "area_id")
	private Long areaId;

	/**
	 * 用户ID
	 */
	@TableField(value = "user_id")
	private Long userId;

	/**
	 * 用户账号
	 */
	@TableField(value = "username")
	private String username;

	/**
	 * 累计收益
	 */
	@TableField(value = "total_income")
	private BigDecimal totalIncome;


	/**
	 * 累计现金收益
	 */
	@TableField(value = "total_cash_income")
	private BigDecimal totalCashIncome;


	/**
	 * 累计提现金额
	 */
	@TableField(value = "total_withdrawal_amount")
	private BigDecimal totalWithdrawalAmount;

	/**
	 * 累计提现成功金额
	 */
	@TableField(value = "total_arrive_amount")
	private BigDecimal totalArriveAmount;

	/**
	 * 累计申请提现金额
	 */
	@TableField(value = "cashed_apply_amount")
	private BigDecimal cashedApplyAmount;

	/**
	 * 累计驳回提现金额
	 */
	@TableField(value = "cashed_reject_amount")
	private BigDecimal cashedRejectAmount;

	/**
	 * 累计通过提现金额
	 */
	@TableField(value = "cashed_pass_amount")
	private BigDecimal cashedPassAmount;

	/**
	 * 首次充值时间
	 */
	@TableField(value = "first_recharge_time")
	private Date firstRechargeTime;

	/**
	 * 单笔最高充值金额
	 */
	@TableField(value = "max_recharge_amount")
	private BigDecimal maxRechargeAmount;

	/**
	 * 累计充值金额
	 */
	@TableField(value = "total_recharge_amount")
	private BigDecimal totalRechargeAmount;

	/**
	 * 角色等级
	 */
	@TableField(value = "role_level")
	private Integer roleLevel;

	/**
	 * 开服时间
	 */
	@TableField(value = "create_area_time")
	private Date createAreaTime;

	/**
	 * 创角时间
	 */
	@TableField(value = "create_role_time")
	private Date createRoleTime;

	/**
	 * 参与活动时间
	 */
	@TableField(value = "campaign_time")
	private Date campaignTime;

	/**
	 * 角色活跃时间
	 */
	@TableField(value = "active_time")
	private Date activeTime;

	/**
	 * 总补签次数/总抽奖次数
	 */
	@TableField(value = "total_sign_num")
	private Integer totalSignNum;

	/**
	 * 已使用补签次数/已抽奖次数
	 */
	@TableField(value = "used_sign_num")
	private Integer usedSignNum;

	/**
	 * 累计获得积分
	 */
	@TableField(value = "total_points_amount")
	private BigDecimal totalPointsAmount;

	/**
	 * 已使用积分
	 */
	@TableField(value = "used_points_amount")
	private BigDecimal usedPointsAmount;

	/**
	 * 查看收益列表最新时间
	 */
	@TableField(value = "read_income_time")
	private Date readIncomeTime;

	/**
	 * 是否删除：0-否；1-是
	 */
	@TableField(value = "deleted")
	private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

}