package com.dy.yunying.api.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/6/17 15:12
 * @description：
 * @modified By：
 */
@Setter
@Getter
public class AdDataVo {

	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	// 周期
	private String period;


	@ApiModelProperty("父游戏id")
	private Long pgid;
	private String parentGameName;


	//系统类型
	@ApiModelProperty("系统类型")
	private Integer os;

	public String getOsStr() {
		String osStr = OsEnum.getName(os);
		return osStr;
	}

	@ExcelProperty("父渠道")
	// 渠道类型
	@ApiModelProperty("父渠道")
	private String parentchl;

	//主渠道名称
	private String parentchlName;


	@ApiModelProperty("部门主键")
	// 广告计划ID
	private String deptId;
	@ExcelProperty("部门名称")
	@ApiModelProperty("部门名称")
	// 广告计划ID
	private String deptName;

	@ApiModelProperty("组别主键")
	private String userGroupId;
	@ExcelProperty("组别名称")
	@ApiModelProperty("组别名称")
	private String userGroupName;

	private String investor;
	@ExcelProperty("投放人姓名")
	@ApiModelProperty("投放人")
	private String investorName;

	@ExcelProperty("新增设备注册数")
	//新增设备注册数(新用户注册)
	@ApiModelProperty("新增设备注册数")
	private Integer usrnamenums = 0;

	@ExcelProperty("新增设备付费数")
	@ApiModelProperty("新增设备付费数")
	private Integer paydeviceAll = 0;

	@ExcelProperty("新增充值金额")
	@ApiModelProperty("新增充值金额")
	private BigDecimal newPayAmount = new BigDecimal(0);


	@ExcelProperty("当周充值金额")
	@ApiModelProperty("当周充值金额")
	private BigDecimal weektotalfee = new BigDecimal(0);


	@ExcelProperty("当月充值金额")
	@ApiModelProperty("当月充值金额")
	private BigDecimal monthtotalfee = new BigDecimal(0);


	@ExcelProperty("活跃设备数")
	@ApiModelProperty("活跃设备数")
	private Integer activeNum = 0;

	@ExcelProperty("活跃充值设备数")
	// 充值金额
	@ApiModelProperty("活跃充值设备数")
	private BigDecimal activpaydevice = new BigDecimal(0);

	@ExcelProperty("活跃充值金额")
	// 充值金额
	@ApiModelProperty("活跃充值金额")
	private BigDecimal activetotalfee = new BigDecimal(0);


	@ExcelProperty("活跃ARPU")
	//	活跃ARPU= 活跃充值金额/活跃设备数
	@ApiModelProperty("活跃ARPU")
	private BigDecimal actarpu = new BigDecimal(0);

	private Integer num1 = 0; //设备次日留存数

	// 次留: 新增设备在次日有登录行为的设备数/新增设备数
	@ExcelProperty("次留")
	@ApiModelProperty("次留")
	@JsonProperty("retention2Ratio")
	private BigDecimal retention2Ratio = new BigDecimal(0);


	@ExcelProperty("消耗")
	@ApiModelProperty("消耗后消耗（总成本）")
	@JsonProperty("cost")
	private BigDecimal cost = new BigDecimal(0);

	// 返点后消耗（总成本）
	@ExcelProperty("返点前消耗")
	@ApiModelProperty("返点前消耗")
	private BigDecimal rudecost = new BigDecimal(0);

	@ExcelProperty("设备成本")
	@ApiModelProperty("设备成本")
	// 设备成本  消耗/新增设备注册数
	private BigDecimal deviceCose = new BigDecimal(0);


	@ExcelProperty("新增arpu")
	@ApiModelProperty("新增arpu ")
	//注册arpu  新增充值金额/新增设备数
	private BigDecimal regarpu = new BigDecimal(0);

	@ExcelProperty("新增付费率")
	@ApiModelProperty("新增付费率 ")
	//新增付费率  新增设备付费数/新增设备注册数
	private BigDecimal regPayRatio = new BigDecimal(0);

	@ExcelProperty("首日ROI")
	@ApiModelProperty("首日ROI  ")
	// roi1
	private BigDecimal roi1 = new BigDecimal(0);


	@ExcelProperty("当周ROI")
	@ApiModelProperty("当周ROI ")
	private BigDecimal weekRoi = new BigDecimal(0);

	@ExcelProperty("当月ROI")
	@ApiModelProperty("当月ROI ")
	private BigDecimal monthRoi = new BigDecimal(0);

	@ExcelProperty("累计充值金额")
	// 累计充值金额
	@ApiModelProperty("累计充值金额")
	private BigDecimal totalPayfee = new BigDecimal(0);

	@ExcelProperty("累计充值ROI")
	@ApiModelProperty("累计充值ROI ")
	//总roi 累计充值金额/总消耗
	private BigDecimal allRoi = new BigDecimal(0);

	public String getPeriod() {
		if (StringUtils.isNotBlank(period)) {
			if (period.contains("周")) {
				String periodYear = period.substring(0, 4);
				String periodWeek = period.substring(4, 7);
				return periodYear + "-" + periodWeek;
			}
		}
		return period;
	}

}
