package com.dy.yunying.api.resp.znfx;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 */
@Data
@Accessors(chain = true)
public class AdPlanAnalyseRes implements Serializable {

	private static final long serialVersionUID = -5853268354883845787L;

	@ApiModelProperty(value = "计划ID")
	private String adId;

	@ApiModelProperty(value = "广告账户ID")
	private String advertiserId;

	@ApiModelProperty(value = "平台ID")
	private Integer ctype;

	@ApiModelProperty(value = "投放范围")
	private String deliveryRange;

	@ApiModelProperty(value = "预算类型")
	private String budgetMode;

	@ApiModelProperty(value = "下载方式")
	private String downloadType;

	@ApiModelProperty(value = "转化目标")
	private String externalAction;

	@ApiModelProperty(value = "深度转化目标")
	private String deepExternalAction;

	@ApiModelProperty(value = "性别")
	private String gender;

	@ApiModelProperty(value = "年龄")
	private String age;

	@ApiModelProperty(value = "行为兴趣")
	private String interestActionMode;

	@ApiModelProperty(value = "设备类型")
	private String deviceType;

	@ApiModelProperty(value = "受众网络类型")
	private String ac;

	@ApiModelProperty(value = "运营商")
	private String carrier;

	@ApiModelProperty(value = "过滤已转化用户")
	private String hideIfConverted;

	@ApiModelProperty(value = "出价方式")
	private String smartBidType;

	@ApiModelProperty(value = "调整自动出价")
	private String adjustCpa;

	@ApiModelProperty(value = "投放时间")
	private String scheduleType;

	@ApiModelProperty(value = "深度优化方式")
	private String deepBidType;

	@ApiModelProperty(value = "学历")
	private String education;
}
