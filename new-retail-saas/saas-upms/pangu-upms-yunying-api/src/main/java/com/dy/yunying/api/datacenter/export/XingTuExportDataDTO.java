package com.dy.yunying.api.datacenter.export;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class XingTuExportDataDTO  implements Serializable {

	private static final long serialVersionUID = 8701827235400187248L;

	/**
	 * 周期
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("周期")
	private String cycleType;

	/**
	 * 投放方式：FIXED_PRICE - KOL(一口价)；CPM - KOL(CPM按次)
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("投放方式")
	private String settleType;

	/**
	 * 投放方式中文名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("投放方式")
	private String settleName;

	/**
	 * 投放人ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("投放人ID")
	private Long investorId;

	/**
	 * 投放人姓名
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("投放人")
	private String investorName;

	/**
	 * 主渠道编码
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("主渠道编码")
	private String parentchl;

	/**
	 * 主渠道名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("主渠道名称")
	private String parentchlName;

	/**
	 * 分包渠道编码
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("分包渠道编码")
	private String appchl;

	/**
	 * 分包渠道名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("分包渠道")
	private String appchlName;

	/**
	 * 部门主键
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("部门主键")
	private Long deptId;

	/**
	 * 部门名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("部门")
	private String deptName;

	/**
	 * 主游戏ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("主游戏ID")
	private Long pgid;

	/**
	 * 主游戏名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("主游戏")
	private String pgname;

	/**
	 * 子游戏ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("子游戏ID")
	private Long gameid;

	/**
	 * 子游戏名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("子游戏")
	private String gname;

	/**
	 * 系统标识
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("系统标识")
	private Integer os;

	/**
	 * 系统名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("系统")
	private String osName;

	/**
	 * 星图账户
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("星图账户")
	private String advertiserId;

	/**
	 * 任务ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("任务ID")
	private Long demandId;

	/**
	 * 任务名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("任务名称")
	private String demandName;

	/**
	 * 达人ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("达人ID")
	private Long authorId;

	/**
	 * 达人名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("达人名称")
	private String authorName;

	/**
	 * 作品名称
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("作品名称")
	private String title;

	/**
	 * 视频ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("视频ID")
	private Long itemId;

	/**
	 * 视频封面连接
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("视频封面连接")
	private String headImageUri;

	/**
	 * 视频链接
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("视频链接")
	private String videoUrl;

	/**
	 * 上线时间
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("上线时间")
	private String onlineTime;

	/**
	 * 成本（订单金额/点击数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("成本")
	private BigDecimal cost;

	/**
	 * 组件点击量
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("组件点击量")
	private Integer clickCount;

	/**
	 * 组件点击率（组件点击量/组件展示量）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("组件点击率")
	private String clickRate;

	/**
	 * 组件展示量
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("组件展示量")
	private Integer showCount;

	/**
	 * 千次播放成本（订单金额/播放次数/1000）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("千次播放成本")
	private BigDecimal cpmCost;

	/**
	 * 播放次数
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("播放次数")
	private Integer playCount;

	/**
	 * 订单金额
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("订单金额")
	private BigDecimal orderPrice;

	/**
	 * 完播率（完成播放数/播放数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("完播率")
	private String finishRate;

	/**
	 * 有效播放率（有效播放数/播放数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("有效播放率")
	private String fivesPlayRate;

	/**
	 * 平均播放率（用户观看该任务视频的平均观看时长/播放数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("平均播放率")
	private BigDecimal avgPlayRate;

	/**
	 * 评论量
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("评论量")
	private Integer commentVolume;

	/**
	 * 点赞量
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("点赞量")
	private Integer likeVolume;

	/**
	 * 播放量
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("播放量")
	private Integer playVolume;

	/**
	 * 分享量
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("分享量")
	private Integer shareVolume;

	/**
	 * 激活数（新激活的设备）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("激活数")
	private Integer deviceCount;

	/**
	 * 点击激活率（激活数/点击数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("点击激活率")
	private String clickDeviceRate;

	/**
	 * 新增设备注册数（新激活的设备并完成注册行为的设备数去重）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("新增设备注册数")
	private Integer registerCount;

	/**
	 * 激活注册率（新增设备注册数/激活数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("激活注册率")
	private String deviceRegisterRate;

	/**
	 * 设备成本（订单金额/新增设备注册数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("设备成本")
	private BigDecimal deviceCost;

	/**
	 * 新增ARPU(新增充值金额/新增设备注册数)
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("新增ARPU")
	private BigDecimal newArpu;

	/**
	 * 新增付费数（新增设备在新增当天有充值行为的设备数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("新增付费数")
	private Integer newPayCount;

	/**
	 * 累计付费数（新增设备从新增当天到当前时间有充值行为的设备数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("累计付费数")
	private Integer totalPayCount;

	/**
	 * 新增付费率（新增设备付费数/新增设备注册数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("新增付费率")
	private String newPayRate;

	/**
	 * 新增充值实付金额（新增设备在新增当天的充值实付金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("新增充值实付金额")
	private BigDecimal newPayFee;

	/**
	 * 新增充值代金券金额（新增设备在新增当天的充值代金券金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("新增充值代金券金额")
	private BigDecimal newPayGivemoney;

	/**
	 * 活跃充值实付金额（活跃设备在选择时间段的充值实付金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活跃充值实付金额")
	private BigDecimal activePayFee;

	/**
	 * 活跃充值代金券金额（活跃设备在选择时间段的充值代金券金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活跃充值代金券金额")
	private BigDecimal activePayGivemoney;

	/**
	 * 当周充值实付金额（新增设备在新增当周的充值实付金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("当周充值实付金额")
	private BigDecimal weekPayFee;

	/**
	 * 当周充值代金券金额（新增设备在新增当周的充值代金券金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("当周充值代金券金额")
	private BigDecimal weekPayGivemoney;

	/**
	 * 当月充值实付金额（新增设备在新增当前月的充值实付金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("当月充值实付金额")
	private BigDecimal monthPayFee;

	/**
	 * 当月充值代金券金额（新增设备在新增当前月的充值代金券金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("当月充值代金券金额")
	private BigDecimal monthPayGivemoney;

	/**
	 * 活跃ARPU（活跃充值金额/活跃设备数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活跃ARPU")
	private BigDecimal activeArpu;

	/**
	 * 活跃设备数（在时间段内有登录行为的设备数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活跃设备数")
	private Integer activeDeviceCount;

	/**
	 * 期内充值实付金额
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("期内充值实付金额")
	private BigDecimal periodPayFee;

	/**
	 * 期内充值代金券金额
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("期内充值代金券金额")
	private BigDecimal periodPayGivemoney;

	/**
	 * 活跃付费ARPPU
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活跃付费ARPPU")
	private BigDecimal activePayArpu;

	/**
	 * 新增付费ARPPU
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("新增付费ARPPU")
	private BigDecimal newPayArpu;

	/**
	 * 活跃付费率
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活跃付费率")
	private BigDecimal activePayRate;

	/**
	 * 首日ROI（新增充值金额/订单金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("首日ROI")
	private String firstDayRoi;

	/**
	 * 当周ROI（当周充值金额/订单金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("当周ROI")
	private String firstWeekRoi;

	/**
	 * 当月ROI（当月充值金额/订单金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("当月ROI")
	private String firstMonthRoi;

	/**
	 * 累计充值ROI（累计充值金额/订单金额）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("累计充值ROI")
	private String totalPayRoi;

	/**
	 * 累计充值实付金额（新增设备从新增当天到当前时间的充值实付金额总和）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("累计充值实付金额")
	private BigDecimal totalPayFee;

	/**
	 * 累计充值代金券金额（新增设备从新增当天到当前时间的充值代金券金额总和）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("累计充值代金券金额")
	private BigDecimal totalPayGivemoney;

	/**
	 * 次留（新增设备在次日有登录行为的设备数/新增设备数）
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("次留")
	private String retention2;

}
