package com.dy.yunying.api.entity.znfx;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * @author: zhuxm
 * @time: 2023/3/21 14:47
 */
@Data
public class ChartVO implements Serializable {
	private static final long serialVersionUID = 8319464242139105553L;

	//标题
	private String title;
	//唯一标识
	private String target;

	//1:横坐标横向放置   2:横坐标纵向放置   默认为1
	private Integer isAxis = 1;

	//数据
	private List<ChartData> series;

}
