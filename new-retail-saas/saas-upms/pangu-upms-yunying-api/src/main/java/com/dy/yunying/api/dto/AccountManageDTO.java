package com.dy.yunying.api.dto;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author sunyq
 * @date 2022/8/18 18:25
 */
@Data
public class AccountManageDTO extends Page {
	/**
	 * 起始时间
	 */
	private String startTime;
	/**
	 * 结束时间
	 */
	private String endTime;
	/**
	 * 主渠道
	 */
	private String parentChlArr;
	/**
	 * 子渠道编码列表
	 */
	private String chlArr;
	/**
	 * 分包编码列表
	 */
	private String appChlArr;
	/**
	 * 0：Web 1：H5  2:Android 3:IOS
	 */
	private String regSrc;
	/**
	 * 防沉迷  0 已成年  1 未成年 2 未实名
	 */
	private Integer antiAddiction;
	/**
	 * 账户状态  0 正常  1 封禁
	 */
	private Integer accountStatus;

	/**
	 * 手机号码
	 */
	private String phoneNumber;
	/**
	 * 账号 （username）
	 */
	private String account;
	/**
	 * 注册ip
	 */
	private String regIp;
	/**
	 * 姓名身份证
	 */
	private String idCard;

	@ApiModelProperty("排序  desc / asc")
	private String sort;

	@ApiModelProperty("排序字段")
	private String kpiValue;
}
