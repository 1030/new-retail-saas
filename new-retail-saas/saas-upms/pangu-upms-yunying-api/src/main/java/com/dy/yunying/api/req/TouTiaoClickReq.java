package com.dy.yunying.api.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @description: 触发头条链接
 * @author yuwenfeng
 * @date 2022/3/10 20:21
 */
@Data
public class TouTiaoClickReq implements Serializable {

	@ApiModelProperty(value = "头条链接")
	@NotNull(message = "头条链接不能为空")
    private String touTiaoUrl;

}
