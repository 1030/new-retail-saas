package com.dy.yunying.api.resp.prize;

import com.dy.yunying.api.enums.PopupMoldEnum;
import com.dy.yunying.api.enums.PushIntervalMapWeekEnum;
import com.dy.yunying.api.enums.PushNodeEnum;
import com.dy.yunying.api.enums.PushTypeEnum;
import com.sjda.framework.common.utils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 奖励推送表
 *
 * @author chenxiang
 * @version 2022-04-25 16:25:27
 * table: prize_push
 */
@Data
public class PrizePushRes implements Serializable {

	@ApiModelProperty(value = "主键id")
	private Long id;

	@ApiModelProperty(value = "用户群组ID(多个逗号分割)")
	private String userGroupId;

	@ApiModelProperty(value = "标题文本")
	private String title;

	@ApiModelProperty(value = "说明文本")
	private String description;

	@ApiModelProperty(value = "icon图")
	private String icon;

	@ApiModelProperty(value = "有效开始时间")
	private Date startTime;

	@ApiModelProperty(value = "有效结束时间")
	private Date endTime;

	@ApiModelProperty(value = "发放频率：1每天，2隔天，3每周，4仅一次")
	private Integer pushType;

	@ApiModelProperty(value = "发放频率(天：为间隔天数，周：逗号分割)")
	private String pushInterval;

	@ApiModelProperty(value = "推送节点：1首次登录成功，2每次登录成功，3首次进入游戏，4每次进入游戏")
	private Integer pushNode;

	@ApiModelProperty(value = "推送目标：1-账户，2-角色")
	private Integer pushTarget;

	@ApiModelProperty(value = "推送类型：1单个奖励，2组合奖励")
	private Integer pushPrizeType;

	@ApiModelProperty(value = "弹框形式：1单个奖励，2弹框，3图文，4通栏，5消息")
	private Integer popupMold;

	@ApiModelProperty(value = "打开类型：0不跳转，1SDK面板，2内置浏览器，3外部链接")
	private Integer openType;

	@ApiModelProperty(value = "菜单Id")
	private String menuId;

	@ApiModelProperty(value = "菜单名称")
	private String menuTitle;

	@ApiModelProperty(value = "菜单编码")
	private String menuCode;

	@ApiModelProperty(value = "ios 菜单编码地址")
	private String menuCodeIos;
	/**
	 * 二级菜单编码(安卓)
	 */
	@ApiModelProperty(value = "二级菜单编码(安卓)")
	private String secondMenuCode;
	/**
	 * 二级菜单编码(苹果)
	 */
	@ApiModelProperty(value = "二级菜单编码(苹果)")
	private String secondMenuCodeIos;

	@ApiModelProperty(value = "来源类型(1红包活动 2签到活动)")
	private Integer skipMold;

	@ApiModelProperty(value = "跳转位置 如：红包类型 1：等级红包、2：充值红包、3：邀请红包、4：定制红包")
	private Integer skipBelongType;

	@ApiModelProperty(value = "来源id, 如：红包活动id")
	private Long skipId;

	@ApiModelProperty(value = "跳转地址")
	private String skipUrl;

	@ApiModelProperty(value = "跳转地址IOS")
	private String skipUrlIOS;

	@ApiModelProperty(value = "展示方式(1横屏展示，2竖屏展示)")
	private Integer showType;

	@ApiModelProperty(value = "展示顺序：降序")
	private Integer showSort;

	@ApiModelProperty(value = "状态：1正常，2停用")
	private Integer status;

	@ApiModelProperty(value = "是否删除  0否 1是")
	private Integer deleted;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

	@ApiModelProperty(value = "创建人")
	private Long createId;

	@ApiModelProperty(value = "修改人")
	private Long updateId;

	@ApiModelProperty(value = "奖品配置集合")
	private List<PrizeConfigRes> prizeConfigList;

	/**
	 * 扩展显示字段
	 */
	/**
	 * 奖励内容
	 */
	private String prizeTitles;

	/**
	 * 推送节点 展示
	 */
	private String pushText;

	/**
	 * 弹窗类型 展示
	 */
	private String popupMoldText;

	/**
	 * 有效开始时间 展示
	 */
	private String startTimeText;

	/**
	 * 有效结束时间 展示
	 */
	private String endTimeText;

	/**
	 * 状态 展示
	 */
	private String statusText;

	/**
	 * 游戏范围展示
	 */
	private String gameNameStr;

	/**
	 * 区服范围展示
	 */
	private String areaNameStr;

	/**
	 * 渠道范围展示
	 */
	private String channelNameStr;
	/**
	 * 列表跳转方式展示
	 */
	private String pushSkipTypeStr;

	@ApiModelProperty(value = "用户群组名称(多个逗号分割)")
	private String userGroupNameStr;

	@ApiModelProperty(value = "渠道集合")
	private List<String[]> channelList;

	@ApiModelProperty(value = "游戏集合")
	private List<Long[]> gameList;

	@ApiModelProperty(value = "区服集合")
	private List<Long[]> areaList;

	@ApiModelProperty(value = "渠道数据集合")
	private List<String[]> channelDataList;

	@ApiModelProperty(value = "游戏数据集合")
	private List<Long[]> gameDataList;

	@ApiModelProperty(value = "区服数据集合")
	private List<Long[]> areaDataList;

	public String getPushText() {
		if (PushTypeEnum.EVERYDAY.getType().equals(pushType)) {
			return PushTypeEnum.getName(pushType) + " - " + PushNodeEnum.getName(pushNode);
		} else if (PushTypeEnum.THENEXTDAY.getType().equals(pushType)) {
			return "每" + pushInterval + "天 - " + PushNodeEnum.getName(pushNode);
		} else if (PushTypeEnum.WEEKLY.getType().equals(pushType)) {
			String[] split = pushInterval.split(",");
			List<String> weeks = new ArrayList<>();
			for (int i = 0; i < split.length; i++) {
				PushIntervalMapWeekEnum pushIntervalMapWeekEnum = PushIntervalMapWeekEnum.getPushIntervalMapWeekEnum(split[i]);
				if (pushIntervalMapWeekEnum != null){
					weeks.add(pushIntervalMapWeekEnum.getName());
				}
			}
			return "每"+String.join("、",weeks) + " - " + PushNodeEnum.getName(pushNode);
		} else if (PushTypeEnum.ONLYONCE.getType().equals(pushType)) {
			return PushTypeEnum.getName(pushType);
		}
		return null;
	}

	public String getPopupMoldText() {
		return PopupMoldEnum.getName(popupMold);
	}

	public String getStartTimeText() {
		return DateUtils.dateToString(startTime, DateUtils.YYYY_MM_DD_HH_MM_SS);
	}

	public String getEndTimeText() {
		return DateUtils.dateToString(endTime, DateUtils.YYYY_MM_DD_HH_MM_SS);
	}

	public String getStatusText() {
		if (1 == status.intValue()) {
			Long currentTime = System.currentTimeMillis();
			if (currentTime < startTime.getTime()) {
				return "待上线";
			} else if (currentTime > endTime.getTime()) {
				return "已下线";
			} else if (currentTime >= startTime.getTime() && currentTime <= endTime.getTime()) {
				return "上线中";
			}
		}
		return "未启用";
	}

}
