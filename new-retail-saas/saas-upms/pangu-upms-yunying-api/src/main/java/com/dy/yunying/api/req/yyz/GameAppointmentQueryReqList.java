package com.dy.yunying.api.req.yyz;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 游戏预约表
 * @author  kongyanfang
 * @version  2022-01-17 17:14:29
 * table: game_appointment
 */
@Data
public class GameAppointmentQueryReqList implements Serializable {

	@ApiModelProperty(value = "预约类型：1幻兽仓角预约  2 烛光勇士预约")
	private String type;

	@ApiModelProperty(value = "开始时间")
	private String appointStartTime;

	@ApiModelProperty(value = "结束时间")
	private String appointEndTime;

	@ApiModelProperty(value = "预约手机号")
	private String mobile;

	@ApiModelProperty(value = "手机系统")
	private String appointDevice;

}
