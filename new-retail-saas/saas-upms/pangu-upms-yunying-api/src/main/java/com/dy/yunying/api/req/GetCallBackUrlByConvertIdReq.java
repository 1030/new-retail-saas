package com.dy.yunying.api.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @description: 获取头条检测链接
 * @author yuwenfeng
 * @date 2022/3/2 17:05
 */
@Data
public class GetCallBackUrlByConvertIdReq implements Serializable {

	@ApiModelProperty(value = "转化ID")
	@NotNull(message = "转化ID不能为空")
    private String adPlatformId;

}
