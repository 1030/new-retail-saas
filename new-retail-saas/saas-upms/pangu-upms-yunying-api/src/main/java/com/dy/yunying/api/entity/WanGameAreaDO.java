package com.dy.yunying.api.entity;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 游戏分区
 * wan_game_area
 * @author kongyanfang
 * @date 2020-07-22 15:21:57
 */
@Getter
@Setter
public class WanGameAreaDO {
    /**
     * ID
     */
    private Long id;

    /**
     * 游戏ID
     */
    private Long gid;

    /**
     * 游戏厂商分区编号
     */
    private String mcode;

    /**
     * 分区名称
     */
    private String name;

    /**
     * 区服地址
     */
    private String url;

    /**
     * 状态 1:火爆 2:流畅 3:维护
     */
    private Short status;

    /**
     * 开区时间
     */
    private Date opentime;

    /**
     * 注册用户数
     */
    private Long registernum;

    /**
     * 充值金额
     */
    private BigDecimal recharge;

    /**
     * 付费用户数
     */
    private Long rechargeusernum;

    /**
     * 是否删除0、否，1、是
     */
    private Short isdelete;

    /**
     * 添加时间
     */
    private Date createtime;

    /**
     * 更新时间
     */
    private Date updatetime;
}