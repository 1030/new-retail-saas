package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author chenxiang
 * @className RewardTypeEnum
 * @date 2022-11-8 17:55
 */
public enum RewardTypeEnum {
	TYPE1("1", "抽奖次数");

	private String type;
	private String name;
	public String getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private RewardTypeEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(String type) {
		if (StringUtils.isBlank(type)){
			return null;
		}
		for (RewardTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return null;
	}
}
