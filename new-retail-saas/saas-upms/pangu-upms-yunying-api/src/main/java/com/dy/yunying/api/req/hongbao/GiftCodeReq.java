package com.dy.yunying.api.req.hongbao;

import lombok.Data;

import java.io.Serializable;

@Data
public class GiftCodeReq implements Serializable {

	private Integer type;
	private Long objectId;
}
