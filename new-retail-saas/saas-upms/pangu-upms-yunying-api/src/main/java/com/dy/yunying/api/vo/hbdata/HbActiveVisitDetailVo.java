package com.dy.yunying.api.vo.hbdata;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;
import java.io.Serializable;

/**
 * @Description
 * @Author chengang
 * @Date 2022/1/13
 */
@Data
public class HbActiveVisitDetailVo implements Serializable {
	//活动ID
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活动ID")
	private Long activityId;

	// 活动名称
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("活动名称")
	private String activityName;

	// 活动类型
	private Integer activityType;

	// 活动类型名称
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活动类型")
	private String activityTypeName;

	//活动PV
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("PV")
	private Long activityPV;

	//活动UV
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("UV")
	private Long activityUV;

	//弹窗送达数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("弹窗送达数")
	private Long popUpNum;

	//弹窗PV
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("弹窗PV")
	private Long popUpPV;

	//弹窗UV
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("弹窗UV")
	private Long popUpUV;

	//一键提现按钮点击数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("一键提现按钮点击数")
	private Long oneWithdrawalClickNum;

	//提现按钮点击数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("提现按钮点击数")
	private Long withdrawalClickNum;

	//提现记录按钮点击数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("提现记录按钮点击数")
	private Long withdrawalRecordClickNum;

	//浮标特效点击总数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("浮标特效点击总数")
	private Long floatClickNum;

	//收益明细按钮点击数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("收益明细按钮点击数")
	private Long earnClickNum;

	public String getActivityTypeName() {
		String activityTypeName;
		switch (activityType) {
			case 1:
				activityTypeName = "等级红包";
				break;
			case 2:
				activityTypeName = "充值红包";
				break;
			case 3:
				activityTypeName = "邀请红包";
				break;
			case 4:
				activityTypeName = "定制红包";
				break;
			default:
				activityTypeName = "-";
		}
		return activityTypeName;
	}


}
