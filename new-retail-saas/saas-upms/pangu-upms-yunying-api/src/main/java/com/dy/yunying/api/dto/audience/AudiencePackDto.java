package com.dy.yunying.api.dto.audience;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

/**
 * @Description
 * @Author chengang
 * @Date 2022/7/18
 */
@Data
public class AudiencePackDto extends Page {

	//父游戏 逗号分割
	private String pgidArr;

	private String startDate;

	private String endDate;

	// 开始充值金额
	private String startPay;

	// 结束充值金额
	private String endPay;

	// 开始活跃天数
	private String activeStartNum;

	//结束活跃天数
	private String activeEndNum;

	//开始活跃最大天数
	private String activeStartMaxNum;

	//结束活跃最大天数
	private String activeEndMaxNum;


	// 2022-07-18 MOD 不支持手机号打包 去除6:MOBILE_HASH_SHA256、9:MOBILE_MD5
	//打包字段 0：IMEI 1:IDFA 2:UID 4:IMEI_MD5 5:IDFA_MD5 6:MOBILE_HASH_SHA256  7:OAID 8:OAID_MD5 9:MOBILE_MD5 10:MAC地址
	private Integer type;
	//人群包名称
	private String packageName;

}
