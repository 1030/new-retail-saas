package com.dy.yunying.api.resp.raffle;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 礼品记录导出
 * @author chenxiang
 * @className RaffleReceiveRecordExportRes
 * @date 2022-11-10 16:53
 */
@Data
public class RaffleReceiveRecordExportRes implements Serializable {

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("用户账号")
	private String username;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("活动名称")
	private String activityName;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("所属游戏")
	private String parentGameName;;

//	@ColumnWidth(25)
//	@HeadFontStyle
//	@ExcelProperty("子游戏")
//	private String gameName;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("区服")
	private String areaId;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("角色名称")
	private String roleName;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("角色ID")
	private String roleId;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("礼品名称")
	private String objectName;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("礼品金额")
	private BigDecimal giftAmount;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("领取时间")
	private String receiveTime;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("状态")
	private String recordStatus;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("驳回原因")
	private String forbidReason;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("操作人")
	private String updateName;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("更新时间")
	private String updateTime;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("收货人姓名")
	private String receiptUsername;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("收货人电话")
	private String receiptPhone;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("收货地址")
	private String userAddress;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("快递名称")
	private String expressName;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("快递单号")
	private String expressCode;

}


