package com.dy.yunying.api.resp.yyz;

import lombok.Data;
import java.util.Date;


/**
 * 游戏预约档位库
 * @date 2021-06-19 16:12:13
 */
@Data
public class GameAppointmentScheduleGearsRes {
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 *
     */
    private Integer type;

	private String gameName;
	/**
	 * 规定要求人数
	 */
	private Long ruleNumber;

	/**
	 * 奖励内容描写
	 */
	private String reward;

	/**
	 * 图片名称
	 */
	private String name;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件访问地址
     */
    private String fileUrl;
    /**
     * 素材封面展示地址
     */
    private String imageUrl;
    /**
     * 素材宽
     */
    private String width;
    /**
     * 素材高
     */
    private String height;
	/**
	 * 尺寸（宽高）
	 */
	private String widthHeight;
    /**
     * 尺寸类型：（1:横屏视频，2:竖屏视频，3:小图，4:横版大图，5:竖版大图，6:gif图，7:卡片主图，0:其他）
     */
    private Integer screenType;
    /**
     * 视频大小
     */
    private String size;
    /**
     * 素材格式
     */
    private String format;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

}
