package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 红包配置表
 * @author  chenxiang
 * @version  2021-10-25 20:36:34
 * table: hb_redpack_config
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "hb_redpack_config")
public class HbRedpackConfig extends Model<HbRedpackConfig>{

	/**
	 * 主键ID
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/**
	 * 活动ID
	 */
	@TableField(value = "activity_id")
	private Long activityId;
	/**
	 * 红包内容：1现金红包，2礼包码，3实物礼品，4代金券
	 */
	@TableField(value = "type")
	private Integer type;
	/**
	 * 领取条件类型：1角色等级，2角色等级和创角时间，3角色累计充值，4角色首次充值，5角色单笔充值，6邀请好友成功注册游戏，7邀请好友首次提现成功，8邀请好友累计充值，9邀请好友创角N天内达到N级
	 */
	@TableField(value = "condition_type")
	private Integer conditionType;
	/**
	 * 领取条件：角色等级
	 */
	@TableField(value = "condition_role_level")
	private Integer conditionRoleLevel;
	/**
	 * 领取条件：创角天数
	 */
	@TableField(value = "condition_role_days")
	private Integer conditionRoleDays;
	/**
	 * 领取条件：角色充值金额
	 */
	@TableField(value = "condition_role_recharge")
	private BigDecimal conditionRoleRecharge;
	/**
	 * 红包数值
	 */
	@TableField(value = "money")
	private BigDecimal money;
	/**
	 * 使用门槛（0为不限制）
	 */
	@TableField(value = "limit_money")
	private BigDecimal limitMoney;
	/**
	 * 现金价值
	 */
	@TableField(value = "cash_values")
	private BigDecimal cashValues;
	/**
	 * 总数量
	 */
	@TableField(value = "gift_amount")
	private Integer giftAmount;
	/**
	 * 已领取数量
	 */
	@TableField(value = "gift_getcounts")
	private Integer giftGetcounts;
	/**
	 * 名称
	 */
	@TableField(value = "name")
	private String name;
	/**
	 * 标题
	 */
	@TableField(value = "title")
	private String title;
	/**
	 * 用户可领数量
	 */
	@TableField(value = "use_amount")
	private Integer useAmount;
	/**
	 * 角标：0无，1推荐
	 */
	@TableField(value = "angle_mark")
	private Integer angleMark;
	/**
	 * 详情状态：0关闭，1开启
	 */
	@TableField(value = "details_status")
	private Integer detailsStatus;
	/**
	 * 详情
	 */
	@TableField(value = "details")
	private String details;
	/**
	 * 状态：0停用，1启用
	 */
	@TableField(value = "status")
	private Integer status;
	/**
	 * 礼包码类型：1唯一码，2通用码
	 */
	@TableField(value = "gift_type")
	private Integer giftType;
	/**
	 * 礼包通用码
	 */
	@TableField(value = "gift_code")
	private String giftCode;
	/**
	 * 代金券有效期类型：1永久有效，2固定时间，3指定天数
	 */
	@TableField(value = "voucher_type")
	private Integer voucherType;
	/**
	 * 代金券领取后有效天数
	 */
	@TableField(value = "voucher_days")
	private Integer voucherDays;
	/**
	 * 代金券有效开始时间
	 */
	@TableField(value = "voucher_start_time")
	private Date voucherStartTime;
	/**
	 * 代金券有效结束时间
	 */
	@TableField(value = "voucher_end_time")
	private Date voucherEndTime;
	/**
	 * 代金券限制类型 1:无门槛 2:指定游戏 3:指定角色
	 */
	@TableField(value = "cdk_limit_type")
	private Integer cdkLimitType;
	/**
	 * 排序
	 */
	@TableField(value = "sort")
	private Integer sort;
	/**
	 * 红包icon地址
	 */
	@TableField(value = "icon_url")
	private String iconUrl;
	/**
	 * 红包总数是否显示：0不显示，1显示
	 */
	@TableField(value = "amount_show")
	private Integer amountShow;
	/**
	 * 是否删除：0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

	/**
	 * 游豆类型 1，普通 2，临时
	 */
	@TableField(value = "currency_type")
	private Integer currencyType;

	/**
	 * 游豆
	 */
	@TableField(value = "currency_amount")
	private BigDecimal currencyAmount;


	/**
	 * 游戏任务ID
	 */
	@TableField(value = "condition_task_id")
	private Long conditionTaskId;

	//是否自动领取，1-是；2-否
	@TableField(value = "auto_receive")
	private Integer autoReceive;
}

	

	
	

