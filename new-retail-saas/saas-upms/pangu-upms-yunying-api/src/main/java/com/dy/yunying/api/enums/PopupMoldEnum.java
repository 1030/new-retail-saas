package com.dy.yunying.api.enums;

import java.util.Objects;

/**
 * @author chenxiang
 * @className PushTypeEnum
 * @date 2022-4-25 17:37
 */
public enum PopupMoldEnum {

	SINGLE(1, "单个奖励"),
	POPOUT(2, "弹框"),
	IMAGE(3, "图文"),
	BANNER(4, "通栏"),
	MSG(5, "消息");

	private Integer type;

	private String name;

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	private PopupMoldEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(Integer type) {
		if (Objects.isNull(type)) {
			return null;
		}
		for (PopupMoldEnum ele : values()) {
			if (ele.getType().equals(type)) {
				return ele.getName();
			}
		}

		return null;
	}

}
