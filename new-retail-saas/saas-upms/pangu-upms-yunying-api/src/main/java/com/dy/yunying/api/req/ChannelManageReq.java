package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

public class ChannelManageReq extends Page {

	private String chnname; // 主渠道名称

	private String chncode; // 主渠道ID

	public String getChnname() {
		return chnname;
	}

	public void setChnname(String chnname) {
		this.chnname = chnname;
	}

	private Integer sonChannelId; //查询子渠道传过来的   主渠道id

	private String soncode;

	private String sonname;

	private String agent;

	private Long manage;

	private String manageStr;

	private String agentname;

	private String managename;

	public Integer getSonChannelId() {
		return sonChannelId;
	}

	public void setSonChannelId(Integer sonChannelId) {
		this.sonChannelId = sonChannelId;
	}

	public String getSoncode() {
		return soncode;
	}

	public void setSoncode(String soncode) {
		this.soncode = soncode;
	}

	public String getSonname() {
		return sonname;
	}

	public void setSonname(String sonname) {
		this.sonname = sonname;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public Long getManage() {
		return manage;
	}

	public void setManage(Long manage) {
		this.manage = manage;
	}

	public String getAgentname() {
		return agentname;
	}

	public void setAgentname(String agentname) {
		this.agentname = agentname;
	}

	public String getManagename() {
		return managename;
	}

	public void setManagename(String managename) {
		this.managename = managename;
	}

	public String getChncode() {
		return chncode;
	}

	public void setChncode(String chncode) {
		this.chncode = chncode;
	}

	public String getManageStr() {
		return manageStr;
	}

	public void setManageStr(String manageStr) {
		this.manageStr = manageStr;
	}
}
