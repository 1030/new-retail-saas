package com.dy.yunying.api.req.yyz;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment
 */
@Data
public class SendMassageReq implements Serializable {

	/***
	 *   主键id
	 */
	private String id;

	/***
	 *   手机号
	 */
	private String mobile;

	/***
	 *   多个手机号
	 */
	private String mobiles;

	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	private String type;

	/**
	 * 发送类型,匹配数据库
	 */
	private String code;

	/**
	 * 开始时间
	 */
	private String appointStartTime;

	/**
	 * 结束时间
	 */
	private String appointEndTime;

	/**
	 * 手机系统
	 */
	private String appointDevice;

}
