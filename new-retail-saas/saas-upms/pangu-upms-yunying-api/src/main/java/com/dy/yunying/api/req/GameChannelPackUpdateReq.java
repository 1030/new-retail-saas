package com.dy.yunying.api.req;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 渠道包更新请求
 *
 * @Author: hjl
 * @Date: 2020/6/11 20:59
 */
@Getter
@Setter
public class GameChannelPackUpdateReq implements Serializable {

    /**
     * 分包记录ID
     */
    private Long packId;

    /**
     *
     */
    private String creator;

}