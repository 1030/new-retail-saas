package com.dy.yunying.api.resp.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dy.yunying.api.entity.hongbao.HbCashConfig;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 资金统计列表
 * @date 2021/10/27 19:19
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CashConfigVo extends HbCashConfig {
	@ApiModelProperty(value = "礼包码")
	private String giftBagCodes;
}
