package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;




/**
 * 红包角色关联信息表
 * @author  chenxiang
 * @version  2021-11-18 15:03:20
 * table: hb_redpack_role_info
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "hb_redpack_role_info")
public class HbRedpackRoleInfo extends Model<HbRedpackRoleInfo>{

	/**
	 * 主键id
	 */
	@TableField(value = "id")
	private Long id;
	/**
	 * 活动ID
	 */
	@TableField(value = "activity_id")
	private Long activityId;
	/**
	 * 红包ID
	 */
	@TableField(value = "redpack_id")
	private Long redpackId;
	/**
	 * 游戏角色ID
	 */
	@TableField(value = "role_id")
	private String roleId;
	/**
	 * 主游戏ID
	 */
	@TableField(value = "pgame_id")
	private Long pgameId;
	/**
	 * 子游戏ID
	 */
	@TableField(value = "game_id")
	private Long gameId;
	/**
	 * 游戏区服ID
	 */
	@TableField(value = "area_id")
	private Long areaId;
	/**
	 * 用户ID
	 */
	@TableField(value = "user_id")
	private Long userId;
	/**
	 * 领取状态：0-不可领；1-可领取; 2-已领取；
	 */
	@TableField(value = "receive_status")
	private Integer receiveStatus;
	/**
	 * 可领取数量
	 */
	@TableField(value = "receivable_count")
	private Integer receivableCount;
	/**
	 * 已领取数量
	 */
	@TableField(value = "received_count")
	private Integer receivedCount;
	/**
	 * 是否删除：0-否；1-是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

