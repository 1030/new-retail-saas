package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 活动上下线
 * @date 2021/10/23 11:04
 */
@Data
public class OnlineStatusReq implements Serializable {

	@ApiModelProperty(value = "ID")
	@NotNull(message = "ID不能为空")
	private Long id;

	@ApiModelProperty(value = "状态(2上线 3下线)")
	@NotNull(message = "状态不能为空")
	private Integer status;

}
