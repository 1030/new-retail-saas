package com.dy.yunying.api.resp.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 资产统计数据
 * @date 2021/10/26 18:11
 */
@Data
public class RedPackCountData implements Serializable {

	@ApiModelProperty(value = "红包类型：1现金红包，2礼包码，3实物礼品，4代金券")
	private Integer type;

	@ApiModelProperty(value = "奖池价值(元)")
	private BigDecimal rewardCost;

	@ApiModelProperty(value = "注资金额(元)")
	private BigDecimal investMoney;

	@ApiModelProperty(value = "消耗金额(元)")
	private BigDecimal expendBalance;

}
