package com.dy.yunying.api.vo;

import com.dy.yunying.api.constant.OsEnum;
import com.pig4cloud.pig.common.core.util.BigDecimalUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * @ClassName AdKpi
 * @Description 绩效管理-kpi
 * @Author nieml
 * @Time 2021/7/8 10:52
 * @Version 1.0
 **/

@Data
@ApiModel(value = "绩效管理kpi报表")
public class AdKpiVo {

	/**
	 * 层级：1 部门；2 组别；3 投放人
	 */
	@ApiModelProperty(value="层级：1 部门；2 组别；3 投放人")
	private Integer hierarchy;

	/**
	 * 所选层级的id：部门id|组别id|投放人id
	 */
	@ApiModelProperty(value="所选层级的id：部门id|组别id|投放人id")
	private Integer hierarchyId;
	@ApiModelProperty(value="所选层级的名称")
	private String hierarchyName;
	/**
	 * 年月
	 */
	@ApiModelProperty(value="年月")
	private Integer yearMonth;

	/**
	 * 主游戏id
	 */
	@ApiModelProperty(value="主游戏id")
	private Integer pgid;
	private String parentGameName;
	/**
	 * 系统
	 */
	@ApiModelProperty(value="系统")
	private Integer os;
	public String getOsStr () {
		if (Objects.isNull(os)){
			return "未知";
		}
		String osStr = OsEnum.getName(os);
		return osStr;
	}

	/**
	 * 目标量级
	 */
	@ApiModelProperty(value="目标量级")
	private Long targetLevel;

	/**
	 * 目标ROI
	 */
	@ApiModelProperty(value="目标ROI")
	private BigDecimal targetRoi;

	/**
	 * 目标ROI-30
	 */
	@ApiModelProperty(value="目标ROI-30")
	private BigDecimal targetRoi30;

	/**
	 * 目标新用户月流水
	 */
	@ApiModelProperty(value="目标新用户月流水")
	private Long targetNewuserMonthWater;

	/**
	 * 目标老用户月流水
	 */
	@ApiModelProperty(value="目标老用户月流水")
	private Long targetOlduserMonthWater;

	/**
	 * 实际指标
	 * */
	@ApiModelProperty("消耗后消耗（总成本）")
	private BigDecimal cost = new BigDecimal(0);

	@ApiModelProperty("首日新增充值金额")
	private Long worth1 ;

	@ApiModelProperty("30天累计 新增充值金额")
	private Long worth30 ;

	@ApiModelProperty("累计充值金额")
	private Long totalPayfee ;

	/**
	 * 实量级
	 */
	@ApiModelProperty(value="实量级")
	private Long realLevel;

	/**
	 * 实ROI
	 */
	@ApiModelProperty(value="实ROI")
	private BigDecimal realRoi;

	public BigDecimal getRealRoi() {
		if (Objects.isNull(totalPayfee) || Objects.isNull(cost)){
			return new BigDecimal(0);
		}
		BigDecimal totalPayfeeB = new BigDecimal(totalPayfee);
		realRoi = BigDecimalUtil.getRatio(totalPayfeeB, cost);
		return realRoi;
	}

	/**
	 * 实ROI-30
	 */
	@ApiModelProperty(value="实ROI-30")
	private BigDecimal realRoi30;

	public BigDecimal getRealRoi30() {
		if (Objects.isNull(worth30) || Objects.isNull(cost)){
			return new BigDecimal(0);
		}
		BigDecimal worth30B = new BigDecimal(worth30);
		realRoi30 = BigDecimalUtil.getRatio(worth30B, cost);
		return realRoi30;
	}

	/**
	 * 实新用户月流水
	 */
	@ApiModelProperty(value="实新用户月流水")
	private Long realNewuserMonthWater;

	public Long getRealNewuserMonthWater() {
		if (Objects.isNull(worth1)){
			return 0L;
		}
		return worth1;
	}

	/**
	 * 实老用户月流水
	 */
	@ApiModelProperty(value="实老用户月流水")
	private Long realOlduserMonthWater;

	public Long getRealOlduserMonthWater() {
		if (Objects.isNull(totalPayfee) || Objects.isNull(worth1)){
			return 0L;
		}
		return totalPayfee - worth1;
	}
}
