package com.dy.yunying.api.resp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 代金券导出数据配置表
 * @author  chenxiang
 * @version  2022-04-18 16:23:05
 * table: cdk_grant_config
 */
@Data
public class WanCdkConfigRes extends Model<WanCdkConfigRes>{
	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 配置名称
	 */
	private String title;
	/**
	 * 指定天数类型：1当天，2指定天
	 */
	private Integer timeType;
	/**
	 * 开始日期
	 */
	private Date startTime;
	/**
	 * 结束日期
	 */
	private Date endTime;
	/**
	 * 最小等级
	 */
	private Integer minLevel;
	/**
	 * 最大等级
	 */
	private Integer maxLevel;
	/**
	 * 最小充值金额
	 */
	private BigDecimal minMoney;
	/**
	 * 最大充值金额
	 */
	private BigDecimal maxMoney;
	/**
	 * 父游戏ID(0为所有游戏)
	 */
	private Integer parentGameId;
	/**
	 * 子游戏ID(0为所有游戏)
	 */
	private Integer gameId;
	/**
	 * 排序
	 */
	private String orderBy;

	private String startTimeStr;
	private String endTimeStr;
}

	

	
	

