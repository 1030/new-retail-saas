package com.dy.yunying.api.enums;

/**
 * @description: 活动类型
 * @author yuwenfeng
 * @date 2021/11/30 21:01
 */
public enum SourceTypeEnum {

	HB(1,"红包活动"),
	SIGN(2,"签到活动"),
	OUT(3,"外部链接活动"),
	RAFFLE(4,"抽奖活动");

	SourceTypeEnum(Integer type, String desc){
		this.type =type;
		this.desc =desc;
	}

	private Integer type;

	private String desc;

	public Integer getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}
}
