package com.dy.yunying.api.datacenter.dto;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：lile
 * @date ：2021/6/22 11:02
 * @description：
 * @modified By：
 */
@Data
public class AdRecoveryDto {

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	@ApiModelProperty("是否系统管理员")
	private int isSys;

	@ApiModelProperty("设备激活时间（开始）/广告统计查询时间")
	@NotNull(message = "查询开始日期不允许为空")
	private Long rsTime;

	@ApiModelProperty("设备激活时间（结束）/广告统计查询时间")
	@NotNull(message = "查询结束日期不允许为空")
	private Long reTime;

	@ApiModelProperty("主渠道")
	private String parentchlArr;

	@ApiModelProperty("分包渠道")
	private String appchlArr;


	@ApiModelProperty("部门id数组,多个用逗号分隔")
	private String deptIdArr;
	@ApiModelProperty(value = "用户组Id列表")
	private String userGroupIdArr;
	@ApiModelProperty("客户端操作系统0=android；1=IOS；3=OTHERS或为空；4=H5; 5=PC")
	private Integer os;


	@ApiModelProperty("投放人数组")
	private String investorArr;

	@ApiModelProperty("子游戏")
	private String gameidArr;

	@ApiModelProperty("父游戏")
	private String pgidArr;

	/**
	 * 广告账户ID，使用逗号分隔
	 */
	@ApiModelProperty("广告账户ID")
	private String advertiserIdArr;

	/**
	 * 广告计划ID，使用逗号分隔
	 */
	private String adidArr;

	/**
	 *  转化目标
	 */
	private String convertArr;

	/**
	 *  深度转化目标
	 */
	private String deepConvertArr;

	/**
	 *  转化统计方式
	 */
	private String convertDataTypeArr;

	/**
	 *  代理商
	 */
	private String agentIdArr;



	@ApiModelProperty("周期类型 ：1 按天 2 按周  3  按月 4 汇总（默认）")
	private Integer cycleType = 4;

	public String getPeriod() {
		String period = "collect";
		switch (cycleType) {
			case 1:
				period = "day";
				break; //可选
			case 2:
				period = "week";
				//语句
				break; //可选
			case 3:
				period = "month";
				break; //可选
			case 4:
				period = "collect";
				break; //可选
			//你可以有任意数量的case语句
			default: //可选
				//语句
		}
		return period;
	}

	@ApiModelProperty("类别 多个用逗号分隔【系统 os,主游戏 pgid，子游戏 gid ,部门 deptId,渠道编码 parentchl，分包编码：appchl,广告计划 adid,  投放人investor   】")
	private String queryColumn;

	@ApiModelProperty("是否显示分成前的数据 ：0 否 1：是")
	private Integer showRatio = 0;


	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	public String getAdAccounts() {
		String adAccounts = "'NO'";  // 默认不查询任何广告账户
		if (CollectionUtil.isNotEmpty(roleAdAccountList)) {
			adAccounts = "'" + StringUtils.join(roleAdAccountList, "','") + "'";
		}
		return adAccounts;
	}

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

	private Boolean enableTest = false;

	private Long current = 1L;
	private Long size = 10000L;

	//排序
	private String sort;

	//排序 指标字段值
	private String kpiValue;

	private String titles;

	private String columns;


	@ApiModelProperty("指标")
	private Integer retentionKpi;
}
