package com.dy.yunying.api.req.yyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * 游戏预约进度档位奖励表
 * game_appointment_schedule_gears_reward
 * @author kongyanfang
 * @date 2022-08-18 13:46:52
 */
@Getter
@Setter
public class GameAppointmentScheduleGearsList {

	/**
	 * 奖励内容
	 */
	private String rewardName;

	/**
	 * 奖励数量
	 */
	private String rewardNum;


	/**
	 * 文件桶
	 */
	private String bucketName;

	/**
	 * 文件访问地址
	 */
	private String fileUrl;

	/**
	 * 排序
	 */
	private Long sort;
	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	private String type;
}