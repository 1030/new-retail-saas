package com.dy.yunying.api.entity.sign;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 签到活动物品表
 * @author  chengang
 * @version  2021-12-01 10:14:07
 * table: sign_activity_goods
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sign_activity_goods")
public class SignActivityGoods extends Model<SignActivityGoods>{

	//columns START
			//主键id
			@TableId(value = "id",type = IdType.AUTO)
			private Long id;
			

			//编码
			@TableField(value = "code")
			private String code;
			

			//名称
			@TableField(value = "name")
			private String name;
			

			//图片地址
			@TableField(value = "url")
			private String url;
			

			//是否删除  0否 1是
			@TableField(value = "deleted")
			private Integer deleted;
			

			//创建时间
			@TableField(value = "create_time")
			private Date createTime;
			

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;
			

			//创建人
			@TableField(value = "create_id")
			private Long createId;
			

			//修改人
			@TableField(value = "update_id")
			private Long updateId;
			

	//columns END 数据库字段结束
	

	
}

	

	
	

