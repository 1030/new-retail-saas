package com.dy.yunying.api.enums;

import cn.hutool.core.util.StrUtil;
import com.pig4cloud.pig.common.core.exception.CheckedException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @ClassName SqlTemplate
 * @Description todo
 * @Author nieml
 * @Time 2023/1/16 17:19
 * @Version 1.0
 **/
@Getter
@RequiredArgsConstructor
public enum SqlTemplate {

	HOURDATA("生成分时报表查询sql"){
		@Override
		public String sql(Map<String, String> param, QueryEngine queryEngine) {
			String sql = null;
			String template = null;
			if (StringUtils.isBlank(param.get("tableName"))) {
				throw new CheckedException("tableName不能为空！");
			}
			if (StringUtils.isBlank(param.get("filterCondition"))) {
				param.put("filterCondition", " 1=1");
			}

			switch (queryEngine) {
				case DORIS:
					template = " select {columnList} {alias}dx_user_id as m_user_id " +
							" {dateTime}" +
							" from {tableName}" +
							" where {filterCondition} "
					;
					break;
				default:
					break;
			}

			sql = StrUtil.format(template, param);
			return String.format(" ( %s ) %s  ", sql, tableAlias());
		}

		@Override
		public String tableAlias() {
			return "ar";
		}

		@Override
		public String onCondition() {
			return StringUtils.EMPTY;
		}

		@Override
		public String join() {
			return StringUtils.EMPTY;
		}
	},

	;

	public abstract String sql(Map<String, String> param, QueryEngine queryEngine);


	public abstract String tableAlias();


	public abstract String onCondition();


	public abstract String join();

	private final String description;

}
