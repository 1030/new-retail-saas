/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 提现档次表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:19
 */
@Data
@TableName("hb_cash_config")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "提现档次表")
public class HbCashConfig extends HbBaseEntity {

	/**
	 * 提现档次ID
	 */
	@TableId(value = "id",type = IdType.AUTO)
	@ApiModelProperty(value = "提现档次ID")
	private Long id;

	/**
	 * 活动ID
	 */
	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	/**
	 * 提现类型，1-游戏货币；2-微信；3-支付宝；4-代金券；5-游豆；
	 */
	@ApiModelProperty(value = "提现类型(1游戏货币 2微信 3支付宝 4代金券)")
	private Integer cashType;

	/**
	 * 提现档次
	 */
	@ApiModelProperty(value = "提现档次")
	private String cashName;

	/**
	 * 现金价值(元)
	 */
	@ApiModelProperty(value = "现金价值(元)")
	private BigDecimal cashMoney;

	/**
	 * 角标开关(1开 0关)
	 */
	@ApiModelProperty(value = "角标开关(1开 0关)")
	private Integer subscriptOff;

	/**
	 * 角标内容
	 */
	@ApiModelProperty(value = "角标内容")
	private String subscriptName;
	/**
	 * 礼包码类型：1唯一码，2通用码
	 */
	@ApiModelProperty(value = "礼包码类型：1唯一码，2通用码")
	private Integer giftType;
	/**
	 * 礼包通用码
	 */
	@ApiModelProperty(value = "礼包通用码")
	private String giftCode;
	/**
	 * 总数量（-1不限制）
	 */
	@ApiModelProperty(value = "总数量（-1不限制）")
	private Integer giftAmount;

	/**
	 * 代金券名称
	 */
	private String cdkName;
	/**
	 * 代金券金额
	 */
	private String cdkAmount;
	/**
	 * 限制金额
	 */
	private String cdkLimitAmount;
	/**
	 * 有效规则类型  1固定时间 2领券后指定天数
	 */
	private Integer expiryType;
	/**
	 * 有效开始时间
	 */
	private String startTime;
	/**
	 * 有效结束时间
	 */
	private String endTime;
	/**
	 * 有效天数
	 */
	private Integer days;

	/**
	 * 提现金额
	 */
	@ApiModelProperty(value = "提现金额")
	private BigDecimal currencyAmount;

	@ApiModelProperty(value = "代金券限制类型 1:无门槛 2:指定游戏 3:指定角色")
	private Integer cdkLimitType;
}
