package com.dy.yunying.api.vo.hongbao;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class HbWithdrawRecordDetailVo implements Serializable {

	private static final long serialVersionUID = -3301109998641913698L;

	//当前等级
	private Integer roleLevel;
	/**
	 * 活动期间累计充值
	 */
	private BigDecimal totalRechargeAmount;
	/**
	 * 注册时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date regtime;


	//创角时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createRoleTime;


	//最后付费时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date payTime;


	//最后登录时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date loginTime;

	//角色最后登录IP
	private String loginIp;
}
