package com.dy.yunying.api.datacenter.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 游戏
 * wan_game
 * @author kongyanfang
 * @date 2020-07-22 15:21:57
 */
@Getter
@Setter
public class WanGameVersionVO {
    /**
     * 游戏ID
     */
    private Long id;

    /**
     * 游戏名称
     */
    private String gname;

    /**
     * 父游戏ID
     */
    private Long pgid;

    /**
     * 父游戏名称
     */
    private String pgname;

    /**
     * 游戏版本ID
     */
    private Long versionId;

    /**
     * 版本编码，数字，如56
     */
    private Integer versionCode;

    /**
     * 版本名称，字符串，如：v3.2.5
     */
    private String versionName;

    /**
     * 包名
     */
    private String pkName;

    /**
     * 包签名串
     */
    private String pkNameSign;
}