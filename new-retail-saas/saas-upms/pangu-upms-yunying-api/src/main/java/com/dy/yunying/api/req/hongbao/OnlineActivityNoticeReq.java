package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 活动公告上下线
 * @date 2021/10/23 11:04
 */
@Data
public class OnlineActivityNoticeReq implements Serializable {

	@ApiModelProperty(value = "活动公告ID")
	@NotNull(message = "活动公告ID不能为空")
	private Long id;

	@ApiModelProperty(value = "公告状态(2上线 3下线)")
	@NotNull(message = "公告状态不能为空")
	private int noticeStatus;

}
