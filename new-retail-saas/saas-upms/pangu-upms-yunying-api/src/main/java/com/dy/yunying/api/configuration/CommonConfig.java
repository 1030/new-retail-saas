package com.dy.yunying.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 公用配置
 *
 * @Author: hjl
 * @Date: 2020/9/27 16:43
 */
@Configuration
public class CommonConfig {

    @Bean
    public TaskExecutor getTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        // 线程池维护线程的最少数量
        taskExecutor.setCorePoolSize(5);
        // 线程池维护线程的最大数量
        taskExecutor.setMaxPoolSize(10);
        // 线程池所使用的缓冲队列
        taskExecutor.setQueueCapacity(20);
        // 线程池维护线程所允许的空闲时间
        taskExecutor.setKeepAliveSeconds(60000);

        return taskExecutor;
    }
}