package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName HbActivityTypeEnum.java
 * @createTime 2021年10月26日 10:35:00
 */
public enum HbReceivStatusEnum {
	STATUS1("1", "已到账"),
	STATUS2("2", "待填写地址"),
	STATUS3("3", "待审核"),
	STATUS4("4", "待发货"),
	STATUS5("5", "已发货"),
	STATUS6("6", "已收货"),
	STATUS7("7", "已驳回");

	private String type;
	private String name;
	public String getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private HbReceivStatusEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(String type) {
		if (StringUtils.isBlank(type)){
			return null;
		}
		for (HbReceivStatusEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return null;
	}
}
