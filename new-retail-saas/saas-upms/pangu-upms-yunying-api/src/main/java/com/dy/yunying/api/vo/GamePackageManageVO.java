package com.dy.yunying.api.vo;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class GamePackageManageVO {

    /**
     * 分包渠道ID
     */
    private Long packId;

    /**
     * 子游戏ID
     */
    private Long gameId;

    /**
    *  子游戏名称
     */
    private String gname;

    /**
     * 主渠道
     */
    private String chnname;
    /**
     * 主渠道ID
     */
    private Integer platformId;

    /**
     * 第三方APPID
     */
    private String appId;

    /**
     * 第三方APPNAME
     */
    private String appName;

    /**
     * 上报方式
     */
    private Integer rtype;
    /**
     * 广点通appid
     */
    private String gdtAppid;
    /**
     * 状态
     */
    private Integer status;

    private Long pgid;

    public String getRtypeStr () {
        String rtypeStr = "";
        switch (rtype) {
            case 0 :
                rtypeStr = "不上报";
                break;
            case 1 :
                rtypeStr = "API上报";
                break;
            case 2 :
                rtypeStr = "SDK上报";
                break;
            default:
                rtypeStr = "";
        }
        return rtypeStr;
    }

    /**
     *
     */
    private String path;

    private String url;
}
