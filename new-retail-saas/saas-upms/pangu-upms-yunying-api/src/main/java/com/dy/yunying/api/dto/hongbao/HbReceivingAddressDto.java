package com.dy.yunying.api.dto.hongbao;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 实物奖品领取地址记录
 * @author  chenxiang
 * @version  2021-10-25 14:20:46
 * table: hb_receiving_address
 */
@Data
public class HbReceivingAddressDto implements Serializable {
	
		//主键ID
		private Long id;
		//领取记录ID
		private Long recordId;
		//收货姓名
		private String receiveUserName;
		//收货电话
		private String receiveUserMobile;
		//收货地址
		private String receiveUserAddress;
		//省编码
		private String addressProvinceCode;
		//省名称
		private String addressProvinceName;
		//市编码
		private String addressCityCode;
		//市名称
		private String addressCityName;
		//区编码
		private String addressAreaCode;
		//区名称
		private String addressAreaName;
		//发货时间
		private Date sendTime;
		//快递名称
		private String expressName;
		//快递订单号
		private String expressCode;
		//是否删除：0否 1是
		private Integer deleted;
		//创建时间
		private Date createTime;
		//修改时间
		private Date updateTime;
		//创建人
		private Long createId;
		//修改人
		private Long updateId;
	
	
}


