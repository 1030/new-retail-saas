package com.dy.yunying.api.req.yyz;

import lombok.Data;

import java.io.Serializable;

/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment
 */
@Data
public class GameCodeReq implements Serializable {

	/***
	 *   主键id
	 */
	private Long id;

	/***
	 *   游戏编码
	 */
	private String gameCode;

	/**
	 * 游戏编码内容
	 */
	private String game;
}
