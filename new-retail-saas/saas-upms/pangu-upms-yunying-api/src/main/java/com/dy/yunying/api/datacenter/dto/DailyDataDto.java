package com.dy.yunying.api.datacenter.dto;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sunyq
 * @date 2022/6/21 13:49
 */
@Data
@ApiModel("每日运营查询对象")
public class DailyDataDto {
	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}
	@ApiModelProperty("查询开始时间")
	@NotNull(message = "查询开始日期不允许为空")
	private String startTime;

	@ApiModelProperty("查询结束时间")
	@NotNull(message = "查询结束日期不允许为空")
	private String endTime;

	@ApiModelProperty("父游戏")
	private String pgidArr;

	@ApiModelProperty("子游戏")
	private String gameidArr;

	@ApiModelProperty("主渠道列表")
	private String parentchlArr;

	@ApiModelProperty("子渠道编码列表")
	private String chlArr;

	@ApiModelProperty("分包编码列表")
	private String appchlArr;

	@ApiModelProperty("查询的字段 多个用逗号分隔  ")
	private String columns;

	@ApiModelProperty("列名")
	private String titles;

	@ApiModelProperty("当前页")
	private Long current;

	@ApiModelProperty("每页的size")
	private Long size;

	@ApiModelProperty("排序  desc / asc")
	private String sort;

	@ApiModelProperty("排序字段")
	private String kpiValue;
}
