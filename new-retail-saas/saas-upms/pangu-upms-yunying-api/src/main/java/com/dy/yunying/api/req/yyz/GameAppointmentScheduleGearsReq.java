package com.dy.yunying.api.req.yyz;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.List;


/**
 * 游戏预约进度档位表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_gears
 */
@Data
public class GameAppointmentScheduleGearsReq  extends Page {

	/***
	 *   主键id
	 */
	private String id;

	@ApiModelProperty(value = "预约类型")
	private String type;

	/**
	 *
	 */
	private String gameName;

	/**
	 * 规则人数
	 */
	private Long ruleNumber;


	/**
	 * 传入的奖励list
	 */
	private List<GameAppointmentScheduleGearsList>  rewardList;

}
