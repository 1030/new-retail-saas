package com.dy.yunying.api.resp.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 资金统计列表
 * @date 2021/10/27 19:19
 */
@Data
public class AssetsData implements Serializable {

	@ApiModelProperty(value = "奖池总价值(元)")
	private BigDecimal totalCost;

	@ApiModelProperty(value = "注资总额(元)")
	private BigDecimal totalInvestMoney;

	@ApiModelProperty(value = "消耗金额(元)")
	private BigDecimal expendBalance;

	@ApiModelProperty(value = "红包统计列表")
	private List<RedPackCountData> activityCountList;

	@ApiModelProperty(value = "礼包列表")
	private List<GiftVoData> giftCountList;
}
