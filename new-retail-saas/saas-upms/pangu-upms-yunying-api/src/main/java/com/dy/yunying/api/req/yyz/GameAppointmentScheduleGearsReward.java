package com.dy.yunying.api.req.yyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * 游戏预约进度档位奖励表
 * game_appointment_schedule_gears_reward
 * @author kongyanfang
 * @date 2022-08-18 13:46:52
 */
@Getter
@Setter
public class GameAppointmentScheduleGearsReward {

	/**
	 * 预约类型
	 */
	private String type;

	/**
	 * 规则id
	 */
	private String ruleId;

	/**
	 * 规定要求人数
	 */
	private Long ruleNumber;


	/**
	 * 奖励名称
	 */
	private String rewardName;

	/**
	 * 奖励数量
	 */
	private String rewardNum;

	/**
	 * 文件桶
	 */
	private String bucketName;

	/**
	 * 文件访问地址
	 */
	private String fileUrl;

	/**
	 * 排序
	 */
	private Long sort;

	/**
	 * 是否删除: (1:表示不删除, 2:表示删除)
	 */
	private Byte isDelete;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 修改时间
	 */
	private Date updateTime;
}