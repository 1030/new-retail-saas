package com.dy.yunying.api.constant;

/**
 * 终端类型（游戏端口）
 *
 * @author hjl
 * @date 2020-5-7 20:00:47
 */
public enum OsEnum {

	ANDROID(0, "安卓"),
	IOS(1, "IOS"),
	OTHERS(3, "其他"),
	H5(4, "H5"),
	PC(5, "PC");

	private OsEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(Integer type) {
		for (OsEnum ele : values()) {
			if (ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return "-";
	}

	private Integer type;

	private String name;

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
