package com.dy.yunying.api.db.user;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
import java.util.Date;

/**
 * mongodb user
 *
 * @author chengjin
 * @version 2022-05-11 11:07:40
 * table: wan_user
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Document(collection = "user_info")
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 8173107766101761015L;
    /**
     * 玩家用户ID
     */
    @Id
    private Long id;
    /***
     * 所属平台 3367、3399、dogame
     */
    private String platformCode;
    /**
     * 用户名
     */
    private String username;
    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 盐
     */
    private String salt;


    /**
     * 论坛昵称
     */
    private String nickname;

    /**
     * 真实名称
     */
    private String realname;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 身份证号码 脱敏的
     */
    private String idcard;
    /**
     * 身份证号码 aes加密的
     */
    private String idCardEncrypt;

    /**
     * 出生日期
     */
    private Date birthday;
    /**
     * 性别,0表示未知，1表示男，2表示女
     */
    private Integer gender;

    /**
     * 注册ip
     */
    private String regIp;

    /**
     * 状态，1表示正常，0表示删除
     */
    private Integer status;
    /**
     * 认证状态
     * 实名状态 0.认证成功 1.认证中  2.认证失败  3.未认证  10.未知  11.待认证
     */
    private Integer realnameStatus;

    /**
     * 是否删除 0：未删除  1：已删除
     */
    private Integer isDeleted;

    /**
     * 注册时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;
    
    
}






