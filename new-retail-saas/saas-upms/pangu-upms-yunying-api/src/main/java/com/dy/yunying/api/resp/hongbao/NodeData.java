package com.dy.yunying.api.resp.hongbao;

import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 下拉数据
 * @date 2021/10/26 18:11
 */
@Data
public class NodeData implements Serializable {

	private String nodeId;

	private String nodeName;

	private List<NodeData> childNodeList;

}
