package com.dy.yunying.api.resp;

import lombok.Data;

@Data
public class WanCdkExportData {
	private String title;
	private String userName;
	private String areaId;
	private String roleId;
	private String roleLevel;
	private String rechargeAmount;
}
