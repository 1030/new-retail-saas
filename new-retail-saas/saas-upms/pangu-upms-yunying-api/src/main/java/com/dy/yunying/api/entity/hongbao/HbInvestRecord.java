/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 注资记录表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:19
 */
@Data
@TableName("hb_invest_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "注资记录表")
public class HbInvestRecord extends HbBaseEntity {

	/**
	 * 注资记录ID
	 */
	@TableId(value = "id",type = IdType.AUTO)
	@ApiModelProperty(value = "注资记录ID")
	private Long id;

	/**
	 * 活动ID
	 */
	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	/**
	 * 活动余额(元)
	 */
	@ApiModelProperty(value = "活动余额(元)")
	private BigDecimal activityBalance;

	/**
	 * 注资类型(1现金红包 2礼包码 3实物礼品 4代金券)
	 */
	@ApiModelProperty(value = "注资类型(1现金红包 2礼包码 3实物礼品 4代金券)")
	private Integer investType;

	/**
	 * 注资金额(元)
	 */
	@ApiModelProperty(value = "注资金额(元)")
	private BigDecimal investMoney;

	/**
	 * 注资状态(1审核中 2已到账 3不通过)
	 */
	@ApiModelProperty(value = "注资状态(1审核中 2已到账 3不通过)")
	private Integer applyStatus;

	/**
	 * 审核备注
	 */
	@ApiModelProperty(value = "审核备注")
	private String applyRemarke;

	/**
	 * 创建者
	 */
	@ApiModelProperty(value = "审核人")
	private Long applyId;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "审核时间")
	private Date applyTime;
}
