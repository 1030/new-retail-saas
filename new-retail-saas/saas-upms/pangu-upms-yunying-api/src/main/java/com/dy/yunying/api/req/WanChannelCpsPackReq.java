package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author sunyq
 * @date 2022/9/16 15:54
 */
@Data
public class WanChannelCpsPackReq extends Page implements Serializable {

	/**
	 * 游戏ID
	 */
	private Integer gameId;

	/**
	 * 游戏名称
	 */
	private String gameName;
	/**
	 * 主渠道编码
	 */
	private String parentCode;
	/**
	 * 主渠道id
	 */
	private Integer pid;
	/**
	 * 子渠道编码
	 */
	private String chlCode;
	/**
	 * 子渠道id
	 */
	private Integer chlId;
	/**
	 * 分包渠道名称、编码
	 */
	private String appChl;
	/**
	 * 用户id集合
	 */
	private List<Integer> userList;

}
