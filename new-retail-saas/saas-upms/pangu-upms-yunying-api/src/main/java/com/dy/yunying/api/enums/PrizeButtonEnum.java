package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 奖励按钮 1 去使用 2 复制
 *
 * @author sunyouquan
 * @date 2022/5/5 11:11
 */
public enum PrizeButtonEnum {

	USE(1, "去使用"),

	COPY(2, "复制");

	private Integer type;

	private String name;

	PrizeButtonEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonCreator
	public static PrizeButtonEnum getPrizeButtonEnum(Integer value) {
		for (PrizeButtonEnum prizeButtonEnum : values()) {
			if (prizeButtonEnum.type.equals(value)) {
				return prizeButtonEnum;
			}
		}
		return null;
	}

	@JsonValue
	public Integer getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}

}
