package com.dy.yunying.api.entity;

import lombok.Data;

/**
 * @author sunyq
 * @date 2022/10/11 15:12
 */
@Data
public class SubChannelInfoDO {
	/**
	 * 分包编码
	 */
	private String appChl;
	/**
	 * 分包编码名称
	 */
	private String appChlName;
	/**
	 * 子渠道编码
	 */
	private String subChl;
	/**
	 * 子渠道编码名称
	 */
	private String subChlName;
	/**
	 * 主渠道编码
	 */
	private String  parentCode;
}
