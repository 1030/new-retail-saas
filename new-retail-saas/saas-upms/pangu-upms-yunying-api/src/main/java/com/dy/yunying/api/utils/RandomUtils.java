package com.dy.yunying.api.utils;

import com.sjda.framework.common.utils.DateUtils;

import java.util.Date;
import java.util.Random;

/**
 * @author yuwenfeng
 * @description: 随机数
 * @date 2021/11/16 14:25
 */
public class RandomUtils {
	private static final String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	/**
	 * @description: 获取随机数, size:位数
	 * @author yuwenfeng
	 * @date 2021/11/16 14:26
	 */
	public static String getRandomNum(int size) {
		StringBuilder sb = new StringBuilder(4);
		for (int i = 0; i < size; i++) {
			char ch = str.charAt(new Random().nextInt(str.length()));
			sb.append(ch);
		}
		return sb.toString();
	}

	public static String getCdkNum() {
		return "cdk_" + DateUtils.dateToString(new Date(), DateUtils.YYYYMMDDHHMMSS) + RandomUtils.getRandomNum(4);
	}

}
