package com.dy.yunying.api.req.sign;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 新增/编辑对象
 * @date 2021/10/23 11:04
 */
@Data
public class SignActivityReq implements Serializable {

	@ApiModelProperty(value = "签到活动ID")
	private Long id;

	@ApiModelProperty(value = "签到活动名称")
	@NotBlank(message = "签到活动名称不能为空")
	@Length(min = 1, max = 10, message = "签到活动名称不能超过10位汉字")
	private String activityName;

	@ApiModelProperty(value = "父游戏ID")
	@NotNull(message = "父游戏ID不能为空")
	private Long parentGameId;

	@NotBlank(message = "开始时间不能为空")
	@ApiModelProperty(value = "开始时间(格式：yyyy-MM-dd)")
	private String startTime;

	@NotBlank(message = "结束时间不能为空")
	@ApiModelProperty(value = "结束时间(格式：yyyy-MM-dd)")
	private String finishTime;

	@ApiModelProperty(value = "活动规则")
	@NotBlank(message = "活动规则不能为空")
	private String activityRule;

	@NotNull(message = "区服范围集合不能为空")
	@ApiModelProperty(value = "区服范围集合")
	private Long[] areaList;
}
