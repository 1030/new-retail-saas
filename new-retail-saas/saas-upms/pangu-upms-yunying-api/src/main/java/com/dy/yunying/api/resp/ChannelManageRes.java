package com.dy.yunying.api.resp;

import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Api("渠道管理")
@ApiModel(value = "渠道管理")
public class ChannelManageRes {
	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	private Integer id;
	/**
	 * 推广渠道编码（pid!=0：第二级）
	 */
	@ApiModelProperty(value = "推广渠道编码")
	private String chncode;

	/**
	 * 推广渠道名称（pid!=0：第二级）
	 */
	@ApiModelProperty(value = "推广渠道名称")
	private String chnname;


	/**
	 * 平台类型 平台，0,3367平台，1头条，2百度,3 新数，4.公会
	 */
	@ApiModelProperty(value = "平台类型")
	private Integer platform;
	@ApiModelProperty(value = "平台类型名")
	private String platformName;

	public String getPlatformName() {
		return PlatformTypeEnum.descByValue(String.valueOf(platform));
	}
}
