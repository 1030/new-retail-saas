package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * WanUser
 * @author  zhuxm
 * @version  2022-01-13 16:01:46
 * table: wan_user
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "wan_user")
public class WanUser extends Model<WanUser>{
	/**
	 * 玩家用户ID
	 */
	@TableId(value = "userid",type = IdType.AUTO)
	private Long userid;
	/**
	 * 玩家编号
	 */
	@TableField(value = "userno")
	private String userno;
	/**
	 * 用户名
	 */
	@TableField(value = "username")
	private String username;
	/**
	 * 论坛昵称
	 */
	@TableField(value = "nickname")
	private String nickname;
	/**
	 * 用户密码
	 */
	@TableField(value = "password")
	private String password;
	/**
	 * 真实名称
	 */
	@TableField(value = "realname")
	private String realname;
	/**
	 * 头像
	 */
	@TableField(value = "picture")
	private String picture;
	/**
	 * 手机号码
	 */
	@TableField(value = "mobile")
	private String mobile;
	/**
	 * 身份证号码
	 */
	@TableField(value = "idcard")
	private String idcard;
	/**
	 * 出生日期
	 */
	@TableField(value = "birthday")
	private Date birthday;
	/**
	 * 性别,0表示男，1表示女，2表示其他
	 */
	@TableField(value = "gender")
	private Integer gender;
	/**
	 * 收入范围
	 */
	@TableField(value = "income")
	private String income;
	/**
	 * 省份
	 */
	@TableField(value = "province")
	private String province;
	/**
	 * 城市
	 */
	@TableField(value = "city")
	private String city;
	/**
	 * 区
	 */
	@TableField(value = "zones")
	private String zones;
	/**
	 * 详细地址
	 */
	@TableField(value = "address")
	private String address;
	/**
	 * 行业
	 */
	@TableField(value = "profession")
	private String profession;
	/**
	 * 积分余额
	 */
	@TableField(value = "points")
	private Integer points;
	/**
	 * 获取积分的总次数
	 */
	@TableField(value = "getcounts")
	private Integer getcounts;
	/**
	 * 消耗积分总次数
	 */
	@TableField(value = "discounts")
	private Integer discounts;
	/**
	 * 消耗积分总额
	 */
	@TableField(value = "dispoints")
	private Long dispoints;
	/**
	 * 注册时间 
	 */
	@TableField(value = "regtime")
	private Date regtime;
	/**
	 * 注册ip
	 */
	@TableField(value = "regip")
	private String regip;
	/**
	 * 登陆时间
	 */
	@TableField(value = "logintime")
	private Date logintime;
	/**
	 * 登陆IP
	 */
	@TableField(value = "loginip")
	private String loginip;
	/**
	 * 是否绑定手机
	 */
	@TableField(value = "isbang")
	private Integer isbang;
	/**
	 * 状态，1表示正常，0表示删除
	 */
	@TableField(value = "status")
	private Integer status;
	/**
	 * 修改时间
	 */
	@TableField(value = "updatetime")
	private Date updatetime;
	/**
	 * 是否完善信息
	 */
	@TableField(value = "isperfect")
	private Integer isperfect;
	/**
	 * 注册来源 0：pc网页端  1：h5   2:安卓手游 3:IOS手游
	 */
	@TableField(value = "regsrc")
	private Integer regsrc;
	/**
	 * 推荐人ID
	 */
	@TableField(value = "recuserid")
	private Long recuserid;
	/**
	 * 用户邮箱
	 */
	@TableField(value = "email")
	private String email;
	/**
	 * QQ号码
	 */
	@TableField(value = "qq")
	private String qq;
	/**
	 * 微信号
	 */
	@TableField(value = "weixin")
	private String weixin;
	/**
	 * 最近兑换物品
	 */
	@TableField(value = "exchangename")
	private String exchangename;
	/**
	 * 兑换次数
	 */
	@TableField(value = "exchangecount")
	private Integer exchangecount;
	/**
	 * qq号码加密唯一标识
	 */
	@TableField(value = "qqopenid")
	private String qqopenid;
	/**
	 * 微信号码加密唯一标识
	 */
	@TableField(value = "wxopenid")
	private String wxopenid;
	/**
	 * 充值金额
	 */
	@TableField(value = "fee")
	private BigDecimal fee;
	/**
	 * 第三方用户编号
	 */
	@TableField(value = "thduid")
	private String thduid;
	/**
	 * 第三方平台编号
	 */
	@TableField(value = "thdpid")
	private String thdpid;
	/**
	 * 腾讯id
	 */
	@TableField(value = "tencentid")
	private String tencentid;
	/**
	 * 新浪id
	 */
	@TableField(value = "sinaid")
	private String sinaid;
	/**
	 * 推广渠道
	 */
	@TableField(value = "channel")
	private String channel;
	/**
	 * 游戏账号
	 */
	@TableField(value = "gameaccount")
	private String gameaccount;
	/**
	 * 推广注册token
	 */
	@TableField(value = "token")
	private String token;
	/**
	 * 账号绑定状态(第三方登录) 0：未绑定，1：已绑定
	 */
	@TableField(value = "bindingstatus")
	private Integer bindingstatus;
	/**
	 * 用户组,1表示普通用户，2表示管理员用户
	 */
	@TableField(value = "user_group")
	private Integer userGroup;
	/**
	 * 二级登录密码
	 */
	@TableField(value = "secondPwd")
	private String secondPwd;
	/**
	 * 58币余额
	 */
	@TableField(value = "coin58")
	private Long coin58;
	/**
	 * vip等级(字典表code)
	 */
	@TableField(value = "dict_vip_code")
	private String dictVipCode;
	/**
	 * 成长值
	 */
	@TableField(value = "growth_value")
	private Long growthValue;
	/**
	 * 本月剩余补签次数
	 */
	@TableField(value = "monthsignnum")
	private Integer monthsignnum;
	/**
	 * 微信unionid
	 */
	@TableField(value = "wxunionid")
	private String wxunionid;
	/**
	 * 微信公众号openid
	 */
	@TableField(value = "pubwxopenid")
	private String pubwxopenid;
	/**
	 * 是否完善资料 1-是，0-否
	 */
	@TableField(value = "isperfection")
	private Integer isperfection;
	/**
	 * 0老数据，1新数据
	 */
	@TableField(value = "data_type")
	private Integer dataType;
}

	

	
	

