package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description: 代金券列表搜索
 * @author yuwenfeng
 * @date 2021/11/16 11:09
 */
@Data
public class SelectCdkPageReq implements Serializable {

	@ApiModelProperty(value="CDK码")
	private String cdkCode;

	@ApiModelProperty(value="创建者id")
	private Long createUserId;

	@ApiModelProperty(value="有效期类型 1:长期有效 2:指定有效期")
	private Integer expiryType;

	@ApiModelProperty(value="父游戏ID")
	private Long pgid;

	@ApiModelProperty(value = "子游戏id")
	private Long gameid;

	@ApiModelProperty(value="所属者账号")
	private String belongsName;

	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	private String finishTime;

	@ApiModelProperty(value = "金额")
	private BigDecimal money;

	@ApiModelProperty(value = "来源ID")
	private Long sourceId;

	@ApiModelProperty(value = "来源类型")
	private Integer sourceType;

	@ApiModelProperty(value = "使用状态：10、未使用；20、已使用")
	private Integer useStatus;

}
