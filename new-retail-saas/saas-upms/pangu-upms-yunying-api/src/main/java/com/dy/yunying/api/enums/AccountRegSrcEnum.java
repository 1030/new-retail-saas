package com.dy.yunying.api.enums;

/**
 * 0：Web 1：H5  2:Android 3:IOS
 * @author sunyq
 * @date 2022/8/22 16:53
 */
public enum AccountRegSrcEnum {
	WEB(0,"Web"),
	H5(1,"H5"),
	Android(2,"Android"),
	IOS(3,"IOS")

	;

	AccountRegSrcEnum(Integer type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	private Integer type;

	private String desc;

	public Integer getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

	public static String getName(Integer type) {
		for (AccountRegSrcEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getDesc();
			}
		}
		return null;
	}
}
