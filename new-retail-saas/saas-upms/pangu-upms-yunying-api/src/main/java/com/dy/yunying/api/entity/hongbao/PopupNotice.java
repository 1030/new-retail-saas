package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.annotations.Update;

import java.util.Date;

/**
 * @author ：lile
 * @date ：2021/10/27 11:15
 * @description：
 * @modified By：
 */
@Data
@TableName("popup_notice")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "公告弹窗信息表")
public class PopupNotice extends HbBaseEntity {

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "公告ID")
	private Long id;

	@ApiModelProperty(value = "菜单id  如 红包、礼包、消息、账户等")
	private Long menuId;

	@ApiModelProperty(value = "菜单名称")
	private String menuTitle;

	@ApiModelProperty(value = "菜单编码")
	private String menuCode;

	@ApiModelProperty(value = "菜单编码 ios")
	private String menuCodeIos;

	@ApiModelProperty(value = "公告类型: 0-通用公告; 1-红包活动公告;")
	private Integer sourceType;

	@ApiModelProperty(value = "如:红包活动公告所属类型 1：等级红包、2：充值红包、3：邀请红包")
	private Integer sourceBelongType;

	@ApiModelProperty(value = "来源id, 如：红包活动id")
	@TableField(updateStrategy = FieldStrategy.IGNORED)
	private Long sourceId;

	@ApiModelProperty(value = "通知弹窗事件")
	private String popupEvent;

	@ApiModelProperty(value = "通知类型, 1、活动开始弹窗 2、活动结束弹窗 3、消息 4、浮标")
	private Integer popupType;

	@ApiModelProperty(value = "0开启 1关闭")
	private Integer popupTimes;

	@ApiModelProperty(value = "通知弹窗内容,图片的话就是url")
	private String popupContent;

	@ApiModelProperty(value = "跳转链接")
	private String imgUrl;

	@ApiModelProperty(value = "公告通知发布时间")
	private Date popupTime;

	@ApiModelProperty(value = "公告通知开始时间")
	private Date popupStartTime;

	@ApiModelProperty(value = "公告通知结束时间")
	private Date popupEndTime;

	@ApiModelProperty(value = "事件编码")
	private String eventCode;

	@ApiModelProperty(value = "跳转方式(1SDK面板 2内置浏览器 3外部浏览器 4不跳转)")
	private Integer openType;

	@ApiModelProperty(value = "展示方式(1横屏展示，2竖屏展示)")
	private Integer showType;

	@ApiModelProperty(value = "公告状态(1待上线 2公告中 3已下线)")
	private Integer popupStatus;

	@ApiModelProperty(value = "公告标题")
	private String popupTitle;
	@ApiModelProperty(value = "公告内容")
	private String popupMessage;
	@ApiModelProperty(value = "公告屏幕方向(1横屏 2竖屏)")
	private Integer popupOrient;
	@ApiModelProperty(value = "公告形式(1弹窗图 2全屏图 3图文 4通栏消息)")
	private Integer popupMold;
	@ApiModelProperty(value = "关闭方式(1手动关闭 2倒计时关闭)")
	private Integer closeType;
	@ApiModelProperty(value = "关闭倒计时(秒)")
	private Integer closeTimes;
	@ApiModelProperty(value = "推送频次: 1-每天; 2-隔天; 3-每周; 4-仅一次;")
	private Integer pushFrequency;
	@ApiModelProperty(value = "每天推送间隔时为天数；或者每周推送间隔时: 1 ~ 7 -> 周一 ~ 周日，使用逗号分隔")
	private String pushInterval;
	@ApiModelProperty(value = "推送节点: 1-首次登录成功; 2-每次登录成功; 3-首次进入游戏; 4-每次进入游戏;")
	private Integer pushNode;
	@ApiModelProperty(value = "用户群组ID，多个使用逗号分隔")
	private String userGroup;
	@ApiModelProperty(value = "展示顺序")
	private Integer popupSort;

	@ApiModelProperty(value = "来源类型(1红包活动 2签到活动)")
	@TableField(updateStrategy = FieldStrategy.IGNORED)
	private Integer sourceMold;
}
