package com.dy.yunying.api.entity.raffle;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 领取记录信息表
 * @author  chenxiang
 * @version  2022-11-08 16:01:24
 * table: raffle_receive_record
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "raffle_receive_record")
public class RaffleReceiveRecord extends Model<RaffleReceiveRecord>{
	/**
	 * 主键id
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/**
	 * 活动ID
	 */
	@TableField(value = "activity_id")
	private Long activityId;
	/**
	 * 领取类型：1-抽奖中奖记录；2-积分兑换记录；
	 */
	@TableField(value = "object_type")
	private Integer objectType;
	/**
	 * 奖品配置ID
	 */
	@TableField(value = "object_id")
	private Long objectId;
	/**
	 * 奖品配置ID
	 */
	@TableField(value = "object_name")
	private String objectName;
	/**
	 * 用户ID
	 */
	@TableField(value = "user_id")
	private Long userId;
	/**
	 * 用户账号
	 */
	@TableField(value = "username")
	private String username;
	/**
	 * 主游戏ID
	 */
	@TableField(value = "parent_game_id")
	private Long parentGameId;
	/**
	 * 子游戏ID
	 */
	@TableField(value = "sub_game_id")
	private Long subGameId;
	/**
	 * 区服ID
	 */
	@TableField(value = "area_id")
	private String areaId;
	/**
	 * 角色ID
	 */
	@TableField(value = "role_id")
	private String roleId;
	/**
	 * 角色名称
	 */
	@TableField(value = "role_name")
	private String roleName;
	/**
	 * 主渠道
	 */
	@TableField(value = "main_chl")
	private String mainChl;
	/**
	 * 子渠道
	 */
	@TableField(value = "sub_chl")
	private String subChl;
	/**
	 * 分包渠道
	 */
	@TableField(value = "pkg_code")
	private String pkgCode;
	/**
	 * 奖品类型：1-礼包码；2-代金券；3-游豆；4-实物礼品；
	 */
	@TableField(value = "gift_type")
	private Integer giftType;
	/**
	 * 奖品发放数量
	 */
	@TableField(value = "gift_num")
	private Integer giftNum;
	/**
	 * 外部奖品ID
	 */
	@TableField(value = "gift_id")
	private Long giftId;
	/**
	 * 礼包码编码/代金券编码(逗号分隔)
	 */
	@TableField(value = "gift_code")
	private String giftCode;
	/**
	 * 代金券名称/实物礼品名称
	 */
	@TableField(value = "gift_name")
	private String giftName;
	/**
	 * 代金券金额/折扣券比例/游豆数量
	 */
	@TableField(value = "gift_amount")
	private BigDecimal giftAmount;
	/**
	 * 代金券使用限制金额
	 */
	@TableField(value = "gift_use_limit")
	private BigDecimal giftUseLimit;
	/**
	 * 奖品使用范围：1-不限；2-游戏；3-角色；
	 */
	@TableField(value = "gift_use_scope")
	private Integer giftUseScope;
	/**
	 * 奖品过期类型：1-永久有效；2-临时奖品；
	 */
	@TableField(value = "gift_expire_type")
	private Integer giftExpireType;
	/**
	 * 奖品固定生效时间
	 */
	@TableField(value = "gift_start_time")
	private Date giftStartTime;
	/**
	 * 奖品固定过期时间
	 */
	@TableField(value = "gift_end_time")
	private Date giftEndTime;
	/**
	 * 领取时间
	 */
	@TableField(value = "receive_time")
	private Date receiveTime;
	/**
	 * 领取记录状态：1-未填写地址；2-待审核；3-审核驳回；4-审核通过/已发货；
	 */
	@TableField(value = "record_status")
	private Integer recordStatus;
	/**
	 * 驳回原因
	 */
	@TableField(value = "forbid_reason")
	private String forbidReason;
	/**
	 * 收货人姓名
	 */
	@TableField(value = "receipt_username")
	private String receiptUsername;
	/**
	 * 收货人电话
	 */
	@TableField(value = "receipt_phone")
	private String receiptPhone;
	/**
	 * receipt_province_code
	 */
	@TableField(value = "receipt_province_code")
	private String receiptProvinceCode;
	/**
	 * receipt_province_name
	 */
	@TableField(value = "receipt_province_name")
	private String receiptProvinceName;
	/**
	 * receipt_city_code
	 */
	@TableField(value = "receipt_city_code")
	private String receiptCityCode;
	/**
	 * receipt_city_name
	 */
	@TableField(value = "receipt_city_name")
	private String receiptCityName;
	/**
	 * receipt_area_code
	 */
	@TableField(value = "receipt_area_code")
	private String receiptAreaCode;
	/**
	 * receipt_area_name
	 */
	@TableField(value = "receipt_area_name")
	private String receiptAreaName;
	/**
	 * receipt_address
	 */
	@TableField(value = "receipt_address")
	private String receiptAddress;
	/**
	 * 发货时间
	 */
	@TableField(value = "deliver_time")
	private Date deliverTime;
	/**
	 * 快递名称
	 */
	@TableField(value = "express_name")
	private String expressName;
	/**
	 * 快递订单号
	 */
	@TableField(value = "express_code")
	private String expressCode;
	/**
	 * 是否删除：0-否；1-是；
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

