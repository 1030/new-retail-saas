package com.dy.yunying.api.entity.prize;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 奖励与区服关系表
 *
 * @author chenxiang
 * @version 2022-04-26 10:20:36 table: prize_push_area
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "prize_push_area")
public class PrizePushArea extends Model<PrizePushArea> {

	/**
	 * 主键ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 奖励推送ID
	 */
	@TableField(value = "prize_push_id")
	private Long prizePushId;

	/**
	 * 主游戏ID
	 */
	@TableField(value = "parent_game_id")
	private Long parentGameId;

	/**
	 * 区服ID
	 */
	@TableField(value = "area_id")
	private Long areaId;

	/**
	 * 区服层级(1主游戏 2区服)
	 */
	@TableField(value = "area_range")
	private Integer areaRange;

	/**
	 * 是否删除(0否 1是)
	 */
	@ApiModelProperty(value = "是否删除(0否 1是)")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建者
	 */
	@TableField(fill = FieldFill.INSERT)
	private Long createId;

	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 更新者
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updateId;

	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}
