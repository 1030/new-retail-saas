package com.dy.yunying.api.cps;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sunyq
 * @date 2022/10/11 17:48
 */
@Data
public class CpsBuckleRateReq extends Page implements Serializable {
	/**
	 * 主键id
	 */
	private Long id;

	/**
	 * 账号ID
	 */
	private Integer userId;

	/**
	 * 账号名称
	 */
	private String username;

	/**
	 * 父游戏id
	 */
	private Long pgid;

	/**
	 * 扣量比例0~99
	 */
	private String buckleRate;

	/**
	 * 开始时间
	 */
	private String startTime;

	/**
	 * 结束时间
	 */
	private String endTime;
}
