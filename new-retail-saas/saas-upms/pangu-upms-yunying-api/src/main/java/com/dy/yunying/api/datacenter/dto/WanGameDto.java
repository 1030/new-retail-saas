package com.dy.yunying.api.datacenter.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 游戏
 * wan_game
 * @author hma
 * @date 2021-06-02 14:22:57
 */
@Getter
@Setter
@ApiModel(value = "渠道管理")
public class WanGameDto {
    /**
     * 游戏ID
     */
	@ApiModelProperty(value = "游戏ID")
    private Long gameId;

    /**
     * 游戏ID
     */
	@ApiModelProperty(value = "游戏名称")
    private String gName;


	/**
	 * 游戏ID
	 */
	@ApiModelProperty(value = "父游戏id")
	private String pgid;

}