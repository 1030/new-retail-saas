package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 活动上下线
 * @date 2021/10/23 11:04
 */
@Data
public class KvpicActivityReq implements Serializable {

	@ApiModelProperty(value = "kv图地址")
	@NotNull(message = "kv图地址不能为空")
	private String kvpic;

	@ApiModelProperty(value = "活动id集合")
	@NotNull(message = "活动id集合不能为空")
	private List<Long> idList;

}
