package com.dy.yunying.api.req.raffle;

import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Description
 * @Author chengang
 * @Date 2022/11/9
 */
@Data
public class RaffleDataReq extends Page {

	/**
	 * 活动ID
	 */
	private String activityId;

	/**
	 * 活动名称
	 */
	private String activityName;

	/**
	 * 开始时间，格式 'yyyy-MM-dd'
	 */
	@NotEmpty(message = "开始时间不能为空")
	private String startTime;

	/**
	 * 结束时间，格式 'yyyy-MM-dd'
	 */
	@NotEmpty(message = "结束时间不能为空")
	private String endTime;
}
