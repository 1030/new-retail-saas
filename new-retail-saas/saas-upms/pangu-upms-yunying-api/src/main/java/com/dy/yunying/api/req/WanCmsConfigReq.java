package com.dy.yunying.api.req;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * cms配置
 * 2020年7月22日10:16:50
 */
@Getter
@Setter
public class WanCmsConfigReq implements Serializable{
    private static final long serialVersionUID = 1253351022427084148L;
    /**
     * 主键
     */
    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 改变状态 0未改变, 1更新栏目，2更新页面
     */
    private String cfgstatus;
    /**
     * 状态 :1 可用 0 删除
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createtime;
    /**
     * 创建者id
     */
    private Integer createuser;
    /**
     * 更新时间
     */
    private Date updatetime;
    /**
     * 类型（index-首页，games-游戏，news-新闻，activity-活动，service-客服）
     */
    private String cfgtype;
    //类型id
    private Integer typeid;
    //网页title
    private String title;
    //网页keywords
    private String keywords;
    //网页描述
    private String description;
    
}
