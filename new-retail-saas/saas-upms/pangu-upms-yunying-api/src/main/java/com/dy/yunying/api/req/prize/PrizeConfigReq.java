package com.dy.yunying.api.req.prize;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * 奖励配置表
 *
 * @author chenxiang
 * @version 2022-04-28 16:37:18 table: prize_config
 */
@Data
public class PrizeConfigReq extends Page {

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "奖品推送主键ID")
	private String prizePushId;

	@ApiModelProperty(value = "标题文本")
	private String title;

	@ApiModelProperty(value = "说明文本")
	private String description;

	@ApiModelProperty(value = "奖励按钮  1 去使用 2 复制")
	private String prizeButton;

	@ApiModelProperty(value = "icon图")
	private String icon;

	@ApiModelProperty(value = "奖励类型：1代金券，2礼包码，3游豆，4卡密")
	private String prizeType;

	@ApiModelProperty(value = "金额")
	private String money;

	@ApiModelProperty(value = "限制金额")
	private String limitMoney;

	@ApiModelProperty(value = "时间类型：1固定时间，2领取后指定天数")
	private String timeType;

	@ApiModelProperty(value = "游豆类型。1：普通 ，常规游豆 ；2：临时限时游豆")
	private String currencyType;

	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	private String endTime;

	@ApiModelProperty(value = "领取后指定时间(秒)")
	private String effectiveTime;

	@ApiModelProperty(value = "使用限制(逗号分割)：all不限，game游戏，role角色")
	private String useLimitType;

	@ApiModelProperty(value = "礼包码类型：1唯一码，2通用码")
	private String giftType;

	@ApiModelProperty(value = "通用码")
	private String giftCode;

	@ApiModelProperty(value = "礼包码数量")
	private String giftAmount;

	@ApiModelProperty(value = "礼包内容")
	private String giftContent;

	@ApiModelProperty(value = "打开类型：0不跳转，1SDK面板，2内置浏览器，3外部链接")
	private String openType;

	@ApiModelProperty(value = "菜单Id")
	private String menuId;

	@ApiModelProperty(value = "菜单名称")
	private String menuTitle;

	@ApiModelProperty(value = "菜单编码")
	private String menuCode;

	@ApiModelProperty(value = "ios 菜单编码地址")
	private String menuCodeIos;
	/**
	 * 二级菜单编码(安卓)
	 */
	@ApiModelProperty(value = "二级菜单编码(安卓)")
	private String secondMenuCode;
	/**
	 * 二级菜单编码(苹果)
	 */
	@ApiModelProperty(value = "二级菜单编码(苹果)")
	private String secondMenuCodeIos;

	@ApiModelProperty(value = "来源类型(1红包活动 2签到活动)")
	private String skipMold;

	@ApiModelProperty(value = "跳转位置 如：红包类型 1：等级红包、2：充值红包、3：邀请红包、4：定制红包")
	private String skipBelongType;

	@ApiModelProperty(value = "来源id, 如：红包活动id")
	private String skipId;

	@ApiModelProperty(value = "跳转地址")
	private String skipUrl;

	@ApiModelProperty(value = "跳转地址")
	private String skipUrlIOS;

	@ApiModelProperty(value = "展示方式(1横屏展示，2竖屏展示)")
	private String showType;

	@ApiModelProperty(value = "唯一码文件")
	private MultipartFile file;

	@ApiModelProperty(value = "推送类型 ：1单个奖励，2组合奖励")
	private String prizePushType;

}
