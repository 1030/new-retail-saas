package com.dy.yunying.api.datacenter.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @ClassName AdmonitorRuleDto
 * @Description todo
 * @Author yangyh
 * @Time 2021/6/22 14:27
 * @Version 1.0
 **/
@Setter
@Getter
@ApiModel("监控规则查询对象")
public class AdmonitorRuleDto extends Page {


	/**
	 * 规则名称
	 */
	@NotNull(message = "规则名称不能为空")
	@ApiModelProperty(value="规则名称")
	private String monitorName;

	/**出库时间开始*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建时间开始")
	private Date rsTime;

	/**出库时间终止*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建时间终止")
	private Date reTime;

	/**
	 * 监控游戏
	 */
	@ApiModelProperty(value="监控游戏")
	private String monitorGames;

	/**
	 * 监控渠道平台类型
	 */
	@ApiModelProperty(value="监控渠道")
	private String monitorChannelTypes;

	/**
	 * 监控项
	 */
	@ApiModelProperty(value="监控项")
	private String monitorAditem;

	/**
	 * 监控状态：0关闭，1开启
	 */
	@ApiModelProperty(value="监控状态：0关闭，1开启")
	private Integer monitorStatus;
}
