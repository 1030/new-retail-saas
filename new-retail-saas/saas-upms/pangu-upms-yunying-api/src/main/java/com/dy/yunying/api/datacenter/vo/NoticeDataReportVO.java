package com.dy.yunying.api.datacenter.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import java.math.BigDecimal;

/**
 * @description: 公告数据报表
 * @author yuwenfeng
 * @date 2022/3/7 17:38
 */
@Data
@ApiModel("公告数据报表")
public class NoticeDataReportVO {

	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	private String period;

	@ApiModelProperty("父游戏id")
	private Long pgid;

	@ExcelProperty("父游戏")
	@ApiModelProperty("父游戏")
	private String parentGameName;

	@ApiModelProperty("子游戏id")
	private Long gameid;

	@ExcelProperty("子游戏")
	@ApiModelProperty("子游戏")
	private String gameName;

	@ExcelProperty("父渠道")
	@ApiModelProperty("父渠道")
	private String parentchlname;

	@ApiModelProperty("父渠道code")
	private String parentchl;

	@ExcelProperty("子渠道")
	@ApiModelProperty("子渠道")
	private String chlName;

	@ApiModelProperty("子渠道code")
	private String chl;

	@ExcelProperty("公告Id")
	@ApiModelProperty("公告Id")
	private Long noticeId;

	@ApiModelProperty(value = "公告形式(1弹窗图 2全屏图 3图文 4通栏消息)")
	private Integer popupMold;

	@ExcelProperty("公告形式")
	@ApiModelProperty(value = "公告形式内容")
	private String popupMoldName;

	@ApiModelProperty(value = "推送节点：1每日首次登录成功，2每日每次登录成功，3每日首次进入游戏，4每日每次进入游戏")
	private Integer pushNode;

	@ExcelProperty("推送节点")
	@ApiModelProperty(value = "推送节点内容")
	private String pushNodeName;

	@ExcelProperty("公告预览")
	@ApiModelProperty(value = "公告预览")
	private String popupContent;


	@ExcelProperty("触达用户")
	@ApiModelProperty("触达用户")
	private BigDecimal touchUserSum = new BigDecimal(0);

	@ExcelProperty("点击用户")
	@ApiModelProperty("点击用户")
	private BigDecimal clickUserSum = new BigDecimal(0);

	@ExcelProperty("展示次数")
	@ApiModelProperty("展示次数")
	private BigDecimal showNumberSum = new BigDecimal(0);

	@ExcelProperty("点击次数")
	@ApiModelProperty("点击次数")
	private BigDecimal clickNumberSum = new BigDecimal(0);

	@ExcelProperty("触达转化率")
	@ApiModelProperty("触达转化率")
	private BigDecimal touchRatio;

	@ExcelProperty("点击转化率")
	@ApiModelProperty("点击转化率")
	private BigDecimal clickRatio;

}
