/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author yuwenfeng
 * @description: SDK动态菜单表
 * @date 2022/3/7 10:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "hb_sdk_dynamic_menus")
public class HbSdkDynamicMenus extends Model<HbSdkDynamicMenus> {

	/**
	 * 主键id
	 */
	@TableField(value = "id")
	private Long id;
	/**
	 * 上级ID(0为一级)
	 */
	@TableField(value = "parent_id")
	private Long parentId;

	/**
	 * 菜单编码
	 */
	@TableField(value = "menu_code")
	private String menuCode;

	/**
	 * 菜单标题
	 */
	@TableField(value = "menu_title")
	private String menuTitle;

	/**
	 * 菜单顺序
	 */
	@TableField(value = "menu_order")
	private Integer menuOrder;

	/**
	 * 菜单类型，0-普通菜单；1-红包菜单；...
	 */
	@TableField(value = "menu_type")
	private Integer menuType;

	/**
	 * 系统类型，0-Andriod；1-ios；...
	 */
	@TableField(value = "os_type")
	private Integer osType;

	/**
	 * 图标类型：0-图片地址；1-矢量图标；2-base64图片文本；
	 */
	@TableField(value = "icon_type")
	private Integer iconType;

	/**
	 * 已选中图标图片地址或矢量图标值，当图标内型为图片地址或矢量图标时，则不允许为空
	 */
	@TableField(value = "active_icon_link")
	private String activeIconLink;

	/**
	 * 已选中图标图片base64值，当图标类型为base64图片文本时，则不允许为空
	 */
	@TableField(value = "active_icon_content")
	private String activeIconContent;

	/**
	 * 未选中图标图片地址或矢量图标值，当图标内型为图片地址或矢量图标时，则不允许为空
	 */
	@TableField(value = "unactive_icon_link")
	private String unactiveIconLink;

	/**
	 * 未选中图标图片base64值，当图标类型为base64图片文本时，则不允许为空
	 */
	@TableField(value = "unactive_icon_content")
	private String unactiveIconContent;

	/**
	 * 资源类型：0-自定义；1-H5页面连接；2-列表内容；...
	 */
	@TableField(value = "resource_type")
	private Integer resourceType;

	/**
	 * 资源连接，当资源为H5页面连接时，不允许为空
	 */
	@TableField(value = "resource_link")
	private String resourceLink;

	/**
	 * 资源内容，当资源为列表内容时，不允许为空
	 */
	@TableField(value = "resource_content")
	private String resourceContent;

	/**
	 * 终端最低允许返回版本
	 */
	@TableField(value = "allow_version")
	private Integer allowVersion;

	/**
	 * 状态：0-启用；1-禁用
	 */
	@TableField(value = "status")
	private Integer status;

	/**
	 * 是否删除：0-否；1-是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}
