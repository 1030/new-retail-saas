package com.dy.yunying.api.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 游豆类型
 */
public enum CurrencyTypeEnum {

    COMM(1,"TIMELESS","普通"),
    TEMP(2,"FLEETING","临时"),
    ;

	private Integer val;
    private String desc;
    private String name;

    private static final Map<Integer, String> map = new HashMap<>();

    static {
        CurrencyTypeEnum[] payArray = values();
        for (CurrencyTypeEnum currencyTypeEnum : payArray) {
            map.put(currencyTypeEnum.getVal(), currencyTypeEnum.getDesc());
        }

    }

    CurrencyTypeEnum(Integer val, String desc, String name) {
        this.val = val;
        this.desc = desc;
        this.name = name;
    }

    public static CurrencyTypeEnum getOneByVal(Integer val){
        CurrencyTypeEnum[] payArray = values();
        for (CurrencyTypeEnum currencyTypeEnum : payArray) {
            if (currencyTypeEnum.getVal().equals(val)) {
                return currencyTypeEnum;
            }
        }
        return null;
    }

    public static CurrencyTypeEnum getByVal(Integer val){
        CurrencyTypeEnum[] payArray = values();
        for (CurrencyTypeEnum currencyTypeEnum : payArray) {
            if (currencyTypeEnum.getVal().equals(val)) {
                return currencyTypeEnum;
            }
        }
        return null;
    }
    
    public static String getNameByDesc(String desc){
    	CurrencyTypeEnum[] payArray = values();
    	for (CurrencyTypeEnum typeEnum : payArray) {
            if (typeEnum.getDesc().equals(desc)) {
                return typeEnum.getName();
            }
        }
    	return null;
    }

    public static String getDescByVal(Integer val) {
        return map.get(val);
    }

    public Integer getVal() {
        return val;
    }

    public String getDesc() {
        return desc;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
}
