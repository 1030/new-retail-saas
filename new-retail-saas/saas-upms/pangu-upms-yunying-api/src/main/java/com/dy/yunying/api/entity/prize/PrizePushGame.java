package com.dy.yunying.api.entity.prize;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 奖励与游戏关系表
 *
 * @author chenxiang
 * @version 2022-04-26 10:20:59 table: prize_push_game
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "prize_push_game")
public class PrizePushGame extends Model<PrizePushGame> {

	/**
	 * 主键ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 奖励推送ID
	 */
	@TableField(value = "prize_push_id")
	private Long prizePushId;

	/**
	 * 主游戏ID
	 */
	@TableField(value = "parent_game_id")
	private Long parentGameId;

	/**
	 * 子游戏ID
	 */
	@TableField(value = "child_game_id")
	private Long childGameId;

	/**
	 * 游戏层级(1主游戏 2子游戏)
	 */
	@TableField(value = "game_range")
	private Integer gameRange;

	/**
	 * 是否删除(0否 1是)
	 */
	@ApiModelProperty(value = "是否删除(0否 1是)")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建者
	 */
	@TableField(fill = FieldFill.INSERT)
	private Long createId;

	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 更新者
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updateId;

	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}
