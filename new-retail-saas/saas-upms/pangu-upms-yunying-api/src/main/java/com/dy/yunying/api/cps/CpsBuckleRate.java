package com.dy.yunying.api.cps;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * cps用户扣量比例
 * @TableName cps_buckle_rate
 */
@TableName(value ="cps_buckle_rate")
@Data
public class CpsBuckleRate implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 账号ID
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 父游戏id
     */
    @TableField(value = "pgid")
    private Long pgid;

    /**
     * 扣量比例0~99
     */
    @TableField(value = "buckle_rate")
    private BigDecimal buckleRate;

    /**
     * 开始时间
     */
    @TableField(value = "start_time")
    private Date startTime;

    /**
     * 结束时间
     */
    @TableField(value = "end_time")
    private Date endTime;

    /**
     * 是否删除  0否 1是
     */
    @TableField(value = "deleted")
    private Byte deleted;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 创建人
     */
    @TableField(value = "create_id")
    private Long createId;

    /**
     * 修改人
     */
    @TableField(value = "update_id")
    private Long updateId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}