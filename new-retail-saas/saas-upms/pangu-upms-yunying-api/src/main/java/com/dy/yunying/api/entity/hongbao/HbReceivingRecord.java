package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;




/**
 * 奖品领取记录
 * @author  chenxiang
 * @version  2021-10-27 13:55:13
 * table: hb_receiving_record
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "hb_receiving_record")
public class HbReceivingRecord extends Model<HbReceivingRecord>{

	/**
	 * 主键ID
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/**
	 * 活动ID
	 */
	@TableField(value = "activity_id")
	private Long activityId;
	/**
	 * 类型：1红包，2提现
	 */
	@TableField(value = "object_type")
	private Integer objectType;
	/**
	 * 红包ID,提现档次id
	 */
	@TableField(value = "object_id")
	private Long objectId;
	/**
	 * 领取类型：1红包，2礼包码，3实物礼品，4代金券
	 */
	@TableField(value = "type")
	private Integer type;
	/**
	 * 用户id
	 */
	@TableField(value = "user_id")
	private Long userId;
	/**
	 * 用户名
	 */
	@TableField(value = "user_name")
	private String userName;
	/**
	 * 主渠道
	 */
	@TableField(value = "parent_chl")
	private String parentChl;
	/**
	 * 子渠道
	 */
	@TableField(value = "chl")
	private String chl;
	/**
	 * 分包渠道
	 */
	@TableField(value = "app_chl")
	private String appChl;
	/**
	 * 父游戏ID
	 */
	@TableField(value = "parent_game_id")
	private Integer parentGameId;
	/**
	 * 子游戏ID
	 */
	@TableField(value = "game_id")
	private Integer gameId;
	/**
	 * 区服ID
	 */
	@TableField(value = "area_id")
	private Integer areaId;
	/**
	 * 角色ID
	 */
	@TableField(value = "role_id")
	private String roleId;
	/**
	 * 角色名称
	 */
	@TableField(value = "role_name")
	private String roleName;
	/**
	 * 礼包ID：如礼包主键，代金券主键
	 */
	@TableField(value = "gift_id")
	private String giftId;
	/**
	 * 礼包码
	 */
	@TableField(value = "gift_code")
	private String giftCode;
	/**
	 * 领取时间
	 */
	@TableField(value = "receive_time")
	private Date receiveTime;
	/**
	 * 驳回原因
	 */
	@TableField(value = "forbid_reason")
	private String forbidReason;
	/**
	 * 状态：1已到账，2待填写地址，3待审核，4待发货，5已发货，6已收货，7已驳回
	 */
	@TableField(value = "status")
	private Integer status;
	/**
	 * 是否删除：0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

