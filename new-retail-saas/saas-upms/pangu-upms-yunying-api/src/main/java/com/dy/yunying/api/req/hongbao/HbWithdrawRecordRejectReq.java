package com.dy.yunying.api.req.hongbao;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;


/**
 * 提现记录表
 *
 * @author zxm
 * @date 2021-10-28 20:45:35
 */
@Data
@Accessors(chain = true)
public class HbWithdrawRecordRejectReq implements Serializable {


	private static final long serialVersionUID = 1773854643238430792L;


	@TableId
	@ApiModelProperty(value = "提现记录ID")
	@NotNull(message = "提现记录不能为空")
	private Long id;

	@ApiModelProperty(value = "状态：0审核中，1审核通过，2提现中 3提现失败 4提现成功 5交易成功（游戏货币使用）")
	private Integer status;


	@ApiModelProperty(value = "审核时间")
	private Date auditingTime;

	@ApiModelProperty(value = "审核人")
	private Long auditingId;

	@ApiModelProperty(value = "驳回原因")
	private String rejectReason;

	@ApiModelProperty(value = "openid")
	private String openid;

	@ApiModelProperty(value = "提现记录ID集合")
	private String ids;

	@ApiModelProperty(value = "审核类型：1通过，2驳回")
	private String reviewerType;
}
