package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 新增/编辑对象
 * @date 2021/10/23 11:04
 */
@Data
public class GameReq implements Serializable {

	@NotNull(message = "主游戏ID不能为空")
	@ApiModelProperty(value = "主游戏ID")
	private Long gameId;

	@ApiModelProperty(value = "子游戏ID")
	private Long childGameId;
}
