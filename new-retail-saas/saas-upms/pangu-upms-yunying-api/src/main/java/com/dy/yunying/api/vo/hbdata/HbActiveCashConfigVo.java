package com.dy.yunying.api.vo.hbdata;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/11/18 17:33
 * @description：
 * @modified By：
 */
@Data
public class HbActiveCashConfigVo implements Serializable {

	//活动ID
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活动ID")
	private Long activityId;

	// 活动名称
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("活动名称")
	private String activityName;

	//(提现方式) 1游戏货币，2微信，3支付宝 4代金券
	private String cashType;

	//提现方式
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("提现类型")
	private String cashTypeName;

	//提现档次ID
	private String cashConfig;

	//提现档次名称
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("档位")
	private String cashConfigName;

	//提现金额
	private BigDecimal moneySum;

	//提现次数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("提现次数")
	private Long cashNum;

	public String getCashTypeName() {
		String cashTypeName = null;
		switch (cashType) {
			case "1":
				cashTypeName = "游戏货币";
				break;
			case "2":
				cashTypeName = "微信";
				break;
			case "3":
				cashTypeName = "支付宝";
				break;
			case "4":
				cashTypeName = "代金券";
				break;
			case "5":
				cashTypeName = "游豆";
				break;
			default:
				cashTypeName = "-";
		}
		return cashTypeName;
	}
}
