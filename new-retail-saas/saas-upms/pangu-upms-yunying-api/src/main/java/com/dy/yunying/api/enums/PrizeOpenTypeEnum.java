package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 打开类型：0不跳转，1SDK面板，2内置浏览器，3外部链接
 * @author sunyouquan
 * @date 2022/5/5 11:01
 */
public enum PrizeOpenTypeEnum {

	NOJUMP(0, "不跳转"),

	SDK(1, "SDK面板"),

	BROWER(2, "内置浏览器"),

	URL(3, "外部链接");

	private Integer type;

	private String name;

	PrizeOpenTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonCreator
	public static PrizeOpenTypeEnum getPrizeOpenTypeEnum(Integer value) {
		for (PrizeOpenTypeEnum prizeOpenTypeEnum : values()) {
			if (prizeOpenTypeEnum.type.equals(value)) {
				return prizeOpenTypeEnum;
			}
		}
		return null;
	}

	@JsonValue
	public Integer getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}

}
