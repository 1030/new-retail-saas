package com.dy.yunying.api.resp.prize;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 奖品礼包码
 *
 * @author chenxiang
 * @version 2022-04-26 10:21:44 table: prize_gift_bag
 */
@Data
public class PrizeGiftBagRes implements Serializable {

	@ApiModelProperty(value = "主键ID")
	private Long id;

	@ApiModelProperty(value = "类型：1礼包，2自定义")
	private Integer sourceType;

	@ApiModelProperty(value = "来源关联ID。如：奖品ID")
	private Long sourceId;

	@ApiModelProperty(value = "礼包码")
	private String giftCode;

	@ApiModelProperty(value = "礼包码类型：1：唯一码，2：通用码")
	private Integer usable;

	@ApiModelProperty(value = "状态：0可用，1已领取")
	private Integer status;

	@ApiModelProperty(value = "领取时间")
	private Date receiveTime;

	@ApiModelProperty(value = "礼包码总数量")
	private Long giftAmount;

	@ApiModelProperty(value = "已领取数量")
	private Long giftGetcounts;

	@ApiModelProperty(value = "是否删除：0否 1是")
	private Integer deleted;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

	@ApiModelProperty(value = "创建人")
	private Long createId;

	@ApiModelProperty(value = "修改人")
	private Long updateId;

}
