package com.dy.yunying.api.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ：lile
 * @date ：2021/5/29 15:46
 * @description：
 * @modified By：
 */
@Data
public class ParentGameVO {
	/**
	 *
	 */
	private Long id;

	/**
	 * 游戏名称
	 */
	private String gname;

	/**
	 * 游戏币汇率
	 */
	private BigDecimal exchangeRate;

	/**
	 * 游戏分成，0到100
	 */
	private BigDecimal sharing;

	/**
	 * 子游戏数量
	 */
	private Long sgnum;

	/**
	 * 玩家人数
	 */
	private Long playernum;

	/**
	 * 充值金额
	 */
	private BigDecimal recharge;

	/**
	 * 游戏厂商
	 */
	private String gmanufacturer;

	/**
	 * 登录地址
	 */
	private String loginaddress;

	/**
	 * 充值回调
	 */
	private String rechargecallback;

	/**
	 * 上架时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date launchtime;

	/**
	 * 包名
	 */
	private String pkName;

	/**
	 * 请求接口秘钥
	 */
	private String queryKey;

	/**
	 * 兑换接口密钥
	 */
	private String exchangeKey;

	/**
	 * 对接、联调游戏ID
	 */
	private Long jointGameId;

	/**
	 * 导出接口密钥
	 */
	private String reportKey;

	/**
	 * 兑换任务类
	 */
	private String exchangeClass;

	/**
	 * 登录接口地址
	 */
	private String loginUrl;

	/**
	 * 兑换接口地址
	 */
	private String exchangeUrl;

	/**
	 * 兑换结果查询接口地址
	 */
	private String exchangeQueryUrl;

	/**
	 *
	 */
	private String roleQueryUrl;

	/**
	 * 登录图地址
	 */
	private String loginMapUrl;

	/**
	 * loging图地址
	 */
	private String logingMapUrl;

	/**
	 * 游戏LOGO地址
	 */
	private String gameLogo;

	/**
	 * 状态，1：正常
	 */
	private Integer status;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/*相对父游戏表，增加的字段，返回给前端*/
	/*
	 * code 版本编码
	 * */
	private String code;

	/*
	 * 版本名称
	 * */
	private String name;
	/**
	 * 母包类型：1-运营母包；2-云微端；
	 */
	private Integer packType;

	private List<Map<String, String>> parentGameVersionList;


}
