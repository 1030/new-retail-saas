package com.dy.yunying.api.vo.hongbao;

import com.dy.yunying.api.entity.hongbao.HbActivityArea;
import com.dy.yunying.api.entity.hongbao.HbActivityChannel;
import com.dy.yunying.api.entity.hongbao.HbActivityGame;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 后台活动列表数据实体
 * @date 2021/10/23 10:53
 */
@Data
@ApiModel(value = "后台活动列表")
public class ActivityVo implements Serializable {

	@ApiModelProperty(value = "活动ID")
	private Long id;

	@ApiModelProperty(value = "活动类型(1等级 2充值 3邀请)")
	private Integer activityType;

	@ApiModelProperty(value = "活动名称")
	private String activityName;

	@ApiModelProperty(value = "活动kv图地址")
	private String kvpic;

	@ApiModelProperty(value = "时间类型(1静态时间 2动态时间)")
	private Integer timeType;

	@ApiModelProperty(value = "开始时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;

	@ApiModelProperty(value = "结束时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date finishTime;

	@ApiModelProperty(value = "有效天数")
	private Integer validDays;

	@ApiModelProperty(value = "活动状态(1待上线 2活动中 3已下线)")
	private Integer activityStatus;

	@ApiModelProperty(value = "活动规则")
	private String activityRule;

	@ApiModelProperty(value = "提现规则")
	private String cashRemarke;

	@ApiModelProperty(value = "角色类型: 0活跃角色 1新角色 2老角色")
	private Integer roleType;

	@ApiModelProperty(value = "注资总额(元)")
	private BigDecimal totalInvestMoney;

	@ApiModelProperty(value = "奖池价值(元)")
	private BigDecimal totalCost;

	@ApiModelProperty(value = "消耗金额(元)")
	private BigDecimal expendBalance;

	@ApiModelProperty(value = "活动金额(元)")
	private BigDecimal activityBalance;

	@ApiModelProperty(value = "渠道集合")
	private List<String[]> channelList;

	@ApiModelProperty(value = "游戏集合")
	private List<Long[]> gameList;

	@ApiModelProperty(value = "区服集合")
	private List<Long[]> areaList;

	@ApiModelProperty(value = "渠道数据集合")
	private List<String[]> channelDataList;

	@ApiModelProperty(value = "游戏数据集合")
	private List<Long[]> gameDataList;

	@ApiModelProperty(value = "区服数据集合")
	private List<Long[]> areaDataList;

	@ApiModelProperty(value = "游戏任务禁用标识。1标识可选游戏任务，2标识游戏任务禁用")
	private Integer taskDisableFlag = 2;

	@ApiModelProperty(value = "所属主游戏")
	private Long pgid;
}
