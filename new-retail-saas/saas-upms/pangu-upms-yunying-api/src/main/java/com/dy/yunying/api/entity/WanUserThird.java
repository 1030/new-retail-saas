package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户第三方绑定状态
 * @TableName wan_user_third
 */
@TableName(value ="wan_user_third")
@Data
public class WanUserThird implements Serializable {
    /**
     * 
     */
    @TableField(value = "id")
    private Long id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 用户平台账号(冗余)
     */
    @TableField(value = "username")
    private String username;

    /**
     * 主游戏id
     */
    @TableField(value = "game_main")
    private Long gameMain;

    /**
     * 子游戏id
     */
    @TableField(value = "game_sub")
    private Long gameSub;

    /**
     * 第三方 qq wechat douyin
     */
    @TableField(value = "third_code")
    private String thirdCode;

    /**
     * 第三方标识 openid或者unionid
     */
    @TableField(value = "openid")
    private String openid;

    /**
     * 第三方标识unionid(taptap)
     */
    @TableField(value = "unionid")
    private String unionid;

    /**
     * 绑定状态 1-正常 0-解绑
     */
    @TableField(value = "status")
    private Byte status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 最后一次解绑时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}