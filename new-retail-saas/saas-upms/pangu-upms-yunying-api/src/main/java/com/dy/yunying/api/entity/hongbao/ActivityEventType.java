package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ：lile
 * @date ：2021/11/16 16:38
 * @description：
 * @modified By：
 */
@Data
@TableName("hb_event_widget")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "事件表")
public class ActivityEventType extends HbBaseEntity {

	// 主键
	private Long id;

	// 事件名称
	private String name;

	// 事件唯一CODE
	private String code;

	//  事件类别
	private String category;

	// 事件描述
	private String description;
}
