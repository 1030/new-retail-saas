package com.dy.yunying.api.vo.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author ：lile
 * @date ：2021/10/27 17:04
 * @description：
 * @modified By：
 */
@Data
public class PopupNoticeVo {

	@ApiModelProperty(value = "菜单id  如 红包、礼包、消息、账户等")
	private Long menuId;

	@ApiModelProperty(value = "菜单编码")
	private String menuCode;

	@ApiModelProperty(value = "公告类型  1、普通公告 2、红包活动公告")
	private Integer sourceType;

	@ApiModelProperty(value = "如:红包活动公告所属类型 1：等级红包、2：充值红包、3：邀请红包")
	private Integer sourceBelongType;

	@ApiModelProperty(value = "来源id, 如：红包活动id")
	private Long sourceId;

	@ApiModelProperty(value = "通知弹窗事件")
	private String popupEvent;

	@ApiModelProperty(value = "通知类型, 1、活动开始弹窗 2、活动结束弹窗 3、消息 4、浮标")
	private String popupType;

	@ApiModelProperty(value = "通知弹窗内容,图片的话就是url")
	private String popupContent;

	@ApiModelProperty(value = "公告通知发布时间")
	private Date popupTime;
}
