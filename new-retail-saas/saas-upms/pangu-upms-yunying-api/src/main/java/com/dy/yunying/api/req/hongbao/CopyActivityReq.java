package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @description: 复制活动
 * @author yuwenfeng
 * @date 2022/1/11 15:48
 */
@Data
public class CopyActivityReq implements Serializable {

	@NotNull(message = "复制活动ID不能为空")
	@ApiModelProperty(value = "复制活动ID")
	private Long copyActivityId;

	@NotNull(message = "活动类型不能为空")
	@ApiModelProperty(value = "活动类型(1等级 2充值 3邀请)")
	private Integer activityType;

	@NotBlank(message = "活动名称不能为空")
	@ApiModelProperty(value = "活动名称")
	@Length(min = 1, max = 10, message = "活动名称不能超过10位汉字")
	private String activityName;

	@NotNull(message = "时间类型不能为空")
	@ApiModelProperty(value = "时间类型(1静态时间 2动态时间)")
	private Integer timeType;

	@NotBlank(message = "开始时间不能为空")
	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@NotBlank(message = "结束时间不能为空")
	@ApiModelProperty(value = "结束时间")
	private String finishTime;

	@ApiModelProperty(value = "有效天数")
	@Max(value = 365, message = "有效天数不能超过365")
	@Min(value = 0, message = "有效天数不能小于0")
	private Integer validDays;

	@NotBlank(message = "活动规则不能为空")
	@ApiModelProperty(value = "活动规则")
	private String activityRule;

	@ApiModelProperty(value = "提现规则")
	@NotBlank(message = "提现规则不能为空")
	private String cashRemarke;

	@ApiModelProperty(value = "角色类型: 0活跃角色 1新角色 2老角色")
	@NotNull(message = "起始条件不能为空")
	private Integer roleType;

	@NotNull(message = "渠道范围集合不能为空")
	@ApiModelProperty(value = "渠道范围集合")
	private List<String[]> channelList;

	@NotNull(message = "游戏范围集合不能为空")
	@ApiModelProperty(value = "游戏范围集合")
	private List<String[]> gameList;

	@NotNull(message = "区服范围集合不能为空")
	@ApiModelProperty(value = "区服范围集合")
	private List<String[]> areaList;

	@ApiModelProperty(value = "复制配置类型集合")
	private List<String> copyConfigList;

	@NotNull(message = "动态活动类型不能为空")
	@ApiModelProperty(value = "动态活动类型")
	private Long dynamicTypeId;
}
