package com.dy.yunying.api.entity;

import java.io.Serializable;
import java.util.Date;


/**
 *  渠道包和游戏渠道包关联
 *  @author zxm
 */
public class WanGameChannelInfo implements Serializable {

	
	/**
	 *  
	 */
	private static final long serialVersionUID = 5947959466668959135L;

	private Integer id;

    /**
     * 游戏id
     */
    private Integer gameid;

    /**
     * 渠道编码
     */
    private String chl;

    /**
     * 第三方平台应用key
     */
    private String appId;

    /**
     * 第三方平台应用名称
     */
    private String appName;

    /**
     * 推广渠道
     */
    private String channel;

    /**
     * 平台，0无平台，1头条，2百度，3新数
     */
    private Integer platform;

    /**
     * 是否上报0不上报1上报
     */
    private Integer isup;

    /**
     * 缓存状态0未缓存，1已经缓存
     */
    private Integer cacheStatus;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 对应渠道包内容描述
     */
    private String content;

    /**
     * 备注
     */
    private String remark;
    
    /**
     * 0 sdk支付，1苹果内置支付
     */
    private Integer paytype;
    
    /**
     * 广告主账户
     */
    private String adAccount;
    
    /**
     * 渠道计划数
     */
    private Integer plannum;
    
    /**
     * 渠道创意数
     */
    private Integer ideanum;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGameid() {
		return gameid;
	}

	public void setGameid(Integer gameid) {
		this.gameid = gameid;
	}

	public String getChl() {
		return chl;
	}

	public void setChl(String chl) {
		this.chl = chl;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Integer getPlatform() {
		return platform;
	}

	public void setPlatform(Integer platform) {
		this.platform = platform;
	}

	public Integer getIsup() {
		return isup;
	}

	public void setIsup(Integer isup) {
		this.isup = isup;
	}

	public Integer getCacheStatus() {
		return cacheStatus;
	}

	public void setCacheStatus(Integer cacheStatus) {
		this.cacheStatus = cacheStatus;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getPaytype() {
		return paytype;
	}

	public void setPaytype(Integer paytype) {
		this.paytype = paytype;
	}

	public String getAdAccount() {
		return adAccount;
	}

	public void setAdAccount(String adAccount) {
		this.adAccount = adAccount;
	}

	public Integer getPlannum() {
		return plannum;
	}

	public void setPlannum(Integer plannum) {
		this.plannum = plannum;
	}

	public Integer getIdeanum() {
		return ideanum;
	}

	public void setIdeanum(Integer ideanum) {
		this.ideanum = ideanum;
	}

    
}
