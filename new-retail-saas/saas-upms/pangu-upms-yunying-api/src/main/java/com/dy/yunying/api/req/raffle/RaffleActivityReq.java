package com.dy.yunying.api.req.raffle;

import com.baomidou.mybatisplus.annotation.TableField;
import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 抽奖活动配置表(raffle_activity)实体类
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class RaffleActivityReq extends Page {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
	private String nameAndId;
    /**
     * 主游戏ID
     */
    private Long parentGameId;
    /**
     * 活动结束时间
     */
    private String startDate;
    /**
     * 活动方式：1-九宫格；
     */
    private String endDate;
    /**
     * 活动状态(1待上线 2活动中 3已下线)
     */
    private Integer activityStatus;

}