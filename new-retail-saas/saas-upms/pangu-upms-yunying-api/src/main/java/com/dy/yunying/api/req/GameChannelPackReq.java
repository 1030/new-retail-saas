package com.dy.yunying.api.req;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 渠道包请求
 *
 * @Author: hjl
 * @Date: 2020/6/11 20:59
 */
@Getter
@Setter
public class GameChannelPackReq extends Page implements Serializable {

	/**
	 * 游戏ID
	 */
	private Long gameId;

	/**
	 * 分包类型：1、渠道分包；2：子游戏基础包
	 */
	private Integer type;

	/**
	 * 分包号起始
	 */
	private Integer packNumFrom;

	/**
	 * 分包号终止
	 */
	private Integer packNumTo;

	/**
	 * 分包备注
	 */
	private String remark;

	private String stime; //查询时间(开始)

	private String etime; //查询时间（结束）

	/**
	 * 应用版本渠道包ID
	 */
	private Long packId;

	/**
	 * 应用版本ID
	 */
	private Long versionId;

	/**
	 * 父渠道ID
	 */
	private Integer parentChlId;

	//主渠道code
	private String parentCode;

	/**
	 * 子渠道ID
	 */
	private Integer chlId;

	/**
	 * 下载链接
	 */
	private String downloadUrl;

	/**
	 * 状态：1：正常；2：失效
	 */
	private Integer status;

	/**
	 * 创建人
	 */
	private String creator;

	List<Integer> channelIdList;

	/**
	 * 推广类型（1信息流 2短视频）
	 */
	private Integer spreadType;

	/**
	 * 结算类型(0默认 1KOL(一口价) 2KOL(CPM按次))
	 */
	private String settleType;
	/**
	 * 母包类型：1-运营母包；2-云微端；
	 */
	private Integer packType;

}