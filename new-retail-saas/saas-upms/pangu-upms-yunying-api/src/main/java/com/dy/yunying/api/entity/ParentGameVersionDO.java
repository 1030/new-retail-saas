package com.dy.yunying.api.entity;

import com.dy.yunying.api.enums.PackTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Objects;

/**
 * 游戏母包
 * parent_game_version
 *
 * @author hjl
 * @date 2020-08-10 16:48:46
 */
@Getter
@Setter
public class ParentGameVersionDO {
	/**
	 * 父游戏母包版本ID
	 */
	private Long versionId;

	/**
	 * 母包类型：1-运营母包；2-云微端；
	 */
	private Integer packType;

	/**
	 * 父游戏ID
	 */
	private Long gameId;

	/**
	 * APP包名
	 */
	private String pkName;

	/**
	 * 版本编码，数字，如56
	 */
	private Integer code;

	/**
	 * 版本名称，字符串，如：v3.2.5
	 */
	private String name;

	/**
	 * 图标
	 */
	private String icon;

	/**
	 * 版本更新内容
	 */
	private String content;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 文件名称
	 */
	private String fileName;

	/**
	 * 文件名称
	 */
	private String size;

	/**
	 * 文件路径
	 */
	private String path;

	/**
	 * 状态：1：正常；2：失效
	 */
	private Integer status;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	public String getPackTypeText(){
		if (Objects.nonNull(packType)){
			return PackTypeEnum.getName(packType);
		}
		return "";
	}
}