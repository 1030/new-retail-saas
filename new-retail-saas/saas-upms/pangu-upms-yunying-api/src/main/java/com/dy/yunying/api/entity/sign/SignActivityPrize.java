package com.dy.yunying.api.entity.sign;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.dy.yunying.api.vo.sign.GoodsVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 签到活动奖品表
 * @author  chengang
 * @version  2021-12-01 10:14:16
 * table: sign_activity_prize
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sign_activity_prize")
public class SignActivityPrize extends Model<SignActivityPrize>{

		//主键id
		@TableId(value = "id",type = IdType.AUTO)
		private Long id;


		//活动ID
		@TableField(value = "activity_id")
		private Long activityId;


		//领取序号
		@TableField(value = "receive_sn")
		private Integer receiveSn;


		//领取时间
		@TableField(value = "receive_date")
		private Date receiveDate;


		//名称
		@TableField(value = "name")
		private String name;


		//类型  1游戏物品 2代金券
		@TableField(value = "type")
		private Integer type;


		//代金券名称
		@TableField(value = "cdk_name")
		private String cdkName;


		//代金券金额
		@TableField(value = "cdk_amount")
		private BigDecimal cdkAmount;


		//限制金额
		@TableField(value = "cdk_limit_amount")
		private BigDecimal cdkLimitAmount;


		//有效规则类型  1固定时间 2领券后指定天数
		@TableField(value = "expiry_type")
		private Integer expiryType;


	//有效开始时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "start_time")
	private Date startTime;


	//有效结束时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "end_time")
	private Date endTime;


		//有效天数
		@TableField(value = "days")
		private Integer days;

	//代金券限制类型 1:无门槛 2:指定游戏 3:指定角色
	@TableField(value = "cdk_limit_type")
	private Integer cdkLimitType;

	/**
	 * 礼包码类型：1唯一码，2通用码
	 */
	@TableField(value = "gift_type")
	private Integer giftType;
	/**
	 * 礼包通用码
	 */
	@TableField(value = "gift_code")
	private String giftCode;
	/**
	 * 礼包总数量
	 */
	@TableField(value = "gift_amount")
	private Integer giftAmount;

	/**
	 * 游豆价值
	 */
	@TableField(value = "currency_amount")
	private BigDecimal currencyAmount;

	/**
	 * 游豆类型：1-永久；2-临时；
	 */
	@TableField(value = "currency_type")
	private Integer currencyType;
	/**
	 * 代金券类型：1-满减；2-折扣；
	 */
	@TableField(value = "cdk_type")
	private Integer cdkType;

	/**
	 * 游豆过期类型：1-固定结束时间；2-领取后过期天数；
	 */
	@TableField(value = "currency_expire_type")
	private Integer currencyExpireType;

	/**
	 * 游豆固定过期时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "currency_expire_time")
	private Date currencyExpireTime;

	/**
	 * 游豆领取后过期天数
	 */
	@TableField(value = "currency_expire_days")
	private Integer currencyExpireDays;

	/**
	 * 游豆使用范围：1-平台通用；2-父游戏；3-子游戏；4-角色；
	 */
	@TableField(value = "currency_use_scope")
	private Integer currencyUseScope;

	/**
	 * 游豆使用主游戏
	 */
	@TableField(value = "currency_use_pgame_id")
	private Long currencyUsePgameId;

	/**
	 * 游豆使用子游戏
	 */
	@TableField(value = "currency_use_game_id")
	private Long currencyUseGameId;

	/**
	 * 游豆使用区服
	 */
	@TableField(value = "currency_use_area_id")
	private String currencyUseAreaId;

	/**
	 * 游豆使用角色
	 */
	@TableField(value = "currency_use_role_id")
	private String currencyUseRoleId;

	//是否删除  0否 1是
	@TableField(value = "deleted")
	private Integer deleted;


	//创建时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "create_time")
	private Date createTime;


	//修改时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "update_time")
	private Date updateTime;


		//创建人
		@TableField(value = "create_id")
		private Long createId;


		//修改人
		@TableField(value = "update_id")
		private Long updateId;


		@TableField(exist = false)
		private List<GoodsVO> goodsList = new ArrayList<>();


		@TableField(exist = false)
		private Long giftTotalNum = 0L;


		@TableField(exist = false)
		private Long giftUseNum = 0L;

		// (1待上线 2活动中 3已下线)
		@TableField(exist = false)
		private Integer activityStatus;

		@TableField(exist = false)
		private Long sendNum = 0L;


}

	

	
	

