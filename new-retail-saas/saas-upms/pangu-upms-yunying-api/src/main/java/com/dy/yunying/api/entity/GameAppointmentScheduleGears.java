package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 游戏预约进度档位表
 * @author  leisw
 * @version  2022-01-17 17:14:29
 * table: game_appointment_schedule_gears
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "game_appointment_schedule_gears")
public class GameAppointmentScheduleGears extends Model<GameAppointmentScheduleGears>{
	/**
	 * 主键id
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	@TableField(value = "type")
	private String type;
	/**
	 * 预约游戏名称
	 */
	@TableField(value = "game_name")
	private String gameName;
	/**
	 * 档位规定人数
	 */
	@TableField(value = "rule_number")
	private Long ruleNumber;
	/**
	 * 奖励内容
	 */
	@TableField(value = "reward")
	private String reward;
	/**
	 * 图片名称
	 */
	@TableField(value = "name")
	private String name;
	/**
	 * 文件名称
	 */
	@TableField(value = "bucket_name")
	private String bucketName;
	/**
	 * 文件访问地址
	 */
	@TableField(value = "file_url")
	private String fileUrl;

	/**
	 * 文件访问地址
	 */
	@TableField(value = "is_delete")
	private Integer isDelete;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
}






