package com.dy.yunying.api.enums;

/**
 * @ClassName RetentionKpiEnum
 * @Description done
 * @Author nieml
 * @Time 2021/6/21 17:12
 * @Version 1.0
 **/
public enum HourDataKpiEnum {

	REG("reg","新增设备","newRegNums"),

	PAYDEVICES("payDevice","付费设备","payAmount"),

	PAY("pay","新增充值金额","newRegPayAmount"),

	LTV("ltv","LTV","ltv");

	private HourDataKpiEnum(String type, String name,String fieldName) {
		this.type = type;
		this.name = name;
		this.fieldName = fieldName;
	}

	public static String getName(String type) {
		for (HourDataKpiEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return "未知";
	}

	/**
	 * 根据type查询fieldName
	 * */
	public static String getFieldName(String type){
		for (HourDataKpiEnum value : HourDataKpiEnum.values()) {
			if (value.getType().equals(type)){
				return value.getFieldName();
			}
		}
		return "未知";
	}

	private String type;
	private String name;
	private String fieldName;

	public String getType() {
		return type;
	}


	public String getName() {
		return name;
	}


	public String getFieldName() {
		return fieldName;
	}


}
