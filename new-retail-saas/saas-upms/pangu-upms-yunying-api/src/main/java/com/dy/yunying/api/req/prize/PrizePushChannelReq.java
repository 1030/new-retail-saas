package com.dy.yunying.api.req.prize;
import io.swagger.annotations.ApiModelProperty;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

/**
 * 奖励与渠道关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:48
 * table: prize_push_channel
 */
@Data
public class PrizePushChannelReq extends Page {

	@ApiModelProperty(value = "主键ID")
	private String id;

	@ApiModelProperty(value = "奖励推送ID")
	private String prizePushId;

	@ApiModelProperty(value = "主渠道编码")
	private String parentChl;

	@ApiModelProperty(value = "子渠道编码")
	private String chl;

	@ApiModelProperty(value = "分包渠道编码")
	private String appChl;

	@ApiModelProperty(value = "渠道层级(1主渠道 2子渠道 3分包渠道)")
	private String channelRange;
}
