package com.dy.yunying.api.vo.hbdata;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/18 16:53
 * @description：
 * @modified By：
 */
@Data
public class HbActiveRuleVo implements Serializable {

	// 活动名称
	private String activityName;

	// PV
	private Integer servicePV;

	// UV
	private Integer serviceUV;
}
