package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * class info
 *
 * @author sunyouquan
 * @date 2022/5/16 14:03
 */
public enum PrizePushHbTypeEnum {
	LEVEL(1, "等级红包"),
	RECHARGE(2, "充值红包"),
	INVITATION(3, "邀请红包"),
	CUSTOM(4,"定制红包");

	private Integer type;

	private String name;

	PrizePushHbTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonCreator
	public static PrizePushHbTypeEnum getPrizePushHbTypeEnum(Integer value) {
		for (PrizePushHbTypeEnum prizePushHbTypeEnum : values()) {
			if (prizePushHbTypeEnum.type.equals(value)) {
				return prizePushHbTypeEnum;
			}
		}
		return null;
	}

	@JsonCreator
	public static PrizePushHbTypeEnum getPrizePushHbTypeEnumByName(String value) {
		for (PrizePushHbTypeEnum prizePushHbTypeEnum : values()) {
			if (prizePushHbTypeEnum.name.equals(value)) {
				return prizePushHbTypeEnum;
			}
		}
		return null;
	}

	@JsonValue
	public Integer getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}
}
