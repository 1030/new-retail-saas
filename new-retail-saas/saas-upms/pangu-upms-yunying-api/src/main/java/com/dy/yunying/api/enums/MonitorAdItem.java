package com.dy.yunying.api.enums;

/**
 * 废弃：老版本监控
 */
public enum MonitorAdItem {

	ADACCOUNT(1,"广告账户"),
	ADPLAN(2,"广告计划"),
	;
	MonitorAdItem(Integer type,String desc){
		this.type =type;
		this.desc =desc;
	}



	/**
	 * 类型
	 */
	private final Integer type;

	/**
	 * 描述
	 */
	private final String desc;

	public Integer getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}
}
