/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * CDK信息表
 *
 * @author yuwenfeng
 * @date 2021-11-16 09:57:41
 */
@Data
@Accessors(chain = true)
@TableName("wan_cdk")
@ApiModel(value = "CDK信息表")
public class WanCdk implements Serializable {

	/**
	 * CDK码
	 */
	@TableId(value = "cdk_code", type = IdType.ASSIGN_UUID)
	@ApiModelProperty(value = "CDK码")
	private String cdkCode;

	/**
	 * CDK名称
	 */
	@ApiModelProperty(value = "CDK名称")
	private String cdkName;

	/**
	 * 礼包id，如果是首充cdk，则本字段为空
	 */
	@ApiModelProperty(value = "礼包id，如果是首充cdk，则本字段为空")
	private Long bagId;

	/**
	 * 金额
	 */
	@ApiModelProperty(value = "金额")
	private BigDecimal money;

	/**
	 * 单位
	 */
	@ApiModelProperty(value = "单位")
	private String unit;

	/**
	 * 所属者id
	 */
	@ApiModelProperty(value = "所属者id")
	private Long belongsId;

	/**
	 * 使用者id
	 */
	@ApiModelProperty(value = "使用者id")
	private Long usedId;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	/**
	 * 发放人id
	 */
	@ApiModelProperty(value = "发放人id")
	private Long giveId;

	/**
	 * 分配时间
	 */
	@ApiModelProperty(value = "分配时间")
	private Date giveTime;

	/**
	 * 使用时间
	 */
	@ApiModelProperty(value = "使用时间")
	private Date useTime;

	/**
	 * 创建者id，为0时则表示为系统自动创建
	 */
	@ApiModelProperty(value = "创建者id，为0时则表示为系统自动创建")
	private Long createUserId;

	/**
	 * 创建者名称
	 */
	@ApiModelProperty(value = "创建者名称")
	private String createUserName;

	/**
	 * 有效期类型 1:长期有效 2:指定有效期
	 */
	@ApiModelProperty(value = "有效期类型 1:长期有效 2:指定有效期")
	private Integer expiryType;

	/**
	 * 有效开始时间
	 */
	@ApiModelProperty(value = "有效开始时间")
	private Date startTime;

	/**
	 * 有效结束时间
	 */
	@ApiModelProperty(value = "有效结束时间")
	private Date endTime;

	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String remark;

	/**
	 * 使用状态：10、未使用；20、已使用
	 */
	@ApiModelProperty(value = "使用状态：10、未使用；20、已使用")
	private Integer useStatus;

	/**
	 * 类型 1:首充，2:活动， 3:专属充值券 4:3367代金券卡包
	 */
	@ApiModelProperty(value = "类型 1:首充，2:活动， 3:专属充值券 4:3367代金券卡包")
	private Integer cdkType;

	/**
	 * 等级(字典表code)
	 */
	@ApiModelProperty(value = "等级(字典表code)")
	private String dictVipCode;

	/**
	 * 金额限制类型 1:无门槛 2:指定金额
	 */
	@ApiModelProperty(value = "金额限制类型 1:无门槛 2:指定金额")
	private Integer limitMoneyType;

	/**
	 * 限制金额(使用时金额超过该金额才可以使用)
	 */
	@ApiModelProperty(value = "限制金额(使用时金额超过该金额才可以使用)")
	private BigDecimal limitMoney;

	@ApiModelProperty(value = "子游戏id")
	private Long gameid;

	@ApiModelProperty(value = "父游戏ID")
	private Long pgid;

	@ApiModelProperty(value = "冻结时间(毫秒)")
	private Long freezeTime;

	@ApiModelProperty(value = "代金券限制类型 1:无门槛 2:指定游戏 3:指定角色")
	private Integer cdkLimitType;

	@ApiModelProperty(value = "来源ID")
	private Long sourceId;

	@ApiModelProperty(value = "来源类型")
	private Integer sourceType;

	@ApiModelProperty(value = "来源业务ID")
	private Integer sourceBusiness;

	@ApiModelProperty(value = "区服ID")
	private String areaId;

	@ApiModelProperty(value = "角色ID")
	private String roleId;

	@ApiModelProperty(value = "角色名称")
	private String roleName;
}
