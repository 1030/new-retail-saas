package com.dy.yunying.api.vo.hongbao;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description 运营后台活动榜单VO
 * @Author chengang
 * @Date 2021/10/28
 */
@Data
public class InvitationTopVo implements Serializable {

	private Long activityId;
	private Long userId;
	private String roleId;
	private String roleName;
	private BigDecimal totalIncome;

	public InvitationTopVo(Long activityId,Long userId,String roleId,String roleName,BigDecimal totalIncome){
		this.activityId = activityId;
		this.userId = userId;
		this.roleId = roleId;
		this.roleName = roleName;
		this.totalIncome = totalIncome;
	}

}
