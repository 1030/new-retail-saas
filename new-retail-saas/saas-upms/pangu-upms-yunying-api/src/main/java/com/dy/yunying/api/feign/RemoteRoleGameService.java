package com.dy.yunying.api.feign;

import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/22
 */
@FeignClient(contextId = "remoteRoleGameService", value = "pangu-upms-yunying-biz")
public interface RemoteRoleGameService {

	/**
	 * 获取自身权限下的主游戏ID列表
	 *
	 * @return R
	 */
	@GetMapping("/rolegame/getOwnerRolePGameIds")
	R<List<Long>> getOwnerRolePGameIds();

	/**
	 * 获取自身权限下的子游戏ID列表
	 *
	 * @return R
	 */
	@GetMapping("/rolegame/getOwnerRoleGameIds")
	R<List<Long>> getOwnerRoleGameIds();

}
