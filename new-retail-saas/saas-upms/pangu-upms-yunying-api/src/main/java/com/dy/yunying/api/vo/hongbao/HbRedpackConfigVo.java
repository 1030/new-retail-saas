package com.dy.yunying.api.vo.hongbao;

import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * 红包配置表
 * @author  chenxiang
 * @version  2021-10-25 20:36:34
 * table: hb_redpack_config
 */
@Data
public class HbRedpackConfigVo extends Page {
	/**
	 * 红包ID
	 */
	private String id;
	/**
	 * 活动ID
	 */
	private String activityId;
	/**
	 * 红包内容：1现金红包，2礼包码，3实物礼品，4代金券
	 */
	private String type;
	/**
	 * 领取条件类型：1角色等级，2角色等级和创角时间，3角色累计充值，4角色首次充值，5角色单笔充值满，6邀请好友成功注册游戏，7邀请好友首次提现成功，8邀请好友累计充值，9邀请好友创角N天内达到N级 11 角色单笔充值等于
	 */
	private String conditionType;

	//1普通  2临时
	private Integer currencyType;
	/**
	 * 领取条件：角色等级
	 */
	private String conditionRoleLevel;
	/**
	 * 领取条件：创角天数
	 */
	private String conditionRoleDays;
	/**
	 * 领取条件：角色充值金额
	 */
	private String conditionRoleRecharge;
	/**
	 * 红包数值
	 */
	private String money;
	/**
	 * 使用门槛（0为不限制）
	 */
	private String limitMoney;
	/**
	 * 现金价值
	 */
	private String cashValues;
	/**
	 * 礼包总数量
	 */
	private String giftAmount;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 用户可领数量
	 */
	private String useAmount;
	/**
	 * 角标：0无，1推荐
	 */
	private String angleMark;
	/**
	 * 详情状态：0关闭，1开启
	 */
	private String detailsStatus;
	/**
	 * 详情
	 */
	private String details;
	/**
	 * 状态：0停用，1启用
	 */
	private String status;
	/**
	 * 排序
	 */
	private String sort;
	/**
	 * 代金券有效期类型：1永久有效，2固定时间，3指定天数
	 */
	private String voucherType;
	/**
	 * 代金券领取后有效天数
	 */
	private String voucherDays;
	/**
	 * 代金券有效开始时间
	 */
	private String voucherStartTime;
	/**
	 * 代金券有效结束时间
	 */
	private String voucherEndTime;
	/**
	 * 代金券限制类型 1:无门槛 2:指定游戏 3:指定角色
	 */
	private Integer cdkLimitType;
	/**
	 * 礼包码类型：1唯一码，2通用码
	 */
	private String giftType;
	/**
	 * 礼包通用码
	 */
	private String giftCode;
	/**
	 * 红包icon地址
	 */
	private String iconUrl;
	/**
	 * 红包总数是否显示：0不显示，1显示
	 */
	private String amountShow;
	/**
	 * 文件
	 */
	private MultipartFile file;


	//游豆
	private String currencyAmount;


	//游戏任务id
	private String conditionTaskId;

	//是否自动领取，1-是；2-否
	private Integer autoReceive;
	
}
