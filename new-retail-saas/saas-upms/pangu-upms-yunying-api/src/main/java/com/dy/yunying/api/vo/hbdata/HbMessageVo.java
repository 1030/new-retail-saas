package com.dy.yunying.api.vo.hbdata;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/26 11:31
 * @description：
 * @modified By：
 */
@Data
public class HbMessageVo implements Serializable {

	//活动ID
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活动ID")
	private Long activityId;

	// 活动名称
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("活动名称")
	private String activityName;

	// 消息事件
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("消息事件")
	private String triggerName;

	// 消息内容
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("消息内容")
	private String floatContent;

	// 推送数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("推送数")
	private Long pushNum;

	// PV
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("PV")
	private Long servicePV;

	// UV
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("UV")
	private Long serviceUV;
}
