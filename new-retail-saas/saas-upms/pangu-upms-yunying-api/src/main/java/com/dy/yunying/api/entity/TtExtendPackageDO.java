package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author ：lile
 * @date ：2021/8/5 11:49
 * @description：
 * @modified By：
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "tt_extend_package")
public class TtExtendPackageDO extends Model<TtExtendPackageDO> {


	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 广告主ID
	 */
	private String advertiserId;

	/**
	 * 应用包ID
	 */
	private String packageId;

	/**
	 * 游戏id
	 */
	private Integer gameid;

	/**
	 * 渠道应用id
	 */
	private String appId;

	/**
	 * 渠道应用名称
	 */
	private String appName;

	/**
	 * 推广渠道编码（第一级）
	 */
	private String parentchl;

	/**
	 * 平台类型:0-3399平台,1-头条,2-百度,3-新数,4-公会,7-UC,8-广点通
	 */
	private Integer platform;

	/**
	 * 分包失败原因
	 */
	private String reason;

	/**
	 * 状态,"NOT_UPDATE"： 未更新,"CREATING"：创建中,"UPDATING"：更新中,"PUBLISHED"：已发布,"CREATION_FAILED"：创建失败,"UPDATE_FAILED"：更新失败
	 */
	private String status;

	/**
	 * 第三方更新时间
	 */
	private String ttUpdateTime;

	/**
	 * 版本号
	 */
	private String versionName;


	/**
	 * 游戏包渠道编码（第三级）
	 */
	private String channelId;

	/**
	 * 下载链接
	 */
	private String downloadUrl;


	/**
	 * 是否删除  0否 1是
	 */
	private Integer isDeleted;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 创建人
	 */
	private Long createId;
	/**
	 * 修改人
	 */
	private Long updateId;
}
