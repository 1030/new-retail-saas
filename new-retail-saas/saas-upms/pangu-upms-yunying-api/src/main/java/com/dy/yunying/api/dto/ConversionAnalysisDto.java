package com.dy.yunying.api.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * @Description 转化分析报表数据传输对象
 * @Author chengang
 * @Date 2022/2/28
 */
@Data
@Accessors(chain = true)
public class ConversionAnalysisDto extends Page {

	/**
	 * 开始日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate startDate;

	/**
	 * 结束日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate endDate;

	/**
	 * 主渠道编码
	 */
	private String parentchl;

	/**
	 * 主游戏ID，多个ID使用逗号分隔
	 */
	private String pgidArr;

	/**
	 * 主游戏ID列表
	 */
	private Set<Long> pgids;

	/**
	 * 子游戏ID，多个ID使用逗号分隔
	 */
	private String gameidArr;

	/**
	 * 子游戏ID列表
	 */
	private Set<Long> gameids;

	/**
	 * 部门ID，多个ID使用逗号分隔
	 */
	private String deptIdArr;

	/**
	 * 部门ID列表
	 */
	private Set<Long> deptIds;

	/**
	 * 组别ID，多个ID使用逗号分隔
	 */
	private String userGroupIdArr;

	/**
	 * 组别ID列表
	 */
	private Set<Long> userGroupIds;

	/**
	 * 投放人ID，多个ID使用逗号分隔
	 */
	private String investorIdArr;

	/**
	 * 投放人ID列表
	 */
	private Set<Long> investorIds;

	/**
	 * 系统标识
	 */
	private Integer os;

	/**
	 * 分组周期：day(1)-按日；week(2)-按周；month(3)-按月；year-按年；collect(4)-汇总；
	 */
	private Integer cycleType;

	/**
	 * 分组字段列表，多个使用逗号分隔：investorId-投放人ID；parentchl-主渠道编码；deptId-部门ID；pgid-主游戏ID；gameid-子游戏ID；os-系统标识；
	 */
	private String groupByArr;

	/**
	 * 分组字段列表：investorId-投放人ID；parentchl-主渠道编码；deptId-部门ID；pgid-主游戏ID；gameid-子游戏ID；os-系统标识；
	 */
	private List<String> groupBys;

	/**
	 * 排序字段名称
	 */
	private String orderByName;

	/**
	 * 排序顺序：0-升序（ASC）；1-降序（DESC）
	 */
	private Integer orderByAsc;

	/**
	 * 导出列名称，多个使用逗号分隔
	 */
	private String exportTitles;

	/**
	 * 导出字段名称，多个使用逗号分隔
	 */
	private String exportColumns;

	/**
	 * 是否为系统管理员
	 */
	private Boolean isAdmin;

	/**
	 * 权限相关投放人条件
	 */
	private Set<Long> prvInvestorIds;

	/**
	 * 等级
	 */
	private String level = "1";


}
