package com.dy.yunying.api.resp.yyz;

import lombok.Data;

import java.io.Serializable;

/**
 * 游戏预约表
 * @author  kongyanfang
 * @version  2022-01-17 17:14:29
 * table: game_appointment
 */
@Data
public class GameAppointmentExportRes implements Serializable {

	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 游戏名称
	 */
	private String gameName;
	/**
	 * 预约手机号
	 */
	private String mobile;
	/**
	 * 预约设备名称（1：IOS  2：安卓）
	 */
	private String appointDeviceName;
	/**
	 * 预约来源名称（1：PC  2：H5）
	 */
	private String appointSourceName;
	/**
	 * 预约时间
	 */
	private String appointTime;

}


