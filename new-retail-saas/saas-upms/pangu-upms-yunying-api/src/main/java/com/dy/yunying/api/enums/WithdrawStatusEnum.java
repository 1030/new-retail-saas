package com.dy.yunying.api.enums;

public enum WithdrawStatusEnum {
    AUDITING(0,"审核中"),
    PASS(1,"审核通过"),
    WITHDRAWING(2,"提现中"),
    FAILURE(3,"提现失败"),
    SUCCESS(4,"提现成功"),
    COMPLETED(5,"交易成功"),
            ;

    private int val;
    private String desc;

    WithdrawStatusEnum(int val, String desc) {
        this.val = val;
        this.desc = desc;
    }

    public static WithdrawStatusEnum getOneByVal(int val){
        for(WithdrawStatusEnum typeEnum : WithdrawStatusEnum.values()){
            if(typeEnum.getVal() == val){
                return typeEnum;
            }
        }
        return null;
    }

    public int getVal() {
        return val;
    }

    public String getDesc() {
        return desc;
    }
}
