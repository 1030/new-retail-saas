package com.dy.yunying.api.req.hongbao;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/10/27 17:00
 * @description：
 * @modified By：
 */
@Data
public class PopupNoticeReq extends Page implements Serializable {

	@ApiModelProperty(value = "活动ID")
	@NotNull(message = "活动ID不能为空")
	private Long id;

}
