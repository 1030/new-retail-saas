package com.dy.yunying.api.datacenter.dto;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName HourDataDto
 * @Description
 * @Author nieml
 * @Time 2021/6/24 14:24
 * @Version 1.0
 **/

@Data
public class HourDataDto {

	private Long current;
	private Long size;

	/*
	 * 筛选条件
	 * */
	@ApiModelProperty("查询日期")
//	@NotNull(message = "查询日期不允许为空")
	private Long date;
	/*
	 * 筛选条件
	 * */
	@ApiModelProperty("查询开始日期")
//	@NotNull(message = "查询开始日期不允许为空")
	private Long rsTime;
	/*
	 * 筛选条件
	 * */
	@ApiModelProperty("查询结束日期")
//	@NotNull(message = "查询结束日期不允许为空")
	private Long reTime;

	@ApiModelProperty("父游戏")
	private String pgidArr;
	@ApiModelProperty("子游戏")
	private String gameidArr;
	private List<Long> pgIdList;

	@ApiModelProperty("部门id数组,多个用逗号分隔")
	private String deptIdArr;
	public List<Long> deptIdList;
	@ApiModelProperty(value = "用户组Id列表,多个用逗号分隔")
	private String userGroupIdArr;
	@ApiModelProperty("投放人数组")
	private String investorArr;
	private List<Integer> investorList;

	@ApiModelProperty("主渠道")
	private String parentchlArr;

	@ApiModelProperty("分包渠道")
	private String appchlArr;

	@ApiModelProperty("客户端操作系统0=android；1=IOS；3=OTHERS或为空；4=H5; 5=PC")
	private Integer os;

	/*
	 *类别
	 * */
	@ApiModelProperty("类别 多个用逗号分隔【系统 os,主游戏 pgid，子游戏 gid ,部门 deptId,渠道编码 parentchl，分包编码：packCode,广告计划 adId,  投放人investor   】")
	private String queryColumn;

	/*
	 * 指标
	 * */
	@ApiModelProperty("指标 新增设备、付费设备、新增充值金额、LTV")
	private String hourDataKpi;

	@ApiModelProperty("广告账号数组")
	private String advertiserIdArr;
	private List<String> advertiserIdList;

	private String titles;
	private String columns;

	public List<String> getAdvertiserIdList() {
		List<String> list = new ArrayList<>();
		return StringUtils.isNotEmpty(advertiserIdArr) ? Arrays.asList(advertiserIdArr.split(",")) : list;
	}

	public void setAdvertiserIdList(List<String> advertiserIdList) {
		this.advertiserIdList = advertiserIdList;
	}

	public List<Long> getDeptIdList() {
		List<Long> list = new ArrayList<>();
		return StringUtils.isNotEmpty(deptIdArr) ? Arrays.stream(Arrays.stream(deptIdArr.split(",")).mapToLong(Long::parseLong).toArray()).boxed().collect(Collectors.toList()) : list;
	}

	public List<Integer> getInvestorList() {
		List<Integer> list = new ArrayList<>();
		return StringUtils.isNotEmpty(investorArr) ? Arrays.stream(Arrays.stream(investorArr.split(",")).mapToInt(Integer::parseInt).toArray()).boxed().collect(Collectors.toList()) : list;
	}

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	@ApiModelProperty("是否系统管理员")
	private int isSys;

	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	public String getAdAccounts() {
		String adAccounts = "'NO'";  // 默认不查询任何广告账户
		if (CollectionUtil.isNotEmpty(roleAdAccountList)) {
			adAccounts = "'" + StringUtils.join(roleAdAccountList, "','") + "'";
		}
		return adAccounts;
	}

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

	private Boolean enableTest = false;

	@ApiModelProperty("周期类型 ：1 按天 2 按周  3  按月 4 年（默认）")
	private Integer cycleType = 4;

	public String getPeriod() {
		String period = "'year'";
		switch (cycleType) {
			case 1:
				period = "day";
				break; //可选
			case 2:
				period = "week";
				//语句
				break; //可选
			case 3:
				period = "month";
				break; //可选
			case 4:
				period = "year";
				break; //可选
			case 5:
				period = "collect";
				break; //可选
			//你可以有任意数量的case语句
			default: //可选
				//语句
		}
		return period;
	}

}
