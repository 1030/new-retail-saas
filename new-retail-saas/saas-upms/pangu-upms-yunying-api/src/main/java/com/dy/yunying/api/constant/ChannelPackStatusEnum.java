package com.dy.yunying.api.constant;

/**
 * 渠道分包状态
 * 0：待生成， 1：生成中，2：已生成（有效）；3：生成失败  4:删除
 * 
 * @author hjl
 * @date 2020-5-7 20:00:47
 */
public enum ChannelPackStatusEnum {

	INIT(0, "待生成"),
	GENERATING(1, "生成中"),
	VALID(2, "已生成（有效） "),
	FAIL(3, "生成失败"),
	DELETE(4, "删除");

	private ChannelPackStatusEnum(Integer status, String name) {
		this.status = status;
		this.name = name;
	}
	public static String getName(Integer status) {
		for (ChannelPackStatusEnum ele : values()) {
			if(ele.getStatus().equals(status)) {
				return ele.getName();
			}
		}
		return "未知";
	}

	private Integer status;

	private String name;

	public Integer getStatus() {
		return status;
	}

	public String getName() {
		return name;
	}
}
