package com.dy.yunying.api.entity.sign;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 签到活动任务配置表
 *
 * @TableName sign_task_config
 */
@Data
@Accessors(chain = true)
@TableName(value = "sign_task_config")
public class SignTaskConfigDO implements Serializable {

	/**
	 *
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 活动ID
	 */
	@TableField(value = "activity_id")
	private Long activityId;

	/**
	 * 任务类型：2-充值任务，...
	 */
	@TableField(value = "task_type")
	private Integer taskType;

	/**
	 * 领取条件类型：10-角色当日累计充值，...
	 */
	@TableField(value = "condition_type")
	private Integer conditionType;

	/**
	 * 领取条件：角色充值金额
	 */
	@TableField(value = "condition_role_recharge")
	private Integer conditionRoleRecharge;

	/**
	 * 任务名称
	 */
	@TableField(value = "task_name")
	private String taskName;

	/**
	 * 任务开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "start_time")
	private Date startTime;

	/**
	 * 任务结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "end_time")
	private Date endTime;

	/**
	 * 额外奖励类型：1-补签次数
	 */
	@TableField(value = "extra_bonus_type")
	private Integer extraBonusType;

	/**
	 * 额外奖励数量
	 */
	@TableField(value = "extra_bonus_num")
	private Integer extraBonusNum;

	/**
	 * 奖品类型：1-游戏物品；2-代金券；3-游豆；
	 */
	@TableField(value = "prize_type")
	private Integer prizeType;

	/**
	 * 礼包码类型：1唯一码，2通用码
	 */
	@TableField(value = "gift_type")
	private Integer giftType;

	/**
	 * 礼包通用码
	 */
	@TableField(value = "gift_code")
	private String giftCode;

	/**
	 * 礼包总数量
	 */
	@TableField(value = "gift_amount")
	private Integer giftAmount;

	/**
	 * 代金券名称
	 */
	@TableField(value = "cdk_name")
	private String cdkName;

	/**
	 * 代金券金额
	 */
	@TableField(value = "cdk_amount")
	private BigDecimal cdkAmount;

	/**
	 * 限制金额
	 */
	@TableField(value = "cdk_limit_amount")
	private BigDecimal cdkLimitAmount;

	/**
	 * 有效过期类型：1-固定结束时间；2-领取后过期天数；
	 */
	@TableField(value = "cdk_expire_type")
	private Integer cdkExpireType;

	/**
	 * 代金券有效开始时间
	 */
	@TableField(value = "cdk_start_time")
	private Date cdkStartTime;

	/**
	 * 代金券有效结束时间
	 */
	@TableField(value = "cdk_end_time")
	private Date cdkEndTime;

	/**
	 * 代金券限制类型：1-无门槛；2-指定游戏；3-指定角色；
	 */
	@TableField(value = "cdk_limit_type")
	private Integer cdkLimitType;

	/**
	 * 代金券领取后过期天数
	 */
	@TableField(value = "cdk_expire_days")
	private Integer cdkExpireDays;

	/**
	 * 代金券类型：1-满减；2-折扣；
	 */
	@TableField(value = "cdk_type")
	private Integer cdkType;

	/**
	 * 游豆价值
	 */
	@TableField(value = "currency_amount")
	private BigDecimal currencyAmount;

	/**
	 * 游豆类型：1-永久游豆；2-临时游豆；
	 */
	@TableField(value = "currency_type")
	private Integer currencyType;

	/**
	 * 游豆过期类型：1-固定结束时间；2-领取后过期天数；
	 */
	@TableField(value = "currency_expire_type")
	private Integer currencyExpireType;

	/**
	 * 游豆固定过期时间
	 */
	@TableField(value = "currency_expire_time")
	private Date currencyExpireTime;

	/**
	 * 游豆领取后过期天数
	 */
	@TableField(value = "currency_expire_days")
	private Integer currencyExpireDays;

	/**
	 * 游豆使用范围：1-平台通用；2-父游戏；3-子游戏；4-角色；
	 */
	@TableField(value = "currency_use_scope")
	private Integer currencyUseScope;

	/**
	 * 游豆使用主游戏
	 */
	@TableField(value = "currency_use_pgame_id")
	private Long currencyUsePgameId;

	/**
	 * 游豆使用子游戏
	 */
	@TableField(value = "currency_use_game_id")
	private Long currencyUseGameId;

	/**
	 * 游豆使用区服
	 */
	@TableField(value = "currency_use_area_id")
	private String currencyUseAreaId;

	/**
	 * 游豆使用角色
	 */
	@TableField(value = "currency_use_role_id")
	private String currencyUseRoleId;

	/**
	 * 是否删除：0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

	/*
	 * 非数据库字段
	 */

	/**
	 * 是否可编辑，0-不可编辑；1-可编辑
	 */
	@TableField(exist = false)
	private Integer editable;

	/**
	 * 礼包码总数量
	 */
	@TableField(exist = false)
	private Integer giftTotalCount;

	/**
	 * 礼包码已使用数量
	 */
	@TableField(exist = false)
	private Integer giftUsedCount;

	/**
	 * 奖品领取数量
	 */
	@TableField(exist = false)
	private Integer receiveCount;

	/**
	 * 任务礼品列表
	 */
	@TableField(exist = false)
	private List<SignTaskGoodsDO> goodsList;

}