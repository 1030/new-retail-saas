package com.dy.yunying.api.resp;

import lombok.Getter;
import lombok.Setter;

/**
 * 分包响应
 *
 * @Author: hjl
 * @Date: 2020/6/11 20:59
 */
@Getter
@Setter
public class ChannelPackRes {

	/**
	 * 分包ID
	 */
	private Long packId;

	private String packCode;

	private String path;

}