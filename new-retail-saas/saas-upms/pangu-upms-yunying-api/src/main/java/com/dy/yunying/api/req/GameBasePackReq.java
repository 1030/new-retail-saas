package com.dy.yunying.api.req;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 游戏基础包请求
 *
 * @Author: hjl
 * @Date: 2020/6/11 20:59
 */
@Getter
@Setter
public class GameBasePackReq implements Serializable {

    /**
     * 游戏ID
     */
    private Long gameId;

    /**
     * 父渠道ID
     */
    private Integer parentChlId;

	/**
	 * 平台ID
	 */
	private String platformId;

    /**
     * 第三方平台上报应用ID
     */
    private Long appId;

    /**
     * 第三方平台上报应用英文名
     */
    private String appName;
    /**
     * 广点通appID
     */
    private String gdtAppid;

    /**
     * 上报方式
     */
    private Integer reportType;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 分包类型
     * 1：渠道分包
     * 2：主渠道基础包
     */
    private Integer packType;


    //付费金额上报  1 真实上报2 固定金额上报 3 固定比率上报
    private Integer payMoneyType;

    //固定金额
    private BigDecimal fixedMoney;

    //固定比率
	private BigDecimal fixedRatio;
}