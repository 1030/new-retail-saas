package com.dy.yunying.api.resp.hongbao;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description
 * @Author chengang
 * @Date 2022/1/12
 */
@Data
public class CashExportVo implements Serializable {

	/**
	 * 礼包码ID
	 */
	private Long giftId;


	/**
	 * 红包ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("档次ID")
	private Long id;

	/**
	 * 提现档次
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("提现档次")
	private String name;

	/**
	 * 现金价值(元)
	 */
	@ColumnWidth(20)
	@ContentStyle(dataFormat = 2)
	@HeadFontStyle
	@ExcelProperty("现金价值(元)")
	private BigDecimal money;

	/**
	 * 礼包码类型
	 */
	@ColumnWidth(20)
	@ExcelProperty("礼包码类型")
	@HeadFontStyle
	private String giftType;

	/**
	 * 礼包码
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("礼包码")
	private String giftCode;

	/**
	 * 剩余数量
	 */
	@ColumnWidth(15)
	@HeadFontStyle(fontHeightInPoints = 16)
	@ExcelProperty("剩余数量")
	private Long remainingNum;


}
