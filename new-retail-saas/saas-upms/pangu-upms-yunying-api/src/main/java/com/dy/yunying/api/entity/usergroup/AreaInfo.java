package com.dy.yunying.api.entity.usergroup;


import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * 用户群组地区表
 * @author zhuxm
 * @date 2022-05-19 19:50:21
 */
@ApiModel(value = "地区表")
@TableName(value ="area_info")
@Data
public class AreaInfo {

	/**
	 * 主键ID
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;


	/**
	 * 省份
	 */
	@TableField(value = "province")
	private String province;


	/**
	 * 市区
	 */
	@TableField(value = "city")
	private String city;

	/**
	 * 是否删除：0否 1是
	 */
	@TableField(value = "deleted")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建时间
	 */

	@TableField(value = "create_time", fill = FieldFill.INSERT)
	private Date createTime;
}
