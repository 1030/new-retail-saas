package com.dy.yunying.api.feign;

import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Map;

@FeignClient(contextId = "remoteChannelManageService", value = ServiceNameConstants.UPMS_YUNYING_SERVICE)
public interface RemoteChannelManageService {

	@PostMapping("/select/queryPlatForm")
	R<List<Map<String, String>>> queryPlatForm();

}
