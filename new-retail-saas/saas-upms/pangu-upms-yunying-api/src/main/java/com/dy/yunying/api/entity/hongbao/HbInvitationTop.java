package com.dy.yunying.api.entity.hongbao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 手动榜单表
 * @author  chengang
 * @version  2021-10-27 19:21:20
 * table: hb_invitation_top
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "hb_invitation_top")
public class HbInvitationTop extends Model<HbInvitationTop>{

	//columns START
		//主键id
		@TableId(value = "id",type = IdType.AUTO)
		private Long id;


		//活动ID
		@TableField(value = "activity_id")
		private Long activityId;


		//昵称
		@TableField(value = "nick_name")
		private String nickName;


		//金额
		@TableField(value = "setting_money")
		private BigDecimal settingMoney;


		//是否删除  0否 1是
		@TableField(value = "deleted")
		private Integer deleted;


		//创建时间
		@TableField(value = "create_time")
		private Date createTime;


		//修改时间
		@TableField(value = "update_time")
		private Date updateTime;


		//创建人
		@TableField(value = "create_id")
		private Long createId;


		//修改人
		@TableField(value = "update_id")
		private Long updateId;


	//columns END 数据库字段结束
	

	
}

	

	
	

