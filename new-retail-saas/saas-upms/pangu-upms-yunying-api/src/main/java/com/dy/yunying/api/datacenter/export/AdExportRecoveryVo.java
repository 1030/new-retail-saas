package com.dy.yunying.api.datacenter.export;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/6/22 11:07
 * @description：
 * @modified By：
 */
@Data
public class AdExportRecoveryVo  implements Serializable {

	private static final long serialVersionUID = 8701826235401117248L;

	//日期
	@ExcelProperty("天")
	@ApiModelProperty("天")
	private String day;

	//周
	@ExcelProperty("周")
	@ApiModelProperty("周")
	private String week;

	//月
	@ExcelProperty("月")
	@ApiModelProperty("月")
	private String month;

	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	// 周期
	private String period;

	@ExcelProperty("系统")
	@ApiModelProperty("系统")
	private String osStr;


	@ExcelProperty("父游戏id")
	@ApiModelProperty("父游戏id")
	private Long pgid;

	// 父游戏名称
	@ExcelProperty("主游戏")
	@ApiModelProperty("主游戏")
	private String parentGameName;

	@ExcelProperty("子游戏id")
	@ApiModelProperty("子游戏id")
	private Long gameid;

	// 子游戏名称
	@ExcelProperty("子游戏名称")
	@ApiModelProperty("子游戏名称")
	private String gname;

	//部门id
	@ExcelProperty("部门id")
	@ApiModelProperty("部门id")
	private String deptId;

	//部门id
	@ExcelProperty("部门")
	@ApiModelProperty("部门")
	private String deptName;

	//组别
	@ExcelProperty("组别")
	@ApiModelProperty("组别")
	private String userGroupId;

	@ExcelProperty("组别")
	@ApiModelProperty("组别")
	private String userGroupName;

	//主渠道名称
	@ExcelProperty("渠道名称")
	@ApiModelProperty("渠道名称")
	private String parentchlName;

	//分包渠道名称
	@ExcelProperty("分包渠道名称")
	@ApiModelProperty("分包渠道名称")
	private String appchlName;

	//主渠道
	@ExcelProperty("主渠道")
	@ApiModelProperty("主渠道")
	private String parentchl;

	//分包渠道
	@ExcelProperty("分包编码")
	@ApiModelProperty("分包编码")
	private String appchl;

	//系统类型
	@ExcelProperty("系统类型")
	@ApiModelProperty("系统类型")
	private Integer os;

	//投放人 id
	@ExcelProperty("投放人")
	@ApiModelProperty("投放人")
	private String investor;

	//投放人名称
	@ExcelProperty("投放人")
	@ApiModelProperty("投放人")
	private String investorName;


	//系统类型名称
	@ExcelProperty("系统类型名称")
	@ApiModelProperty("系统类型名称")
	private String osName;

	//广告计划
	@ExcelProperty("广告计划")
	@ApiModelProperty("广告计划")
	private String adid;

	/**
	 * 广告计划名称
	 */
	@ExcelProperty("广告计划名称")
	@ApiModelProperty("广告计划名称")
	private String adidName;

	/**
	 * 广告账户ID
	 */
	@ExcelProperty("广告账户ID")
	@ApiModelProperty("广告账户ID")
	private String advertid;

	/**
	 * 广告计账户名称
	 */
	@ExcelProperty("广告计账户名称")
	@ApiModelProperty("广告计账户名称")
	private String adAccountName;

	/**
	 * 转化类型
	 */
	@ExcelProperty("转化类型")
	@ApiModelProperty("转化类型")
	private String converType;

	/**
	 * 转化类型名称
	 */
	@ExcelProperty("转化类型名称")
	@ApiModelProperty("转化类型名称")
	private String converTypeName;

	//消耗
	@ExcelProperty("消耗")
	@ApiModelProperty("消耗")
	private BigDecimal rudeCost;

	//返点后消耗
	@ExcelProperty("返点后消耗")
	@ApiModelProperty("返点后消耗")
	private BigDecimal cost;

	//新增设备注册数
	@ExcelProperty("新增设备注册数")
	@ApiModelProperty("新增设备注册数")
	private Integer usrnamenums;

	//新增设备成本
	@ExcelProperty("新增设备成本")
	@ApiModelProperty("新增设备成本")
	private BigDecimal deviceCose;

	//累计充值ROI
	@ExcelProperty("累计充值ROI")
	@ApiModelProperty("累计充值ROI")
	private String roiRatio;

	//总LTV
	@ExcelProperty("总LTV")
	@ApiModelProperty("总LTV")
	private BigDecimal ltvRatio;

	private BigDecimal roiRatioNum;

	private BigDecimal ltvRatioNum;


	@ExcelProperty("ltv1")
	@ApiModelProperty("ltv1")
	public String ltv1;

	@ExcelProperty("ltv2")
	@ApiModelProperty("ltv2")
	public String ltv2;

	@ExcelProperty("ltv3")
	@ApiModelProperty("ltv3")
	public String ltv3;

	@ExcelProperty("ltv4")
	@ApiModelProperty("ltv4")
	public String ltv4;

	@ExcelProperty("ltv5")
	@ApiModelProperty("ltv5")
	public String ltv5;

	@ExcelProperty("ltv6")
	@ApiModelProperty("ltv6")
	public String ltv6;

	@ExcelProperty("ltv7")
	@ApiModelProperty("ltv7")
	public String ltv7;

	@ExcelProperty("ltv8")
	@ApiModelProperty("ltv8")
	public String ltv8;

	@ExcelProperty("ltv9")
	@ApiModelProperty("ltv9")
	public String ltv9;

	@ExcelProperty("ltv10")
	@ApiModelProperty("ltv10")
	public String ltv10;

	@ExcelProperty("ltv11")
	@ApiModelProperty("ltv11")
	public String ltv11;

	@ExcelProperty("ltv12")
	@ApiModelProperty("ltv12")
	public String ltv12;

	@ExcelProperty("ltv13")
	@ApiModelProperty("ltv13")
	public String ltv13;

	@ExcelProperty("ltv14")
	@ApiModelProperty("ltv14")
	public String ltv14;

	@ExcelProperty("ltv15")
	@ApiModelProperty("ltv15")
	public String ltv15;

	@ExcelProperty("ltv16")
	@ApiModelProperty("ltv16")
	public String ltv16;

	@ExcelProperty("ltv17")
	@ApiModelProperty("ltv17")
	public String ltv17;

	@ExcelProperty("ltv18")
	@ApiModelProperty("ltv18")
	public String ltv18;

	@ExcelProperty("ltv19")
	@ApiModelProperty("ltv19")
	public String ltv19;

	@ExcelProperty("ltv20")
	@ApiModelProperty("ltv20")
	public String ltv20;

	@ExcelProperty("ltv21")
	@ApiModelProperty("ltv21")
	public String ltv21;

	@ExcelProperty("ltv22")
	@ApiModelProperty("ltv22")
	public String ltv22;

	@ExcelProperty("ltv23")
	@ApiModelProperty("ltv23")
	public String ltv23;

	@ExcelProperty("ltv24")
	@ApiModelProperty("ltv24")
	public String ltv24;

	@ExcelProperty("ltv25")
	@ApiModelProperty("ltv25")
	public String ltv25;

	@ExcelProperty("ltv26")
	@ApiModelProperty("ltv26")
	public String ltv26;

	@ExcelProperty("ltv27")
	@ApiModelProperty("ltv27")
	public String ltv27;

	@ExcelProperty("ltv28")
	@ApiModelProperty("ltv28")
	public String ltv28;

	@ExcelProperty("ltv29")
	@ApiModelProperty("ltv29")
	public String ltv29;

	@ExcelProperty("ltv30")
	@ApiModelProperty("ltv30")
	public String ltv30;

	@ExcelProperty("ltv45")
	@ApiModelProperty("ltv45")
	public String ltv45;

	@ExcelProperty("ltv60")
	@ApiModelProperty("ltv60")
	public String ltv60;

	@ExcelProperty("ltv75")
	@ApiModelProperty("ltv75")
	public String ltv75;

	@ExcelProperty("ltv90")
	@ApiModelProperty("ltv90")
	public String ltv90;

	@ExcelProperty("ltv120")
	@ApiModelProperty("ltv120")
	public String ltv120;

	@ExcelProperty("ltv150")
	@ApiModelProperty("ltv150")
	public String ltv150;

	@ExcelProperty("ltv180")
	@ApiModelProperty("ltv180")
	public String ltv180;




	@ExcelProperty("roi1")
	@ApiModelProperty("roi1")
	public String roi1;

	@ExcelProperty("roi2")
	@ApiModelProperty("roi2")
	public String roi2;

	@ExcelProperty("roi3")
	@ApiModelProperty("roi3")
	public String roi3;

	@ExcelProperty("roi4")
	@ApiModelProperty("roi4")
	public String roi4;

	@ExcelProperty("roi5")
	@ApiModelProperty("roi5")
	public String roi5;

	@ExcelProperty("roi6")
	@ApiModelProperty("roi6")
	public String roi6;

	@ExcelProperty("roi7")
	@ApiModelProperty("roi7")
	public String roi7;

	@ExcelProperty("roi8")
	@ApiModelProperty("roi8")
	public String roi8;

	@ExcelProperty("roi9")
	@ApiModelProperty("roi9")
	public String roi9;

	@ExcelProperty("roi10")
	@ApiModelProperty("roi10")
	public String roi10;

	@ExcelProperty("roi11")
	@ApiModelProperty("roi11")
	public String roi11;

	@ExcelProperty("roi12")
	@ApiModelProperty("roi12")
	public String roi12;

	@ExcelProperty("roi13")
	@ApiModelProperty("roi13")
	public String roi13;

	@ExcelProperty("roi14")
	@ApiModelProperty("roi14")
	public String roi14;

	@ExcelProperty("roi15")
	@ApiModelProperty("roi15")
	public String roi15;

	@ExcelProperty("roi16")
	@ApiModelProperty("roi16")
	public String roi16;

	@ExcelProperty("roi17")
	@ApiModelProperty("roi17")
	public String roi17;

	@ExcelProperty("roi18")
	@ApiModelProperty("roi18")
	public String roi18;

	@ExcelProperty("roi19")
	@ApiModelProperty("roi19")
	public String roi19;

	@ExcelProperty("roi20")
	@ApiModelProperty("roi20")
	public String roi20;

	@ExcelProperty("roi21")
	@ApiModelProperty("roi21")
	public String roi21;

	@ExcelProperty("roi22")
	@ApiModelProperty("roi22")
	public String roi22;

	@ExcelProperty("roi23")
	@ApiModelProperty("roi23")
	public String roi23;

	@ExcelProperty("roi24")
	@ApiModelProperty("roi24")
	public String roi24;

	@ExcelProperty("roi25")
	@ApiModelProperty("roi25")
	public String roi25;

	@ExcelProperty("roi26")
	@ApiModelProperty("roi26")
	public String roi26;

	@ExcelProperty("roi27")
	@ApiModelProperty("roi27")
	public String roi27;

	@ExcelProperty("roi28")
	@ApiModelProperty("roi28")
	public String roi28;

	@ExcelProperty("roi29")
	@ApiModelProperty("roi29")
	public String roi29;

	@ExcelProperty("roi30")
	@ApiModelProperty("roi30")
	public String roi30;

	@ExcelProperty("roi45")
	@ApiModelProperty("roi45")
	public String roi45;

	@ExcelProperty("roi60")
	@ApiModelProperty("roi60")
	public String roi60;

	@ExcelProperty("roi75")
	@ApiModelProperty("roi75")
	public String roi75;

	@ExcelProperty("roi90")
	@ApiModelProperty("roi90")
	public String roi90;

	@ExcelProperty("roi120")
	@ApiModelProperty("roi120")
	public String roi120;

	@ExcelProperty("roi150")
	@ApiModelProperty("roi150")
	public String roi150;

	@ExcelProperty("roi180")
	@ApiModelProperty("roi180")
	public String roi180;



	@ExcelProperty("倍数1")
	@ApiModelProperty("倍数1")
	public String ltvtimes1;

	@ExcelProperty("倍数2")
	@ApiModelProperty("倍数2")
	public String ltvtimes2;

	@ExcelProperty("倍数3")
	@ApiModelProperty("倍数3")
	public String ltvtimes3;

	@ExcelProperty("倍数4")
	@ApiModelProperty("倍数4")
	public String ltvtimes4;

	@ExcelProperty("倍数5")
	@ApiModelProperty("倍数5")
	public String ltvtimes5;

	@ExcelProperty("倍数6")
	@ApiModelProperty("倍数6")
	public String ltvtimes6;

	@ExcelProperty("倍数7")
	@ApiModelProperty("倍数7")
	public String ltvtimes7;

	@ExcelProperty("倍数8")
	@ApiModelProperty("倍数8")
	public String ltvtimes8;

	@ExcelProperty("倍数9")
	@ApiModelProperty("倍数9")
	public String ltvtimes9;

	@ExcelProperty("倍数10")
	@ApiModelProperty("倍数10")
	public String ltvtimes10;

	@ExcelProperty("倍数11")
	@ApiModelProperty("倍数11")
	public String ltvtimes11;

	@ExcelProperty("倍数12")
	@ApiModelProperty("倍数12")
	public String ltvtimes12;

	@ExcelProperty("倍数13")
	@ApiModelProperty("倍数13")
	public String ltvtimes13;

	@ExcelProperty("倍数14")
	@ApiModelProperty("倍数14")
	public String ltvtimes14;

	@ExcelProperty("倍数15")
	@ApiModelProperty("倍数15")
	public String ltvtimes15;

	@ExcelProperty("倍数16")
	@ApiModelProperty("倍数16")
	public String ltvtimes16;

	@ExcelProperty("倍数17")
	@ApiModelProperty("倍数17")
	public String ltvtimes17;

	@ExcelProperty("倍数18")
	@ApiModelProperty("倍数18")
	public String ltvtimes18;

	@ExcelProperty("倍数19")
	@ApiModelProperty("倍数19")
	public String ltvtimes19;

	@ExcelProperty("倍数20")
	@ApiModelProperty("倍数20")
	public String ltvtimes20;

	@ExcelProperty("倍数21")
	@ApiModelProperty("倍数21")
	public String ltvtimes21;

	@ExcelProperty("倍数22")
	@ApiModelProperty("倍数22")
	public String ltvtimes22;

	@ExcelProperty("倍数23")
	@ApiModelProperty("倍数23")
	public String ltvtimes23;

	@ExcelProperty("倍数24")
	@ApiModelProperty("倍数24")
	public String ltvtimes24;

	@ExcelProperty("倍数25")
	@ApiModelProperty("倍数25")
	public String ltvtimes25;

	@ExcelProperty("倍数26")
	@ApiModelProperty("倍数26")
	public String ltvtimes26;

	@ExcelProperty("倍数27")
	@ApiModelProperty("倍数27")
	public String ltvtimes27;

	@ExcelProperty("倍数28")
	@ApiModelProperty("倍数28")
	public String ltvtimes28;

	@ExcelProperty("倍数29")
	@ApiModelProperty("倍数29")
	public String ltvtimes29;

	@ExcelProperty("倍数30")
	@ApiModelProperty("倍数30")
	public String ltvtimes30;

	@ExcelProperty("倍数45")
	@ApiModelProperty("倍数45")
	public String ltvtimes45;

	@ExcelProperty("倍数60")
	@ApiModelProperty("倍数60")
	public String ltvtimes60;

	@ExcelProperty("倍数75")
	@ApiModelProperty("倍数75")
	public String ltvtimes75;

	@ExcelProperty("倍数90")
	@ApiModelProperty("倍数90")
	public String ltvtimes90;

	@ExcelProperty("倍数120")
	@ApiModelProperty("倍数120")
	public String ltvtimes120;

	@ExcelProperty("倍数150")
	@ApiModelProperty("倍数150")
	public String ltvtimes150;

	@ExcelProperty("倍数180")
	@ApiModelProperty("倍数180")
	public String ltvtimes180;


	public BigDecimal ltv1Num;
	public BigDecimal ltv2Num;
	public BigDecimal ltv3Num;
	public BigDecimal ltv4Num;
	public BigDecimal ltv5Num;
	public BigDecimal ltv6Num;
	public BigDecimal ltv7Num;
	public BigDecimal ltv8Num;
	public BigDecimal ltv9Num;
	public BigDecimal ltv10Num;
	public BigDecimal ltv11Num;
	public BigDecimal ltv12Num;
	public BigDecimal ltv13Num;
	public BigDecimal ltv14Num;
	public BigDecimal ltv15Num;
	public BigDecimal ltv16Num;
	public BigDecimal ltv17Num;
	public BigDecimal ltv18Num;
	public BigDecimal ltv19Num;
	public BigDecimal ltv20Num;
	public BigDecimal ltv21Num;
	public BigDecimal ltv22Num;
	public BigDecimal ltv23Num;
	public BigDecimal ltv24Num;
	public BigDecimal ltv25Num;
	public BigDecimal ltv26Num;
	public BigDecimal ltv27Num;
	public BigDecimal ltv28Num;
	public BigDecimal ltv29Num;
	public BigDecimal ltv30Num;
	public BigDecimal ltv45Num;
	public BigDecimal ltv60Num;
	public BigDecimal ltv75Num;
	public BigDecimal ltv90Num;
	public BigDecimal ltv120Num;
	public BigDecimal ltv150Num;
	public BigDecimal ltv180Num;

	public BigDecimal roi1Num;
	public BigDecimal roi2Num;
	public BigDecimal roi3Num;
	public BigDecimal roi4Num;
	public BigDecimal roi5Num;
	public BigDecimal roi6Num;
	public BigDecimal roi7Num;
	public BigDecimal roi8Num;
	public BigDecimal roi9Num;
	public BigDecimal roi10Num;
	public BigDecimal roi11Num;
	public BigDecimal roi12Num;
	public BigDecimal roi13Num;
	public BigDecimal roi14Num;
	public BigDecimal roi15Num;
	public BigDecimal roi16Num;
	public BigDecimal roi17Num;
	public BigDecimal roi18Num;
	public BigDecimal roi19Num;
	public BigDecimal roi20Num;
	public BigDecimal roi21Num;
	public BigDecimal roi22Num;
	public BigDecimal roi23Num;
	public BigDecimal roi24Num;
	public BigDecimal roi25Num;
	public BigDecimal roi26Num;
	public BigDecimal roi27Num;
	public BigDecimal roi28Num;
	public BigDecimal roi29Num;
	public BigDecimal roi30Num;
	public BigDecimal roi45Num;
	public BigDecimal roi60Num;
	public BigDecimal roi75Num;
	public BigDecimal roi90Num;
	public BigDecimal roi120Num;
	public BigDecimal roi150Num;
	public BigDecimal roi180Num;


	public BigDecimal ltvtimes1Num;
	public BigDecimal ltvtimes2Num;
	public BigDecimal ltvtimes3Num;
	public BigDecimal ltvtimes4Num;
	public BigDecimal ltvtimes5Num;
	public BigDecimal ltvtimes6Num;
	public BigDecimal ltvtimes7Num;
	public BigDecimal ltvtimes8Num;
	public BigDecimal ltvtimes9Num;
	public BigDecimal ltvtimes10Num;
	public BigDecimal ltvtimes11Num;
	public BigDecimal ltvtimes12Num;
	public BigDecimal ltvtimes13Num;
	public BigDecimal ltvtimes14Num;
	public BigDecimal ltvtimes15Num;
	public BigDecimal ltvtimes16Num;
	public BigDecimal ltvtimes17Num;
	public BigDecimal ltvtimes18Num;
	public BigDecimal ltvtimes19Num;
	public BigDecimal ltvtimes20Num;
	public BigDecimal ltvtimes21Num;
	public BigDecimal ltvtimes22Num;
	public BigDecimal ltvtimes23Num;
	public BigDecimal ltvtimes24Num;
	public BigDecimal ltvtimes25Num;
	public BigDecimal ltvtimes26Num;
	public BigDecimal ltvtimes27Num;
	public BigDecimal ltvtimes28Num;
	public BigDecimal ltvtimes29Num;
	public BigDecimal ltvtimes30Num;
	public BigDecimal ltvtimes45Num;
	public BigDecimal ltvtimes60Num;
	public BigDecimal ltvtimes75Num;
	public BigDecimal ltvtimes90Num;
	public BigDecimal ltvtimes120Num;
	public BigDecimal ltvtimes150Num;
	public BigDecimal ltvtimes180Num;


	public String getOsStr() {
		String osStr = OsEnum.getName(os);
		return osStr;
	}

	public String getPeriod() {
		if (StringUtils.isNotBlank(period)) {
			if (period.contains("周")) {
				String periodYear = period.substring(0, 4);
				String periodWeek = period.substring(4, 7);
				return periodYear + "-" + periodWeek;
			}
		}
		return period;
	}
}
