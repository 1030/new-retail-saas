package com.dy.yunying.api.vo;

import com.dy.yunying.api.entity.WanChannelPackRule;
import com.dy.yunying.api.enums.ExtendPackageEnum;
import com.dy.yunying.api.enums.PackTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * wan_channel_pack
 *
 * @author hjl
 * @date 2022-07-19 13:50:03
 */
@Getter
@Setter
public class WanChannelPackPageVO {

	/**
	 * 分包渠道ID
	 */
	private Long packId;

	/**
	 * 游戏ID
	 */
	private Long gameId;

	/**
	 * 母包类型：1-运营母包；2-云微端；
	 */
	private Integer packType;

	/**
	 * 游戏名称
	 */
	private String gameName;

	/**
	 * 游戏包名
	 */
	private String pkName;

	/**
	 *
	 */
	private Long versionId;

	/**
	 * 分包编码（游戏编码）
	 */
	private String code;

	/**
	 * 分包编码 名称
	 */
	private String codeName;

	/**
	 * 所属渠道编码（子渠道）
	 */
	private String chlCode;

	/**
	 * 所属渠道编码名称（子渠道）
	 */
	private String chlName;
	/**
	 * 子渠道负责人id
	 */
	private Long manage;

	/**
	 * 子渠道负责人
	 */
	private String leadperson;

	private String parentCode;

	/**
	 * 所属渠道编码名称（主渠道）
	 */
	private String parentName;

	/**
	 * 游戏包路径
	 */
	private String path;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 状态：1：生成中，2：已生成；3：失效
	 */
	private Integer status;

	/**
	 * 头条状态
	 */
	private String ttStatus;

	/**
	 * 推广类型（1信息流 2短视频）
	 */
	private Integer spreadType;

	/**
	 * 结算类型(KOL(一口价) KOL(CPM按次))
	 */
	private String settleType;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建人名称
	 */
	private String creatorName;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

	private String detectionUrl;

	/**
	 * 展示监测链接
	 */
	private String displayMonitorUrl;

	private String app_file_url_root;

	private String url;

	private String platform;

	private List<WanChannelPackRule> ruleList;

	private Integer backhaulType;


	/**
	 * 星图监测链接
	 */
	private String xtMonitorUrl;

	public String getAdUrl() {
		if (StringUtils.isNotBlank(path)) {
			return "/pack" + path;
		}
		return "";
	}

	public String getStatusStr() {
		String statusStr = null;
		switch (status) {
			case 1:
				statusStr = "生成中";
				break;
			case 2:
				statusStr = "已生成";
				break;
			case 3:
				statusStr = "失效";
				break;
			default:
				statusStr = "";
		}
		return statusStr;
	}

	public String getPackTypeText(){
		if (Objects.nonNull(packType)){
			return PackTypeEnum.getName(packType);
		}
		return "-";
	}
}