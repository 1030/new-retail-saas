package com.dy.yunying.api.req.gametask;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GameTaskSelectReq extends Page{

	@ApiModelProperty(value = "主游戏id")
	private Long parentGameId;


	@ApiModelProperty(value = "任务描述")
	private String taskDescription;
}
