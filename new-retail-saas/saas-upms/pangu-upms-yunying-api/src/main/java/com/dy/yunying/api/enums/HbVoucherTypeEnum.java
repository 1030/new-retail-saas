package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName HbActivityTypeEnum.java
 * @createTime 2021年10月26日 10:35:00
 */
public enum HbVoucherTypeEnum {
	PERMANENT("1", "永久有效"),
	FIXED("2", "固定时间"),
	APPOINT("3", "指定天数");

	private String type;
	private String name;
	public String getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private HbVoucherTypeEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(String type) {
		if (StringUtils.isBlank(type)){
			return null;
		}
		for (HbVoucherTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return null;
	}
}
