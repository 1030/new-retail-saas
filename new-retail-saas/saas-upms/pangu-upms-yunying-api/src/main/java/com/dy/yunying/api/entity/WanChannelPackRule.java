package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 分包回传规则表(wan_channel_pack_rule)实体类
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("wan_channel_pack_rule")
public class WanChannelPackRule extends Model<WanChannelPackRule> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id",type = IdType.AUTO)
	private Long id;
    /**
     * 分包编码
     */
    private String packCode;
    /**
     * 事件类型（1：初始化，2：注册，3：付费）
     */
    private Integer eventType;
    /**
     * 时间范围 0：任意时间范围 1：大于N天
     */
    private Integer timeScope;
    /**
     * N天 time_scope为1时必填
     */
    private Integer timeValue;
    /**
     * 0：不回传 1：限制回传频次（event_type=3付费时有）
     */
    private Integer backhaulRule;
    /**
     * 回传传频：每N笔订单回传1次，backhaul_rule为1时必填
     */
    private Integer backhaulFrequency;
    /**
     * 回传类型（0：全量回传 1：限制回传）
     */
    private Integer backhaulType;
    /**
     * 是否删除  0否 1是
     */
    private Integer deleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(update = "now()")
	private Date updateTime;
    /**
     * 创建人
     */
    private Long createId;
    /**
     * 修改人
     */
    private Long updateId;

}