package com.dy.yunying.api.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 余额类型
 */
public enum CurrencyChangeTypeEnum {

    ADD(1,"INCOME","增加"),
    REDUCE(2,"EXPEND","消耗"),
    ;

    private Integer val;
    private String desc;
    private String name;

    private static final Map<Integer, String> map = new HashMap<>();

    static {
        CurrencyChangeTypeEnum[] payArray = values();
        for (CurrencyChangeTypeEnum typeEnum : payArray) {
            map.put(typeEnum.getVal(), typeEnum.getDesc());
        }

    }

    CurrencyChangeTypeEnum(Integer val, String desc,String name) {
        this.val = val;
        this.desc = desc;
        this.name = name;
    }

    public static CurrencyChangeTypeEnum getOneByVal(Integer val){
        CurrencyChangeTypeEnum[] payArray = values();
        for (CurrencyChangeTypeEnum typeEnum : payArray) {
            if (typeEnum.getVal().equals(val)) {
                return typeEnum;
            }
        }
        return null;
    }
    
    public static String getNameByDesc(String desc){
    	CurrencyChangeTypeEnum[] payArray = values();
    	for (CurrencyChangeTypeEnum typeEnum : payArray) {
            if (typeEnum.getDesc().equals(desc)) {
                return typeEnum.getName();
            }
        }
    	return null;
    }

    public static String getDescByVal(Integer val) {
        return map.get(val);
    }

    public Integer getVal() {
        return val;
    }

    public String getDesc() {
        return desc;
    }

	public String getName() {
		return name;
	}
}
