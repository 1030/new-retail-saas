package com.dy.yunying.api.resp.hongbao;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 父级节点
 * @date 2021/10/26 18:11
 */
@Data
public class NodeParentData implements Serializable {

	private String nodeId;

	private String nodeName;

	private String pId;
}
