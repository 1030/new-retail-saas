package com.dy.yunying.api.entity.hongbao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 三方支付信息设置表
 * @author  chengang
 * @version  2023-02-28 14:11:55
 * table: third_pay_setting
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "third_pay_setting")
public class ThirdPaySetting extends Model<ThirdPaySetting>{

		//主键id
		@TableId(value = "id",type = IdType.AUTO)
		private Long id;


		//支付宝:ZFB 微信:WX
		@TableField(value = "p_type")
		private String pType;


		//应用ID
		@TableField(value = "app_id")
		private String appId;


		//权限范围
		@TableField(value = "scope")
		private String scope;


		//第三方公钥
		@TableField(value = "third_public_key")
		private String thirdPublicKey;


		//应用私钥
		@TableField(value = "app_private_key")
		private String appPrivateKey;


		//签名类型 公钥:SECRET 证书:CERT
		@TableField(value = "sign_type")
		private String signType;


		//环境 沙箱:SANDBOX 生产:PROD
		@TableField(value = "env_type")
		private String envType;


		//应用公钥证书地址
		@TableField(value = "app_public_cert")
		private String appPublicCert;


		//支付平台公钥证书地址
		@TableField(value = "third_public_cert")
		private String thirdPublicCert;


		//支付平台根证书地址
		@TableField(value = "third_root_cert")
		private String thirdRootCert;


		//支付平台授权地址
		@TableField(value = "third_grant_url")
		private String thirdGrantUrl;

		//支付平台网关地址
		@TableField(value = "third_gateway_url")
		private String thirdGatewayUrl;


		//回调地址
		@TableField(value = "redirect_uri")
		private String redirectUri;


		//token授权类型
		@TableField(value = "token_grant_type")
		private String tokenGrantType;


		//AES加密秘钥
		@TableField(value = "aes_encrypt_key")
		private String aesEncryptKey;


		//业务备注
		@TableField(value = "remark")
		private String remark;


		//是否删除  0否 1是
		@TableField(value = "deleted")
		private Integer deleted;


		//创建时间
		@TableField(value = "create_time")
		private Date createTime;


		//修改时间
		@TableField(value = "update_time")
		private Date updateTime;


		//创建人
		@TableField(value = "create_id")
		private Long createId;


		//修改人
		@TableField(value = "update_id")
		private Long updateId;
	
}

	

	
	

