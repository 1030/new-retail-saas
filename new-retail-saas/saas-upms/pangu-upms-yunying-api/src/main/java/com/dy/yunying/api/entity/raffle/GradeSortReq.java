package com.dy.yunying.api.entity.raffle;

import lombok.Data;

@Data
public class GradeSortReq {

	private String prizeGrade;

	/**
	 * 活动ID
	 */
	private Long activityId;
}
