package com.dy.yunying.api.req.yyz;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment
 */
@Data
public class SendTemplateCodeReq implements Serializable {

	/***
	 *   主键id
	 */
	private String id;

	/***
	 *   短信编码
	 */
	private String templateCode;

	/**
	 * 短信编码内容
	 */
	private String template;
}
