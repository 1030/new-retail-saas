package com.dy.yunying.api.datacenter.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: hjl
 * @Date: 2020/8/10 20:43
 */
@Data
public class ParentGamePackDto implements Serializable {

	private static final long serialVersionUID = -7418876743494860495L;

	/**
	 * 父游戏母包版本ID
	 */
	private Long versionId;

	/**
	 * 父游戏ID
	 */
	private Long gameId;

	/**
	 * APP包名
	 */
	private String pkName;

	/**
	 * 版本编码，数字，如56
	 */
	private Integer versionCode;

	/**
	 * 版本名称，字符串，如：v3.2.5
	 */
	private String versionName;

	/**
	 * 文件路径
	 */
	private String path;

}