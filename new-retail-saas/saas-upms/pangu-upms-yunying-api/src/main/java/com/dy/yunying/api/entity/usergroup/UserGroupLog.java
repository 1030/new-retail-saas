package com.dy.yunying.api.entity.usergroup;


import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * 用户群组操作明细
 * @author zhuxm
 * @date 2022-04-29 12:35:28
 */
@ApiModel(value = "用户群组操作明细")
@TableName(value ="user_group_log")
@Data
public class UserGroupLog {

	/**
	 * 主键ID
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;


	/**
	 * 群组ID
	 */
	@TableField(value="group_id")
	private Long groupId;


	/**
	 * 类型：1.新增，2.修改 3.删除
	 */
	@TableField(value = "type")
	private Integer type;


	/**
	 * 记录内容
	 */
	@TableField(value = "content")
	private String content;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time", fill = FieldFill.INSERT)
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time", fill = FieldFill.UPDATE)
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}
