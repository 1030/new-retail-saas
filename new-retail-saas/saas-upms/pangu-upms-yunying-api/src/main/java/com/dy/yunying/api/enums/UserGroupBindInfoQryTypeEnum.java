package com.dy.yunying.api.enums;

/**
 * hezz
 * */
public enum UserGroupBindInfoQryTypeEnum {

	// "查询类型 1-公告和奖励 2-公告 3-奖励 4-公告或奖励"
	NOTICE_AND_PRIZE(1, "公告和奖励"),
	NOTICE(2, "公告"),

	// update_mode  更新方式：1自动更新，2不更新
	PRIZE(3,"奖励"),
	NOTICE_OR_PRIZE(4,"公告或奖励"),

	;

	private Integer type;
	private String name;
	public Integer getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private UserGroupBindInfoQryTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static UserGroupBindInfoQryTypeEnum getOneByType(Integer type){
		for(UserGroupBindInfoQryTypeEnum dimensionEnum: UserGroupBindInfoQryTypeEnum.values()){
			if(dimensionEnum.getType() == type){
				return dimensionEnum;
			}
		}
		return null;
	}
}
