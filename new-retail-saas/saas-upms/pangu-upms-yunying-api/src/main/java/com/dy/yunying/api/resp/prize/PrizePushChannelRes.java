package com.dy.yunying.api.resp.prize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 奖励与渠道关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:48
 * table: prize_push_channel
 */
@Data
public class PrizePushChannelRes implements Serializable {

	@ApiModelProperty(value = "主键ID")
	private Long id;

	@ApiModelProperty(value = "奖励推送ID")
	private Long prizePushId;

	@ApiModelProperty(value = "主渠道编码")
	private String parentChl;

	@ApiModelProperty(value = "子渠道编码")
	private String chl;

	@ApiModelProperty(value = "分包渠道编码")
	private String appChl;

	@ApiModelProperty(value = "渠道层级(1主渠道 2子渠道 3分包渠道)")
	private Integer channelRange;
}


