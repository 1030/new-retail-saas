package com.dy.yunying.api.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TopTitleVo {
	private String title;
}
