package com.dy.yunying.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 广告管理
 * advertiser_monitor_info
 * @author kongyanfang
 * @date 2020-10-23 11:49:17
 */
@Getter
@Setter
public class AdvertiserMonitorInfoDO extends BaseEntity {
    /**
     * 广告id
     */
    private Long adid;
    /**
     * 广告账户id
     */
    private String advertiserId;

    /**
     * 广告名称
     */
    private String adname;

    /**
     * 端口
     */
    private Integer os;

    /**
     * 渠道编码
     */
    private String chl;

    /**
     * 子渠道编码
     */
    private String childChl;

    /**
     * 分包渠道编码
     */
    private String appChl;

    /**
     * 父游戏ID
     */
    private Long gid;

    /**
     * 子游戏ID
     */
    private Long childGid;

    /**
     * 广告投放人
     */
    private Integer advertiser;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date updatetime;

    /**
     * 下载链接
     */
    private String downloadUrl;

	/**
	 * 点击监测链接
	 */
	private String monitorUrl;

	/**
	 * 展示监测链接
	 */
	private String displayMonitorUrl;

	/**
	 * 星图监测链接
	 */
	private String xtMonitorUrl;

    /**
     * 代理商
     */
    private Integer agentid;

    /**
     * 推广域名
     */
    private String promotionDomain;

    /**
     * 打包方式，1：自动分包；2：选择已有分包
     */
    private Integer packType;

    /**
     * 推广域名
     */
    private Long packId;

    /**
     * 是否批量复制 0否，1是
     */
    private Integer iscopy;

    /**
     * 拷贝之前的名称
     */
    private String hisName;

    /**
     * 开始打包时间，用于判断打包超时
     */
    private Date packtime;

    /**
     * 0 未打包    1正在打包   2打包成功  3打包失败   4打包超时
     */
    private Integer packState;

    /**
     * 是否删除 0否，1是
     */
    private Integer isdelete;

    /**
     * 广告域名
     */
    private String domainAddr;
    /**
     * 转换目标
     */
    private String convertType;
    /**
     * 深度转换目标
     */
    private String deepExternalAction;
    /**
     * 转化同步状态
     */
    private String convertStatus;
    /**
     * 转化统计方式：ONLY_ONE（仅一次），EVERY_ONE（每一次）
     */
    private String convertDataType;
	/**
	 * 平台ID：1，头条。8，广点通
	 */
	private String platformId;
	/**
	 * 转化场景
	 */
	private String conversionScene;
	/**
	 * 归因方式
	 */
	private String claimType;
	/**
	 * 母包类型：1-运营母包；2-云微端；
	 */
	private Integer appType;
}