package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;

/**
 * WanGamePo Po
 *
 * @author system
 */

public class WanGameReq extends Page implements Serializable {
	/**
	 * @author cx
	 * @date 2020年7月24日12:20:25
	 */
	private static final long serialVersionUID = 7980528629348181946L;

	/**
	 * 子游戏主键集合
	 */
	private Collection<Long> ids;

	/**
	 * 游戏ID
	 */
	private BigInteger id;

	//英文名称
	private String enname;

	/**
	 * 游戏名称
	 */
	private String gname;


	/**
	 * 父游戏id
	 */
	private BigInteger pgid;

	private Integer status;

	private Integer ishot;

	/**
	 * 是否推荐  0:否  1:是
	 */
	private Integer isrecommend;


	/**
	 * 包名
	 */
	private String pk_name;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Collection<Long> getIds() {
		return ids;
	}

	public WanGameReq setIds(Collection<Long> ids) {
		this.ids = ids;
		return this;
	}

	public String getGname() {
		return gname;
	}

	public void setGname(String gname) {
		this.gname = gname;
	}

	public String getPk_name() {
		return pk_name;
	}

	public void setPk_name(String pk_name) {
		this.pk_name = pk_name;
	}

	public BigInteger getPgid() {
		return pgid;
	}

	public void setPgid(BigInteger pgid) {
		this.pgid = pgid;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIshot() {
		return ishot;
	}

	public void setIshot(Integer ishot) {
		this.ishot = ishot;
	}

	public Integer getIsrecommend() {
		return isrecommend;
	}

	public void setIsrecommend(Integer isrecommend) {
		this.isrecommend = isrecommend;
	}

	public String getEnname() {
		return enname;
	}

	public void setEnname(String enname) {
		this.enname = enname;
	}
}