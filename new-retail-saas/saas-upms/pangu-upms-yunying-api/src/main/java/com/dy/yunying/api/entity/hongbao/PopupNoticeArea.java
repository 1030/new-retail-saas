package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ：lile
 * @date ：2021/10/27 11:24
 * @description：
 * @modified By：
 */
@Data
@TableName("popup_notice_area")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "公告区服关系表")
public class PopupNoticeArea extends HbBaseEntity {

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "公告区服关系ID")
	private Long id;


	@ApiModelProperty(value = "公告ID")
	private Long noticeId;

	@ApiModelProperty(value = "主游戏ID")
	private Long pgameId;


	@ApiModelProperty(value = "区服ID")
	private Long areaId;

	@ApiModelProperty(value = "区服层级(1主游戏 2区服)")
	private int areaRange;
}
