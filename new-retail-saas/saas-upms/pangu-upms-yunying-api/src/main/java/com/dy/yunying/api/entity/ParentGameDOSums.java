package com.dy.yunying.api.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ParentGameDOSums {

	private Long pgid;

	/**
	 * 子游戏数量
	 */
	private Long gameSum = 0L;

	/**
	 * 玩家人数
	 */
	private Long registerSum = 0L;

	/**
	 * 充值金额
	 */
	private BigDecimal rechargeSum = BigDecimal.ZERO;
}
