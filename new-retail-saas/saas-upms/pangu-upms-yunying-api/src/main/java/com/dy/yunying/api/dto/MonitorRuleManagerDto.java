package com.dy.yunying.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @ClassName AdmonitorRuleDto
 * @Description todo
 * @Author yangyh
 * @Time 2021/6/22 14:27
 * @Version 1.0
 **/
@Setter
@Getter
@ApiModel("监控规则查询对象")
public class MonitorRuleManagerDto  {


	@ApiModelProperty(value = "主键id")
	private Integer id;


	/**规则名称*/
	@NotBlank(message = "规则名称不能为空")
	@ApiModelProperty(value = "规则名称")
	private String monitorName;
	/**监控范围*/
	@NotNull(message = "监控范围不能为空")
	@ApiModelProperty(value = "监控范围:主渠道")
	private Integer monitorChannelType;
	/**广告账户*/
	@NotBlank(message = "广告账户不能为空")
	@ApiModelProperty(value = "广告账户")
	private String adAccount;
	/**监控游戏ID,多个用逗号分隔，0为不限*/
	@ApiModelProperty(value = "监控游戏ID,多个用逗号分隔，0为不限")
	private String monitorGameId;
	/**监控项：1广告计划  2 广告账号*/
	@NotNull(message = "监控项不能为空")
	@ApiModelProperty(value = "监控项：1广告计划  2 广告账号")
	private Integer monitorItem;
	/**监控指标:   1.账号余额 2：0注册的消耗  3 注册成本 4 新增付费成本 5 首日roi 6:7日roi 7:15日roi */

	@NotNull(message = "监控指标不能为空")
	@ApiModelProperty(value = "监控指标:   1.账号余额 2：0注册的消耗  3 注册成本 4 新增付费成本 5 首日roi 6:7日roi 7:15日roi ")
	private Integer monitorTarget;
	/**比较运算符：=，!=,>,< ,>=,<= */
	@NotBlank(message = "比较运算符不能为空")
	@ApiModelProperty(value = "比较运算符：=，!=,>,< ,>=,<= ")
	private String compare;
	/**比较值*/

	@NotBlank(message = "比较值不能为空")
	@ApiModelProperty(value = "比较值")
	private String  compareValue;
	/**通知类型：飞书消息*/
	@ApiModelProperty(value = "通知类型：飞书消息")
	private Integer notifyType;
	/**通知人员*/
	@NotBlank(message = "通知人员不能为空")
	@ApiModelProperty(value = "通知人员")
	private String notifytor;
	/**广告计划*/
	@ApiModelProperty(value = "广告计划")
	private String exceptPlan;
	/**监控状态：0关闭，1开启*/
	@ApiModelProperty(value = "监控状态：0关闭，1开启")
	private Integer monitorStatus;
}
