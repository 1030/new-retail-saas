package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pig4cloud.pig.common.core.jackson.BigDecimalSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 游豆充值配置表
 *
 * @TableName currency_recharge_config
 */
@Data
@Accessors(chain = true)
@TableName(value = "currency_recharge_config")
public class CurrencyRechargeConfigDO extends Model<CurrencyRechargeConfigDO> implements Serializable {

	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 游豆名称
	 */
	private String currencyName;

	/**
	 * 游豆价值
	 */
	private BigDecimal currencyAmount;

	/**
	 * 充值金额
	 */
	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal rechargeAmount;

	/**
	 * 使用范围：1-平台通用；2-父游戏；3-子游戏；4-角色；
	 */
	private Integer useScope;

	/**
	 * 使用主游戏
	 */
	private Long usePgameId;

	/**
	 * 使用子游戏
	 */
	private Long useGameId;

	/**
	 * 使用区服
	 */
	private String useAreaId;

	/**
	 * 使用角色
	 */
	private String useRoleId;

	/**
	 * 是否存在角标：0-不存在；1-存在；
	 */
	private Integer marked;

	/**
	 * 角标内容
	 */
	private String markValue;

	/**
	 * 是否删除(0否 1是)
	 */
	private Integer deleted;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 修改时间
	 */
	private Date updateTime;

	/**
	 * 创建人
	 */
	private Long createId;

	/**
	 * 修改人
	 */
	private Long updateId;

}