package com.dy.yunying.api.req.prize;

import io.swagger.annotations.ApiModelProperty;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

import java.util.Date;

/**
 * 奖品礼包码
 *
 * @author chenxiang
 * @version 2022-04-26 10:21:44 table: prize_gift_bag
 */
@Data
public class PrizeGiftBagReq extends Page {

	@ApiModelProperty(value = "主键ID")
	private String id;

	@ApiModelProperty(value = "类型：1礼包，2自定义")
	private String sourceType;

	@ApiModelProperty(value = "来源关联ID。如：奖品ID")
	private String sourceId;

	@ApiModelProperty(value = "礼包码")
	private String giftCode;

	@ApiModelProperty(value = "礼包码类型：1：唯一码，2：通用码")
	private String usable;

	@ApiModelProperty(value = "状态：0可用，1已领取")
	private String status;

	@ApiModelProperty(value = "领取时间")
	private String receiveTime;

	@ApiModelProperty(value = "礼包码总数量")
	private String giftAmount;

	@ApiModelProperty(value = "已领取数量")
	private String giftGetcounts;

	@ApiModelProperty(value = "是否删除：0否 1是")
	private String deleted;

}
