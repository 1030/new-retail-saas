package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 时间类型：1固定时间，2领取后指定天数
 *
 * @author sunyouquan
 * @date 2022/5/5 14:36
 */
public enum TimeTypeEnum {

	FIXTIME(1, "固定时间"), ASSIGNTIME(2, "指定时间");

	private Integer type;

	private String name;

	TimeTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonValue
	public Integer getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}

	@JsonCreator
	public static TimeTypeEnum getTimeTypeEnum(Integer value) {
		for (TimeTypeEnum timeTypeEnum : values()) {
			if (timeTypeEnum.type.equals(value)) {
				return timeTypeEnum;
			}
		}
		return null;
	}

}
