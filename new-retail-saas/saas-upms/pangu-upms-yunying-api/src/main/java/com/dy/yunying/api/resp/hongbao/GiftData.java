package com.dy.yunying.api.resp.hongbao;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description: 礼包码数据
 * @author yuwenfeng
 * @date 2022/3/17 15:03
 */
@Data
public class GiftData implements Serializable {

	private Long giftId;

	private String giftCodes;

}
