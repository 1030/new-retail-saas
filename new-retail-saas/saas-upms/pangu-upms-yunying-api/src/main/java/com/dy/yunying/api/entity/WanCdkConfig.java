package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 代金券导出数据配置表
 * @author  chenxiang
 * @version  2022-04-18 16:23:05
 * table: cdk_grant_config
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "wan_cdk_config")
public class WanCdkConfig extends Model<WanCdkConfig>{
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 配置名称
	 */
	@TableField(value = "title")
	private String title;
	/**
	 * 指定天数类型：1当天，2指定天
	 */
	@TableField(value = "time_type")
	private Integer timeType;
	/**
	 * 开始日期
	 */
	@TableField(value = "start_time")
	private Date startTime;
	/**
	 * 结束日期
	 */
	@TableField(value = "end_time")
	private Date endTime;
	/**
	 * 最小等级
	 */
	@TableField(value = "min_level")
	private Integer minLevel;
	/**
	 * 最大等级
	 */
	@TableField(value = "max_level")
	private Integer maxLevel;
	/**
	 * 最小充值金额
	 */
	@TableField(value = "min_money")
	private BigDecimal minMoney;
	/**
	 * 最大充值金额
	 */
	@TableField(value = "max_money")
	private BigDecimal maxMoney;
	/**
	 * 父游戏ID(0为所有游戏)
	 */
	@TableField(value = "parent_game_id")
	private Integer parentGameId;
	/**
	 * 子游戏ID(0为所有游戏)
	 */
	@TableField(value = "game_id")
	private Integer gameId;
	/**
	 * 排序
	 */
	@TableField(value = "order_by")
	private String orderBy;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

