package com.dy.yunying.api.enums;

/**
 * 废弃：老版本监控
 */
public enum MonitorTargetEnum {

	COST(1,"成本"),
	EXPEND(2,"消耗"),
	ROI(3,"ROI"),
	NEW_ARPU(4,"新增ARPU"),
	NEW_PAY_RATE(5,"新增付费率"),
	;
	MonitorTargetEnum(Integer type, String desc){
		this.type =type;
		this.desc =desc;
	}

	/**
	 * 类型
	 */
	private final Integer type;

	/**
	 * 描述
	 */
	private final String desc;
}
