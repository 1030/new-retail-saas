package com.dy.yunying.api.dto.hongbao;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 手动榜单表
 * @author  chengang
 * @version  2021-10-27 19:21:20
 * table: hb_invitation_top
 */
@Data
public class HbInvitationTopDto implements Serializable {
	
	//columns START
		//主键id
		private Long id;
		//活动ID
		private Long activityId;
		//昵称
		private String nickName;
		//金额
		private String settingMoney;
		//是否删除  0否 1是
		private Integer deleted;
		//创建时间
		private Date createTime;
		//修改时间
		private Date updateTime;
		//创建人
		private Long createId;
		//修改人
		private Long updateId;
	//columns END 数据库字段结束
	
	
}


