package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yuwenfeng
 * @description: 生成代金券
 * @date 2021/11/16 11:08
 */
@Data
public class GenerateCdkReq implements Serializable {

	@NotBlank(message = "CDK名称不能为空")
	@Length(min = 1, max = 50, message = "CDK名称字符长度不合法")
	@ApiModelProperty(value = "CDK名称")
	private String cdkName;

	@ApiModelProperty(value = "金额")
	@NotNull(message = "金额不能为空")
	@Max(value = 999999, message = "金额必须小于等于999999")
	@Min(value = 1, message = "金额必须大于等于1")
	private BigDecimal money;

	@ApiModelProperty(value = "数量")
	@NotNull(message = "数量不能为空")
	@Max(value = 9999999, message = "数量必须小于等于9999999")
	@Min(value = 1, message = "数量必须大于等于1")
	private Integer num;

	@ApiModelProperty(value = "限制金额")
	@NotNull(message = "限制金额不能为空")
	@Max(value = 999999, message = "限制金额必须小于等于999999")
	@Min(value = 1, message = "限制金额必须大于等于1")
	private BigDecimal limitMoney;

	@ApiModelProperty(value = "父游戏ID")
	private Long pgid;

	@ApiModelProperty(value = "子游戏id")
	private Long gameid;

	@ApiModelProperty(value = "有效期类型 1:长期有效 2:指定有效期")
	@NotNull(message = "有效期类型不能为空")
	private Integer expiryType;

	@ApiModelProperty(value = "限制类型(1时间段 2指定天数)")
	private Integer validType;

	@ApiModelProperty(value = "有效开始时间(格式：yyyy-MM-dd HH:mm:ss)")
	private String startTime;

	@ApiModelProperty(value = "有效结束时间(格式：yyyy-MM-dd HH:mm:ss)")
	private String endTime;

	@ApiModelProperty(value = "有效天数")
	private Integer validDays;

	@ApiModelProperty(value="备注")
	private String remark;
}
