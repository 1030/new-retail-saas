package com.dy.yunying.api.req.gametask;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * 导入游戏配置类
 */
@Data
public class ImportGameTaskReq {

	@ApiModelProperty(value = "主游戏id")
	@NotNull(message = "主游戏id不能为空")
	private Long parentGameId;

	@ApiModelProperty(value = "主游戏名称")
	@NotNull(message = "主游戏名称不能为空")
	private String parentGameName;

	private MultipartFile gameTaskExcelFile;
}
