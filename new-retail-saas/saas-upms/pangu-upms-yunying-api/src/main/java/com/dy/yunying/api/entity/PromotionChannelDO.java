package com.dy.yunying.api.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * wan_promotion_channel_v3
 *
 * @author hjl
 * @date 2020-09-01 14:24:35
 */
@Getter
@Setter
public class PromotionChannelDO {
	/**
	 * 主键
	 */
	private Integer id;

	/**
	 * 主广渠道父级id（pid=0：第一级，用来分组管理推广渠道）
	 */
	private Integer pid;

	/**
	 * 推广渠道编码（pid!=0：第二级）
	 */
	private String chncode;

	/**
	 * 推广渠道名称（pid!=0：第二级）
	 */
	private String chnname;

	/**
	 * 创建时间
	 */
	private Date createtime;

	/**
	 * 修改时间
	 */
	private Date updatetime;

	/**
	 * 创建者id
	 */
	private Long creator;

	/**
	 * 修改者id
	 */
	private Long editor;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 是否删除0、否，1、是
	 */
	private Integer isdelete;

	/**
	 * 推广游戏数
	 */
	private Integer gamenum;

	/**
	 * 推广渠道父级编码（pid=0：第一级，用来分组管理推广渠道）
	 */
	private String parentCode;

	/**
	 * 子渠道数
	 */
	private Integer chlnum;

	/**
	 * 游戏包数量
	 */
	private Integer appchlnum;

	/**
	 * 渠道计划数
	 */
	private Integer plannum;

	/**
	 * 渠道创意数
	 */
	private Integer ideanum;

	/**
	 * 登陆是否检测 0不检测1检测，默认0
	 */
	private Integer isLoginChk;

	/**
	 * 时长是否检测 0不检测 1检测，默认0
	 */
	private Integer isTimeChk;

	/**
	 * 支付是否检测 0不检测 1检测，默认0
	 */
	private Integer isPayChk;

	/**
	 * 未成年人支付限制是否检测 0不检测 1检测，默认0
	 */
	private Integer isPayLimitChk;

	/**
	 * 代理
	 */
	private String agent;

	/**
	 * 返点默认0
	 */
	private Integer rebatePoint;

	/**
	 * 头条账号
	 */
	private String toutiaoAccount;

	/**
	 * 1.api   2.sdk 上报方式
	 */
	private Integer rtype;

	/**
	 * 负责人或者投放人
	 */
	private Long manage;

	/**
	 * 平台，0,3367平台，1头条，2百度,3 新数，4.公会
	 */
	private Integer platform;

	/**
	 * 是否上报0不上报1上报
	 */
	private Integer isup;

	/**
	 * 0 sdk支付，1苹果内置支付
	 */
	private Integer paytype;

	/**
	 * 默认生成（0 非默认生成，1默认生成）
	 */
	private Integer isDefault;
}