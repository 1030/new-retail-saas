package com.dy.yunying.api.entity.currency;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 账号临时平台币信息
 * @TableName user_currency_tmp
 *
 * @Author: hjl
 * @Date: 2022-3-25 10:38:09
 */
@ApiModel(value = "账号平台币明细")
@TableName(value ="user_currency_tmp")
@Data
public class UserCurrencyTmp implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "userid")
    private Long userid;

    /**
     * 用户账号
     */
    @TableField(value = "username")
    private String username;

    /**
     * 平台币使用范围。1：平台通用；2：父游戏；3：子游戏；4：角色
     */
    @TableField(value = "scope")
    private String scope;

    /**
     * 父游戏ID，0：通用
     */
    @TableField(value = "pgid")
    private Long pgid;

    /**
     * 子游戏ID，0：通用
     */
    @TableField(value = "game_id")
    private Long gameId;

    /**
     * 区服ID
     */
    @TableField(value = "area_id")
    private String areaId;

    /**
     * 角色ID
     */
    @TableField(value = "role_id")
    private String roleId;

    /**
     * 总数
     */
    @TableField(value = "total")
    private BigDecimal total;

    /**
     * 余额
     */
    @TableField(value = "balance")
    private BigDecimal balance;

    /**
     * 开始时间
     */
    @TableField(value = "begin_time")
    private Date beginTime;

    /**
     * 结束时间
     */
    @TableField(value = "end_time")
    private Date endTime;

    /**
     * 状态，1：正常
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 删除标识，1：删除；0：未删除
     */
    @TableField(value = "deleted")
    private Integer deleted;

    /**
     * 创建人
     */
    @TableField(value = "create_id")
    private Long createId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "update_id")
    private Long updateId;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}