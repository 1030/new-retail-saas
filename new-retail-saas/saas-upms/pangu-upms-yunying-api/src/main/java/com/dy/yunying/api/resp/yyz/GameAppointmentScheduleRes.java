package com.dy.yunying.api.resp.yyz;

import com.baomidou.mybatisplus.annotation.TableField;
import com.dy.yunying.api.entity.GameAppointmentScheduleGears;
import com.dy.yunying.api.entity.GameAppointmentScheduleGearsPage;
import lombok.Data;

import java.util.Date;
import java.util.List;


/**
 * 游戏预约档位库
 * @date 2021-06-19 16:12:13
 */
@Data
public class GameAppointmentScheduleRes {
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 *
     */
    private String type;

	private String gameName;

	private Integer allTotal;

	private List<GameAppointmentScheduleGearsPage>  gameAppointmentSchedule;

	private List<GameAppointmentScheduleOnlineRes> onLine;

}
