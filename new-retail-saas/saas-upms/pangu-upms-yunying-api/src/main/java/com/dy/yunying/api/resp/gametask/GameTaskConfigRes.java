package com.dy.yunying.api.resp.gametask;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;

@Data
public class GameTaskConfigRes implements Serializable {
	/**
	 * 主键id
	 */
	private Long id;

	/**
	 * 主游戏id
	 */
	private Long parentGameId;

	/**
	 * 主游戏名称
	 */
	private String parentGameName;

	/**
	 * 游戏app_id
	 */
	private String appId;

	/**
	 * 任务id
	 */
	private String taskId;

	/**
	 * 任务描述
	 */
	private String taskDescription;

	/**
	 * 是否删除  0否 1是
	 */
	private Integer deleted;
}
