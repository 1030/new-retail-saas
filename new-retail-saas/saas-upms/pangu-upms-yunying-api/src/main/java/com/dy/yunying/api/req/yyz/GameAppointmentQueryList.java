package com.dy.yunying.api.req.yyz;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * 角色查询
 *
 * @author leishaowei
 * @version 2022-03-17 11:06:03
 * table: dwd_gm_player_query
 */
@Data
public class GameAppointmentQueryList {

	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 父游戏ID
	 */
	private Long pgid;
	/**
	 * 预约手机号
	 */
	private String mobile;
	/**
	 * 预约设备（1：IOS  2：安卓）
	 */
	private Integer appointDevice;
	/**
	 * 预约设备名称（1：IOS  2：安卓）
	 */
	private String appointDeviceName;
	/**
	 * 预约来源（1：PC  2：H5）
	 */
	private Integer appointSource;
	/**
	 * 预约来源名称（1：PC  2：H5）
	 */
	private String appointSourceName;
	/**
	 * 预约时间
	 */
	private String appointTime;
	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	private Integer type;
	/**
	 * 游戏名称
	 */
	private String gameName;

}
