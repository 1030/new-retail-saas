package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 广点通推广目标表
 * @author  chenxiang
 * @version  2022-10-13 13:33:44
 * table: gdt_app
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "gdt_app")
public class GdtApp extends Model<GdtApp>{
	/**
	 * 主键id
	 */
	@TableId(value = "id",type = IdType.AUTO)
	private Long id;
	/**
	 * 广告主帐号 id
	 */
	@TableField(value = "account_id")
	private Long accountId;
	/**
	 * 推广目标id
	 */
	@TableField(value = "promoted_object_id")
	private String promotedObjectId;
	/**
	 * 推广目标名称
	 */
	@TableField(value = "promoted_object_name")
	private String promotedObjectName;
	/**
	 * 推广目标类型
	 */
	@TableField(value = "promoted_object_type")
	private String promotedObjectType;
	/**
	 * 推广目标详细信息
	 */
	@TableField(value = "promoted_object_spec")
	private String promotedObjectSpec;
	/**
	 * 创建时间（时间戳）
	 */
	@TableField(value = "created_time")
	private Long createdTime;
	/**
	 * 最后修改时间（时间戳）
	 */
	@TableField(value = "last_modified_time")
	private Long lastModifiedTime;
	/**
	 * 来源类型(1:API,2:渠道后台)
	 */
	@TableField(value = "source_type")
	private Integer sourceType;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

