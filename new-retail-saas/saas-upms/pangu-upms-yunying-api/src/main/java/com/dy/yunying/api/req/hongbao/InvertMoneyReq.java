package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yuwenfeng
 * @description: 活动ID
 * @date 2021/10/23 11:04
 */
@Data
public class InvertMoneyReq implements Serializable {

	@ApiModelProperty(value = "活动ID")
	@NotNull(message = "活动ID不能为空")
	private Long activityId;

	@ApiModelProperty(value = "注资类型(1现金红包 3实物礼品 4代金券)")
	@NotNull(message = "注资类型不能为空")
	private Integer investType;

	@ApiModelProperty(value = "注资金额(元)")
	@NotNull(message = "注资金额不能为空")
	@Max(value = 9999999, message = "注资金额必须小于等于9999999")
	@Min(value = 1, message = "注资金额必须大于等于1")
	private BigDecimal investMoney;

}
