/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 搜索模板表
 *
 * @author xuzilei
 * @date 2021-06-17 16:45:36
 */
@Data
@TableName("dm_search_tmplate")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "搜索模板表")
public class DmSearchTmplate extends Model<DmSearchTmplate> {
private static final long serialVersionUID = 1L;

    /**
     * 唯一标识
     */
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value="唯一标识")
    private Long id;
    /**
     * 模板名称
     */
    @ApiModelProperty(value="模板名称")
    private String name;
    /**
     * 模板元素json信息
     */
    @ApiModelProperty(value="模板元素json信息",example="\tinfo:{\n" +
			"\t\tfilter:{\n" +
			"\t\t\tcycle:[]\n" +
			"\t\t\ttypes:[]\n" +
			"\t\t\tfunction:[]\n" +
			"\t\t},\n" +
			"\t\tcustomColumn:[]\n" +
			"\t}")
    private String info;
    /**
     * 模板类型，0:广告数据分析表搜索模板
     */
    @ApiModelProperty(value="模板类型，0:广告数据分析表搜索模板")
    private Integer type;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private Date updateTime;
    /**
     * 创建用户
     */
    @ApiModelProperty(value="创建用户")
    private String createUser;

	public String getInfo() {
		if(StringUtils.isBlank(info)){
			return "{}";
		}
		return info;
	}
}
