package com.dy.yunying.api.entity.usergroup;


import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * 用户群组
 * @author zhuxm
 * @date 2022-04-25 15:54:28
 */
@ApiModel(value = "用户群组")
@TableName(value ="user_group")
@Data
public class UserGroup {

	/**
	 * 主键ID
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;


	/**
	 * 群组名称
	 */
	@TableField(value = "group_name")
	private String groupName;


	/**
	 * 群组归属：1.平台，2.用户，3.产品，4.渠道，10.其他
	 */
	@TableField(value = "group_type")
	private Integer groupType;


	/**
	 * 群组分类：1.全部游戏、2.部分游戏、3.指定游戏
	 */
	@TableField(value = "group_class")
	private Integer groupClass;


	/**
	 * 指定游戏id
	 */
	@TableField(value = "class_gameid")
	private Long classGameid;


	/**
	 * 筛选方式：1规则筛选，2ID筛选
	 */
	@TableField(value = "type")
	private Integer type;



	/**
	 * 筛选维度：1账号，2角色
	 */
	@TableField(value = "dimension")
	private Integer dimension;


	/**
	 * 主游戏ID，角色必填
	 */
	@TableField(value = "pgame_id")
	private Long pgameId;


	/**
	 * 区服，角色必填
	 */
	@TableField(value = "area_id")
	private Long areaId;


	/**
	 * 添加方式：1手动录入，2批量导入
	 */
	@TableField(value = "add_type")
	private Integer addType;



	/**
	 * 用户归属
	 */
	@TableField(value = "attribution")
	private String attribution;


	/**
	 * 用户行为
	 */
	@TableField(value = "behavior")
	private String behavior;

	/**
	 * 群组备注
	 */
	@TableField(value = "remark")
	private String remark;

	/**
	 * 更新方式：1自动更新，2不更新
	 */
	@TableField(value = "update_mode")
	private Integer updateMode;

	/**
	 * 更新标识：0-不需要更新，1-需要更新
	 */
	@TableField(value = "update_mark")
	private Integer updateMark;


	/**
	 * 群组人数
	 */
	@TableField(exist = false)
	private Long groupUserNum;


	/**
	 * 最后更新时间
	 */
	@TableField(value = "last_update_time")
	private Date lastUpdateTime;


	/**
	 * 最后完成更新时间
	 */
	@TableField(value = "refresh_time")
	private Date refreshTime;

	/**
	 * 群组人数
	 */
	@TableField(value = "user_nums")
	private Long userNums;

	/**
	 * 是否删除：0否 1是
	 */
	@TableField(value = "deleted")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time", fill = FieldFill.INSERT)
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time", fill = FieldFill.UPDATE)
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

	@TableField(exist = false)
	private String createName;

	/**
	 * 游戏名称
	 */
	@TableField(exist = false)
	private String gname;

	@TableField(exist = false)
	private Integer noticeNums;
	@TableField(exist = false)
	private Integer prizeNums;

	/**
	 * 分群条件
	 */
	@TableField(value = "conditions_detail")
	private String conditionsDetail;

	@TableField(value = "dx_app_id")
	private String dxAppId;

	/**
	 *  规则查询版本号
	 */
	@TableField(value = "data_version")
	private Long dataVersion;
}
