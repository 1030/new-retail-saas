package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 活动列表入参对象
 * @date 2021/10/23 11:04
 */
@Data
public class SelectActivityPageReq implements Serializable {

	@ApiModelProperty(value = "活动类型(1等级 2充值 3邀请)")
	@NotNull(message = "活动类型不能为空")
	private Integer activityType;

	@ApiModelProperty(value = "活动状态(1待上线 2活动中 3已下线)")
	private Integer activityStatus;

	@ApiModelProperty(value = "游戏推广名")
	private String gameName;

	@ApiModelProperty(value = "活动名称")
	private String activityName;

	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	private String finishTime;

	@ApiModelProperty(value = "排序字段")
	private String orderFile;

	@ApiModelProperty(value = "排序方式(ASC/DESC)")
	private String orderType;

	@ApiModelProperty(value = "活动动态类型ID")
	@NotNull(message = "活动动态类型ID不能为空")
	private Long dynamicTypeId;

	/**
	 * 起始条件: 0活跃角色 1创建角色
	 */
	@ApiModelProperty(value = "角色类型: 0活跃角色 1创建角色")
	private Integer roleType;

}
