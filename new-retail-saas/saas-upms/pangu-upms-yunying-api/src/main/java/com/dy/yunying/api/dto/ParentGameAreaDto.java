package com.dy.yunying.api.dto;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 主游戏区服关联表
 * @author  chengang
 * @version  2021-10-27 10:42:03
 * table: parent_game_area
 */
@Data
public class ParentGameAreaDto implements Serializable {
	
	//columns START
		//主键id
		private Long id;
		//父游戏ID
		private Long parentGameId;
		//区服ID
		private Long areaId;
		//区服名称
		private String areaName;
		//是否删除  0否 1是
		private Integer deleted;
		//创建时间
		private Date createTime;
		//修改时间
		private Date updateTime;
		//创建人
		private Long createId;
		//修改人
		private Long updateId;
	//columns END 数据库字段结束
	
	
}


