package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

/**
 * class info
 *
 * @author sunyouquan
 * @date 2022/5/19 20:05
 */
public enum PushIntervalMapWeekEnum {
	Monday("1","周一"),
	Tuesday("2","周二"),
	Wednesday("3","周三"),
	Thursday("4","周四"),
	Friday("5","周五"),
	Saturday("6","周六"),
	Sunday("7","周日");

	private String type;

	private String name;


	private PushIntervalMapWeekEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonCreator
	public static PushIntervalMapWeekEnum getPushIntervalMapWeekEnum(String value) {
		for (PushIntervalMapWeekEnum pushIntervalMapWeekEnum : values()) {
			if (pushIntervalMapWeekEnum.type.equals(value)) {
				return pushIntervalMapWeekEnum;
			}
		}
		return null;
	}

	@JsonValue
	public String getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}
}
