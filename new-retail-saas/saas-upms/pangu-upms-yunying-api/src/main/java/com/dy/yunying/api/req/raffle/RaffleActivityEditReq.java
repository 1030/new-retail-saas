package com.dy.yunying.api.req.raffle;

import com.baomidou.mybatisplus.annotation.TableField;
import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 抽奖活动配置表(raffle_activity)实体类
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class RaffleActivityEditReq extends Page {
    /**
     * 主键id
     */
	private Long id;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * kv图
     */
    private String kvpic;
    /**
     * 主游戏ID
     */
    private Long parentGameId;
    /**
     * 用户群组ID
     */
    private String userGroupId;
    /**
     * 活动开始时间
     */
    private String startTime;
    /**
     * 活动结束时间
     */
    private String finishTime;
    /**
     * 活动方式：1-九宫格；
     */
    private Integer activityMethod;
    /**
     * 是否开启兑换功能：0-否；1-是；
     */
    private Integer enableExchange;
    /**
     * 兑换类型：1-积分兑换；
     */
    private Integer exchangeType;
    /**
     * 中奖获得积分数量
     */
    private BigDecimal pointsQuota;
    /**
     * 积分展示名称
     */
    private String pointsShow;
    /**
     * 积分icon地址
     */
    private String pointsIconUrl;
    /**
     * 活动状态(1待上线 2活动中 3已下线)
     */
    private Integer activityStatus;
    /**
     * 活动规则
     */
    private String activityRule;
    /**
     * 是否删除：0-否；1-是；
     */
    private Integer deleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(update = "now()")
	private Date updateTime;
    /**
     * 创建人
     */
    private Long createId;
    /**
     * 修改人
     */
    private Long updateId;

	@ApiModelProperty(value = "区服范围集合")
	private List<Long> areaList;
	// 活动id集合逗号分割
	private String ids;

}