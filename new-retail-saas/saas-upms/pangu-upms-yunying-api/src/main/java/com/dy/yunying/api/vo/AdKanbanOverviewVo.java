package com.dy.yunying.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.constant.OsEnum;
import com.dy.yunying.api.constant.TerminalTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Objects;

@Setter
@Getter
@ApiModel("广告数据分析")
public class AdKanbanOverviewVo extends Page {

	@ApiModelProperty("父游戏id")
	private String pgid;
	// 父游戏
	@ApiModelProperty("主游戏")
	private String pgame;

	// 子游戏id
	@ApiModelProperty("子游戏id")
	private String gid;
	// 游戏
	@ApiModelProperty("子游戏")
	private String sgame;

	// 分成比例
	@ApiModelProperty("分成比例")
	private BigDecimal sharing;


	@ApiModelProperty("消耗")
	private BigDecimal cost;
	// 消耗
	@ApiModelProperty("返点后消耗（总成本）")
	private BigDecimal rudecost;

	//总roi 累计充值金额/总消耗
	@ApiModelProperty("累计充值ROI ")
	private BigDecimal roiratio;

	@ApiModelProperty("累计充值金额")
	private BigDecimal uuidsumfees;

	@ApiModelProperty("累计充值金额（分成后）")
	private BigDecimal dividefees;

	@ApiModelProperty("利润(分成后)")
	private BigDecimal profit;

	//新增设备数
	@ApiModelProperty("系统类型")
	private String os;
	//新增设备数
	@ApiModelProperty("系统名称")
	private String osName;

	@ApiModelProperty("部门主键")
	// 广告计划ID
	private String deptId;
	@ApiModelProperty("部门")
	private String deptName;

	public BigDecimal getRoiratio() {
		if (Objects.nonNull(uuidsumfees) && Objects.nonNull(rudecost) && 0 != rudecost.intValue()) {
			BigDecimal ret = uuidsumfees.divide(rudecost, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getSharing() {
		if (Objects.nonNull(sharing)) {
			BigDecimal ret = sharing.multiply(new BigDecimal(100));
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getProfit() {
		if (Objects.nonNull(dividefees) && Objects.nonNull(rudecost)) {
			BigDecimal ret = dividefees.subtract(rudecost);
			return ret;
		}
		return BigDecimal.ZERO;
	}

	public String getOsName(){
		if (StringUtils.isNotBlank(os)){
			return OsEnum.getName(Integer.parseInt(os));
		}
		return "-";
	}
	public String getPgame(){
		if (StringUtils.isBlank(pgame)){
			return "-";
		}
		return pgame;
	}
	public String getSgame(){
		if (StringUtils.isBlank(sgame)){
			return "-";
		}
		return sgame;
	}
	public String getDeptName(){
		if (StringUtils.isBlank(deptName)){
			return "-";
		}
		return deptName;
	}

}
