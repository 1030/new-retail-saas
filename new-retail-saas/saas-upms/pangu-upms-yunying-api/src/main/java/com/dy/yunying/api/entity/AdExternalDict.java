package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * 转化目标字典表
 * @author  chenxiang
 * @version  2022-07-04 13:58:21
 * table: ad_external_dict
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_external_dict")
public class AdExternalDict extends Model<AdExternalDict>{
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 平台ID：1头条，8广点通，10快手
	 */
	@TableField(value = "platform_id")
	private Integer platformId;
	/**
	 * 转化名称
	 */
	@TableField(value = "external_name")
	private String externalName;
	/**
	 * 转化目标
	 */
	@TableField(value = "external_action")
	private String externalAction;
	/**
	 * 深度转化名称
	 */
	@TableField(value = "deep_external_name")
	private String deepExternalName;
	/**
	 * 深度转化目标
	 */
	@TableField(value = "deep_external_action")
	private String deepExternalAction;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

