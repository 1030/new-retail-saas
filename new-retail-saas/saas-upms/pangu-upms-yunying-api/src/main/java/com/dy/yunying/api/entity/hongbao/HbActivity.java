/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 红包活动表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:18
 */
@Data
@TableName("hb_activity")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "红包活动表")
public class HbActivity extends HbBaseEntity {

	/**
	 * 活动ID
	 */
	@TableId(value = "id",type = IdType.AUTO)
	@ApiModelProperty(value = "活动ID")
	private Long id;

	/**
	 * 活动类型(1等级 2充值 3邀请)
	 */
	@ApiModelProperty(value = "活动类型(1等级 2充值 3邀请)")
	private Integer activityType;

	/**
	 * 活动动态类型ID
	 */
	@ApiModelProperty(value = "活动动态类型ID")
	private Long dynamicTypeId;

	/**
	 * 活动名称
	 */
	@ApiModelProperty(value = "活动名称")
	private String activityName;

	/**
	 * 时间类型(1静态时间 2动态时间)
	 */
	@ApiModelProperty(value = "时间类型(1静态时间 2动态时间)")
	private Integer timeType;

	/**
	 * 开始时间
	 */
	@ApiModelProperty(value = "开始时间")
	private Date startTime;

	/**
	 * 结束时间
	 */
	@ApiModelProperty(value = "结束时间")
	private Date finishTime;

	/**
	 * 有效天数
	 */
	@ApiModelProperty(value = "有效天数")
	private Integer validDays;

	/**
	 * 活动状态(1待上线 2活动中 3已下线)
	 */
	@ApiModelProperty(value = "活动状态(1待上线 2活动中 3已下线)")
	private Integer activityStatus;

	/**
	 * 活动规则
	 */
	@ApiModelProperty(value = "活动规则")
	private String activityRule;

	/**
	 * 注资总额(元)
	 */
	@ApiModelProperty(value = "注资总额(元)")
	private BigDecimal totalInvestMoney;

	/**
	 * 奖池价值(元)
	 */
	@ApiModelProperty(value = "奖池价值(元)")
	private BigDecimal totalCost;

	/**
	 * 消耗金额(元)
	 */
	@ApiModelProperty(value = "消耗金额(元)")
	private BigDecimal expendBalance;

	/**
	 * 提现规则
	 */
	@ApiModelProperty(value = "提现规则")
	private String cashRemarke;

	/**
	 * 起始条件: 0活跃角色 1创建角色
	 */
	@ApiModelProperty(value = "角色类型: 0活跃角色 1创建角色")
	private Integer roleType;
}
