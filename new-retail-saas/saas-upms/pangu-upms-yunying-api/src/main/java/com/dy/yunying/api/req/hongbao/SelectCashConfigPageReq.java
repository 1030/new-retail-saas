package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 活动列表入参对象
 * @date 2021/10/23 11:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SelectCashConfigPageReq extends ActivityIdReq implements Serializable {

	@ApiModelProperty(value = "提现类型(1游戏货币 2微信 3支付宝 4代金券)")
	@NotNull(message = "提现类型不能为空")
	private Integer cashType;
}
