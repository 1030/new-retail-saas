/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 活动公告表
 *
 * @author yuwenfeng
 * @date 2021-11-03 15:26:05
 */
@Data
@TableName("hb_activity_notice")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "活动公告表")
public class HbActivityNotice extends HbBaseEntity {

    /**
     * 活动公告ID
     */
	@TableId(value = "id",type = IdType.AUTO)
    @ApiModelProperty(value="活动公告ID")
    private Long id;

    /**
     * 活动ID
     */
    @ApiModelProperty(value="活动ID")
    private Long activityId;

    /**
     * 公告标题
     */
    @ApiModelProperty(value="公告标题")
    private String noticeTitle;

    /**
     * 开始时间
     */
    @ApiModelProperty(value="开始时间")
    private Date startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value="结束时间")
    private Date finishTime;

    /**
     * 跳转开关(1开启 0关闭)
     */
    @ApiModelProperty(value="跳转开关(1开启 0关闭)")
    private Integer jumpOff;

    /**
     * 公告详情
     */
    @ApiModelProperty(value="公告详情")
    private String noticeInfo;

    /**
     * 公告状态(1待上线 2进行中 3已下线)
     */
    @ApiModelProperty(value="公告状态(1待上线 2进行中 3已下线)")
    private Integer noticeStatus;

}
