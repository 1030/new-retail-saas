package com.dy.yunying.api.datacenter.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

/**
 * wan_game_version
 *
 * @author hjl
 * @date 2020-07-21 14:17:19
 */
@Getter
@Setter
public class GameVersionVO {

	private static final long serialVersionUID = -6514113348227828234L;

	/**
	 * 游戏版本ID
	 */
	private Long versionId;

	/**
	 * 游戏ID
	 */
	private Long gameId;

	/**
	 * 游戏名称
	 */
	private String gameName;

	/**
	 * 游戏包名
	 */
	private String pkName;

	/**
	 * 版本编码，数字，如56
	 */
	private Integer code;

	/**
	 * 版本名称，字符串，如：v3.2.5
	 */
	private String name;

	/**
	 * 图标
	 */
	private String icon;

	/**
	 * 版本更新内容
	 */
	private String content;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 文件大小
	 */
	private String size;

	/**
	 * 文件路径
	 */
	private String path;

	/**
	 * 状态：1：正常；2：失效
	 */
	private Integer status;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建人名称
	 */
	private String creatorName;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

	@Value("${pack.app_file_url_root}")
	private String app_file_url_root;

	/**
	 * 下载连接
	 */
	public String getUrl() {
		if (StringUtils.isNotBlank(path)) {
			return path;
		}
		return "";
	}

	public String getStatusStr() {
		String statusStr = "失效";
		switch (status) {
			case 1:
				statusStr = "正常";
				break;
		}
		return statusStr;
	}
}