package com.dy.yunying.api.entity;

import lombok.Data;

/**
 * @author sunyq
 * @date 2022/8/30 11:02
 */
@Data
public class ProhibitInfoDO {
	private String usrname;

	private Integer gameid;
}
