package com.dy.yunying.api.vo;

import com.dy.yunying.api.entity.AdvertiserMonitorInfoDO;
import com.dy.yunying.api.enums.GdtOptimizationGoalEnum;
import com.dy.yunying.api.enums.TtConvertTypeEnum;
import com.dy.yunying.api.enums.TtDeepExternalActionEnum;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Base64Utils;

/**
 * @Author: kyf
 * @Date: 2020/8/11 15:18
 */
@Setter
@Getter
public class AdvertiserMonitorVO extends AdvertiserMonitorInfoDO {

    /**
     * 父游戏名称
     */
    private String parentGameName;

    /**
     * 子游戏名称
     */
    private String childGameName;

    /**
     * 主渠道名称
     */
    private String parentChlName;

    /**
     * 子渠道名称
     */
    private String childChlName;

    /**
     * 投放人名称
     */
    private String advertiserName;
    /**
     * 广告账户名称
     */
    private String adAccountName;
	/**
	 * 转换目标名称
	 */
	private String convertTypeName;

	/**
	 * 推广类型（1信息流 2短视频）
	 */
	private Integer spreadType;

	/**
	 * 结算类型(KOL(一口价) KOL(CPM按次))
	 */
	private String settleType;

	@ApiModelProperty(value = "转化跟踪在 第三方平台中的ID (头条/UC 的转化id)")
	private String idAdPlatform;

    /**
     * 获取打包状态
     */
    public String getPackStateStr() {
        String packStateStr = "";
        Integer packState = super.getPackState();
        if (packState != null) {
            switch (packState) {
                case 1:
                    packStateStr = "<span style='#ADD8E6'>正在打包</span>";
                    break;
                case 2:
                    packStateStr = "<span style='color:green'>打包成功</span>";
                    break;
                default:
                    packStateStr = "<span style='color:red'>打包失败</span>";
                    break;
            }
        }
        return packStateStr;
    }

    /**
     * 落地页链接
     * todo:落地页地址未实现
     *
     * @return
     */
    public String getLandingPageUrl() {
        String landingPageUrl = "";
        String downloadUrl = getDownloadUrl();
        if (StringUtils.isNotBlank(downloadUrl)) {
            landingPageUrl = "http://apk.3399.com/landing_page/2_19_uc/" + "?downloadUrl=" + Base64Utils.encodeToUrlSafeString(downloadUrl.getBytes());
        }
        return landingPageUrl;
    }


    /* 待删除字段 */
    private String osName;
    private String chlName;
    private String advertiserRealName;
    private String packStateName;
    private String realname; // 投放人真是姓名
	/**
	 * 渠道包状态
	 */
	private String channelPackageStatus;

	public String getConvertTypeName(){
		String text = "";
		if (PlatformTypeEnum.TT.getValue().equals(getPlatformId())){
			if (StringUtils.isNotBlank(getConvertType())){
				text += TtConvertTypeEnum.valueByName(getConvertType());
				if (StringUtils.isNotBlank(getDeepExternalAction())){
					text += "-" + TtDeepExternalActionEnum.valueByName(getDeepExternalAction());
				}
			}
		}else if(PlatformTypeEnum.GDT.getValue().equals(getPlatformId())){
			if (StringUtils.isNotBlank(getConvertType())){
				text += GdtOptimizationGoalEnum.valueByType(getConvertType());
				if (StringUtils.isNotBlank(getDeepExternalAction())){
					text += "-" + GdtOptimizationGoalEnum.valueByType(getDeepExternalAction());
				}
			}
		}
		return text;
	}

    public String getNewDownloadUrl() {
        return "http://" + getDomainAddr() + getDownloadUrl();
    }

}
