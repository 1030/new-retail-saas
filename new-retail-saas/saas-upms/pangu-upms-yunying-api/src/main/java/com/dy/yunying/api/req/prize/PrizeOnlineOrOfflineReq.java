package com.dy.yunying.api.req.prize;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 上下线请求
 *
 * @author sunyouquan
 * @date 2022/4/29 15:05
 */
@Data
public class PrizeOnlineOrOfflineReq {

	@ApiModelProperty(value = "ID")
	@NotNull(message = "ID不能为空")
	private Long id;

	@ApiModelProperty(value = "状态(1启用，2停用)")
	@NotNull(message = "状态不能为空")
	private Integer status;

}
