/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 广告域名管理
 *
 * @author pigx code generator
 * @date 2021-06-04 15:53:15
 */
@Data
public class AdvertiserDomainReq extends Page {
    /**
     * 自增ID
     */
    private Integer domainId;
    /**
     * 域名地址
     */
    private String domainAddr;
    /**
     * 是否默认：0：否 1：是 
     */
    private Integer isDefault;
    /**
     * 状态：0：禁用 1：启用  2：删除
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;
}
