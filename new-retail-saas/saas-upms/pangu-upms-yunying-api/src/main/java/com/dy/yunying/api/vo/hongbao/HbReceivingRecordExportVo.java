package com.dy.yunying.api.vo.hongbao;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 奖品领取记录
 * @author  chenxiang
 * @version  2021-10-27 13:55:13
 * table: hb_receiving_record
 */
@Data
public class HbReceivingRecordExportVo implements Serializable {

	private static final long serialVersionUID = -1336951485867536066L;
	/**
	 * 活动ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活动ID")
	private Long activityId;


	/**
	 * 用户名
	 */
	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("用户账号")
	private String userName;


	/**
	 * 活动ID
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("活动名称")
	private String activityName;


	/**
	 * 父游戏名称
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("所属游戏")
	private String parentGameName;


	/**
	 * 子游戏名称
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("子游戏")
	private String gameName;



	/**
	 * 区服ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("区服")
	private Integer areaId;


	/**
	 * 角色名称
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("角色名称")
	private String roleName;



	/**
	 * 角色ID
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("角色ID")
	private String roleId;


	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("红包类型")
	private String typeName;


	/**
	 * 红包名称
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("红包名称")
	private String redpackName;


	/**
	 * 红包价值
	 */
	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("红包价值")
	private BigDecimal cashValues;



	/**
	 * 领取时间
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("领取时间")
	private String receiveTimeStr;




	/**
	 * 状态：1已到账，2待填写地址，3待审核，4待发货，5已发货，6已收货，7已驳回
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("状态")
	private String statusName;



	/**
	 * 接收人姓名
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("收货姓名")
	private String receiveUserName;
	/**
	 * 接收人电话
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("收货电话")
	private String receiveUserMobile;
	/**
	 * 接收人地址
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("收货地址")
	private String receiveUserAddress;


	/**
	 * 发货时间
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("发货时间")
	private String sendTimeStr;



	/**
	 * 快递公司
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("快递名称")
	private String expressName;
	/**
	 * 快递单号
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("快递订单号")
	private String expressCode;


	/**
	 * 修改昵称
	 */
	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("操作人")
	private String updateRealName;


	/**
	 * 修改时间
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("更新时间")
	private String updateTimeStr;


	/**
	 * 驳回原因
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("驳回原因")
	private String forbidReason;

}
