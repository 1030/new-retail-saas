package com.dy.yunying.api.enums;

public enum AddTypeEnum {

	HAND(1, "手动录入"),
	UPLOAD(2, "批量导入");

	private Integer type;
	private String name;
	public Integer getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private AddTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static AddTypeEnum getOneByType(Integer type){
		for(AddTypeEnum classEnum: AddTypeEnum.values()){
			if(classEnum.getType() == type){
				return classEnum;
			}
		}
		return null;
	}
}
