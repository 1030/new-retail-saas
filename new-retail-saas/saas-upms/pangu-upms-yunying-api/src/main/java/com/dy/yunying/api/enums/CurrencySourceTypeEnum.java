package com.dy.yunying.api.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 余额类型
 */
public enum CurrencySourceTypeEnum {

    // 增加
    /*CURRENCY_RECHARGE(101,"充值"),
    HB_ACTIVITY_REDPACK(111,"红包活动-红包"),
    HB_ACTIVITY_WITHDRAW(112,"红包活动-提现"),
    SIGN_ACTIVITY_SIGN(121,"签到活动-签到"),
    SIGN_ACTIVITY_TASK(122,"签到活动-任务"),
    GAME_ORDER_DEFROST(131,"订单抵扣解冻"),
	PRIZE_PUSH_GRANT(141,"奖励发放"),
	-- 奖励活动抽奖  RAFFLE_IN_DOING("142","抽奖活动-抽奖")
	-- 新增 MANUAL_GRANT(151,"人工发放")
	-- 新增 RAFFLE_IN_POINTS(152,"抽奖活动-兑换")

    GAME_ORDER(201,"订单抵扣"),
	CURRENCY_EXPIRE(211,"游豆过期"),
    ;*/
	
	ORDER_IN_RECHARGE(101,"ORDER_IN_RECHARGE","充值"),
	HONGBAO_IN_RECEIVE(111,"HONGBAO_IN_RECEIVE","红包活动-红包"),
	HONGBAO_IN_WITHDRAWAL(112,"HONGBAO_IN_WITHDRAWAL","红包活动-提现"),
	SIGN_IN_DOING(121,"SIGN_IN_DOING","签到活动-签到"),
	SIGN_IN_TASK(122,"SIGN_IN_TASK","签到活动-任务"),
	ORDER_IN_DEFROST(131,"ORDER_IN_DEFROST","订单退回"),
	PRIZE_PUSH(141,"PRIZE_PUSH","奖励推送"),
	
	RAFFLE_IN_DOING(142,"RAFFLE_IN_DOING","抽奖活动-抽奖"),
	MANUAL_GRANT(151,"MANUAL_GRANT","人工发放"),
	RAFFLE_IN_POINTS(152,"RAFFLE_IN_POINTS","抽奖活动-兑换"),
	
	ORDER_IN_DEDUCT(201,"ORDER_IN_DEDUCT","订单扣除"),
	EXPIRED(211,"EXPIRED","已过期"),
	
	;
	

	
    private Integer val;
    private String desc;
    private String name;

    private static final Map<Integer, String> map = new HashMap<>();

    static {
        CurrencySourceTypeEnum[] payArray = values();
        for (CurrencySourceTypeEnum typeEnum : payArray) {
            map.put(typeEnum.getVal(), typeEnum.getDesc());
        }

    }

    CurrencySourceTypeEnum(Integer val, String desc ,String name) {
        this.val = val;
        this.desc = desc;
        this.name = name;
    }

    public static CurrencySourceTypeEnum getOneByVal(Integer val){
        CurrencySourceTypeEnum[] payArray = values();
        for (CurrencySourceTypeEnum typeEnum : payArray) {
            if (typeEnum.getVal().equals(val)) {
                return typeEnum;
            }
        }
        return null;
    }
    
    public static String getNameByDesc(String desc){
    	CurrencySourceTypeEnum[] payArray = values();
    	for (CurrencySourceTypeEnum typeEnum : payArray) {
            if (typeEnum.getDesc().equals(desc)) {
                return typeEnum.getName();
            }
        }
    	return null;
    }

    public static String getDescByVal(Integer val) {
        return map.get(val);
    }
    
    public Integer getVal() {
        return val;
    }

    public String getDesc() {
        return desc;
    }

	public String getName() {
		return name;
	}
    
}
