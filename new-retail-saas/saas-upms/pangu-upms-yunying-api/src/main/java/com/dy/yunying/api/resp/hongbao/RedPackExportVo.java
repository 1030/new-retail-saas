package com.dy.yunying.api.resp.hongbao;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description
 * @Author chengang
 * @Date 2022/1/12
 */
@Data
public class RedPackExportVo implements Serializable {

	/**
	 * 礼包码ID
	 */
	private Long giftId;

	/**
	 * 红包ID
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("红包ID")
	private Long id;

	/**
	 * 红包名称
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("红包名称")
	private String name;

	/**
	 * 红包金额
	 */
	@ColumnWidth(15)
	@ContentStyle(dataFormat = 2)
	@HeadFontStyle
	@ExcelProperty("红包金额")
	private BigDecimal money;

	/**
	 * 礼包码类型
	 */
	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("礼包码类型")
	private String giftType;

	/**
	 * 礼包码
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("礼包码")
	private String giftCode;

	/**
	 * 剩余数量
	 */
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("剩余数量")
	private Long remainingNum;


}
