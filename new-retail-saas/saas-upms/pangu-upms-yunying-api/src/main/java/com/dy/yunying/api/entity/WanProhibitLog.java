package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName wan_prohibit_log
 */
@TableName(value ="wan_prohibit_log")
@Data
public class WanProhibitLog implements Serializable {
    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 类型：1：账号；2：IP；3：OAID；4：IMEI
     */
    @TableField(value = "type")
    private Short type;

    /**
     * 封禁内容
     */
    @TableField(value = "content")
    private String content;

    /**
     * 备注，封禁原因
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 封禁时长，单位：分钟。0：永久封禁
     */
    @TableField(value = "time")
    private Integer time;

    /**
     * 封禁状态，1：封禁；2：解封
     */
    @TableField(value = "status")
    private Short status;

    /**
     * 创建者
     */
    @TableField(value = "creator")
    private Long creator;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}