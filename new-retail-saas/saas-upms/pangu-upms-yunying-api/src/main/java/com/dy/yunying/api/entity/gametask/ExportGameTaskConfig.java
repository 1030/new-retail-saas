package com.dy.yunying.api.entity.gametask;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sunyq
 */
@Slf4j
@Data
public class ExportGameTaskConfig {
	/**
	 * 游戏app_id
	 */
	@ExcelProperty(index = 0,value = "app_id")
	private String appId;

	/**
	 * 任务id
	 */
	@ExcelProperty(index = 1,value = "任务Id")
	private String taskId;

	/**
	 * 任务描述
	 */
	@ExcelProperty(index = 2,value = "任务描述")
	private String taskDescription;
}
