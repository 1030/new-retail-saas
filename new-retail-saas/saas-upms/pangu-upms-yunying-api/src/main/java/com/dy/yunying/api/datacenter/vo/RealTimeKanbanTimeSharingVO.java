package com.dy.yunying.api.datacenter.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分时看板数据
 * @author sunyq
 * @date 2022/8/16 17:28
 */
@Data
public class RealTimeKanbanTimeSharingVO implements Serializable {

	private String  name;

	private String title;

	private List<RealTimeKanbanTimeSharing> data;
}
