package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 游戏预约进度档位表
 * @author  leisw
 * @version  2022-08-18 17:14:29
 * table: game_appointment_schedule_user_num
 */
@Data
@TableName(value = "game_appointment_schedule_user_num")
public class GameAppointmentScheduleUserNum{


	/**
	 * 预约类型
	 */
	@TableField(value = "appoint_day")
	private String appointDay;

	/**
	 * 实际预约人数
	 */
	@TableField(value = "actual_appoint_num")
	private Long actualAppointNum;

	/**
	 * 标签预约人数
	 */
	@TableField(value = "label_appoint_num")
	private Long labelAppointNum;

	/**
	 * 规则预约人数
	 */
	@TableField(value = "rule_appoint_num")
	private Long ruleAppointNum;

	/**
	 * 预约人数的
	 */
	@TableField(value = "appoint_all_num")
	private Long appointAllNum;

}






