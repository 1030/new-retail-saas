package com.dy.yunying.api.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class FutureMaterialVo {
	/**
	 * 注册成本
	 */
	private BigDecimal value;

	private String materialName;

	private String materialUrl;

	private String materialId;

	private String imageUrl;

	private String	format;

	private String	mid;

	private String advertiserId;

	private String tmId;


}
