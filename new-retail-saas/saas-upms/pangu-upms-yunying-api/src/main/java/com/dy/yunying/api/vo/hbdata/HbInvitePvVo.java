package com.dy.yunying.api.vo.hbdata;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/26 13:59
 * @description：
 * @modified By：
 */
@Data
public class HbInvitePvVo implements Serializable {

	//活动ID
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活动ID")
	private Long activityId;

	// 活动名称
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("活动名称")
	private String activityName;

	// 展示数PV
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("PV")
	private Long servicePV;

	// 展示UV
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("UV")
	private Long serviceUV;
}
