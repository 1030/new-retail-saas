package com.dy.yunying.api.req.znfx;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @description:
 * @author: admin
 * @time: 2023/3/21 14:12
 */
@Data
public class ZnfxReq implements Serializable {
	private static final long serialVersionUID = -5808137062113262806L;

	/**
	 * 查询范围 使用ZnfxCodeEnum的code拼接
	 */
	@NotBlank(message = "查询范围不能为空")
	private String searchRange;

	@NotBlank(message = "开始时间不能为空")
	@ApiModelProperty(value = "开始时间(格式：yyyyMMdd)")
	private String startTime;

	@NotBlank(message = "结束时间不能为空")
	@ApiModelProperty(value = "结束时间(格式：yyyyMMdd)")
	private String endTime;

	/**
	 * 主渠道编码，多个英文逗号隔开
	 */
	private String parentchlArr;
	/**
	 * 广告账户，多个英文逗号隔开
	 */
	private String adAccount;
	/**
	 * 计划ID，多个英文逗号隔开
	 */
	private String adId;
	/**
	 * 首日ROI-最小
	 */
	private String firstRoiMin;
	/**
	 * 首日ROI-最大
	 */
	private String firstRoiMax;
	/**
	 * 3日ROI-最小
	 */
	private String threeRoiMin;
	/**
	 * 3日ROI-最大
	 */
	private String threeRoiMax;
	/**
	 * 7日ROI-最小
	 */
	private String sevenRoiMin;
	/**
	 * 7日ROI-最大
	 */
	private String sevenRoiMax;
	/**
	 * 15日ROI-最小
	 */
	private String fifteenRoiMin;
	/**
	 * 15日ROI-最大
	 */
	private String fifteenRoiMax;
	/**
	 * 30日ROI-最小
	 */
	private String thirtyRoiMin;
	/**
	 * 30日ROI-最大
	 */
	private String thirtyRoiMax;
	/**
	 * 注册成本-最小
	 */
	private String regCostMin;
	/**
	 * 注册成本-最大
	 */
	private String regCostMax;
	/**
	 * 首日付费成本-最小
	 */
	private String firstPayCostMin;
	/**
	 * 首日付费成本-最大
	 */
	private String firstPayCostMax;
	/**
	 * 累计付费成本-最小
	 */
	private String totalPayCostMin;
	/**
	 * 累计付费成本-最大
	 */
	private String totalPayCostMax;

}
