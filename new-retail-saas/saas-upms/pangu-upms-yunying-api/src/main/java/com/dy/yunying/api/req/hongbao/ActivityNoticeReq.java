package com.dy.yunying.api.req.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 新增/编辑对象
 * @date 2021/10/23 11:04
 */
@Data
public class ActivityNoticeReq implements Serializable {

	@ApiModelProperty(value = "活动公告ID")
	private Long id;

	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	@ApiModelProperty(value = "公告标题")
	@NotBlank(message = "公告标题不能为空")
	@Length(min = 1, max = 50, message = "公告标题字符长度不合法")
	private String noticeTitle;

	@ApiModelProperty(value = "开始时间")
	@NotBlank(message = "开始时间不能为空")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	@NotBlank(message = "结束时间不能为空")
	private String finishTime;

	@ApiModelProperty(value = "跳转开关(1开启 0关闭)")
	@NotNull(message = "跳转开关不能为空")
	private Integer jumpOff;

	@ApiModelProperty(value = "公告详情")
	private String noticeInfo;

}
