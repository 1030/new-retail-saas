package com.dy.yunying.api.vo.usergroup;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProvinceCityVo implements Serializable {

	private static final long serialVersionUID = -987975607088420670L;
	//主键ID
	private Long id;


	//父ID
	private Long pid;


	private String name;

}
