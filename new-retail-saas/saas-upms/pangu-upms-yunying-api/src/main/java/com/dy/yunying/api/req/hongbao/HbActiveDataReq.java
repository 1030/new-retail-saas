package com.dy.yunying.api.req.hongbao;

import lombok.Data;

/**
 * @author ：lile
 * @date ：2021/11/17 18:20
 * @description：
 * @modified By：
 */
@Data
public class HbActiveDataReq {

	private String activeId;

	private String activeName;

	/**
	 * 开始时间，格式 'yyyy-MM-dd'
	 */
	private String startTime;

	/**
	 * 结束时间，格式 'yyyy-MM-dd'
	 */
	private String endTime;

}
