package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 活动中心与区服关系表
 * @author  chenxiang
 * @version  2022-06-18 11:46:48
 * table: hb_activity_center_area
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "hb_activity_center_area")
public class HbActivityCenterArea extends Model<HbActivityCenterArea>{
	/**
	 * 主键ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 活动中心ID
	 */
	@TableField(value = "activity_center_id")
	private Long activityCenterId;
	/**
	 * 主游戏ID
	 */
	@TableField(value = "parent_game_id")
	private Long parentGameId;
	/**
	 * 区服ID
	 */
	@TableField(value = "area_id")
	private Long areaId;
	/**
	 * 区服层级(1主游戏 2区服)
	 */
	@TableField(value = "area_range")
	private Integer areaRange;
	/**
	 * 是否删除(0否 1是)
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

