package com.dy.yunying.api.resp.znfx;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 */
@Data
@Accessors(chain = true)
public class AdPlanReportRes implements Serializable {

	private static final long serialVersionUID = 5491439265041046186L;

	@ApiModelProperty(value = "原始消耗")
	private BigDecimal rudeCost;

	@ApiModelProperty(value = "返点后消耗")
	private BigDecimal totalCost;

	@ApiModelProperty(value = "计划数")
	private Long adIdNum;

	@ApiModelProperty(value = "用户数")
	private Long userNum;
}
