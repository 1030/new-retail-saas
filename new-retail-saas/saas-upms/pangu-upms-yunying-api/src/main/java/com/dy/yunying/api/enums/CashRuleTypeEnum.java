package com.dy.yunying.api.enums;

public enum CashRuleTypeEnum {

	DAYLIMIT(1,"每天次数"),
	TOTALLIMIT(2,"累计次数"),
	TOTALMONEY(3,"累计金额");

	CashRuleTypeEnum(Integer type, String desc){
		this.type =type;
		this.desc =desc;
	}

	private Integer type;

	private String desc;

	public Integer getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}
}
