package com.dy.yunying.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Collection;
import java.util.Date;

/**
 * 余额充值订单
 */
@Data
@Accessors(chain = true)
public class CurrencyOrderVO extends Page<Object> {

	/**
	 * 充值开始时间，格式：2022-03-30 12:21:32
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date rechargeStartTime;

	/**
	 * 充值结束时间，格式：2022-03-30 12:21:32
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date rechargeEndTime;

	/**
	 * 注册开始时间，格式：2022-03-30 12:21:32
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date registerStartTime;

	/**
	 * 注册结束时间，格式：2022-03-30 12:21:32
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date registerEndTime;

	/**
	 * 充值账号
	 */
	private String username;

	/**
	 * 充值ip
	 */
	private String ip;

	/**
	 * 平台订单编号
	 */
	private String orderno;

	/**
	 * 支付平台交易号
	 */
	private String tradeno;

	/**
	 * 支付状态：2-成功；127-失败；
	 */
	private Integer rechargeStatus;

	/**
	 * 充值方式：14-支付宝；15-微信；
	 */
	private Integer rechargeType;

	/**
	 * 主游戏ID
	 */
	private Collection<Long> pgids;

	/**
	 * 子游戏ID
	 */
	private Collection<Long> gameids;

	/**
	 * 导出列标题，多个使用逗号分隔
	 */
	private String titles;

	/**
	 * 导出列名，多个使用逗号分隔
	 */
	private String columns;

}
