package com.dy.yunying.api.datacenter.export;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pig4cloud.pig.common.core.util.BigDecimalUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("广告数据分析")
public class AdExportDataAnalysisVO  implements Serializable {

	private static final long serialVersionUID = 8701826234401217248L;

	private static final String DEFAULT_VALUE = "-";

	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	// 周期
	private String period;



	@ExcelProperty("广告计划名称")
	@ApiModelProperty("广告计划名称")
	// 广告计划名称
	private String adidName;

	@ExcelProperty("系统")
	@ApiModelProperty("系统")
	private String osStr;


	@ExcelProperty("主游戏")
	@ApiModelProperty("主游戏")
	private String parentGameName;

	@ExcelProperty("子游戏")
	@ApiModelProperty("子游戏")
	private String gameName;

	// 部门
	@ExcelProperty("部门")
	@ApiModelProperty("部门")
	private String deptName;

	@ExcelProperty("组别")
	@ApiModelProperty("组别")
	private String userGroupName;

	// 渠道名称
	@ExcelProperty("渠道名称")
	@ApiModelProperty("渠道名称")
	private String parentchlname;


	@ExcelProperty("分包编码")
	@ApiModelProperty("分包编码")
	private String appchl;

	@ExcelProperty("投放人")
	@ApiModelProperty("投放人")
	private String investorName;

	@ExcelProperty("广告主ID")
	@ApiModelProperty("广告主ID")
	private String advertid;

	@ExcelProperty("广告账号名称")
	@ApiModelProperty("广告账号名称")
	private String adAccountName;

	@ExcelProperty("计划ID")
	@ApiModelProperty("计划ID")
	// 广告计划ID
	private String adid;

	@ExcelProperty("转化目标")
	@ApiModelProperty("转化目标")
	private String convertDescri;

	// 点击率   点击数/展示数
	@ExcelProperty("点击率")
	@ApiModelProperty("点击率")
	private String clickRatio;

	// 点击注册率  注册数/点击数
	@ExcelProperty("点击注册率")
	@ApiModelProperty("点击注册率")
	private String regRatio;

	// 返点后消耗（总成本）
	@ExcelProperty("消耗")
	@ApiModelProperty("返点前消耗")
	private BigDecimal rudeCost = new BigDecimal(0);

	@ExcelProperty("返点后消耗")
	@ApiModelProperty("消耗后消耗（总成本）")
	@JsonProperty("cost")
	private BigDecimal cost = new BigDecimal(0);

	//新增设备数
	@ExcelProperty("激活数")
	@ApiModelProperty("新增设备数")
	private Integer uuidnums = 0;

	//新增设备注册数(新用户注册)
	@ExcelProperty("新增设备注册数")
	@ApiModelProperty("新增设备注册数")
	private Integer usrnamenums = 0;

	// 点击率   点击数/展示数
	@ExcelProperty("激活注册率")
	@ApiModelProperty("激活注册率")
	private String activationRatio;

	// 新增注册创角数
	@ExcelProperty("创角数")
	@ApiModelProperty("新增注册创角数")
	private Integer createRoleCount;

	// 新增创角率
	@ExcelProperty("注册创角率")
	@ApiModelProperty("新增创角率")
	private String createRoleRate;

	// 新增注册实名数
	@ExcelProperty("新增注册实名数")
	@ApiModelProperty("新增注册实名数")
	private Integer certifiedCount;

	// 注册未实名数
	@ExcelProperty("注册未实名数")
	@ApiModelProperty("注册未实名数")
	private Integer notCertifiedCount;

	// 新增实名制转化率
	@ExcelProperty("注册实名率")
	@ApiModelProperty("新增实名制转化率")
	private String certifiedRate;

	// 设备成本  消耗/新增设备注册数
	@ExcelProperty("设备成本")
	@ApiModelProperty("设备成本")
	private BigDecimal deviceCose = new BigDecimal(0);

	//注册arpu  新增充值金额/新增设备数
	@ExcelProperty("新增ARPPU")
	@ApiModelProperty("新增ARPPU")
	private BigDecimal regarpu = new BigDecimal(0);

	// 新增付费ARPPU
	@ExcelProperty("新增付费ARPPU")
	@ApiModelProperty("新增付费ARPPU")
	private BigDecimal payarppu;

	// 首日 新增充值设备数
	@ExcelProperty("新增付费数")
	@ApiModelProperty("首日新增充值设备数")
	private Integer paydevice1 = 0;

	//首充党占比（¥0-10）
	@ExcelProperty("首充党占比（¥0-10）")
	private Integer pay0;

	//新增付费¥10-50
	@ExcelProperty("新增付费¥10-50")
	private Integer pay10;

	//新增付费¥50-100
	@ExcelProperty("新增付费¥50-100")
	private Integer pay50;

	//新增付费¥100-200
	@ExcelProperty("新增付费¥100-200")
	private Integer pay100;

	//新增付费¥200-500
	@ExcelProperty("新增付费¥200-500")
	private Integer pay200;

	//新增付费¥500+
	@ExcelProperty("新增付费¥500+")
	private Integer pay500;

	//新增付费率  新增设备付费数/新增设备注册数
	@ExcelProperty("新增付费率")
	@ApiModelProperty("新增付费率")
	private String regPayRatio;

	// 新增充值金额
	@ExcelProperty("新增充值实付金额")
	@ApiModelProperty("新增充值实付金额")
	private BigDecimal newPayFee = new BigDecimal(0);

	// 新增充值代金券金额
	@ExcelProperty("新增充值代金券金额")
	@ApiModelProperty("新增充值代金券金额")
	private BigDecimal newPayGivemoney = new BigDecimal(0);

	@ExcelProperty("累计付费数")
	@ApiModelProperty("累计付费数")
	private Integer paydeviceAll = 0;

	@ExcelProperty("累计付费率")
	@ApiModelProperty("累计付费率")
	private String totalPayRate;

	@ExcelProperty("活跃充值实付金额")
	@ApiModelProperty("活跃充值实付金额")
	private BigDecimal activePayFee = new BigDecimal(0);


	@ExcelProperty("活跃充值代金券金额")
	@ApiModelProperty("活跃充值代金券金额")
	private BigDecimal activePayGivemoney = new BigDecimal(0);

	@ExcelProperty("当周充值实付金额")
	@ApiModelProperty("当周充值实付金额")
	private BigDecimal weekPayFee = new BigDecimal(0);

	@ExcelProperty("当月充值实付金额")
	@ApiModelProperty("当月充值实付金额")
	private BigDecimal monthPayFee = new BigDecimal(0);

	// 期内设备付费数
	@ExcelProperty("期内充值人数")
	@ApiModelProperty("期内设备付费数")
	private Integer periodPayCount;


	// 期内充值实付金额
	@ExcelProperty("期内充值实付金额")
	@ApiModelProperty("期内充值实付金额")
	private BigDecimal periodPayFee;

	@ExcelProperty("期内付费成本")
	@ApiModelProperty("期内付费成本")
	private BigDecimal periodPayCose = new BigDecimal(0);

	// 期内付费率
	@ExcelProperty("期内付费率")
	@ApiModelProperty("期内付费率")
	private String periodPayRate;

	@ExcelProperty("期内ROI")
	@ApiModelProperty("期内ROI")
	private String periodROI;

	//活跃ARPU= 活跃充值金额/活跃设备数
	@ExcelProperty("活跃ARPU")
	@ApiModelProperty("活跃设备ARPU")
	private BigDecimal actarpu = new BigDecimal(0);

	// 活跃付费ARPPU
	@ExcelProperty("活跃付费ARPPU")
	@ApiModelProperty("活跃付费ARPPU")
	private BigDecimal activearppu;

	@ExcelProperty("活跃设备数")
	@ApiModelProperty("活跃设备数")
	private Integer activeNum = 0;

	@ExcelProperty("活跃付费数")
	@ApiModelProperty("活跃付费数")
	private BigDecimal activePayCount = new BigDecimal(0);

	@ExcelProperty("活跃付费次数")
	@ApiModelProperty("活跃付费次数")
	private Integer payNum = 0;

	@ExcelProperty("首日付费次数")
	@ApiModelProperty("首日付费次数")
	private Integer payNum1 = 0;

	// 每日付费成本
	@ExcelProperty("每日付费成本")
	private BigDecimal dayPayCost = new BigDecimal(0);

	@ExcelProperty("首周付费次数")
	@ApiModelProperty("首周付费次数")
	private Integer payNum7 = 0;

	//首日付费成本	返点后消耗/新增付费数
	@ExcelProperty("首日付费成本")
	@ApiModelProperty("首日付费成本")
	private BigDecimal payCose1 = new BigDecimal(0);


	//累计付费成本	返点后消耗/累计付费数
	@ExcelProperty("累计付费成本")
	@ApiModelProperty("累计付费成本")
	private BigDecimal payCoseAll = new BigDecimal(0);

	// 活跃付费率
	@ExcelProperty("活跃付费率")
	@ApiModelProperty("活跃付费率")
	private String activePayRate;

	@ExcelProperty("首日ROI")
	@ApiModelProperty("首日ROI")
	private String roi1;

	@ExcelProperty("当周ROI")
	@ApiModelProperty("当周ROI")
	private String weekRoi;

	@ExcelProperty("当月ROI")
	@ApiModelProperty("当月ROI")
	private String monthRoi;

	//总roi 累计充值金额/总消耗
	@ExcelProperty("累计充值ROI")
	@ApiModelProperty("累计充值ROI")
	private String allRoi;

	// 累计充值金额
	@ExcelProperty("累计充值实付金额")
	@ApiModelProperty("累计充值实付金额")
	private BigDecimal totalPayFee = new BigDecimal(0);

	// 累计充值金额
	@ExcelProperty("累计充值代金券金额")
	@ApiModelProperty("累计充值代金券金额")
	private BigDecimal totalPayGivemoney = new BigDecimal(0);

	// 次留: 新增设备在次日有登录行为的设备数/新增设备注册数
	@ExcelProperty("次留")
	@ApiModelProperty("次留")
	@JsonProperty("retention2Ratio")
	private String retention2Ratio;

	// 付费次留
	@ExcelProperty("付费次留")
	@ApiModelProperty("付费次留")
	private String payedRetention2;

	// 去重设备数
	@ExcelProperty("去重设备数")
	private Integer deduplicateDeviceCount;

	// 回归设备数
	@ExcelProperty("回归设备数")
	private Integer returnDeviceCount;

	// 重复设备数
	@ExcelProperty("重复设备数")
	private Integer duplicateDeviceCount;


	@ExcelProperty("设备重复率")
	@ApiModelProperty("设备重复率")
	private String duplicateDeviceRatio;

	@ExcelProperty("父游戏id")
	@ApiModelProperty("父游戏id")
	private Long pgid;

	// 子游戏id
	@ExcelProperty("子游戏id")
	@ApiModelProperty("子游戏id")
	//	@JsonProperty("gameid")
	private Long gameid;

	@ExcelProperty("显示分成前的数据")
	@ApiModelProperty("是否显示分成前的数据 ：0 否 1：是")
	private Integer showRatio = 0;

	//系统类型
	@ExcelProperty("系统类型")
	@ApiModelProperty("系统类型")
	private Integer os;

	@ExcelProperty("父渠道")
	// 渠道类型
	@ApiModelProperty("父渠道")
	private String parentchl;

	@ExcelProperty("子渠道")
	// 渠道名称
	@ApiModelProperty("子渠道")
	private String chl;

	@ExcelProperty("部门ID")
	// 部门ID
	@ApiModelProperty("部门ID")
	private String deptId;

	@ExcelProperty("组别主键")
	@ApiModelProperty("组别主键")
	private String userGroupId;

	@ExcelProperty("投放人id")
	@ApiModelProperty("投放人id")
	private String investor;

	@ExcelProperty("广告状态描述")
	@ApiModelProperty("广告状态描述")
	private String status;

	@ExcelProperty("广告操作状态")
	@ApiModelProperty("广告操作状态")
	private String optStatus;

	@ExcelProperty("投放开始时间")
	@ApiModelProperty("投放开始时间")
	private String startTime;

	@ExcelProperty("投放开始时间")
	@ApiModelProperty("投放开始时间")
	private String endTime;

	@ExcelProperty("投放时间")
	@ApiModelProperty("投放时间")
	private String scheduleType;

	@ExcelProperty("出价(元)")
	@ApiModelProperty("出价(元)")
	private BigDecimal pricing;

	@ExcelProperty("计划预算(元)")
	@ApiModelProperty("计划预算(元)")
	private BigDecimal budget;

	@ExcelProperty("投放方式")
	@ApiModelProperty("投放方式")
	private String unionVideoType;

	/**
	 * 投放范围 ： 通投智选(UNIVERSAL)  默认(DEFAULT) 穿山甲(UNION)
	 */
	@ExcelProperty("投放范围")
	@ApiModelProperty("投放范围")
	private String deliveryRange;

	@ExcelProperty("转化目标")
	@ApiModelProperty("转化目标")
	private String convertName;


	@ExcelProperty("深化目标")
	@ApiModelProperty("深化目标")
	private String deepConvert;

	@ExcelProperty("深度转化目标描述")
	@ApiModelProperty("深度转化目标描述")
	private String deepConvertDescri;

	@ExcelProperty("判断新老广告计划")
	@ApiModelProperty("判断新老广告计划,1:表示老广告计划 2:表示新广告计划")
	private String versionType;

	// 转化统计方式
	@ExcelProperty("转化统计方式")
	@ApiModelProperty("转化统计方式")
	private String convertDataType;

	@ExcelProperty("转化目标id")
	@ApiModelProperty("转化目标id")
	private String convertId;

	@ExcelProperty("投放位置")
	@ApiModelProperty("投放位置")
	private String inventoryType;

	@ExcelProperty("广告组id")
	@ApiModelProperty("广告组id")
	private String campaignId;

	@ExcelProperty("广告组名称")
	@ApiModelProperty("广告组名称")
	private String campaignName;

	@ExcelProperty("创建时间")
	@ApiModelProperty("创建时间")
	private String createTime;

	@ExcelProperty("修改时间")
	@ApiModelProperty("修改时间")
	private String updateTime;

	@ExcelProperty("平台类型")
	@ApiModelProperty("平台类型：1头条 7 uc  8 广点通 9 ")
	private String ctype;

	@ExcelProperty("展示数")
	@ApiModelProperty("展示数")
	@JsonProperty("showNums")
	private Integer shownums = 0;

	// 点击数
	@ExcelProperty("点击数")
	@ApiModelProperty("点击数")
	private Integer clicknums = 0;



	// 点击激活率   新增设备数/点击数
	@ExcelProperty("点击激活率")
	@ApiModelProperty("点击激活率")
	private String clickActiveRatio;

	//新增分成后
	@ExcelProperty("新增分成后")
	@ApiModelProperty("新增分成后")
	private BigDecimal newdevicesharfee = new BigDecimal(0);

	// 周分成后
	@ExcelProperty("周分成后")
	@ApiModelProperty("周分成后")
	private BigDecimal weeksharfee = new BigDecimal(0);

	// 月分成后
	@ExcelProperty("月分成后")
	@ApiModelProperty("月分成后")
	private BigDecimal monthsharfee = new BigDecimal(0);

	// 累计分成后
	@ExcelProperty("累计分成后")
	@ApiModelProperty("累计分成后")
	private BigDecimal totalPaysharfee = new BigDecimal(0);

	// 活跃分成后
	@ExcelProperty("活跃分成后")
	@ApiModelProperty("活跃分成后")
	private BigDecimal activesharfee = new BigDecimal(0);

	// 未成年人数
	@ExcelProperty("未成年人数")
	@ApiModelProperty("未成年人数")
	private Integer youngCount;

	// 当周充值设备数
	@ExcelProperty("当周充值设备数")
	@ApiModelProperty("当周充值设备数")
	private Integer paydevice7 = 0;


	@ExcelProperty("当周充值代金券金额")
	@ApiModelProperty("当周充值代金券金额")
	private BigDecimal weekPayGivemoney = new BigDecimal(0);

	// 首日 新增充值设备数
	@ExcelProperty("首日新增充值设备数")
	@ApiModelProperty("首日新增充值设备数")
	private Integer paydevice30 = 0;



	@ExcelProperty("当月充值代金券金额")
	@ApiModelProperty("当月充值代金券金额")
	private BigDecimal monthPayGivemoney = new BigDecimal(0);


	@ExcelProperty("期内充值代金券金额")
	@ApiModelProperty("期内充值代金券金额")
	private BigDecimal periodPayGivemoney;

	@ExcelProperty("活跃用户数")
	@ApiModelProperty("活跃用户(有登录) ")
	private Integer activeLoginNum = 0;



	//设备次日留存数
	@ExcelProperty("设备次日留存数")
	@ApiModelProperty("设备次日留存数")
	private Integer retention2 = 0;


	// 广告名称
	@ExcelProperty("广告账号名称")
	@ApiModelProperty("广告账号名称")
	private String adname;

	// 广告计划ID
	@ApiModelProperty("广告主键id")
	private String adgroupId;

	// 游戏
	@ExcelProperty("游戏")
	@ApiModelProperty("游戏")
	private String gname;

	// 父游戏
	@ExcelProperty("父游戏")
	@ApiModelProperty("父游戏")
	private String pgame;

	@ExcelProperty("分包编码")
	@ApiModelProperty("分包编码")
	private String packCode;


	public BigDecimal getNewdevicesharfee() {
		return BigDecimalUtils.round(newdevicesharfee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getWeeksharfee() {
		return BigDecimalUtils.round(weeksharfee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getMonthsharfee() {
		return BigDecimalUtils.round(monthsharfee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getTotalPaysharfee() {
		return BigDecimalUtils.round(totalPaysharfee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivesharfee() {
		return BigDecimalUtils.round(activesharfee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getNewPayFee() {
		return BigDecimalUtils.round(newPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getNewPayGivemoney() {
		return BigDecimalUtils.round(newPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getWeekPayFee() {
		return BigDecimalUtils.round(weekPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getWeekPayGivemoney() {
		return BigDecimalUtils.round(weekPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getMonthPayFee() {
		return BigDecimalUtils.round(monthPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getMonthPayGivemoney() {
		return BigDecimalUtils.round(monthPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getPeriodPayFee() {
		return BigDecimalUtils.round(periodPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getPeriodPayGivemoney() {
		return BigDecimalUtils.round(periodPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getTotalPayFee() {
		return BigDecimalUtils.round(totalPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getTotalPayGivemoney() {
		return BigDecimalUtils.round(totalPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivePayFee() {
		return BigDecimalUtils.round(activePayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivePayGivemoney() {
		return BigDecimalUtils.round(activePayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActarpu() {
		return BigDecimalUtils.round(actarpu, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivearppu() {
		return BigDecimalUtils.round(activearppu, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getRegarpu() {
		return BigDecimalUtils.round(regarpu, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getPayarppu() {
		return BigDecimalUtils.round(payarppu, BigDecimalUtils.DEF_SCALE);
	}


	public String getInvestorName() {
		return StringUtils.isBlank(investorName) ? "-" : investorName;
	}


	public String getUserGroupName() {
		return StringUtils.isBlank(userGroupName) ? "-" : userGroupName;
	}

	public String getOsStr() {
		if (null == this.osStr) {
			this.osStr = null == this.os ? DEFAULT_VALUE : OsEnum.getName(this.os);
		}
		return this.osStr;
	}

	public String getDeptName() {
		return StringUtils.isBlank(deptName) ? "-" : deptName;
	}

}
