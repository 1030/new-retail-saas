package com.dy.yunying.api.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sunyq
 * @date 2022/8/18 19:35
 */
@Data
public class AccountManageVO implements Serializable {
	/**
	 * 用户id
	 */
	@ExcelIgnore
	private Long userId;
	/**
	 * 注册时间
	 */
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("注册时间")
	private Date regtime;

	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("手机号")
	private String phoneNumber;

	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("账号")
	private String account;

	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("平台昵称")
	private String nickName;

	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("注册端口")
	private String regSrc;

	@ExcelIgnore
	private String channel;

	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("注册平台")
	private String channelName;

	/**
	 * 防沉迷  0 已成年  1 未成年 2 未实名
	 */
	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("防沉迷")
	private String antiAddictionStr;

	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("注册IP")
	private String regIp;

	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("注册地区")
	private String regArea;
	/**
	 * 账号状态
	 */
	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("账号状态")
	private String accountStatus;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("最后活跃时间")
	private String lastActiveTime;


}
