package com.dy.yunying.api.dto.sign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 签到活动奖品表
 * @author  chengang
 * @version  2021-12-01 10:14:16
 * table: sign_activity_prize
 */
@Data
public class SignActivityPrizeDto extends Page<Object> implements Serializable {

	//columns START
	//主键id
	private Long id;
	//活动ID
	private Long activityId;
	//领取序号
	private Integer receiveSn;
	//领取时间
	private Date receiveDate;
	//名称
	private String name;
	//类型  1游戏物品 2代金券
	private Integer type;
	//礼包码类型：1唯一码，2通用码
	private String giftType;
	// 唯一码文件
	private MultipartFile file;
	//礼包通用码
	private String giftCode;
	//礼包总数量
	private String giftAmount;
	//代金券名称
	private String cdkName;
	//代金券金额
	private String cdkAmount;
	//限制金额
	private String cdkLimitAmount;
	//有效规则类型  1固定时间 2领券后指定天数
	private Integer expiryType;
	//有效开始时间
	private String startTime;
	//有效结束时间
	private String endTime;
	//有效天数
	private Integer days;
	//代金券限制类型 1:无门槛 2:指定游戏 3:指定角色
	private Integer cdkLimitType;

	/**
	 * 游豆价值
	 */
	private BigDecimal currencyAmount;

	/**
	 * 游豆类型：1-永久游豆；2-临时游豆；
	 */
	private Integer currencyType;

	/**
	 * 游豆过期类型：1-固定结束时间；2-领取后过期天数；
	 */
	private Integer currencyExpireType;

	/**
	 * 游豆固定过期时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date currencyExpireTime;

	/**
	 * 游豆领取后过期天数
	 */
	private Integer currencyExpireDays;

	//是否删除  0否 1是
	private Integer deleted;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	//创建人
	private Long createId;
	//修改人
	private Long updateId;
	//columns END 数据库字段结束

	private String goodsJson;

	/**
	 * 代金券类型  1 满减  2 折扣
	 */
	private Integer cdkType;

	
	
}


