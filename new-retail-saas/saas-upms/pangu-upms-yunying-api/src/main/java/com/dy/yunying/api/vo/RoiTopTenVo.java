package com.dy.yunying.api.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;


@Data
@Accessors(chain = true)
public class RoiTopTenVo {

	private Integer sort;
	/**
	 * roi
	 */
	private BigDecimal roi;
	/**
	 * 开始日期，格式为：20220322
	 */
	private BigDecimal cost;

	private String materialId;

	private String materialName;

	private String materialUrl;

	private String pageUrl;

	private List<String> title;

	private String cid;
	private String imageUrl;
	private String	format;

	private String pageImage;

	private String pageName;

	private String advertiserId;

	private String tmId;
}
