package com.dy.yunying.api.resp.yyz;

import lombok.Data;

import java.util.Date;
import java.util.List;


/**
 * 游戏预约档位库
 * @date 2021-06-19 16:12:13
 */
@Data
public class GameAppointmentScheduleOnlineRes {


	private String onlineName;

	private Integer onlineNum;

}
