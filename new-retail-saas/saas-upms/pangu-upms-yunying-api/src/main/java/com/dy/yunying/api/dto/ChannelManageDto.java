package com.dy.yunying.api.dto;

import com.dy.yunying.api.entity.ChannelManageDO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChannelManageDto extends ChannelManageDO {

	private String soncode;

	private String sonname;

	private Integer sonChannelId; //查询子渠道传过来的   主渠道id

	private Integer chlId;  //查询子渠道传过来的   子渠道id

	private String oldManager;

	private Integer packUserid;
}
