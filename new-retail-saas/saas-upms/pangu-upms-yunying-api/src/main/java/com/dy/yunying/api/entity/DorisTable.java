package com.dy.yunying.api.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName DorisTable
 * @Description todo
 * @Author nieml
 * @Time 2022/5/26 15:34
 * @Version 1.0
 **/

@Data
public class DorisTable {

	private Integer k1;
	private BigDecimal k2;
	private String k3;
	private Integer k4;


}
