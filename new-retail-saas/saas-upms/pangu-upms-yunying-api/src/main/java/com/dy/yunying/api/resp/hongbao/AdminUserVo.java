package com.dy.yunying.api.resp.hongbao;

import com.dy.yunying.api.entity.hongbao.HbInvestRecord;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 后台用户
 * @date 2021/11/5 14:03
 */
@Data
public class AdminUserVo implements Serializable {

	@ApiModelProperty(value = "主键id")
	private Integer userId;

	@ApiModelProperty(value = "用户名")
	private String username;

}
