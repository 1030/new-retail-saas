package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 活动中心与渠道关系表
 * @author  chenxiang
 * @version  2022-06-18 11:46:59
 * table: hb_activity_center_channel
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "hb_activity_center_channel")
public class HbActivityCenterChannel extends Model<HbActivityCenterChannel>{
	/**
	 * 主键ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 活动中心ID
	 */
	@TableField(value = "activity_center_id")
	private Long activityCenterId;
	/**
	 * 主渠道编码
	 */
	@TableField(value = "parent_chl")
	private String parentChl;
	/**
	 * 子渠道编码
	 */
	@TableField(value = "chl")
	private String chl;
	/**
	 * 分包渠道编码
	 */
	@TableField(value = "app_chl")
	private String appChl;
	/**
	 * 渠道层级(1主渠道 2子渠道 3分包渠道)
	 */
	@TableField(value = "channel_range")
	private Integer channelRange;
	/**
	 * 是否删除(0否 1是)
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

