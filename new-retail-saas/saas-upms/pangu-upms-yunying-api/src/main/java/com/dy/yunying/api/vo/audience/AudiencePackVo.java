package com.dy.yunying.api.vo.audience;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 迁移并重写实现逻辑，故出入参数保持不变
 * @Description
 * @Author chengang
 * @Date 2022/7/18
 */
@Data
public class AudiencePackVo implements Serializable {

	private static final long serialVersionUID = -669239803188590584L;

	// 父游戏id
	private Integer pgid;

	// 父游戏名称
	private String pgidName;

	//IMEI
	private String imei;

	//IDFA
	private String idfa;

	//OAID
	private String oaid;

	//mac
	private String mac;

	//手机号
	private String mobile;

	//充值金额
	private BigDecimal payfee = BigDecimal.ZERO;

	//活跃天数
	private Integer activenum = 0;

	//最高连续活跃天数
	private Integer activemaxnum = 0;

}
