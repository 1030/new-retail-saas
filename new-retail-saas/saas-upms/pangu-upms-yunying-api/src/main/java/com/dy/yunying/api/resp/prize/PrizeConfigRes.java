package com.dy.yunying.api.resp.prize;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 奖励配置表
 *
 * @author chenxiang
 * @version 2022-04-28 16:37:18 table: prize_config
 */
@Data
public class PrizeConfigRes implements Serializable {

	@ApiModelProperty(value = "主键id")
	private Long id;

	@ApiModelProperty(value = "奖品推送主键ID")
	private Long prizePushId;

	@ApiModelProperty(value = "奖励推送类型 推送类型 ：1单个奖励，2组合奖励 ")
	private Integer prizePushType;

	@ApiModelProperty(value = "标题文本")
	private String title;

	@ApiModelProperty(value = "说明文本")
	private String description;

	@ApiModelProperty(value = "奖励按钮")
	private Integer prizeButton;

	@ApiModelProperty(value = "icon图")
	private String icon;

	@ApiModelProperty(value = "奖励类型：1代金券，2礼包码，3游豆，4卡密")
	private Integer prizeType;

	@ApiModelProperty(value = "金额")
	private BigDecimal money;

	@ApiModelProperty(value = "限制金额")
	private BigDecimal limitMoney;

	@ApiModelProperty(value = "时间类型：1固定时间，2领取后指定天数")
	private Integer timeType;

	@ApiModelProperty(value = "余额类型（游豆类型）。1：普通；2：临时")
	private Integer currencyType;

	@ApiModelProperty(value = "开始时间")
	private Date startTime;

	@ApiModelProperty(value = "结束时间")
	private Date endTime;

	@ApiModelProperty(value = "领取后指定时间(秒)")
	private Long effectiveTime;

	@ApiModelProperty(value = "使用限制(逗号分割)：all不限，game游戏，role角色")
	private String useLimitType;

	@ApiModelProperty(value = "礼包码类型：1唯一码，2通用码")
	private Integer giftType;

	@ApiModelProperty(value = "通用码")
	private String giftCode;

	@ApiModelProperty(value = "礼包码数量")
	private Integer giftAmount;

	@ApiModelProperty(value = "礼包内容")
	private String giftContent;

	@ApiModelProperty(value = "打开类型：0不跳转，1SDK面板，2内置浏览器，3外部链接")
	private Integer openType;

	@ApiModelProperty(value = "菜单Id")
	private String menuId;

	@ApiModelProperty(value = "菜单名称")
	private String menuTitle;

	@ApiModelProperty(value = "菜单编码")
	private String menuCode;

	@ApiModelProperty(value = "ios 菜单编码地址")
	private String menuCodeIos;
	/**
	 * 二级菜单编码(安卓)
	 */
	@ApiModelProperty(value = "二级菜单编码(安卓)")
	private String secondMenuCode;
	/**
	 * 二级菜单编码(苹果)
	 */
	@ApiModelProperty(value = "二级菜单编码(苹果)")
	private String secondMenuCodeIos;

	@ApiModelProperty(value = "来源类型(1红包活动 2签到活动)")
	private Integer skipMold;

	@ApiModelProperty(value = "跳转位置 如：红包类型 1：等级红包、2：充值红包、3：邀请红包、4：定制红包")
	private Integer skipBelongType;

	@ApiModelProperty(value = "来源id, 如：红包活动id")
	private Long skipId;

	@ApiModelProperty(value = "跳转地址")
	private String skipUrl;

	@ApiModelProperty(value = "跳转地址")
	private String skipUrlIOS;

	@ApiModelProperty(value = "展示方式(1横屏展示，2竖屏展示)")
	private Integer showType;

	@ApiModelProperty(value = "是否删除  0否 1是")
	private Integer deleted;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

	@ApiModelProperty(value = "创建人")
	private Long createId;

	@ApiModelProperty(value = "修改人")
	private Long updateId;

}
