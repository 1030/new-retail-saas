package com.dy.yunying.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 主游戏区服关联表
 * @author  chengang
 * @version  2021-10-27 10:42:03
 * table: parent_game_area
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "parent_game_area")
public class ParentGameArea extends Model<ParentGameArea>{

	//columns START
			//主键id
			@TableId(value = "id",type = IdType.AUTO)
			private Long id;

			//父游戏ID
			@TableField(value = "parent_game_id")
			private Long parentGameId;

			//区服ID
			@TableField(value = "area_id")
			private Long areaId;

			//区服名称
			@TableField(value = "area_name")
			private String areaName;

			//是否删除  0否 1是
			@TableField(value = "deleted")
			private Integer deleted;

			//创建时间
			@TableField(value = "create_time")
			private Date createTime;

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;

			//创建人
			@TableField(value = "create_id")
			private Long createId;

			//修改人
			@TableField(value = "update_id")
			private Long updateId;

	//columns END 数据库字段结束
	

	
}

	

	
	

