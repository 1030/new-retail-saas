package com.dy.yunying.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public enum Comparator {

	/**
	 * 等于
	 */
	EQUAL("equal", "等于"),

	/**
	 * 不等于
	 */
	NOTEQUAL("notEqual", "不等于"),

    /**
     * 大于
     */
    GREATERTHEN("greaterThen", "大于"),

	/**
	 * 小于
	 */
    LESSTHEN("lessThen", "小于"),

	/**
	 * 大于等于
	 */
	GREATEREQUAL("ge", "大于等于"),

	/**
	 * 小于等于
	 */
	LESSEQUAL("le", "小于等于"),

    /**
     * 包含
     */
    INCLUDE("include", "包含"),

    /**
     * 不包含
     */
    NOTINCLUDE("notInclude", "不包含"),



            ;

    /**
     * 类型
     */
    private final String type;

    /**
     * 描述
     */
    private final String description;

}
