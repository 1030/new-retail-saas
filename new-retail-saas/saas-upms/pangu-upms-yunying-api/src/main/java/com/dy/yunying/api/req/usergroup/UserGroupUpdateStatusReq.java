package com.dy.yunying.api.req.usergroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * hezz 2022-05-17 17:00:00
 * */
@Data
public class UserGroupUpdateStatusReq implements Serializable {


	private static final long serialVersionUID = 6296976379567355787L;


	@NotNull(message = "群组ID不能为空")
	@ApiModelProperty(value = "群组ID")
	private Long groupId;

	@ApiModelProperty(value = "操作类型 1更新标识")
	private Integer operType = 1;

	@NotNull(message = "状态值")
	@ApiModelProperty(value = "状态值")
	private Integer status;

	@ApiModelProperty(value = "用户数量")
	private Long groupUserNum;

	@ApiModelProperty(value = "规则查询版本号")
	private Long data_version;
}
