package com.dy.yunying.api.req.hongbao;

import com.pig4cloud.pig.common.core.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 提现记录表
 *
 * @author zxm
 * @date 2021-10-28 20:45:35
 */
@Data
public class HbWithdrawRecordReq extends Page implements Serializable {
	private static final long serialVersionUID = 5736716192296775395L;


	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	@ApiModelProperty(value = "提现方式：1游戏货币，2微信，3支付宝")
	private Integer type;

	@ApiModelProperty(value = "用户ID")
	private Long userId;

	@ApiModelProperty(value = "主游戏ID")
	private Integer parentGameId;
	/**
	 * 调整字段名：由以前复选改为单选
	 */
	@ApiModelProperty(value = "游戏ID")
	private String gameId;

	@ApiModelProperty(value = "区服ID")
	private Integer areaId;

	@ApiModelProperty(value = "角色ID")
	private String roleId;

	@ApiModelProperty(value = "角色名称")
	private String roleName;

	@ApiModelProperty(value = "状态：0审核中，1审核通过，2提现中 3提现失败 4提现成功 5交易成功（游戏货币使用）")
	private Integer status;

	@ApiModelProperty(value = "是否删除：0否 1是")
	private Integer deleted;

	@ApiModelProperty(value = "金额")
	private String money;


	@ApiModelProperty(value = "用户名")
	private String username;


	@ApiModelProperty("查询的字段 多个用逗号分隔  ")
	private String columns;

	@ApiModelProperty("导出的时候对应的表名")
	private String titles;


	@ApiModelProperty(value = "领取时间开始时间")
	private String withdrawTimeStart;
	@ApiModelProperty(value = "领取时间结束时间")
	private String withdrawTimeEnd;


	@ApiModelProperty(value = "到账时间开始段")
	private String payedTimeStart;
	@ApiModelProperty(value = "到账时间结束段")
	private String payedTimeEnd;

}
