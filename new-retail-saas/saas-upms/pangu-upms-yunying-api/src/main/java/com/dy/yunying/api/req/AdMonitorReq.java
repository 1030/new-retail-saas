package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
public class AdMonitorReq extends Page {
    /**
     * 广告id或广告名称,查询时，传入的实际上是 渠道包名
     */
    private String adArg;

    /**
     * 子渠道编码 选择
     */
    private String chl;

    /**
     * 子渠道编码 选择
     */
    private String appChl;

    /**
     * 投放人
     */
    private String manager;

    /**
     * 打包状态
     */
    private Integer packState;

    /**
     * 父游戏ID
     */
    private String gid;

    /**
     * 子游戏ID
     */
    private String childGid;

    /**
     * 子游戏ID
     */
    private String gameid;
    /**
     * 包ID
     */
    private String packid;
    /**
     * 同步状态
     */
    private String convertStatus;

    /**
     * 子游戏ID
     */
    private Long gameId;

    /**
     * 广告投放人
     */
    private Integer advertiser;

    /**
     * 广告投放人
     */
    private Integer userId;

    /**
     * 能够查看的投放人 列表  用逗号隔开
     */
    private String advertiserList;


    private String managerStr;

	private List<Integer> userList;
	/**
	 * 广告账号ID,支持模糊查询
	 */
	private String advertiserId;
	/**
	 * 广告账号ID，多个英文逗号隔开
	 */
	private String advertiserIdArr;
	/**
	 * 主渠道编码，多个英文逗号隔开
	 */
	private String  parentchlArr;

	/**
	 * 推广类型（1信息流 2短视频）
	 */
	private Integer spreadType;

	/**
	 * 结算类型(KOL(一口价) KOL(CPM按次))
	 */
	private String settleType;

	/**
	 * 转化名称
	 */
	private String adname;
}
