package com.dy.yunying.api.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author sunyq
 * @date 2022/8/16 16:53
 */
@Data
public class RealTimeKanbanRatio implements Serializable {
	/**
	 * 	趋势
	 */
	private String trend;
	/**
	 * 比例
	 */
	private String ratio;
}
