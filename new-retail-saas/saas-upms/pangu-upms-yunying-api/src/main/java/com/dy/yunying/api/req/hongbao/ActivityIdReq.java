package com.dy.yunying.api.req.hongbao;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 活动ID
 * @date 2021/10/23 11:04
 */
@Data
public class ActivityIdReq implements Serializable {

	@ApiModelProperty(value = "活动ID")
	@NotNull(message = "活动ID不能为空")
	private Long id;

}
