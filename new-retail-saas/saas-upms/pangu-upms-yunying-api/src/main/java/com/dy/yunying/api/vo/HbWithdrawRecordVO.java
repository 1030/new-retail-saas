package com.dy.yunying.api.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dy.yunying.api.entity.hongbao.HbBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 提现记录表
 *
 * @author hma
 * @date 2023-02-15 15:14:35
 */
@Data
@ApiModel(value = "提现记录导出vo")
public class HbWithdrawRecordVO  {



	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活动ID")
	private Long activityId;

	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("用户账号")
	@ApiModelProperty(value = "用户账号")
	private String username;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("活动名称")
	@ApiModelProperty(value = "活动名称")
	private String activityName;

	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("所属游戏")
	@ApiModelProperty(value = "所属游戏")
	private String parentGameName;


	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("子游戏")
	@ApiModelProperty(value = "子游戏")
	private String gameName;




	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("区服")
	@ApiModelProperty(value = "区服")
	private Integer areaId;


	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("角色名称")
	@ApiModelProperty(value = "角色名称")
	private String roleName;


	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("角色ID")
	@ApiModelProperty(value = "角色ID")
	private String roleId;



	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("提现类型")
	@ApiModelProperty(value = "提现方式：1游戏货币，2微信，3支付宝")
	private String typeName;

	@ApiModelProperty(value = "昵称")
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("收款账号")
	private String nickname;


	//todo
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("状态")
	@ApiModelProperty(value = "状态：0审核中，1审核通过，2提现中 3提现失败 4提现成功 5交易成功（游戏货币使用）")
	private String statusName;



	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("驳回原因")
	@ApiModelProperty(value = "驳回原因")
	private String rejectReason;

	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("提现金额")
	@ApiModelProperty(value = "提现金额")
	private BigDecimal money;



	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("领取时间")
	@ApiModelProperty(value = "领取时间")
	private Date withdrawTime;



	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("审核时间")
	@ApiModelProperty(value = "审核时间")
	private Date auditingTime;



	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("到账时间")
	@ApiModelProperty(value = "到账时间")
	private Date payedTime;







}
