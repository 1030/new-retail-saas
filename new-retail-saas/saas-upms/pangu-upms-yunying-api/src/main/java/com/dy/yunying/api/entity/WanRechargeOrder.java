package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户充值记录表
 *
 * @author zhuxm
 * @version 2022-01-13 16:02:25
 * table: wan_recharge_order
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "wan_recharge_order")
public class WanRechargeOrder extends Model<WanRechargeOrder> {
	/**
	 * 用户ID
	 */
	@TableField(value = "uid")
	private Long uid;
	/**
	 * 充值方式（1-支付宝；2-微信；3-网银支付,4-银联快捷支付；5-点卡支付；6-手机卡支付；7-财付通；8-Q币卡; 9-支付宝(pc); 10-补单；11-首充；12-活动CDK; 13-58币;14-h5支付宝; 15-h5微信）
	 */
	@TableField(value = "rtype")
	private Integer rtype;
	/**
	 * 充值产品类型 1：充值到游戏，2：充值到58币, 3 : 充值到h5游戏
	 */
	@TableField(value = "ptype")
	private Integer ptype;
	/**
	 * 发起充值请求的ip
	 */
	@TableField(value = "reqip")
	private String reqip;
	/**
	 * 发起充值请求的时间
	 */
	@TableField(value = "reqtime")
	private Date reqtime;
	/**
	 * 发起请求的流水号
	 */
	@TableField(value = "reqno")
	private String reqno;
	/**
	 * 请求接收时间
	 */
	@TableField(value = "receivetime")
	private Date receivetime;
	/**
	 * 失效时间
	 */
	@TableField(value = "expiretime")
	private Date expiretime;
	/**
	 * 补单对应的原订单号
	 */
	@TableField(value = "oldorderno")
	private String oldorderno;
	/**
	 * 系统生成的充值订单号
	 */
	@TableField(value = "orderno")
	private String orderno;
	/**
	 * 充值的账号
	 */
	@TableField(value = "account")
	private String account;
	/**
	 * 如果充值的是游戏，游戏ID
	 */
	@TableField(value = "gameid")
	private Long gameid;
	/**
	 * 如果充值的游戏,的区域ID
	 */
	@TableField(value = "areaid")
	private Long areaid;
	/**
	 * 如果充值的游戏，区服ID
	 */
	@TableField(value = "serverid")
	private Long serverid;
	/**
	 * 游戏平台id(对应游戏中的游戏渠道id)
	 */
	@TableField(value = "gplatformid")
	private Long gplatformid;
	/**
	 * 充值金额
	 */
	@TableField(value = "fee")
	private BigDecimal fee;
	/**
	 * 单价
	 */
	@TableField(value = "price")
	private BigDecimal price;
	/**
	 * 充值数量
	 */
	@TableField(value = "amount")
	private Integer amount;
	/**
	 * 单位
	 */
	@TableField(value = "unit")
	private String unit;
	/**
	 * 兑换游戏币的比值
	 */
	@TableField(value = "ratio")
	private Integer ratio;
	/**
	 * 兑换的数量
	 */
	@TableField(value = "examount")
	private Integer examount;
	/**
	 * 交易号
	 */
	@TableField(value = "tradeno")
	private String tradeno;
	/**
	 * 二维码信息
	 */
	@TableField(value = "qrcode")
	private String qrcode;
	/**
	 * 付款时间
	 */
	@TableField(value = "paytime")
	private Date paytime;
	/**
	 * 充值成功后与游戏兑换的请求流水号
	 */
	@TableField(value = "exrqno")
	private String exrqno;
	/**
	 * 充值成功后与系统发起游戏币兑换的时间
	 */
	@TableField(value = "extime")
	private Date extime;
	/**
	 * 兑换状态 0：等待 1：成功 2：失败  6 ：执行中  9：初始化报错
	 */
	@TableField(value = "exstatus")
	private Integer exstatus;
	/**
	 * 兑换成功后返回的流水号
	 */
	@TableField(value = "exrtno")
	private String exrtno;
	/**
	 * 兑换成功时间
	 */
	@TableField(value = "exsuctime")
	private Date exsuctime;
	/**
	 * 实际兑换数量
	 */
	@TableField(value = "realamount")
	private Integer realamount;
	/**
	 * 获得积分
	 */
	@TableField(value = "points")
	private Integer points;
	/**
	 * 订单的状态 0：预订单 1：未支付 2：支付成功 3：支付失败 4：异常订单 5：退款 6：操作中
	 */
	@TableField(value = "status")
	private Integer status;
	/**
	 * 错误码
	 */
	@TableField(value = "errcode")
	private String errcode;
	/**
	 * 错误信息
	 */
	@TableField(value = "errmsg")
	private String errmsg;
	/**
	 * 查询订单结果的次数
	 */
	@TableField(value = "qtimes")
	private Integer qtimes;
	/**
	 * 下次查询的时间
	 */
	@TableField(value = "nqtime")
	private Date nqtime;
	/**
	 * 订单的产生时间
	 */
	@TableField(value = "createtime")
	private Date createtime;
	/**
	 * 随机参数
	 */
	@TableField(value = "randomarg")
	private String randomarg;
	/**
	 * 充值摘要
	 */
	@TableField(value = "remark")
	private String remark;
	/**
	 * 充值用户注册时间
	 */
	@TableField(value = "registertime")
	private Date registertime;
	/**
	 * 第三方平台编号
	 */
	@TableField(value = "pno")
	private String pno;
	/**
	 * 游戏角色编号
	 */
	@TableField(value = "grole")
	private String grole;
	/**
	 * 游戏角色名
	 */
	@TableField(value = "grolename")
	private String grolename;
	/**
	 * 扩展参数
	 */
	@TableField(value = "exparams")
	private String exparams;
	/**
	 * 充值渠道
	 */
	@TableField(value = "channel")
	private String channel;
	/**
	 * 标记是否从活动页进入充值页,1表示是从活动页进入充值页，为空表示不是从活动页面进入充值页
	 */
	@TableField(value = "actflag")
	private Integer actflag;
	/**
	 * 补单者id  对应admin_user表中的id
	 */
	@TableField(value = "sptuser")
	private Integer sptuser;
	/**
	 * 用户token
	 */
	@TableField(value = "token")
	private String token;
	/**
	 * cdk编码
	 */
	@TableField(value = "cdk")
	private String cdk;
	/**
	 * 赠送金额
	 */
	@TableField(value = "givemoney")
	private BigDecimal givemoney;
	/**
	 * 折扣金额
	 */
	@TableField(value = "disfee")
	private BigDecimal disfee;
	/**
	 * 游戏订单号
	 */
	@TableField(value = "gameorder")
	private String gameorder;
	/**
	 * 商品ID
	 */
	@TableField(value = "gdsid")
	private String gdsid;
	/**
	 * 终端类型,0：pc网页端  1：h5   2:安卓手游 3:IOS手游
	 */
	@TableField(value = "terminal_type")
	private Integer terminalType;
	/**
	 * 1 未调用(包含调用失败)   2已调用成功
	 */
	@TableField(value = "istatus")
	private Integer istatus;
	/**
	 * 接口调用修改时间
	 */
	@TableField(value = "iupdatetime")
	private Date iupdatetime;
	/**
	 * 0 老版本  1新版本
	 */
	@TableField(value = "ver")
	private String ver;
	/**
	 * 兑换次数
	 */
	@TableField(value = "excount")
	private Integer excount;
	/**
	 * 0老数据1新数据
	 */
	@TableField(value = "data_type")
	private Integer dataType;
	/**
	 * 支付上报状态，0 待上报，1 上报中，2 已上报，3 上报失败，4 无需上报
	 */
	@TableField(value = "rpt_status")
	private Integer rptStatus;

	/**
	 * 分包渠道
	 */
	@TableField(value = "appchl")
	private String appchl;

	/**
	 * 充值游豆
	 */
	//余额价值[自定义传值,自定义的充值余额传值到Tmoney]
	@TableField(value = "currency_amount")
	private BigDecimal currencyAmount;

	/**
	 * 订单类型 1：充值到游戏，2：充值到游豆
	 */
	@TableField(value = "order_type")
	private Integer orderType;

}

	

	
	

