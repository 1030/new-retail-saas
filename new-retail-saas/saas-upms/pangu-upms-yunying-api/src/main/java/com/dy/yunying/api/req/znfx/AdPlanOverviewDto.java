package com.dy.yunying.api.req.znfx;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@ApiModel("广告报表查询对象")
public class AdPlanOverviewDto extends AdPlanAnalyseReq {

	@ApiModelProperty("是否系统管理员")
	private int isSys = 0;

	@ApiModelProperty("是否显示分成前的数据 ：0 是 1：否")
	private Integer showRatio = 1;

	@ApiModelProperty("周期类型 ：1 按天 2 按周  3  按月 4 汇总（默认）")
	private Integer cycleType = 4;

	@ApiModelProperty("平台类型 ：1头条，8广点通")
	private String platformType;

	public String getPeriod() {
		String period = "collect";
		switch (cycleType) {
			case 1:
				period = "day";
				break; //可选
			case 2:
				period = "week";
				//语句
				break; //可选
			case 3:
				period = "month";
				break; //可选
			case 4:
				period = "collect";
				break; //可选
			//你可以有任意数量的case语句
			default: //可选
				//语句
		}
		return period;
	}

	@ApiModelProperty("类别 多个用逗号分隔【系统 os,主游戏 pgid，子游戏 gid ,部门 deptId,渠道编码 parentchl，分包编码：appchl,广告计划 adId,  投放人investor   】")
	private String queryColumn;

	private String titles;
	private String columns;

	private Long current = 1L;
	private Long size = 10000L;

	//排序
	private String sort;

	//排序 指标字段值
	private String kpiValue;

}
