package com.dy.yunying.api.req.raffle;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 积分商店配置表(raffle_exchange_store)实体类
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class RaffleExchangeStoreReq {

	private static final long serialVersionUID = -8111293400535065182L;

	/**
     * 主键id
     */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
    /**
     * 活动ID
     */
    private Long activityId;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 商品消耗积分
     */
    private BigDecimal productPrice;
    /**
     * 商品库存
     */
    private Integer productInventory;
    /**
     * 商品消耗数量
     */
    private Integer productCostNum;
    /**
     * 单人兑换上限
     */
    private Integer productSingleLimit;
    /**
     * 商品排序序号
     */
    private Integer productOrder;
    /**
     * 商品icon地址
     */
    private String productIconUrl;
    /**
     * 商品类型：1-游戏物品；2-代金券；3-游豆；4-实物礼品；
     */
    private Integer productType;
    /**
     * 奖品类型：1-唯一礼包码；2-通用礼包码；1-满减优惠券；2-折扣优惠券；
     */
    private Integer giftType;
    /**
     * 通用礼包码编码/代金券名称/实物礼品名称
     */
    private String giftName;

	/**
	 * 唯一码文件
	 */
	private MultipartFile file;
    /**
     * 通用礼包码总数量
     */
    private Integer giftTotalNum;
    /**
     * 代金券一次发放数量
     */
    private Integer giftGrantNum;
    /**
     * 代金券金额/折扣券比例/游豆数量
     */
    private BigDecimal giftAmount;
    /**
     * 代金券使用限制金额
     */
    private BigDecimal giftUseLimit;
    /**
     * 奖品使用范围：1-不限；2-游戏；3-角色；
     */
    private Integer giftUseScope;
    /**
     * 奖品过期类型：1-永久有效；2-固定过期时间；3-领取后指定过期天数；
     */
    private Integer giftExpireType;
    /**
     * 奖品固定生效时间
     */
    private String giftStartTime;
    /**
     * 奖品固定过期时间
     */
    private String giftEndTime;
    /**
     * 领取后指定过期天数
     */
    private Integer giftExpireDays;
    /**
     * 是否删除：0-否；1-是；
     */
    private Integer deleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(update = "now()")
	private Date updateTime;
    /**
     * 创建人
     */
    private Long createId;
    /**
     * 修改人
     */
    private Long updateId;

	private String remake;

	private String productInfo;

	private Integer infoOpen;

}