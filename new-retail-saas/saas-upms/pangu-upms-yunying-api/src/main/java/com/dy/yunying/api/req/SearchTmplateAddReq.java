package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.annotation.TableId;
import com.dy.yunying.api.entity.DmSearchTmplate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.Date;

@ApiModel(value = "添加搜索模板")
@Data
public class SearchTmplateAddReq {
	/**
	 * 模板名称
	 */
	@NotBlank
	@ApiModelProperty(value="模板名称")
	private String name;
	/**
	 * 模板元素json信息
	 */
	@NotBlank
	@ApiModelProperty(value="模板元素json信息",example="\"{filter:{cycle:[],types:[],function:[]},customColumn:[]}\"")
	private String info;
	/**
	 * 模板类型，0:广告数据分析表搜索模板
	 */
	@ApiModelProperty(value="模板类型，0:广告数据分析表搜索模板")
	private Integer type=0;

	public String getInfo() {
		if(StringUtils.isBlank(info)){
			return "{}";
		}
		return info;
	}
	public DmSearchTmplate convertDmSearchTmplate(String createUser){
		DmSearchTmplate tmplate=new DmSearchTmplate();
		Date currentDdate=new Date();
		tmplate.setCreateTime(currentDdate);
		tmplate.setCreateUser(createUser);
		tmplate.setInfo(this.getInfo());
		tmplate.setName(this.getName());
		tmplate.setType(this.getType());
		tmplate.setUpdateTime(currentDdate);
		return tmplate;
	}
}
