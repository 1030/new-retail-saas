package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 活动ID
 * @date 2021/10/23 11:04
 */
@Data
public class SetCashLimitReq extends SelectCashLimitReq implements Serializable {

	@ApiModelProperty(value = "每天提现次数")
	@NotNull(message = "每天提现次数不能为空")
	@Max(value = 99, message = "提现次数必须小于等于99")
	@Min(value = 1, message = "提现次数必须大于等于1")
	private Integer dayCashLimit;

}
