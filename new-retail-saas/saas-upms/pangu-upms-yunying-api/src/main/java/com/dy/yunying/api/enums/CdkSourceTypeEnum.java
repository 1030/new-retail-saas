package com.dy.yunying.api.enums;

/**
 * @author yuwenfeng
 * @description: 代金券来源类型
 * @date 2022/3/17 18:03
 */
public enum CdkSourceTypeEnum {
	LEVEL(1, "红包-等级红包"),
	RECHARGE(2, "红包-充值红包"),
	INVITATION(3, "红包-邀请红包"),
	CUSTOM(4, "红包-定制红包"),
	SIGNACTIVETY(20, "签到活动"),
	PRIZE(30, "奖励推送");

	private Integer type;
	private String name;

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	CdkSourceTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

}
