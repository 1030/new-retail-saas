package com.dy.yunying.api.datacenter.vo;

import com.dy.yunying.api.entity.RealTimeKanban;
import com.dy.yunying.api.entity.RealTimeKanbanRatio;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sunyq
 * @date 2022/8/16 16:46
 */
@Data
public class RealTimeKanbanSummaryVO implements Serializable {
	/**
	 * 卡片名称
	 */
	private String name;
	/**
	 * 浮标信息
	 */
	private String tips;
	/**
	 *
	 */
	private String color;

	/**
	 * 数据
	 */
	private String data;
	/**
	 * 日环比
	 */
	private RealTimeKanbanRatio daysFromRatio;
	/**
	 * 周同比
	 */
	private RealTimeKanbanRatio weeksFromRatio;

}
