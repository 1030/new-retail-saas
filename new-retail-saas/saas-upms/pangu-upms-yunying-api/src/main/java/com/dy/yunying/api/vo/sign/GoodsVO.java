package com.dy.yunying.api.vo.sign;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author chengang
 * @Date 2021/12/1
 */
@Data
public class GoodsVO implements Serializable {

	private static final long serialVersionUID = -2154718623153838859L;


	private Long goodsId;
	private String goodsName;
	private String goodsUrl;
	private Integer goodsCount;

}
