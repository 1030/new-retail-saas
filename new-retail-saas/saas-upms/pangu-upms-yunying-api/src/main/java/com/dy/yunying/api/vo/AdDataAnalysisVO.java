package com.dy.yunying.api.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pig4cloud.pig.common.core.util.BigDecimalUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Objects;

@Setter
@Getter
@ApiModel("广告数据分析")
public class AdDataAnalysisVO {

	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	// 周期
	private String period;

	@ExcelProperty("广告计划ID")
	@ApiModelProperty("广告计划ID")
	// 广告计划ID
	private String adid;

	@ExcelProperty("广告计划名称")
	@ApiModelProperty("广告计划名称")
	// 广告计划名称
	private String adidName;

	@ApiModelProperty("父游戏id")
	private Long pgid;
	@ExcelProperty("父游戏名称")
	private String parentGameName;

	// 子游戏id
	@ApiModelProperty("子游戏id")
	//	@JsonProperty("gameid")
	private Long gameid;
	@ExcelProperty("子游戏名称")
	private String gameName;

	@ApiModelProperty("是否显示分成前的数据 ：0 否 1：是")
	private Integer showRatio = 0;


	//系统类型
	@ApiModelProperty("系统类型")
//	@JsonProperty("os")
	private Integer os;

	public String getOsStr() {
		String osStr = OsEnum.getName(os);
		return osStr;
	}

	@ExcelProperty("父渠道")
	// 渠道类型
	@ApiModelProperty("父渠道")
	private String parentchl;

	@ExcelProperty("子渠道")
	// 渠道名称
	@ApiModelProperty("子渠道")
	private String chl;

	@ExcelProperty("分包编码")
	// 渠道名称
	@ApiModelProperty("分包编码")
	private String appchl;


	// 部门ID
	private String deptId;

	@ExcelProperty("部门名称")
	@ApiModelProperty("部门名称")
	// 部门
	private String deptName;

	public String getDeptName() {
		if (StringUtils.isBlank(deptName)) {
			return "-";
		}
		return deptName;
	}

	@ApiModelProperty("组别主键")
	private String userGroupId;
	@ExcelProperty("组别名称")
	@ApiModelProperty("组别名称")
	private String userGroupName;

	public String getUserGroupName() {
		if (StringUtils.isBlank(userGroupName)) {
			return "-";
		}
		return userGroupName;
	}

	private String investor;
	@ExcelProperty("投放人姓名")
	// 投放人
	@ApiModelProperty("投放人")
	private String investorName;

	public String getInvestorName() {
		if (StringUtils.isBlank(investorName)) {
			return "-";
		}
		;
		return investorName;
	}


	@ExcelProperty("广告状态描述")
	@ApiModelProperty("广告状态描述")
	private String status;


	@ApiModelProperty("广告操作状态")
	private String optStatus;

	@ApiModelProperty("投放开始时间")
	private String startTime;
	@ApiModelProperty("投放结束时间")
	private String endTime;

	@ExcelProperty("投放时间")
	@ApiModelProperty("投放时间")
	private String scheduleType;


	@ExcelProperty("广告主")
	@ApiModelProperty("广告主")
	private String advertid;

	@ExcelProperty("广告账号名称")
	@ApiModelProperty("广告账号名称")
	private String adAccountName;


	@ExcelProperty("出价(元)")
	@ApiModelProperty("出价(元)")
	private BigDecimal pricing;

	@ExcelProperty("计划预算(元)")
	@ApiModelProperty("计划预算(元)")
	private BigDecimal budget;

	@ExcelProperty("投放方式")
	@ApiModelProperty("投放方式")
	private String unionVideoType;

	/**
	 * 投放范围 ： 通投智选(UNIVERSAL)  默认(DEFAULT) 穿山甲(UNION)
	 */
	@ExcelProperty("投放范围")
	@ApiModelProperty("投放范围")
	private String deliveryRange;

	@ApiModelProperty("转化目标")
	private String convertName;

	@ExcelProperty("转化目标")
	@ApiModelProperty("转化目标")
	private String convertDescri;


	@ApiModelProperty("深化目标")
	private String deepConvert;

	@ExcelProperty("转化目标")
	@ApiModelProperty("深度转化目标描述")
	private String deepConvertDescri;

	@ApiModelProperty("转化目标id")
	private String convertId;

	@ExcelProperty("投放位置")
	@ApiModelProperty("投放位置")
	private String inventoryType;

	@ApiModelProperty("广告组id")
	private String campaignId;

	@ExcelProperty("广告组名称")
	@ApiModelProperty("广告组名称")
	private String campaignName;

	@ApiModelProperty("创建时间")
	private String createTime;

	@ApiModelProperty("修改时间")
	private String updateTime;

	@ApiModelProperty("平台类型：1头条 7 uc  8 广点通 9 ")
	private String ctype;

	@ExcelProperty("展示数")
	@ApiModelProperty("展示数")
	@JsonProperty("showNums")
	// 展示数
	private Integer shownums = 0;

	// 点击数
	@ExcelProperty("点击数")
	@ApiModelProperty("点击数")
	private Integer clicknums = 0;

	@ExcelProperty("点击率")
	@ApiModelProperty("点击率")
	// 点击率   点击数/展示数
	private BigDecimal clickRatio = new BigDecimal(0);

	@ExcelProperty("点击激活率")
	@ApiModelProperty("点击激活率")
	// 点击激活率   新增设备数/点击数
	private BigDecimal clickActiveRatio = new BigDecimal(0);

	//新增分成后
	private BigDecimal newdevicesharfee = new BigDecimal(0);
	// 周分成后
	private BigDecimal weeksharfee = new BigDecimal(0);
	// 月分成后
	private BigDecimal monthsharfee = new BigDecimal(0);
	// 累计分成后
	private BigDecimal totalPaysharfee = new BigDecimal(0);

	// 活跃分成后
	private BigDecimal activesharfee = new BigDecimal(0);


	@ExcelProperty("新增设备数")
	//新增设备数
	@ApiModelProperty("新增设备数")
//	@JsonProperty("uuidNums")
	private Integer uuidnums = 0;

	@ExcelProperty("新增设备注册数")
	//新增设备注册数(新用户注册)
	@ApiModelProperty("新增设备注册数")
	private Integer usrnamenums = 0;


	@ExcelProperty("激活注册率")
	@ApiModelProperty("激活注册率")
	// 点击率   点击数/展示数
	private BigDecimal activationRatio = new BigDecimal(0);


	@ExcelProperty("新增充值设备数")
	@ApiModelProperty("新增充值设备数")
	private Integer paydeviceAll = 0;

	@ExcelProperty("新增充值金额(现金)")
	// 新增充值金额
	@ApiModelProperty("新增充值金额(现金)")
	private BigDecimal userfeeAll = new BigDecimal(0);

	@ExcelProperty("新增充值金额（代金券）")
	// 新增充值金额
	@ApiModelProperty("新增充值金额（代金券）")
	private BigDecimal givemoneyAll = new BigDecimal(0);

	@ExcelProperty("新增充值金额")
	// 新增充值金额
	@ApiModelProperty("新增充值金额")
	private BigDecimal newPayAmount = new BigDecimal(0);

	public BigDecimal getNewPayAmount() {
		if (showRatio == 0) {
			newPayAmount = new BigDecimal(0).add(userfeeAll).add(givemoneyAll);
		}
		if (showRatio == 1) {
			newPayAmount = totalPaysharfee;
		}
		return newPayAmount;
	}

	@ExcelProperty("首日新增充值金额")
	@ApiModelProperty("首日新增充值金额")
	@JsonProperty("worth1")
	private BigDecimal worth1 = new BigDecimal(0); // 首日 新增充值金额

	public BigDecimal getWorth1() {
		if (showRatio == 0) {
			worth1 = worth1;
		}
		if (showRatio == 1) {
			worth1 = getNewdevicesharfee();
		}
		return worth1;
	}

	private Integer paydevice1 = 0; // 首日 新增充值设备数


	@ExcelProperty("当周充值金额")
	@ApiModelProperty("当周充值金额")
	private BigDecimal worth7 = new BigDecimal(0); //7天累计 新增充值金额

	public BigDecimal getWorth7() {
		if (showRatio == 0) {
			worth7 = worth7;
		}
		if (showRatio == 1) {
			worth7 = getWeeksharfee();
		}
		return worth7;
	}

	private Integer paydevice7 = 0; // 首日 新增充值设备数


	@ExcelProperty("当月充值金额")
	@ApiModelProperty("当月充值金额")
	private BigDecimal worth30 = new BigDecimal(0); //30天累计 新增充值金额

	public BigDecimal getWorth30() {
		if (showRatio == 0) {
			worth30 = worth30;
		}
		if (showRatio == 1) {
			worth30 = getMonthsharfee();
		}
		return worth30;
	}

	private Integer paydevice30 = 0; // 首日 新增充值设备数


	@ExcelProperty("活跃设备数")
	@ApiModelProperty("活跃设备数")
	private Integer activeNum = 0;

	@ExcelProperty("活跃用户数")
	@ApiModelProperty("活跃用户(有登录) ")
	private Integer activeLoginNum = 0;

	@ExcelProperty("活跃充值设备数")
	// 充值金额
	@ApiModelProperty("活跃充值设备数")
	private BigDecimal paydevice = new BigDecimal(0);

	@ExcelProperty("活跃充值现金")
	// 充值金额
	@ApiModelProperty("活跃充值现金")
	private BigDecimal userfee = new BigDecimal(0);

	@ExcelProperty("活跃充值代金券")
	// 充值金额
	@ApiModelProperty("活跃充值代金券")
	private BigDecimal givemoney = new BigDecimal(0);

	@ExcelProperty("活跃充值金额")
	// 充值金额
	@ApiModelProperty("活跃充值金额")
	private BigDecimal payAmount = new BigDecimal(0);

	public BigDecimal getPayAmount() {
		if (showRatio == 0) {
			payAmount = new BigDecimal(0).add(userfee).add(givemoney);
		}
		if (showRatio == 1) {
			payAmount = activesharfee;
		}
		return payAmount;
	}


	@ExcelProperty("活跃ARPU")
	//	活跃ARPU= 活跃充值金额/活跃设备数
	@ApiModelProperty("活跃ARPU")
	private BigDecimal actarpu = new BigDecimal(0);

	public BigDecimal getActarpu() {
		actarpu = BigDecimalUtil.divide(getPayAmount(), new BigDecimal(activeNum));
		return actarpu;
	}

	public BigDecimal getActivationRatio() {
		activationRatio = BigDecimalUtil.getRatio(usrnamenums, uuidnums);
		return activationRatio;
	}


	private Integer retention2 = 0; //设备次日留存数

	// 次留: 新增设备在次日有登录行为的设备数/新增设备注册数
	@ExcelProperty("次留")
	@ApiModelProperty("次留")
	@JsonProperty("retention2Ratio")
	private BigDecimal retention2Ratio = new BigDecimal(0);

	public BigDecimal getRetention2Ratio() {
		retention2Ratio = BigDecimalUtil.getRatio(retention2, usrnamenums);
		return retention2Ratio;
	}

	/**
	 * 点击率
	 *
	 * @return
	 */
	public BigDecimal getClickRatio() {
		clickRatio = BigDecimalUtil.getRatio(clicknums, shownums);
		return clickRatio;
	}

	/**
	 * 点击率激活率
	 *
	 * @return
	 */
	public BigDecimal getClickActiveRatio() {
		clickActiveRatio = BigDecimalUtil.getRatio(uuidnums, clicknums);
		return clickActiveRatio;
	}

	@ExcelProperty("点击注册率")
	@ApiModelProperty("点击注册率")
	// 点击注册率  注册数/点击数
	private BigDecimal regRatio = new BigDecimal(0);

	public BigDecimal getRegRatio() {
		regRatio = BigDecimalUtil.getRatio(usrnamenums, clicknums);
		return regRatio;
	}

	@ExcelProperty("消耗")
	@ApiModelProperty("消耗后消耗（总成本）")
	@JsonProperty("cost")
	private BigDecimal cost = new BigDecimal(0);

	// 返点后消耗（总成本）
	@ExcelProperty("返点前消耗")
	@ApiModelProperty("返点前消耗")
	private BigDecimal rudecost = new BigDecimal(0);

	@ExcelProperty("设备成本")
	@ApiModelProperty("设备成本")
	// 设备成本  消耗/新增设备注册数
	private BigDecimal deviceCose = new BigDecimal(0);

	public BigDecimal getDeviceCose() {
		regRatio = BigDecimalUtil.divide(cost, new BigDecimal(usrnamenums));
		return regRatio;
	}

	@ExcelProperty("新增arpu")
	@ApiModelProperty("新增arpu ")
	//注册arpu  新增充值金额/新增设备数
	private BigDecimal regarpu = new BigDecimal(0);

	public BigDecimal getRegarpu() {
		if (showRatio == 0) {
			regarpu = BigDecimalUtil.divide(worth1, new BigDecimal(usrnamenums));
		}
		if (showRatio == 1) {
			regarpu = BigDecimalUtil.divide(newdevicesharfee, new BigDecimal(usrnamenums));
		}
		return regarpu;
	}

	@ExcelProperty("新增付费率")
	@ApiModelProperty("新增付费率 ")
	//新增付费率  新增设备付费数/新增设备注册数
	private BigDecimal regPayRatio = new BigDecimal(0);

	public BigDecimal getRegPayRatio() {
		regPayRatio = BigDecimalUtil.getRatio(paydevice1, usrnamenums);
		return regPayRatio;
	}

	@ExcelProperty("首日ROI")
	@ApiModelProperty("首日ROI  ")
	// roi1
	private BigDecimal roi1 = new BigDecimal(0);

	public BigDecimal getRoi1() {
		if (showRatio == 0) {
			roi1 = BigDecimalUtil.getRatio(worth1, cost);
		}
		if (showRatio == 1) {
			roi1 = BigDecimalUtil.getRatio(newdevicesharfee, cost);
		}
		return roi1;
	}

	@ExcelProperty("当周ROI")
	@ApiModelProperty("当周ROI ")
	private BigDecimal weekRoi = new BigDecimal(0);

	public BigDecimal getWeekRoi() {
		if (showRatio == 0) {
			weekRoi = BigDecimalUtil.getRatio(worth7, cost);
		}
		if (showRatio == 1) {
			weekRoi = BigDecimalUtil.getRatio(weeksharfee, cost);
		}
		return weekRoi;
	}

	@ExcelProperty("当月ROI")
	@ApiModelProperty("当月ROI ")
	private BigDecimal monthRoi = new BigDecimal(0);

	public BigDecimal getMonthRoi() {
		if (showRatio == 0) {
			monthRoi = BigDecimalUtil.getRatio(worth30, cost);
		}
		if (showRatio == 1) {
			monthRoi = BigDecimalUtil.getRatio(monthsharfee, cost);
		}
		return monthRoi;
	}

	@ExcelProperty("累计充值ROI")
	@ApiModelProperty("累计充值ROI ")
	//总roi 累计充值金额/总消耗
	private BigDecimal allRoi = new BigDecimal(0);

	public BigDecimal getAllRoi() {
		allRoi = BigDecimalUtil.getRatio(getNewPayAmount(), cost);
		return allRoi;
	}


	/**
	 * 上面是已处理
	 **/

	// 广告名称
	@ExcelProperty("广告账号名称")
	@ApiModelProperty("广告账号名称")
	private String adname;


	@ApiModelProperty("广告主键id")
	// 广告计划ID
	private String adgroupId;


	@ExcelProperty("游戏")
	// 游戏
	@ApiModelProperty("游戏")
	private String gname;


	@ExcelProperty("父游戏")
	// 父游戏
	@ApiModelProperty("父游戏")
	private String pgame;

	@ExcelProperty("渠道类型")
	// 渠道类型
	@ApiModelProperty("渠道类型")
//	@JsonProperty("parentChlName")
	private String parentchlname;

	@ExcelProperty("分包编码")
	@ApiModelProperty("分包编码")
//	@JsonProperty("packCode")
	private String packCode;

	public String getPeriod() {
		if (StringUtils.isNotBlank(period)) {
			if (period.contains("周")) {
				String periodYear = period.substring(0, 4);
				String periodWeek = period.substring(4, 7);
				return periodYear + "-" + periodWeek;
			}
		}
		return period;
	}
}
