package com.dy.yunying.api.dto;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：lile
 * @date ：2021/6/17 15:09
 * @description：
 * @modified By：
 */
@Data
public class AdDataDto {

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	private String titles;
	private String columns;

	@ApiModelProperty("是否系统管理员")
	private int isSys;

	@ApiModelProperty("设备激活时间（开始）/广告统计查询时间")
	@NotNull(message = "查询开始日期不允许为空")
	private Long sTime;

	@ApiModelProperty("设备激活时间（结束）/广告统计查询时间")
	@NotNull(message = "查询结束日期不允许为空")
	private Long eTime;

	@ApiModelProperty("主渠道")
	private String parentchlArr;

	@ApiModelProperty("分包渠道")
	private String appchlArr;


	@ApiModelProperty("部门id数组,多个用逗号分隔")
	private String deptIdArr;

	@ApiModelProperty("客户端操作系统0=android；1=IOS；3=OTHERS或为空；4=H5; 5=PC")
	private Integer os;


	@ApiModelProperty("投放人数组")
	private String investorArr;

	@ApiModelProperty("子游戏")
	private String gameidArr;

	@ApiModelProperty("父游戏")
	private String pgidArr;

	@ApiModelProperty("周期类型 ：1 按天 2 按周  3  按月 4 年（默认）")
	private Integer cycleType = 4;

	public String getPeriod() {
		String period = "'year'";
		switch (cycleType) {
			case 1:
				period = "day";
				break; //可选
			case 2:
				period = "week";
				//语句
				break; //可选
			case 3:
				period = "month";
				break; //可选
			case 4:
				period = "year";
				break; //可选
			case 5:
				period = "'汇总'";
				break; //可选
			//你可以有任意数量的case语句
			default: //可选
				//语句
		}
		return period;
	}

	@ApiModelProperty("类别 多个用逗号分隔【系统 os,主游戏 pgid，子游戏 gid ,部门 deptId,渠道编码 parentchl，分包编码：appchl,广告计划 adId,  投放人investor   】")
	private String queryColumn;

	@ApiModelProperty("是否显示分成前的数据 ：0 否 1：是")
	private Integer showRatio = 0;


	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	public String getAdAccounts() {
		String adAccounts = "'NO'";  // 默认不查询任何广告账户
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			adAccounts = "'" + StringUtils.join(roleAdAccountList, "','") + "'";
		}
		return adAccounts;
	}

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

	private Boolean enableTest = false;
}
