package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 提现档次与角色关系表
 * @TableName hb_withdraw_role_info
 */
@Data
@Accessors(chain = true)
@TableName(value ="hb_withdraw_role_info")
public class HbWithdrawRoleInfoDO implements Serializable {

	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

    /**
     * 提现档次ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 活动ID
     */
    private Long activityId;

    /**
     * 提现当次ID
     */
    private Long cashConfigId;

    /**
     * 主游戏ID
     */
    private Long pgameId;

    /**
     * 子游戏ID
     */
    private Long gameId;

    /**
     * 主渠道编码
     */
    private String parentchl;

    /**
     * 子渠道编码
     */
    private String chl;

    /**
     * 分包渠道编码
     */
    private String appchl;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 区服ID
     */
    private String areaId;

    /**
     * 角色ID
     */
    private String roleId;

    /**
     * 前置任务完成，0-未完成；1-已完成；
     */
    private Integer dependCompleted;

    /**
     * 当前任务完成，0-未完成；1-已完成；
     */
    private Integer ownedCompleted;

    /**
     * 累计提现次数
     */
    private Integer totalCashCount;

	/**
	 * 累计申请提现次数
	 */
	private Integer cashedApplyCount;

	/**
	 * 累计驳回提现次数
	 */
	private Integer cashedRejectCount;

	/**
	 * 累计驳回提现次数
	 */
	private Integer cashedPassCount;

    /**
     * 是否删除(0否 1是)
     */
    private Integer deleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private Long createId;

    /**
     * 修改人
     */
    private Long updateId;

}