package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName HbActivityTypeEnum.java
 * @createTime 2021年10月26日 10:35:00
 */
public enum HbRedpackTypeEnum {
	CASH("1", "现金红包"),
	GIFT("2", "礼包码"),
	GOODS("3", "实物礼品"),
	VOUCHER("4", "代金券"),
	BALANCE("5", "游豆");;

	private String type;
	private String name;
	public String getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private HbRedpackTypeEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(String type) {
		if (StringUtils.isBlank(type)){
			return null;
		}
		for (HbRedpackTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return null;
	}
}
