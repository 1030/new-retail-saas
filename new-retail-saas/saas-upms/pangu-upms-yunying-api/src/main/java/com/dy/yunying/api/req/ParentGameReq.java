package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author ：lile
 * @date ：2021/5/29 15:24
 * @description：
 * @modified By：
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ParentGameReq extends Page {

	/**
	 * 主键集合
	 */
	private Collection<Long> ids;

	/**
	 *
	 */
	private Long id;

	/**
	 * 游戏名称
	 */
	private String gname;

	/**
	 * 游戏币汇率
	 */
	private BigDecimal exchangeRate;

	/**
	 * 游戏分成，0到100
	 */
	private BigDecimal sharing;

	/**
	 * 子游戏数量
	 */
	private Long sgnum;

	/**
	 * 玩家人数
	 */
	private Long playernum;

	/**
	 * 充值金额
	 */
	private BigDecimal recharge;

	/**
	 * 游戏厂商
	 */
	private String gmanufacturer;

	/**
	 * 登录地址
	 */
	private String loginaddress;

	/**
	 * 充值回调
	 */
	private String rechargecallback;

	/**
	 * 上架时间
	 */
	private Date launchtime;

	/**
	 * 包名
	 */
	private String pkName;

	/**
	 * 请求接口秘钥
	 */
	private String queryKey;

	/**
	 * 兑换接口密钥
	 */
	private String exchangeKey;

	/**
	 * 对接、联调游戏ID
	 */
	private Long jointGameId;

	/**
	 * 导出接口密钥
	 */
	private String reportKey;

	/**
	 * 兑换任务类
	 */
	private String exchangeClass;

	/**
	 * 登录接口地址
	 */
	private String loginUrl;

	/**
	 * 兑换接口地址
	 */
	private String exchangeUrl;

	/**
	 * 兑换结果查询接口地址
	 */
	private String exchangeQueryUrl;

	/**
	 *
	 */
	private String roleQueryUrl;

	/**
	 * 状态，1：正常
	 */
	private Integer status;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	public ParentGameReq setIds(Collection<Long> ids) {
		this.ids = ids;
		return this;
	}

	public ParentGameReq setId(Long id) {
		this.id = id;
		return this;
	}

}
