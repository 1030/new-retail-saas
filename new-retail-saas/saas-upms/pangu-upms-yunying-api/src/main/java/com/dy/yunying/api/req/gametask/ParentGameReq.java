package com.dy.yunying.api.req.gametask;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author sunyq
 * @date 2022/6/6 15:13
 */
@Data
public class ParentGameReq implements Serializable {
	private List<Long> parentGameIds;
}
