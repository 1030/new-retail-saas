package com.dy.yunying.api.enums;


/**
 * @author yuwenfeng
 * @description: 维护活动与角色关系事件编码
 * @date 2022/6/21 19:49
 */
public enum ActivityEventEnum {

	ENTER_GAME(1, "进入游戏"),
	LEVEL_CHANGE(2, "角色等级变动"),
	RECHARGE_SUCCESS(3, "支付成功"),
	GAME_TASK_FINISH(4, "游戏任务完成"),
	RECEIVE_REDPACK(5, "领取红包"),

	WITHDRAWAL_APPLY(6, "申请提现"),
	WITHDRAWAL_REJECT(7, "提现拒绝"),
	WITHDRAWAL_AUDIT_PASS(8, "提现审核通过"),
	WITHDRAWAL_DIRECT_PASS(9, "提现免审通过"),

	INKIND_GIFTS_APPLY(10, "实物礼品申请发货"),
	INKIND_GIFTS_REJECTED(11, "实物礼品申请驳回"),
	INKIND_GIFTS_SHIPPED(12, "实物礼品已发货"),

	ACTIVITY_EXPIRING(101, "活动即将过期"),
	ACTIVITY_EXPIRED(102, "活动已过期"),
	;

	private final Integer code;
	private final String name;

	ActivityEventEnum(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}
