package com.dy.yunying.api.enums;

public enum ZnfxCodeEnum {

	RANGE ("投放范围","range"),
	BUDGET("预算类型","budget"),
	APPDOWNLOAD("应用下载方式","appdownload"),
	EXTERNAL ("转化目标","external"),
	ADSEX("受众性别","adsex"),
	ADAGE("人群年龄勾选","adage"),
	INTEREST("行为兴趣","interest"),
	DEVICE("设备类型","device"),
	NETWORK("网络类型","network"),
	OPERATOR("运营商","operator"),
	FILTEREXTERNAL("过滤已转化用户","filterexternal"),
	BID("出价方式","bid"),
	AUTOBID("自动调整出价","autobid"),
	ADTIME("投放时间类型","adtime"),
	DEEPBID("深度优化方式","deepbid"),
	EDUCATION("学历","education"),
	CLICKTIME("点击时间分布","clicktime"),
	FRISTTIME("首日时长统计","fristtime"),
	OS("系统分布","os"),
	AREA("地域分布","area"),
	MODE("机型分布","mode"),
	AGE("年龄分布","age"),
	SEX("性别分布","sex"),
	MEDIO("媒体来源","medio"),
	;

	ZnfxCodeEnum(String name, String code) {
		this.name = name;
		this.code = code;
	}

	private String name;

	private String code;

	public static ZnfxCodeEnum getOneByCode(String code) {
		for (ZnfxCodeEnum codeEnum : ZnfxCodeEnum.values()) {
			if (codeEnum.getCode().equals(code)) {
				return codeEnum;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}
}
