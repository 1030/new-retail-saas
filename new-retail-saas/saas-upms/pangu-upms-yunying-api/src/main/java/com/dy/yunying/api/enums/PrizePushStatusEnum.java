package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * class info
 *状态：1启用，2停用，3 待上线,  4 上线中 , 5 已下线
 * @author sunyouquan
 * @date 2022/5/6 17:03
 */

public enum PrizePushStatusEnum {

	START(1, "启用"),

	STOP(2, "停用"),

	READY(3,"待上线"),

	ACTIVITING(4,"活动中"),

	CLOSE(5,"已下线");

	private Integer type;

	private String name;

	PrizePushStatusEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonCreator
	public static PrizePushStatusEnum getPrizePushStatusEnum(Integer value) {
		for (PrizePushStatusEnum prizePushStatusEnum : values()) {
			if (prizePushStatusEnum.type.equals(value)) {
				return prizePushStatusEnum;
			}
		}
		return null;
	}

	@JsonValue
	public Integer getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}

}
