package com.dy.yunying.api.resp.znfx;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.dy.yunying.api.constant.OsEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pig4cloud.pig.common.core.util.BigDecimalUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

@Data
@ApiModel("广告数据分析")
public class AdPlanDataExportRes {

	@ColumnWidth(15)
	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	private String period;

	@ColumnWidth(30)
	@ExcelProperty("广告计划")
	@ApiModelProperty("计划名称")
	private String adidName;

	@ColumnWidth(20)
	@ExcelProperty("计划ID")
	@ApiModelProperty("计划ID")
	private String adid;

	// 返点后消耗（总成本）
	@ExcelProperty("消耗")
	@ApiModelProperty("返点前消耗")
	private BigDecimal rudeCost = new BigDecimal(0);

	@ExcelProperty("返点后消耗")
	@ApiModelProperty("消耗后消耗（总成本）")
	@JsonProperty("cost")
	private BigDecimal cost = new BigDecimal(0);

	// 点击率   点击数/展示数
	@ExcelProperty("点击率")
	@ApiModelProperty("点击率")
	private String clickRatio;

	// 点击注册率  注册数/点击数
	@ExcelProperty("点击注册率")
	@ApiModelProperty("点击注册率")
	private String regRatio;

	//新增设备数
	@ExcelProperty("激活数")
	@ApiModelProperty("新增设备数")
	private Integer uuidnums = 0;

	//新增设备注册数(新用户注册)
	@ExcelProperty("新增设备注册数")
	@ApiModelProperty("新增设备注册数")
	private Integer usrnamenums = 0;

	// 点击率   点击数/展示数
	@ExcelProperty("激活注册率")
	@ApiModelProperty("激活注册率")
	private String activationRatio;

	// 新增注册创角数
	@ExcelProperty("创角数")
	@ApiModelProperty("新增注册创角数")
	private Integer createRoleCount;

	// 新增创角率
	@ExcelProperty("注册创角率")
	@ApiModelProperty("新增创角率")
	private String createRoleRate;

	// 新增注册实名数
	@ExcelProperty("注册实名数")
	@ApiModelProperty("注册实名数")
	private Integer certifiedCount;

	// 注册未实名数
	@ExcelProperty("注册未实名数")
	@ApiModelProperty("注册未实名数")
	private Integer notCertifiedCount;

	// 新增实名制转化率
	@ExcelProperty("注册实名率")
	@ApiModelProperty("新增实名制转化率")
	private String certifiedRate;

	// 设备成本  消耗/新增设备注册数
	@ExcelProperty("设备成本")
	@ApiModelProperty("设备成本")
	private BigDecimal deviceCose = new BigDecimal(0);

	// 首日 新增充值设备数
	@ExcelProperty("首日付费数")
	@ApiModelProperty("首日新增充值设备数")
	private Integer paydevice1 = 0;

	//首日付费成本	返点后消耗/新增付费数
	@ExcelProperty("首日付费成本")
	@ApiModelProperty("首日付费成本")
	private BigDecimal payCose1 = new BigDecimal(0);

	//新增付费率  新增设备付费数/新增设备注册数
	@ExcelProperty("首日付费率")
	@ApiModelProperty("首日付费率")
	private String regPayRatio;

	@ExcelProperty("首日付费次数")
	@ApiModelProperty("首日付费次数")
	private Integer payNum1 = 0;

	@ExcelProperty("首周付费次数")
	@ApiModelProperty("首周付费次数")
	private Integer payNum7 = 0;

	@ExcelProperty("首日充值实付金额")
	@ApiModelProperty("新增充值实付金额")
	private BigDecimal newPayFee = new BigDecimal(0);

	// 新增充值代金券金额
	@ExcelProperty("首日充值代金券金额")
	@ApiModelProperty("新增充值代金券金额")
	private BigDecimal newPayGivemoney = new BigDecimal(0);

	//注册arpu  新增充值金额/新增设备数
	@ExcelProperty("首日ARPPU")
	@ApiModelProperty("新增ARPPU")
	private BigDecimal regarpu = new BigDecimal(0);

	// 新增付费ARPPU
	@ExcelProperty("首日付费ARPPU")
	@ApiModelProperty("新增付费ARPPU")
	private BigDecimal payarppu;

	@ExcelProperty("累计付费数")
	@ApiModelProperty("累计付费数")
	private Integer paydeviceAll = 0;

	//累计付费成本	返点后消耗/累计付费数
	@ExcelProperty("累计付费成本")
	@ApiModelProperty("累计付费成本")
	private BigDecimal payCoseAll = new BigDecimal(0);

	@ExcelProperty("累计付费率")
	@ApiModelProperty("累计付费率")
	private String totalPayRate;

	// 累计充值金额
	@ExcelProperty("累计充值实付金额")
	@ApiModelProperty("累计充值实付金额")
	private BigDecimal totalPayFee = new BigDecimal(0);

	// 累计充值金额
	@ExcelProperty("累计充值代金券金额")
	@ApiModelProperty("累计充值代金券金额")
	private BigDecimal totalPayGivemoney = new BigDecimal(0);

	@ExcelProperty("首日ROI")
	@ApiModelProperty("首日ROI")
	private String roi1;

	//总roi 累计充值金额/总消耗
	@ExcelProperty("累计ROI")
	@ApiModelProperty("累计充值ROI")
	private String allRoi;

	// 次留: 新增设备在次日有登录行为的设备数/新增设备注册数
	@ExcelProperty("次留")
	@ApiModelProperty("次留")
	@JsonProperty("retention2Ratio")
	private String retention2Ratio;

	// 付费次留
	@ExcelProperty("付费次留")
	@ApiModelProperty("付费次留")
	private String payedRetention2;

	@ExcelProperty("当周ROI")
	@ApiModelProperty("当周ROI")
	private String weekRoi;

	@ExcelProperty("当月ROI")
	@ApiModelProperty("当月ROI")
	private String monthRoi;

	@ExcelProperty("当周充值实付金额")
	@ApiModelProperty("当周充值实付金额")
	private BigDecimal weekPayFee = new BigDecimal(0);

	@ExcelProperty("当月充值实付金额")
	@ApiModelProperty("当月充值实付金额")
	private BigDecimal monthPayFee = new BigDecimal(0);

	// 期内设备付费数
	@ExcelProperty("期内充值人数")
	@ApiModelProperty("期内设备付费数")
	private Integer periodPayCount;

	// 期内充值实付金额
	@ExcelProperty("期内充值实付金额")
	@ApiModelProperty("期内充值实付金额")
	private BigDecimal periodPayFee;

	// 期内付费率
	@ExcelProperty("期内付费率")
	@ApiModelProperty("期内付费率")
	private String periodPayRate;

	@ExcelProperty("期内ROI")
	@ApiModelProperty("期内ROI")
	private String periodROI;

	@ExcelProperty("活跃设备数")
	@ApiModelProperty("活跃设备数")
	private Integer activeNum = 0;

	@ExcelProperty("活跃付费数")
	@ApiModelProperty("活跃付费数")
	private BigDecimal activePayCount = new BigDecimal(0);

	// 活跃付费率
	@ExcelProperty("活跃付费率")
	@ApiModelProperty("活跃付费率")
	private String activePayRate;

	@ExcelProperty("活跃付费次数")
	@ApiModelProperty("活跃付费次数")
	private Integer payNum = 0;

	@ExcelProperty("活跃充值实付金额")
	@ApiModelProperty("活跃充值实付金额")
	private BigDecimal activePayFee = new BigDecimal(0);

	@ExcelProperty("活跃充值代金券金额")
	@ApiModelProperty("活跃充值代金券金额")
	private BigDecimal activePayGivemoney = new BigDecimal(0);

	//活跃ARPU= 活跃充值金额/活跃设备数
	@ExcelProperty("活跃ARPU")
	@ApiModelProperty("活跃设备ARPU")
	private BigDecimal actarpu = new BigDecimal(0);

	// 活跃付费ARPPU
	@ExcelProperty("活跃付费ARPPU")
	@ApiModelProperty("活跃付费ARPPU")
	private BigDecimal activearppu;

	// 去重设备数
	@ExcelProperty("去重设备数")
	private Integer deduplicateDeviceCount;

	// 回归设备数
	@ExcelProperty("回归设备数")
	private Integer returnDeviceCount;

	// 重复设备数
	@ExcelProperty("重复设备数")
	private Integer duplicateDeviceCount;

	public BigDecimal getNewPayFee() {
		return BigDecimalUtils.round(newPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getNewPayGivemoney() {
		return BigDecimalUtils.round(newPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getWeekPayFee() {
		return BigDecimalUtils.round(weekPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getMonthPayFee() {
		return BigDecimalUtils.round(monthPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getPeriodPayFee() {
		return BigDecimalUtils.round(periodPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getTotalPayFee() {
		return BigDecimalUtils.round(totalPayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getTotalPayGivemoney() {
		return BigDecimalUtils.round(totalPayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivePayFee() {
		return BigDecimalUtils.round(activePayFee, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivePayGivemoney() {
		return BigDecimalUtils.round(activePayGivemoney, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActarpu() {
		return BigDecimalUtils.round(actarpu, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getActivearppu() {
		return BigDecimalUtils.round(activearppu, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getRegarpu() {
		return BigDecimalUtils.round(regarpu, BigDecimalUtils.DEF_SCALE);
	}

	public BigDecimal getPayarppu() {
		return BigDecimalUtils.round(payarppu, BigDecimalUtils.DEF_SCALE);
	}

}
