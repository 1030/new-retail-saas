package com.dy.yunying.api.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Date;

@Slf4j
@Data
public class ImportCdkDTO {

	/**
	 * 代金券金额
	 */
	@ExcelProperty(index = 0)
	private BigDecimal cdkAmount;

	/**
	 * 满可用金额
	 */
	@ExcelProperty(index = 1)
	private BigDecimal cdkLimitAmount;

	/**
	 * 发放数量
	 */
	@ExcelProperty(index = 2)
	private Integer cdkCount;

	/**
	 * 代金券名称
	 */
	@ExcelProperty(index = 3)
	private String cdkName;

	/**
	 * 主游戏ID
	 */
	@ExcelProperty(index = 4)
	private Long pgid;

	/**
	 * 开始生效时间
	 */
	@ExcelProperty(index = 5)
	@DateTimeFormat("yyyy-MM-dd HH:mm:ss")
	private Date startTime;

	/**
	 * 结束生效时间
	 */
	@ExcelProperty(index = 6)
	@DateTimeFormat("yyyy-MM-dd HH:mm:ss")
	private Date endTime;

	/**
	 * 用户名/用户账号
	 */
	@ExcelProperty(index = 7)
	private String username;

	/**
	 * 发放人
	 */
	@ExcelProperty(index = 8)
	private String grantUser;

	/**
	 * 备注
	 */
	@ExcelProperty(index = 9)
	private String remark;

	private Long userId;

}
