package com.dy.yunying.api.resp.prize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 奖励与区服关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:36
 * table: prize_push_area
 */
@Data
public class PrizePushAreaRes implements Serializable {

	@ApiModelProperty(value = "主键ID")
	private Long id;

	@ApiModelProperty(value = "奖励推送ID")
	private Long prizePushId;

	@ApiModelProperty(value = "主游戏ID")
	private Long parentGameId;

	@ApiModelProperty(value = "区服ID")
	private Long areaId;

	@ApiModelProperty(value = "区服层级(1主游戏 2区服)")
	private Integer areaRange;
}


