package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 奖励类型：1代金券，2礼包码，3游豆，4卡密
 * @author sunyouquan
 * @date 2022/5/5 10:49
 */

public enum PrizeTypeEnum {

	VOUCHER(1, "代金券"),

	BAGCODE(2, "礼包码"),

	SWIMBEAN(3, "游豆"),

	CAMILO(4, "卡密");

	private Integer type;

	private String name;

	PrizeTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonCreator
	public static PrizeTypeEnum getPrizeTypeEnum(Integer value) {
		for (PrizeTypeEnum prizeTypeEnum : values()) {
			if (prizeTypeEnum.type.equals(value)) {
				return prizeTypeEnum;
			}
		}
		return null;
	}

	@JsonValue
	public Integer getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}

}
