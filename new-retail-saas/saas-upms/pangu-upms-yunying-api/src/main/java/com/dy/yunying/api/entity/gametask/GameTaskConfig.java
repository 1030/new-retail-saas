package com.dy.yunying.api.entity.gametask;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 游戏任务配置表
 * @TableName game_task_config
 */
@Data
public class GameTaskConfig implements Serializable {
    /**
     * 主键id
     */
	@TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 主游戏id
     */
	@TableField(value = "parent_game_id")
    private Long parentGameId;

    /**
     * 游戏app_id
     */
	@TableField(value = "app_id")
    private String appId;

    /**
     * 任务id
     */
	@TableField(value = "task_id")
    private String taskId;

    /**
     * 任务描述
     */
	@TableField(value = "task_description")
    private String taskDescription;

    /**
     * 是否删除  0否 1是
     */
	@TableLogic
	@TableField(value = "deleted")
    private Integer deleted;

    /**
     * 创建时间
     */
	@TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
	@TableField(value = "update_time")
    private Date updateTime;

    /**
     * 创建人
     */
	@TableField(value = "create_id")
    private Long createId;

    /**
     * 修改人
     */
	@TableField(value = "update_id")
    private Long updateId;

    private static final long serialVersionUID = 1L;


}