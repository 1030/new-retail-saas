package com.dy.yunying.api.req.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 发放代金券
 * @date 2021/11/16 11:08
 */
@Data
@Accessors(chain = true)
public class GrantCdkReq implements Serializable {

	@ApiModelProperty(value = "CDK码")
	@NotBlank(message = "CDK码不能为空")
	@Length(min = 1, max = 50, message = "CDK码字符长度不合法")
	private String cdkCode;

	@ApiModelProperty(value = "发放类型(1:账号 2：角色)")
	@NotNull(message = "发放类型不能为空")
	private Integer grantType;

	@ApiModelProperty(value = "账号")
	private String belongsName;

	@ApiModelProperty(value="父游戏ID")
	private Long pgid;

	@ApiModelProperty(value = "区服ID")
	private Long areaId;

	@ApiModelProperty(value = "角色ID")
	private String roleId;

}
