package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReward;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 游戏预约进度档位表
 * @author  leisw
 * @version  2022-01-17 17:14:29
 * table: game_appointment_schedule_gears
 */
@Data
public class GameAppointmentScheduleGearsPage{
	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	@TableField(value = "type")
	private String type;
	/**
	 * 预约游戏名称
	 */
	@TableField(value = "game_name")
	private String gameName;
	/**
	 * 档位规定人数
	 */
	@TableField(value = "rule_number")
	private Long ruleNumber;
	/**
	 * 奖励内容list
	 */
	List<GameAppointmentScheduleGearsReward> scheduleGearsRewards;
}






