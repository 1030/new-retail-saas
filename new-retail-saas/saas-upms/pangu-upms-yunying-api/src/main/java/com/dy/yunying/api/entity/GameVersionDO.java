package com.dy.yunying.api.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * wan_game_version
 *
 * @author hjl
 * @date 2020-07-21 14:17:19
 */
@Getter
@Setter
public class GameVersionDO {

	private static final long serialVersionUID = 3604499146246680448L;

	/**
	 * 游戏版本ID
	 */
	private Long versionId;

	/**
	 * 母包类型：1-运营母包；2-云微端；
	 */
	private Integer packType;

	/**
	 * 游戏ID
	 */
	private Long gameId;

	/**
	 * 版本编码，数字，如56
	 */
	private Integer code;

	/**
	 * 版本名称，字符串，如：v3.2.5
	 */
	private String name;

	/**
	 * 图标
	 */
	private String icon;

	/**
	 * 版本更新内容
	 */
	private String content;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 文件大小
	 */
	private String size;

	/**
	 * 文件路径
	 */
	private String path;

	/**
	 * 状态：1：正常；2：失效
	 */
	private Integer status;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 父游戏名称
	 */
	private String pgname;
}
