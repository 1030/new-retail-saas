package com.dy.yunying.api.datacenter.export;

import com.alibaba.excel.annotation.ExcelProperty;
import com.dy.yunying.api.constant.OsEnum;
import com.dy.yunying.api.enums.MakeTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;
import org.jpedal.parser.shape.S;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class MaterialExportDataDTO  implements Serializable {

	private static final long serialVersionUID = 8731827235444187248L;

	private static final String DEFAULT_VALUE = "-";

	/**
	 * 周期
	 */
	@ExcelProperty("周期")
	@ApiModelProperty("周期")
	private String period;

	/**
	 * 素材ID
	 */
	@ExcelProperty("素材ID")
	@ApiModelProperty("素材ID")
	private Long materialId;

	/**
	 * 素材名称
	 */
	@ExcelProperty("素材内容")
	@ApiModelProperty("素材内容")
	private String materialName;

	/**
	 * 落地页ID
	 */
	@ExcelProperty("落地页ID")
	@ApiModelProperty("落地页ID")
	private String landingPageId;

	/**
	 * 落地页名称
	 */
	@ExcelProperty("落地页内容")
	@ApiModelProperty("落地页内容")
	private String landingPageName;

	/**
	 * 素材类型
	 */
	@ExcelProperty("素材类型")
	@ApiModelProperty("素材类型")
	private Integer materialType;

	/**
	 * 素材类型名称
	 */
	@ExcelProperty("素材类型")
	@ApiModelProperty("素材类型")
	private String materialTypeStr;

	/**
	 * 落地页类型：TT_GROUP-头条程序化落地页；TT_ORANGE-头条橙子落地页；…
	 */
	@ExcelProperty("落地页类型")
	@ApiModelProperty("落地页类型")
	private String landingPageType;

	/**
	 * 落地页类型名称
	 */
	@ExcelProperty("落地页类型")
	@ApiModelProperty("落地页类型")
	private String landingPageTypeStr;


	/**
	 * 创意者ID
	 */
	@ExcelProperty("创意者ID")
	@ApiModelProperty("创意者ID")
	private Integer creatorId;

	/**
	 * 创意者姓名
	 */
	@ExcelProperty("创意者")
	@ApiModelProperty("创意者")
	private String creatorName;

	/**
	 * 制作者ID
	 */
	@ExcelProperty("制作者ID")
	@ApiModelProperty("制作者ID")
	private Integer makerId;

	/**
	 * 制作者姓名
	 */
	@ExcelProperty("制作者")
	@ApiModelProperty("制作者")
	private String makerName;

	/**
	 * 素材宽度
	 */
	@ExcelProperty("素材宽度")
	@ApiModelProperty("素材宽度")
	private String materialWidth;

	/**
	 * 素材高度
	 */
	@ExcelProperty("素材高度")
	@ApiModelProperty("素材高度")
	private String materialHeight;

	/**
	 * 素材尺寸/像素
	 */
	@ExcelProperty("尺寸")
	@ApiModelProperty("尺寸")
	private String materialPixel;

	/**
	 * 主游戏ID
	 */
	@ExcelProperty("主游戏ID")
	@ApiModelProperty("主游戏ID")
	private Long pgid;

	/**
	 * 主游戏名称
	 */
	@ExcelProperty("主游戏")
	@ApiModelProperty("主游戏")
	private String pgname;

	/**
	 * 制作类型
	 */
	@ExcelProperty("制作类型")
	@ApiModelProperty("制作类型")
	private Integer makeType;

	/**
	 * 制作类型名称
	 */
	@ExcelProperty("制作类型")
	@ApiModelProperty("制作类型")
	private String makeTypeStr;

	/**
	 * 卖点ID
	 */
	@ExcelProperty("卖点ID")
	@ApiModelProperty("卖点ID")
	private Long sellingPointId;

	/**
	 * 卖点名称
	 */
	@ExcelProperty("卖点")
	@ApiModelProperty("卖点")
	private String sellingPointName;

	/**
	 * 素材持续使用天数
	 */
	@ExcelProperty("素材持续使用天数")
	@ApiModelProperty("素材持续使用天数")
	private Long materialUsageDays;

	/**
	 * 素材被使用次数
	 */
	@ExcelProperty("素材被使用次数")
	@ApiModelProperty("素材被使用次数")
	private Long materialUsageNum;

	/**
	 * 素材使用率(%)
	 */
	@ExcelProperty("素材使用率")
	@ApiModelProperty("素材使用率")
	private String materialUsageRate;

	/**
	 * 10%播放数
	 */
	@ExcelProperty("10%播放数")
	@ApiModelProperty("10%播放数")
	private Long play10perNum;

	/**
	 * 10%播放率(%)
	 */
	@ExcelProperty("10%播放率")
	@ApiModelProperty("10%播放率")
	private String play10perRate;

	/**
	 * 25%播放数
	 */
	@ExcelProperty("25%播放数")
	@ApiModelProperty("25%播放数")
	private Long play25perNum;

	/**
	 * 25%播放率(%)
	 */
	@ExcelProperty("25%播放率")
	@ApiModelProperty("25%播放率")
	private String play25perRate;

	/**
	 * 50%播放数
	 */
	@ExcelProperty("50%播放数")
	@ApiModelProperty("50%播放数")
	private Long play50perNum;

	/**
	 * 50%播放率(%)
	 */
	@ExcelProperty("50%播放率")
	@ApiModelProperty("50%播放率")
	private String play50perRate;

	/**
	 * 75%播放数
	 */
	@ExcelProperty("75%播放数")
	@ApiModelProperty("75%播放数")
	private Long play75perNum;

	/**
	 * 75%播放率(%)
	 */
	@ExcelProperty("75%播放率")
	@ApiModelProperty("75%播放率")
	private String play75perRate;

	/**
	 * 95%播放数
	 */
	@ExcelProperty("95%播放数")
	@ApiModelProperty("95%播放数")
	private Long play95perNum;

	/**
	 * 95%播放率(%)
	 */
	@ExcelProperty("95%播放率")
	@ApiModelProperty("95%播放率")
	private String play95perRate;

	/**
	 * 99%播放数
	 */
	@ExcelProperty("99%播放数")
	@ApiModelProperty("99%播放数")
	private Long play99perNum;

	/**
	 * 99%播放率(%)
	 */
	@ExcelProperty("99%播放率")
	@ApiModelProperty("99%播放率")
	private String play99perRate;

	/**
	 * 总播放数
	 */
	@ExcelProperty("播放数")
	@ApiModelProperty("播放数")
	private Long playTotalNum;

	/**
	 * 完播数
	 */
	@ExcelProperty("完播数")
	@ApiModelProperty("完播数")
	private Long playOverNum;

	/**
	 * 完成播放率(%)
	 */
	@ExcelProperty("完播率")
	@ApiModelProperty("完播率")
	private String playOverRate;

	/**
	 * 有效播放数
	 */
	@ExcelProperty("有效播放数")
	@ApiModelProperty("有效播放数")
	private Long playValidNum;

	/**
	 * 有效播放率(%)
	 */
	@ExcelProperty("有效播放率")
	@ApiModelProperty("有效播放率")
	private String playValidRate;

	/**
	 * 有效播放成本
	 */
	@ExcelProperty("有效播放成本")
	@ApiModelProperty("有效播放成本")
	private BigDecimal playValidCost;

	/**
	 * 展示数
	 */
	@ExcelProperty("展示数")
	@ApiModelProperty("展示数")
	private Long showNum;

	/**
	 * 点击数
	 */
	@ExcelProperty("点击数")
	@ApiModelProperty("点击数")
	private Long clickNum;

	/**
	 * 原始消耗
	 */
	@ExcelProperty("原始消耗")
	@ApiModelProperty("原始消耗")
	private BigDecimal rudeCost;

	/**
	 * 返点后消耗
	 */
	@ExcelProperty("返点后消耗")
	@ApiModelProperty("返点后消耗")
	private BigDecimal cost;

	/**
	 * 展示点击率(%)
	 */
	@ExcelProperty("点击率")
	@ApiModelProperty("点击率")
	private String showClickRate;

	/**
	 * 激活数
	 */
	@ExcelProperty("激活设备数")
	@ApiModelProperty("激活设备数")
	private Long activeNum;

	// 重复设备数
	@ExcelProperty("重复设备数")
	@ApiModelProperty("激活设备数")
	private Integer duplicateDeviceCount;

	/**
	 * 点击激活率(%)
	 */
	@ExcelProperty("点击激活率")
	@ApiModelProperty("点击激活率")
	private String clickActiveRate;

	/**
	 * 注册设备数
	 */
	@ExcelProperty("注册设备数")
	@ApiModelProperty("注册设备数")
	private Long registerNum;

	/**
	 * 点击注册率(%)
	 */
	@ExcelProperty("点击注册率")
	@ApiModelProperty("点击注册率")
	private String clickRegisterRate;

	/**
	 * 注册成本
	 */
	@ExcelProperty("注册成本")
	@ApiModelProperty("注册成本")
	private BigDecimal registerCost;

	/**
	 * 新增付费设备数
	 */
	@ExcelProperty("新增付费设备数")
	@ApiModelProperty("新增付费设备数")
	private Long newPayNum;

	/**
	 * 新增付费次留(%)
	 */
	@ExcelProperty("新增付费次留")
	@ApiModelProperty("新增付费次留")
	private String newPayRetention2;

	/**
	 * 新增付费率(%)
	 */
	@ExcelProperty("新增付费率")
	@ApiModelProperty("新增付费率")
	private String newPayRate;

	/**
	 * 新增付费成本
	 */
	@ExcelProperty("新增付费成本")
	@ApiModelProperty("新增付费成本")
	private BigDecimal newPayCost;

	/**
	 * 新增付费实付金额
	 */
	@ExcelProperty("新增付费实付金额")
	@ApiModelProperty("新增付费实付金额")
	private BigDecimal newPayFee;

	/**
	 * 新增付费代金券金额
	 */
	@ExcelProperty("新增付费代金券金额")
	@ApiModelProperty("新增付费代金券金额")
	private BigDecimal newPayGivemoney;

	/**
	 * 累计付费实付金额
	 */
	@ExcelProperty("累计付费实付金额")
	@ApiModelProperty("累计付费实付金额")
	private BigDecimal totalPayFee;

	/**
	 * 累计付费代金券金额
	 */
	@ExcelProperty("累计付费代金券金额")
	@ApiModelProperty("累计付费代金券金额")
	private BigDecimal totalPayGivemoney;

	/**
	 * 累计ROI(%)
	 */
	@ExcelProperty("累计ROI")
	@ApiModelProperty("累计ROI")
	private String totalRoi;

	/**
	 * 首日ROI(%)
	 */
	@ExcelProperty("首日ROI")
	@ApiModelProperty("首日ROI")
	private String firstRoi;

	/**
	 * 次留(%)
	 */
	@ExcelProperty("次留")
	@ApiModelProperty("次留")
	private String retention2;

	/**
	 * 7留(%)
	 */
	@ExcelProperty("7留")
	@ApiModelProperty("7留")
	private String retention7;

	/**
	 * 30留(%)
	 */
	@ExcelProperty("30留")
	@ApiModelProperty("30留")
	private String retention30;



	/**
	 * 素材文件URL
	 */
	@ExcelProperty("素材文件URL")
	@ApiModelProperty("素材文件URL")
	private String materialFileUrl;

	/**
	 * 素材视频封面URL
	 */
	@ExcelProperty("素材视频封面URL")
	@ApiModelProperty("素材视频封面URL")
	private String materialCoverUrl;


	/**
	 * 落地页缩略图URL
	 */
	@ExcelProperty("落地页缩略图URL")
	@ApiModelProperty("落地页缩略图URL")
	private String landingPageUrl;

	/**
	 * 主渠道编码
	 */
	@ExcelProperty("主渠道编码")
	@ApiModelProperty("主渠道编码")
	private String parentchl;

	/**
	 * 主渠道名称
	 */
	@ExcelProperty("主渠道名称")
	@ApiModelProperty("主渠道名称")
	private String parentchlName;

	/**
	 * 系统类型
	 */
	@ExcelProperty("系统类型")
	@ApiModelProperty("系统类型")
	private Integer os;

	/**
	 * 系统名称
	 */
	@ExcelProperty("系统名称")
	@ApiModelProperty("系统名称")
	private String osStr;

	public String getMaterialName() {
		return StringUtils.defaultIfBlank(this.materialName, DEFAULT_VALUE);
	}

	public String getLandingPageName() {
		return StringUtils.defaultIfBlank(this.landingPageName, DEFAULT_VALUE);
	}

	public String getParentchlName() {
		return StringUtils.defaultIfBlank(this.parentchlName, DEFAULT_VALUE);
	}

	public String getOsStr() {
		if (null == this.osStr) {
			this.osStr = null == this.os ? DEFAULT_VALUE : OsEnum.getName(this.os);
		}
		return this.osStr;
	}

	public String getMaterialTypeStr() {
		if (null == this.materialTypeStr) {
			if (null == this.materialType) {
				this.materialTypeStr = DEFAULT_VALUE;
			} else if (1 == this.materialType) {
				this.materialTypeStr = "视频";
			} else if (2 == this.materialType) {
				this.materialTypeStr = "图片";
			} else {
				this.materialTypeStr = DEFAULT_VALUE;
			}
		}
		return this.materialTypeStr;
	}

	public String getLandingPageTypeStr() {
		return StringUtils.defaultIfBlank(this.landingPageTypeStr, DEFAULT_VALUE);
	}

	public String getCreatorName() {
		return StringUtils.defaultIfBlank(this.creatorName, DEFAULT_VALUE);
	}

	public String getMakerName() {
		return StringUtils.defaultIfBlank(this.makerName, DEFAULT_VALUE);
	}

	public String getMaterialPixel() {
		if (null == this.materialPixel) {
			this.materialPixel = StringUtils.isAnyBlank(this.materialWidth, this.materialHeight) ? DEFAULT_VALUE : String.format("%sx%s", this.materialWidth, this.materialHeight);
		}
		return this.materialPixel;
	}

	public String getPgname() {
		return StringUtils.defaultIfBlank(this.pgname, DEFAULT_VALUE);
	}

	public String getMakeTypeStr() {
		return null == this.makeType ? DEFAULT_VALUE : MakeTypeEnum.getName(this.makeType);
	}

	public String getSellingPointName() {
		return StringUtils.defaultIfBlank(this.sellingPointName, DEFAULT_VALUE);
	}

}
