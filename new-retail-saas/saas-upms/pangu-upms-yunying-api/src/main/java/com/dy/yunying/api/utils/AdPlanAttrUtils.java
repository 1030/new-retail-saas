package com.dy.yunying.api.utils;

import com.alibaba.fastjson.JSON;
import com.dy.yunying.api.entity.znfx.ChartData;
import com.dy.yunying.api.entity.znfx.ChartDataDetail;
import com.dy.yunying.api.entity.znfx.ChartVO;
import com.dy.yunying.api.enums.ChartTypeEnum;
import com.dy.yunying.api.enums.ZnfxCodeEnum;
import com.dy.yunying.api.resp.znfx.AdPlanAnalyseRes;
import com.pig4cloud.pig.common.core.util.R;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class AdPlanAttrUtils {

	public static final String OTHER = "其他", UNLIMITED = "不限", UNKNOWN = "未知";

	// 头条 - 投放范围
	public static final Map<String, String> TT_DELIVERY_RANGE = new HashMap<String, String>() {{
		put("UNIVERSAL", "通投智选");
		put("DEFAULT", "默认");
		put("UNION", "穿山甲");
		// 体验版
		put("UNIVERSAL_SMART", "通投智选");
		put("MANUAL", "首选媒体");
	}};
	// 头条 - 预算类型
	public static final Map<String, String> TT_BUDGET_MODE = new HashMap<String, String>() {{
		put("BUDGET_MODE_DAY", "日预算");
		put("BUDGET_MODE_TOTAL", "总预算");
		// 体验版
		put("BUDGET_MODE_INFINITE", "不限");
	}};
	// 头条 - 下载方式
	public static final Map<String, String> TT_DOWNLOAD_TYPE = new HashMap<String, String>() {{
		put("DOWNLOAD_URL", "下载链接");
		put("QUICK_APP_URL", "快应用+下载链接");
		put("EXTERNAL_URL", "落地页链接");
		put("OTHER", "其他");
	}};
	// 头条 - 转化目标
	public static final Map<String, String> TT_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("AD_CONVERT_TYPE_ACTIVE", "激活");
		put("AD_CONVERT_TYPE_ACTIVE_REGISTER", "注册");
		put("AD_CONVERT_TYPE_PAY", "付费");
		put("AD_CONVERT_TYPE_GAME_ADDICTION", "关键行为");
	}};
	// 头条 - 深度转化目标
	public static final Map<String, String> TT_DEEP_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("AD_CONVERT_TYPE_PAY", "付费");
		put("AD_CONVERT_TYPE_NEXT_DAY_OPEN", "次留");
		put("AD_CONVERT_TYPE_GAME_ADDICTION", "关键行为");
		put("AD_CONVERT_TYPE_PURCHASE_ROI", "付费ROI");
		put("AD_CONVERT_TYPE_LT_ROI", "广告变现ROI");
		put("AD_CONVERT_TYPE_UG_ROI", "内广ROI");
		put("DEEP_BID_DEFAULT", "无");
	}};
	// 头条 - 性别
	public static final Map<String, String> TT_GENDER = new HashMap<String, String>() {{
		put("NONE", "不限");
		put("GENDER_UNLIMITED", "不限");
		put("GENDER_MALE", "男");
		put("GENDER_FEMALE", "女");
	}};
	// 头条 - 年龄
	public static final Map<String, String> TT_AGE = new HashMap<String, String>() {{
		put("AGE_BETWEEN_18_23", "18-23岁");
		put("AGE_BETWEEN_24_30", "24-30岁");
		put("AGE_BETWEEN_31_40", "31-40岁");
		put("AGE_BETWEEN_41_49", "41-49岁");
		put("AGE_ABOVE_50", "大于等于50岁");
		put("UNLIMITED", "不限");
		// 体验版
		put("AGE_BETWEEN_18-19", "18-19岁");
		put("AGE_BETWEEN_20-23", "20-23岁");
		put("AGE_BETWEEN_24_30", "24-30岁");
		put("AGE_BETWEEN_31_35", "31-35岁");
		put("AGE_BETWEEN_36_40", "36-40岁");
		put("AGE_BETWEEN_41_45", "41-45岁");
		put("AGE_BETWEEN_46_50", "46-50岁");
		put("AGE_BETWEEN_51_55", "51-55岁");
		put("AGE_BETWEEN_56_59", "56-59岁");
		put("AGE_ABOVE_60", "大于等于60岁");
	}};
	// 头条 - 行为兴趣
	public static final Map<String, String> TT_INTEREST_ACTION_MODE = new HashMap<String, String>() {{
		put("UNLIMITED", "不限");
		put("CUSTOM", "自定义");
		put("RECOMMEND", "系统推荐");
	}};
	// 头条 - 设备类型
	public static final Map<String, String> TT_DEVICE_TYPE = new HashMap<String, String>() {{
		put("MOBILE", "手机");
		put("PAD", "平板");
		put("UNLIMITED", "不限");
	}};
	// 头条 - 网络类型
	public static final Map<String, String> TT_AC = new HashMap<String, String>() {{
		put("WIFI", "WIFI");
		put("2G", "2G");
		put("3G", "3G");
		put("4G", "4G");
		put("5G", "5G");
		put("UNLIMITED", "不限");
	}};
	// 头条 - 运营商
	public static final Map<String, String> TT_CARRIER = new HashMap<String, String>() {{
		put("MOBILE", "移动");
		put("UNICOM", "联通");
		put("TELCOM", "电信");
		put("UNLIMITED", "不限");
	}};
	// 头条 - 过滤已转化用户
	public static final Map<String, String> TT_HIDE_IF_CONVERTED = new HashMap<String, String>() {{
		put("NO_EXCLUDE", "不过滤");
		put("AD", "广告计划");
		put("CAMPAIGN", "广告组");
		put("ADVERTISER", "广告账户");
		put("APP", "APP");
		put("CUSTOMER", "公司账户");
		put("ORGANIZATION", "组织账户");
	}};
	// 头条 - 出价方式(投放场景)
	public static final Map<String, String> TT_SMART_BID_TYPE = new HashMap<String, String>() {{
		put("SMART_BID_CUSTOM", "常规投放");
		put("SMART_BID_CONSERVATIVE", "放量投放");
		// 体验版
		put("CUSTOM", "稳定成本");
		put("NO_BID", "最大转化投放");
		put("UPPER_CONTROL", "控制成本上限");
	}};
	// 头条 - 是否调整自动出价
	public static final Map<String, String> TT_ADJUST_CPA = new HashMap<String, String>() {{
		put("0", "否");
		put("1", "是");
	}};
	// 头条 - 投放时间
	public static final Map<String, String> TT_SCHEDULE_TYPE = new HashMap<String, String>() {{
		put("SCHEDULE_FROM_NOW", "长期投放");
		put("SCHEDULE_START_END", "起止时间");
	}};
	// 头条 - 深度优化方式
	public static final Map<String, String> TT_DEEP_BID_TYPE = new HashMap<String, String>() {{
		put("DEEP_BID_DEFAULT", "不启用，无深度优化");
		put("DEEP_BID_PACING", "自动优化（手动出价方式下）");
		put("DEEP_BID_MIN", "自定义双出价（手动出价方式下）");
		put("SMARTBID", "自动优化（自动出价方式下）");
		put("AUTO_MIN_SECOND_STAGE", "自定义双出价（自动出价方式下）");
		put("ROI_COEFFICIENT", "ROI系数");
		put("ROI_PACING", "ROI系数——自动优化");
		put("ROI_DIRECT_MAIL", "ROI直投");
		put("MIN_SECOND_STAGE", "两阶段优化");
		put("PACING_SECOND_STAGE", "动态两阶段");
		put("BID_PER_ACTION", "每次付费出价");
		put("SOCIAL_ROI", "ROI三出价");
		put("DEEP_BID_TYPE_RETENTION_DAYS", "留存天数");
	}};

	// 头条 - 体验版 - 投放范围
	public static final Map<String, String> TT_EXP_DELIVERY_RANGE = new HashMap<String, String>() {{
		put("UNIVERSAL_SMART", "通投智选");
		put("MANUAL", "首选媒体");
	}};
	// 头条 - 体验版 - 预算类型
	public static final Map<String, String> TT_EXP_BUDGET_MODE = new HashMap<String, String>() {{
		put("BUDGET_MODE_INFINITE", "不限");
		put("BUDGET_MODE_DAY", "日预算");
	}};
	// 头条 - 体验版 - 下载方式
	public static final Map<String, String> TT_EXP_DOWNLOAD_TYPE = new HashMap<String, String>() {{
		put("DOWNLOAD_URL", "直接下载");
		put("EXTERNAL_URL", "落地页下载");
		put("OTHER", "其他");
	}};
	// 头条 - 体验版 - 转化目标
	public static final Map<String, String> TT_EXP_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("AD_CONVERT_TYPE_ACTIVE", "激活");
		put("AD_CONVERT_TYPE_ACTIVE_REGISTER", "注册");
		put("AD_CONVERT_TYPE_PAY", "付费");
		put("AD_CONVERT_TYPE_GAME_ADDICTION", "关键行为");
	}};
	// 头条 - 体验版 - 深度转化目标
	public static final Map<String, String> TT_EXP_DEEP_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("AD_CONVERT_TYPE_PAY", "付费");
		put("AD_CONVERT_TYPE_NEXT_DAY_OPEN", "次留");
		put("AD_CONVERT_TYPE_GAME_ADDICTION", "关键行为");
		put("AD_CONVERT_TYPE_PURCHASE_ROI", "付费ROI");
		put("AD_CONVERT_TYPE_LT_ROI", "广告变现ROI");
		put("AD_CONVERT_TYPE_UG_ROI", "内广ROI");
		put("DEEP_BID_DEFAULT", "无");
	}};
	// 头条 - 体验版 - 性别
	public static final Map<String, String> TT_EXP_GENDER = new HashMap<String, String>() {{
		put("NONE", "不限");
		put("GENDER_UNLIMITED", "不限");
		put("GENDER_MALE", "男");
		put("GENDER_FEMALE", "女");
	}};
	// 头条 - 体验版 - 年龄
	public static final Map<String, String> TT_EXP_AGE = new HashMap<String, String>() {{
		put("AGE_BETWEEN_18_23", "18-23岁");
		put("AGE_BETWEEN_31_40", "31-40岁");
		put("AGE_BETWEEN_41_49", "41-49岁");
		put("AGE_ABOVE_50", "大于等于50岁");
		put("AGE_BETWEEN_18-19", "18-19岁");
		put("AGE_BETWEEN_20-23", "20-23岁");
		put("AGE_BETWEEN_24_30", "24-30岁");
		put("AGE_BETWEEN_31_35", "31-35岁");
		put("AGE_BETWEEN_36_40", "36-40岁");
		put("AGE_BETWEEN_41_45", "41-45岁");
		put("AGE_BETWEEN_46_50", "46-50岁");
		put("AGE_BETWEEN_51_55", "51-55岁");
		put("AGE_BETWEEN_56_59", "56-59岁");
		put("AGE_ABOVE_60", "大于等于60岁");
		put("UNLIMITED", "不限");
	}};
	// 头条 - 体验版 - 行为兴趣
	public static final Map<String, String> TT_EXP_INTEREST_ACTION_MODE = new HashMap<String, String>() {{
		put("UNLIMITED", "不限");
		put("CUSTOM", "自定义");
		put("RECOMMEND", "系统推荐");
	}};
	// 头条 - 体验版 - 设备类型
	public static final Map<String, String> TT_EXP_DEVICE_TYPE = new HashMap<String, String>() {{
		put("MOBILE", "手机");
		put("PAD", "平板");
		put("UNLIMITED", "不限");
	}};
	// 头条 - 体验版 - 网络类型
	public static final Map<String, String> TT_EXP_AC = new HashMap<String, String>() {{
		put("WIFI", "WIFI");
		put("2G", "2G");
		put("3G", "3G");
		put("4G", "4G");
		put("5G", "5G");
		put("UNLIMITED", "不限");
	}};
	// 头条 - 体验版 - 运营商
	public static final Map<String, String> TT_EXP_CARRIER = new HashMap<String, String>() {{
		put("MOBILE", "移动");
		put("UNICOM", "联通");
		put("TELCOM", "电信");
		put("UNLIMITED", "不限");
	}};
	// 头条 - 体验版 - 过滤已转化用户
	public static final Map<String, String> TT_EXP_HIDE_IF_CONVERTED = new HashMap<String, String>() {{
		put("NO_EXCLUDE", "不过滤");
		put("AD", "广告计划");
		put("CAMPAIGN", "广告组");
		put("ADVERTISER", "广告账户");
		put("APP", "APP");
		put("CUSTOMER", "公司账户");
		put("ORGANIZATION", "组织账户");
	}};
	// 头条 - 体验版 - 出价方式(投放场景)
	public static final Map<String, String> TT_EXP_SMART_BID_TYPE = new HashMap<String, String>() {{
		put("CUSTOM", "稳定成本");
		put("NO_BID", "最大转化投放");
		put("UPPER_CONTROL", "控制成本上限");
	}};
	// 头条 - 体验版 - 是否调整自动出价
	public static final Map<String, String> TT_EXP_ADJUST_CPA = new HashMap<String, String>() {{
		put("0", "否");
		put("1", "是");
	}};
	// 头条 - 体验版 - 投放时间
	public static final Map<String, String> TT_EXP_SCHEDULE_TYPE = new HashMap<String, String>() {{
		put("SCHEDULE_FROM_NOW", "长期投放");
		put("SCHEDULE_START_END", "起止时间");
	}};
	// 头条 - 体验版 - 深度优化方式
	public static final Map<String, String> TT_EXP_DEEP_BID_TYPE = new HashMap<String, String>() {{
		put("DEEP_BID_DEFAULT", "不启用，无深度优化");
		put("DEEP_BID_PACING", "自动优化（手动出价方式下）");
		put("DEEP_BID_MIN", "自定义双出价（手动出价方式下）");
		put("SMARTBID", "自动优化（自动出价方式下）");
		put("AUTO_MIN_SECOND_STAGE", "自定义双出价（自动出价方式下）");
		put("ROI_COEFFICIENT", "ROI系数");
		put("ROI_PACING", "ROI系数——自动优化");
		put("ROI_DIRECT_MAIL", "ROI直投");
		put("MIN_SECOND_STAGE", "两阶段优化");
		put("PACING_SECOND_STAGE", "动态两阶段");
		put("BID_PER_ACTION", "每次付费出价");
		put("SOCIAL_ROI", "ROI三出价");
		put("DEEP_BID_TYPE_RETENTION_DAYS", "留存天数");
	}};

	// 广点通 - 投放范围
	public static final Map<String, String> GDT_DELIVERY_RANGE = new HashMap<String, String>() {{
		put("SITE_SET_QZONE", "QQ空间，PC版位");
		put("SITE_SET_MOBILE_MYAPP", "应用宝");
		put("SITE_SET_MOBILE_INNER", "QQ、腾讯看点、腾讯音乐");
		put("SITE_SET_MOBILE_UNION", "优量汇");
		put("SITE_SET_WECHAT", "微信公众号与小程序");
		put("SITE_SET_MOBILE_MYAPP", "应用宝");
		put("SITE_SET_MOBILE_INNER", "QQ、腾讯看点、腾讯音乐");
		put("SITE_SET_TENCENT_NEWS", "腾讯新闻");
		put("SITE_SET_TENCENT_VIDEO", "腾讯视频");
		put("SITE_SET_TENCENT_KUAIBAO", "天天快报");
		put("SITE_SET_MOBILE_YYB", "应用宝");
		put("SITE_SET_PENGYOU", "朋友社区");
		put("SITE_SET_TUAN", "QQ团购");
		put("SITE_SET_MEISHI", "QQ美食");
		put("SITE_SET_PIAO", "QQ票务");
		put("SITE_SET_MAIL", "QQ邮箱");
		put("SITE_SET_PC_UNION", "优量汇，PC版位");
		put("SITE_SET_YINGYONGBAO_PC", "应用宝，PC版位");
		put("SITE_SET_PAIPAISEARCH", "拍拍站内搜索");
		put("SITE_SET_QQSHOP", "QQ商城");
		put("SITE_SET_PAIPAIDAOGOU", "拍拍导购咨询");
		put("SITE_SET_QZONESEARCH", "QQ空间搜索");
		put("SITE_SET_WEBUNION_DELETED", "网站联盟");
		put("SITE_SET_EXPRESSPORTAL", "直通车外投");
		put("SITE_SET_WEIBO", "微博");
		put("SITE_SET_WANGGOU", "QQ网购");
		put("SITE_SET_MOBILE_UNION_DELETED", "移动应用联盟");
		put("SITE_SET_THIRDPARTY", "第三方流量");
		put("SITE_SET_JD_WAICAI", "京东外采流量");
		put("SITE_SET_PCQQ", "PCQQ、QQ空间、腾讯音乐");
		put("SITE_SET_KANDIAN", "QQ浏览器（原腾讯看点）");
		put("SITE_SET_QQ_MUSIC_GAME", "QQ、腾讯音乐及游戏");
		put("SITE_SET_MOMENTS", "微信朋友圈");
		put("SITE_SET_MINI_GAME_WECHAT", "微信小游戏");
		put("SITE_SET_MINI_GAME_QQ", "QQ小游戏");
		put("SITE_SET_MOBILE_GAME", "App游戏");
		put("SITE_SET_QQSHOPPING", "QQ购物");
		put("SITE_SET_CHANNELS", "微信视频号");
		put("SITE_SET_QBSEARCH", "QQ浏览器等");
		put("SITE_SET_WECHAT_PLUGIN", "微信新闻插件");
		put("SITE_SET_WECHAT_SEARCH", "微信搜一搜");
		put("UNKNOWN", "未知");
	}};
	// 广点通 - 预算类型
	public static final Map<String, String> GDT_BUDGET_MODE = new HashMap<String, String>() {{
		put("0", "不限");
		put("1", "日预算");
	}};
	// 广点通 - 下载方式
	public static final Map<String, String> GDT_DOWNLOAD_TYPE = new HashMap<String, String>() {{
		put("1", "落地页下载");
	}};
	// 广点通 - 转化目标
	public static final Map<String, String> GDT_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("OPTIMIZATIONGOAL_CLICK", "点击");
		put("OPTIMIZATIONGOAL_FIRST_PURCHASE", "首次付费");
		put("OPTIMIZATIONGOAL_ECOMMERCE_ORDER", "下单");
		put("OPTIMIZATIONGOAL_APP_ACTIVATE", "App激活");
		put("OPTIMIZATIONGOAL_ONE_DAY_RETENTION", "次日留存");
		put("OPTIMIZATIONGOAL_VIEW_COMMODITY_PAGE", "商品详情页浏览");
		put("OPTIMIZATIONGOAL_APP_REGISTER", "App注册");
		put("OPTIMIZATIONGOAL_PROMOTION_VIEW_KEY_PAGE", "关键页面访问");
		put("OPTIMIZATIONGOAL_ECOMMERCE_CART", "加入购物车");
		put("OPTIMIZATIONGOAL_APP_DOWNLOAD", "App下载");
		put("OPTIMIZATIONGOAL_MOBILE_APP_AD_INCOME", "广告变现");
		put("OPTIMIZATIONGOAL_WITHDRAW_DEPOSITS", "提现");
		put("OPTIMIZATIONGOAL_PRE_CREDIT", "预授信");
		put("OPTIMIZATIONGOAL_APPLY", "完件");
		put("OPTIMIZATIONGOAL_APP_PURCHASE", "付费次数");
	}};
	// 广点通 - 深度转化目标
	public static final Map<String, String> GDT_DEEP_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("GOAL_NONE", "初始值");
		put("GOAL_7DAY_PURCHASE_ROAS", "7 天付费 ROI");
		put("GOAL_15DAY_PURCHASE_ROAS", "15 天付费 ROI");
		put("GOAL_30DAY_PURCHASE_ROAS", "30 天付费 ROI");
		put("GOAL_60DAY_PURCHASE_ROAS", "60 天付费 ROI");
		put("GOAL_30DAY_MONETIZATION_ROAS", "30 天变现 ROI");
		put("GOAL_30DAY_ORDER_ROAS", "下单 ROI");
		put("GOAL_1DAY_PURCHASE_ROAS", "首日付费 ROI");
		put("GOAL_1DAY_MONETIZATION_ROAS", "首日变现 ROI");
		put("GOAL_3DAY_PURCHASE_ROAS", "3 日付费 ROI");
		put("GOAL_3DAY_MONETIZATION_ROAS", "3 日变现 ROI");
		put("GOAL_7DAY_MONETIZATION_ROAS", "7 日变现 ROI");
		put("GOAL_15DAY_MONETIZATION_ROAS", "15 日变现 ROI");
	}};
	// 广点通 - 性别
	public static final Map<String, String> GDT_GENDER = new HashMap<String, String>() {{
		put("UNLIMITED", "不限");
		put("MALE", "男");
		put("FEMALE", "女");
	}};
	// 广点通 - 年龄 - 代码根据min，max自动处理
	public static final Map<String, String> GDT_AGE = new HashMap<String, String>() {{
		put("UNLIMITED", "不限");
	}};
	// 广点通 - 行为兴趣
	public static final Map<String, String> GDT_INTEREST_ACTION_MODE = new HashMap<String, String>() {{
		put("UNLIMITED", "不限");
	}};
	// 广点通 - 设备类型
	public static final Map<String, String> GDT_DEVICE_TYPE = new HashMap<String, String>() {{
		put("UNLIMITED", "不限");
	}};
	// 广点通 - 网络类型
	public static final Map<String, String> GDT_AC = new HashMap<String, String>() {{
		put("WIFI", "无线网络");
		put("NET_2G", "2G网络");
		put("NET_3G", "3G网络");
		put("NET_4G", "4G网络");
		put("NET_5G", "5G网络");
		put("UNLIMITED", "不限");
	}};
	// 广点通 - 运营商
	public static final Map<String, String> GDT_CARRIER = new HashMap<String, String>() {{
		put("CMCC", "中国移动");
		put("CUC", "中国联通");
		put("CTC", "中国电信");
		put("UNLIMITED", "不限");
	}};
	// 广点通 - 过滤已转化用户
	public static final Map<String, String> GDT_HIDE_IF_CONVERTED = new HashMap<String, String>() {{
		put("NO_EXCLUDE", "不过滤");
		put("EXCLUDED_DIMENSION_CAMPAIGN", "同计划广告"); // 没有选择自定义转化行为（excluded_dimension）时，使用该定向出价需要满足是 oCPC、oCPM 广告
		put("EXCLUDED_DIMENSION_UID", "同账号广告"); // 没有选择自定义转化行为（excluded_dimension）时，使用该定向出价需要满足是 oCPC、oCPM 广告
		put("EXCLUDED_DIMENSION_BUSINESS_MANAGER", "同商务管家广告"); // 没有选择自定义转化行为（excluded_dimension）时，使用该定向出价需要满足是 oCPC、oCPM 广告
		put("EXCLUDED_DIMENSION_COMPANY_ACCOUNT", "同主体广告"); // 没有选择自定义转化行为（excluded_dimension）时，使用该定向出价需要满足是 oCPC、oCPM 广告
		put("EXCLUDED_DIMENSION_APP", "同应用"); // 仅当推广目标为应用下载（PROMOTED_OBJECT_TYPE_APP_ANDROID、PROMOTED_OBJECT_TYPE_APP_IOS、PROMOTED_OBJECT_TYPE_APP_ANDROID_UNION）时可以使用
		put("EXCLUDED_DIMENSION_PRODUCT", "同商品"); // 仅当 SDPA 单商品动态商品广告（动态商品广告类型为 SINGLE）时使用。同商品不支持单独设置自定义转化行为（excluded_dimension），默认排除同商品下已下单和已付费用户，不限制出价方式
	}};
	// 广点通 - 出价方式(投放场景)
	public static final Map<String, String> GDT_SMART_BID_TYPE = new HashMap<String, String>() {{
		put("BID_STRATEGY_AVERAGE_COST", "稳定拿量");
		put("BID_STRATEGY_TARGET_COST", "优先拿量");
		put("BID_STRATEGY_PRIORITY_LOW_COST", "优先低成本");
		put("BID_STRATEGY_PRIORITY_CAP_COST", "控制成本上限");
	}};
	// 广点通 - 是否调整自动出价
	public static final Map<String, String> GDT_ADJUST_CPA = new HashMap<String, String>() {{
		put("0", "否");
		put("1", "是");
	}};
	// 广点通 - 投放时间
	public static final Map<String, String> GDT_SCHEDULE_TYPE = new HashMap<String, String>() {{
		put("0", "长期投放");
		put("1", "限制投放");
	}};
	// 广点通 - 深度优化方式
	public static final Map<String, String> GDT_DEEP_BID_TYPE = new HashMap<String, String>() {{
		put("DEEP_OPTIMIZATION_ACTION_TYPE_DOUBLE_GOAL_BID", "双目标出价");
		put("DEEP_OPTIMIZATION_ACTION_TYPE_TWO_STAGE_BID", "两阶段出价");
		put("UNKNOWN", "未知");
	}};

	// 快手 - 投放范围
	public static final Map<String, String> KS_DELIVERY_RANGE = new HashMap<String, String>() {{
		put("1", "优选广告位");
		put("2", "按场景选择广告位-信息流广告");
		put("6", "上下滑大屏广告");
		put("7", "信息流广告");
		put("11", "快看点场景");
		put("24", "激励视频");
		put("UNKNOWN", "未知");
	}};
	// 快手 - 预算类型
	public static final Map<String, String> KS_BUDGET_MODE = new HashMap<String, String>() {{
		put("0", "不限");
		put("1", "统一预算");
		put("2", "分日预算");
	}};
	// 快手 - 下载方式
	public static final Map<String, String> KS_DOWNLOAD_TYPE = new HashMap<String, String>() {{
		put("0", "直接下载");
		put("1", "落地页下载");
	}};
	// 快手 - 转化目标
	public static final Map<String, String> KS_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("0", "未知");
		put("2", "点击转化链接");
		put("10", "曝光");
		put("11", "点击");
		put("31", "下载完成");
		put("53", "提交线索");
		put("109", "电话卡激活");
		put("137", "量房");
		put("180", "激活");
		put("190", "付费");
		put("191", "首日ROI");
		put("348", "有效线索");
		put("383", "授信");
		put("384", "完件");
		put("715", "微信复制");
		put("739", "7日付费次数");
	}};
	// 快手 - 深度转化目标
	public static final Map<String, String> KS_DEEP_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("3", "付费");
		put("7", "次日留存");
		put("10", "完件");
		put("11", "授信");
		put("13", "添加购物车");
		put("14", "提交订单");
		put("15", "购买");
		put("44", "有效线索");
		put("92", "付费roi");
		put("181", "激活后24H次日留存");
		put("0", "无");
	}};
	// 快手 - 性别
	public static final Map<String, String> KS_GENDER = new HashMap<String, String>() {{
		put("0", "不限");
		put("1", "女性");
		put("2", "男性");
	}};
	// 快手 - 年龄
	public static final Map<String, String> KS_AGE = new HashMap<String, String>() {{
		put("18", "18-23岁");
		put("24", "24-30岁");
		put("31", "31-40岁");
		put("41", "41-49岁");
		put("50", "50-100岁");
		put("UNLIMITED", "不限");
	}};
	// 快手 - 行为兴趣
	public static final Map<String, String> KS_INTEREST_ACTION_MODE = new HashMap<String, String>() {{
		put("UNLIMITED", "不限");
	}};
	// 快手 - 设备类型
	public static final Map<String, String> KS_DEVICE_TYPE = new HashMap<String, String>() {{
		put("1", "OPPO");
		put("2", "VIVO");
		put("3", "华为");
		put("4", "小米");
		put("5", "荣耀");
		put("6", "三星");
		put("7", "金立");
		put("8", "魅族");
		put("9", "乐视");
		put("10", "其他");
		put("11", "苹果");
		put("UNLIMITED", "不限");
	}};
	// 快手 - 网络类型
	public static final Map<String, String> KS_AC = new HashMap<String, String>() {{
		put("0", "不限");
		put("1", "WIFI");
		put("2", "移动网络");
		put("UNLIMITED", "不限");
	}};
	// 快手 - 运营商
	public static final Map<String, String> KS_CARRIER = new HashMap<String, String>() {{
		put("UNLIMITED", "不限");
	}};
	// 快手 - 过滤已转化用户
	public static final Map<String, String> KS_HIDE_IF_CONVERTED = new HashMap<String, String>() {{
		put("0", "不限");
		put("1", "广告组");
		put("2", "广告计划");
		put("3", "本账户");
		put("4", "公司主体");
		put("5", "APP");
	}};
	// 快手 - 出价方式(投放场景)
	public static final Map<String, String> KS_SMART_BID_TYPE = new HashMap<String, String>() {{
		put("0", "成本优先");
		put("1", "最大转化");
	}};
	// 快手 - 是否调整自动出价
	public static final Map<String, String> KS_ADJUST_CPA = new HashMap<String, String>() {{
		put("0", "手动出价");
		put("1", "自动出价");
	}};
	// 快手 - 投放时间
	public static final Map<String, String> KS_SCHEDULE_TYPE = new HashMap<String, String>() {{
		put("0", "长期投放");
		put("1", "限制投放");
	}};
	// 快手 - 深度优化方式
	public static final Map<String, String> KS_DEEP_BID_TYPE = new HashMap<String, String>() {{
	}};

	// 百度 - 投放范围
	public static final Map<String, String> BD_DELIVERY_RANGE = new HashMap<String, String>() {{
		put("1", "百度信息流 ");
		put("2", "贴吧");
		put("4", "百青藤");
		put("8", "好看视频");
		put("64", "百度小说");
		put("UNKNOWN", "未知");
	}};
	// 百度 - 预算类型
	public static final Map<String, String> BD_BUDGET_MODE = new HashMap<String, String>() {{
		put("0", "不限");
		put("1", "统一预算");
	}};
	// 百度 - 下载方式
	public static final Map<String, String> BD_DOWNLOAD_TYPE = new HashMap<String, String>() {{
		put("0", "直接下载");
		put("1", "落地页下载");
	}};
	// 百度 - 转化目标
	public static final Map<String, String> BD_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("1", "咨询按钮点击");
		put("2", "电话按钮点击");
		put("3", "表单提交成功");
		put("4", "激活");
		put("5", "表单按钮点击");
		put("6", "下载（预约）按钮点击（小流量）");
		put("10", "购买成功");
		put("14", "订单提交成功");
		put("17", "三句话咨询");
		put("18", "留线索");
		put("19", "一句话咨询");
		put("20", "关键页面浏览");
		put("25", "注册（小流量）");
		put("26", "付费（小流量）");
		put("27", "客户自定义（小流量）");
		put("28", "次日留存（小流量）");
		put("30", "电话拨通");
		put("35", "微信复制按钮点击（小流量）");
		put("41", "申请（小流量）");
		put("42", "授信（小流量）");
		put("45", "商品下单成功");
		put("46", "加入购物车");
		put("47", "商品收藏");
		put("48", "商品详情页到达");
		put("53", "订单核对成功");
		put("54", "收货成功");
		put("56", "到店（小流量）");
		put("57", "店铺调起");
		put("67", "微信调起按钮点击");
		put("68", "粉丝关注成功");
		put("71", "应用调起");
		put("72", "聊到相关业务（小流量）");
		put("73", "回访-电话接通（小流量）");
		put("74", "回访-信息确认（小流量）");
		put("75", "回访-发现意向（小流量）");
		put("76", "回访-高潜成交（小流量）");
		put("77", "回访-成单客户（小流量）");
		put("93", "付费阅读(小流量)");
	}};
	// 百度 - 深度转化目标
	public static final Map<String, String> BD_DEEP_EXTERNAL_ACTION = new HashMap<String, String>() {{
		put("10", "购买成功");
		put("25", "注册（小流量）");
		put("26", "付费（小流量）");
		put("27", "客户自定义（小流量）");
		put("28", "次日留存（小流量）");
		put("42", "授信（小流量）");
		put("45", "商品下单成功");
		put("53", "订单核对成功");
		put("54", "收货成功");
		put("56", "到店（小流量）");
		put("72", "聊到相关业务（小流量）");
		put("73", "回访-电话接通（小流量）");
		put("74", "回访-信息确认（小流量）");
		put("75", "回访-发现意向（小流量）");
		put("76", "回访-高潜成交（小流量）");
		put("77", "回访-成单客户（小流量）");
	}};
	// 百度 - 性别
	public static final Map<String, String> BD_GENDER = new HashMap<String, String>() {{
		put("0", "全部");
		put("1", "女");
		put("2", "男");
	}};
	// 百度 - 年龄
	public static final Map<String, String> BD_AGE = new HashMap<String, String>() {{
		put("0", "全部");
		put("1", "0-18岁");
		put("2", "18-24岁");
		put("3", "25-34岁");
		put("4", "35-44岁");
		put("5", "45-54岁");
		put("6", "55岁及以上");
		put("UNLIMITED", "不限");
	}};
	// 百度 - 行为兴趣
	public static final Map<String, String> BD_INTEREST_ACTION_MODE = new HashMap<String, String>() {{
		put("UNLIMITED", "不限");
		put("500001", "0-3岁训练");
		put("500002", "3-6岁训练");
		put("500101", "小学辅导");
		put("500102", "初中辅导");
		put("500103", "高中辅导");
		put("500104", "艺考培训");
		put("500201", "专升本");
		put("500202", "自考");
		put("500203", "成人高考");
		put("500204", "考研");
		put("500205", "在职MBA");
		put("500301", "雅思/托福");
		put("500302", "留学入学考试");
		put("500303", "美国高考");
		put("500304", "留学申请");
		put("500305", "国际游学");
		put("500401", "少儿英语");
		put("500402", "青少年英语");
		put("500403", "四六级英语考试");
		put("500404", "成人英语");
		put("500405", "其他小语种");
		put("500406", "对外汉语");
		put("500501", "IT培训与考试");
		put("500502", "公务员培训与考试");
		put("500503", "财经考试");
		put("500504", "医学资格培训与考试");
		put("500505", "教师资格考试");
		put("500506", "建造师资格考试");
		put("500507", "厨师培训");
		put("500509", "化妆培训");
		put("500510", "消防工程师培训");
		put("500511", "心理咨询师培训");
		put("500513", "营养师培训");
		put("500514", "影视剪辑培训");
		put("500515", "健康管理师培训");
		put("500516", "法律考试");
		put("500601", "舞蹈培训");
		put("500602", "少儿音乐");
		put("500603", "少儿美术");
		put("510001", "二手房交易");
		put("510002", "新房交易");
		put("510003", "豪宅交易");
		put("510004", "写字楼交易");
		put("510005", "海外房产交易");
		put("510006", "住宅租赁");
		put("510007", "商业租赁");
		put("510008", "房产资讯");
		put("510201", "别墅装修");
		put("510202", "小户型装修");
		put("510203", "装修建材");
		put("520101", "西游");
		put("520102", "战争");
		put("520103", "传奇");
		put("520104", "历史");
		put("520105", "仙侠");
		put("520106", "小说");
		put("520107", "二次元");
		put("520108", "西方魔幻");
		put("520109", "三国");
		put("520201", "休闲益智");
		put("520202", "体育竞技");
		put("520203", "动作");
		put("520204", "卡牌");
		put("520205", "在线棋牌");
		put("520206", "射击");
		put("520207", "角色扮演");
		put("520208", "经营养成");
		put("520209", "酷跑竞速");
		put("520210", "模拟辅助");
		put("520211", "宝石消除");
		put("520212", "儿童游戏");
		put("520213", "策略塔防");
		put("520214", "格斗");
		put("520306", "360");
		put("520307", "3975");
		put("520301", "腾讯");
		put("520302", "网易");
		put("520303", "完美世界");
		put("520304", "宝果科技");
		put("520305", "搜狐畅游");
		put("520308", "诚品软件");
		put("520401", "PC游戏");
		put("520402", "手机游戏");
		put("520403", "游戏机");
		put("520404", "网页游戏");
		put("520405", "网络游戏");
		put("500401", "少儿英语");
		put("500402", "青少年英语");
		put("500403", "四六级英语考试");
		put("500404", "成人英语");
		put("500405", "其他小语种");
		put("500406", "对外汉语");
		put("500501", "IT培训与考试");
		put("500502", "公务员培训与考试");
		put("500503", "财经考试");
		put("500504", "医学资格培训与考试");
		put("500505", "教师资格考试");
		put("500506", "建造师资格考试");
		put("500507", "厨师培训");
		put("500509", "化妆培训");
		put("500510", "消防工程师培训");
		put("500511", "心理咨询师培训");
		put("500513", "营养师培训");
		put("500514", "影视剪辑培训");
		put("500515", "健康管理师培训");
		put("500516", "法律考试");
		put("500601", "舞蹈培训");
		put("500602", "少儿音乐");
		put("500603", "少儿美术");
		put("530101", "MPV");
		put("530102", "SUV");
		put("530103", "轿车");
		put("530104", "皮卡");
		put("530201", "汽油");
		put("530202", "新能源");
		put("530301", "低档车");
		put("530302", "中档车");
		put("530303", "高档车");
		put("530304", "豪华车");
		put("530401", "汽车保险");
		put("530402", "汽车改装");
		put("530403", "汽车用品");
		put("530404", "汽车维修");
		put("530405", "汽车美容");
		put("530901", "二手车抵押");
		put("530902", "购车贷款");
		put("540101", "贷款");
		put("540102", "信用卡");
		put("540103", "理财");
		put("540201", "财险");
		put("540202", "寿险");
		put("540203", "疾病保险");
		put("540301", "期货");
		put("540302", "股票");
		put("540303", "基金");
		put("540304", "债券");
		put("540305", "贵金属");
		put("540306", "外汇");
		put("550301", "飞机票");
		put("550302", "火车票");
		put("550303", "大巴票");
		put("550304", "游轮票");
		put("550501", "高端酒店");
		put("550502", "中端酒店");
		put("550503", "经济酒店");
		put("570101", "办公设备");
		put("570102", "手机通讯");
		put("570103", "电脑整机");
		put("570104", "电脑配件");
		put("570105", "摄影设备");
		put("570106", "手机配件");
		put("570107", "网络设备");
		put("570108", "影音设备");
		put("570109", "厨房电器");
		put("570110", "大家电");
		put("570111", "个人护理电器");
		put("570201", "时尚女装");
		put("570202", "精品男装");
		put("570203", "女鞋");
		put("570204", "男鞋");
		put("570205", "内衣");
		put("570206", "珠宝配饰");
		put("570207", "箱包皮具");
		put("570208", "手表");
		put("570209", "奢侈品");
		put("570301", "日用百货");
		put("570302", "厨具餐具");
		put("570305", "家具（+办公家具）");
		put("570306", "家纺");
		put("570307", "灯具");
		put("570401", "水果");
		put("570402", "蔬菜");
		put("570403", "肉品");
		put("570404", "水产");
		put("580101", "奶粉");
		put("580102", "辅食");
		put("580201", "孕婴保健");
		put("580202", "母婴护理服务");
		put("580203", "纸尿裤湿巾");
		put("580204", "产后护理");
		put("580205", "宝宝护洗用户");
		put("580206", "宝宝家电");
		put("580207", "宝宝安全");
		put("580208", "宝宝玩乐");
		put("580209", "妈妈用品");
		put("580210", "胎教");
		put("580301", "童装童鞋");
		put("580302", "童车童床");
		put("580303", "儿童摄影");
		put("580304", "儿童玩具");
		put("600101", "面包蛋糕");
		put("600102", "休闲小食");
		put("600103", "快餐");
		put("600104", "火锅");
		put("600105", "烧烤");
		put("600106", "海鲜");
		put("620201", "婚恋型");
		put("620202", "交友型");
		put("620401", "月嫂");
		put("620402", "保姆");
		put("620403", "保洁");
		put("620404", "搬家服务");
		put("620801", "礼品");
		put("620802", "鲜花配送");
		put("630301", "彩妆");
		put("630302", "护肤品");
		put("630401", "美发");
		put("630402", "美甲");
		put("630403", "SPA美体");
		put("650101", "热血动漫");
		put("650102", "动漫资讯");
		put("650103", "国产动漫");
		put("650104", "韩国动漫");
		put("650105", "欧美动漫");
		put("650106", "日本动漫");
		put("650201", "爱情");
		put("650202", "家庭");
		put("650203", "动作");
		put("650204", "犯罪");
		put("650205", "中国近代战争");
		put("650206", "搞笑");
		put("650207", "武侠");
		put("650601", "星娱");
		put("650602", "游戏直播");
		put("650603", "音乐直播");
		put("651601", "都市爽文");
		put("651602", "仙侠玄幻");
		put("651603", "言情");
	}};
	// 百度 - 设备类型
	public static final Map<String, String> BD_DEVICE_TYPE = new HashMap<String, String>() {{
		put("0", "不限");
		put("1", "OPPO");
		put("2", "VIVO");
		put("3", "华为");
		put("4", "小米");
		put("6", "三星");
		put("8", "魅族");
		put("10", "其他");
	}};
	// 百度 - 网络类型
	public static final Map<String, String> BD_AC = new HashMap<String, String>() {{
		put("0", "全部");
		put("1", "WIFI");
		put("2", "移动网络");
	}};
	// 百度 - 运营商
	public static final Map<String, String> BD_CARRIER = new HashMap<String, String>() {{
		put("0", "不限");
		put("1", "中国移动");
		put("2", "中国联通");
		put("3", "中国电信");
	}};
	// 百度 - 过滤已转化用户
	public static final Map<String, String> BD_HIDE_IF_CONVERTED = new HashMap<String, String>() {{
		put("1", "当天");
		put("2", "7天");
		put("3", "一个月");
		put("4", "三个月");
		put("5", "六个月");
	}};
	// 百度 - 出价方式(投放场景)
	public static final Map<String, String> BD_SMART_BID_TYPE = new HashMap<String, String>() {{
		put("1", "普通模式");
		put("4", "放量模式");
	}};
	// 百度 - 是否调整自动出价
	public static final Map<String, String> BD_ADJUST_CPA = new HashMap<String, String>() {{
	}};
	// 百度 - 投放时间
	public static final Map<String, String> BD_SCHEDULE_TYPE = new HashMap<String, String>() {{
		put("0", "长期投放");
		put("1", "限制投放");
	}};
	// 百度 - 深度优化方式
	public static final Map<String, String> BD_DEEP_BID_TYPE = new HashMap<String, String>() {{
	}};
	// 百度 - 学历
	public static final Map<String, String> BD_EDUCATION = new HashMap<String, String>() {{
		put("0", "全部");
		put("1", "大学及以上");
		put("2", "高中及以下");
		put("3", "大专");
	}};

	private static void dealChart(Map<String, Long> dataMap, List<ChartVO> chartList, String title, String target){
		List<ChartDataDetail> detailList = new ArrayList<>();
		for (Map.Entry<String, Long> entry : dataMap.entrySet()) {
			ChartDataDetail data = new ChartDataDetail();
			data.setName(entry.getKey());
			data.setValue(entry.getValue());
			detailList.add(data);
		}
		//if (CollectionUtils.isNotEmpty(detailList)){
			ChartData chartData = new ChartData();
			chartData.setType(ChartTypeEnum.PIE.getCode());
			chartData.setData(detailList);

			List<ChartData> dataList = new ArrayList<>();
			dataList.add(chartData);

			ChartVO chartVO = new ChartVO();
			chartVO.setTitle(title);
			chartVO.setTarget(target);
			chartVO.setSeries(dataList);
			chartList.add(chartVO);
		//}
	}
	private static String dealValue(String value){
		return value.replace("[", "")
				.replace("]", "")
				.replaceAll("\"", "")
				.replaceAll("'","");
	}


	public static void main(String[] args) {
		List<ChartVO> chartList = new ArrayList<>();
		List<AdPlanAnalyseRes> list = new ArrayList<>();
		AdPlanAnalyseRes adPlanAnalyse = new AdPlanAnalyseRes().setDeliveryRange("[SITE_SET_CHANNELS,SITE_SET_TENCENT_VIDEO]").setGender("2").setExternalAction("AD_CONVERT_TYPE_PAY").setDeepExternalAction("AD_CONVERT_TYPE_PURCHASE_ROI")
				;
		list.add(adPlanAnalyse);
		AdPlanAnalyseRes adPlanAnalyse1 = new AdPlanAnalyseRes().setDeliveryRange("[SITE_SET_TENCENT_VIDEO]").setGender("3").setExternalAction("AD_CONVERT_TYPE_PAY").setDeepExternalAction("AD_CONVERT_TYPE_PURCHASE_ROI")
			;
		list.add(adPlanAnalyse1);
		AdPlanAnalyseRes adPlanAnalyse2 = new AdPlanAnalyseRes().setDeliveryRange("UNIVERSAL_SMART").setGender("2").setExternalAction("AD_CONVERT_TYPE_PAY").setDeepExternalAction("")
				.setAge("0");
		list.add(adPlanAnalyse2);
		AdPlanAnalyseRes adPlanAnalyse3 = new AdPlanAnalyseRes().setDeliveryRange("UNKNOWN").setGender("")
				.setAge("");
		list.add(adPlanAnalyse3);

		Map<String, Long> dataMap = list.stream()
				.filter(s -> StringUtils.isNotBlank(s.getAge()))
				.flatMap(s -> Arrays.stream(dealValue(s.getAge()).split(",")))
				.collect(Collectors.groupingBy(s -> GDT_AGE.getOrDefault(s, OTHER), Collectors.counting()));

		Map<String, Long> dataMap0 = new HashMap<>();
		dealChart(dataMap0, chartList, ZnfxCodeEnum.RANGE.getName(), ZnfxCodeEnum.RANGE.getCode());
		dealChart(dataMap, chartList, ZnfxCodeEnum.BUDGET.getName(), ZnfxCodeEnum.BUDGET.getCode());
		System.out.println(JSON.toJSONString(R.ok(chartList)));

		// 广点通 - 年龄
/*		List<String> ageList = new ArrayList<>();
		list.forEach(s -> {
			String age = s.getAge();
			if (StringUtils.isBlank(age) || "UNLIMITED".equals(age)){
				ageList.add("不限");
			}else{
				List<Map> list1 = JSON.parseArray(s.getAge(), Map.class);
				list1.forEach(v ->{
					ageList.add(v.get("min") + "-"+ v.get("max") + "岁");
				});
			}
		});
		Map<String, Long> dataMap = ageList.stream()
				.collect(Collectors.groupingBy(s -> s, Collectors.counting()));
		dealChart(dataMap, chartList, ZnfxCodeEnum.RANGE.getName(), ZnfxCodeEnum.RANGE.getCode());
		System.out.println(JSON.toJSONString(chartList));*/


/*		// 数据统计：[1,2,3]
		Map<String, Long> dataMap = list.stream()
				.filter(s -> StringUtils.isNotBlank(s.getAge()))
				.flatMap(s -> Arrays.stream(dealValue(s.getAge()).split(",")))
				.collect(Collectors.groupingBy(s -> GDT_AGE.getOrDefault(s, OTHER), Collectors.counting()));
		dealChart(dataMap, chartList, ZnfxCodeEnum.RANGE.getName(), ZnfxCodeEnum.RANGE.getCode());
		System.out.println(JSON.toJSONString(chartList));*/

		// 头条 - 投放范围
		/*Map<String, Long> dataMap = list.stream()
				.filter(s -> StringUtils.isNotBlank(s.getDeliveryRange()))
				.collect(Collectors.groupingBy(s -> TT_DELIVERY_RANGE.getOrDefault(s.getDeliveryRange(), OTHER), Collectors.counting()));
		dealChart(dataMap, chartList, ZnfxCodeEnum.RANGE.getName(), ZnfxCodeEnum.RANGE.getCode());
		System.out.println(JSON.toJSONString(R.ok(chartList)));*/

		/*Map<String, Long> dataMap = list.stream()
				.filter(s -> StringUtils.isNotBlank(s.getExternalAction()))
				.collect(Collectors.groupingBy(s -> TT_EXTERNAL_ACTION.getOrDefault(s.getExternalAction(), OTHER)
						+ (StringUtils.isNotBlank(s.getDeepExternalAction()) ? "-" + TT_DEEP_EXTERNAL_ACTION.getOrDefault(s.getDeepExternalAction(), OTHER) : ""),
						Collectors.counting()));
		dealChart(dataMap, chartList, ZnfxCodeEnum.EXTERNAL.getName(), ZnfxCodeEnum.EXTERNAL.getCode());
		System.out.println(JSON.toJSONString(chartList));*/
	}

}
