package com.dy.yunying.api.vo.hbdata;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/19 10:19
 * @description：
 * @modified By：
 */
@Data
public class HbActiveHbProfitVo implements Serializable {

	// 红包按钮点击数
	private Integer hbClickNum;

	// 浮标特效点击数
	private Integer floatClickNum;

	// 收益明细按钮点击数
	private Integer earnClickNum;
}
