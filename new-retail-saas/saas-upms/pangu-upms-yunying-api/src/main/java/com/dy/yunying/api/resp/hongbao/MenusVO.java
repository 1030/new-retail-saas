package com.dy.yunying.api.resp.hongbao;

import lombok.Data;

import java.io.Serializable;

/**
 * class info
 *
 * @author sunyouquan
 * @date 2022/5/10 15:23
 */
@Data
public class MenusVO implements Serializable {
	/**
	 * 菜单id
	 */
	private Long menusId;
	/**
	 * 菜单title
	 */
	private String menusTitle;
	/**
	 * 菜单编码
	 */
	private String menusCode;
}
