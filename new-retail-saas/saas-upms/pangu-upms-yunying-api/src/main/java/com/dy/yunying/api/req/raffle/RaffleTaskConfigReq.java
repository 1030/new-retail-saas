package com.dy.yunying.api.req.raffle;
import io.swagger.annotations.ApiModelProperty;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

/**
 * 抽奖任务配置表
 * @author  chenxiang
 * @version  2022-11-08 15:59:48
 * table: raffle_task_config
 */
@Data
public class RaffleTaskConfigReq extends Page {

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "活动ID")
	private String activityId;

	@ApiModelProperty(value = "任务名称/目标")
	private String taskName;

	@ApiModelProperty(value = "任务达成条件类型：1-角色等级，2-角色等级和创角时间，3-角色累计充值，4-角色首次充值，5-角色单笔充值满额，6-邀请好友成功注册游戏，7-邀请好友首次提现成功，8-邀请好友累计充值，9-邀请好友创角N天内达到N级，11-角色单笔充值等额，12-角色连续登录N天，13-创角N天累计充值X元，14-创建N天首次充值，15-开服N天达到X级，16-开服N天累计充值X元，17-开服N天首次充值，18-游戏任务，19-角色累计登录N天；20-红包活动提现N次；")
	private String conditionType;

	@ApiModelProperty(value = "任务达成条件：角色等级")
	private String conditionRoleLevel;

	@ApiModelProperty(value = "任务达成条件：创角天数/开服天数；")
	private String conditionRoleDays;

	@ApiModelProperty(value = "任务达成条件：角色充值金额")
	private String conditionRoleRecharge;

	@ApiModelProperty(value = "游戏任务ID")
	private String conditionTaskId;

	@ApiModelProperty(value = "条件达成次数")
	private String conditionReachNum;

	@ApiModelProperty(value = "任务可完成次数")
	private String conditionLimitNum;

	@ApiModelProperty(value = "奖励类型：1-抽奖次数；")
	private String rewardType;

	@ApiModelProperty(value = "奖励数量")
	private String rewardNum;

	@ApiModelProperty(value = "是否删除：0-否；1-是；")
	private String deleted;

	@ApiModelProperty(value = "创建时间")
	private String createTime;

	@ApiModelProperty(value = "修改时间")
	private String updateTime;

	@ApiModelProperty(value = "创建人")
	private String createId;

	@ApiModelProperty(value = "修改人")
	private String updateId;
}
