package com.dy.yunying.api.req.prize;

import io.swagger.annotations.ApiModelProperty;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

/**
 * 奖励与区服关系表
 *
 * @author chenxiang
 * @version 2022-04-26 10:20:36 table: prize_push_area
 */
@Data
public class PrizePushAreaReq extends Page {

	@ApiModelProperty(value = "主键ID")
	private String id;

	@ApiModelProperty(value = "奖励推送ID")
	private String prizePushId;

	@ApiModelProperty(value = "主游戏ID")
	private String parentGameId;

	@ApiModelProperty(value = "区服ID")
	private String areaId;

	@ApiModelProperty(value = "区服层级(1主游戏 2区服)")
	private String areaRange;

}
