package com.dy.yunying.api.dto.currency;

import com.alibaba.fastjson.annotation.JSONField;
import com.dy.yunying.api.enums.CurrencyChangeTypeEnum;
import com.dy.yunying.api.enums.CurrencySourceTypeEnum;
import com.dy.yunying.api.enums.CurrencyTypeEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pig4cloud.pig.common.core.jackson.BigDecimalSerializer;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 账号平台币明细
 * @TableName user_currency_record
 *
 * @Author: hejiale
 * @version  2022-3-25 16:08:56
 */
@Data
public class UserCurrencyRecordDto implements Serializable {
	/**
	 * 主键id
	 */
	private Long id;

	/**
	 * 用户ID
	 */
	private Long userid;

	/**
	 * 用户账号
	 */
	private String username;

	/**
	 * 游豆类型。1：普通；2：临时
	 */
	private Integer currencyType;

	public String getCurrencyTypeStr() {
		return StringUtils.defaultIfBlank(CurrencyTypeEnum.getDescByVal(currencyType), "-");
	}

	/**
	 * 类型。1：增加；2：消耗
	 */
	private Integer type;

	public String getTypeStr() {
		return StringUtils.defaultIfBlank(CurrencyChangeTypeEnum.getDescByVal(type), "-");
	}

	/**
	 * 来源。11：游豆充值；21：游戏订单付费
	 */
	private Integer sourceType;

	public String getSourceTypeStr() {
		return StringUtils.defaultIfBlank(CurrencySourceTypeEnum.getDescByVal(sourceType), "");
	}

	/**
	 * 来源业务ID1
	 */
	private String sourceId1;

	/**
	 * 来源业务名称1
	 */
	private String sourceName1;

	/**
	 * 来源业务ID2
	 */
	private String sourceId2;

	/**
	 * 来源业务名称2
	 */
	private String sourceName2;

	/**
	 * 父游戏ID
	 */
	private Long pgid;
	private String parentGameName;

	/**
	 * 子游戏ID
	 */
	private Long gameId;
	private String gameName;

	/**
	 * 区服ID
	 */
	private String areaId;

	/**
	 * 角色ID
	 */
	private String roleId;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * 金额
	 */
	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal amount;

	/**
	 * 变动前游豆
	 */
	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal beforeBalance;

	/**
	 * 变动后游豆
	 */
	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal afterBalance;

	/**
	 * 详情，JSON字符串
	 */
	private String detail;

	/**
	 * 删除标识，1：删除；0：未删除
	 */
	private Integer deleted;

	/**
	 * IP
	 */
	private String ip;

	/**
	 * 创建人
	 */
	private Long createId;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 修改人
	 */
	private Long updateId;

	/**
	 * 修改时间
	 */
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	public String getSourceIdStr() {
		List<String> sourceIdList = Arrays.asList(StringUtils.trimToNull(sourceId1), StringUtils.trimToNull(sourceId2));
		return StringUtils.join(sourceIdList, "-");
	}

	public String getSourceNameStr() {
		List<String> sourceIdList = Arrays.asList(StringUtils.trimToNull(sourceName1), StringUtils.trimToNull(sourceName2));
		return StringUtils.join(sourceIdList, "-");
	}
}