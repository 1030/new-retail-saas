package com.dy.yunying.api.vo.hongbao;

import com.dy.yunying.api.entity.hongbao.HbReceivingAddress;
import lombok.Data;

/**
 * 实物奖品领取地址记录
 * @author  chenxiang
 * @version  2021-10-25 14:20:46
 * table: hb_receiving_address
 */
@Data
public class HbReceivingAddressVo  extends HbReceivingAddress {
	
	
	
}
