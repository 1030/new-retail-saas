package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 活动中心配置表
 *
 * @TableName hb_activity_center
 */
@Data
@Accessors(chain = true)
@TableName(value = "hb_activity_center")
public class HbActivityCenterDO implements Serializable {

	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 用户群组ID(多个逗号分割)
	 */
	@TableField(value = "user_group_id")
	private String userGroupId;

	/**
	 * 活动名称
	 */
	@TableField(value = "activity_name")
	private String activityName;

	/**
	 * 活动来源：1-红包活动；2-签到活动；3-外部链接; 4-抽奖活动；
	 */
	@TableField(value = "source_type")
	private Integer sourceType;

	/**
	 * 活动类型：1-等级活动；2-充值活动；3-邀请活动；4-定制活动；
	 */
	@TableField(value = "activity_type")
	private Integer activityType;

	/**
	 * 活动ID
	 */
	@TableField(value = "activity_id")
	private Long activityId;

	/**
	 * H5打开链接方式：1-刷新当前页面；2-APP打开全屏新窗口；3-APP外部打开链接；
	 */
	@TableField(value = "open_type")
	private Integer openType;

	/**
	 * APP打开全屏新窗口：1-横屏展示；2-竖屏展示；
	 */
	@TableField(value = "show_type")
	private Integer showType;

	/**
	 * 打开地址
	 */
	@TableField(value = "open_url", updateStrategy = FieldStrategy.IGNORED)
	private String openUrl;

	/**
	 * 推广图片uri
	 */
	@TableField(value = "promote_image_uri")
	private String promoteImageUri;

	/**
	 * 开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "start_time")
	private Date startTime;

	/**
	 * 结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "finish_time")
	private Date finishTime;

	/**
	 * 活动顺序
	 */
	@TableField(value = "activity_order")
	private Integer activityOrder;

	/**
	 * 是否删除：0-否；1-是
	 */
	@TableField(value = "deleted")
	private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

	@ApiModelProperty(value = "活动动态类型ID")
	@TableField(value = "dynamic_type_id")
	private Long dynamicTypeId;

	/*
	 *
	 */

	/*
	 * 非数据库字段
	 */
		/**
	 * 活动状态：1-在线；0-不在线；
	 */
	@TableField(exist = false)
	private Integer activityStatus;

	/**
	 * 跳转到活动链接
	 */
	@TableField(exist = false)
	private String activityUri;

	/**
	 * 排序字段
	 */
	@TableField(exist = false)
	private String orderByName;

	/**
	 * 排序顺序
	 */
	@TableField(exist = false)
	private String orderByAsc;

	/**
	 * 动态展示名称
	 */
	@TableField(exist = false)
	private String showName;

	@ApiModelProperty(value = "游戏数据集合")
	@TableField(exist = false)
	private List<Long[]> gameDataList;

	@ApiModelProperty(value = "区服数据集合")
	@TableField(exist = false)
	private List<Long[]> areaDataList;

	@ApiModelProperty(value = "渠道数据集合")
	@TableField(exist = false)
	private List<String[]> channelDataList;
}