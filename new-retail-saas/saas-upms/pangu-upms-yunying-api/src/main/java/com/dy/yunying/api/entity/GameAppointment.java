package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 游戏预约表
 * @author  kongyanfang
 * @version  2022-01-17 17:14:29
 * table: game_appointment
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "game_appointment")
public class GameAppointment extends Model<GameAppointment>{
	/**
	 * 主键id
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/**
	 * 父游戏ID
	 */
	@TableField(value = "pgid")
	private Long pgid;
	/**
	 * 预约手机号
	 */
	@TableField(value = "mobile")
	private String mobile;
	/**
	 * 预约设备（1：IOS  2：安卓）
	 */
	@TableField(value = "appoint_device")
	private Integer appointDevice;
	/**
	 * 预约来源（1：PC  2：H5）
	 */
	@TableField(value = "appoint_source")
	private Integer appointSource;
	/**
	 * 预约时间
	 */
	@TableField(value = "appoint_time")
	private Date appointTime;

	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	@TableField(value = "type")
	private Integer type;

	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}






