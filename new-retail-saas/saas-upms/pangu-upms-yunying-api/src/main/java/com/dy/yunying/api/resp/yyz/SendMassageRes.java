package com.dy.yunying.api.resp.yyz;

import lombok.Data;

import java.util.Date;

/**
 * 角色查询
 *
 * @author leishaowei
 * @version 2022-03-17 11:06:03
 * table: dwd_gm_player_query
 */
@Data
public class SendMassageRes {

	/***
	 * 预约手机号
	 */
	private String mobile;


}
