package com.dy.yunying.api.vo;

import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 游豆充值配置视图类
 */
@Data
@Accessors(chain = true)
public class CurrencyRechargeConfigVO extends Page<Object> implements Serializable {

	/**
	 * 主键
	 */
	@NotNull(message = "主键不能为空", groups = {Edit.class})
	private Long id;

	/**
	 * 商品名称
	 */
	@NotBlank(message = "商品名称不能为空", groups = {Save.class})
	@Size(min = 1, max = 10, message = "商品名称长度分为必须在不能为空[1~10]之间", groups = {Save.class, Edit.class})
	private String currencyName;

	/**
	 * 游豆数量
	 */
	@NotNull(message = "游豆数量不能为空", groups = {Save.class})
	@Range(min = 0, max = 99999, message = "游豆数量范围必须在[0~99999]之间", groups = {Save.class, Edit.class})
	private BigDecimal currencyAmount;

	/**
	 * 商品金额
	 */
	@NotNull(message = "商品金额不能为空", groups = {Save.class})
	@Range(min = 0, max = 99999, message = "商品金额范围必须在[0~99999]之间", groups = {Save.class, Edit.class})
	private BigDecimal rechargeAmount;

	/**
	 * 使用范围：1-平台通用；2-父游戏；3-子游戏；4-角色；
	 */
	private Integer useScope;

	/**
	 * 使用主游戏
	 */
	private Long usePgameId;

	/**
	 * 使用子游戏
	 */
	private Long useGameId;

	/**
	 * 使用区服
	 */
	private String useAreaId;

	/**
	 * 使用角色
	 */
	private String useRoleId;

	/**
	 * 是否存在角标：0-不存在；1-存在；
	 */
	@NotNull(message = "请必须选择是否存在角标", groups = {Save.class})
	@Range(min = 0, max = 1, message = "是否存在角标只能选择是或者否", groups = {Save.class, Edit.class})
	private Integer marked;

	/**
	 * 角标内容
	 */
	@Size(min = 0, max = 10, message = "角标内容类容长度范围必须在[0~10]之间", groups = {Save.class, Edit.class})
	private String markValue;

	public interface Save {
	}

	public interface Edit {
	}

}
