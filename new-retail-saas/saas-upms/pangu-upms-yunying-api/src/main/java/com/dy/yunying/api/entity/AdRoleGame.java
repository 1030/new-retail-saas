package com.dy.yunying.api.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色游戏用户表
 * @author zhuxm
 *
 */
@Data
@TableName(value="ad_role_game")
public class AdRoleGame implements Serializable {

	private static final long serialVersionUID = -2007506146295854346L;
	/**
	 * 角色ID
	 */
	@ApiModelProperty(value = "角色id")
	private Integer roleId;

	/**
	 * 子游戏ID
	 */
	@ApiModelProperty(value = "子游戏ID")
	private Long gameid;

	/**
	 * 主游戏ID
	 */
	@ApiModelProperty(value = "主游戏ID")
	private Long pgid;


	/**
	 * 类型
	 */
	@ApiModelProperty(value = "类型")
	private Integer type;

	
}