package com.dy.yunying.api.req.sign;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @description: 通知公告搜索
 * @author yuwenfeng
 * @date 2021/12/1 18:24
 */
@Data
public class SelectCommonNoticeReq implements Serializable {

	@ApiModelProperty(value = "开始时间(格式：yyyy-MM-dd)")
	private String startTime;

	@ApiModelProperty(value = "结束时间(格式：yyyy-MM-dd)")
	private String finishTime;

	@ApiModelProperty(value = "公告状态(1待上线 2公告中 3已下线)")
	private Integer popupStatus;

	@ApiModelProperty(value = "公告形式(1弹窗图 2全屏图 3图文 4通栏消息)")
	private Integer popupMold;

	@ApiModelProperty(value = "推送频次: 1-每天; 2-隔天; 3-每周; 4-仅一次;")
	private Integer pushFrequency;

	@ApiModelProperty(value = "推送节点: 1-首次登录成功; 2-每次登录成功; 3-首次进入游戏; 4-每次进入游戏;")
	private Integer pushNode;

	@ApiModelProperty(value = "父游戏范围集合")
	private String parentGameArr;

	@ApiModelProperty(value = "子游戏范围集合")
	private String gameArr;

	@ApiModelProperty(value = "区服范围集合")
	private String areaArr;

	@ApiModelProperty(value = "主渠道范围集合")
	private String parentChannelArr;

	@ApiModelProperty(value = "子渠道范围集合")
	private String childChannelArr;

	@ApiModelProperty(value = "排序方式(DESC:倒序 ASC：正序)")
	private String sortType;
}
