package com.dy.yunying.api.vo.usergroup;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class UserGroupVo implements Serializable {
	private static final long serialVersionUID = -7515998475943768874L;

	private Long id;


	/**
	 * 群组名称
	 */
	@NotBlank(message = "群组名称不能为空")
	private String groupName;


	/**
	 * 群组归属：1.平台，2.用户，3.产品，4.渠道，10.其他
	 */
	@NotNull(message = "群组归属不能为空")
	private Integer groupType;


	/**
	 * 群组分类：1.全部游戏、2.部分游戏、3.指定游戏
	 */
	@NotNull(message = "群组分类不能为空")
	private Integer groupClass;


	/**
	 * 指定游戏id
	 */
	private Long classGameid;


	/**
	 * 筛选方式：1规则筛选，2ID筛选
	 */
	@NotNull(message = "筛选方式不能为空")
	private Integer type;



	/**
	 * 筛选维度：1账号，2角色
	 */
	@NotNull(message = "筛选维度不能为空")
	private Integer dimension;


	/**
	 * 主游戏ID，角色必填
	 */
	private Long pgameId;


	/**
	 * 区服，角色必填
	 */
	private Long areaId;


	/**
	 * 添加方式：1手动录入，2批量导入
	 */
	private Integer addType;



	/**
	 * 用户归属
	 */
	private String attribution;


	/**
	 * 用户行为
	 */
	private String behavior;

	/**
	 * 群组备注
	 */
	private String remark;

	/**
	 * 更新方式：1自动更新，2不更新
	 */
	private Integer updateMode;

	/**
	 * 用户角色/账号，使用逗号隔开
	 */
	private String groupData;

	private String conditionsDetail;

	/**
	 * 批量导入-上传文件
	 */
	private MultipartFile excelFile;


	/**
	 * 更新标识：0-不需要更新，1-需要更新
	 */
	private Integer updateMark;


	/**
	 * 最后更新时间
	 */
	private Date lastUpdateTime;

}
