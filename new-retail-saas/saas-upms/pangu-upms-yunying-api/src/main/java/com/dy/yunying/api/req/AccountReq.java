package com.dy.yunying.api.req;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Author: kyf
 * @Date: 2020/6/12 16:47
 */
@Setter
@Getter
public class AccountReq implements Serializable {

    /***
     * 用户ID
     */
    private Long userId;
    /**
     * 登陆用户名
     */
    private String username;

    /**
     * 登陆密码
     */
    private String password;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 联系邮箱
     */
    private String email;

    /**
     * 所属角色
     */
    private Integer roleId;

    /**
     * 分组ID
     */
    private Integer groupId;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * on:启用 off:禁用 lock:锁定 delete:删除 add:添加 update:修改
     */
    private String tag;
}
