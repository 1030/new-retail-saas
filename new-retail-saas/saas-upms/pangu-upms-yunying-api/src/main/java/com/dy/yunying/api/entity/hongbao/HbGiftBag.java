package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 红包礼包码
 *
 * @author chenxiang
 * @version 2021-10-25 14:20:24
 * table: hb_gift_bag
 */
@Data
@Accessors(chain = true)
@TableName(value = "hb_gift_bag")
public class HbGiftBag implements Serializable {

	//主键ID
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	//类型：1红包 2提现 3签到奖品 4签到任务
	@TableField(value = "type")
	private Integer type;

	//来源关联ID。如：红包id, 提现档次id, 奖品ID, 任务ID
	@TableField(value = "object_id")
	private Long objectId;

	//礼包码
	@TableField(value = "gift_code")
	private String giftCode;

	//1：唯一码(使用一次) 2：通用码(使用多次)
	@TableField(value = "usable")
	private Integer usable;

	//状态：0可用，1已领取
	@TableField(value = "status")
	private Integer status;

	//领取时间
	@TableField(value = "receive_time")
	private Date receiveTime;

	//礼包码总数量
	@TableField(value = "gift_amount")
	private Integer giftAmount;

	//已领取数量
	@TableField(value = "gift_getcounts")
	private Integer giftGetcounts;

	/**
	 * 是否删除(0否 1是)
	 */
	@ApiModelProperty(value = "是否删除(0否 1是)")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建者
	 */
	@TableField(fill = FieldFill.INSERT)
	private Long createId;

	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 更新者
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updateId;

	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}

	

	
	

