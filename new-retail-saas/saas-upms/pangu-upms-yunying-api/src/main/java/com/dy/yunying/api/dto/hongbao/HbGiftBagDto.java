package com.dy.yunying.api.dto.hongbao;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 红包礼包码
 * @author  chenxiang
 * @version  2021-10-25 14:20:24
 * table: hb_gift_bag
 */
@Data
public class HbGiftBagDto implements Serializable {
	
		//主键ID
		private Long id;
		//类型：1红包，2提现
		private Integer type;
		//来源关联ID。如：红包id，提现档次id
		private Long objectId;
		//礼包码
		private String giftCode;
		//1：使用一次 2：使用多次
		private Integer usable;
		//状态：0可用，1已领取
		private Integer status;
		//领取时间
		private Date receiveTime;
		//是否删除：0否 1是
		private Integer deleted;
		//创建时间
		private Date createTime;
		//修改时间
		private Date updateTime;
		//创建人
		private Long createId;
		//修改人
		private Long updateId;
	
	
}


