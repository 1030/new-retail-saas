package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

/**
 * @author ：lile
 * @date ：2021/5/29 17:57
 * @description：
 * @modified By：
 */
@Data
public class GameSupplierReq extends Page {
	/**
	 * 父游戏ID
	 */
	private Integer pgid;

	/**
	 * 充值回调地址
	 */
	private String exchangeUrl;

}
