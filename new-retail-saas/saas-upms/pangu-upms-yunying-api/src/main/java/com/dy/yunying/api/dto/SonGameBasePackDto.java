package com.dy.yunying.api.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 子游戏自出包
 *
 * @Author: hjl
 * @Date: 2020/8/8 11:08
 */
@Getter
@Setter
public class SonGameBasePackDto {

    /**
     * 子游戏ID
     */
    private Long gameId;

    /**
     * 子游戏反编译路径
     */
    private String decodePath;

}