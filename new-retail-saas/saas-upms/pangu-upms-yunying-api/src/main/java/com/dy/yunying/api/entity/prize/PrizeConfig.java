package com.dy.yunying.api.entity.prize;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 奖励配置表
 *
 * @author chenxiang
 * @version 2022-04-28 16:37:18 table: prize_config
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "prize_config")
public class PrizeConfig extends Model<PrizeConfig> {

	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 奖品推送主键ID
	 */
	@TableField(value = "prize_push_id")
	private Long prizePushId;

	@TableField(value = "prize_push_type")
	private Integer prizePushType;

	/**
	 * 标题文本
	 */
	@TableField(value = "title")
	private String title;

	/**
	 * 说明文本
	 */
	@TableField(value = "description")
	private String description;

	/**
	 * 奖励按钮
	 */
	@TableField(value = "prize_button")
	private Integer prizeButton;

	/**
	 * icon图
	 */
	@TableField(value = "icon")
	private String icon;

	/**
	 * 奖励类型：1代金券，2礼包码，3游豆，4卡密
	 */
	@TableField(value = "prize_type")
	private Integer prizeType;

	/**
	 * 金额
	 */
	@TableField(value = "money")
	private BigDecimal money;

	/**
	 * 限制金额
	 */
	@TableField(value = "limit_money")
	private BigDecimal limitMoney;

	/**
	 * 代金券类型：1-满减；2-折扣；
	 */
	@TableField(value = "cdk_type")
	private Integer cdkType;

	/**
	 * 折扣券-折扣上限金额(-1:不限;)
	 */
	@TableField(value = "discount_limit_money")
	private BigDecimal discountLimitMoney;

	/**
	 * 时间类型：1固定时间，2领取后指定天数
	 */
	@TableField(value = "time_type")
	private Integer timeType;

	/**
	 * 余额类型。1：普通；2：临时
	 */
	@TableField(value = "currency_type")
	private Integer currencyType;

	/**
	 * 开始时间
	 */
	@TableField(value = "start_time")
	private Date startTime;

	/**
	 * 结束时间
	 */
	@TableField(value = "end_time")
	private Date endTime;

	/**
	 * 领取后指定时间(秒)
	 */
	@TableField(value = "effective_time")
	private Long effectiveTime;

	/**
	 * 使用限制(逗号分割)：all不限，game游戏，role角色
	 */
	@TableField(value = "use_limit_type")
	private String useLimitType;

	/**
	 * 礼包码类型：1唯一码，2通用码
	 */
	@TableField(value = "gift_type")
	private Integer giftType;

	/**
	 * 通用码
	 */
	@TableField(value = "gift_code" )
	private String giftCode;

	/**
	 * 礼包码数量
	 */
	@TableField(value = "gift_amount")
	private Integer giftAmount;

	/**
	 * 礼包内容
	 */
	@TableField(value = "gift_content")
	private String giftContent;

	/**
	 * 打开类型：0不跳转，1SDK面板，2内置浏览器，3外部链接
	 */
	@TableField(value = "open_type")
	private Integer openType;

	/**
	 * 菜单Id
	 */
	@TableField(value = "menu_id")
	private String menuId;

	/**
	 * 菜单名称
	 */
	@TableField(value = "menu_title")
	private String menuTitle;

	/**
	 * 菜单编码(安卓)
	 */
	@TableField(value = "menu_code")
	private String menuCode;
	/**
	 * 菜单编码(苹果)
	 */
	@TableField(value = "menu_code_ios")
	private String menuCodeIos;
	/**
	 * 二级菜单编码(安卓)
	 */
	@TableField(value = "second_menu_code")
	private String secondMenuCode;
	/**
	 * 二级菜单编码(苹果)
	 */
	@TableField(value = "second_menu_code_ios")
	private String secondMenuCodeIos;

	/**
	 * 来源类型(1红包活动 2签到活动)
	 */
	@TableField(value = "skip_mold")
	private Integer skipMold;

	/**
	 * 跳转位置 如：红包类型 1：等级红包、2：充值红包、3：邀请红包、4：定制红包
	 */
	@TableField(value = "skip_belong_type")
	private Integer skipBelongType;

	/**
	 * 来源id, 如：红包活动id
	 */
	@TableField(value = "skip_id")
	private Long skipId;

	/**
	 * 跳转地址
	 */
	@TableField(value = "skip_url")
	private String skipUrl;

	/**
	 * 跳转地址(ios)
	 */
	@TableField(value = "skip_url_ios")
	private String skipUrlIOS;

	/**
	 * 展示方式(1横屏展示，2竖屏展示)
	 */
	@TableField(value = "show_type")
	private Integer showType;

	/**
	 * 是否删除 0否 1是
	 */
	@TableLogic
	@TableField(value = "deleted")
	private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

}
