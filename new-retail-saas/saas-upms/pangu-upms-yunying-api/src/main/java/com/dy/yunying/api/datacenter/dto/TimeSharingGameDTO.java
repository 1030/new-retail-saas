package com.dy.yunying.api.datacenter.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sunyq
 * @date 2022/8/17 14:50
 */
@Data
public class TimeSharingGameDTO implements Serializable {
	/**
	 * 起始时间
	 */
	private String startTime;
	/**
	 * 终止时间
	 */
	private String endTime;

	@ApiModelProperty("父游戏")
	private String pgidArr;

	@ApiModelProperty("子游戏")
	private String gameidArr;

	@ApiModelProperty("主渠道列表")
	private String parentchlArr;

	@ApiModelProperty("子渠道编码列表")
	private String chlArr;

	@ApiModelProperty("分包编码列表")
	private String appchlArr;

	@ApiModelProperty("查询的字段 多个用逗号分隔  ")
	private String columns;

	@ApiModelProperty("列名")
	private String titles;

//	@ApiModelProperty("当前页")
//	private Long current;
//
//	@ApiModelProperty("每页的size")
//	private Long size;

	@ApiModelProperty("排序  desc / asc")
	private String sort;

	@ApiModelProperty("排序字段")
	private String kpiValue;

}
