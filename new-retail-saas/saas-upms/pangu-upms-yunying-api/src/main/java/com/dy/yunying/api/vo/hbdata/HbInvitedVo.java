package com.dy.yunying.api.vo.hbdata;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/26 14:05
 * @description：
 * @modified By：
 */
@Data
public class HbInvitedVo implements Serializable {

	/**
	 * 活动名称
	 */
	private String activityName;

	/**
	 * PV
	 */
	private Long servicePV;

	/**
	 * UV
	 */
	private Long serviceUV;

}
