package com.dy.yunying.api.enums;

/**
 * @author chenxiang
 * @className PackTypeEnum
 * @date 2022-12-15 11:29
 */
public enum PackTypeEnum {
	LEVEL(1, "运营母包"),
	RECHARGE(2, "云微端");

	private Integer type;
	private String name;
	public Integer getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	private PackTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(Integer type) {
		if (type == null){
			return null;
		}
		for (PackTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return null;
	}
}
