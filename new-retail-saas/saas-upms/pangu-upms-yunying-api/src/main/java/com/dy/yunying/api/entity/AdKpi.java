package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName AdKpi
 * @Description 绩效管理-kpi
 * @Author nieml
 * @Time 2021/7/8 10:52
 * @Version 1.0
 **/

@Data
@TableName("ad_kpi")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "绩效管理-kpi")
public class AdKpi extends Model<AdKpi> {

	/**
	 * 主键
	 */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value="主键")
	private Integer id;

	/**
	 * 层级：1 部门；2 组别；3 投放人
	 */
	@ApiModelProperty(value="层级：1 部门；2 组别；3 投放人")
	@TableField(value = "`hierarchy`")
	private Integer hierarchy;

	/**
	 * 所选层级的id：部门id|组别id|投放人id
	 */
	@ApiModelProperty(value="所选层级的id：部门id|组别id|投放人id")
	private Integer hierarchyId;

	/**
	 * 年月
	 */
	@ApiModelProperty(value="年月")
	@TableField(value = "`year_month`")
	private Integer yearMonth;

	/**
	 * 主游戏id
	 */
	@ApiModelProperty(value="主游戏id")
	private Integer pgid;

	/**
	 * 系统
	 */
	@ApiModelProperty(value="系统")
	private Integer os;

	/**
	 * 目标量级
	 */
	@ApiModelProperty(value="目标量级")
	private Long targetLevel;

	/**
	 * 目标ROI
	 */
	@ApiModelProperty(value="目标ROI")
	private BigDecimal targetRoi;

	/**
	 * 目标ROI-30
	 */
	@ApiModelProperty(value="目标ROI-30")
	private BigDecimal targetRoi30;

	/**
	 * 目标新用户月流水
	 */
	@ApiModelProperty(value="目标新用户月流水")
	private Long targetNewuserMonthWater;

	/**
	 * 目标老用户月流水
	 */
	@ApiModelProperty(value="目标老用户月流水")
	private Long targetOlduserMonthWater;

	/**
	 * 实际量级
	 */
	@ApiModelProperty(value="实际量级")
	private Long realLevel;

	/**
	 * 实际ROI
	 */
	@ApiModelProperty(value="实际ROI")
	private BigDecimal realRoi;

	/**
	 * 实际ROI-30
	 */
	@ApiModelProperty(value="实际ROI-30")
	private BigDecimal realRoi30;

	/**
	 * 实际新用户月流水
	 */
	@ApiModelProperty(value="实际新用户月流水")
	private Long realNewuserMonthWater;

	/**
	 * 实际老用户月流水
	 */
	@ApiModelProperty(value="实际老用户月流水")
	private Long realOlduserMonthWater;

	/**
	 * 删除状态：0正常，1删除
	 */
	@ApiModelProperty(value="删除状态：0正常，1删除")
	private Integer isDelete;

	/**
	 * 创建人
	 */
	@ApiModelProperty(value="创建人")
	private String createUser;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value="创建时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 修改人
	 */
	@ApiModelProperty(value="修改人")
	private String updateUser;

	/**
	 * 修改时间
	 */
	@ApiModelProperty(value="修改时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}
