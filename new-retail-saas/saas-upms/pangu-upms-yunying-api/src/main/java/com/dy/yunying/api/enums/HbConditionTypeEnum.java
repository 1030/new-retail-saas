package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName HbActivityTypeEnum.java
 * @createTime 2021年10月26日 10:35:00
 */
public enum HbConditionTypeEnum {
	Condition1("1", "角色等级", "角色达到{1}级"),
	Condition2("2", "角色等级和创角时间", "创角{2}天达到{1}级"),
	Condition3("3", "角色累计充值", "角色累计充值{3}元"),
	Condition4("4", "角色首次充值", "角色首次充值"),
	Condition5("5", "角色单笔充值满", "角色单笔充值{3}元"),
	Condition6("6", "邀请好友成功注册游戏", "邀请好友成功注册游戏"),
	Condition7("7", "邀请好友首次提现成功", "邀请好友首次提现成功"),
	Condition8("8", "邀请好友累计充值", "邀请好友累计充值{3}元"),
	Condition9("9", "邀请好友创角N天内达到N级", "邀请好友创角{2}天达到{1}级"),
	Condition11("11", "角色单笔充值等于", "角色单笔充值等于{3}元"),
	Condition12("12", "角色连续登录N天", "角色连续登录{2}天"),
	Condition13("13", "创角N天累计充值X元", "创角{2}天累计充值{3}元"),
	Condition14("14", "创角N天首次充值", "创角{2}天首次充值"),
	Condition15("15", "开服N天达到X级", "开服{2}天达到{1}级"),
	Condition16("16", "开服N天累计充值X元", "开服{2}天累计充值{3}元"),
	Condition17("17", "开服N天首次充值", "开服{2}天首次充值"),
	Condition18("18", "游戏任务", "游戏任务"),
	Condition19("19", "角色累计登录N天", "角色累计登录{2}天"),
	Condition20("20", "红包活动提现N次", "红包活动提现{4}次");

	private String type;
	private String name;
	private String desc;
	public String getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	public String getDesc() {
		return desc;
	}
	private HbConditionTypeEnum(String type, String name, String desc) {
		this.type = type;
		this.name = name;
		this.desc = desc;
	}

	public static String getName(String type) {
		if (StringUtils.isBlank(type)){
			return null;
		}
		for (HbConditionTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return null;
	}

	public static String getDesc(String type, String level, String day, String fee, String num) {
		if (StringUtils.isBlank(type)){
			return null;
		}
		for (HbConditionTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				// 等级：{1}，天数：{2}，金额：{3}，次数：{4}
				return ele.getDesc().replace("{1}", level).replace("{2}", day).replace("{3}", fee).replace("{4}", num);
			}
		}
		return null;
	}
}
