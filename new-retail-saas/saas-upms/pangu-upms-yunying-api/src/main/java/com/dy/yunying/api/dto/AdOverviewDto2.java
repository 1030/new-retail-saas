package com.dy.yunying.api.dto;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@ApiModel("广告报表查询对象")
public class AdOverviewDto2 {

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	public String getUserIds() {
		String userIds = "-1";  // 默认不查询任何投放账号
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			userIds = StringUtils.join(roleUserIdList, ",");
		}
		return userIds;
	}

	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	public String getAdAccounts() {
		String adAccounts = "'NO'";  // 默认不查询任何广告账户
		if (CollectionUtil.isNotEmpty(roleUserIdList)) {
			adAccounts = "'" + StringUtils.join(roleAdAccountList, "','") + "'";
		}
		return adAccounts;
	}

	@ApiModelProperty("是否系统管理员")
	private int isSys = 0;

	@ApiModelProperty("设备激活时间（开始）/广告统计查询时间")
	@NotNull(message = "查询开始日期不允许为空")
	private Long rsTime;
	@ApiModelProperty("设备激活时间（结束）/广告统计查询时间")
	@NotNull(message = "查询结束日期不允许为空")
	private Long reTime;

	@ApiModelProperty("主渠道")
	private String parentchl;

	@ApiModelProperty("渠道编码")
	private String chl;

	@ApiModelProperty("分包编码")
	private String appchl;

	@ApiModelProperty("主渠道列表")
	private String parentchlArr;

	@ApiModelProperty("渠道编码列表")
	private String chlArr;

	@ApiModelProperty("分包编码列表")
	private String appchlArr;

	@ApiModelProperty("广告账号名称")
	private String adName;


	@ApiModelProperty("广告计划名称")
	private String adidName;
	@ApiModelProperty("广告计划")
	private String adidArr;

	@ApiModelProperty("部门id数组,多个用逗号分隔")
	private String deptIdArr;
	@ApiModelProperty(value = "用户组Id列表")
	private String userGroupIdArr;
	@ApiModelProperty("客户端操作系统0=android；1=IOS；3=OTHERS或为空；4=H5; 5=PC")
	private Integer os;

	@ApiModelProperty("父游戏")
	private String pgidArr;

	@ApiModelProperty("子游戏")
	private String gameidArr;

	@ApiModelProperty("投放人数组")
	private String investorArr;

	@ApiModelProperty("广告账号数组")
	private String advertiserIdArr;

	@ApiModelProperty("代理商数组")
	private String agentIdArr;

	@ApiModelProperty("是否显示分成前的数据 ：0 否 1：是")
	private Integer showRatio = 0;


	@ApiModelProperty("转化/深化目标")
	private String convertArr;

	@ApiModelProperty("深度转化目标")
	private String deepConvertArr;

	@ApiModelProperty("深度转化目标付费方式")
	private String convertDataTypeArr;

	//	@ApiModelProperty("游戏对象信息")
//	private List<WanGameDto> wanGameDtoList;

	@ApiModelProperty("周期类型 ：1 按天 2 按周  3  按月 4 汇总（默认）")
	private Integer cycleType = 4;

	@ApiModelProperty("平台类型 ：1头条，8广点通")
	private String platformType;

	public String getPeriod() {
		String period = "'汇总'";
		switch (cycleType) {
			case 1:
				period = "day";
				break; //可选
			case 2:
				period = "weekStr";
				//语句
				break; //可选
			case 3:
				period = "monthStr";
				break; //可选
			//你可以有任意数量的case语句
			default: //可选
				//语句
		}
		return period;
	}

	@ApiModelProperty("类别 多个用逗号分隔【系统 os,主游戏 pgid，子游戏 gid ,部门 deptId,渠道编码 parentchl，分包编码：appchl,广告计划 adId,  投放人investor   】")
	private String queryColumn;


	@ApiModelProperty("搜索名称")
	private String searchName;

	public List<Long> deptIdList;
	private List<Integer> investorList;
	private List<String> advertiserIdList;


	private String titles;
	private String columns;

	private Long current;
	private Long size;


	public List<String> getAdvertiserIdList() {
		return StringUtils.isNotEmpty(advertiserIdArr) ? Arrays.asList(advertiserIdArr.split(",")) : new ArrayList<>();
	}

	public void setAdvertiserIdList(List<String> advertiserIdList) {
		this.advertiserIdList = advertiserIdList;
	}

	public List<Long> getDeptIdList() {
		return StringUtils.isNotEmpty(deptIdArr) ? Arrays.stream(Arrays.stream(deptIdArr.split(",")).mapToLong(Long::parseLong).toArray()).boxed().collect(Collectors.toList()) : new ArrayList<>();
	}

	public List<Integer> getInvestorList() {
		return StringUtils.isNotEmpty(investorArr) ? Arrays.stream(Arrays.stream(investorArr.split(",")).mapToInt(Integer::parseInt).toArray()).boxed().collect(Collectors.toList()) : new ArrayList<>();
	}

	private Boolean enableTest = false;
}
