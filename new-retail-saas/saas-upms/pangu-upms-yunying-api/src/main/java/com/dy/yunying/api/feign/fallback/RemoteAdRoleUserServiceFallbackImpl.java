/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dy.yunying.api.feign.fallback;

import com.dy.yunying.api.entity.AdRoleUser;
import com.dy.yunying.api.feign.RemoteAdRoleUserService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lengleng
 * @date 2019/2/1
 */
@Slf4j
@Component
public class RemoteAdRoleUserServiceFallbackImpl implements RemoteAdRoleUserService {

	@Setter
	private Throwable cause;

	@Override
	public R getOwnerRoleUsers() {
		return null;
	}

	@Override
	public R<List<AdRoleUser>> getUserListByRole(String from,List<Integer> roles) {
		return null;
	}
}
