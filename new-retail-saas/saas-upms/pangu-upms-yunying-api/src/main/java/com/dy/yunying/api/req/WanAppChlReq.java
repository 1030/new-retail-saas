package com.dy.yunying.api.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author ：lile
 * @date ：2021/8/26 11:36
 * @description：
 * @modified By：
 */
@Data
public class WanAppChlReq {

	private String codeName;

	private Integer limitNum;

	private List<Integer> userList;

	private Integer cpsParentChannelId;

	/**
	 * 当前角色管理的用户列表
	 */
	private List<Integer> roleUserIdList;

	/**
	 * 当前角色授权的投放账户
	 */
	private List<String> roleAdAccountList;

	@ApiModelProperty("是否系统管理员")
	private int isSys;
}
