package com.dy.yunying.api.datacenter.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author sunyq
 * @date 2022/8/17 14:43
 */
@Data
public class TimeSharingGameDataVO implements Serializable {
	/**
	 * 日期
	 */
	private String day;
	/**
	 * 小时
	 */
	private String hour;
	/**
	 * 新增设备
	 */
	private String deviceNums="0";

	/**
	 * 新增账号
	 */
	private String accountNums="0";
	/**
	 * 注册转化率
	 */
	private String regRate="0.0";
	/**
	 * 充值金额
	 */
	private String activePayAmounts="0";
	/**
	 * 充值人数
	 */
	private String activeFeeAccounts="0";
	/**
	 * 付费率
	 */
	private String payFeeRate="0.0";
	/**
	 * ARPU ARPU指每活跃账号的平均收入，时间段内充值金额÷活跃账号
	 */
	private String arpu="0.0";
	/**
	 * ARPPU ARPPU指每活跃付费账号平均收益，时间段内充值金额 ÷充值人数
	 */
	private String arppu="0.0";
	/**
	 * 累计充值人数
	 */
	private String totalFeeAccounts="0";
	/**
	 * 累计充值金额
	 */
	private String totalPayAmounts="0";
	/**
	 * 新账号充值金额
	 */
	private String newPayAmounts="0";
	/**
	 * 新账号充值人数
	 */
	private String newPayNums="0";

	/**
	 * 新账号付费率
	 */
	private String newPayFeeRate="0.0";
	/**
	 * 新账号ARPU
	 */
	private String newArpu="0.0";
	/**
	 * 新账号ARPPU
	 */
	private String newArppu="0.0";
	/**
	 * 老帐号充值金额
	 */
	private String oldActivePayAmounts="0";
	/**
	 * 老帐号充值人数
	 */
	private String oldActiveFeeAccounts="0";
	/**
	 * 老帐号付费率
	 */
	private String oldPayFeeRate="0.0";
	/**
	 * 老帐号ARPU
	 */
	private String oldArpu="0.0";
	/**
	 * 老账号ARPPU
	 */
	private String oldArppu="0.0";




}
