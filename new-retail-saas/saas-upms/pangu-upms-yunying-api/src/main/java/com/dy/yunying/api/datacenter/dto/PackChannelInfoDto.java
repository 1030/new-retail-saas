package com.dy.yunying.api.datacenter.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 分包渠道信息
 *
 * @Author: hjl
 * @Date: 2020/8/8 11:08
 */
@Getter
@Setter
public class PackChannelInfoDto {

    /**
     * 上报方式
     */
    private int rtype;

    /**
     * 平台类型
     */
    private int platform;

    /**
     * 上报应用ID
     */
    private String appId;

    /**
     * 上报应用名称
     */
    private String appName;

    /**
     * 子渠道编码
     */
    private String chlCode;

    /**
     * 分包渠道编码
     */
    private String appChlCode;

}