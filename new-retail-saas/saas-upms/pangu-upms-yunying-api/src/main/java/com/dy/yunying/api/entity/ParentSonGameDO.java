package com.dy.yunying.api.entity;

import lombok.Data;

@Data
public class ParentSonGameDO {
	/*
	 * 父游戏id
	 * */
	private String pid;

	/*
	 * 父游戏名称
	 * */
	private String pgname;

	/*
	 * 子游戏id
	 * */
	private String id;

	/*
	 * 子游戏名称
	 * */
	private String gname;

	/*
	 * 终端类型id
	 * */
	private String os;

	/*
	 * 终端类型名称
	 * */
	private String osname;

}
