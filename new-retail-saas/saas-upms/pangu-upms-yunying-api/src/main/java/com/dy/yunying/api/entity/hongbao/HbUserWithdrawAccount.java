/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 提现记录表
 *
 * @author zxm
 * @date 2021-10-28 20:14:35
 */
@Data
@TableName("hb_user_withdraw_account")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "用户、提现账号绑定表")
public class HbUserWithdrawAccount extends HbBaseEntity {

	@TableId(value = "id",type = IdType.AUTO)
	@ApiModelProperty(value = "主键ID")
	private Long id;

	@ApiModelProperty(value = "用户ID")
	private Long userId;

	@ApiModelProperty(value = "提现方式：1游戏货币，2微信，3支付宝")
	private Integer type;

	@ApiModelProperty(value = "openid")
	private String openid;

	@ApiModelProperty(value = "昵称")
	private Integer nickname;

	@ApiModelProperty(value = "手机")
	private String mobile;

	@ApiModelProperty(value = "头像")
	private String avator;

	@ApiModelProperty(value = "是否删除：0否 1是")
	private Integer deleted;


}
