package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 更新子游戏基础包
 *
 * @Author: hjl
 * @Date: 2020/6/11 20:59
 */
@Getter
@Setter
public class UpdateSonPackReq extends Page implements Serializable {

	/**
	 * 父游戏版本ID
	 */
	private Long parentVersionId;

	/**
	 * 子游戏列表
	 */
	private String gameIds;

	/**
	 * 子游戏列表
	 */
	private List<Long> gameIdList;

}