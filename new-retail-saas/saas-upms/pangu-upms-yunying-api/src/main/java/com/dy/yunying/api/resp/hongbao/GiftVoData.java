package com.dy.yunying.api.resp.hongbao;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 礼包码总计
 * @date 2021/10/27 19:19
 */
@Data
public class GiftVoData implements Serializable {

	@ApiModelProperty(value = "红包标题")
	private String name;

	@ApiModelProperty(value = "总数量")
	private Integer totalNum;

	@ApiModelProperty(value = "已领取数量")
	private Integer receiveNum;
}
