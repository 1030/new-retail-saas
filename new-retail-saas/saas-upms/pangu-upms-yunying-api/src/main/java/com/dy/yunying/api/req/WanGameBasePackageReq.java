package com.dy.yunying.api.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class WanGameBasePackageReq extends  Page implements Serializable {

	private static final long serialVersionUID = 5024260167507311451L;
	/**
     * 子游戏ID
     */
    private Long gameId;

    /**
     *  子游戏名称
     */
    private String gname;

    /**
     * 父渠道ID
     */
    private Long parentChlId;

    //主渠道code
    private String parentCode;

    /**
     * 主渠道
     */
    private String chnname;

    /**
     * 第三方APPID
     */
    private String appId;

    /**
     * 第三方APPNAME
     */
    private String appName;

    /**
     * 上报方式
     */
    private String rtype;

    /**
     * 母包版本
     */
    private String pCode;

    /**
     * 基础包包版本
     */
    private String sCode;

    //多父游戏查询
	private String pgids;


	/**
	 * 父游戏ID
	 */
	private Long pgid;

}
