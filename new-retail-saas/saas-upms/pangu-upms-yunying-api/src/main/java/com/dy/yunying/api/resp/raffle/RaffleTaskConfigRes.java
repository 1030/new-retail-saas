package com.dy.yunying.api.resp.raffle;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 抽奖任务配置表
 * @author  chenxiang
 * @version  2022-11-08 15:59:48
 * table: raffle_task_config
 */
@Data
public class RaffleTaskConfigRes implements Serializable {

	@ApiModelProperty(value = "主键id")
	private Long id;

	@ApiModelProperty(value = "活动ID")
	private Long activityId;

	@ApiModelProperty(value = "任务名称/目标")
	private String taskName;

	@ApiModelProperty(value = "任务达成条件类型：1-角色等级，2-角色等级和创角时间，3-角色累计充值，4-角色首次充值，5-角色单笔充值满额，6-邀请好友成功注册游戏，7-邀请好友首次提现成功，8-邀请好友累计充值，9-邀请好友创角N天内达到N级，11-角色单笔充值等额，12-角色连续登录N天，13-创角N天累计充值X元，14-创建N天首次充值，15-开服N天达到X级，16-开服N天累计充值X元，17-开服N天首次充值，18-游戏任务，19-角色累计登录N天；20-红包活动提现N次；")
	private Integer conditionType;

	@ApiModelProperty(value = "任务达成条件：角色等级")
	private Integer conditionRoleLevel;

	@ApiModelProperty(value = "任务达成条件：创角天数/开服天数；")
	private Integer conditionRoleDays;

	@ApiModelProperty(value = "任务达成条件：角色充值金额")
	private BigDecimal conditionRoleRecharge;

	@ApiModelProperty(value = "游戏任务ID")
	private String conditionTaskId;

	@ApiModelProperty(value = "条件达成次数")
	private Integer conditionReachNum;

	@ApiModelProperty(value = "任务可完成次数")
	private Integer conditionLimitNum;

	@ApiModelProperty(value = "奖励类型：1-抽奖次数；")
	private Integer rewardType;

	@ApiModelProperty(value = "奖励数量")
	private Integer rewardNum;

	@ApiModelProperty(value = "是否删除：0-否；1-是；")
	private Integer deleted;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

	@ApiModelProperty(value = "创建人")
	private Long createId;

	@ApiModelProperty(value = "修改人")
	private Long updateId;
}


