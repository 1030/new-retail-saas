package com.dy.yunying.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 用户表
 * admin_user_new
 * @author kongyanfang
 * @date 2020-09-29 14:35:34
 */
@Getter
@Setter
public class AdminUserNewDO {
    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 登陆用户名
     */
    private String username;

    /**
     * 登陆密码
     */
    private String password;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 最后登陆时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date lastLoginTime;

    /**
     */
    private String lastLoginIp;

    /**
     * 连续登陆失败次数
     */
    private Integer loginFailCount;

    /**
     * 锁定时间
     */
    private Date lockTime;

    /**
     * 所属角色
     */
    private Integer roleId;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 联系邮箱
     */
    private String email;

    /**
     * 分组ID
     */
    private Integer groupId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建人用户ID
     */
    private Integer createUserId;

    /**
     * 创建人用户名
     */
    private String createUserName;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /**
     * 更新人用户ID
     */
    private Integer updateUserId;

    /**
     * 更新人用户名
     */
    private String updateUserName;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 二级登录名
     */
    private String secondName;

    /**
     * 二级登录密码
     */
    private String secondPwd;
}