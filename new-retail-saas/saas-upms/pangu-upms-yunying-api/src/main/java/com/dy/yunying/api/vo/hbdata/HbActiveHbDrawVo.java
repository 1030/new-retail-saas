package com.dy.yunying.api.vo.hbdata;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：lile
 * @date ：2021/11/18 17:49
 * @description：
 * @modified By：
 */
@Data
public class HbActiveHbDrawVo implements Serializable {

	//活动ID
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("活动ID")
	private Long activityId;

	// 活动名称
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("活动名称")
	private String activityName;

	//红包ID
	private Long redpackId;

	// 红包名称
	@ColumnWidth(25)
	@HeadFontStyle
	@ExcelProperty("红包名称")
	private String redpackName;

	//领取条件
	@ColumnWidth(20)
	@HeadFontStyle
	@ExcelProperty("领取条件")
	private String redpackTitle;

	// 领取数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("领取数")
	private Long getNum;

	// 领取按钮点击数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("领取按钮点击数")
	private Long clickNum;

	// 完成数
	@ColumnWidth(15)
	@HeadFontStyle
	@ExcelProperty("完成数")
	private Long finishNum;
}
