package com.dy.yunying.api.req;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 渠道包请求
 *
 * @Author: hjl
 * @Date: 2020/6/11 20:59
 */
@Getter
@Setter
public class GamePlatformReq implements Serializable {

	private static final long serialVersionUID = 7313945359735645478L;
	/**
     * 游戏ID
     */
    private Integer gameId;

    /**
     * 主渠道ID
     */
    private Integer parentChlId;

    /**
     * 平台类型ID
     */
    private Integer platformid;
	/**
	 * 应用ID
	 */
    private Integer appId;

}