package com.dy.yunying.api.req.usergroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * hezz 2022-05-17 17:00:00
 * */
@Data
public class UserGroupStatReq implements Serializable {

	private static final long serialVersionUID = 3799186434822397683L;

	@NotNull(message = "群组ID不能为空")
	@ApiModelProperty(value = "群组ID")
	private List<Long> groupIds;

	@ApiModelProperty(value = "查询类型 1-公告和奖励 2-公告 3-奖励 4-公告或奖励")
	private Integer qryType = 1;
}
