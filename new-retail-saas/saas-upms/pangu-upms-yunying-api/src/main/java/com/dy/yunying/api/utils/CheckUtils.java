package com.dy.yunying.api.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName CheckUtils.java
 * @createTime 2021年10月26日 16:36:00
 */
public class CheckUtils {
	/**
	 * 验证金额
	 * @param val
	 * @return
	 */
	public static boolean checkMoney(String val){
		// 金额正则,可以没有小数，小数最多不超过两位
		String reg_money = "\\d+(\\.\\d{1,2})?";
		Pattern pattern = Pattern.compile(reg_money);
		Matcher matcher = pattern.matcher(String.valueOf(val));
		return matcher.matches();
	}
	/**
	 * 验证整数
	 * @param str
	 * @return
	 */
	public static boolean checkInt(String str){
		if (StringUtils.isBlank(str)){
			return false;
		}
		return str.matches("^(([^0][0-9]+|0)$)|^(([1-9]+)$)");
	}

	public static void main(String[] args) {
		System.out.println(checkInt("99"));
	}
}
