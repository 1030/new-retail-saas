package com.dy.yunying.api.cps;


import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author sunyq
 * @date 2022/10/11 17:59
 */
@Data
public class CpsNodeData implements Serializable {
	private String nodeId;

	private String nodeName;

	private boolean checked;

	private boolean isParentChl;

	private List<CpsNodeData> childNodeList;

	@Override
	public boolean equals(Object o) {
		if (this == o){
			return true;
		}
		if (o == null || getClass() != o.getClass()){
			return false;
		}
		CpsNodeData that = (CpsNodeData) o;
		return nodeId.equals(that.nodeId) &&
				nodeName.equals(that.nodeName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(nodeId, nodeName);
	}
}
