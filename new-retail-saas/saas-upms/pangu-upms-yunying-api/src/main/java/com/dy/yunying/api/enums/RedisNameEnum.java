package com.dy.yunying.api.enums;

import com.dy.yunying.api.constant.Constant;

/**
 * redis名称定义规则，
 *
 * @auther kongyanfang
 */
public enum RedisNameEnum {
	/**
	 * 短信验证码
	 */
	SMS_CODE {
		@Override
		public String get(Object phone) {
			return Constant.REDIS_CACHE_PREFIX + "SMS_CODE_" + phone;
		}
	},
	/***
	 * 短息验证码时间
	 */
	SMS_CODE_TIME {
		@Override
		public String get(Object phone) {
			return Constant.REDIS_CACHE_PREFIX + "SMS_CODE_TIME_" + phone;
		}
	},
	/***
	 * 图形验证码
	 */
	SCAPTCHA_KEY {
		@Override
		public String get(Object codeType) {
			return Constant.REDIS_CACHE_PREFIX + "SCAPTCHA_KEY_" + codeType;
		}
	},
	/***
	 * 随机码
	 */
	RANDOM_KEY {
		@Override
		public String get(Object codeType) {
			return Constant.REDIS_CACHE_PREFIX + "RANDOM_KEY" + codeType;
		}
	},
	/***
	 * 定时时间key
	 */
	TIME_KEY {
		@Override
		public String get(Object codeType) {
			return Constant.REDIS_CACHE_PREFIX + "TIME_KEY";
		}
	},
	/***
	 * 累加数量
	 */
	ACCUMULATIVE_KEY {
		@Override
		public String get(Object codeType) {
			return Constant.REDIS_CACHE_PREFIX + "ACCUMULATIVE_KEY";
		}
	},
	/***
	 * TapTap预约人数
	 */
	TAPTAP_HSCJ_KEY {
		@Override
		public String get(Object codeType) {
			return Constant.REDIS_CACHE_PREFIX + "TAPTAP_HSCJ_KEY";
		}
	},
	;

	public abstract String get(Object param);


}
