package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ：lile
 * @date ：2021/10/27 11:29
 * @description：
 * @modified By：
 */
@Data
@TableName("popup_notice_game")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "公告子游戏范围关系")
public class PopupNoticeGame extends HbBaseEntity {

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "公告子游戏关系ID")
	private Long id;

	@ApiModelProperty(value = "公告ID")
	private Long noticeId;

	@ApiModelProperty(value = "主游戏ID")
	private Long pgameId;

	@ApiModelProperty(value = "子游戏ID")
	private Long childGameId;

	@ApiModelProperty(value = "游戏层级(1主游戏 2子游戏)")
	private int gameRange;

}
