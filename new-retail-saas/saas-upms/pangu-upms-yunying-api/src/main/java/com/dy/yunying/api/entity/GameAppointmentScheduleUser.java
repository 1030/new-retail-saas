package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jpedal.parser.shape.S;

import java.util.Date;

/**
 * 游戏预约进度人数表
 * @author  leisw
 * @version  2022-05-09 17:14:29
 * table: game_appointment_schedule_user
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "game_appointment_schedule_user")
public class GameAppointmentScheduleUser extends Model<GameAppointmentScheduleUser>{
	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Long id;
	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	private String type;
	/**
	 * 预约类型：1实际预约人数 2 标签预约人数 3.规则预约人数
	 */
	private Integer conventionTypeId;
	/**
	 * 标签来源/规则名称
	 */
	private String conventionName;
	/**
	 * 生成小时类型(1.全天 2.早5晚12 3.早8晚10 4.自定义)
	 */
	private String scheduleType;
	/**
	 *生成时间小时(1-24)
	 */
	private String scheduleHour;

	/**
	 *结束生成时间小时
	 */
	private String startHour;

	/**
	 *开始生成时间小时
	 */
	private String endHour;

	/**
	 * 规定人数
	 */
	private Long number;
	/**
	 * 开始生成时间
	 */
	private Date startTime;

	/**
	 * 结束生成时间
	 */
	private Date endTime;

	/**
	 * 生成频率
	 */
	private Long frequency;

	/**
	 * 生成时间单位: (1:表示分, 2:表示小时, 3:表示天,4:表示月)
	 */
	private Integer timeUnit;
	/**
	 * 生成时间单位: (1:表示分, 2:表示小时, 3:表示天,4:表示月)
	 */
	private Date addTime;
	/**
	 * 最小生成人数
	 */
	private Long minNum;
	/**
	 * 最大生成人数
	 */
	private Long maxNum;
	/**
	 * 预计增长人数
	 */
	private String expectNum;

	/**
	 * 启开关: (1:表示不启用, 2:表示启用)
	 */
	private Integer isStart;

	/**
	 * 是否删除: (1:表示不删除, 2:表示删除)
	 */
	private Integer isDelete;

	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
}






