package com.dy.yunying.api.resp.yyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 游戏预约进度人数表
 * @author  leisw
 * @version  2022-05-09 17:14:29
 * table: game_appointment_schedule_user
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "game_appointment_schedule_user")
public class GameAppointmentScheduleUserRes extends Model<GameAppointmentScheduleUserRes>{
	/**
	 * 主键id
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/**
	 * 预约类型：1幻兽仓角预约  2 烛光勇士预约
	 */
	@TableField(value = "type")
	private String type;
	/**
	 * 预约类型：1实际预约人数 2 标签预约人数 3.规则预约人数
	 */
	@TableField(value = "convention_type_id")
	private Integer conventionTypeId;
	/**
	 * 标签来源/规则名称
	 */
	@TableField(value = "convention_name")
	private String conventionName;
	/**
	 * 规定人数
	 */
	@TableField(value = "number")
	private Long number;
	/**
	 * 开始生成时间
	 */
	@TableField(value = "start_time")
	private Date startTime;

	/**
	 * 结束生成时间
	 */
	@TableField(value = "end_time")
	private Date endTime;

	/**
	 * 生成频率
	 */
	@TableField(value = "frequency")
	private Long frequency;

	/**
	 * 生成时间单位: (1:表示分, 2:表示小时, 3:表示天,4:表示月)
	 */
	@TableField(value = "time_unit")
	private Integer timeUnit;
	/**
	 * 最小生成人数
	 */
	@TableField(value = "min_num")
	private Long minNum;
	/**
	 * 最大生成人数
	 */
	@TableField(value = "max_num")
	private Long maxNum;

	/**
	 * 启开关: (1:表示不启用, 2:表示启用)
	 */
	@TableField(value = "is_start")
	private Integer isStart;

	/**
	 * 是否删除: (1:表示不删除, 2:表示删除)
	 */
	@TableField(value = "is_delete")
	private Integer isDelete;
}






