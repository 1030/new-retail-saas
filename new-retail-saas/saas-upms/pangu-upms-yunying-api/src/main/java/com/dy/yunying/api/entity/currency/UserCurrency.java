package com.dy.yunying.api.entity.currency;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 账号平台币信息
 * @TableName user_currency
 *
 * @Author: hjl
 * @Date: 2022/3/23 14:32
 */
@ApiModel(value = "用户平台币表")
@TableName(value ="user_currency")
@Data
public class UserCurrency implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "userid")
    private Long userid;

    /**
     * 用户账号
     */
    @TableField(value = "username")
    private String username;

    /**
     * 平台币使用范围。1：平台通用；2：父游戏；3：子游戏；4：角色
     */
    @TableField(value = "scope")
    private String scope;

    /**
     * 父游戏ID，0：通用
     */
    @TableField(value = "pgid")
    private Long pgid;

    /**
     * 子游戏ID，0：通用
     */
    @TableField(value = "game_id")
    private Long gameId;

    /**
     * 区服ID
     */
    @TableField(value = "area_id")
    private String areaId;

    /**
     * 角色ID
     */
    @TableField(value = "role_id")
    private String roleId;

    /**
     * 总计充值
     */
    @TableField(value = "total")
    private BigDecimal total;

    /**
     * 游豆
     */
    @TableField(value = "balance")
    private BigDecimal balance;

    /**
     * 临时平台币总额
     */
    @TableField(value = "tmp_total")
    private BigDecimal tmpTotal;

    /**
     * 临时平台币游豆
     */
    @TableField(value = "tmp_balance")
    private BigDecimal tmpBalance;

    /**
     * 状态，1：正常
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 删除标识，1：删除；0：未删除
     */
    @TableField(value = "deleted")
    private Integer deleted;

    /**
     * 创建人
     */
    @TableField(value = "create_id")
    private Long createId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "update_id")
    private Long updateId;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UserCurrency other = (UserCurrency) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserid() == null ? other.getUserid() == null : this.getUserid().equals(other.getUserid()))
            && (this.getUsername() == null ? other.getUsername() == null : this.getUsername().equals(other.getUsername()))
            && (this.getScope() == null ? other.getScope() == null : this.getScope().equals(other.getScope()))
            && (this.getPgid() == null ? other.getPgid() == null : this.getPgid().equals(other.getPgid()))
            && (this.getGameId() == null ? other.getGameId() == null : this.getGameId().equals(other.getGameId()))
            && (this.getAreaId() == null ? other.getAreaId() == null : this.getAreaId().equals(other.getAreaId()))
            && (this.getRoleId() == null ? other.getRoleId() == null : this.getRoleId().equals(other.getRoleId()))
            && (this.getTotal() == null ? other.getTotal() == null : this.getTotal().equals(other.getTotal()))
            && (this.getBalance() == null ? other.getBalance() == null : this.getBalance().equals(other.getBalance()))
            && (this.getTmpTotal() == null ? other.getTmpTotal() == null : this.getTmpTotal().equals(other.getTmpTotal()))
            && (this.getTmpBalance() == null ? other.getTmpBalance() == null : this.getTmpBalance().equals(other.getTmpBalance()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getCreateId() == null ? other.getCreateId() == null : this.getCreateId().equals(other.getCreateId()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateId() == null ? other.getUpdateId() == null : this.getUpdateId().equals(other.getUpdateId()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserid() == null) ? 0 : getUserid().hashCode());
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
        result = prime * result + ((getScope() == null) ? 0 : getScope().hashCode());
        result = prime * result + ((getPgid() == null) ? 0 : getPgid().hashCode());
        result = prime * result + ((getGameId() == null) ? 0 : getGameId().hashCode());
        result = prime * result + ((getAreaId() == null) ? 0 : getAreaId().hashCode());
        result = prime * result + ((getRoleId() == null) ? 0 : getRoleId().hashCode());
        result = prime * result + ((getTotal() == null) ? 0 : getTotal().hashCode());
        result = prime * result + ((getBalance() == null) ? 0 : getBalance().hashCode());
        result = prime * result + ((getTmpTotal() == null) ? 0 : getTmpTotal().hashCode());
        result = prime * result + ((getTmpBalance() == null) ? 0 : getTmpBalance().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getCreateId() == null) ? 0 : getCreateId().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateId() == null) ? 0 : getUpdateId().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userid=").append(userid);
        sb.append(", username=").append(username);
        sb.append(", scope=").append(scope);
        sb.append(", pgid=").append(pgid);
        sb.append(", gameId=").append(gameId);
        sb.append(", areaId=").append(areaId);
        sb.append(", roleId=").append(roleId);
        sb.append(", total=").append(total);
        sb.append(", balance=").append(balance);
        sb.append(", tmpTotal=").append(tmpTotal);
        sb.append(", tmpBalance=").append(tmpBalance);
        sb.append(", status=").append(status);
        sb.append(", deleted=").append(deleted);
        sb.append(", createId=").append(createId);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateId=").append(updateId);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}