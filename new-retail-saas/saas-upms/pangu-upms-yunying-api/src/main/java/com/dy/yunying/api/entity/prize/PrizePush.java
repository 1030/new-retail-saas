package com.dy.yunying.api.entity.prize;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 奖励推送表
 *
 * @author chenxiang
 * @version 2022-04-28 16:37:05 table: prize_push
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "prize_push")
public class PrizePush extends Model<PrizePush> {

	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 用户群组ID(多个逗号分割)
	 */
	@TableField(value = "user_group_id")
	private String userGroupId;

	/**
	 * 标题文本
	 */
	@TableField(value = "title")
	private String title;

	/**
	 * 说明文本
	 */
	@TableField(value = "description")
	private String description;

	/**
	 * icon图
	 */
	@TableField(value = "icon")
	private String icon;

	/**
	 * 有效开始时间
	 */
	@TableField(value = "start_time")
	private Date startTime;

	/**
	 * 有效结束时间
	 */
	@TableField(value = "end_time")
	private Date endTime;

	/**
	 * 发放频率：1每天，2隔天，3每周，4仅一次
	 */
	@TableField(value = "push_type")
	private Integer pushType;

	/**
	 * 发放频率(天：为间隔天数，周：逗号分割)
	 */
	@TableField(value = "push_interval")
	private String pushInterval;

	/**
	 * 推送节点：1首次登录成功，2每次登录成功，3首次进入游戏，4每次进入游戏
	 */
	@TableField(value = "push_node")
	private Integer pushNode;

	/**
	 * 推送目标：1-账户，2-角色
	 */
	@TableField(value = "push_target")
	private Integer pushTarget;

	/**
	 * 推送类型：1单个奖励，2组合奖励
	 */
	@TableField(value = "push_prize_type")
	private Integer pushPrizeType;

	/**
	 * 弹框形式：1单个奖励，2弹框，3图文，4通栏，5消息
	 */
	@TableField(value = "popup_mold")
	private Integer popupMold;

	/**
	 * 打开类型：0不跳转，1SDK面板，2内置浏览器，3外部链接
	 */
	@TableField(value = "open_type")
	private Integer openType;

	/**
	 * 菜单Id(多个菜单)
	 */
	@TableField(value = "menu_id")
	private String menuId;

	/**
	 * 菜单名称
	 */
	@TableField(value = "menu_title")
	private String menuTitle;

	/**
	 * 菜单编码(安卓)
	 */
	@TableField(value = "menu_code")
	private String menuCode;
	/**
	 * 菜单编码(苹果)
	 */
	@TableField(value = "menu_code_ios")
	private String menuCodeIos;
	/**
	 * 二级菜单编码(安卓)
	 */
	@TableField(value = "second_menu_code")
	private String secondMenuCode;
	/**
	 * 二级菜单编码(苹果)
	 */
	@TableField(value = "second_menu_code_ios")
	private String secondMenuCodeIos;

	/**
	 * 来源类型(1红包活动 2签到活动)
	 */
	@TableField(value = "skip_mold")
	private Integer skipMold;

	/**
	 * 跳转位置 如：红包类型 1：等级红包、2：充值红包、3：邀请红包、4：定制红包
	 */
	@TableField(value = "skip_belong_type")
	private Integer skipBelongType;

	/**
	 * 来源id, 如：红包活动id
	 */
	@TableField(value = "skip_id")
	private Long skipId;

	/**
	 * 跳转地址
	 */
	@TableField(value = "skip_url")
	private String skipUrl;
	/**
	 * 跳转地址（IOS）
	 */
	@TableField(value = "skip_url_ios")
	private String skipUrlIOS;

	/**
	 * 展示方式(1横屏展示，2竖屏展示)
	 */
	@TableField(value = "show_type")
	private Integer showType;

	/**
	 * 展示顺序：降序
	 */
	@TableField(value = "show_sort")
	private Integer showSort;


	/**
	 * 状态：1 启用，2 停用
	 */
	@TableField(value = "status")
	private Integer status;

	/**
	 * 是否删除 0否 1是
	 */
	@TableLogic
	@TableField(value = "deleted")
	private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;

	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

}
