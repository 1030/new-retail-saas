package com.dy.yunying.api.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author yuwenfeng
 * @description: 公告数据入参
 * @date 2022/3/7 18:01
 */
@Data
public class NoticeDataReq {

	private Long current = 1L;
	private Long size = 10000L;

	private String titles;
	private String columns;

	@ApiModelProperty(value = "开始时间(格式：时间戳)")
	private Long startTime;

	@ApiModelProperty(value = "结束时间(格式：时间戳)")
	private Long finishTime;

	@ApiModelProperty(value = "父游戏范围集合")
	private String parentGameArr;

	@ApiModelProperty(value = "子游戏范围集合")
	private String gameArr;

	@ApiModelProperty(value = "区服范围集合")
	private String areaArr;

	@ApiModelProperty(value = "主渠道范围集合")
	private String parentChannelArr;

	@ApiModelProperty(value = "子渠道范围集合")
	private String childChannelArr;

	@ApiModelProperty("公告ID")
	private String noticeId;

	@ApiModelProperty("周期类型 ：1 按天 2 按周  3  按月 4 汇总（默认）")
	private Integer cycleType = 4;

	public String getPeriod() {
		String period = "summary";
		switch (cycleType) {
			case 1:
				period = "dx_part_date";
				break; //可选
			case 2:
				period = "week_date";
				//语句
				break; //可选
			case 3:
				period = "month";
				break; //可选
			case 4:
				period = "summary";
				break; //可选
			//你可以有任意数量的case语句
			default: //可选
				//语句
		}
		return period;
	}

	@ApiModelProperty("类别 多个用逗号分隔【主游戏：pgid，子游戏：gameid ,主渠道：parentchl，子渠道：chl，公告ID：notice_id】")
	private String queryColumn;

	@ApiModelProperty("排序方式")
	private String sortType;

	@ApiModelProperty("排序字段")
	private String sortColumn;
}
