package com.dy.yunying.api.entity.hongbao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 活动中心与游戏关系表
 * @author  chenxiang
 * @version  2022-06-18 11:46:38
 * table: hb_activity_center_game
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "hb_activity_center_game")
public class HbActivityCenterGame extends Model<HbActivityCenterGame>{
	/**
	 * 主键ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 活动中心ID
	 */
	@TableField(value = "activity_center_id")
	private Long activityCenterId;
	/**
	 * 主游戏ID
	 */
	@TableField(value = "parent_game_id")
	private Long parentGameId;
	/**
	 * 子游戏ID
	 */
	@TableField(value = "child_game_id")
	private Long childGameId;
	/**
	 * 游戏层级(1主游戏 2子游戏)
	 */
	@TableField(value = "game_range")
	private Integer gameRange;
	/**
	 * 是否删除(0否 1是)
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

