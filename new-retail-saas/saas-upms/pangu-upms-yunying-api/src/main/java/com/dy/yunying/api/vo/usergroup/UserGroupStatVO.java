package com.dy.yunying.api.vo.usergroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * hezz 2022-05-17 17:00:00
 * */
@Data
public class UserGroupStatVO implements Serializable {

	private static final long serialVersionUID = 3799186434822397683L;

	/**用户群组ID*/
	private Long groupId;
	/**用户群组的公告数或奖励数*/
	private Integer nums = 0;

	@ApiModelProperty(value = "公告数量")
	private Integer noticeNums = 0;

	@ApiModelProperty(value = "奖励数量")
	private Integer prizeNums = 0;

	public UserGroupStatVO() {
	}

	public UserGroupStatVO(Long groupId) {
		this.groupId = groupId;
	}
}
