package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @ClassName RetentionKpiEnum
 * @Description todo
 * @Author nieml
 * @Time 2021/6/21 17:12
 * @Version 1.0
 **/
public enum ExtendPackageEnum {

	NOT_UPDATE("NOT_UPDATE","未更新"),
	CREATING("CREATING","创建中"),
	UPDATING("UPDATING","更新中"),
	PUBLISHED("PUBLISHED","已发布"),
	CREATION_FAILED("CREATION_FAILED","创建失败"),
	UPDATE_FAILED("UPDATE_FAILED","更新失败");

	private ExtendPackageEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(String type) {
		if (StringUtils.isBlank(type)){
			return "-";
		}
		for (ExtendPackageEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return "-";
	}

	private String type;

	private String name;

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
