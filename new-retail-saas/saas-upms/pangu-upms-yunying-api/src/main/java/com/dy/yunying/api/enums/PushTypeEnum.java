package com.dy.yunying.api.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @author chenxiang
 * @className PushTypeEnum
 * @date 2022-4-25 17:37
 */
public enum PushTypeEnum {

	EVERYDAY(1, "每天"), THENEXTDAY(2, "隔天"), WEEKLY(3, "每周"), ONLYONCE(4, "仅一次");

	private Integer type;

	private String name;

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	private PushTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public static String getName(Integer type) {
		if (Objects.isNull(type)) {
			return null;
		}
		for (PushTypeEnum ele : values()) {
			if (ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return null;
	}

}
