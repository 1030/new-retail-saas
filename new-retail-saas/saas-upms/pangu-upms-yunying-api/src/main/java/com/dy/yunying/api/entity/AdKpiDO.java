package com.dy.yunying.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.dy.yunying.api.constant.OsEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @ClassName AdKpi
 * @Description 绩效管理-kpi
 * @Author nieml
 * @Time 2021/7/8 10:52
 * @Version 1.0
 **/

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "绩效管理-kpi")
public class AdKpiDO extends Model<AdKpiDO> {

	/**
	 * 主键
	 */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value="主键")
	private Integer id;

	/**
	 * 层级：1 部门；2 组别；3 投放人
	 */
	@ApiModelProperty(value="层级：1 部门；2 组别；3 投放人")
	private Integer hierarchy;
	private String hierarchyStr;

	/**
	 * 所选层级的id：部门id|组别id|投放人id
	 */
	@ApiModelProperty(value="所选层级的id：部门id|组别id|投放人id")
	private Integer hierarchyId;

	@ApiModelProperty(value="所选层级的名称")
	private String hierarchyName;

	/**
	 * 年月
	 */
	@ApiModelProperty(value="年月")
	private Integer yearMonth;
	private String yearMonthStr;

	public String getYearMonthStr() {
		String yearMonthString = String.valueOf(yearMonth);
		return yearMonthString.substring(0,4).concat("年").concat(yearMonthString.substring(4,6).concat("月"));
	}

	/**
	 * 主游戏id
	 */
	@ApiModelProperty(value="主游戏id")
	private Integer pgid;
	private String parentGameName;

	/**
	 * 系统
	 */
	@ApiModelProperty(value="系统")
	private Integer os;
	public String getOsStr () {
		String osStr = OsEnum.getName(os);
		return osStr;
	}

	/**
	 * 目标量级
	 */
	@ApiModelProperty(value="目标量级")
	private Long targetLevel;

	/**
	 * 目标ROI
	 */
	@ApiModelProperty(value="目标ROI")
	private BigDecimal targetRoi;

	/**
	 * 目标ROI-30
	 */
		@ApiModelProperty(value="目标ROI-30")
	private BigDecimal targetRoi30;

	/**
	 * 目标新用户月流水
	 */
	@ApiModelProperty(value="目标新用户月流水")
	private Long targetNewuserMonthWater;

	/**
	 * 目标老用户月流水
	 */
	@ApiModelProperty(value="目标老用户月流水")
	private Long targetOlduserMonthWater;

	/**
	 * 实际量级
	 */
	@ApiModelProperty(value="实际量级")
	private Long realLevel;

	/**
	 * 实际ROI
	 */
	@ApiModelProperty(value="实际ROI")
	private BigDecimal realRoi;

	/**
	 * 实际ROI-30
	 */
	@ApiModelProperty(value="实际ROI-30")
	private BigDecimal realRoi30;

	/**
	 * 实际新用户月流水
	 */
	@ApiModelProperty(value="实际新用户月流水")
	private Long realNewuserMonthWater;

	/**
	 * 实际老用户月流水
	 */
	@ApiModelProperty(value="实际老用户月流水")
	private Long realOlduserMonthWater;

	private String colorLevel;
	private String colorRoi;
	private String colorRoi30;
	private String colorNewuserMonthWater;
	private String colorOlduserMonthWater;

}
