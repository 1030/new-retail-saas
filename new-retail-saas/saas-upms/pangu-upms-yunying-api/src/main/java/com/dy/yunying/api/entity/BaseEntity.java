package com.dy.yunying.api.entity;

import java.io.Serializable;

/**
 *
 * @Author: hjl
 * @Date: 2020/7/21 13:39
 */
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public BaseEntity() {
    }

}