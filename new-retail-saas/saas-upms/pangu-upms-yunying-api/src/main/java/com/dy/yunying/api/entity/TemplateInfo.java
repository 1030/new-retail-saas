package com.dy.yunying.api.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author sunyq
 * @date 2022/9/2 15:05
 */
@Data
public class TemplateInfo implements Serializable {

	private String filter;

	private List<String> customColumn;
}
