package com.dy.yunying.api.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * class info
 *
 * @author sunyouquan
 * @date 2022/5/12 11:56
 */
public enum PrizeBeanTypeEnum {
	COMMON(1, "常规"),

	TEMP(2, "临时");

	private Integer type;

	private String name;

	PrizeBeanTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	@JsonCreator
	public static PrizeBeanTypeEnum getPrizeBeanTypeEnum(Integer value) {
		for (PrizeBeanTypeEnum prizeBeanTypeEnum : values()) {
			if (prizeBeanTypeEnum.type.equals(value)) {
				return prizeBeanTypeEnum;
			}
		}
		return null;
	}

	@JsonValue
	public Integer getType() {
		return type;
	}

	@JsonValue
	public String getName() {
		return name;
	}

}
