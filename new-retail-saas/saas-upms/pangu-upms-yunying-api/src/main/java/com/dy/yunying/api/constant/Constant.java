package com.dy.yunying.api.constant;

/**
 * @author ：lile
 * @date ：2021/5/10 10:36
 * @description：
 * @modified By：
 */
public class Constant {


	/**
	 * redis缓存前缀，redis 前面都需要带
	 */
	public static final String REDIS_CACHE_PREFIX = "pangu:upms:yunying:yyz:";

	public static final String RESULT_STATUS_SUCCESS = "success";

	/**
	 * 二级登录加密key
	 */
	public static final String SECOND_LOGIN_KEY = "3399dgvbj";

	public static final String[] nums = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "60", "90", "120", "150", "180"};

	public static final String REDIS_KEY_IP_CACHE_PREFIX = "platform_ip_cache_key_";//ip缓存key

	/**
	 * 封禁功能
	 */
	public static final String REDIS_KEY_PROHIBIT_ = "3367v2_api_prohibit_";   // 封禁缓存

	public static final String AREA_COUNTRY_KEY = "ip_area_country"; //国家

	public static final String AREA_PROVINCE_KEY_PREFIX = "ip_area_country_";//省份

	public static final String AREA_CITY_KEY_PREFIX = "ip_area_city_province_";//城市

	public static final String COUNTRY_LEVEL = "0";

	public static final String PROVINCE_LEVEL = "1";

	public static final String CITY_LEVEL = "2";
	/**
	 * 右小括号
	 */
	public static final String RIGHT_SMALL_BRANCKET = ")";
	/**
	 * 英文逗号
	 */
	public static final String COMMA = ",";
	/**
	 * 英文分号
	 */
	public static final String SEMICOLON = ";";
	/**
	 * 英文冒号
	 */
	public static final String CONLON = ":";

	/**
	 * 空字符串
	 */
	public static final String EMPTTYSTR = "";
	/**
	 * 百分号
	 */
	public static final String PERCENT = "%";
	/**
	 *
	 */
	public static final String POINT = ".";
	/**
	 * 路径分隔符
	 */
	public static final String SPT = "/";
	/**
	 * UTF-8编码
	 */
	public static final String UTF8 = "UTF-8";
	/**
	 * 默认模板后缀名
	 */
	public static final String DEFAULT_TMPLATE_SUFFIX = ".html";
	/**
	 *
	 */
	public static final String IMG_URL = "images";
	/**
	 *
	 */
	public static final String MODULE_TPL_PAGE = "mpage/";
	/**
	 *
	 */
	public static final String JSON_FILE_URL = "json";
	/**
	 *
	 */
	public static final String PAGE_SIZE = "pageSize";
	/**
	 * 设置cookie有效期是1天，根据需要自定义
	 */
	public final static int USER_TOKEN_EXPIRE_SECOND = 60 * 60 * 24;
	public final static String COOKIE_SELECT_SITE_KEY = "cookie_select_site_key";
	/**
	 * 广告账户的名称在redis中存储的前缀
	 */
	public static String OCEANENGINE_ADACCOUNT_NAME_KEY_PRIX_ = "oceanengine_adaccount_name_key_prix_";

	/**
	 * 广告账户的名称在redis中存储的前缀   uc、头条、广点通公用
	 */
	public static String COMMON_ADACCOUNT_NAME_KEY_PRIX_ = "common_adaccount_name_key_prix_";

	/**
	 * 广告计划的信息在redis中存储的前缀
	 */
	public static String OCEANENGINE_AD_INFO_KEY_PRIX_ = "oceanengine_ad_info_key_prix_";

	/**
	 * 广告创意的信息在redis中存储的前缀
	 */
	public static String OCEANENGINE_CREATIVE_INFO_KEY_PRIX_ = "oceanengine_creative_info_key_prix_";
	/**
	 * 红包活动发送消息rediskey
	 */
	public static String HB_ACTIVITY_MSG = "hb_activity_msg_";

	/**
	 * 表头字段隐藏控制在redis中存储的前缀
	 */
	public static String HIDE_COLS_ = "hide_cols_";

	public static String USER_NAME_PRIX = "uk_s_";

	public static final int DEL_YES = 1;
	public static final int DEL_NO = 0;

	public static final int APPLY_WAIT = 1;//待审核
	public static final int APPLY_SUCC = 2;//通过
	public static final int APPLY_FAIL = 3;//拒绝

	public static String ORDER_DESC = "DESC";//降序
	public static String ORDER_ASC = "ASC";//正序

	public static  final Integer TYPE_1=15;
	public static  final Integer TYPE_2=2;
	public static String CMS_APPLET_LABEL = "cms_applet_label";//主键标签

	public static final String DEFAULT_VALUE = "0.00%";// 为百分比的值拼接百分号

	public static final String REDIS_KEY_API_LOGIN_USER_ = "3367v2_user_api_login_";   // 已登录用户
}
