package com.dy.yunying.api.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MaterialTopVo {

	private String cid;

	private String landingPageId;

	private String platformId;

	private String materialName;

	private String materialUrl;

	private String materialId;

	private String title;

	private String exTitle;

	private String pageUrl;

	private String pageName;

	private String pageImage;

	private String imageUrl;

	private String format;

	private String advertiserId;

	private String tmId;

}
