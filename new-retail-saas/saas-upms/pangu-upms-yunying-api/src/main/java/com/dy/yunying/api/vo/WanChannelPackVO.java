package com.dy.yunying.api.vo;

import com.dy.yunying.api.enums.ExtendPackageEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

/**
 * wan_channel_pack
 *
 * @author hjl
 * @date 2020-07-24 13:50:03
 */
@Getter
@Setter
public class WanChannelPackVO {

	/**
	 * 分包渠道ID
	 */
	private Long packId;

	/**
	 * 游戏ID
	 */
	private Long gameId;

	/**
	 * 母包类型：1-运营母包；2-云微端；
	 */
	private Integer packType;

	/**
	 * 游戏名称
	 */
	private String gameName;

	/**
	 * 游戏包名
	 */
	private String pkName;

	/**
	 *
	 */
	private Long versionId;

	/**
	 * 分包编码（游戏编码）
	 */
	private String code;

	/**
	 * 分包编码 名称
	 */
	private String codeName;

	/**
	 * 所属渠道编码（子渠道）
	 */
	private String chlCode;

	private String parentCode;

	/**
	 * 游戏包路径
	 */
	private String path;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 状态：1：生成中，2：已生成；3：失效
	 */
	private Integer status;

	/**
	 * 头条状态
	 */
	private String ttStatus;

	/**
	 * 推广类型（1信息流 2短视频）
	 */
	private Integer spreadType;

	/**
	 * 结算类型(KOL(一口价) KOL(CPM按次))
	 */
	private String settleType;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建人名称
	 */
	private String creatorName;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

	private String detectionUrl;

	/**
	 * 展示监测链接
	 */
	private String displayMonitorUrl;

	private String app_file_url_root;

	private String url;

	/**
	 * 星图监测链接
	 */
	private String xtMonitorUrl;

	public String getAdUrl() {
		if (StringUtils.isNotBlank(path)) {
			return "/pack" + path;
		}
		return "";
	}

	public String getStatusStr() {
		String statusStr = null;
		switch (status) {
			case 1:
				statusStr = "生成中";
				break;
			case 2:
				statusStr = "已生成";
				break;
			case 3:
				statusStr = "失效";
				break;
			default:
				statusStr = "";
		}
		return statusStr;
	}

	public String getTtStatus() {
		return ExtendPackageEnum.getName(ttStatus);
	}

	//担心前台对象自动获取getTtStatus 故，新增一个方法
	public String getTtStatusStr() {
		return this.ttStatus;
	}
}