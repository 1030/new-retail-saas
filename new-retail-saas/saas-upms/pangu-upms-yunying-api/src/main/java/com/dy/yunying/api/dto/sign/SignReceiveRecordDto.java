package com.dy.yunying.api.dto.sign;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 奖品领取记录表
 * @author  chengang
 * @version  2021-12-01 10:14:36
 * table: sign_receive_record
 */
@Data
public class SignReceiveRecordDto implements Serializable {
	
	//columns START
		//主键id
		private Long id;
		//活动ID
		private Long activityId;
		//业务ID
		private Long businessId;
		//类型  1签到 2充值任务
		private Integer businessType;
		//业务code
		private String businessCode;
		//活动名称
		private String activityName;
		//奖品名称
		private String prizeName;
		//类型  1游戏物品 2代金券
		private Integer prizeType;
		//领取序号
		private Integer receiveSn;
		//用户ID
		private Long userId;
		//用户名
		private String userName;
		//父游戏ID
		private Long parentGameId;
		//父游戏名称
		private String parentGameName;
		//子游戏ID
		private Long subGameId;
		//区服ID
		private Long areaId;
		//区服名称
		private String areaName;
		//角色ID
		private String roleId;
		//角色名称
		private String roleName;
		//领取时间
		private Date receiveTime;
		//是否删除  0否 1是
		private Integer deleted;
		//创建时间
		private Date createTime;
		//修改时间
		private Date updateTime;
		//创建人
		private Long createId;
		//修改人
		private Long updateId;
	//columns END 数据库字段结束
	
	
}


