package com.dy.yunying.api.dto;

import com.dy.yunying.api.entity.WanChannelPackRule;
import lombok.Data;

import java.util.List;

@Data
public class BatchSettingRuleDto {
	/**
	 * 素材id 集合 ,隔开
	 */
	private String codes;

	/**
	 * 标签id集合 ,隔开
	 */
	private Integer backhaulType;


	private List<WanChannelPackRule> packRuleList;
}
