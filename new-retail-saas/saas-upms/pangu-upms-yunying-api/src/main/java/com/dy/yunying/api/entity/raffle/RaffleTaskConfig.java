package com.dy.yunying.api.entity.raffle;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 抽奖任务配置表
 * @author  chenxiang
 * @version  2022-11-08 15:59:48
 * table: raffle_task_config
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "raffle_task_config")
public class RaffleTaskConfig extends Model<RaffleTaskConfig>{
	/**
	 * 主键id
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/**
	 * 活动ID
	 */
	@TableField(value = "activity_id")
	private Long activityId;
	/**
	 * 任务名称/目标
	 */
	@TableField(value = "task_name")
	private String taskName;
	/**
	 * 任务达成条件类型：1-角色等级，2-角色等级和创角时间，3-角色累计充值，4-角色首次充值，5-角色单笔充值满额，6-邀请好友成功注册游戏，7-邀请好友首次提现成功，8-邀请好友累计充值，9-邀请好友创角N天内达到N级，11-角色单笔充值等额，12-角色连续登录N天，13-创角N天累计充值X元，14-创建N天首次充值，15-开服N天达到X级，16-开服N天累计充值X元，17-开服N天首次充值，18-游戏任务，19-角色累计登录N天；20-红包活动提现N次；
	 */
	@TableField(value = "condition_type")
	private Integer conditionType;
	/**
	 * 任务达成条件：角色等级
	 */
	@TableField(value = "condition_role_level")
	private Integer conditionRoleLevel;
	/**
	 * 任务达成条件：创角天数/开服天数；
	 */
	@TableField(value = "condition_role_days")
	private Integer conditionRoleDays;
	/**
	 * 任务达成条件：角色充值金额
	 */
	@TableField(value = "condition_role_recharge")
	private BigDecimal conditionRoleRecharge;
	/**
	 * 游戏任务ID
	 */
	@TableField(value = "condition_task_id")
	private String conditionTaskId;
	/**
	 * 条件达成次数
	 */
	@TableField(value = "condition_reach_num")
	private Integer conditionReachNum;
	/**
	 * 任务可完成次数
	 */
	@TableField(value = "condition_limit_num")
	private Integer conditionLimitNum;
	/**
	 * 奖励类型：1-抽奖次数；
	 */
	@TableField(value = "reward_type")
	private Integer rewardType;
	/**
	 * 奖励数量
	 */
	@TableField(value = "reward_num")
	private Integer rewardNum;
	/**
	 * 是否删除：0-否；1-是；
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

