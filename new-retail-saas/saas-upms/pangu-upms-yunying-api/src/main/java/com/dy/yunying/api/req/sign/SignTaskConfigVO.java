package com.dy.yunying.api.req.sign;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Title null.java
 * @Package com.dy.yunying.api.req.sign
 * @Author 马嘉祺
 * @Date 2021/12/1 10:46
 * @Description
 */
@Data
@Accessors(chain = true)
public class SignTaskConfigVO {

	/**
	 * 任务主键
	 */
	@NotNull(message = "主键不能为空", groups = {Edit.class})
	private Long id;

	/**
	 * 活动ID
	 */
	@NotNull(message = "活动ID不能为空", groups = {Save.class})
	private Long activityId;

	/**
	 * 任务类型：2-充值任务，...
	 */
	@NotNull(message = "任务类型不能为空", groups = {Save.class})
	private Integer taskType;

	/**
	 * 领取条件类型：10-角色当日累计充值，...
	 */
	@NotNull(message = "领取条件类型不能为空", groups = {Save.class})
	private Integer conditionType;

	/**
	 * 领取条件：角色充值金额
	 */
	private Integer conditionRoleRecharge;

	/**
	 * 任务开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message = "任务开始时间不能为空", groups = {Save.class})
	private Date startTime;

	/**
	 * 任务结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message = "任务结束时间不能为空", groups = {Save.class})
	private Date endTime;

	/**
	 * 额外奖励类型：1-补签次数
	 */
	@NotNull(message = "额外奖励类型不能为空", groups = {Save.class})
	private Integer extraBonusType;

	/**
	 * 额外奖励数量
	 */
	private Integer extraBonusNum;

	/**
	 * 奖品类型：1-游戏物品；2-代金券；3-游豆；
	 */
	@NotNull(message = "奖品类型不能为空", groups = {Save.class})
	private Integer prizeType;

	/**
	 * 礼包码类型：1唯一码，2通用码
	 */
	private Integer giftType;

	/**
	 * 礼包通用码
	 */
	private String giftCode;

	/**
	 * 礼包总数量
	 */
	private Integer giftAmount;

	/**
	 * 代金券名称
	 */
	private String cdkName;

	/**
	 * 代金券金额
	 */
	private BigDecimal cdkAmount;

	/**
	 * 代金券限制金额
	 */
	private BigDecimal cdkLimitAmount;

	/**
	 * 有效过期类型：1-固定结束时间；2-领取后过期天数；
	 */
	private Integer cdkExpireType;

	/**
	 * 代金券有效开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date cdkStartTime;

	/**
	 * 代金券有效结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date cdkEndTime;

	/**
	 * 代金券领取后过期天数
	 */
	private Integer cdkExpireDays;

	/**
	 * 代金券限制类型：1-无门槛；2-指定游戏；3-指定角色；
	 */
	private Integer cdkLimitType;

	/**
	 * 游豆价值
	 */
	private BigDecimal currencyAmount;

	/**
	 * 游豆类型：1-永久游豆；2-临时游豆；
	 */
	private Integer currencyType;

	/**
	 * 游豆过期类型：1-固定结束时间；2-领取后过期天数；
	 */
	private Integer currencyExpireType;

	/**
	 * 游豆固定过期时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date currencyExpireTime;

	/**
	 * 游豆领取后过期天数
	 */
	private Integer currencyExpireDays;

	/**
	 * 物品ID，多个ID使用逗号分隔
	 */
	private String goodsIdArr;

	private String goodsNumArr;

	/**
	 * 物品ID数组
	 */
	private Long[] goodsIds;

	/**
	 * 物品数量
	 */
	private Integer[] goodsNums;

	/**
	 * 礼包码文件
	 */
	private MultipartFile giftBagFile;

	public interface Save {
	}

	public interface Edit {
	}

}
