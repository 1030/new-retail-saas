package com.dy.yandi.biz.controller;

import com.dy.yandi.api.client.demo.service.DemoService;
import com.dy.yandi.biz.component.bean.YandiProperties;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description
 * @Author chengang
 * @Date 2021/9/3
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("demo")
@Inner(value = false)
public class DemoController {

	private final DemoService demoService;

	@Resource
	private YandiProperties yandiProperties;

	@RequestMapping("index")
	public R index() {
		demoService.sayHello("炎帝 ");
		return R.ok(yandiProperties.getName());
	}

}
