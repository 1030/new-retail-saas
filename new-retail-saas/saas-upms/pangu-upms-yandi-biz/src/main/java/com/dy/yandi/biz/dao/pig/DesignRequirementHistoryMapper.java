package com.dy.yandi.biz.dao.pig;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yandi.api.client.requirement.model.DesignRequirementHistory;
import org.apache.ibatis.annotations.Mapper;

/**
 * 需求历史记录表
 * @author  chengang
 * @version  2021-09-28 13:44:30
 * table: design_requirement_history
 */
@Mapper
public interface DesignRequirementHistoryMapper extends BaseMapper<DesignRequirementHistory> {
	
}


