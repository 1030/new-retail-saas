package com.dy.yandi.biz.service.requirement;

import com.dy.yandi.api.client.demo.model.entity.DataChartDO;
import com.dy.yandi.api.client.demo.model.vo.DataChartVO;
import com.dy.yandi.biz.dao.clicktmp.DataChartDao;
import com.dy.yandi.biz.service.DataChartService;
import com.pig4cloud.pig.admin.api.entity.SysDictItem;
import com.pig4cloud.pig.admin.api.feign.RemoteDictService;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Title null.java
 * @Package com.dy.yandi.biz.service.requirement
 * @Author 马嘉祺
 * @Date 2021/10/8 14:35
 * @Description
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DataChartServiceImpl implements DataChartService {

	private final String DESIGN_MANAGER_DICT_TYPE = "design_manager";

	private final DataChartDao dataChartDao;

	private final RemoteDictService remoteDictService;

	/**
	 * 获取kpi消耗信息
	 *
	 * @param dataCharts
	 * @return
	 */
	@Override
	public List<DataChartDO> getKpiCost(DataChartVO dataCharts) {
		return dataChartDao.queryMaterialCost(dataCharts);
	}

	/**
	 * 获取素材计划数
	 *
	 * @param dataCharts
	 * @return
	 */
	@Override
	public List<DataChartDO> getMaterialAdCount(DataChartVO dataCharts) {
		return dataChartDao.getMaterialAdCount(dataCharts);
	}

	/**
	 * 获取卖点使用统计列表
	 *
	 * @param dataCharts
	 * @return
	 */
	@Override
	public List<DataChartDO> getUsedSellingPointList(DataChartVO dataCharts) {
		return dataChartDao.getUsedSellingPointList(dataCharts);
	}

	/**
	 * 获取个人素材卖点分布
	 *
	 * @param dataCharts
	 * @return
	 */
	@Override
	public List<DataChartDO> getSellingPointDistributed(DataChartVO dataCharts) {
		return dataChartDao.getSellingPointDistributed(dataCharts);
	}

	/**
	 * 获取综合信息统计列表
	 *
	 * @param dataCharts
	 * @return
	 */
	@Override
	public List<DataChartDO> getMixedStatisticsList(DataChartVO dataCharts) {
		Integer deptId = Objects.requireNonNull(SecurityUtils.getUser()).getDeptId();
		if (null == deptId) {
			return Collections.emptyList();
		}
		List<DataChartDO> list = dataChartDao.getMixedStatisticsList(dataCharts, deptId, Collections.emptyList());
		if (!this.isDesignManager()) {
			return list;
		}
		List<DataChartDO> dataChartList = dataChartDao.getMixedStatisticsList(dataCharts, deptId, Collections.singleton("makerId"));
		list.addAll(dataChartList);
		return list;
	}

	/**
	 * 当前用户是否是设计主管
	 * @return
	 */
	@Override
	public boolean isDesignManager() {
		final List<SysDictItem> items = Optional.ofNullable(remoteDictService.getDictByType4Inner(SecurityConstants.FROM_IN,DESIGN_MANAGER_DICT_TYPE)).filter(v -> 0 == v.getCode()).map(R::getData).orElse(Collections.emptyList());
//		return !items.isEmpty() && SecurityUtils.getRoles().contains(Integer.parseInt(items.get(0).getValue()));
		List<Integer> dictList = items.stream().map(SysDictItem::getValue).map(v -> Integer.parseInt(v)).collect(Collectors.toList());
		List<Integer> list = SecurityUtils.getRoles().stream().filter(v -> dictList.contains(v)).collect(Collectors.toList());
		return !items.isEmpty() && !list.isEmpty();
	}

}
