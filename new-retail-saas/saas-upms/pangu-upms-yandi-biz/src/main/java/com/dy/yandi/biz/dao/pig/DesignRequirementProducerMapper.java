package com.dy.yandi.biz.dao.pig;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yandi.api.client.requirement.model.DesignRequirementProducer;
import com.dy.yandi.api.client.requirement.model.dto.DesignRequirementProducerDto;
import com.dy.yandi.api.client.requirement.model.vo.DesignRequirementProducerVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 需求制作者表
 * @author  chengang
 * @version  2021-09-28 13:44:36
 * table: design_requirement_producer
 */
@Mapper
public interface DesignRequirementProducerMapper extends BaseMapper<DesignRequirementProducer> {

	List<DesignRequirementProducerDto> selectByList(DesignRequirementProducerVo vo);
	
}


