package com.dy.yandi.biz.service.demo;

import com.dy.yandi.api.client.demo.service.DemoService;
import com.dy.yandi.biz.component.bean.YandiProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Description //TODO
 * @Author chengang
 * @Date 2021/9/3
 */
@Service
@RequiredArgsConstructor
public class DemoServiceImpl implements DemoService {

	@Resource
	private YandiProperties yandiProperties;

	@Override
	public void sayHello(String name) {
		System.out.println(name + yandiProperties.getName());
	}
}
