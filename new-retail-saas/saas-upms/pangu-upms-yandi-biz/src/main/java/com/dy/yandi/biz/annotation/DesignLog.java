package com.dy.yandi.biz.annotation;

import java.lang.annotation.*;

/**
 * @ClassName DesignLog.java
 * @author cx
 * @version 1.0.0
 * @Time 2021/10/11 20:52
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DesignLog {

	/**
	 * 描述
	 * @return {String}
	 */
	String value();

}
