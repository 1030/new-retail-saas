package com.dy.yandi.biz.dao.pig;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yandi.api.client.requirement.model.DesignBonus;
import com.dy.yandi.api.client.requirement.model.dto.PerformanceDto;
import com.dy.yandi.api.client.requirement.model.dto.PerformanceVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 绩效奖金表
 * @author  chengang
 * @version  2021-09-28 13:43:52
 * table: design_bonus
 */
@Mapper
public interface DesignBonusMapper extends BaseMapper<DesignBonus> {

	IPage<PerformanceVo> getPerformance(IPage<PerformanceVo> pge, @Param("dto") PerformanceDto dto );

	/**
	 * 计算团队分用-获取当前组别下所有人的数据
	 * @param dto
	 * @return
	 */
	List<PerformanceVo> getPerformanceNoPage(@Param("dto") PerformanceDto dto);

	/**
	 * 定时执行获取当前月所有人的已完成需求的绩效，用户没有做需求的不记录数据
	 * @param dto
	 * @return
	 */
	List<PerformanceVo> getPerformance4Task(@Param("dto") PerformanceDto dto);

}


