package com.dy.yandi.biz.dao.pig;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yandi.api.client.requirement.model.DesignSellingPoint;
import org.apache.ibatis.annotations.Mapper;

/**
 * 需求卖点表
 * @author  chengang
 * @version  2021-09-28 13:44:43
 * table: design_selling_point
 */
@Mapper
public interface DesignSellingPointMapper extends BaseMapper<DesignSellingPoint> {
	
}


