package com.dy.yandi.biz.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dy.yandi.api.client.requirement.model.DesignRequirement;
import com.dy.yandi.api.client.requirement.model.vo.DesignMaterialVo;
import com.dy.yandi.api.client.requirement.model.vo.DesignRequirementVo;
import com.dy.yandi.api.client.requirement.service.DesignRequirementService;
import com.dy.yandi.biz.annotation.DesignLog;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * 需求表
 * @author  chengang
 * @version  2021-09-28 13:44:24
 * table: design_requirement
 */
@RestController
@RequestMapping("/design")
@RequiredArgsConstructor
public class DesignRequirementController {

	private final DesignRequirementService designRequirementService;

	/**
	 * 需求列表 - 分页
	 * @param vo
	 * @return
	 */
	@RequestMapping("/getPage")
	public R getPage(@RequestBody DesignRequirementVo vo){
		return designRequirementService.getPage(vo);
	}
	/**
	 * 我的需求 - 分页
	 * @param vo
	 * @return
	 */
	@RequestMapping("/getMyNeedPage")
	public R getMyNeedPage(@RequestBody DesignRequirementVo vo){
		return designRequirementService.getMyNeedPage(vo);
	}
	/**
	 * 已完成 - 分页
	 * @param vo
	 * @return
	 */
	@RequestMapping("/getComplete")
	public R getComplete(@RequestBody DesignRequirementVo vo){
		return designRequirementService.getComplete(vo);
	}
	/**
	 * 待审核 - 分页
	 * @param vo
	 * @return
	 */
	@RequestMapping("/getAudit")
	public R getAudit(@RequestBody DesignRequirementVo vo){
		return designRequirementService.getAudit(vo);
	}
	/**
	 * 需求操作記錄
	 * @param vo
	 * @return
	 */
	@RequestMapping("/getRecordList")
	public R getRecordList(@RequestBody DesignRequirementVo vo){
		if (StringUtils.isBlank(vo.getId())){
			return R.failed("需求ID不能为空");
		}
		return designRequirementService.getRecordList(vo);
	}
	/**
	 * 发布需求
	 * @param vo
	 * @return
	 */
	@DesignLog("发起了需求")
	@RequestMapping("/add")
	public R addNeed(DesignRequirementVo vo){
		vo.setId(null);
		// 验证参数
		R result = checkParam(vo);
		if (0 != result.getCode()){
			return result;
		}
		return designRequirementService.addEditNeed(vo);
	}
	/**
	 * 编辑需求
	 * @param vo
	 * @return
	 */
	@DesignLog("编辑了需求")
	@RequestMapping("/edit")
	public R editNeed(DesignRequirementVo vo){
		if (StringUtils.isBlank(vo.getId())){
			return R.failed("需求唯一ID不能为空");
		}
		// 验证参数
		R result = checkParam(vo);
		if (0 != result.getCode()){
			return result;
		}
		return designRequirementService.addEditNeed(vo);
	}
	/**
	 * 驳回
	 * @param vo
	 * @return
	 */
	@DesignLog("驳回了需求")
	@RequestMapping("/reject")
	public R reject(@RequestBody DesignRequirementVo vo){
		if (StringUtils.isBlank(vo.getId())){
			return R.failed("需求唯一ID不能为空");
		}
		if (StringUtils.isBlank(vo.getForbidReason())){
			return R.failed("请输入驳回原因");
		}
		if (vo.getForbidReason().length() > 200){
			return R.failed("驳回原因限制200字以内");
		}
		vo.setStatus("2");
		return designRequirementService.editNeedStatus(vo);
	}
	/**
	 * 通过
	 * @param vo
	 * @return
	 */
	@DesignLog("审核通过了需求")
	@RequestMapping("/pass")
	public R pass(@RequestBody DesignRequirementVo vo){
		if (StringUtils.isBlank(vo.getId())){
			return R.failed("需求唯一ID不能为空");
		}
		if (StringUtils.isBlank(vo.getSellingPointId())){
			return R.failed("卖点不能为空");
		}
		boolean flag = true;
		if (Objects.nonNull(vo.getProducerJson())) {
			JSONArray array = vo.getProducerJson();
			if (Objects.nonNull(array)) {
				for (Object object : array) {
					JSONObject jsonObject = JSON.parseObject(JSON.toJSONString(object));
					String producerId = jsonObject.getString("producerId");
					if (StringUtils.isNotBlank(producerId)){
						flag = false;
					}
				}
			}
		}
		if (flag){
			return R.failed("制作人不能为空");
		}
		vo.setStatus("3");
		return designRequirementService.editNeedStatus(vo);
	}
	/**
	 * 转UE4
	 * @param vo
	 * @return
	 */
	@DesignLog("审核通过了需求并转为UE4")
	@RequestMapping("/turn")
	public R turn(@RequestBody DesignRequirementVo vo){
		if (StringUtils.isBlank(vo.getId())){
			return R.failed("需求唯一ID不能为空");
		}
		if (StringUtils.isBlank(vo.getDuration())){
			return R.failed("工期不能为空不能为空");
		}
		if (StringUtils.isBlank(vo.getSellingPointId())){
			return R.failed("卖点不能为空");
		}
		boolean flag = true;
		if (Objects.nonNull(vo.getProducerJson())) {
			JSONArray array = vo.getProducerJson();
			if (Objects.nonNull(array)) {
				for (Object object : array) {
					JSONObject jsonObject = JSON.parseObject(JSON.toJSONString(object));
					String producerId = jsonObject.getString("producerId");
					if (StringUtils.isNotBlank(producerId)){
						flag = false;
					}
				}
			}
		}
		if (flag){
			return R.failed("制作人不能为空");
		}
		vo.setType("2");
		return designRequirementService.editNeedStatus(vo);
	}
	/**
	 * 开始任务
	 * @param vo
	 * @return
	 */
	@DesignLog("开始了需求任务")
	@RequestMapping("/startTask")
	public R startTask(@RequestBody DesignRequirementVo vo){
		if (StringUtils.isBlank(vo.getId())){
			return R.failed("需求唯一ID不能为空");
		}
		vo.setStatus("4");
		return designRequirementService.editNeedStatus(vo);
	}
	/**
	 * 提交成果
	 * @param vo
	 * @return
	 */
	@DesignLog("完成了需求，上传了需求成果")
	@RequestMapping("/submitResult")
	public R submitResult(DesignMaterialVo vo){
		if (StringUtils.isBlank(vo.getId())){
			return R.failed("需求唯一ID不能为空");
		}
		if (Objects.isNull(vo.getResultFile())){
			return R.failed("请选择文件");
		}
		if (StringUtils.isBlank(vo.getFileName())){
			return R.failed("文件名称不能为空");
		}
		return designRequirementService.submitResult(vo);
	}
	/**
	 * 标记
	 * @param vo
	 * @return
	 */
	@DesignLog("给需求打了标记")
	@RequestMapping("/mark")
	public R mark(@RequestBody DesignRequirementVo vo){
		if (StringUtils.isBlank(vo.getId())){
			return R.failed("需求唯一ID不能为空");
		}
		if (Objects.isNull(vo.getProducerJson())){
			return R.failed("标记信息不能为空");
		}
		return designRequirementService.mark(vo);
	}
	/**
	 * 根据需求类型查询对应制作人列表
	 * @param vo
	 * @return
	 */
	@RequestMapping("/getProducerListByType")
	public R getProducerListByType(@RequestBody DesignRequirementVo vo){
		if (StringUtils.isBlank(vo.getType())){
			return R.failed("类型不能为空");
		}
		return designRequirementService.getProducerListByType(vo);
	}
	/**
	 * 待审核需求总数
	 * @return
	 */
	@RequestMapping("/auditTotal")
	public R auditTotal(){
		return designRequirementService.auditTotal();
	}
	/**
	 * 验证参数
	 */
	public R checkParam(DesignRequirementVo vo){
		if (StringUtils.isBlank(vo.getPriority())){
			return R.failed("请选择需求优先级");
		}
		if (StringUtils.isBlank(vo.getTitle())){
			return R.failed("请输入需求标题");
		}
		if (StringUtils.isBlank(vo.getGameId())){
			return R.failed("请选择主游戏");
		}
		if (StringUtils.isBlank(vo.getDetail())){
			return R.failed("请输入需求详情");
		}
		if (StringUtils.isBlank(vo.getEndTime())){
			return R.failed("请选择截止日期");
		}
		if (vo.getTitle().length() > 20){
			return R.failed("需求标题限制20字以内");
		}
		if (vo.getDetail().length() > 500){
			return R.failed("需求详情限制500字以内");
		}
		if (StringUtils.isNotBlank(vo.getRemark())){
			if (vo.getRemark().length() > 200){
				return R.failed("备注限制200字以内");
			}
		}
		return R.ok();
	}
}


