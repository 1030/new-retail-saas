package com.dy.yandi.biz.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dy.yandi.api.client.requirement.model.DesignSellingPoint;
import com.dy.yandi.api.client.requirement.model.vo.DesignSellingPointVo;
import com.dy.yandi.api.client.requirement.service.DesignSellingPointService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 卖点
 * @author cx
 * @version 1.0.0
 * @Time 2021/10/12 11:12
 */
@RestController
@RequestMapping("/sellingPoint")
@RequiredArgsConstructor
public class DesignSellingPointController {
	private final DesignSellingPointService designSellingPointService;

	/**
	 * 卖点列表
	 * @param vo
	 * @return
	 */
	@RequestMapping("/getList")
	public R getList(@RequestBody DesignSellingPointVo vo){
		return designSellingPointService.getList(vo);
	}
	/**
	 * 新增卖点
	 * @param vo
	 * @return
	 */
	@RequestMapping("/add")
	public R add(@RequestBody DesignSellingPointVo vo){
		if (StringUtils.isBlank(vo.getName())){
			return R.failed("卖点名称不能为空");
		}
		if (vo.getName().length() > 20){
			return R.failed("卖点名称限制最大20个字");
		}
		int count = designSellingPointService.count(Wrappers.<DesignSellingPoint>query().lambda().eq(DesignSellingPoint::getName,vo.getName()).eq(DesignSellingPoint::getIsDeleted,0).eq(DesignSellingPoint::getStatus,1));
		if (count > 0){
			return R.failed("卖点名称已经存在");
		}
		DesignSellingPoint sellingPoint = new DesignSellingPoint();
		sellingPoint.setName(vo.getName());
		sellingPoint.setCreateId(Long.valueOf(SecurityUtils.getUser().getId()));
		sellingPoint.setUpdateId(Long.valueOf(SecurityUtils.getUser().getId()));
		boolean flag = designSellingPointService.save(sellingPoint);
		if (flag) {
			return R.ok();
		}
		return R.failed();
	}
}


