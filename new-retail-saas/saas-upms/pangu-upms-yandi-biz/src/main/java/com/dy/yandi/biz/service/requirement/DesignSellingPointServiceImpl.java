package com.dy.yandi.biz.service.requirement;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yandi.api.client.requirement.model.DesignSellingPoint;
import com.dy.yandi.api.client.requirement.model.vo.DesignSellingPointVo;
import com.dy.yandi.api.client.requirement.service.DesignSellingPointService;
import com.dy.yandi.biz.dao.pig.DesignSellingPointMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 需求卖点表
 * @author  chengang
 * @version  2021-09-28 13:44:43
 * table: design_selling_point
 */
@Log4j2
@Service("designSellingPointService")
@RequiredArgsConstructor
public class DesignSellingPointServiceImpl extends ServiceImpl<DesignSellingPointMapper, DesignSellingPoint> implements DesignSellingPointService {
	private final DesignSellingPointMapper designSellingPointMapper;
	/**
	 * 卖点列表
	 * @param vo
	 * @return
	 */
	@Override
	public R getList(DesignSellingPointVo vo){
		QueryWrapper<DesignSellingPoint> wrapper = new QueryWrapper<>();
		wrapper.eq("status",1);
		wrapper.eq("is_deleted",0);
		return R.ok(designSellingPointMapper.selectList(wrapper));
	}

	
	
}


