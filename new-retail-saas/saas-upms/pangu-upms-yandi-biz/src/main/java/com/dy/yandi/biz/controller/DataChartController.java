package com.dy.yandi.biz.controller;

import com.dy.yandi.api.client.demo.model.entity.DataChartDO;
import com.dy.yandi.api.client.demo.model.vo.DataChartVO;
import com.dy.yandi.api.client.demo.model.vo.DataChartVO.ChartsQuery1;
import com.dy.yandi.api.client.demo.model.vo.DataChartVO.ChartsQuery2;
import com.dy.yandi.api.client.demo.model.vo.DataChartVO.GetKpiCost;
import com.dy.yandi.biz.service.DataChartService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Title null.java
 * @Package com.dy.yandi.biz.controller
 * @Author 马嘉祺
 * @Date 2021/10/8 14:34
 * @Description
 */
@Slf4j
@RestController
@RequestMapping("/datachart")
@RequiredArgsConstructor
@Validated
public class DataChartController {

	private final DataChartService dataChartService;

	/**
	 * 获取kpi消耗信息
	 *
	 * @param dataCharts
	 * @return
	 */
	@PostMapping("/kpiCost")
	public R getKpiCost(@RequestBody @Validated(GetKpiCost.class) DataChartVO dataCharts) {
		List<DataChartDO> kpiCostList = dataChartService.getKpiCost(dataCharts);
		return R.ok(kpiCostList);
	}

	/**
	 * 获取素材计划数
	 *
	 * @param dataCharts
	 * @return
	 */
	@PostMapping("/materialAdCount")
	public R getMaterialAdCount(@RequestBody @Validated(ChartsQuery1.class) DataChartVO dataCharts) {
		List<DataChartDO> materialAdCountList = dataChartService.getMaterialAdCount(dataCharts);
		return R.ok(materialAdCountList);
	}

	/**
	 * 获取卖点使用统计列表
	 *
	 * @param dataCharts
	 * @return
	 */
	@PostMapping("/usedSellingPointList")
	public R getUsedSellingPointList(@RequestBody @Validated(ChartsQuery1.class) DataChartVO dataCharts) {
		List<DataChartDO> usedSellingPointList = dataChartService.getUsedSellingPointList(dataCharts);
		return R.ok(usedSellingPointList);
	}

	/**
	 * 获取个人素材卖点分布
	 *
	 * @param dataCharts
	 * @return
	 */
	@PostMapping("/sellingPointDistributed")
	public R getSellingPointDistributed(@RequestBody @Validated(ChartsQuery2.class) DataChartVO dataCharts) {
		List<DataChartDO> sellingPointDistributed = dataChartService.getSellingPointDistributed(dataCharts);
		return R.ok(sellingPointDistributed);
	}

	/**
	 * 获取综合信息统计列表
	 *
	 * @param dataCharts
	 * @return
	 */
	@PostMapping("/mixedStatisticsList")
	public R getMixedStatisticsList(@RequestBody @Validated(ChartsQuery1.class) DataChartVO dataCharts) {
		List<DataChartDO> mixedStatisticsList = dataChartService.getMixedStatisticsList(dataCharts);
		return R.ok(mixedStatisticsList);
	}

}
