package com.dy.yandi.biz.service.requirement;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yandi.api.client.requirement.model.DesignRequirementProducer;
import com.dy.yandi.api.client.requirement.service.DesignRequirementProducerService;
import com.dy.yandi.biz.dao.pig.DesignRequirementProducerMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 需求制作者表
 * @author  chengang
 * @version  2021-09-28 13:44:36
 * table: design_requirement_producer
 */
@Log4j2
@Service("designRequirementProducerService")
@RequiredArgsConstructor
public class DesignRequirementProducerServiceImpl extends ServiceImpl<DesignRequirementProducerMapper, DesignRequirementProducer> implements DesignRequirementProducerService {
	

	
}


