package com.dy.yandi.biz.dao.pig;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yandi.api.client.requirement.model.DesignRequirement;
import com.dy.yandi.api.client.requirement.model.dto.DesignRequirementDto;
import com.dy.yandi.api.client.requirement.model.vo.DesignRequirementVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 需求表
 * @author  chengang
 * @version  2021-09-28 13:44:24
 * table: design_requirement
 */
@Mapper
public interface DesignRequirementMapper extends BaseMapper<DesignRequirement> {
	/**
	 * 需求列表 - 分页
	 * @param vo
	 * @return
	 */
	IPage<DesignRequirementDto> selectRequirementByPage(DesignRequirementVo vo);

	/**
	 * 根据需求类型查询对应制作人列表
	 * @param param
	 * @return
	 */
	List<Map<String,Object>> selectProducerList(Map<String,Object> param);
}


