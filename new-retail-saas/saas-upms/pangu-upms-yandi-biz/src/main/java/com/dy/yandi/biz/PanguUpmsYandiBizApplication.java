package com.dy.yandi.biz;

import com.pig4cloud.pig.common.security.annotation.EnablePigFeignClients;
import com.pig4cloud.pig.common.security.annotation.EnablePigResourceServer;
import com.pig4cloud.pig.common.swagger.annotation.EnablePigSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableConfigurationProperties
@EnablePigSwagger2
@SpringCloudApplication
@EnablePigFeignClients(basePackages = {"com.dy.yandi", "com.dy.yunying", "com.pig4cloud.pig"})
@EnablePigResourceServer
@EnableAsync
public class PanguUpmsYandiBizApplication {

	public static void main(String[] args) {
		SpringApplication.run(PanguUpmsYandiBizApplication.class, args);
	}

}
