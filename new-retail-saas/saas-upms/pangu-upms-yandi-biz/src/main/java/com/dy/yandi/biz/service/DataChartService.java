package com.dy.yandi.biz.service;

import com.dy.yandi.api.client.demo.model.entity.DataChartDO;
import com.dy.yandi.api.client.demo.model.vo.DataChartVO;

import java.util.List;

/**
 * @Title null.java
 * @Package com.dy.yandi.biz.service
 * @Author 马嘉祺
 * @Date 2021/10/8 14:35
 * @Description
 */
public interface DataChartService {

	/**
	 * 获取kpi消耗信息
	 *
	 * @param dataCharts
	 * @return
	 */
	List<DataChartDO> getKpiCost(DataChartVO dataCharts);

	/**
	 * 获取素材计划数
	 *
	 * @param dataCharts
	 * @return
	 */
	List<DataChartDO> getMaterialAdCount(DataChartVO dataCharts);

	/**
	 * 获取卖点使用统计列表
	 *
	 * @param dataCharts
	 * @return
	 */
	List<DataChartDO> getUsedSellingPointList(DataChartVO dataCharts);

	/**
	 * 获取个人素材卖点分布
	 *
	 * @param dataCharts
	 * @return
	 */
	List<DataChartDO> getSellingPointDistributed(DataChartVO dataCharts);

	/**
	 * 获取综合信息统计列表
	 *
	 * @param dataCharts
	 * @return
	 */
	List<DataChartDO> getMixedStatisticsList(DataChartVO dataCharts);
	/**
	 * 当前用户是否是设计主管
	 * @return
	 */
	boolean isDesignManager();

}
