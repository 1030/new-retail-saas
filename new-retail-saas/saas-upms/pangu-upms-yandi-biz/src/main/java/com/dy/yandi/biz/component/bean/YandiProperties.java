package com.dy.yandi.biz.component.bean;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Description 集中管理炎帝nacos配置
 * @Author chengang
 * @Date 2021/9/3
 */
@RefreshScope
@Configuration
@Data
public class YandiProperties {

	@Value("${demo_name}")
	private String name;
	/**
	 * minio 访问前缀地址
	 */
	@Value("${minio_file_prefix_url}")
	private String minioFilePrefixUrl;
	/**
	 * 上传地址
	 */
	@Value("${video_download_url}")
	private String downloadUrl;
	/**
	 * 下载地址
	 */
	@Value("${video_upload_path}")
	private String uploadPath;
	/**
	 * 限制大小
	 */
	@Value("${video_size}")
	private String size;

}
