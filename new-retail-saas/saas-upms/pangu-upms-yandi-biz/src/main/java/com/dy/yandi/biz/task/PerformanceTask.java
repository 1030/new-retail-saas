package com.dy.yandi.biz.task;

import com.dy.yandi.api.client.requirement.model.dto.PerformanceVo;
import com.dy.yandi.api.client.requirement.service.DesignBonusService;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.common.core.util.R;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 按月统计炎帝平台所有人的绩效
 * @Description
 * @Author chengang
 * @Date 2021/10/15
 */
@Slf4j
@Component
public class PerformanceTask {

	@Autowired
	private DesignBonusService designBonusService;

	/**
	 * 支持补跑绩效(记得还原补跑月份的规则噢！)
	 * param = {"time":"2021-09","tenantId":7}
	 * @param param
	 * @return
	 */
	@XxlJob("calcPerformance")
	public ReturnT<String> calcPerformance(String param){
		XxlJobLogger.log("-----------------计算绩效 START--------------------------");
		/**
		 * 1.每小时执行一次
		 * 2.按系统时间的月份计算-当月第一天~当月最后一天
		 * 3.因组别ID不确定故计算所有人的绩效
		 * 4.默认按月--后续再支持按季度和按年
		 */
		try {
			if (StringUtils.isBlank(param)) {
				XxlJobLogger.log("缺少任务启动参数 {\"time\":\"2021-09\",\"tenantId\":7} time 非必填 tenantId必填");
				return ReturnT.FAIL;
			}
			//1:按月 2:按季 3:按年
//			List<Integer> periods = Lists.newArrayList(1,2,3);
			List<Integer> periods = Lists.newArrayList(1);
			periods.forEach(period -> {
				R r = designBonusService.getPerformance4Task(1,param);
				XxlJobLogger.log("绩效执行返回[{}]",r.getMsg());
			});
			XxlJobLogger.log("-----------------计算绩效 END--------------------------");
			return ReturnT.SUCCESS;
		}catch (Exception e) {
			e.printStackTrace();
			return new ReturnT<String>(ReturnT.FAIL.getCode(), "command exit value("+param+") is failed");
		}
	}

}
