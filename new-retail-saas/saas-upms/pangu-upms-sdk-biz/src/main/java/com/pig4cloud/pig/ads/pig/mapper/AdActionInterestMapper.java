package com.pig4cloud.pig.ads.pig.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdActionInterest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 行为兴趣数据层
 *
 * @author hma
 * @date 2021-05-07
 */
@Mapper
public interface AdActionInterestMapper extends BaseMapper<AdActionInterest>
{
	@Select("select third_id thirdId,name,type from ad_action_interest ")
    List<AdActionInterest> getAllThirdIdAndNames();
}
