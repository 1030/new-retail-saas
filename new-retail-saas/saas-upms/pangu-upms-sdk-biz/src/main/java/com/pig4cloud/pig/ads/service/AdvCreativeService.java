/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdvertiserDto;
import com.pig4cloud.pig.api.dto.CampaignDto;
import com.pig4cloud.pig.api.dto.TtAdCreativeResponseNew;
import com.pig4cloud.pig.api.entity.AdCreative;
import com.pig4cloud.pig.api.entity.TtAdCreativeTemp;
import com.pig4cloud.pig.api.entity.TtCreativeDetail;
import com.pig4cloud.pig.common.core.util.R;


/**
 * 广告创意接口类
 *
 * @author hma
 */
public interface AdvCreativeService extends IService<AdCreative> {


	/**
	 * 获取广告创意报表数据
	 *
	 * @param campaignDto
	 * @return
	 */
	R getAdvCreativeStatistic(CampaignDto campaignDto);

	/**
	 * 修改广告创意状态
	 *
	 * @param advertiserDto
	 * @return
	 */
	R updateCreativeStatus(AdvertiserDto advertiserDto);


	/**
	 * 创建广告创意
	 *
	 * @param ttAdCreativeTemp
	 * @return
	 */
	TtAdCreativeResponseNew createAdvCreative(TtAdCreativeTemp ttAdCreativeTemp);

//	void getAdCreative(String advertiserId,String creativeIds);

	void getAdCreativeByAdId(String advertiserId, Long adId);

//	void getAdCreativeByAdId(String advertiserId, List<Long> adIdList);

	TtAdCreativeResponseNew copyAdvCreative(TtCreativeDetail ttCreativeDetail) throws Exception;
}
