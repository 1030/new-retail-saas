package com.pig4cloud.pig.ads.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.NumberSerializer;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.util.TimeZone;


public class JacksonMapper extends ObjectMapper {

  public JacksonMapper() {
    super();
    // 设置时区
    this.setTimeZone(TimeZone.getDefault());//getTimeZone("GMT+8:00")
    SimpleModule simpleModule = new SimpleModule();
    simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
    simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
    // long 还是以数值型显示
    simpleModule.addSerializer(long.class, NumberSerializer.instance);
    registerModule(simpleModule);

  }
}
