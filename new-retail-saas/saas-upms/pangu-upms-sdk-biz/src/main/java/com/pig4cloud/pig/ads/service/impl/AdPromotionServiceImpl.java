package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdPromotionMapper;
import com.pig4cloud.pig.ads.service.AdPromotionService;
import com.pig4cloud.pig.api.entity.AdPromotion;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 广告表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:21:02
 * table: ad_promotion
 */
@Log4j2
@Service("adPromotionService")
@RequiredArgsConstructor
public class AdPromotionServiceImpl extends ServiceImpl<AdPromotionMapper, AdPromotion> implements AdPromotionService {
}


