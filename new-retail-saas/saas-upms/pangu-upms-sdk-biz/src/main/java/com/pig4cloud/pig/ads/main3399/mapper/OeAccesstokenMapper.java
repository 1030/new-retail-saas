package com.pig4cloud.pig.ads.main3399.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.OeAccesstoken;

public interface OeAccesstokenMapper  extends BaseMapper<OeAccesstoken>{
}
