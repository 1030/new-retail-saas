package com.pig4cloud.pig.ads.task;

import com.pig4cloud.pig.ads.service.xingtu.XingtuTaskService;
import com.pig4cloud.pig.common.core.util.R;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Description 获取头条广告创意基础组件定时器
 * @Author chengang
 * @Date 2021/8/25
 */
@Component
public class TtXingtuTask {

	private static Logger logger = LoggerFactory.getLogger(TtXingtuTask.class);

	@Resource
	private XingtuTaskService xingtuTaskService;

	/**
	 * 获取星图任务列表--每3分钟执行一次
	 * https://ad.oceanengine.com/open_api/2/star/demand/list/
	 * @param param
	 * @return
	 */
	@XxlJob("getTaskList4XT")
	public ReturnT<String> getTaskList4XT(String param) {
		try {
			XxlJobLogger.log("================开始执行获取星图任务列表========================");
			R r = xingtuTaskService.getTaskList(param);
			if (r.getCode() == 0) {
				XxlJobLogger.log("获取星图任务列表，已执行完成--{}", r.getMsg());
			} else {
				XxlJobLogger.log("获取星图任务列表，执行失败--{}",r.getMsg());
			}
			XxlJobLogger.log("================执行结束获取星图任务列表========================");
		} catch (Throwable e) {
			logger.error("获取星图任务列表ERROR",e);
			return ReturnT.FAIL;
		}
		return ReturnT.SUCCESS;
	}

	/**
	 * 获取星图任务订单列表 -- 每3分钟执行一次
	 * https://ad.oceanengine.com/open_api/2/star/demand/order/list/
	 * @param param
	 * @return
	 */
	@XxlJob("getTaskOrderList4XT")
	public ReturnT<String> getTaskOrderList4XT(String param) {
		try {
			XxlJobLogger.log("================开始执行获取星图任务订单列表========================");
			R r = xingtuTaskService.getTaskOrderList(param);
			if (r.getCode() == 0) {
				XxlJobLogger.log("获取星图任务订单列表，已执行完成--{}", r.getMsg());
			} else {
				XxlJobLogger.log("获取星图任务订单列表，执行失败--{}",r.getMsg());
			}
			XxlJobLogger.log("================执行结束获取星图任务订单列表========================");
		} catch (Throwable e) {
			logger.error("获取星图任务订单列表ERROR",e);
			return ReturnT.FAIL;
		}
		return ReturnT.SUCCESS;
	}

	/**
	 * 获取星图订单明细 -- 每4小时执行一次 0 0 0/4 * * ?
	 * 数据为非实时更新，一般在次日凌晨产出前一天的数据
	 * 一般历史数据都不会变，除了数据有问题有校对的情况会更新历史数据。
	 * https://ad.oceanengine.com/open_api/2/star/report/order_overview/get/
	 *
	 * @param param
	 * @return
	 */
	@XxlJob("getOrderDetail4XT")
	public ReturnT<String> getOrderDetail4XT(String param) {
		try {
			XxlJobLogger.log("================开始执行获取星图订单明细========================");
			R r = xingtuTaskService.getTaskOrderDetail(param);
			if (r.getCode() == 0) {
				XxlJobLogger.log("获取星图订单明细，已执行完成--{}", r.getMsg());
			} else {
				XxlJobLogger.log("获取星图订单明细，执行失败--{}",r.getMsg());
			}
			XxlJobLogger.log("================执行结束获取星图订单明细========================");
		} catch (Throwable e) {
			logger.error("获取星图订单明细ERROR",e);
			return ReturnT.FAIL;
		}
		return ReturnT.SUCCESS;
	}

}
