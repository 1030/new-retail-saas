package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdAssetsTrack;
import com.pig4cloud.pig.api.vo.AdAssetsTrackReq;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 监测链接组表
 * @author  chenxiang
 * @version  2022-04-06 13:31:02
 * table: ad_assets_track
 */
public interface AdAssetsTrackService extends IService<AdAssetsTrack> {
	/**
	 * 调用头条 - 创建监测组链接
	 * @param record
	 * @return
	 */
	R createEventTrackUrl(AdAssetsTrack record);
	/**
	 * 获取事件资产下的监测链接组
	 * @param record
	 * @return
	 */
	R getTrackUrl(AdAssetsTrackReq record);
	/**
	 * 获取优化目标
	 * @param record
	 * @return
	 */
	R getOptimizedGoal(AdAssetsTrackReq record);
}


