package com.pig4cloud.pig.ads.pig.mapper.bd;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.bd.BdCampaign;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 百度信息流计划表(bd_campaign)数据Mapper
 *
 * @author zjz
 * @since 2022-12-27 18:35:43
 * @description 由 zjz 创建
*/
@Mapper
public interface BdCampaignMapper extends BaseMapper<BdCampaign> {


    List<BdCampaign> getListByCampaignIds(@Param("list") List<Long> list);

	BdCampaign getByAdgroupFeedId(@Param("adgroupFeedId") Long adgroupFeedId);
}
