package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.TtAdCreativeTemp;
import com.pig4cloud.pig.common.core.util.R;

public interface TtAdCreativeTempService extends IService<TtAdCreativeTemp> {

	R create(TtAdCreativeTemp record);

	TtAdCreativeTemp detail(Integer id);

	R update(TtAdCreativeTemp record);

	R delete(Long id);

	R latestRecord(Long advertiserId, Long adId,Long id);

	R detailById(Long id);
	

}
