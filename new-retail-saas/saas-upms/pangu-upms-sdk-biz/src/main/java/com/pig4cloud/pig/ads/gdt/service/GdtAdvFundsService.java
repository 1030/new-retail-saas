package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.entity.GdtAdvFunds;

public interface GdtAdvFundsService  extends IService<GdtAdvFunds> {

}
