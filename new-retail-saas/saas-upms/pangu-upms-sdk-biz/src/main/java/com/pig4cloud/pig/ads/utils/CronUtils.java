package com.pig4cloud.pig.ads.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * cron表达式工具类
 * 
 * @author hma
 * @version 2019-01-15
 *
 */
public class CronUtils {
	
	/**
	 * 返回一个布尔值代表一个给定的Cron表达式的有效性
	 *
	 * @param cronExpression Cron表达式
	 * @return boolean 表达式是否有效
	 */
	public static boolean isValid(String cronExpression) {
		return CronExpression.isValidExpression(cronExpression);
	}

	/**
	 * 返回一个字符串值,表示该消息无效Cron表达式给出有效性
	 *
	 * @param cronExpression Cron表达式
	 * @return String 无效时返回表达式错误描述,如果有效返回null
	 */
	public static String getInvalidMessage(String cronExpression) {
		try {
			new CronExpression(cronExpression);
			return null;
		} catch (ParseException pe) {
			return pe.getMessage();
		}
	}

	/**
	 * 返回下一个执行时间根据给定的Cron表达式
	 *
	 * @param cronExpression Cron表达式
	 * @return Date 下次Cron表达式执行时间
	 */
	public static Date getNextExecution(String cronExpression) {
		try {
			CronExpression cron = new CronExpression(cronExpression);
			return cron.getNextValidTimeAfter(new Date(System.currentTimeMillis()));
		} catch (ParseException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	/**
	 * 根据时间返回对应cron表达式
	 */
	public static String getCron(Date  date) throws ParseException {
		String dateFormat="ss mm HH dd MM ? yyyy";
		return formatDateByPattern(date, dateFormat);
	}


	public static String formatDateByPattern(Date date,String dateFormat){
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		String formatTimeStr = null;
		if (date != null ) {
			formatTimeStr = sdf.format(date);
		}
		return formatTimeStr;
	}
}
