package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.api.client.util.Lists;
import com.pig4cloud.pig.ads.service.TtAppManagementService;
import com.pig4cloud.pig.api.entity.TtAppManagement;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：lile
 * @date ：2021/8/5 10:38
 * @description：
 * @modified By：
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/ttApp")
@Api(value = "ttApp", tags = "头条刚好账户获取应用包id")
public class TtAppManagementController {

	@Autowired
	private TtAppManagementService ttAppManagementService;

	@GetMapping(value = "/queryPackage")
	public R getPackageIdByAppId(String appId) {
		try {
			TtAppManagement ttAppManagement = ttAppManagementService.getPackageIdByAppId(appId);
			return R.ok(ttAppManagement);
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed();
		}
	}
	/**
	 * 获取应用列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/getList")
	public R getList(@RequestBody TtAppManagement req){
		String advertiserId = req.getAdvertiserId();
		if (StringUtils.isBlank(advertiserId)){
			return R.ok(Lists.newArrayList());
		}
		return R.ok(ttAppManagementService.list(Wrappers.<TtAppManagement>lambdaQuery().eq(TtAppManagement::getAdvertiserId, advertiserId).eq(TtAppManagement::getIsDeleted, 0)));
	}
}
