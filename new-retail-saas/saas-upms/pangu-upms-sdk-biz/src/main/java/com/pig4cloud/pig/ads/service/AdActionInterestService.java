package com.pig4cloud.pig.ads.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdActionInterest;

import java.util.List;
import java.util.Map;

/**
 * @用户账号申请授权表
 * @author john
 *
 */
public interface AdActionInterestService extends IService<AdActionInterest> {
	/**
	 * 获取所有thirdid 和 name 的关系列表
	 * @return
	 */
	List<AdActionInterest> getAllThirdIdAndNames();

}
