package com.pig4cloud.pig.ads.gdt.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.dto.GdtCustomAudiencesDto;
import com.pig4cloud.pig.api.gdt.entity.GdtCustomAudiences;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.io.File;
import java.util.List;

/**
 * @projectName:pig
 * @description:广点通人群信息
 * @author:Zhihao
 * @createTime:2020/12/4 17:56
 * @version:1.0
 */
public interface GdtCustomAudiencesService extends IService<GdtCustomAudiences> {

	/**
	 * 上传到广点通人群包文件
	 *
	 * @param customFile
	 * @param gdtCustomAudiencesDto
	 * @return
	 */
	R uploadCustomAudiencesFile(MultipartFile[] customFile, GdtCustomAudiencesDto gdtCustomAudiencesDto);

	R pushSelfCustomAudience(File cfile, @Valid GdtCustomAudiencesDto gdtCustomAudiencesDto);
	List<GdtCustomAudiences> getAllThirdIdAndNames();
}
