package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtAudiencePackage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface GdtAudiencePackageMapper extends BaseMapper<GdtAudiencePackage> {

    int updateByPrimaryKeySelective(GdtAudiencePackage record);

    int updateInfoByPrimaryKey(GdtAudiencePackage record);

	GdtAudiencePackage selectByName(@Param("name") String name);

}