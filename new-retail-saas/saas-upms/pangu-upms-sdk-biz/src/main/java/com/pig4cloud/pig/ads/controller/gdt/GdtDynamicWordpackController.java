package com.pig4cloud.pig.ads.controller.gdt;

import com.pig4cloud.pig.ads.gdt.service.GdtDynamicWordpackService;
import com.pig4cloud.pig.api.vo.GdtDynamicWordpackVo;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: nml
 * @time: 2020/12/3 15:43
 **/

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/gdt/dynamicWordpack")
@Api(value = "/gdt/dynamicWordpack", tags = "广点通动态词包模块")
public class GdtDynamicWordpackController {

	@Autowired
	private GdtDynamicWordpackService gdtDynamicWordpackService;

	//分页列表
	@RequestMapping("/page")
	public R findAll(GdtDynamicWordpackVo req){
		return gdtDynamicWordpackService.findPage(req);
	}

	//列表
	@RequestMapping("/list")
	public R list(GdtDynamicWordpackVo req){
		return gdtDynamicWordpackService.findList(req);
	}

}
