package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdAgentTransferDto;
import com.pig4cloud.pig.api.entity.AdAgentTransfer;
import com.pig4cloud.pig.api.vo.AdAgentTransferVo;

public interface AdAgentTransferService extends IService<AdAgentTransfer> {

    IPage<AdAgentTransferVo> queryList(AdAgentTransferDto req);

	AdAgentTransferVo queryPtypeByid(AdAgentTransferDto req);
}
