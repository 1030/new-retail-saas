package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdVideoPlatform;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdVideoService.java
 * @createTime 2020年11月05日 14:01:00
 */
public interface AdVideoPlatformService extends IService<AdVideoPlatform> {
}
