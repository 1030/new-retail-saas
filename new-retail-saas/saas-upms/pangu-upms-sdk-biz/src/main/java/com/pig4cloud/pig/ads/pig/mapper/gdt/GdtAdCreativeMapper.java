package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtAdCreative;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 广点通广告创意 数据层
 * 
 * @author hma
 * @date 2020-12-10
 */
@Mapper
public interface GdtAdCreativeMapper   extends BaseMapper<GdtAdCreative>
{


	List<GdtAdCreative> getAdCreativeByGrupId(@Param(value = "adgroupId") String adgroupId);

	
}