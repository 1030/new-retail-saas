package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdVideoPlatformMapper;
import com.pig4cloud.pig.ads.service.AdVideoPlatformService;
import com.pig4cloud.pig.api.entity.AdVideoPlatform;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdVideoServiceImpl.java
 * @createTime 2020年11月05日 14:02:00
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class AdVideoPlatformServiceImpl extends ServiceImpl<AdVideoPlatformMapper, AdVideoPlatform> implements AdVideoPlatformService {

}
