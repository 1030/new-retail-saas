/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdAccountResp;
import com.pig4cloud.pig.api.entity.AdAccount;
import com.pig4cloud.pig.api.vo.AdAccountRequest;
import com.pig4cloud.pig.api.vo.AdAccountVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 广告账户表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
public interface AdAccountMapper extends BaseMapper<AdAccount> {

	IPage<AdAccountResp> selectByPage(AdAccountVo req);

	List<AdAccountResp> selectDataByList(AdAccountRequest req);

	List<Map<String, Object>> selectTtAdAccountBalance(@Param("records") List<String> records);

	List<Map<String, Object>> selectGdtAdAccountBalance(@Param("records") List<String> records);

	List<Map<String, String>> queryAccountByHousekeeper(@Param("housekeeper") String housekeeper, @Param("adAccount") String adAccount);

	AdAccount queryAdByHousekeeper(AdAccountVo req);
}
