package com.pig4cloud.pig.ads.service;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdverDayReport;
import com.pig4cloud.pig.api.util.Page;

/**
  * 日报表
 *  @author zxm
 */
public interface AdverDayReportService extends IService<AdverDayReport> {

	Page selectByPage(Page page, Map<String, Object> params);
}
