package com.pig4cloud.pig.ads.service;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.DailyStat;
import com.pig4cloud.pig.api.util.Page;

/**
  * 日流水
 *  @author zxm
 */
public interface DailyStatService extends IService<DailyStat> {
	
	Page selectByPage(Page page, Map<String, Object> params);
}
