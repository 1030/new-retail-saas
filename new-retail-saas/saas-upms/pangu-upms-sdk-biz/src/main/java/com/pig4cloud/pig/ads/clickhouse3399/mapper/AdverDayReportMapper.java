package com.pig4cloud.pig.ads.clickhouse3399.mapper;

import java.util.Map;

import com.pig4cloud.pig.api.entity.AdverDayReport;
import com.pig4cloud.pig.api.entity.TtStatistic;
import com.pig4cloud.pig.api.util.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface AdverDayReportMapper extends BaseMapper<AdverDayReport>{

	Page<AdverDayReport> selectByPage(Page page, @Param("query") Map<String, Object> params);

	TtStatistic selectDayReportStatisticCount(@Param("query") Map<String, Object> params);

}
