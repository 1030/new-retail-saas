/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pig4cloud.pig.ads.controller;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.api.entity.AudiencePackage;
import com.pig4cloud.pig.ads.service.AudiencePackageService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


/**
 * @ 广告账户 前端控制器
 * @author john
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/audience_pack")
@Api(value = "audience_pack", tags = "定向包")
public class AudiencePackageController {

	private final AudiencePackageService audiencePackageService;




	//新建
	@RequestMapping("/create")
	public R create(AudiencePackage record) {
		return audiencePackageService.create(record);
	}

	//详情
	@RequestMapping("/detail")
	public R detail(Integer id) {
		AudiencePackage ap = audiencePackageService.detail(id);
		if (ap != null) {		    
		    return R.ok(ap);
		} else {		    
		    return R.failed("未找到记录");
		}
	}

	//更新
	@RequestMapping("/update")
	public R update(AudiencePackage record) {
		return audiencePackageService.update(record);
	}

	//删除
	@RequestMapping("/delete")
	public R delete(Integer id) {
		return audiencePackageService.remove(id);
	}

	//绑定 广告计划
	@RequestMapping("/ad/bind")
	public R bindAdplan(Integer audiencePackageId, String ads) {
		String[] adsStr = ads.split(",");
		List<Long> adids = new ArrayList<>();
		for (String str : adsStr){
			if (!StringUtils.isNumber(str)){
				R.failed("广告id不合法");
			}
			adids.add(Long.valueOf(str));
		}
		return audiencePackageService.bindAdplan(audiencePackageId, adids);
	}

	//解绑 广告计划
	@RequestMapping("/ad/unbind")
	public R unbindAdplan(Integer audiencePackageId, String ads) {
		String[] adsStr = ads.split(",");
		List<Long> adids = new ArrayList<>();
		for (String str : adsStr){
			if (!StringUtils.isNumber(str)){
				R.failed("广告id不合法");
			}
			adids.add(Long.valueOf(str));
		}
		return audiencePackageService.unbindAdplan(audiencePackageId, adids);
	}

	//分页查询
	@GetMapping("/page")
	public R getApPage(Page page,String[] advertiserIds) {
		return R.ok(audiencePackageService.pagedAudiencePackage(page, advertiserIds));
	}

	//分页查询
	///** 投放范围 ： 通投智选(UNIVERSAL)  默认(DEFAULT) 穿山甲(UNION) */
	///** 下载方式：DOWNLOAD_URL下载链接，QUICK_APP_URL快应用+下载链接，EXTERNAL_URL落地页链接 */
	@GetMapping("/page_syc_suc")
	public R getApPageSycSuc(Page page, String advertiserId, String deliveryRange, String downloadType) {
		QueryWrapper<AudiencePackage> wrapper = new QueryWrapper<>();
		wrapper.orderByDesc("id");
		wrapper.eq("advertiser_id", advertiserId);

		wrapper.eq("sync_status", 1);

		//目前投放范围，界面只能选择默认和穿山甲
		if("DEFAULT".equals(deliveryRange) || "UNION".equals(deliveryRange)){
			wrapper.eq("delivery_range", deliveryRange);
		}

		//目前定向包类型只有应用下载-Android和穿山甲
		//下载链接
		if("DOWNLOAD_URL".equals(downloadType)){
			wrapper.eq("landing_type", "APP_ANDROID");
			//落地页
		}else if("EXTERNAL_URL".equals(downloadType)){
			wrapper.eq("landing_type", "EXTERNAL");
		}

		return R.ok(audiencePackageService.pagedAudiencePackageSycSuc(page, wrapper));
	}
}
