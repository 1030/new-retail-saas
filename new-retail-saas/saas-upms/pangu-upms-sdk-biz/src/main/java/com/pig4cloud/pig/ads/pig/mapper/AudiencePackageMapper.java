package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AudiencePackage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AudiencePackageMapper extends BaseMapper<AudiencePackage> {
	int updateByPrimaryKeySelective(AudiencePackage record);

	int updateByPrimaryKeySelectivePartial(AudiencePackage record);

	AudiencePackage selectByName(@Param("name") String name);
}