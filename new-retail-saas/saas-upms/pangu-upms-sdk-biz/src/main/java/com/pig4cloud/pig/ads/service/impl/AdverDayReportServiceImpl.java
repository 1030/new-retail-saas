package com.pig4cloud.pig.ads.service.impl;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.pig4cloud.pig.ads.clickhouse3399.mapper.AdverDayReportMapper;
import com.pig4cloud.pig.ads.service.AdvCampaignService;
import com.pig4cloud.pig.ads.service.AdverDayReportService;
import com.pig4cloud.pig.api.entity.TtStatistic;
import com.pig4cloud.pig.api.util.Constants;
import com.pig4cloud.pig.api.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.api.entity.AdverDayReport;

@Service
public class AdverDayReportServiceImpl extends ServiceImpl<AdverDayReportMapper, AdverDayReport> implements AdverDayReportService {
	
	@Autowired
	private AdverDayReportMapper adverDayReportMapper;
	
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	@Autowired
	private AdvCampaignService advCampaignService;

	@Override
	public Page selectByPage(Page page, Map<String, Object> params) {
		Page ipage=  adverDayReportMapper.selectByPage(page, params);
		//  添加汇总数据
		TtStatistic ttStatistic=new TtStatistic();
		if(CollectionUtils.isNotEmpty(ipage.getRecords())){
			ttStatistic=adverDayReportMapper.selectDayReportStatisticCount(params);
			// 计算 率
			advCampaignService.statisticCalculate(ttStatistic);
		}
		ipage.setRow(ttStatistic);
		if(ipage !=null && ipage.getRecords() !=null && ipage.getRecords().size() != 0) {
			List<AdverDayReport> list = ipage.getRecords();
			list.forEach(item -> {
				Long advertiserId = item.getAdvertiserId();
				item.setName(stringRedisTemplate.opsForValue().get(Constants.AD_REDIS_AD_NAME_KEY_PRIX_ + advertiserId));
			});
		}
		return ipage;
	}


}
