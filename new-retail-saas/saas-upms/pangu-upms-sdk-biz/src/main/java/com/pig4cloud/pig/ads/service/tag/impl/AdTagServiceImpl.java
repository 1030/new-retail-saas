package com.pig4cloud.pig.ads.service.tag.impl;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.tag.AdTagMapper;
import com.pig4cloud.pig.ads.pig.mapper.tag.AdTagRelateMapper;
import com.pig4cloud.pig.ads.service.tag.AdTagService;
import com.pig4cloud.pig.api.entity.tag.AdTag;
import com.pig4cloud.pig.api.entity.tag.AdTagDto;
import com.pig4cloud.pig.api.entity.tag.AdTagList;
import com.pig4cloud.pig.api.entity.tag.AdTagVo;
import com.pig4cloud.pig.api.entity.tag.AdTagRelate;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 标签表服务接口实现
 *
 * @author zjz
 * @since 2023-02-23 11:39:32
 * @description 由 zjz 创建
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class AdTagServiceImpl extends ServiceImpl<AdTagMapper, AdTag> implements AdTagService {
    private final AdTagMapper adTagMapper;
	private final AdTagRelateMapper adTagRelateMapper;
	@Override
	public R tagList(AdTagDto dto){
		List<AdTagList> list = adTagMapper.tagList(dto.getTagName());
		return R.ok(list);
	}

	@Override
	public R pageList(AdTagDto dto){
		Page page = new Page();
		page.setSize(dto.getSize());
		page.setCurrent(dto.getCurrent());
		IPage<AdTagVo> data = adTagMapper.pageList(dto);
		if(CollectionUtils.isEmpty(data.getRecords())){
			return R.ok(data);
		}
		QueryWrapper<AdTagRelate> reWrapper = new QueryWrapper<>();
		reWrapper.in("tag_id",data.getRecords().stream().map(AdTagVo::getId).collect(Collectors.toList()));
		reWrapper.eq("deleted", 0);
		List<AdTagRelate> list = adTagRelateMapper.selectList(reWrapper);
		data.getRecords().forEach(a->{
			long imageNum = list.stream().filter(b->(b.getTagId()+"").equals(a.getId()+"")&& b.getType()==2).count();
			a.setImageNum(imageNum);
			long videoNum = list.stream().filter(b->(b.getTagId()+"").equals(a.getId()+"")&& b.getType()==1).count();
			a.setVideoNum(videoNum);
		});
		return R.ok(data);
	}


	@Override
	public R add(AdTagDto dto){
		if(StringUtils.isBlank(dto.getTagColor())){
			return R.failed("颜色不能为空");
		}
		if(StringUtils.isBlank(dto.getTagName())|| dto.getTagName().length()>4){
			return R.failed("标签名称不能为空且最多4个汉字或字母");
		}
		List<AdTagList> list = adTagMapper.tagListByName(dto.getTagName());
		if(CollectionUtils.isNotEmpty(list)){
			return R.failed("标签名称已存在");
		}
		AdTag adTag = new AdTag();
		adTag.setTagName(dto.getTagName());
		adTag.setTagColor(dto.getTagColor());
		adTag.setDeleted(0);
		adTag.setCreateId(SecurityUtils.getUser().getId().longValue());
		adTag.setUpdateId(SecurityUtils.getUser().getId().longValue());
		adTagMapper.insert(adTag);
		return R.ok();
	}

	@Override
	public R edit(AdTagDto dto){
		if(StringUtils.isBlank(dto.getTagName())){
			return R.failed("标签名不能为空");
		}
		if(StringUtils.isBlank(dto.getTagColor())){
			return R.failed("颜色不能为空");
		}
		if(dto.getId()==null){
			return R.failed("标签ID不能为空");
		}
		AdTag adTag =adTagMapper.selectById(dto.getId());
		if(adTag==null){
			return R.failed("标签ID不存在");
		}
		if(!adTag.getTagName().equals(dto.getTagName())) {
			List<AdTagList> list = adTagMapper.tagListByName(dto.getTagName());
			if (CollectionUtils.isNotEmpty(list)) {
				return R.failed("标签名称已存在");
			}
		}
		adTag.setTagName(dto.getTagName());
		adTag.setTagColor(dto.getTagColor());
		adTag.setUpdateTime(new Date());
		adTag.setUpdateId(SecurityUtils.getUser().getId().longValue());
		QueryWrapper<AdTag> wrapper = new QueryWrapper<>();
		wrapper.eq("id", dto.getId());
		adTagMapper.update(adTag,wrapper);
		return R.ok();
	}

	@Override
	public R del(AdTagDto dto){
		if(dto.getId()==null){
			return R.failed("标签ID不能为空");
		}
		QueryWrapper<AdTagRelate> reWrapper = new QueryWrapper<>();
		reWrapper.eq("tag_id", dto.getId());
		reWrapper.eq("deleted", 0);
		List<AdTagRelate> list = adTagRelateMapper.selectList(reWrapper);
		if(CollectionUtils.isNotEmpty(list)){
			return R.failed("当前标签存在绑定素材，请全部删除后再进行操作");
		}
		AdTag adTag = new AdTag();
		adTag.setId(dto.getId());
		adTag.setDeleted(1);
		adTag.setUpdateTime(new Date());
		adTag.setUpdateId(SecurityUtils.getUser().getId().longValue());
		QueryWrapper<AdTag> wrapper = new QueryWrapper<>();
		wrapper.eq("id", dto.getId());
		adTagMapper.update(adTag,wrapper);
		return R.ok();
	}
}