package com.pig4cloud.pig.ads.clickhouse3399.mapper;

import java.util.Map;

import com.pig4cloud.pig.api.util.Page;
import com.pig4cloud.pig.api.entity.DailyStat;
import com.pig4cloud.pig.api.vo.DailyStatCountVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface DailyStatMapper extends BaseMapper<DailyStat>{

	Page<DailyStat> selectByPage(Page page, @Param("query") Map<String, Object> params);

	/**
	 * 汇总日报表数据
	 * @param params
	 * @return
	 */
	DailyStatCountVo selectDailyStatCount(@Param("query") Map<String, Object> params);
}
