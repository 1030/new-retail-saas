package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtAd;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface GdtAdMapper extends BaseMapper<GdtAd> {

	/**
	 * 批量插入广告
	 *
	 * @param adList
	 * @return
	 */
//	int batchInsertGdtAd(List<GdtAd> adList);

}

