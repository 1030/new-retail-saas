package com.pig4cloud.pig.ads.pig.mapper.xingtu;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.xingtu.XingtuTask;
import org.apache.ibatis.annotations.Mapper;

/**
 * 星图任务表
 * @author  chengang
 * @version  2021-12-13 16:47:48
 * table: xingtu_task
 */
@Mapper
public interface XingtuTaskMapper extends BaseMapper<XingtuTask> {

}


