package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdAssetsTrack;
import org.apache.ibatis.annotations.Mapper;

/**
 * 监测链接组表
 * @author  chenxiang
 * @version  2022-04-06 13:31:02
 * table: ad_assets_track
 */
@Mapper
public interface AdAssetsTrackMapper extends BaseMapper<AdAssetsTrack> {
}


