package com.pig4cloud.pig.ads.pig.mapper.ks;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.KsAccountWater;
import org.apache.ibatis.annotations.Mapper;

/**
 * 快手账号流水明细
 * @author  chenxiang
 * @version  2022-03-24 13:56:21
 * table: ks_account_water
 */
@Mapper
public interface KsAccountWaterMapper extends BaseMapper<KsAccountWater> {
}


