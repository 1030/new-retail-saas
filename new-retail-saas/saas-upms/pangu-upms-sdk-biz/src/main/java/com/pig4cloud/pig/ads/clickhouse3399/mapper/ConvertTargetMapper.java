package com.pig4cloud.pig.ads.clickhouse3399.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.admin.api.entity.SysDictItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @ClassName ConvertTargetMapper.java
 * @author hma
 * @version 1.0.0
 * @Time 2021/06/16 16:35
 */
@Mapper
public interface ConvertTargetMapper extends BaseMapper<SysDictItem> {
	@SqlParser(filter=true)
	List<SysDictItem> selectConvertTarget(Map<String,Object> param);

}
