package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdPromotion;
import org.apache.ibatis.annotations.Mapper;

/**
 * 广告表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:21:02
 * table: ad_promotion
 */
@Mapper
public interface AdPromotionMapper extends BaseMapper<AdPromotion> {

	AdPromotion selectByPromotionId(Long promotionId);

	int updateByPrimaryKeySelective(AdPromotion adPromotion);

}


