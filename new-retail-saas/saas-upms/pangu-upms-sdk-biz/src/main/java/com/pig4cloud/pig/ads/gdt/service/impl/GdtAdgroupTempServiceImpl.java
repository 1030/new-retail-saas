package com.pig4cloud.pig.ads.gdt.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbActivity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.pig4cloud.pig.ads.gdt.service.*;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtAdgroupMapper;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtAdgroupTempMapper;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtCampaignMapper;
import com.pig4cloud.pig.api.enums.GdtOptimizationGoalEnum;
import com.pig4cloud.pig.api.enums.StatusEnum;
import com.pig4cloud.pig.api.gdt.dto.*;
import com.pig4cloud.pig.api.gdt.entity.*;
import com.pig4cloud.pig.api.gdt.vo.SiteSetVo;
import com.pig4cloud.pig.api.util.JsonUtil;
import com.pig4cloud.pig.api.vo.GdtAdgroupTempVo;
import com.pig4cloud.pig.ads.gdt.service.*;
import com.pig4cloud.pig.api.vo.GdtOptimzationGoalVo;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jpedal.parser.color.G;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 广点通-广告组临时 服务层实现
 *
 * @author hma
 * @date 2020-12-05
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class GdtAdgroupTempServiceImpl extends ServiceImpl<GdtAdgroupTempMapper, GdtAdgroupTemp> implements IGdtAdgroupTempService {


	@Value(value = "${gdt_url}")
	private String gdtUrl;

	@Value(value = "${gdt_creative_templates_get_url}")
	private String gdtSiteSetUrl;
	@Resource
	private IGdtCampaignService gdtCampaignService;
	@Resource
	private IGdtAdgroupService gdtAdgroupService;
	@Resource
	private GdtCampaignMapper gdtCampaignMapper;
	@Resource
	private IGdtAdCreativeService gdtAdCreativeService;
	@Resource
	private GdtAudiencePackageService gdtAudiencePackageService;
	@Resource
	private GdtAdgroupMapper gdtAdgroupMapper;

	@Value(value = "${gdt_promoted_object_url}")
	private String gdtPromotedObjectUrl;
	@Value(value = "${gdt_conversions_get_url}")
	private String gdtConversionsGetUrl;
	@Value(value = "${gdt_optimization_goal_permissions_url}")
	private String gdtOptimizationGoalUrl;
	@Value(value = "${gdt_user_action}")
	private String gdtUserAction;


	@Override
	public R getGroupSiteSet(GdtAdgroupTempDto gdtAdgroupTempDto) {
		//todo  广告版本查询，缓存，存储广告版本表--或者直接拉取存储到字典表，避免每一块功能都创建新表
		if (StringUtils.isBlank(gdtAdgroupTempDto.getPromotedObjectType())) {
			//推广目标为空，查询推广列表获取推广计划对应的推广目标
			List<GdtCampaign> gdtCampaignList = gdtCampaignService.gdtCampaignList(new GdtCampaign(gdtAdgroupTempDto.getCampaignId()));
			if (CollectionUtils.isEmpty(gdtCampaignList)) {
				return R.failed("当前推广计划不存在");
			}
			gdtAdgroupTempDto.setPromotedObjectType(gdtCampaignList.get(0).getPromotedObjectType());
		}
		String fields = "promoted_object_type,site_set,adcreative_template_id";
		R result = gdtAdCreativeService.getCreativeTemplates(new GdtAdCreativeDto(gdtAdgroupTempDto.getPromotedObjectType()), fields, StatusEnum.TYPE_ONE.getStatus());
		if (null != result.getData()) {
			JSONArray jsonArray = JSONArray.parseArray(JSONArray.toJSONString(result.getData()));
			List<GdtCreativeTemplates> gdtCreativeTemplatesList = jsonArray.toJavaList(GdtCreativeTemplates.class);
			List<String> siteSetList = new ArrayList<>();
			gdtCreativeTemplatesList.forEach(cv -> {
				String siteS = cv.getSiteSet();
				siteSetList.add(siteS);
			});
			List<String> sitList = siteSetList.stream().distinct().collect(Collectors.toList());
			List<SiteSetVo> siteSetVoList = new ArrayList<>();
			sitList.forEach(s -> {
				SiteSetVo siteSetVo = new SiteSetVo();
				siteSetVo.setSiteSet(s);
				//  添加广告版本中文显示内容
				siteSetVo.setSiteSetValue(getSiteSetValue(s));
				siteSetVoList.add(siteSetVo);
			});
			return R.ok(siteSetVoList);
		}
		return R.failed("当前不支持广告版位");


	}

	private R dataValidate(JSONObject targeting){
		if (targeting.getJSONArray("age") != null){
			JSONArray ages = targeting.getJSONArray("age");
			if (ages.size() != 1){
				return R.failed("年龄 数组长度为 1");
			}

			JSONObject age = ages.getJSONObject(0);
			if (StringUtils.isBlank(age.getString("min"))) {
				return R.failed("缺少最小年龄");
			}
			if (StringUtils.isBlank(age.getString("max"))) {
				return R.failed("缺少最大年龄");
			}

			if (age.getInteger("min") > age.getInteger("max")) {
				return R.failed("最小年龄不能大于最大年龄");
			}
		}

		if (targeting.getJSONArray("temperature") != null){
			JSONArray temperatures = targeting.getJSONArray("temperature");
			if (targeting.getJSONArray("temperature").size() != 1) {
				return R.failed("温度 为单选");
			}
			JSONObject temperature = temperatures.getJSONObject(0);
			if (StringUtils.isBlank(temperature.getString("min"))) {
				return R.failed("缺少最低温度");
			}
			if (StringUtils.isBlank(temperature.getString("max"))) {
				return R.failed("缺少最高温度");
			}
			if (temperature.getInteger("min") > temperature.getInteger("max")) {
				return R.failed("最低温度不能大于最高温度");
			}
		}

		if (targeting.getJSONArray("residential_community_price") != null){
			JSONArray arr = targeting.getJSONArray("residential_community_price");
			if (targeting.getJSONArray("residential_community_price").size() != 1) {
				return R.failed("居住社区价格 为单选");
			}
			JSONObject price = arr.getJSONObject(0);
			if (StringUtils.isBlank(price.getString("min"))) {
				return R.failed("居住社区价格 缺少最小价格");
			}
			if (StringUtils.isBlank(price.getString("max"))) {
				return R.failed("居住社区价格 缺少最大价格");
			}
			if (price.getInteger("min") >= price.getInteger("max")) {
				return R.failed("居住社区价格 最小价格不能>=最大价格");
			}
		}

		//行为兴趣 校验
		JSONObject behaviorInterest = targeting.getJSONObject("behavior_or_interest");
		if (behaviorInterest != null){
			JSONObject interest = behaviorInterest.getJSONObject("interest");
			JSONArray behavior = behaviorInterest.getJSONArray("behavior");
			JSONObject intention = behaviorInterest.getJSONObject("intention");
			if (interest != null){
				JSONArray cateList = intention.getJSONArray("category_id_list");
				JSONArray keywords = intention.getJSONArray("keyword_list");
				if (cateList != null && (cateList.size() <1 || cateList.size() > 250 )) {
					return R.failed("兴趣目录选项 最小长度 1，最大长度 250");
				}
				if (keywords != null && (keywords.size() < 1 || keywords.size()>250))
					return R.failed("兴趣关键词选项 最小长度 1，最大长度 250");
			}
			if (behavior != null){
				if (behavior.size() != 1) {
					return R.failed("behavior 数组长度必须为1");
				}
				JSONObject bahObj = behavior.getJSONObject(0);
				if (StringUtils.isBlank(bahObj.getString("time_window"))) {
					return R.failed("行为定向 必须提供时间窗");
				}
				JSONArray scene = bahObj.getJSONArray("scene");
				JSONArray intensity = bahObj.getJSONArray("intensity");
				JSONArray cateList = bahObj.getJSONArray("category_id_list");
				JSONArray keywords = bahObj.getJSONArray("keyword_list");
				if (cateList != null && (cateList.size() <1 || cateList.size() > 250 )) {
					return R.failed("行为目录选项 最小长度 1，最大长度 250");
				}
				if (keywords != null && (keywords.size() < 1 || keywords.size()>250)) {
					return R.failed("行为关键词选项 最小长度 1，最大长度 250");
				}
				if (scene !=null && scene.size()<1 && scene.size()>3) {
					return R.failed("行为定向 最少提供1个场景，最多提供3个");
				}
				if (intensity != null && intensity.size() !=1) {
					return R.failed("行为定向 需提供且只能提供1个强度选项");
				}
			}
			if (intention != null){
				JSONArray tags = intention.getJSONArray("targeting_tags");
				if (tags != null && tags.size()<1 && tags.size() > 250) {
					return R.failed("行为兴趣-意向选项为1~250个");
				}
			}

		}
		return R.ok("验证通过");
	}
	//todo 临时广告计划 刚广告组创建异常也存储作为一个用户的模板
	@Override
	public R createGdtGroupTemp(GdtAdgroupCreateTempDto gdtAdgroupCreateTempDto) {
		//  添加userId字段
		try {
			//是否是拷贝
			Boolean copyFlag = gdtAdgroupCreateTempDto.getCopyFlag();

			Integer userId = Objects.requireNonNull(SecurityUtils.getUser()).getId();
			gdtAdgroupCreateTempDto.setUserId(userId.longValue());
			if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getPromotedObjectType())) {
				GdtCampaign gdtCampaign = gdtCampaignMapper.selectById(gdtAdgroupCreateTempDto.getCampaignId());
				if (null == gdtCampaign) {
					return R.failed("当前推广计划不存在");
				}
				gdtAdgroupCreateTempDto.setPromotedObjectType(gdtCampaign.getPromotedObjectType());
			}
			if (!"1".equals(gdtAdgroupCreateTempDto.getAutomaticSiteEnabled())) {
				JSONArray jsonArray = JSON.parseArray(gdtAdgroupCreateTempDto.getSiteSets());
				if (CollectionUtils.isEmpty(jsonArray)) {
					return R.failed("广告版位不允许为空");
				}
			}
			if (StringUtils.isNotBlank(gdtAdgroupCreateTempDto.getSiteSets())) {
				gdtAdgroupCreateTempDto.setSiteSet(gdtAdgroupCreateTempDto.getSiteSets());
			}
			Long id = gdtAdgroupCreateTempDto.getId();
			if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getEndDate())) {
				gdtAdgroupCreateTempDto.setEndDate(null);
			}
			if (StringUtils.equals(gdtAdgroupCreateTempDto.getLaunchDateType(), "1")) {
				if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getBeginDate())) {
					return R.failed("开始时间不允许为空");
				}
				gdtAdgroupCreateTempDto.setEndDate(null);
			}
			if (StringUtils.equals(gdtAdgroupCreateTempDto.getLaunchDateType(), "2")) {
				if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getBeginDate()) || StringUtils.isBlank(gdtAdgroupCreateTempDto.getEndDate())) {
					return R.failed("开始时间和结束时间不允许为空");
				}
			}
			//复制不执行以下方法
			if (!copyFlag) {
				//idStr为0时，前端返回主键id为空
				if (Objects.isNull(id)) {
					int count = gdtAdgroupService.count(Wrappers.<GdtAdgroup>query().lambda().eq(GdtAdgroup::getAdgroupName,gdtAdgroupCreateTempDto.getAdgroupName()));
					if (count>0) {
						return R.failed("广告名称不允许重复");
					}
					baseMapper.insert(gdtAdgroupCreateTempDto);
				} else {
					GdtAdgroupTemp gdtAdgroupTemp = baseMapper.selectById(id);
					if (Objects.isNull(gdtAdgroupTemp)) {
						return R.failed("广告组临时表中无此广告组");
					}
					gdtAdgroupCreateTempDto.setAdgroupId(gdtAdgroupTemp.getAdgroupId());
					baseMapper.updateById(gdtAdgroupCreateTempDto);
				}
			} else {
				int count = gdtAdgroupService.count(Wrappers.<GdtAdgroup>query().lambda()
						.eq(GdtAdgroup::getAdgroupName,gdtAdgroupCreateTempDto.getAdgroupName()));
				if (count>0) {
					return R.failed("广告名称不允许重复");
				}
				//处理为{}空的情况
				String targeting = gdtAdgroupCreateTempDto.getTargeting();
				Long targetingId = gdtAdgroupCreateTempDto.getTargetingId();
				if (StringUtils.isNotBlank(targeting)) {
					JSONObject jsonObject = JSONObject.parseObject(targeting);
					if (jsonObject.isEmpty() && targetingId != null) {
						gdtAdgroupCreateTempDto.setTargeting(null);
					} else {
						gdtAdgroupCreateTempDto.setTargeting(jsonObject.toJSONString());
					}
				}
			}
			gdtAdgroupCreateTempDto.setAdgroupId(gdtAdgroupCreateTempDto.getId());
			if (null == gdtAdgroupCreateTempDto.getTargetingId()) {
				//  当用户定向为空的时候,判断定向里面的参数
				if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getTargeting())) {
					return R.failed("定向包id与定向详细设置不能同时为空");
				}
				GdtAudiencePackage gdtAudiencePackage = JSONObject.parseObject(gdtAdgroupCreateTempDto.getTargeting(), GdtAudiencePackage.class);

				String paramJson= JsonUtil.toUnderlineJSONString(gdtAudiencePackage);
				JSONObject jsonObject =JSONObject.parseObject(paramJson);
				//数据校验
				R validateResult = this.dataValidate(jsonObject);
				if (validateResult.getCode() != CommonConstants.SUCCESS) {
					return validateResult;
				}

				if (null != gdtAudiencePackage) {
					Optional.ofNullable(gdtAudiencePackage.getBehaviorOrInterest()).ifPresent(p -> {
						Map<String, Object> stringObjectMap = JsonUtil.parseJSON2Map(p);
						gdtAudiencePackage.setBehaviorOrInterest(JSON.toJSONString(stringObjectMap));
					});
					Optional.ofNullable(gdtAudiencePackage.getEducation()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getEducation()).size() < 1) ? null : p;
						gdtAudiencePackage.setEducation(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getFinancialSituation()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getFinancialSituation()).size() < 1) ? null : p;
						gdtAudiencePackage.setFinancialSituation(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getConsumptionType()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getConsumptionType()).size() < 1) ? null : p;
						gdtAudiencePackage.setConsumptionType(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getGameConsumptionLevel()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getGameConsumptionLevel()).size() < 1) ? null : p;
						gdtAudiencePackage.setGameConsumptionLevel(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getConsumptionStatus()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getConsumptionStatus()).size() < 1) ? null : p;
						gdtAudiencePackage.setConsumptionStatus(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getNewDevice()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getNewDevice()).size() < 1) ? null : p;
						gdtAudiencePackage.setNewDevice(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getNetworkScene()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getNetworkScene()).size() < 1) ? null : p;
						gdtAudiencePackage.setNetworkScene(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getNetworkType()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getNetworkType()).size() < 1) ? null : p;
						gdtAudiencePackage.setNetworkType(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getNetworkOperator()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getNetworkOperator()).size() < 1) ? null : p;
						gdtAudiencePackage.setNetworkOperator(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getDevicePrice()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getDevicePrice()).size() < 1) ? null : p;
						gdtAudiencePackage.setDevicePrice(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getUvIndex()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getUvIndex()).size() < 1) ? null : p;
						gdtAudiencePackage.setUvIndex(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getDressingIndex()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getDressingIndex()).size() < 1) ? null : p;
						gdtAudiencePackage.setDressingIndex(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getMakeupIndex()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getMakeupIndex()).size() < 1) ? null : p;
						gdtAudiencePackage.setMakeupIndex(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getClimate()).ifPresent(p -> {
						p = (JSONArray.parseArray(gdtAudiencePackage.getClimate()).size() < 1) ? null : p;
						gdtAudiencePackage.setClimate(p);
					});
					Optional.ofNullable(gdtAudiencePackage.getExcludedConvertedAudience()).ifPresent(p -> {
						Map<String, Object> stringObjectMap = JsonUtil.parseJSON2Map(p);
						gdtAudiencePackage.setExcludedConvertedAudience(JSON.toJSONString(stringObjectMap));
					});
					gdtAudiencePackage.setAccountId(null);
					gdtAudiencePackage.setTargetingName(null);
					//对象大小写专家为下划线
					gdtAdgroupCreateTempDto.setTargeting(JsonUtil.toUnderlineJSONString(gdtAudiencePackage));
				}
			}


			if (StringUtils.equals(gdtAdgroupCreateTempDto.getAutoAcquisitionEnabled(), "true")) {
				Integer autoBudget = gdtAdgroupCreateTempDto.getAutoAcquisitionBudget();
				if (null != autoBudget) {
					if (autoBudget < 20000 || autoBudget > 10000000) {
						return R.failed("一键起量探索预算，区间值为 20000~10000000(单位:分)");
					}
				}
			}

			if (null != gdtAdgroupCreateTempDto.getTargetingObject()) {
				gdtAdgroupCreateTempDto.setTargeting(JSONObject.toJSONString(gdtAdgroupCreateTempDto.getTargetingObject()));
			}
			if (null != gdtAdgroupCreateTempDto.getBidAdjustmentObject()) {
				gdtAdgroupCreateTempDto.setBidAdjustment(JSONObject.toJSONString(gdtAdgroupCreateTempDto.getBidAdjustmentObject()));
			}
			if (StringUtils.isNotBlank(gdtAdgroupCreateTempDto.getSceneSpec())) {
				//场景优良汇对应的内容
				if (!gdtAdgroupCreateTempDto.getSiteSets().equals("SITE_SET_MOBILE_UNION")||MapUtil.isEmpty(JsonUtil.parseJSON2Map(gdtAdgroupCreateTempDto.getSceneSpec()))) {
					gdtAdgroupCreateTempDto.setSceneSpec(null);
				}
			}
			return R.ok(gdtAdgroupCreateTempDto, "广告创建成功！");
		} catch (Exception e) {
			throw new BusinessException(11, e.getMessage());
		}

	}

	/**
	 * 获取广告组模板
	 *
	 * @return
	 */
	@Override
	public GdtAdgroupTemp getCreateGdtGroupById(Long adgroupId){
		return baseMapper.selectById(adgroupId);
	}



	/**
	 * 创建广告组模板
	 *
	 * @return
	 */
	@Override
	public GdtAdgroupCreateTempDto createGdtGroup(GdtAdCreativeCreateDto dto, List<Long> creativeIdList) throws JsonProcessingException {
		GdtAdgroupTemp gdtAdgroupTemp = baseMapper.selectById(dto.getAdgroupId());
		GdtAdgroupCreateTempDto gdtAdgroupCreateTempDto = new GdtAdgroupCreateTempDto();
		BeanUtils.copyProperties(gdtAdgroupTemp,gdtAdgroupCreateTempDto);
		gdtAdgroupCreateTempDto.setAutoDerivedCreativeEnabled(StringUtils.isNotBlank(dto.getAutoDerivedCreativeEnabled())||"1".equals(dto.getAutoDerivedCreativeEnabled())?"true":"false");
		gdtAdgroupCreateTempDto.setCreativeDisplayType(StringUtils.isNotBlank(dto.getCreativeDisplayType())||"1".equals(dto.getCreativeDisplayType())?"CREATIVE_DISPLAY_TYPE_AVERAGE":"CREATIVE_DISPLAY_TYPE_INTELLIGENCE");
		gdtAdgroupCreateTempDto.setAccountId(dto.getAccountId()+"");

		if (null == gdtAdgroupCreateTempDto.getTargetingId()) {
			GdtAudiencePackage gdtAudiencePackage = JSONObject.parseObject(gdtAdgroupCreateTempDto.getTargeting(), GdtAudiencePackage.class);
			if (null != gdtAudiencePackage) {
				Optional.ofNullable(gdtAudiencePackage.getBehaviorOrInterest()).ifPresent(p -> {
					Map<String, Object> stringObjectMap = JsonUtil.parseJSON2Map(p);
					gdtAudiencePackage.setBehaviorOrInterest(JSON.toJSONString(stringObjectMap));
				});
				Optional.ofNullable(gdtAudiencePackage.getEducation()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getEducation()).size() < 1) ? null : p;
					gdtAudiencePackage.setEducation(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getFinancialSituation()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getFinancialSituation()).size() < 1) ? null : p;
					gdtAudiencePackage.setFinancialSituation(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getConsumptionType()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getConsumptionType()).size() < 1) ? null : p;
					gdtAudiencePackage.setConsumptionType(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getGameConsumptionLevel()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getGameConsumptionLevel()).size() < 1) ? null : p;
					gdtAudiencePackage.setGameConsumptionLevel(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getConsumptionStatus()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getConsumptionStatus()).size() < 1) ? null : p;
					gdtAudiencePackage.setConsumptionStatus(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getNewDevice()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getNewDevice()).size() < 1) ? null : p;
					gdtAudiencePackage.setNewDevice(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getNetworkScene()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getNetworkScene()).size() < 1) ? null : p;
					gdtAudiencePackage.setNetworkScene(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getNetworkType()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getNetworkType()).size() < 1) ? null : p;
					gdtAudiencePackage.setNetworkType(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getNetworkOperator()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getNetworkOperator()).size() < 1) ? null : p;
					gdtAudiencePackage.setNetworkOperator(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getDevicePrice()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getDevicePrice()).size() < 1) ? null : p;
					gdtAudiencePackage.setDevicePrice(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getUvIndex()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getUvIndex()).size() < 1) ? null : p;
					gdtAudiencePackage.setUvIndex(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getDressingIndex()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getDressingIndex()).size() < 1) ? null : p;
					gdtAudiencePackage.setDressingIndex(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getMakeupIndex()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getMakeupIndex()).size() < 1) ? null : p;
					gdtAudiencePackage.setMakeupIndex(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getClimate()).ifPresent(p -> {
					p = (JSONArray.parseArray(gdtAudiencePackage.getClimate()).size() < 1) ? null : p;
					gdtAudiencePackage.setClimate(p);
				});
				Optional.ofNullable(gdtAudiencePackage.getExcludedConvertedAudience()).ifPresent(p -> {
					Map<String, Object> stringObjectMap = JsonUtil.parseJSON2Map(p);
					gdtAudiencePackage.setExcludedConvertedAudience(JSON.toJSONString(stringObjectMap));
				});
				gdtAudiencePackage.setAccountId(null);
				gdtAudiencePackage.setTargetingName(null);
				//对象大小写专家为下划线
				gdtAdgroupCreateTempDto.setTargeting(JsonUtil.toUnderlineJSONString(gdtAudiencePackage));
			}
		}
		gdtAdgroupService.createGdtGroup(gdtAdgroupCreateTempDto,creativeIdList);
		GdtAdgroupCreateTempDto gdtAdgroupCreateTempDtoF = gdtAdgroupCreateTempDto;
		ExecutorService es = new ThreadPoolExecutor(5, 5, 10, TimeUnit.SECONDS, new ArrayBlockingQueue<>(50));
		es.submit(() -> {
			try {
				// 拉取广告组数据并存储
				QueryWrapper<GdtAdgroupTemp> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("id", gdtAdgroupCreateTempDtoF.getId());//条件：广告id
				String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
				Integer time = Integer.valueOf(timestamp);
				baseMapper.update(new GdtAdgroupTemp(gdtAdgroupCreateTempDtoF.getAdgroupId(), time.longValue()), queryWrapper);
				if (null != gdtAdgroupCreateTempDtoF.getAdgroupId()) {
					gdtAdgroupService.getGdtGroup(gdtAdgroupCreateTempDtoF);
				}
			} catch (Exception e) {
				log.error("createGdtGroupTemp is error", e);
			}
		});
		return gdtAdgroupCreateTempDto;
	}


	/**
	 * 获取广告组模板
	 *
	 * @param gdtAdgroupTempDto
	 * @return
	 */
	@Override
	public R getGroupTemp(GdtAdgroupTempDto gdtAdgroupTempDto) {
		GdtAdgroupTemp gdtAdgroupTemp = null;
		if (Objects.nonNull(gdtAdgroupTempDto.getId())) {
			gdtAdgroupTemp = baseMapper.getGdtAdgroupTemplateById(gdtAdgroupTempDto);
			if (Objects.isNull(gdtAdgroupTemp)) {
				return R.failed("无此广告组");
			}
		}
		if(Objects.nonNull(gdtAdgroupTempDto.getCampaignId())){
			gdtAdgroupTemp = baseMapper.getGdtAdgroupTemplateById(gdtAdgroupTempDto);
		}

		if(Objects.isNull(gdtAdgroupTemp)) {
			// 查询最新的，取一个
			Integer userId = Objects.requireNonNull(SecurityUtils.getUser()).getId();
			List<GdtAdgroupTemp> gdtAdgroupTempList = baseMapper.selectGdtAdgroupTemplate(new GdtAdgroupTempDto(userId.longValue(), gdtAdgroupTempDto.getAccountId(), StatusEnum.TYPE_ONE.getStatus()));
			if (CollectionUtils.isEmpty(gdtAdgroupTempList)) {
				return R.ok();
			}
			gdtAdgroupTemp = gdtAdgroupTempList.get(0);
		}
		GdtAdgroupTemp finalGdtAdgroupTemp = gdtAdgroupTemp;
		Optional.ofNullable(gdtAdgroupTemp.getDailyBudget()).ifPresent(p -> finalGdtAdgroupTemp.setDailyBudget(p.divide(new BigDecimal("100").setScale(2))));
		Optional.ofNullable(gdtAdgroupTemp.getBidAmount()).ifPresent(p -> finalGdtAdgroupTemp.setBidAmount(p.divide(new BigDecimal("100").setScale(2))));
		Optional.ofNullable(gdtAdgroupTemp.getAutoAcquisitionBudget()).ifPresent(p -> finalGdtAdgroupTemp.setAutoAcquisitionBudget(p / 100));
		GdtAdgroupTempVo gdtAdgroupTempVo = new GdtAdgroupTempVo();
		BeanUtils.copyProperties(gdtAdgroupTemp, gdtAdgroupTempVo);
		if (null != gdtAdgroupTempVo.getTargetingId()) {
			LambdaQueryWrapper<GdtAudiencePackage> queryWrapper = Wrappers.<GdtAudiencePackage>query().lambda()
					.and(wrapper -> wrapper.eq(GdtAudiencePackage::getIdAdplatform, gdtAdgroupTempVo.getTargetingId()))
					.last("LIMIT 1");
			List<GdtAudiencePackage> gdtAudiencePackageList = gdtAudiencePackageService.getBaseMapper().selectList(queryWrapper);
			if (CollectionUtils.isNotEmpty(gdtAudiencePackageList)) {
				gdtAdgroupTempVo.setTargetingName(gdtAudiencePackageList.get(0).getTargetingName());
			}
		}
		if(StringUtils.isBlank(gdtAdgroupTempVo.getAppAndroidChannelPackageId())){
			gdtAdgroupTempVo.setAppAndroidChannelPackageId("0");
		}

		Date sDate = DateUtil.parse(gdtAdgroupTempVo.getBeginDate(), "yyyy-MM-dd");
		if (sDate.compareTo(new Date()) < 0) {
			gdtAdgroupTempVo.setBeginDate(DateUtil.format(new Date(),"yyyy-MM-dd"));
		}
		return R.ok(gdtAdgroupTempVo);
	}


	/**
	 * 每一个推广目标只对应一个推广对象id
	 *
	 * @return
	 */
	@Override
	public R getPromoteObjectId(GdtAdgroupTempDto gdtAdgroupTempDto) {
		String fields = "promoted_object_name,promoted_object_id,promoted_object_type,promoted_object_spec";
		List<FilteringDto> filteringDtos = new ArrayList<FilteringDto>();
		if (StringUtils.isNotBlank(gdtAdgroupTempDto.getPromotedObjectType())) {
			filteringDtos.add(new FilteringDto("promoted_object_type", "EQUALS", gdtAdgroupTempDto.getPromotedObjectType().split(",")));
		}
		List<JSONArray> jsonArrayList = gdtCampaignService.getThirdResponse(gdtAdgroupTempDto.getAccountId(), "getPromoteObjectId", gdtPromotedObjectUrl, filteringDtos, null, fields, Boolean.TRUE);
		List<Map<String, Object>> promoteObjectList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(jsonArrayList)) {
			//todo  字典替换
			jsonArrayList.forEach(ps -> {
				ps.forEach(r -> {
					JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(r));
					String promoted_object_type = jsonObject.getString("promoted_object_type");
					if("PROMOTED_OBJECT_TYPE_APP_ANDROID".equals(promoted_object_type)){
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("projectObjectName", jsonObject.getString("promoted_object_name"));
						map.put("promotedObjectId", jsonObject.getString("promoted_object_id"));
						promoteObjectList.add(map);
					}
				});

			});

			return R.ok(promoteObjectList);
		}
		return R.failed("当前推广目标没有支持对应的推广对象");
	}

	/**
	 * @return 应用对应的转换归因数据查询
	 */
	/*@Override
	public R getUserActionSets(GdtAdgroupTempDto gdtAdgroupTempDto) {
		//todo 转换归因受投放广告版位影响
		//todo  删除状态 ：是否已删除，true：是，false：否，以及当前长点是否可用  当前站点是否可用，true：是，false：否
		String fields = "activate_status,type,name,description,user_action_set_id,created_time";
		Map<String, String> paramMap = new HashMap<>();
		if (StringUtils.isNotBlank(gdtAdgroupTempDto.getPromotedObjectId())) {
			paramMap.put("mobile_app_id", gdtAdgroupTempDto.getPromotedObjectId());
		}
		List<JSONArray> jsonArrayList = gdtCampaignService.getThirdResponse(gdtAdgroupTempDto.getAccountId(), "getUserActionSets", gdtUserAction, null, paramMap, fields, Boolean.FALSE);
		List resultList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(jsonArrayList)) {
			//todo  字典替换
			jsonArrayList.forEach(ps -> ps.forEach(r -> {
				JSONObject jsonObject = JSONObject.parseObject(r.toString());
				JSONArray jsonArray = jsonObject.getJSONArray("list");
				resultList.addAll(jsonArray);
			}));
			return R.ok(resultList);
		}
		return R.failed("当前没有对应的转换归因数据");
	}*/


	/**
	 * todo  如果查询对应的投放目标
	 *
	 * @return 广告优化目标权限查询
	 */
	@Override
	public R getOptimizationPermission(GdtAdgroupTempDto gdtAdgroupTempDto) {
		//todo 查询广告版位的影响
		String fields = "";

		if (StringUtils.isBlank(gdtAdgroupTempDto.getSiteSets())) {
			//默认设置腾讯新闻 广告版位
			String siteSets = "SITE_SET_TENCENT_NEWS";
			gdtAdgroupTempDto.setSiteSets(JSONArray.toJSONString(siteSets.split(",")));
		} else {
			JSONArray jsonArray = JSONArray.parseArray(gdtAdgroupTempDto.getSiteSets());
			if ((null != jsonArray && jsonArray.size() > 1) || jsonArray.size() == 0) {
				String siteSets = "SITE_SET_TENCENT_NEWS";
				gdtAdgroupTempDto.setSiteSets(JSONArray.toJSONString(siteSets.split(",")));
				gdtAdgroupTempDto.setSiteSet(JSONArray.toJSONString(siteSets.split(",")));
			}
		}

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("promoted_object_type", gdtAdgroupTempDto.getPromotedObjectType());
		if (StringUtils.isNotBlank(gdtAdgroupTempDto.getSiteSets())) {
			paramMap.put("site_set", gdtAdgroupTempDto.getSiteSets());
		}
		List<JSONArray> jsonArrayList = gdtCampaignService.getThirdResponse(gdtAdgroupTempDto.getAccountId(), "getUserActionSets", gdtOptimizationGoalUrl, null, paramMap, fields, Boolean.FALSE);
		JSONArray permissionResult = new JSONArray();
		if (CollectionUtils.isNotEmpty(jsonArrayList)) {
			//todo  字典替换
			jsonArrayList.forEach(ps -> {
				ps.forEach(r -> {
					JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(r));
					permissionResult.addAll(JSON.parseArray(jsonObject.getString("optimization_goal_permission_list")));
				});

			});
			List<Map<String, Object>> mapList = new ArrayList<>();
			permissionResult.forEach(p -> {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("permissionName", p);
				map.put("permissionValue", getReturnValue(p.toString()));
				mapList.add(map);
			});
			return R.ok(mapList);


		}
		return R.ok("");
	}


	/**
	 * 获取出价方式
	 *
	 * @return
	 */
	@Override
	public R getBidMode(GdtAdgroupTempDto gdtAdgroupTempDto) {
		GdtAdCreativeDto creativeDto = new GdtAdCreativeDto();
		creativeDto.setAccountId(gdtAdgroupTempDto.getAccountId());
		creativeDto.setPromotedObjectType(gdtAdgroupTempDto.getPromotedObjectType());
		creativeDto.setSiteSets(gdtAdgroupTempDto.getSiteSets());
		//todo 广告在不同的位置可能会有更好的表现，使用自动版位时，系统将根据你的设置，选择表现更好的位置进行投放，并帮你触达尽可能多的目标用户。
		// 自动版位暂不支持包括微信朋友圈、微信公众号与小程序、应用宝、PC等版位，如需投放可按媒体选择。
		String fields = "promoted_object_type,site_set,support_bid_mode_list";
		//todo  替换查询数据库表
		R r = gdtAdCreativeService.getCreativeTemplates(creativeDto, fields, StatusEnum.TYPE_TWO.getStatus());

		List<Map> bidMapList = new ArrayList<>();
		if (null != r.getData()) {
			List<String> bidModeR = new ArrayList<>();
			JSONArray jsonArray = JSONArray.parseArray(JSONArray.toJSONString(r.getData()));
			List<GdtCreativeTemplates> gdtCreativeTemplatesList = jsonArray.toJavaList(GdtCreativeTemplates.class);
			gdtCreativeTemplatesList.forEach(cv -> {
				JSONArray bidModes = JSONArray.parseArray(cv.getSupportBidModeList());
				bidModeR.addAll(bidModes.stream().map(Object::toString).collect(Collectors.toList()));
			});
			List<String> bidModeList = bidModeR.stream().distinct().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
			bidModeList.forEach(bidMode -> {
				Map<String, Object> bidMap = new HashMap<>();
				bidMap.put("bidMode", bidMode);
				bidMap.put("bidModeName", getReturnValue(bidMode));
				bidMapList.add(bidMap);
			});
		}

		return R.ok(bidMapList);
	}

	@Override
	public R getOptimizationGoal(GdtAdgroupTempDto gdtAdgroupTempDto) {
		//todo 查询广告版位的影响
		String fields = "";

		if (StringUtils.isBlank(gdtAdgroupTempDto.getSiteSets())) {
			//默认设置腾讯新闻 广告版位
			String siteSets = "SITE_SET_TENCENT_NEWS";
			gdtAdgroupTempDto.setSiteSets(JSONArray.toJSONString(siteSets.split(",")));
		} else {
			JSONArray jsonArray = JSONArray.parseArray(gdtAdgroupTempDto.getSiteSets());
			if ((null != jsonArray && jsonArray.size() > 1) || jsonArray.size() == 0) {
				String siteSets = "SITE_SET_TENCENT_NEWS";
				gdtAdgroupTempDto.setSiteSets(JSONArray.toJSONString(siteSets.split(",")));
				gdtAdgroupTempDto.setSiteSet(JSONArray.toJSONString(siteSets.split(",")));
			}
		}
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("promoted_object_type", gdtAdgroupTempDto.getPromotedObjectType());
		if (StringUtils.isNotBlank(gdtAdgroupTempDto.getSiteSets())) {
			paramMap.put("site_set", gdtAdgroupTempDto.getSiteSets());
		}
		List<JSONArray> jsonArrayList = gdtCampaignService.getThirdResponse(gdtAdgroupTempDto.getAccountId(), "getUserActionSets", gdtOptimizationGoalUrl, null, paramMap, fields, Boolean.FALSE);
		List<GdtOptimzationGoalVo> resultDataList=new ArrayList<>();
		if (CollectionUtils.isNotEmpty(jsonArrayList)) {
			jsonArrayList.forEach(ps -> ps.forEach(r -> {
				JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(r));
				JSONArray jsonArray=JSON.parseArray(jsonObject.getString("deep_behavior_optimization_goal_permission_list"));
				Map<String,List<GdtOptimzationGoalVo>> subVos=new HashMap<>();
				if(!Objects.isNull(jsonArray)){
					jsonArray.forEach(v->{
						JSONObject val=(JSONObject)v;
						String og=String.valueOf(val.get("optimization_goal"));
						List<GdtOptimzationGoalVo> vos=val.getJSONArray("deep_behavior_optimization_goal_list").stream().map(v1->{
							String key= String.valueOf(v1);
							GdtOptimzationGoalVo optimzationGoalVo=new GdtOptimzationGoalVo();
							optimzationGoalVo.setKey(key);
							optimzationGoalVo.setValue(GdtOptimizationGoalEnum.valueByType(key));
							return optimzationGoalVo;
						}).filter(v2->GdtOptimizationGoalEnum.checkAavailable(v2.getKey())).collect(Collectors.toList());
						subVos.put(og,vos);
					});
				}
				resultDataList.addAll(JSON.parseArray(jsonObject.getString("optimization_goal_permission_list")).stream()
						.filter(v->GdtOptimizationGoalEnum.checkAavailable(String.valueOf(v))).map(v->{
							String val= String.valueOf(v);
							GdtOptimzationGoalVo optimzationGoalVo=new GdtOptimzationGoalVo();
							optimzationGoalVo.setKey(val);
							optimzationGoalVo.setValue(GdtOptimizationGoalEnum.valueByType(val));
							if(MapUtil.isNotEmpty(subVos)){
								optimzationGoalVo.setOptions(subVos.get(val));
							}
					return optimzationGoalVo;
				}).collect(Collectors.toList()));
			}));
		}
		return R.ok(resultDataList);
	}


	public static String getSiteSetValue(String siteSet) {
		//todo  目前平台已废弃的不考虑
		if (StringUtils.isBlank(siteSet)) {
			return null;
		}
		switch (siteSet) {
			case "SITE_SET_MOBILE_UNION":
				return "优量汇";
			case "SITE_SET_KUAISHOU":
				return "快手";
			case "SITE_SET_WECHAT":
				return "微信";
			case "SITE_SET_TENCENT_NEWS":
				return "腾讯新闻";
			case "SITE_SET_TENCENT_VIDEO":
				return "腾讯视频";
			case "SITE_SET_MOBILE_YYB":
				return "应用宝";
			case "SITE_SET_PCQQ":
				return "QQ、QQ 空间、腾讯音乐、PC 版位";
			case "SITE_SET_KANDIAN":
				return "腾讯看点";
			case "SITE_SET_QQ_MUSIC_GAME":
				return "QQ、腾讯音乐及游戏";
			case "SITE_SET_MOMENTS":
				return "微信朋友圈";
			case "SITE_SET_MINI_GAME_WECHAT":
				return "微信生态内的小游戏场景";
			case "SITE_SET_MINI_GAME_QQ":
				return "手机 QQ 生态内的小游戏场景";
			case "SITE_SET_MOBILE_GAME":
				return "集合腾讯游戏和优量汇联盟生态的手机端游戏";
			case "SITE_SET_MOBILE_INNER":
				return "QQ、腾讯看点、腾讯音乐 (待废弃) ";
			case "SITE_SET_MOBILE_MYAPP":
				return "应用宝（待废弃） ";
			case "SITE_SET_QZONE":
				return "QQ 空间，PC 版位 (已废弃)";
			case "SITE_SET_QQCLIENT":
				return "QQ 客户端(已废弃)";
			case "SITE_SET_JD_WAICAI":
				return "京东外采流量 (已废弃)";
			case "SITE_SET_THIRDPARTY":
				return "第三方流量 (已废弃)";
			case "SITE_SET_MOBILE_UNION_DELETED":
				return "移动应用联盟（已废弃）";
			case "SITE_SET_WANGGOU":
				return "QQ 网购 (已废弃)";
			case "SITE_SET_WEIBO":
				return "微博 (已废弃)";
			case "SITE_SET_EXPRESSPORTAL":
				return "直通车外投 (已废弃)";
			case "SITE_SET_WEBUNION_DELETED":
				return "网站联盟 (已废弃)";
			case "SITE_SET_QZONESEARCH":
				return "QQ 空间搜索 (已废弃)";
			case "SITE_SET_PAIPAIDAOGOU":
				return "拍拍导购咨询 (已废弃)";
			case "SITE_SET_QQSHOP":
				return "QQ 商城 (已废弃)";
			case "SITE_SET_PAIPAISEARCH":
				return "拍拍站内搜索 (已废弃)";
			case "SITE_SET_YINGYONGBAO_PC":
				return "应用宝，PC 版位(已废弃)";
			case "SITE_SET_PC_UNION":
				return "优量汇，PC 版位(已废弃)";
			case "QQ 邮箱":
				return "QQ 邮箱 (已废弃)";
			case "SITE_SET_PIAO":
				return "QQ 票务 (已废弃)";
			case "SITE_SET_MEISHI":
				return "QQ 美食 (已废弃)";
			case "SITE_SET_TUAN":
				return "QQ 团购 (已废弃)";
			case "SITE_SET_PENGYOU":
				return "朋友社区 (已废弃)";
			case "SITE_SET_TENCENT_KUAIBAO":
				return "天天快报 (已废弃)";
			case "SITE_SET_QQCOM":
				return "腾讯网，PC 版位 (已废弃)";
			case "SITE_SET_MUSIC":
				return "QQ 音乐，PC 版位 (已废弃)";
			default:
				return null;
		}
	}


	/**
	 * 根据类型值返回对应的名称
	 *
	 * @param name
	 * @return
	 */
	private String getReturnValue(String name) {
		if (StringUtils.isBlank(name)) {
			return null;
		}
		switch (name) {
			case "OPTIMIZATIONGOAL_APP_ACTIVATE":
				return "激活";
			case "OPTIMIZATIONGOAL_ECOMMERCE_ORDER":
				return "下单";
			case "OPTIMIZATIONGOAL_FIRST_PURCHASE":
				return "首次付费";
			case "OPTIMIZATIONGOAL_APP_REGISTER":
				return "注册";
			case "OPTIMIZATIONGOAL_VIEW_COMMODITY_PAGE":
				return "商品详情页浏览";
			case "OPTIMIZATIONGOAL_ONE_DAY_RETENTION":
				return "次日留存";
			case "OPTIMIZATIONGOAL_ECOMMERCE_CART":
				return "加入购物车";
			case "OPTIMIZATIONGOAL_CLICK":
				return "点击";
			case "OPTIMIZATIONGOAL_PROMOTION_VIEW_KEY_PAGE":
				return "关键页面访问";
			case "OPTIMIZATIONGOAL_APP_DOWNLOAD":
				return "下载";
			case "OPTIMIZATIONGOAL_PAGE_RESERVATION":
				return "表单预约";
			case "OPTIMIZATIONGOAL_CANVAS_CLICK":
				return "跳转按钮点击";
			case "OPTIMIZATIONGOAL_ECOMMERCE_ADD_TO_WISHLIST":
				return "商品收藏";
			case "OPTIMIZATIONGOAL_CREDIT":
				return "授信";
			case "OPTIMIZATIONGOAL_WITHDRAW_DEPOSITS":
				return "提现";
			case "OPTIMIZATIONGOAL_APPLY":
				return "进件";
			case "OPTIMIZATIONGOAL_MOBILE_APP_AD_INCOME":
				return "广告变现";
			case "OPTIMIZATIONGOAL_PRE_CREDIT":
				return "预授信";
			case "BID_MODE_OCPC":
				return "oCPC";
			case "BID_MODE_OCPM":
				return "oCPM";
			case "BID_MODE_CPC":
				return "CPC";
			case "BID_MODE_CPM":
				return "CPM";
			case "BID_MODE_CPA":
				return "CPA";

			default:
				return null;
		}
	}


}
