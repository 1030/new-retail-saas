package com.pig4cloud.pig.ads.gdt.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.dto.GdtAdDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAd;

/**
 * 广告
 *
 * @author hma
 */
public interface IGdtAdService extends IService<GdtAd> {

	/**
	 * 创建广告
	 *
	 * @param gdtAdDto
	 * @return
	 */
	GdtAdDto createGdtAd(GdtAdDto gdtAdDto);

	/**
	 * 拉取并存储广告数据
	 *
	 * @param gdtAdDto
	 */
	void getGdtAdData(GdtAdDto gdtAdDto);

}
