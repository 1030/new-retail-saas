package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdAssets;
import com.pig4cloud.pig.api.vo.AdAssetsReq;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 广告资产表
 * @author  chenxiang
 * @version  2022-04-01 15:02:28
 * table: ad_assets
 */
public interface AdAssetsService extends IService<AdAssets> {
	/**
	 * 分页列表
	 * @param req
	 * @return
	 */
	R getPage(AdAssetsReq req);
	/**
	 *
	 * @param req
	 * @return
	 */
	R addAssets(AdAssetsReq req);
}


