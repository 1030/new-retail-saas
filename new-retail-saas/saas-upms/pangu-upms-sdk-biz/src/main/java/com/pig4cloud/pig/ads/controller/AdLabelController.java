/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.ads.service.AdLabelService;
import com.pig4cloud.pig.api.vo.AdLabelVo;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * 素材标签
 *
 * @author cx
 * @date 2021-06-29 14:27:26
 */
@RestController
@AllArgsConstructor
@RequestMapping("/label" )
@Api(value = "label", tags = "素材标签管理")
public class AdLabelController {

    private final  AdLabelService adLabelService;

	/**
	 * 标签列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/getList")
	public R getList(@RequestBody AdLabelVo req){
		return adLabelService.getList(req);
	}
	/**
	 * 添加
	 * @param req
	 * @return
	 */
	@SysLog("添加标签关联")
	@RequestMapping("/addLabelRelate")
	public R addLabelRelate(@Valid @RequestBody AdLabelVo req){
		return adLabelService.addLabelRelate(req);
	}

}
