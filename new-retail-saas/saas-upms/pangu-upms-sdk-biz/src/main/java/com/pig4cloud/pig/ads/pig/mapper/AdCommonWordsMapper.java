package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdCommonWordsDto;
import com.pig4cloud.pig.api.entity.AdCommonWordsEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdCommonWordsMapper extends BaseMapper<AdCommonWordsEntity> {

	/**
	 * 分页列表
	 * @return
	 */
	IPage<AdCommonWordsEntity> selectPageList(AdCommonWordsDto dto);

}
