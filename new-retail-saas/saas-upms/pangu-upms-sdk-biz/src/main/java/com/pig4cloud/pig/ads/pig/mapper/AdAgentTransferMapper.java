package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdAgentTransferDto;
import com.pig4cloud.pig.api.entity.AdAgentTransfer;
import com.pig4cloud.pig.api.vo.AdAgentTransferVo;

public interface AdAgentTransferMapper extends BaseMapper<AdAgentTransfer> {

	IPage<AdAgentTransferVo> queryList(AdAgentTransferDto req);

	AdAgentTransferVo queryPtypeById(AdAgentTransferDto req);
}
