package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdCommonWordsDto;
import com.pig4cloud.pig.api.entity.AdCommonWordsEntity;
import com.pig4cloud.pig.api.vo.AdCommonWordsVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface AdCommonWordsService extends IService<AdCommonWordsEntity> {

	/**
	 * 常用语列表
	 *
	 * @param words
	 * @return
	 */
	List<AdCommonWordsEntity> getWordsList(AdCommonWordsVO words);

	/**
	 * 常用语分页列表
	 *
	 * @param words
	 * @return
	 */
	Page<AdCommonWordsEntity> getWordsPage(AdCommonWordsVO words);

	/**
	 * 添加常用语信息
	 *
	 * @param words
	 */
	void addWords(AdCommonWordsVO words);

	/**
	 * 编辑常用语
	 *
	 * @param words
	 */
	void editWords(AdCommonWordsVO words);


	/**
	 * 新增
	 *
	 * @param
	 * @return
	 */
	@Deprecated
	R insert(AdCommonWordsDto dto);

	/**
	 * 更新
	 *
	 * @return
	 */
	@Deprecated
	R update(AdCommonWordsDto dto);

	/**
	 * 分页列表
	 *
	 * @return
	 */
	@Deprecated
	IPage<AdCommonWordsEntity> selectPageList(AdCommonWordsDto dto);

	/**
	 * 根据id删除
	 *
	 * @param id
	 * @return
	 */
	@Deprecated
	R delete(Long id);
}
