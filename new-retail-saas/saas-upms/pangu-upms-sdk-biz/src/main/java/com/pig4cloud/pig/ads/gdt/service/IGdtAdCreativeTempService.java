package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.dto.GdtAdCreativeDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAdCreativeTemp;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 广点通广告创意临时 服务层
 * 
 * @author hma
 * @date 2020-12-11
 */
public interface IGdtAdCreativeTempService  extends IService<GdtAdCreativeTemp>
{

	/**
	 * 查询创意模板
	 * @param gdtAdCreativeDto
	 * @return
	 */
	 R getCreativeTemp(GdtAdCreativeDto gdtAdCreativeDto);
	
}
