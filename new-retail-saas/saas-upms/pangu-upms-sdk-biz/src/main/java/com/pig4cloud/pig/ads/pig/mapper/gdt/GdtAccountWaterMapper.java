package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdAccountWaterDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAccountWater;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: nml
 * @time: 2021/7/16 10:05
 **/
@Component
public interface GdtAccountWaterMapper extends BaseMapper<GdtAccountWater> {

	IPage<GdtAccountWater> selectPageByParams(AdAccountWaterDto req);

}
