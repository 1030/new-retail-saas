package com.pig4cloud.pig.ads.controller.tag;

import com.pig4cloud.pig.ads.service.tag.AdTagRelateService;
import com.pig4cloud.pig.ads.service.tag.AdTagService;
import com.pig4cloud.pig.api.entity.tag.AdTagDto;
import com.pig4cloud.pig.api.entity.tag.BatchSettingTagDto;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标签表服务控制器
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/tag")
public class AdTagController {

    private final AdTagService adTgService;
	private final AdTagRelateService adTagRelateService;
	/**
	 * 目标账户下拉框 主要用于转账  此管家账户下的广告账户
	 */

	@PostMapping("/tagList")
	public R tagList(@RequestBody AdTagDto adTagDto) {
		return adTgService.tagList(adTagDto);
	}

	/**
	 * 目标账户下拉框 主要用于转账  此管家账户下的广告账户
	 */
	@PostMapping("/page")
	public R pageList(@RequestBody AdTagDto adTagDto) {
		return adTgService.pageList(adTagDto);
	}

	/**
	 * 目标账户下拉框 主要用于转账  此管家账户下的广告账户
	 */
	@PreAuthorize("@pms.hasPermission('tag_add')")
	@PostMapping("/save")
	public R add(@RequestBody AdTagDto adTagDto) {
		return adTgService.add(adTagDto);
	}

	/**
	 * 目标账户下拉框 主要用于转账  此管家账户下的广告账户
	 */
	@PreAuthorize("@pms.hasPermission('tag_edit')")
	@PostMapping("/edit")
	public R edit(@RequestBody AdTagDto adTagDto) {
		return adTgService.edit(adTagDto);
	}

	/**
	 * 目标账户下拉框 主要用于转账  此管家账户下的广告账户
	 */
	@PreAuthorize("@pms.hasPermission('tag_del')")
	@PostMapping("/del")
	public R del(@RequestBody AdTagDto adTagDto) {
		return adTgService.del(adTagDto);
	}

	@PreAuthorize("@pms.hasPermission('tag_edit*')")
	@PostMapping("/batchSettingTag")
	public R batchSettingTag(@RequestBody BatchSettingTagDto req) {
		return adTagRelateService.batchSettingTag(req);
	}
}