package com.pig4cloud.pig.ads.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pig4cloud.pig.ads.pig.mapper.AdvertiserMapper;
import com.pig4cloud.pig.ads.service.CustomAudienceService;
import com.pig4cloud.pig.ads.service.TtAccesstokenService;
import com.pig4cloud.pig.ads.utils.OEHttpUtils;
import com.pig4cloud.pig.api.dto.CustomAudienceDto;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class CustomAudienceServiceImpl implements CustomAudienceService {

	private final TtAccesstokenService ttAccesstokenService;

	@Autowired
	private AdvertiserMapper advertiserMapper;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Override
	public R customAudienceList(CustomAudienceDto dto) {

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", dto.getAdvertiserId());
		data.put("select_type", 1);
		data.put("offset", 0);
		data.put("limit", 100);

		String token = ttAccesstokenService.fetchAccesstoken(dto.getAdvertiserId());   //this.getAccesstoken(entity);
		//String accessToken = "3e09cc34a5812083ad9bd2f9eca6659fe2aadaa6";

		//调用头条接口
		String resultStr = OEHttpUtils.doGet(DmpCustomAudienceUrlTt + "select/", data, token);

		//结果为null，当做失败
		if (StringUtils.isBlank(resultStr))
			return R.failed("从toutiao获取失败:resultStr is blank");

		JSONObject obj = JSON.parseObject(resultStr);
		if (!obj.getInteger("code").equals(0)){	//同步失败
			return R.failed("从toutiao获取失败:" + obj.getString("message"));
		}else{

			JSONArray arr = obj.getJSONObject("data").getJSONArray("custom_audience_list");
			return R.ok(arr, "在toutiao绑定成功");
		}




/*		R getResult =  this.getDataFromToutiao(dto);
		if (getResult.getCode() != CommonConstants.SUCCESS)
			return getResult;

		String resultStr = (String)getResult.getData();
		JSONObject obj = JSON.parseObject(resultStr);



		List<Map> mapArr = JSON.parseArray(arrStr, Map.class);

		List<Map<String,Object>> resultList = new ArrayList<>();
		for (Map<String, Object> map :mapArr){
			Map<String, Object> item = new HashMap<>();
			item.put("custom_audience_id", map.get("custom_audience_id"));
			item.put("name", map.get("name"));
			resultList.add(item);
		}

		return R.ok(resultList);*/
	}

	@Value("${dmp_custom_audience_url_tt}")
	private String DmpCustomAudienceUrlTt;

/*	private R getDataFromToutiao(CustomAudienceDto entity){
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", entity.getAdvertiserId());
		data.put("select_type", 1);
		data.put("offset", 0);
		data.put("limit", 100);

		String token = ttAccesstokenService.fetchAccesstoken(entity.getAdvertiserId());   //this.getAccesstoken(entity);
		//String accessToken = "3e09cc34a5812083ad9bd2f9eca6659fe2aadaa6";

		//调用头条接口
		String resultStr = OEHttpUtils.doGet(DmpCustomAudienceUrlTt + "select/", data, token);

		//结果为null，当做失败
		if (StringUtils.isBlank(resultStr))
			return R.failed("从toutiao获取失败 ");

		JSONObject obj = JSON.parseObject(resultStr);
		if (!obj.getInteger("code").equals(0)){	//同步失败
			return R.failed("从toutiao获取失败");
		}else{
			return R.ok(resultStr, "在toutiao绑定成功");
		}
	}*/


	/**
	 * 根据广告账号查询对应授权token
	 * @param entity
	 * @return
	 */
	/*private String getAccesstoken(CustomAudienceDto entity){
		AdvertiserInfo advertiserInfo=  advertiserMapper.selectAdvertiserByAdAccount(entity.getAdvertiserId());
		String housekeeper = advertiserInfo.getHousekeeper();

		//根据housekeeper获取accessToken
		String tokenContent = stringRedisTemplate.opsForValue().get(Constants.OCEANENGINE_REDIS_TOKEN_KEY_PRIX_ + housekeeper);
		Accesstoken accessToken = JSON.parseObject(tokenContent, Accesstoken.class);
		String token = accessToken.getAccessToken();

		token = "1d4291e0b80d7d2dc716c6dfb810ef4da0651457";
		return token;
	}*/


}
