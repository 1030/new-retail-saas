package com.pig4cloud.pig.ads.gdt.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.gdt.service.GdtXlpService;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtXlpMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtXlp;
import org.springframework.stereotype.Service;


/**
 * @落地页 服务实现类
 * @author yk
 */
@Service
public class GdtXlpServiceImpl extends ServiceImpl<GdtXlpMapper, GdtXlp> implements GdtXlpService {



}
