package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdAccesstoken;
import com.pig4cloud.pig.api.entity.AdAccountToken;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 媒体授权TOKEN表
 * @author  chengang
 * @version  2022-12-19 15:51:30
 * table: ad_accesstoken
 */
@Mapper
public interface AdAccesstokenMapper extends BaseMapper<AdAccesstoken> {

	List<AdAccountToken> getAdAccountToken(@Param("platformId") String platformId, @Param("advertiserId") String advertiserId);

}


