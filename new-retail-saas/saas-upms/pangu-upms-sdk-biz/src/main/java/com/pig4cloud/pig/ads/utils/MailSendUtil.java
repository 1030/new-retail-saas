package com.pig4cloud.pig.ads.utils;

import com.sjda.framework.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

/**
 * @Description 发送邮件服务
 * @Author chengang
 * @Date 2021/7/13
 */
@Component
public class MailSendUtil {

	private static Logger logger = LoggerFactory.getLogger(MailSendUtil.class);

	@Autowired
	private TaskExecutor taskExecutor;

	@Value("${email.host}")
	private String emailHost;

	// 发件邮箱
	@Value("${email.username}")
	private String emailUsername;

	@Value("${email.password}")
	private String emailPassword;

	@Value("${email.port}")
	private String emailPort;

	// 发件人名 -- 默认 3367游戏
	@Value("${email.fromUsername}")
	private String emailFromUsername;



	public void sendMail(String mailSubject, String emailContent, String sendTo) {
		try {
			if (StringUtils.isNotBlank(sendTo)) {
				MailSender mail = new MailSender();
				// 发送的邮箱地址
				mail.setAddress(sendTo, MailSender.TO);
				mail.setFromUsername(emailFromUsername);
				mail.setFromAddress(emailUsername);
				mail.setSMTPHost(emailHost, emailPort, emailUsername, emailPassword);
				mail.setSubject(mailSubject);
				mail.setSsl(true);
				String content = "<!DOCTYPE html>" + "<html>" + "<head>" + "<meta charset='UTF-8'>" + "<title>"+mailSubject+"</title>"
						+ "</head>" + "<body>" + emailContent + "</body>" + "</html>";
				mail.setHtmlBody(content);
				SendMailRunner sendMailRunner = new SendMailRunner(mail);
				taskExecutor.execute(sendMailRunner);
			}

		} catch (Throwable t) {
			logger.error("支付失败发送邮箱异常", t);
		}
	}
	
	


}
