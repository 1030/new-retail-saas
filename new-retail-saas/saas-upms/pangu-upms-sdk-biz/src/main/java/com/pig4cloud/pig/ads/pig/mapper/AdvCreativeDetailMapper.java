package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.TtCreativeDetail;

public interface AdvCreativeDetailMapper extends BaseMapper<TtCreativeDetail> {
}
