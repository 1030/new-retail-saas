package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdProject;
import org.apache.ibatis.annotations.Mapper;

/**
 * 项目表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:20:40
 * table: ad_project
 */
@Mapper
public interface AdProjectMapper extends BaseMapper<AdProject> {
}


