package com.pig4cloud.pig.ads.gdt.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.gdt.service.IGdtCreativeTemplatesService;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtCreativeTemplatesMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtCreativeTemplates;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 创意形式 服务层实现
 * 
 * @author hma
 * @date 2020-12-16
 */
@Service
public class GdtCreativeTemplatesServiceImpl  extends ServiceImpl<GdtCreativeTemplatesMapper, GdtCreativeTemplates> implements IGdtCreativeTemplatesService
{

	/**
	 * 查询创意形式（内容）列表
	 * @param gdtCreativeTemplates
	 * @return
     */
	@Override
	public List<GdtCreativeTemplates> selectGdtCreativeTemplates(GdtCreativeTemplates gdtCreativeTemplates){
		return baseMapper.selectGdtCreativeTemplatesList(gdtCreativeTemplates);
	}



}
