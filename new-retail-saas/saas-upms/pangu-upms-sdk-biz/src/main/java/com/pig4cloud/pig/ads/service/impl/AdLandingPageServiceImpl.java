package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdLandingPageMapper;
import com.pig4cloud.pig.ads.service.AdLandingPageService;
import com.pig4cloud.pig.api.entity.AdLandingPage;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

/**
 * 落地页表
 *
 * @author chenxiang
 * @version 2022-03-18 10:59:34
 * table: ad_landing_page
 */
@Log4j2
@Service("adLandingPageService")
@RequiredArgsConstructor
public class AdLandingPageServiceImpl extends ServiceImpl<AdLandingPageMapper, AdLandingPage> implements AdLandingPageService {
	/**
	 * 分页列表
	 *
	 * @param page
	 * @param wrapper
	 * @return
	 */
	public IPage<AdLandingPage> getLandingIPage(Page page, QueryWrapper<AdLandingPage> wrapper) {
		IPage<AdLandingPage> iPage = this.page(page, wrapper);
		return iPage;
	}

	/**
	 * 根据素材ID列表查询素材信息
	 *
	 * @param landingPageIds
	 * @return
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	@Override
	public List<AdLandingPage> getLandingPageListByIds(List<String> landingPageIds) {
		if (CollectionUtils.isEmpty(landingPageIds)) {
			return Collections.emptyList();
		}
		return this.list(Wrappers.<AdLandingPage>lambdaQuery().in(AdLandingPage::getPageId, landingPageIds));
	}

}


