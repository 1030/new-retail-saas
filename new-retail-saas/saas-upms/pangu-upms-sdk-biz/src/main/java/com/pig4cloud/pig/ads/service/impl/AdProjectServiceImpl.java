package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdProjectMapper;
import com.pig4cloud.pig.ads.service.AdProjectService;
import com.pig4cloud.pig.api.entity.AdProject;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 项目表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:20:40
 * table: ad_project
 */
@Log4j2
@Service("adProjectService")
@RequiredArgsConstructor
public class AdProjectServiceImpl extends ServiceImpl<AdProjectMapper, AdProject> implements AdProjectService {
}


