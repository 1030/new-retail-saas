package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.GdtChannelPackage;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName GdtChannelPackageMapper.java
 * @createTime 2021年07月12日 20:32:00
 */
public interface GdtChannelPackageMapper extends BaseMapper<GdtChannelPackage> {
}
