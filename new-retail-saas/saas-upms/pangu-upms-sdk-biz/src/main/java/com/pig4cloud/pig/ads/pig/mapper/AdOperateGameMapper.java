package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdOperateGameDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.pig4cloud.pigx.mapper.AdOperateFileDO
 */
@Mapper
public interface AdOperateGameMapper extends BaseMapper<AdOperateGameDO> {

}




