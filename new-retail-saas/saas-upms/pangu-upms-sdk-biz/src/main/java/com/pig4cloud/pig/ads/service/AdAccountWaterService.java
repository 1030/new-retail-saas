package com.pig4cloud.pig.ads.service;

import com.pig4cloud.pig.api.dto.AdAccountWaterDto;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @description:
 * @author: nml
 * @time: 2021/7/14 17:42
 **/
public interface AdAccountWaterService {

	R queryAdAccountWaterPage(AdAccountWaterDto req);

}
