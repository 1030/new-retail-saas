package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.dto.AdTtTransferDto;
import com.pig4cloud.pig.api.entity.AdTtTransfer;
import com.pig4cloud.pig.api.vo.AdTtTransferVo;
import com.pig4cloud.pig.api.vo.AdTtadvertiserVO;
import org.apache.ibatis.annotations.Param;

public interface AdTtTransferMapper extends BaseMapper<AdTtTransfer> {
	AdTtTransferVo queryAgentList(AdTtTransferDto ato);

	AdTtadvertiserVO getTtBalance(@Param("advertiserId") String advertiserId);
}
