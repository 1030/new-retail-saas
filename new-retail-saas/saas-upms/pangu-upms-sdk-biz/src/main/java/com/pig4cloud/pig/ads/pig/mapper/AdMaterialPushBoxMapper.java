package com.pig4cloud.pig.ads.pig.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdMaterialPushBox;
import com.pig4cloud.pig.api.vo.AdMaterialPushBoxVO;

@Mapper
public interface AdMaterialPushBoxMapper extends BaseMapper<AdMaterialPushBox>{

	/**
	 * 查询推送箱
	 * @param loginUserId
	 * @return
	 */
	List<AdMaterialPushBoxVO> getMaterialPushBoxInfo(@Param(value = "loginUserId")String loginUserId);

	/**
	 * 删除单条预推送
	 * @param adMaterialPushBox
	 */
	void deleteAdMaterialPushBox(AdMaterialPushBox adMaterialPushBox);

	/**
	 * 更新推送结果
	 * @param adMaterialPushBox
	 * @return
	 */
	int updatePushStatus(AdMaterialPushBox adMaterialPushBox);
	
	/**
	 * 查询所有待推送素材
	 * @param loginUserId
	 * @return
	 */
	List<AdMaterialPushBox> getPrepareMaterialPushBox(@Param(value = "loginUserId")String loginUserId);

}
