package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdLandingPage;

import java.util.List;

/**
 * 落地页表
 *
 * @author chenxiang
 * @version 2022-03-18 10:59:34
 * table: ad_landing_page
 */
public interface AdLandingPageService extends IService<AdLandingPage> {
	/**
	 * 分页列表
	 *
	 * @param page
	 * @param wrapper
	 * @return
	 */
	IPage<AdLandingPage> getLandingIPage(Page page, QueryWrapper<AdLandingPage> wrapper);

	/**
	 * 根据素材ID列表查询素材信息
	 *
	 * @param landingPageIds
	 * @return
	 */
	List<AdLandingPage> getLandingPageListByIds(List<String> landingPageIds);

}


