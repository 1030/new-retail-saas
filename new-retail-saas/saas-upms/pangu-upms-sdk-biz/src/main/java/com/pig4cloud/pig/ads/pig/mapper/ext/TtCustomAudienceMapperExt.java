package com.pig4cloud.pig.ads.pig.mapper.ext;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.ads.pig.mapper.TtCustomAudienceMapper;
import com.pig4cloud.pig.api.dto.AllCustomAudienceDTO;
import com.pig4cloud.pig.api.entity.AllCustomAudience;
import com.pig4cloud.pig.api.entity.TtCustomAudience;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TtCustomAudienceMapperExt extends TtCustomAudienceMapper {

	IPage<AllCustomAudience> getAudienceList(IPage<AllCustomAudience> pge, @Param("audienceDTO") AllCustomAudienceDTO audienceDTO);

	List<String> selectAdIdsByTt(@Param("audienceId") Long audienceId, @Param("advertiserIds") List<String> advertiserIds);

	List<String> selectAdIdsByGdt(@Param("audienceId") Long audienceId, @Param("advertiserIds") List<String> advertiserIds);
	@Select("select custom_audience_id customAudienceId , name from tt_custom_audience where isdel = 0 and media_type = 1 ")
	List<TtCustomAudience> getAllThirdIdAndNames();
}
