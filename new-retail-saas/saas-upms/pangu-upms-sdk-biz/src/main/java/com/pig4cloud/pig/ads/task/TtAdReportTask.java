package com.pig4cloud.pig.ads.task;

import com.alibaba.fastjson.JSON;
import com.pig4cloud.pig.ads.service.TtAdReportService;
import com.pig4cloud.pig.api.util.StringUtils;
import com.pig4cloud.pig.api.vo.TtAdReportVO;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.ads.task
 * @Author 马嘉祺
 * @Date 2021/9/6 16:30
 * @Description
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class TtAdReportTask {

	private final TtAdReportService ttAdReportService;

	/**
	 * 同步头条创意日报表数据
	 *
	 * @return
	 */
	@XxlJob("syncCreativeDayList")
	public ReturnT<String> syncCreativeDayList(String param) {
		log.info("-----------------同步头条创意日报表数据开始---------------------");
		XxlJobLogger.log("-----------------同步头条创意日报表数据开始---------------------");
		try {
			TtAdReportVO report = StringUtils.isNotBlank(param) ? JSON.parseObject(param, TtAdReportVO.class) : new TtAdReportVO();
			ttAdReportService.syncCreativeDayList(report);
		} catch (Exception e) {
			log.error("-----------------同步头条创意日报表数据异常---------------------", e);
			XxlJobLogger.log("-----------------同步头条创意日报表数据异常---------------------");
		}
		XxlJobLogger.log("-----------------同步头条创意日报表数据结束---------------------");
		log.info("-----------------同步头条创意日报表数据结束---------------------");
		return ReturnT.SUCCESS;
	}

	/**
	 * 同步头条创意小时报表数据
	 *
	 * @return
	 */
	@XxlJob("syncCreativeHourList")
	public ReturnT<String> syncCreativeHourList(String param) {
		log.info("-----------------同步头条创意小时报表数据开始---------------------");
		XxlJobLogger.log("-----------------同步头条创意小时报表数据开始---------------------");
		try {
			TtAdReportVO report = StringUtils.isNotBlank(param) ? JSON.parseObject(param, TtAdReportVO.class) : new TtAdReportVO();
			ttAdReportService.syncCreativeHourList(report);
		} catch (Exception e) {
			log.error("-----------------同步头条创意小时报表数据异常---------------------", e);
			XxlJobLogger.log("-----------------同步头条创意小时报表数据异常---------------------");
		}
		XxlJobLogger.log("-----------------同步头条创意小时报表数据结束---------------------");
		log.info("-----------------同步头条创意小时报表数据结束---------------------");
		return ReturnT.SUCCESS;
	}

}
