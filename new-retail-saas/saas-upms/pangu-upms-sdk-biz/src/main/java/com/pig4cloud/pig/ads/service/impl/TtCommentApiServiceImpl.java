package com.pig4cloud.pig.ads.service.impl;

import com.alibaba.fastjson.JSON;
import com.pig4cloud.pig.ads.service.TtCommentApiService;
import com.pig4cloud.pig.ads.utils.OEHttpUtils;
import com.pig4cloud.pig.api.entity.ResponseBean;
import com.pig4cloud.pig.api.vo.TtResultVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class TtCommentApiServiceImpl implements TtCommentApiService {

	@Override
	public TtResultVo reply(Long advertiser_id, String access_token, Long comment_id, String reply_text, Long reply_id) {
		Long[] comment_ids = {comment_id};
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiser_id);//广告主ID必填
		data.put("comment_ids", comment_ids);//评论id列表
		data.put("inventory_type", "INVENTORY_AWEME_FEED");//广告位 允许值："INVENTORY_AWEME_FEED"（抖音）
		data.put("operate_type", "REPLY");//操作类型
		data.put("reply_text", reply_text);//回复内容
		if (null != reply_id) {
			data.put("operate_type", "REPLY_TO_REPLY");//二级回复
			data.put("reply_id", reply_id);//回复的id
		}
		return commentOperateTtApi(data, access_token, "广告评论回复");
	}

	@Override
	public TtResultVo hide(Long advertiser_id, String access_token, Long comment_id) {
		Long[] comment_ids = {comment_id};
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiser_id);//广告主ID必填
		data.put("comment_ids", comment_ids);//评论id列表
		data.put("inventory_type", "INVENTORY_AWEME_FEED");//广告位 允许值："INVENTORY_AWEME_FEED"（抖音）
		data.put("operate_type", "HIDE");//操作类型
		return commentOperateTtApi(data, access_token, "广告评论隐藏");
	}

	@Override
	public TtResultVo stickOnTop(Long advertiser_id, String access_token, Long comment_id) {
		Long[] comment_ids = {comment_id};
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiser_id);//广告主ID必填
		data.put("comment_ids", comment_ids);//评论id列表
		data.put("inventory_type", "INVENTORY_AWEME_FEED");//广告位 允许值："INVENTORY_AWEME_FEED"（抖音）
		data.put("operate_type", "STICK_ON_TOP");//操作类型
		return commentOperateTtApi(data, access_token, "广告评论置顶");
	}

	@Override
	public TtResultVo blockUsers(Long advertiser_id, String access_token, Long comment_id) {
		Long[] comment_ids = {comment_id};
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiser_id);//广告主ID必填
		data.put("comment_ids", comment_ids);//评论id列表
		data.put("inventory_type", "INVENTORY_AWEME_FEED");//广告位 允许值："INVENTORY_AWEME_FEED"（抖音）
		data.put("operate_type", "BLOCK_USERS");//操作类型
		return commentOperateTtApi(data, access_token, "屏蔽用户");
	}

	/**
	 * 头条评论操作API
	 */
	private TtResultVo commentOperateTtApi(Map<String, Object> data, String access_token, String logstr) {
		TtResultVo resultVo = new TtResultVo();
		String url = "https://ad.oceanengine.com/open_api/2/tools/comment/operate/";
		String resultStr = OEHttpUtils.doPost(url, data, access_token);
		log.info("{}resultStr=={}, advertiser_id={}", logstr, resultStr, data.get("advertiser_id"));
		ResponseBean res = Objects.requireNonNull(JSON.parseObject(resultStr, ResponseBean.class), "媒体接口调用失败: NULL");
		if ("0".equals(res.getCode())) { // 调用成功
			resultVo.setSuccess(true);
			resultVo.setData(res.getData());
			return resultVo;
		} else { // 调用失败
			resultVo.setSuccess(false);
			resultVo.setMessage(String.format("媒体接口调用失败: %s - %s", res.getCode(), res.getMessage()));
		}
		return resultVo;
	}

	@Override
	public TtResultVo termsBannedAdd(Long advertiser_id, String access_token, List<String> terms) {
		TtResultVo resultVo = new TtResultVo();
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiser_id);//广告主ID必填
		data.put("terms", terms);//待添加的屏蔽词列表
		String url = "https://ad.oceanengine.com/open_api/2/tools/comment/terms_banned/add/";
		String resultStr = OEHttpUtils.doPost(url, data, access_token);
		log.info("屏蔽词添加resultStr=={}, advertiser_id={}", resultStr, data.get("advertiser_id"));
		ResponseBean res = Objects.requireNonNull(JSON.parseObject(resultStr, ResponseBean.class), "媒体接口调用失败: NULL");
		if ("0".equals(res.getCode())) { // 调用成功
			resultVo.setSuccess(true);
			resultVo.setData(res.getData());
			return resultVo;
		} else { // 调用失败
			resultVo.setSuccess(false);
			resultVo.setMessage(String.format("媒体接口调用失败: %s - %s", res.getCode(), res.getMessage()));
		}
		return resultVo;
	}

	@Override
	public TtResultVo termsBannedDelete(Long advertiser_id, String access_token, List<String> terms) {
		TtResultVo resultVo = new TtResultVo();
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiser_id);//广告主ID必填
		data.put("terms", terms);//待删除的屏蔽词列表
		String url = "https://ad.oceanengine.com/open_api/2/tools/comment/terms_banned/delete/";
		String resultStr = OEHttpUtils.doPost(url, data, access_token);
		log.info("屏蔽词删除resultStr=={}, advertiser_id={}", resultStr, data.get("advertiser_id"));
		ResponseBean res = Objects.requireNonNull(JSON.parseObject(resultStr, ResponseBean.class), "媒体接口调用失败: NULL");
		if ("0".equals(res.getCode())) { // 调用成功
			resultVo.setSuccess(true);
			resultVo.setData(res.getData());
			return resultVo;
		} else { // 调用失败
			resultVo.setSuccess(false);
			resultVo.setMessage(String.format("媒体接口调用失败: %s - %s", res.getCode(), res.getMessage()));
		}
		return resultVo;
	}

	@Override
	public TtResultVo termsBannedUpdate(Long advertiser_id, String access_token, String origin_terms, String new_terms) {
		TtResultVo resultVo = new TtResultVo();
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiser_id);//广告主ID必填
		data.put("origin_terms", origin_terms);//待更新的屏蔽词
		data.put("new_terms", new_terms);//更新后的屏蔽词
		String url = "https://ad.oceanengine.com/open_api/2/tools/comment/terms_banned/update/";
		String resultStr = OEHttpUtils.doPost(url, data, access_token);
		log.info("屏蔽词删除resultStr=={}, advertiser_id={}", resultStr, data.get("advertiser_id"));
		ResponseBean res = Objects.requireNonNull(JSON.parseObject(resultStr, ResponseBean.class), "媒体接口调用失败: NULL");
		if ("0".equals(res.getCode())) { // 调用成功
			resultVo.setSuccess(true);
			resultVo.setData(res.getData());
			return resultVo;
		} else { // 调用失败
			resultVo.setSuccess(false);
			resultVo.setMessage(String.format("媒体接口调用失败: %s - %s", res.getCode(), res.getMessage()));
		}
		return resultVo;
	}

	@Override
	public TtResultVo awemeBannedCreate(Long advertiser_id, String access_token, String banned_type, List<String> aweme_ids, List<String> nickname_keywords) {
		TtResultVo resultVo = new TtResultVo();
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiser_id);//广告主ID必填
		data.put("banned_type", banned_type);//屏蔽类型 CUSTOM_TYPE：自定义规则，根据昵称关键词屏蔽；AWEME_TYPE：根据抖音id屏蔽
		if ("AWEME_TYPE".equals(banned_type)) {//屏蔽类型为 AWEME_TYPE：根据抖音id屏蔽
			data.put("aweme_ids", aweme_ids);
		} else {//自定义规则，根据昵称关键词屏蔽
			data.put("nickname_keywords", nickname_keywords);
		}
		String url = "https://ad.oceanengine.com/open_api/2/tools/aweme_banned/create/";
		String resultStr = OEHttpUtils.doPost(url, data, access_token);
		log.info("添加屏蔽用户resultStr=={}, advertiser_id={}", resultStr, data.get("advertiser_id"));
		ResponseBean res = Objects.requireNonNull(JSON.parseObject(resultStr, ResponseBean.class), "媒体接口调用失败: NULL");
		if ("0".equals(res.getCode())) { // 调用成功
			resultVo.setSuccess(true);
			resultVo.setData(res.getData());
			return resultVo;
		} else { // 调用失败
			resultVo.setSuccess(false);
			resultVo.setMessage(String.format("媒体接口调用失败: %s - %s", res.getCode(), res.getMessage()));
		}
		return resultVo;
	}

	@Override
	public TtResultVo awemeBannedDelete(Long advertiser_id, String access_token, String banned_type, String aweme_ids, String nickname_keywords) {
		TtResultVo resultVo = new TtResultVo();
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiser_id);//广告主ID必填
		data.put("banned_type", banned_type);//屏蔽类型 CUSTOM_TYPE：自定义规则，根据昵称关键词屏蔽；AWEME_TYPE：根据抖音id屏蔽
		if ("AWEME_TYPE".equals(banned_type)) {//屏蔽类型为 AWEME_TYPE：根据抖音id屏蔽
			data.put("aweme_ids", splitStrForSpace(aweme_ids));
		} else {//自定义规则，根据昵称关键词屏蔽
			data.put("nickname_keywords", splitStrForSpace(nickname_keywords));
		}
		String url = "https://ad.oceanengine.com/open_api/2/tools/aweme_banned/delete/";
		String resultStr = OEHttpUtils.doPost(url, data, access_token);
		log.info("删除屏蔽用户resultStr=={}, advertiser_id={}", resultStr, data.get("advertiser_id"));
		ResponseBean res = Objects.requireNonNull(JSON.parseObject(resultStr, ResponseBean.class), "媒体接口调用失败: NULL");
		if ("0".equals(res.getCode())) { // 调用成功
			resultVo.setSuccess(true);
			resultVo.setData(res.getData());
			return resultVo;
		} else { // 调用失败
			resultVo.setSuccess(false);
			resultVo.setMessage(String.format("媒体接口调用失败: %s - %s", res.getCode(), res.getMessage()));
		}
		return resultVo;
	}

	/**
	 * 拆分字符串数组
	 *
	 * @param str
	 * @return
	 */
	private String[] splitStrForSpace(String str) {
		String[] str_arr = {};
		if (str.contains(" ")) {
			str_arr = str.split(" ");
		} else {
			str_arr = new String[]{str};
		}
		return str_arr;
	}
}
