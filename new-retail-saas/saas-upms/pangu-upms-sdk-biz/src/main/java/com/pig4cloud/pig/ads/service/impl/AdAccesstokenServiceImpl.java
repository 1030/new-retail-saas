package com.pig4cloud.pig.ads.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.api.client.util.Lists;
import com.pig4cloud.pig.ads.pig.mapper.AdAccesstokenMapper;
import com.pig4cloud.pig.ads.service.AdAccesstokenService;
import com.pig4cloud.pig.api.entity.AdAccesstoken;
import com.pig4cloud.pig.api.entity.AdAccountToken;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 媒体授权TOKEN表
 * @author  chengang
 * @version  2022-12-19 15:51:30
 * table: ad_accesstoken
 */
@Log4j2
@Service("adAccesstokenService")
@RequiredArgsConstructor
public class AdAccesstokenServiceImpl extends ServiceImpl<AdAccesstokenMapper, AdAccesstoken> implements AdAccesstokenService {

	private final AdAccesstokenMapper adAccesstokenMapper;

	@Override
	public AdAccountToken getAdAccountToken(String platformId, String advertiserId){
		List<AdAccountToken> adAccountTokenList = adAccesstokenMapper.getAdAccountToken(platformId, advertiserId);
		if (CollectionUtils.isNotEmpty(adAccountTokenList)){
			return adAccountTokenList.get(0);
		}
		return null;
	}
	@Override
	public List<AdAccountToken> getAdAccountToken(String platformId){
		List<AdAccountToken> adAccountTokenList = adAccesstokenMapper.getAdAccountToken(platformId, null);
		if (CollectionUtils.isNotEmpty(adAccountTokenList)){
			return adAccountTokenList;
		}
		return Lists.newArrayList();
	}
}


