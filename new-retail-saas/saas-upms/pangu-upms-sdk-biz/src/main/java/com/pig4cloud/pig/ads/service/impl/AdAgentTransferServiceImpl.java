package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.feign.RemoteChannelManageService;
import com.pig4cloud.pig.ads.pig.mapper.AdAgentTransferMapper;
import com.pig4cloud.pig.ads.service.AdAgentTransferService;
import com.pig4cloud.pig.api.dto.AdAgentTransferDto;
import com.pig4cloud.pig.api.entity.AdAgentTransfer;
import com.pig4cloud.pig.api.vo.AdAgentTransferVo;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author ：lile
 * @date ：2021/7/13 17:37
 * @description：
 * @modified By：
 */
@Service
@RequiredArgsConstructor
public class AdAgentTransferServiceImpl extends ServiceImpl<AdAgentTransferMapper, AdAgentTransfer> implements AdAgentTransferService {

	@Autowired
	private AdAgentTransferMapper adAgentTransferMapper;

	@Autowired
	private RemoteChannelManageService remoteChannelManageService;

	@Override
	public IPage<AdAgentTransferVo> queryList(AdAgentTransferDto req) {
		Page pge = new Page();
		pge.setSize(req.getSize());
		pge.setCurrent(req.getCurrent());
		IPage<AdAgentTransferVo> page = adAgentTransferMapper.queryList(req);
		List<AdAgentTransferVo> list = page.getRecords();
		Map<String, String> platFormMap = this.platFormMap();
		if (CollectionUtils.isNotEmpty(list)) {
			list.forEach(data -> {
				if (Objects.nonNull(data.getPlatform())) {
					data.setPlatformName(platFormMap.get(String.valueOf(data.getPlatform())));
				}
			});
		}
		return page;
	}

	/**
	 * 确保编辑唯一
	 *
	 * @param req
	 * @return
	 */
	@Override
	public AdAgentTransferVo queryPtypeByid(AdAgentTransferDto req) {
		return adAgentTransferMapper.queryPtypeById(req);
	}

	/**
	 * 主渠道类型名称
	 */
	public Map<String, String> platFormMap() {
		R<List<Map<String, String>>> list = remoteChannelManageService.queryPlatForm();
		List<Map<String, String>> platFormlist = list.getData();
		Map<String, String> platFormMap = new HashMap<>();
		platFormlist.forEach(manageData -> {
			platFormMap.put(manageData.get("code"), manageData.get("dictValue"));
		});
		return platFormMap;
	}
}
