package com.pig4cloud.pig.ads.pig.mapper.xingtu;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.xingtu.XingtuOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * 星图任务订单表
 * @author  chengang
 * @version  2021-12-13 16:48:01
 * table: xingtu_order
 */
@Mapper
public interface XingtuOrderMapper extends BaseMapper<XingtuOrder> {

}


