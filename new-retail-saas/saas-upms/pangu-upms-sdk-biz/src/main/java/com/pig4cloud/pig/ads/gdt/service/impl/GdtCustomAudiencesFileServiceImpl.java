package com.pig4cloud.pig.ads.gdt.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.gdt.service.GdtCustomAudiencesFileService;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtCustomAudiencesFileMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtCustomAudiencesFile;
import org.springframework.stereotype.Service;

/**
 * @projectName:pig
 * @description:广点通人群文件信息
 * @author:Zhihao
 * @createTime:2020/12/5 21:00
 * @version:1.0
 */
@Service
public class GdtCustomAudiencesFileServiceImpl extends ServiceImpl<GdtCustomAudiencesFileMapper, GdtCustomAudiencesFile> implements GdtCustomAudiencesFileService {
}
