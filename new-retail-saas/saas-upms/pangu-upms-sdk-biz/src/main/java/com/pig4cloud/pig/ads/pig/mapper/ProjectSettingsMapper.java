package com.pig4cloud.pig.ads.pig.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.ProjectSettingsDto;
import com.pig4cloud.pig.api.entity.ProjectSettings;
import com.pig4cloud.pig.api.vo.ProjectSettingsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface ProjectSettingsMapper extends BaseMapper<ProjectSettings> {
    int deleteByPrimaryKey(@Param("id") Integer id);

    int insert(ProjectSettings record);

    int insertSelective(ProjectSettings record);

    ProjectSettings selectByPrimaryKey(@Param("id") Integer id);

    int updateByPrimaryKeySelective(ProjectSettings record);

    int updateByPrimaryKey(ProjectSettings record);

	//查询所有，包括资源数
	IPage<ProjectSettingsDto> selectAll(ProjectSettingsVo req);
}