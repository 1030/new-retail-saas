package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdAccountWarningResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface AdAccountWarningResultMapper extends BaseMapper<AdAccountWarningResult> {

	@Update("delete from ad_account_warning_result where 1=1")
	void deleteAll();

}
