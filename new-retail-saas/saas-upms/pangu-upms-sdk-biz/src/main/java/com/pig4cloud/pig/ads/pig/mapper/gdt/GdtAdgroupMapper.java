package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.dto.GdtAdgroupDetail;
import com.pig4cloud.pig.api.gdt.dto.GdtAdgroupDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAdgroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 广点通-广告组 数据层
 *
 * @author hma
 * @date 2020-12-05
 */
@Mapper
public interface GdtAdgroupMapper  extends BaseMapper<GdtAdgroup>
{

//	/**
//     * 查询广点通-广告组信息
//     *
//     * @param adgroupId 广点通-广告组ID
//     * @return 广点通-广告组信息
//     */
//	public GdtAdgroup selectGdtAdgroupById(Long adgroupId);
//
//	/**
//     * 查询广点通-广告组列表
//     *
//     * @param gdtAdgroup 广点通-广告组信息
//     * @return 广点通-广告组集合
//     */
//	public List<GdtAdgroup> selectGdtAdgroupList(GdtAdgroup gdtAdgroup);
//
//	/**
//     * 新增广点通-广告组
//     *
//     * @param gdtAdgroup 广点通-广告组信息
//     * @return 结果
//     */
//	public int insertGdtAdgroup(GdtAdgroup gdtAdgroup);
//
//	/**
//     * 批量添加广点通-广告组
//     *
//     * @param gdtAdgroupList 需要添加的数据集合
//     * @return 结果
//     */
//	public int batchInsertGdtAdgroup(List<GdtAdgroup> gdtAdgroupList);
//
//	int batchReplaceGdtAdgroup(List<GdtAdgroup> gdtAdgroupList);
//	/**
//	 * 修改广点通-广告组
//	 * @param req 广点通-广告组信息
//	 * @return 结果
//	 */
	int update(GdtAdgroupDto req);

	GdtAdgroupDetail findGdtAdgroupDetailById(GdtAdgroup gdtAdgroup);
	List<GdtAdgroup> selectGdtAdgroupListByAdids(@Param("adids") String adids);
}
