package com.pig4cloud.pig.ads.service.impl;

import com.pig4cloud.pig.ads.service.DingTalkService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.ads.service.impl
 * @Author 马嘉祺
 * @Date 2021/8/16 17:13
 * @Description 钉钉开放平台接入
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DingTalkServiceImpl implements DingTalkService {
}
