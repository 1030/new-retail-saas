package com.pig4cloud.pig.ads.service.impl;

import java.util.List;
import java.util.Map;


import com.pig4cloud.pig.ads.clickhouse3399.mapper.DailyStatMapper;
import com.pig4cloud.pig.ads.service.DailyStatService;
import com.pig4cloud.pig.api.util.Page;
import com.pig4cloud.pig.api.util.Constants;
import com.pig4cloud.pig.api.vo.DailyStatCountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.api.entity.DailyStat;

@Service
public class DailyStatServiceImpl extends ServiceImpl<DailyStatMapper, DailyStat> implements DailyStatService {
	
	@Autowired
	private DailyStatMapper dailyStatMapper;
	
	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Override
	public Page selectByPage(Page page, Map<String, Object> params) {
		Page ipage=  dailyStatMapper.selectByPage(page, params);
		DailyStatCountVo dailyStatCountVo=new DailyStatCountVo();
		if(ipage !=null && ipage.getRecords() !=null && ipage.getRecords().size() != 0) {
			List<DailyStat> list = ipage.getRecords();
			list.forEach(item -> {
				Long advertiserId = item.getAdvertiserId();
				item.setName(stringRedisTemplate.opsForValue().get(Constants.AD_REDIS_AD_NAME_KEY_PRIX_ + advertiserId));
			});

			//添加日流水汇总数据 todo  缓存
			dailyStatCountVo=dailyStatMapper.selectDailyStatCount(params);

		}
		ipage.setRow(dailyStatCountVo);
		return ipage;
	}

}
