package com.pig4cloud.pig.ads.controller;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pig4cloud.pig.ads.service.AdMaterialPushBoxService;
import com.pig4cloud.pig.api.dto.AdMaterialPushBoxDTO;
import com.pig4cloud.pig.api.entity.AdMaterial;
import com.pig4cloud.pig.api.entity.AdMaterialPushBox;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.annotation.Inner;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


/**
 * 
 * @author chengjin
 */
@RestController
@AllArgsConstructor
@RequestMapping("/materialPushBox" )
@Api(value = "materialPushBox", tags = "素材推送箱")
@Slf4j
public class AdMaterialPushBoxController {
	
	private final  AdMaterialPushBoxService adMaterialPushBoxService;
	
	
	/**
	 * 素材推送箱详情
	 * @return
	 */
	@SysLog("素材推送箱详情")
	@RequestMapping("/info")
	public R getMaterialPushBoxInfo() {
		return adMaterialPushBoxService.getMaterialPushBoxInfo();
	}
	
	/**
	 * 素材推送箱详情
	 * @return
	 */
	@SysLog("素材推送箱-删除待推送（单条）")
	@RequestMapping("/delete")
	public R deleteAdMaterialPushBox(@RequestBody AdMaterialPushBox req) {
		try {
			Integer id = req.getId();
			if(Objects.isNull(id)){
				return R.failed("缺失参数");
			}
			adMaterialPushBoxService.deleteAdMaterialPushBox(req.getId());
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}
	
	/**
	 * 素材推送箱详情
	 * @return
	 */
	@SysLog("素材推送箱-清空待推送")
	@RequestMapping("/clear")
	public R clearAdMaterialPushBox() {
		try {
			adMaterialPushBoxService.clearAdMaterialPushBox();
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}
	
	/**
	 * 素材推送箱详情
	 * @return
	 */
	@SysLog("素材推送箱-添加待推送")
	@RequestMapping("/save")
	public R saveAdMaterialPushBox(@RequestBody AdMaterialPushBox req) {
		try {
			Long materialId = req.getMaterialId();
			if(Objects.isNull(materialId)){
				return R.failed("缺失参数");
			}
			return adMaterialPushBoxService.saveAdMaterialPushBox(materialId);
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}
	
	/**
	 * 素材推送箱详情
	 * @return
	 */
	@SysLog("素材数据报表-推送单条")
	@RequestMapping("/push")
	public R pushAdMaterialPushBox(@RequestBody AdMaterialPushBoxDTO req) {
		try {
			return adMaterialPushBoxService.pushAdMaterialPushBox(req);
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}
	
	/**
	 * 素材推送箱详情
	 * @return
	 */
	@SysLog("素材推送箱-全部推送")
	@RequestMapping("/pushAll")
	public R pushAllAdMaterialPushBox(@RequestBody AdMaterialPushBoxDTO req) {
		try {
			return adMaterialPushBoxService.pushAllAdMaterialPushBox(req);
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}
	
	/**
	 * 素材推送箱详情
	 * @return
	 */
	@SysLog("资源管理-素材管理-批量推送")
	@RequestMapping("/batchPushMaterial")
	public R batchPushMaterial(@RequestBody AdMaterialPushBoxDTO req) {
		try {
			return adMaterialPushBoxService.batchPushMaterial(req);
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}
	/**
	 * 查询推送箱-待推送数据
	 * @param loginUserId
	 * @return
	 */
	@Inner
	@PostMapping("/getPreparePushMaterial")
	public R<Set<Long>> getPreparePushMaterial(@RequestBody String loginUserId) {
		final Set<Long> preparePushMaterial = adMaterialPushBoxService.getPreparePushMaterial(loginUserId);
		return R.ok(preparePushMaterial);
	}
	
	

}
