package com.pig4cloud.pig.ads.task;

import com.pig4cloud.pig.ads.service.AdCreativeComponentService;
import com.pig4cloud.pig.common.core.util.R;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Description 获取头条广告创意基础组件定时器
 * @Author chengang
 * @Date 2021/8/25
 */
@Component
public class AdCreativeComponentTask {

	private static Logger logger = LoggerFactory.getLogger(AdCreativeComponentTask.class);

	@Resource
	private AdCreativeComponentService adCreativeComponentService;

	@XxlJob("getCreativeComponentList")
	public ReturnT<String> getCreativeComponentList(String param) {
		try {
			XxlJobLogger.log("================开始执行获取广告创意基础组件========================");
			R r = adCreativeComponentService.getCreativeComponentList(param,null);
			if (r.getCode() == 0) {
				XxlJobLogger.log("获取广告创意基础组件，已执行完成--{}", r.getMsg());
			} else {
				XxlJobLogger.log("获取广告创意基础组件，执行失败--{}",r.getMsg());
			}
			XxlJobLogger.log("================执行结束获取广告创意基础组件========================");
		} catch (Throwable e) {
			logger.error("获取广告创意基础组件ERROR",e);
			return ReturnT.FAIL;
		}
		return ReturnT.SUCCESS;
	}



}
