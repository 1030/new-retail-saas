package com.pig4cloud.pig.ads.service.bd;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.bd.BdAdgroupReq;
import com.pig4cloud.pig.api.entity.bd.BdCampaign;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface BdCampaignService extends IService<BdCampaign> {


   void saveUpdateList(List<BdCampaign> list);


	/**
	 * 广告计划修改预算
	 * @param req
	 * @return
	 */
	R updateDayBudget(BdAdgroupReq req);
}
