package com.pig4cloud.pig.ads.pig.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.dto.AdPicturePlatformDto;
import com.pig4cloud.pig.api.entity.AdPicturePlatform;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface AdPicturePlatformMapper extends BaseMapper<AdPicturePlatform> {

	//查询图片第三方平台状态信息
	List<AdPicturePlatformDto> selectPictureStatus();
}