package com.pig4cloud.pig.ads.service.ks.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.ks.KsAccountWaterMapper;
import com.pig4cloud.pig.ads.service.ks.KsAccountWaterService;
import com.pig4cloud.pig.api.entity.KsAccountWater;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 快手账号流水明细
 * @author  chenxiang
 * @version  2022-03-24 13:56:21
 * table: ks_account_water
 */
@Log4j2
@Service("ksAccountWaterService")
@RequiredArgsConstructor
public class KsAccountWaterServiceImpl extends ServiceImpl<KsAccountWaterMapper, KsAccountWater> implements KsAccountWaterService {
}


