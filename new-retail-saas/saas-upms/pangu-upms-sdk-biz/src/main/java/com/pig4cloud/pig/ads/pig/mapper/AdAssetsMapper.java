package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdAssetsRes;
import com.pig4cloud.pig.api.entity.AdAssets;
import com.pig4cloud.pig.api.vo.AdAssetsReq;
import org.apache.ibatis.annotations.Mapper;

/**
 * 广告资产表
 * @author  chenxiang
 * @version  2022-04-01 15:02:28
 * table: ad_assets
 */
@Mapper
public interface AdAssetsMapper extends BaseMapper<AdAssets> {
	/**
	 * 分页列表
	 * @param req
	 * @return
	 */
	IPage<AdAssetsRes> selectAdAssets(AdAssetsReq req);
}


