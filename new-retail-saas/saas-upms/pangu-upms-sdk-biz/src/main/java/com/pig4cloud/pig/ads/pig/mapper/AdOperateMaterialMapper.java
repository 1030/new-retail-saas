package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.api.dto.UserByMaterialResp;
import com.pig4cloud.pig.api.entity.AdOperateMaterialDO;
import com.pig4cloud.pig.api.vo.MaterialUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity AdOperateMaterialDO
 */
@Mapper
public interface AdOperateMaterialMapper extends BaseMapper<AdOperateMaterialDO> {

	/**
	 * 查询列表
	 *
	 * @param record
	 * @return
	 */
	List<AdOperateMaterialDO> list(@Param("record") AdOperateMaterialDO record);

	/**
	 * 查询分页列表
	 *
	 * @param page
	 * @param record
	 * @return
	 */
	IPage<AdOperateMaterialDO> pageList(Page<Object> page, @Param("record") AdOperateMaterialDO record);

	List<UserByMaterialResp> getUserByMaterial(MaterialUserVo record);

}