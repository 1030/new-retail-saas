

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdAccesstoken;
import com.pig4cloud.pig.api.entity.AdAccountToken;

import java.util.List;

/**
 * 媒体授权TOKEN表
 * @author  chengang
 * @version  2022-12-19 15:51:30
 * table: ad_accesstoken
 */
public interface AdAccesstokenService extends IService<AdAccesstoken> {

	AdAccountToken getAdAccountToken(String platformId, String advertiserId);

	List<AdAccountToken> getAdAccountToken(String platformId);
	
}


