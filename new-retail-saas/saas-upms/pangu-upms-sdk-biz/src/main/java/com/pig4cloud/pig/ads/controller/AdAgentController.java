/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.AdAgentService;
import com.pig4cloud.pig.ads.utils.CheckUtil;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.api.vo.AdAgentRebateVo;
import com.pig4cloud.pig.api.vo.AdAgentVo;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;


/**
 * 代理商表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
@RestController
@AllArgsConstructor
@RequestMapping("/agent" )
@Api(value = "agent", tags = "代理商表管理")
public class AdAgentController {

    private final  AdAgentService adAgentService;

	/**
	 * 分页查询
	 * @param req
	 * @return
	 */
	@SysLog("代理商管理")
	@RequestMapping("/getPage")
	public R getPage(@RequestBody AdAgentVo req){
		return adAgentService.getPage(req);
	}
	/**
	 * 代理商列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/getList")
	public R getList(AdAgentVo req){
		return adAgentService.getList(req);
	}
	/**
	 * 添加
	 * @param req
	 * @return
	 */
	@SysLog("添加代理商")
	@RequestMapping("/add")
	public R add(@RequestBody AdAgentVo req){
		if (StringUtils.isBlank(req.getAgentName())){
			return R.failed("名称不能为空");
		}
		return adAgentService.add(req);
	}
	/**
	 * 编辑
	 * @param req
	 * @return
	 */
	@SysLog("编辑代理商")
	@RequestMapping("/edit")
	public R edit(@RequestBody AdAgentVo req){
		if (StringUtils.isBlank(req.getId())){
			return R.failed("ID不能为空");
		}
		if (StringUtils.isBlank(req.getAgentName())){
			return R.failed("名称不能为空");
		}
		return adAgentService.edit(req);
	}
	/**
	 * 删除
	 * @param req
	 * @return
	 */
	@SysLog("删除代理商")
	@RequestMapping("/del")
	public R del(@RequestBody AdAgentVo req){
		if (StringUtils.isBlank(req.getId())){
			return R.failed("ID不能为空");
		}
		return adAgentService.del(req);
	}
	/**
	 * 返点列表 - 分页
	 * @param req
	 * @return
	 */
	@SysLog("返点列表")
	@RequestMapping("/getRebatePage")
	public R getRebatePage(@RequestBody AdAgentRebateVo req){
		return adAgentService.getRebatePage(req);
	}
	/**
	 * 返点列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/getRebateList")
	public R getRebateList(@RequestBody AdAgentRebateVo req){
		if (StringUtils.isBlank(req.getAgentId())){
			return R.failed("代理商ID不能为空");
		}
		return adAgentService.getRebateList(req);
	}
	/**
	 * 添加返点
	 * @param req
	 * @return
	 */
	@SysLog("添加返点")
	@RequestMapping("/addRebate")
	public R addRebate(@RequestBody AdAgentRebateVo req){
		if (StringUtils.isBlank(req.getAgentId())){
			return R.failed("代理商ID不能为空");
		}
		if (StringUtils.isBlank(req.getAgentName())){
			return R.failed("代理商名称不能为空");
		}
		if (StringUtils.isBlank(req.getPlatformId())){
			return R.failed("平台ID不能为空");
		}
		if (StringUtils.isBlank(req.getChannelCode())){
			return R.failed("请选择渠道");
		}
		if (StringUtils.isBlank(req.getChannelName())){
			return R.failed("请选择渠道");
		}
		if (StringUtils.isBlank(req.getRebate())){
			return R.failed("返点不能为空");
		}
		req.setRebate(CheckUtil.delZero(req.getRebate()));
		if (!CheckUtil.checkInteger(req.getRebate())){
			return R.failed("请输入0-100 精确到2位小数的数！");
		}
		if (StringUtils.isBlank(req.getEffectiveTime())){
			return R.failed("生效时间不能为空");
		}
		return adAgentService.addRebate(req);
	}
	/**
	 * 编辑返点
	 * @param req
	 * @return
	 */
	@SysLog("编辑返点")
	@RequestMapping("/editRebate")
	public R editRebate(@RequestBody AdAgentRebateVo req){
		if (StringUtils.isBlank(req.getId())){
			return R.failed("ID不能为空");
		}
		if (StringUtils.isBlank(req.getAgentId())){
			return R.failed("未获取到代理商ID");
		}
		if (StringUtils.isBlank(req.getPlatformId())){
			return R.failed("平台ID不能为空");
		}
		if (StringUtils.isBlank(req.getChannelCode())){
			return R.failed("请选择渠道");
		}
		if (StringUtils.isBlank(req.getChannelName())){
			return R.failed("请选择渠道");
		}
		if (StringUtils.isBlank(req.getRebate())){
			return R.failed("返点不能为空");
		}
		req.setRebate(CheckUtil.delZero(req.getRebate()));
		if (!CheckUtil.checkInteger(req.getRebate())){
			return R.failed("请输入0-100 精确到2位小数的数！");
		}
		if (StringUtils.isBlank(req.getEffectiveTime())){
			return R.failed("生效时间不能为空");
		}
		return adAgentService.editRebate(req);
	}
	/**
	 * 删除返点
	 * @param req
	 * @return
	 */
	@SysLog("删除返点")
	@RequestMapping("/delRebate")
	public R delRebate(@RequestBody AdAgentRebateVo req){
		if (StringUtils.isBlank(req.getId())){
			return R.failed("ID不能为空");
		}
		return adAgentService.delRebate(req);
	}


}
