package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.LandingGroup;

/*
 * @author yk
 */
public interface LandingGroupService extends IService<LandingGroup> {


}
