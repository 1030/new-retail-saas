package com.pig4cloud.pig.ads.gdt.service;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.dto.GdtAdvertiserDailyReportReq;
import com.pig4cloud.pig.api.gdt.entity.GdtAdvertiser;
import com.pig4cloud.pig.api.gdt.vo.GdtAuthCodeReqVo;
import com.pig4cloud.pig.api.vo.AdAccountAgentVo;
import com.pig4cloud.pig.common.core.util.R;

/**
  *     广点通广告账户
 *  @author zxm
 */
public interface GdtAdvertiserService extends IService<GdtAdvertiser> {

	/**
	 * @可授权的账号列表
	 * @return
	 */
	List<String> getAuthAdvList();

	R pagedAdversWithFund(Page page, String searchStr);

	R pagedDailyReport(GdtAdvertiserDailyReportReq req);

	R changeDailyBudget(String accountId, BigDecimal budget);

	R orderDailyBudget(String accountId, BigDecimal budget);

	List<GdtAdvertiser> curUserAdvers();

	void saveUpdateList(List<GdtAdvertiser> list);

	List<AdAccountAgentVo.Advertiser> saveAuth(GdtAuthCodeReqVo authVo) throws Exception;
	
}
