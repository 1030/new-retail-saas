package com.pig4cloud.pig.ads.controller.bd;

import com.pig4cloud.pig.ads.service.bd.BdAdgroupService;
import com.pig4cloud.pig.ads.service.bd.BdCampaignService;
import com.pig4cloud.pig.api.entity.bd.BdAdgroupReq;
import com.pig4cloud.pig.common.core.util.CommonUtils;
import com.pig4cloud.pig.common.core.util.ECollectionUtil;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

/**
 * 百度
 * @author  zjz
 * @version  2022-12-28 10:17:47
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bdPlan")
public class BdPlanController {
	private final BdAdgroupService bdAdgroupService;

	private final BdCampaignService bdCampaignService;

	/**
	 * 修改广告计划的预算
	 * @param req
	 * @return
	 */
	@RequestMapping("/updateDayBudget")
	public R updateDayBudget(@RequestBody BdAdgroupReq req){
		// 校验入参
		if (null == req) {
			return R.failed("参数不能为空");
		}
		Long advertiserId = req.getAdvertiserId();
		if (Objects.isNull(advertiserId)) {
			return R.failed("广告账户id不能为空");
		}
		if (null == req.getAdgroupFeedId()) {
			return R.failed("广告计划id不能为空");
		}
		if (null == req.getType()) {
			return R.failed("修改预算类型不能为空");
		}
		if (null == req.getBudget()) {
			return R.failed("预算金额不能为空");
		}
		if (!CommonUtils.isMoney(req.getBudget().toString(),2)) {
			return R.failed("预算金额必须是纯数字,且小数点后两位");
		}
		return bdCampaignService.updateDayBudget(req);
	}


	/**
	 * 修改广告计划的出价
	 * @param req
	 * @return
	 */
	@RequestMapping("/updateBid")
	public R updateBid(@RequestBody BdAdgroupReq req){
		// 校验入参
		if (null == req) {
			return R.failed("参数不能为空");
		}
		Long advertiserId = req.getAdvertiserId();
		if (Objects.isNull(advertiserId)) {
			return R.failed("广告账户id不能为空");
		}
		if (null == req.getAdgroupFeedId()) {
			return R.failed("广告计划id不能为空");
		}
		if (null == req.getCpaBid()) {
			return R.failed("出价值不能为空");
		}
		return bdAdgroupService.updateBid(req);
	}



	/**
	 * 通过广告计划ID查询广告计划操作状态
	 * @param adIds
	 * @returnput_status
	 */
	@GetMapping("/getPutStatus")
	public R getPutStatus(@RequestParam String adIds){
		// 校验入参
		Set<Long> adIdSet = ECollectionUtil.stringToLongSet(adIds);
		if (adIdSet.isEmpty()) {
			return R.ok(Collections.emptyList());
		}
		return bdAdgroupService.getPutStatus(adIdSet);
	}


}


