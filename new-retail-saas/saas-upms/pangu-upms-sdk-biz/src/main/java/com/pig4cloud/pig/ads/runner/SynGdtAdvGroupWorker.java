package com.pig4cloud.pig.ads.runner;

import com.pig4cloud.pig.ads.gdt.service.IGdtAdgroupService;
import com.pig4cloud.pig.api.gdt.dto.GdtAdgroupCreateTempDto;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.util.Times;

import java.util.concurrent.TimeUnit;

@Slf4j
public class SynGdtAdvGroupWorker implements Runnable {

	private IGdtAdgroupService gdtAdgroupService;

	private String accountId;

	private Long newAdgroupId;

	//最大重试次数
	private static final Integer TRY_TIMES = 6;
	//重试间隔时间单位秒
	private static final Integer[] INTERVAL_TIME = {15, 60, 120, 150, 300, 600};

	public SynGdtAdvGroupWorker(IGdtAdgroupService gdtAdgroupService, String accountId, Long newAdgroupId) {
		this.gdtAdgroupService = gdtAdgroupService;
		this.accountId = accountId;
		this.newAdgroupId = newAdgroupId;
	}


	@Override
	public void run() {
		this.fetchAdgroupId();
	}


	private void fetchAdgroupId(){
		try {
			Integer retryNum = 1;
			while (retryNum <= TRY_TIMES) {
				try {
					GdtAdgroupCreateTempDto gdtAdgroupCreateTempDtoF = new GdtAdgroupCreateTempDto();
					gdtAdgroupCreateTempDtoF.setAdgroupId(newAdgroupId);
					gdtAdgroupCreateTempDtoF.setAccountId(accountId);
					gdtAdgroupService.getGdtGroup(gdtAdgroupCreateTempDtoF);
					TimeUnit.SECONDS.sleep(INTERVAL_TIME[retryNum - 1]);
					retryNum++;
				} catch (Throwable e) {
					retryNum++;
					TimeUnit.SECONDS.sleep(INTERVAL_TIME[retryNum - 1]);
					continue;
				}
			}
		}catch(Throwable e){
			log.error("拉取第三方广告组失败", e);
		}
	}
}
