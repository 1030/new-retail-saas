package com.pig4cloud.pig.ads.gdt.service;

import com.pig4cloud.pig.api.gdt.dto.GdtUnionPackageDto;
import com.pig4cloud.pig.common.core.util.R;

public interface GdtSceneSpecService {
	/**
	 * 获取场景定向数据
	 * @param accountId
	 * @param type
	 * @return
	 */
	R getGdtSenPositionTags(String accountId, String type);

	/**
	 * 获取优量汇流量包
	 * @param accountId
	 * @param type
	 * @param page
	 * @param pageSize
	 * @return
	 */
	R getGdtUnionPositionPackages(String accountId, String type,int page,int pageSize);

	/**
	 * 创建优量汇流量包
	 * @param gdtUnionPackageDto
	 * @return
	 */
	R createGdtUnionPositionPackages(GdtUnionPackageDto gdtUnionPackageDto);
}
