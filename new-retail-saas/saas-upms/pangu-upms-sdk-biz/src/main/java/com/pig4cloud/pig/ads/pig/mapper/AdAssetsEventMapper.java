package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdAssetsEvent;
import org.apache.ibatis.annotations.Mapper;

/**
 * 广告资产事件表
 * @author  chenxiang
 * @version  2022-04-01 16:25:19
 * table: ad_assets_event
 */
@Mapper
public interface AdAssetsEventMapper extends BaseMapper<AdAssetsEvent> {
}


