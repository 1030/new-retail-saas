package com.pig4cloud.pig.ads.pig.mapper.gdt;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.dto.GdtAdgroupTempDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAdgroupTemp;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 广点通-广告组临时 数据层
 * 
 * @author hma
 * @date 2020-12-05
 */
@Mapper
public interface GdtAdgroupTempMapper   extends BaseMapper<GdtAdgroupTemp>
{

//	/**
//     * 查询广点通-广告组临时信息
//     *
//     * @param id 广点通-广告组临时ID
//     * @return 广点通-广告组临时信息
//     */
//	public GdtAdgroupTemp selectGdtAdgroupTempById(Long id);
//
//	/**
//     * 查询广点通-广告组临时列表
//     *
//     * @param gdtAdgroupTemp 广点通-广告组临时信息
//     * @return 广点通-广告组临时集合
//     */
//	public List<GdtAdgroupTemp> selectGdtAdgroupTempList(GdtAdgroupTemp gdtAdgroupTemp);
//
//	/**
//     * 新增广点通-广告组临时
//     *
//     * @param gdtAdgroupTemp 广点通-广告组临时信息
//     * @return 结果
//     */
//	public int insertGdtAdgroupTemp(GdtAdgroupTemp gdtAdgroupTemp);
//
//	/**
//     * 修改广点通-广告组临时
//     *
//     * @param gdtAdgroupTemp 广点通-广告组临时信息
//     * @return 结果
//     */
//	public int updateGdtAdgroupTemp(GdtAdgroupTemp gdtAdgroupTemp);
//
//	int updateGdtAdgroupTempById(GdtAdgroupTemp gdtAdgroupTemp);
//
//	/**
//     * 批量添加广点通-广告组临时
//     *
//     * @param gdtAdgroupTempList 需要添加的数据集合
//     * @return 结果
//     */
//	public int batchInsertGdtAdgroupTemp(List<GdtAdgroupTemp> gdtAdgroupTempList);


	/**
	 * 广点通广告组模板查询
	 * @param gdtAdgroupTempDto
	 * @return
     */
	List<GdtAdgroupTemp> selectGdtAdgroupTemplate(GdtAdgroupTempDto gdtAdgroupTempDto);


	GdtAdgroupTemp getGdtAdgroupTemplateById(GdtAdgroupTempDto gdtAdgroupTempDto);
}