package com.pig4cloud.pig.ads.presto.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.dto.AdCommentStatisticsDTO;
import com.pig4cloud.pig.api.entity.AdCommentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * trino跨库统计查询评论相关信息
 *
 * @Title null.java
 * @Package com.pig4cloud.pig.ads.presto.mapper
 * @Author 马嘉祺
 * @Date 2021/7/9 14:23
 * @Description
 */
@Mapper
@InterceptorIgnore(tenantLine = "1")
public interface AdCommentStatisticsMapper extends BaseMapper<AdCommentEntity> {

//	int selectCommentStatisticsCount(@Param("pigSchema") String pigSchema, @Param("m3399Schema") String m3399Schema, @Param("ch3399Schema") String ch3399Schema, @Param("record") AdCommentStatisticsDTO record);
//
//	List<AdCommentStatisticsDTO> selectCommentStatisticsList(@Param("pigSchema") String pigSchema, @Param("m3399Schema") String m3399Schema, @Param("ch3399Schema") String ch3399Schema, @Param("record") AdCommentStatisticsDTO record);

}
