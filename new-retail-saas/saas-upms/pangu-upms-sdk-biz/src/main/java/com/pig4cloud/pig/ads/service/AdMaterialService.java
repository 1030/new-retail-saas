/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdMaterial;
import com.pig4cloud.pig.api.entity.AdMaterialReportDO;
import com.pig4cloud.pig.api.vo.*;
import com.pig4cloud.pig.common.core.util.R;

import java.io.IOException;
import java.util.List;

/**
 * 素材库
 *
 * @date 2021-06-19 16:12:13
 */
public interface AdMaterialService extends IService<AdMaterial> {

	R getUserByMaterial(UserByMaterialVo req);

	/**
	 * 素材 - 页面
	 *
	 * @param req
	 * @return
	 */
	R getPage(AdMaterialPageVo req);

	/**
	 * 素材 - 页面
	 *
	 * @param req
	 * @return
	 */
	R getList(AdMaterialVo req);

	/**
	 * 素材 - 下拉框
	 * @param req
	 * @return
	 */
	R getDownList(AdMaterialVo req);

	/**
	 * 添加素材
	 *
	 * @param req
	 * @return
	 */
	R addMaterial(AdMaterialVo req);

	/**
	 * 编辑
	 *
	 * @param req
	 * @return
	 */
	R edit(AdMaterialVo req);

	/**
	 * 删除
	 *
	 * @param req
	 * @return
	 */
	R del(AdMaterialVo req);

	/**
	 * 根据id查询素材信息
	 *
	 * @param req
	 * @return
	 */
	R findById(AdMaterialVo req);

	/**
	 * 添加收藏
	 *
	 * @param req
	 * @return
	 */
	R addCollect(AdMaterialVo req);

	/**
	 * 删除收藏
	 *
	 * @param req
	 * @return
	 */
	R delCollect(AdMaterialVo req);

	/**
	 * 批量推送
	 *
	 * @param req
	 * @return
	 */
	R pushMaterial(AdMaterialVo req);

	IPage<AdMaterial> pageList(Page<AdMaterial> objectPage, AdMaterialSearchVo searchVo);

	/**
	 * 根据素材ID列表查询素材信息
	 *
	 * @param materialIds
	 * @return
	 */
	List<AdMaterial> getMaterialListByIds(List<Long> materialIds);


	/**
	 * 素材数据报表
	 *
	 * @param amr
	 * @return
	 */
	List<AdMaterialReportDO> getMaterialReportList(AdMaterialReportVo amr);

}
