/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pig4cloud.pig.ads.controller.gdt;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.pig4cloud.pig.ads.gdt.service.GdtSelectListService;
import com.pig4cloud.pig.ads.service.AdIndustryService;
import com.pig4cloud.pig.ads.service.TtCustomAudienceService;
import com.pig4cloud.pig.api.entity.AdIndustry;
import com.pig4cloud.pig.api.entity.TtCustomAudience;
import com.pig4cloud.pig.api.gdt.entity.GdtCustomAudiences;
import com.pig4cloud.pig.api.util.JsonUtil;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


/**
 * @ 广告账户 前端控制器
 * @author john
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/gdt/select_list")
@Api(value = "/gdt/select_list", tags = "下拉框列表")
public class GdtSelectListController {

	private final GdtSelectListService baseService;

	private final TtCustomAudienceService ttCustomAudienceService;

	private final AdIndustryService adIndustryService;

	@RequestMapping("/cz_site")
	public R czSite(String advertiserId, Integer page, Integer page_size) {
		return baseService.czSite(advertiserId, page, page_size);
	}

	@RequestMapping("/industry_list")
	public R industryList(String advertiserId) throws JsonProcessingException {
		//return baseService.industryList(advertiserId);
		String result= JsonUtil.toUnderlineJSONStringValue(adIndustryService.selectAdIndustryResAll(new QueryWrapper<AdIndustry>()));
		return R.ok(JSONObject.parseArray(result));
	}

	@RequestMapping("/marital_status")
	public R maritalStatus() {
		return baseService.maritalStatus();
	}

	@RequestMapping("/working_status")
	public R workingStatus() {
		return baseService.workingStatus();
	}

	/**
	 * @广点通定向包-人群包列表
	 */
	@RequestMapping("/custom_audience")
	public R customAudience(Page page, String accountId, String searchName) {
		//兼容，前端不需要修改
//		return baseService.customAudience(page, accountId, searchName);
		Integer type = Integer.parseInt(PlatformTypeEnum.GDT.getValue());
		List<TtCustomAudience> list = ttCustomAudienceService.list(Wrappers.<TtCustomAudience>lambdaQuery()
				.eq(TtCustomAudience::getMediaType,type)
				.eq(StringUtils.isNotBlank(accountId),TtCustomAudience::getAccountId,accountId)
				.eq(TtCustomAudience::getGdtStatus,"SUCCESS")
				.like(StringUtils.isNotBlank(searchName),TtCustomAudience::getName,searchName)
				.orderByDesc(TtCustomAudience::getTtCreateTime));
		List<GdtCustomAudiences> result = new ArrayList<>();
		//兼容广点通返回格式，前端仅需更换请求链接
		if (CollectionUtils.isNotEmpty(list)) {
			for (TtCustomAudience audience: list) {
				GdtCustomAudiences gdtCustomAudiences = new GdtCustomAudiences();
				gdtCustomAudiences.setId(audience.getId().intValue());
				gdtCustomAudiences.setAudienceId(audience.getCustomAudienceId());
				gdtCustomAudiences.setAccountId(Long.valueOf(audience.getAccountId()));
				gdtCustomAudiences.setName(audience.getName());
				gdtCustomAudiences.setExternalAudienceId(audience.getExternalAudienceId());
				gdtCustomAudiences.setDescription(audience.getDescription());
				gdtCustomAudiences.setType(audience.getGdtType());
				gdtCustomAudiences.setStatus(audience.getGdtStatus());
				gdtCustomAudiences.setErrorCode(audience.getErrorCode());
				gdtCustomAudiences.setUserCount(audience.getCoverNum());
				gdtCustomAudiences.setCreatedTime(audience.getTtCreateTime());
				gdtCustomAudiences.setLastModifiedTime(audience.getModifyTime());
				result.add(gdtCustomAudiences);
			}
		}
		return R.ok(result);
	}

	/**
	 * @广点通定向包-人群包列表---统一到 tt_custom_audience
	 */
	@RequestMapping("/custom_audience_new")
	public R customAudienceNew(Page page, String accountId, String searchName) {
		Integer type = Integer.parseInt(PlatformTypeEnum.GDT.getValue());
		IPage<TtCustomAudience> pageList = ttCustomAudienceService.page(page, Wrappers.<TtCustomAudience>lambdaQuery()
				.eq(TtCustomAudience::getMediaType,type)
//				.eq(TtCustomAudience::getGdtStatus,"SUCCESS")
				.eq(StringUtils.isNotBlank(accountId),TtCustomAudience::getAccountId,accountId)
				.like(StringUtils.isNotBlank(searchName),TtCustomAudience::getName,searchName)
				.or().like(TtCustomAudience::getCustomAudienceId,searchName)
				.orderByDesc(TtCustomAudience::getTtCreateTime));
		return R.ok(pageList);
	}

	@RequestMapping("/user_os")
	public R userOs() {
		return baseService.userOs();
	}

	@RequestMapping("/behavior_interest")
	public R behaviorInterest(String accountId, String tag) {
		return baseService.behaviorInterest(accountId, tag);
	}

	@RequestMapping("/parent_levels")
	public R getParentLevels(String accountId, String ids, String tag) {
		return baseService.getParentLevels(accountId, ids, tag);
	}
}
