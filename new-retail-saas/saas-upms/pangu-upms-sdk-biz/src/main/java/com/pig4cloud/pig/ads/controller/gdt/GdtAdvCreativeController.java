package com.pig4cloud.pig.ads.controller.gdt;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.pig4cloud.pig.ads.gdt.service.IGdtAdCreativeService;
import com.pig4cloud.pig.ads.gdt.service.IGdtAdCreativeTempService;
import com.pig4cloud.pig.ads.gdt.service.IGdtAdService;
import com.pig4cloud.pig.api.enums.GdtPageTypeEnum;
import com.pig4cloud.pig.api.gdt.dto.*;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.api.enums.StatusEnum;
import com.pig4cloud.pig.api.gdt.vo.GdtAdCreativeVo;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @广点通创意管理模板
 * @author hma
 * 2020-12-09
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/gdt/creative")
@Api(value = "/gdt/creative", tags = "广点通创意管理模板")
public class GdtAdvCreativeController {

  @Resource
  private IGdtAdCreativeService gdtAdCreativeService;
  @Resource
  private IGdtAdCreativeTempService gdtAdCreativeTempService;
  @Resource
  private IGdtAdService gdtAdService;


	@RequestMapping("/creativeOnOff")
	public R updateGdtAdOnOff(GdtAdCreativeVo gdtAdCreativeVo) {
		if(null == gdtAdCreativeVo){
			return R.failed( "参数不能为空");
		}
		return gdtAdCreativeService.updateGdtAdOnOff(gdtAdCreativeVo) ;
	}

  /*
  * 获取广点通广告创意列表
  *
  * */
  @RequestMapping("/creativeReport")
  public R getGdtAdCreativeReport(GdtAdCreativeVo gdtAdCreativeVo) {

	  if(null == gdtAdCreativeVo){
		  return R.failed( "参数不能为空");
	  }
	  if (null == gdtAdCreativeVo.getSdate() || StringUtils.isBlank(gdtAdCreativeVo.getSdate())){
		  return R.failed( "起始时间不能为空");
	  }
	  if (null == gdtAdCreativeVo.getEdate() || StringUtils.isBlank(gdtAdCreativeVo.getEdate())){
		  return R.failed( "结束时间不能为空");
	  }
	  return gdtAdCreativeService.getGdtAdCreativeReport(gdtAdCreativeVo) ;
  }

	/**
	 * 获取广告版本列表
	 * @param gdtAdCreativeDto
	 * @return
     */
	@SysLog("获取广告创意形式/创意内容")
	@RequestMapping("/getCreativeTemplates")
	public R getCreativeTemplates(GdtAdCreativeDto gdtAdCreativeDto) {
		if(null == gdtAdCreativeDto){
			return R.failed( "参数不能为空");
		}
		if(StringUtils.isBlank(gdtAdCreativeDto.getAccountId())){
			return R.failed( "广告账号不允许为空");
		}
		if(null == gdtAdCreativeDto.getPromotedObjectType()&& null == gdtAdCreativeDto.getCampaignId() ){
			return R.failed( "推广目标或推广计划id两者不同同时为空");
		}
		if(null == gdtAdCreativeDto.getBidMode()){
			return R.failed( "出价方式不允许为空");
		}

		if(null == gdtAdCreativeDto.getSiteSets()){
			String siteSets=JSONArray.toJSONString("SITE_SET_MOBILE_UNION,SITE_SET_MOBILE_INNER,SITE_SET_TENCENT_NEWS,SITE_SET_TENCENT_VIDEO,SITE_SET_KANDIAN,SITE_SET_QQ_MUSIC_GAME".split(","));
			gdtAdCreativeDto.setSiteSets(siteSets);
			gdtAdCreativeDto.setSiteSet(siteSets);
		}else{
			gdtAdCreativeDto.setSiteSet(gdtAdCreativeDto.getSiteSets());
		}
		return gdtAdCreativeService.getCreativeTemplatesUpdate(gdtAdCreativeDto,null, StatusEnum.TYPE_THREE.getStatus()) ;

	}


	/**
	 * 创建创意  定义post结构 @json格式
	 * @return
     */
	@SysLog("创建创意")
	@RequestMapping("/createGdtCreative")
	public R  createGdtGroupTemp(@RequestBody  GdtAdCreativeAddDto  gdtAdCreativeAddDto ){
			log.info("createGdtGroupTemp reqeust:"+ JSONObject.toJSONString(gdtAdCreativeAddDto));
		if(null  == gdtAdCreativeAddDto ){
			return R.failed("创意参数不能为空");
		}
		List<GdtAdCreativeCreateDto> creativeCreateDtoList = gdtAdCreativeAddDto.getGdtAdCreativeCreateDtoList();
		if(CollectionUtils.isEmpty(creativeCreateDtoList)){
			return R.failed("创意对象不存在");
		}
		   creativeCreateDtoList.forEach(gdtAdCreativeCreateDto->{
				if(null == gdtAdCreativeCreateDto){
					throw new BusinessException(11,"参数不能为空");
				}
				if(null == gdtAdCreativeCreateDto.getAccountId()){
					throw new BusinessException(11,"广告账号不允许为空");
				}
				if(null == gdtAdCreativeCreateDto.getCampaignId() ){
					throw new BusinessException(11,"推广计划id不允许为空");
				}
				if(null == gdtAdCreativeCreateDto.getAdgroupId()){
					throw new BusinessException(11,"广告组不允许为空");
				}
				if(null == gdtAdCreativeCreateDto.getAdcreativeName()){
					throw new BusinessException(11,"创意名称不允许为空");
				}
				if(null == gdtAdCreativeCreateDto.getAdcreativeTemplateId()){
					throw new BusinessException(11,"id不允许为空");
				}
				if(null == gdtAdCreativeCreateDto.getAdcreativeElementsObject()&&StringUtils.isBlank(gdtAdCreativeCreateDto.getAdcreativeElements())){
					throw new BusinessException(11,"创意元素不允许为空");
				}
				if(StringUtils.isBlank(gdtAdCreativeCreateDto.getPromotedObjectType())){
					throw new BusinessException(11,"推广目标类型不允许为空");
				}
				if(StringUtils.isBlank(gdtAdCreativeCreateDto.getPageType())){
					throw new BusinessException(11,"落地页类型不能为空");
				}
			    if(!GdtPageTypeEnum.containsType(gdtAdCreativeCreateDto.getPageType())){
				   throw new BusinessException(11,String.format("[%s]此落地页类型不支持",gdtAdCreativeCreateDto.getPageType()));
			    }
			   	if(GdtPageTypeEnum.PAGE_TYPE_DEFAULT.getType().equals(gdtAdCreativeCreateDto.getPageType())){
					gdtAdCreativeCreateDto.setPageSpec(null);
				}
			   JSONObject pageSpec=null;
			    if((!GdtPageTypeEnum.PAGE_TYPE_DEFAULT.getType().equals(gdtAdCreativeCreateDto.getPageType()))
					   &&(Objects.isNull(gdtAdCreativeCreateDto.getPageSpec())
						||(Objects.isNull(pageSpec=JSON.parseObject(gdtAdCreativeCreateDto.getPageSpec()))
						||(StringUtils.isBlank(pageSpec.getString("page_id")))
				))){
					throw new BusinessException(11,"落地页不能为空");
			   }
				if(gdtAdCreativeCreateDto.getAutomaticSiteEnabled() != null && gdtAdCreativeCreateDto.getAutomaticSiteEnabled()){
					gdtAdCreativeCreateDto.setSiteSet(null);
					gdtAdCreativeCreateDto.setSiteSets(null);
				}
			});
			return gdtAdCreativeService.createGdtCreativeTemp(creativeCreateDtoList);


	}





	/**
	 * 创意预览
	 * @return
	 */
	@SysLog("创意预览")
	@RequestMapping("/creativePreviews")
	public R  creativePreviews(GdtAdCreativeDto gdtAdCreativeDto)
	{
		if(null == gdtAdCreativeDto){
			return R.failed( "参数不能为空");
		}
		if(StringUtils.isBlank(gdtAdCreativeDto.getAccountId())){
			return R.failed( "广告账号不允许为空");
		}
		if(null == gdtAdCreativeDto.getAdgroupId()){
			return R.failed( "广告组id不允许为空");
		}

		return gdtAdCreativeService.creativePreviews(gdtAdCreativeDto);
	}


  //todo 落地页接口已有



	/**
	 * 获取广告创意按钮文案
	 * @return
	 */
	@SysLog("获取广告创意按钮文案")
	@RequestMapping("/getButtonText")
	public R getButtonText() {
		return gdtAdCreativeService.getButtonText();
	}


	/**
	 * todo 暂时先放着 对应模板
	 * 获取广告版本列表
	 * @param gdtAdCreativeDto
	 * @return
	 */
	@SysLog("获取广告创意模板")
	@RequestMapping("/getCreativeTemp")
	public R getCreativeTemp(GdtAdCreativeDto gdtAdCreativeDto) {
		if(null == gdtAdCreativeDto){
			return R.failed( "参数不能为空");
		}

		if(null == gdtAdCreativeDto.getAccountId()){
			return R.failed("广告账号不允许为空");
		}
		return gdtAdCreativeTempService.getCreativeTemp(gdtAdCreativeDto);
	}



	/**
	 * todo 暂时先放着 对应模板
	 * 获取广告版本列表
	 * @param gdtAdgroupTempDto
	 * @return
	 */
	@SysLog("获取落地页")
	@RequestMapping("/getGdtPage")
	public R getGdtPage(GdtAdgroupTempDto gdtAdgroupTempDto) {
		if(null == gdtAdgroupTempDto){
			return R.failed( "参数不能为空");
		}

		if(null == gdtAdgroupTempDto.getAccountId()){
			return R.failed("广告账号不允许为空");
		}
		if(null == gdtAdgroupTempDto.getPromotedObjectType()){
			return R.failed("推广目标类型不允许为空");
		}
		return gdtAdCreativeService.getGdtPage(gdtAdgroupTempDto);
	}




	/**
	 *
	 * 测试环境广告通广告
	 * @param gdtAd
	 * @return
	 */
	@SysLog("测试环境广告通广告")
	@RequestMapping("/testCreateGdtAd")
	public R testCreateGdtAd(GdtAdDto gdtAd) {
		if(null == gdtAd){
			return R.failed( "参数不能为空");
		}

		if(null == gdtAd.getAccountId()){
			return R.failed("广告账号不允许为空");
		}
		if(null == gdtAd.getAdgroupId()){
			return R.failed("推广组id不允许为空");
		}
		if(null == gdtAd.getAdcreativeId()){
			return R.failed("创意id不允许为空");
		}
		if(null == gdtAd.getAdName()){
			return R.failed("广告名称不允许为空");
		}
		gdtAd= gdtAdService.createGdtAd(gdtAd);
		return R.ok(gdtAd);
	}


	/**
	 *
	 * 查询广告文案
	 * @param creativetoolsDto
	 * @return
	 */
	@SysLog("查询广告文案")
	@RequestMapping("/getCreativetoolsText")
	public R getCreativetoolsText(CreativetoolsDto creativetoolsDto) {
		if(null == creativetoolsDto){
			return R.failed( "参数不能为空");
		}

		if(null == creativetoolsDto.getAccountId()){
			return R.failed("广告账号不允许为空");
		}
		if(null != creativetoolsDto.getKeyword() && creativetoolsDto.getKeyword().length() > 255 ){
			return R.failed("关键字长度不允许超过255个长度");
		}
		return  gdtAdCreativeService.getCreativetoolsText(creativetoolsDto);
	}




}
