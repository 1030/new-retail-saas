/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdAccountAgentDto;
import com.pig4cloud.pig.api.entity.AdAccount;
import com.pig4cloud.pig.api.vo.AdAccountAgentVo;
import com.pig4cloud.pig.api.vo.AdAccountRequest;
import com.pig4cloud.pig.api.vo.AdAccountVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 广告账户表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
public interface AdAccountService extends IService<AdAccount> {
	/**
	 * 分页列表
	 *
	 * @param req
	 * @return
	 */
	R getPage(AdAccountRequest req);

	/**
	 * 查询列表
	 *
	 * @param req
	 * @return
	 */
	R getList(AdAccountVo req);

	/**
	 * 编辑信息
	 *
	 * @param req
	 * @return
	 */
	R edit(AdAccountVo req);

	/**
	 * 代理列表
	 *
	 * @param req
	 * @return
	 */
	R getAccountAgentList(AdAccountAgentVo req);

	/**
	 * 根据当前登录用户查询列表
	 *
	 * @param req
	 * @return
	 */
	List<AdAccount> getListByAccount(AdAccountVo req);

	/**
	 * 设置代理商
	 *
	 * @param req
	 * @return
	 */
	R setupAccountAgent(AdAccountAgentVo req);

	/**
	 * 保存代理商
	 *
	 * @param req
	 */
	void saveAccountAgent(AdAccountAgentVo req);

	/**
	 * 修改代理商
	 *
	 * @param req
	 */
	void updateAccountAgent(AdAccountAgentVo req);

	/**
	 * 批量添加代理商
	 *
	 * @param req
	 */
	List<String> batchAddAccountAgent(AdAccountAgentVo req, Integer currentUserId);

	/**
	 * 删除账户代理商
	 *
	 * @param req
	 * @return
	 */
	R delAccountAgent(AdAccountAgentVo req);

	/**
	 * 广告账户树形
	 *
	 * @param req
	 * @return
	 */
	public R getTree(AdAccountVo req);


	/**
	 * 查询用户列表绑定的广告账号列表
	 *
	 * @return
	 */

	R<List<AdAccount>> getAccountList(List<Integer> userIdArr);

	/**
	 * 获取授权的广告账号列表
	 *
	 * @param userIdArr
	 * @return
	 */
	List<String> getAccountList2(List<Integer> userIdArr);

	/**
	 * 查询列表-所有广告账户
	 *
	 * @param req
	 * @return
	 */
	List<AdAccount> getAllList(AdAccountVo req);

	R queryAccountByHousekeeper(String housekeeper, String adAccount);

	R setBatchAccountAgent(AdAccountAgentDto req);

	AdAccount queryAdByHousekeeper(AdAccountVo req);

	/**
	 * 根据授权查看广告账户列表
	 *
	 * @param req
	 * @return
	 */
	List<AdAccount> getAccountsByAuthorize(AdAccountVo req);


	void saveUpdateList(List<AdAccount> list);

}
