package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdAccountWarningSetting;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdAccountWarningSettingMapper extends BaseMapper<AdAccountWarningSetting> {

}
