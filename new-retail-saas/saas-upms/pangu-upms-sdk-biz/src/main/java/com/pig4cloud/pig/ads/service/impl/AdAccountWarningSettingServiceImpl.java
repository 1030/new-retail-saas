package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdAccountWarningSettingMapper;
import com.pig4cloud.pig.ads.service.AdAccountWarningSettingService;
import com.pig4cloud.pig.api.entity.AdAccountWarningSetting;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * @Description 账户提醒设置service
 * @Author chengang
 * @Date 2021/7/13
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class AdAccountWarningSettingServiceImpl extends ServiceImpl<AdAccountWarningSettingMapper, AdAccountWarningSetting> implements AdAccountWarningSettingService {

}
