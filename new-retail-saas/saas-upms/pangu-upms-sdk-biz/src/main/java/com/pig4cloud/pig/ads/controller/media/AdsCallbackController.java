package com.pig4cloud.pig.ads.controller.media;

import com.alibaba.fastjson.JSON;
import com.pig4cloud.pig.ads.service.media.MediaEngine;
import com.pig4cloud.pig.ads.service.media.MediaFactory;
import com.pig4cloud.pig.api.entity.ads.AdsCallBackResponse;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description
 * @Author chengang
 * @Date 2022/12/13
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("ads")
@Slf4j
@Inner(value = false)
public class AdsCallbackController {

	@Resource
	private MediaFactory mediaFactory;

	/**
	 *
	 * @param mediaCode 目前仅百度渠道使用，其他渠道暂使用原回调地址 [bd:百度]
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/callback/{mediaCode}")
	public R callback(@PathVariable String mediaCode, AdsCallBackResponse response) {
		log.info("回调原始参数:{}", JSON.toJSONString(response));
		MediaEngine bdEngine = mediaFactory.getMediaEngineMap().get(mediaCode);
		if (bdEngine != null) {
			return bdEngine.callback(response,null);
		}
		return R.failed("媒体渠道不存在,请确认回调地址是否正确");
	}

}
