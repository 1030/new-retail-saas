package com.pig4cloud.pig.ads.service;

import com.pig4cloud.pig.api.dto.CustomAudienceDto;
import com.pig4cloud.pig.common.core.util.R;

public interface CustomAudienceService {
	R customAudienceList(CustomAudienceDto dto);
}
