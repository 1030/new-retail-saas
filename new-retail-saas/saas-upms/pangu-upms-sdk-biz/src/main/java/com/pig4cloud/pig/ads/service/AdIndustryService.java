/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdIndustry;
import com.pig4cloud.pig.api.vo.AdIndustryRes;

import java.util.List;

/**
 * 巨量行业列表
 *
 * @author yuwenfeng
 * @date 2022-02-08 15:28:03
 */
public interface AdIndustryService extends IService<AdIndustry> {

	List<AdIndustryRes> selectAdIndustryResAll(QueryWrapper<AdIndustry> queryWrapper);

	void getIndustryList(String param);
}
