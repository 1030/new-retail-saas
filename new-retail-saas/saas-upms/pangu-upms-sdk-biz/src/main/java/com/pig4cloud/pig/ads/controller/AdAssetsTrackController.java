package com.pig4cloud.pig.ads.controller;

import com.alibaba.fastjson.JSON;
import com.pig4cloud.pig.ads.service.AdAssetsTrackService;
import com.pig4cloud.pig.api.entity.AdAssetsTrack;
import com.pig4cloud.pig.api.vo.AdAssetsTrackReq;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * 监测链接组表
 * @author  chenxiang
 * @version  2022-04-06 13:31:02
 * table: ad_assets_track
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/adAssetsTrack")
public class AdAssetsTrackController {
	
    private final AdAssetsTrackService adAssetsTrackService;

	/**
	 * 调用头条 - 创建监测组链接
	 * @param record
	 * @return
	 */
	@Inner
    @RequestMapping("/create")
    public R create(@RequestBody AdAssetsTrack record){
		if (StringUtils.isBlank(record.getAdvertiserId())
				|| Objects.isNull(record.getAssetId())
				|| StringUtils.isBlank(record.getDownloadUrl())
				|| StringUtils.isBlank(record.getActionTrackUrl())
				|| StringUtils.isBlank(record.getTrackUrlGroupName())
				|| StringUtils.isBlank(record.getPackageName())
				|| Objects.isNull(record.getAppCloudId())
				|| StringUtils.isBlank(record.getAppName())
				|| Objects.isNull(record.getPgameId())
				|| Objects.isNull(record.getGameId())
				|| StringUtils.isBlank(record.getChl())
				|| Objects.isNull(record.getAdId())){
			log.info(">>>>>>监测链接组创建失败：{}", JSON.toJSONString(record));
			return R.failed("监测链接组创建失败：缺失必须参数");
		}
    	return adAssetsTrackService.createEventTrackUrl(record);
	}
	/**
	 * 获取事件资产下的监测链接组
	 * @param record
	 * @return
	 */
	@RequestMapping("/getTrackUrl")
	public R getTrackUrl(@RequestBody AdAssetsTrackReq record){
		if (StringUtils.isBlank(record.getAdvertiserId())){
			return R.failed("未获取到广告账户ID");
		}
		if (StringUtils.isBlank(record.getAppCloudId())){
			return R.failed("缺少必要参数应用ID");
		}
		return adAssetsTrackService.getTrackUrl(record);
	}
	/**
	 * 获取优化目标
	 * @param record
	 * @return
	 */
	@RequestMapping("/getOptimizedGoal")
	public R getOptimizedGoal(@RequestBody AdAssetsTrackReq record){
		if (StringUtils.isBlank(record.getAdvertiserId())){
			return R.failed("未获取到广告账户ID");
		}
		if (StringUtils.isBlank(record.getAppCloudId())){
			return R.failed("缺少必要参数应用ID");
		}
		return adAssetsTrackService.getOptimizedGoal(record);
	}
}


