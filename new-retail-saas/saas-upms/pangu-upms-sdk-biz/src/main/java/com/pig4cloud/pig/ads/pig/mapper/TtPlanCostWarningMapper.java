package com.pig4cloud.pig.ads.pig.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.TtPlanCostWarning;

@Mapper
public interface TtPlanCostWarningMapper extends BaseMapper<TtPlanCostWarning> {

}
