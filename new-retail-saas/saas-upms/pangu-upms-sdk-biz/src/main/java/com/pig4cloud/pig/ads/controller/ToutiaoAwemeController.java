/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.ToutiaoAwemeService;
import com.pig4cloud.pig.ads.service.*;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @ 广告账户 前端控制器
 * @author john
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/tt_aweme")
@Api(value = "tt_aweme", tags = "头条抖音数据")
public class ToutiaoAwemeController {

	private final ToutiaoAwemeService toutiaoAwemeService;


	@RequestMapping("/category")
	public R getAwemeCategory(String advertiserId) {
		return toutiaoAwemeService.getAwemeCategory(advertiserId);
	}

	@RequestMapping("/authors")
	public R getAwemeAuthor(String advertiserId, String categoryId,String behaviors) {
		return toutiaoAwemeService.getAwemeAuthor(advertiserId,categoryId, behaviors);
	}

}
