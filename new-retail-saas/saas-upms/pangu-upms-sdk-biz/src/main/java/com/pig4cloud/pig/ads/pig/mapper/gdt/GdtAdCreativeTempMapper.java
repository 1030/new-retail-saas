package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtAdCreativeTemp;
import org.apache.ibatis.annotations.Mapper;


/**
 * 广点通广告创意临时 数据层
 * 
 * @author hma
 * @date 2020-12-11
 */
@Mapper
public interface GdtAdCreativeTempMapper  extends BaseMapper<GdtAdCreativeTemp>
{

}