package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.CreativeComponentDto;
import com.pig4cloud.pig.api.entity.AdCreativeComponent;
import com.pig4cloud.pig.common.core.util.R;

public interface AdCreativeComponentService extends IService<AdCreativeComponent> {

	R component4Create(CreativeComponentDto dto);

	R component4Edit(CreativeComponentDto dto);

	R getCreativeComponentList(String advertiserIds,String componentId);

}
