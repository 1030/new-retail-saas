package com.pig4cloud.pig.ads.task;

import com.pig4cloud.pig.ads.service.AdAccountWarningResultService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Description 获取不活跃广告账户定时器
 * @Author chengang
 * @Date 2021/7/13
 */
@Component
public class GetNoActiveAccountJob {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private AdAccountWarningResultService adAccountWarningResultService;

	/**
	 * String param 必须要加，不加会出现数据源加载失败的问题
	 * @param param
	 * @return
	 */
//	@Scheduled(cron = "${GetNoActiveAccountTask.cron}")
	@XxlJob("getNoActiveAccountJob")
	public ReturnT<String> getNoActiveAccountTask(String param){
		XxlJobLogger.log("-----------------获取不活跃广告账户定时任务开始--------------------------");
		adAccountWarningResultService.getNoActiveAccountList(-1);
		XxlJobLogger.log("-----------------获取不活跃广告账户定时任务结束--------------------------");
		return ReturnT.SUCCESS;
	}

}
