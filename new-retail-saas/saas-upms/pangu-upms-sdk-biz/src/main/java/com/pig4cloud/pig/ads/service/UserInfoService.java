package com.pig4cloud.pig.ads.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.UserInfo;

public interface UserInfoService  extends IService<UserInfo>{

	   // 写入数据
    void saveData (UserInfo userInfo) ;
    // ID 查询
    UserInfo selectById (Integer id) ;
    // 查询全部
    List<UserInfo> selectList () ;
}
