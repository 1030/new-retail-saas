package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.ads.service.AdCommentService;
import com.pig4cloud.pig.api.dto.AdCommentDTO;
import com.pig4cloud.pig.api.dto.AdCommentOperateDto;
import com.pig4cloud.pig.api.dto.AdCommentStatisticsDTO;
import com.pig4cloud.pig.api.dto.OperateComment;
import com.pig4cloud.pig.api.entity.AdCommentEntity;
import com.pig4cloud.pig.api.vo.AdCommentStatisticsVO;
import com.pig4cloud.pig.api.vo.AdCommentVo;
import com.pig4cloud.pig.api.vo.AdCommentVo.*;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 评论
 */
@Slf4j
@RestController
@RequestMapping(value = "/adcomment")
@AllArgsConstructor
@Validated
public class AdCommentController {

	private final AdCommentService adCommentService;

	/**
	 * 评论统计分页列表
	 *
	 * @param req
	 * @return
	 */
	@SysLog("评论管理")
	@PostMapping("commentStatisticsPage")
	public R<Page<AdCommentStatisticsDTO>> getCommentStatisticsPage(@RequestBody AdCommentStatisticsVO req) {
		Page<AdCommentStatisticsDTO> page = adCommentService.getCommentStatisticsPage(req);
		return R.ok(page);
	}

	/**
	 * 获取评论列表
	 *
	 * @param comment
	 * @return
	 */
	@PostMapping("getCommentPage")
	public R<Page<AdCommentEntity>> getCommentPage(@RequestBody @Validated(CommentPage.class) AdCommentVo comment) {
		Page<AdCommentEntity> commentPage = adCommentService.getCommentPage(comment);
		return R.ok(commentPage);
	}

	/**
	 * 回复评论
	 *
	 * @param comment
	 * @return
	 */
	@PostMapping("commentReply")
	public R commentReply(@RequestBody @Validated(CommentReplay.class) AdCommentVo comment) {
		try {
			adCommentService.commentReply(comment);
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 评论隐藏
	 */
	@PostMapping(value = "/commentHide")
	public R commentHide(@RequestBody @Validated(CommentHide.class) AdCommentVo comment) {
		try {
			adCommentService.commentHide(comment);
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 评论置顶操作
	 *
	 * @param comment
	 * @return
	 */
	@PostMapping(value = "/commentStickOnTop")
	public R commentStickOnTop(@RequestBody @Validated(CommentStickOnTop.class) AdCommentVo comment) {
		try {
			adCommentService.commentStickOnTop(comment);
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 评论用户屏蔽
	 *
	 * @param comment
	 * @return
	 */
	@PostMapping(value = "/commentBlockUsers")
	public R commentBlockUsers(@RequestBody @Validated(CommentBlockUsers.class) AdCommentVo comment) {
		try {
			adCommentService.commentBlockUsers(comment);
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}


	/**
	 * 分页列表
	 *
	 * @param req
	 * @return
	 */
	@Deprecated
	@RequestMapping("/getPage")
	public R getPage(AdCommentDTO req) {
		//设置分页条件
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());
		IPage<AdCommentVo> data = adCommentService.selectPageList(req);
		return R.ok(data, "请求成功");
	}

	/**
	 * 评论回复
	 */
	@Deprecated
	@PostMapping(value = "/reply")
	public R reply(@RequestBody AdCommentOperateDto req) {
		List<OperateComment> commentList = req.getOperateComments();
		if (!(null != commentList && commentList.size() > 0)) {
			return R.failed("评论id列表为空");
		}
		if (StringUtils.isEmpty(req.getText())) {
			return R.failed("回复内容为空");
		}
		return adCommentService.commentReply(req);
	}

	/**
	 * 隐藏评论
	 */
	@Deprecated
	@PostMapping(value = "/hide")
	public R hide(@RequestBody AdCommentOperateDto req) {
		List<OperateComment> commentList = req.getOperateComments();
		if (!(null != commentList && commentList.size() > 0)) {
			return R.failed("评论id列表为空");
		}
		return adCommentService.commentHide(req);
	}

	/**
	 * 置顶评论
	 *
	 * @param req
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/stickOnTop")
	public R stickOnTop(@RequestBody AdCommentOperateDto req) {
		List<OperateComment> commentList = req.getOperateComments();
		if (!(null != commentList && commentList.size() > 0)) {
			return R.failed("评论id列表为空");
		}
		return adCommentService.commentStick(req);
	}

	/**
	 * 查询广告账号--广告组，-广告计划 树形结构
	 *
	 * @return
	 */
	@RequestMapping("/getCommentAdPlanTree")
	public R getCommentAdPlanTree(String name) {
		return adCommentService.getCommentAdPlanTree(name);
	}
}
