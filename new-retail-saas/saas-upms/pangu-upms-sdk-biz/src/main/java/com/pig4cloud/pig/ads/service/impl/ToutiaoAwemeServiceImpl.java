package com.pig4cloud.pig.ads.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pig4cloud.pig.ads.service.ToutiaoAwemeService;
import com.pig4cloud.pig.ads.service.TtAccesstokenService;
import com.pig4cloud.pig.ads.utils.OEHttpUtils;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ToutiaoAwemeServiceImpl implements ToutiaoAwemeService {


	private final TtAccesstokenService ttAccesstokenService;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

/*	@Override
	public void test() {
		System.out.println("111111111");
	}*/


	/*@Override
	public R customAudienceList(CustomAudienceDto dto) {
		R getResult =  this.getDataFromToutiao(dto);
		if (getResult.getCode() != CommonConstants.SUCCESS)
			return getResult;

		String resultStr = (String)getResult.getData();
		JSONObject obj = JSON.parseObject(resultStr);

		JSONObject dataObj = obj.getJSONObject("data");
		String arrStr = dataObj.getString("custom_audience_list");

		List<Map> mapArr = JSON.parseArray(arrStr, Map.class);

		List<Map<String,Object>> resultList = new ArrayList<>();
		for (Map<String, Object> map :mapArr){
			Map<String, Object> item = new HashMap<>();
			item.put("custom_audience_id", map.get("custom_audience_id"));
			item.put("name", map.get("name"));
			resultList.add(item);
		}

		return R.ok(resultList);
	}
*/
	@Value("${toutiao_tools_url}")
	private String toutiaoToolsUrl;

	@Override
	public R getAwemeCategory(String advertiserId){
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiserId);

		String token = ttAccesstokenService.fetchAccesstoken(advertiserId);			//this.getAccesstoken(advertiserId);

		//调用头条接口
		String resultStr = OEHttpUtils.doGet(toutiaoToolsUrl + "aweme_multi_level_category/get/", data, token);

		//结果为null，当做失败
		if (StringUtils.isBlank(resultStr))
			return R.failed("从toutiao获取失败:resultStr is blank");

		JSONObject obj = JSON.parseObject(resultStr);
		if (!obj.getInteger("code").equals(0)){	//同步失败
			return R.failed("从toutiao获取失败:" + obj.getString("message"));
		}else{
			JSONArray arr = obj.getJSONObject("data").getJSONArray("categories");
			return R.ok(arr, "获取成功");
		}
	}

	@Override
	public R getAwemeAuthor(String advertiserId, String categoryId,String behaviors){
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", advertiserId);
		data.put("category_id", categoryId);
		data.put("behaviors", behaviors.split(","));

		String token = ttAccesstokenService.fetchAccesstoken(advertiserId);		// this.getAccesstoken(advertiserId);
		//String accessToken = "3e09cc34a5812083ad9bd2f9eca6659fe2aadaa6";

		//调用头条接口
		String resultStr = OEHttpUtils.doGet(toutiaoToolsUrl + "aweme_category_top_author/get/", data, token);

		//结果为null，当做失败
		if (StringUtils.isBlank(resultStr))
			return R.failed("从toutiao获取失败:resultStr is blank");

		JSONObject obj = JSON.parseObject(resultStr);
		if (!obj.getInteger("code").equals(0)){	//同步失败
			return R.failed("从toutiao获取失败:" + obj.getString("message"));
		}else{
			JSONArray arr = obj.getJSONObject("data").getJSONArray("authors");
			return R.ok(arr, "获取成功");
		}
	}


	/*private String getAccesstoken(String advertiserId){
		AdvertiserInfo advertiserInfo=  advertiserMapper.selectAdvertiserByAdAccount(advertiserId);
		String housekeeper = advertiserInfo.getHousekeeper();

		//根据housekeeper获取accessToken
		String tokenContent = stringRedisTemplate.opsForValue().get(Constants.OCEANENGINE_REDIS_TOKEN_KEY_PRIX_ + housekeeper);
		Accesstoken accessToken = JSON.parseObject(tokenContent, Accesstoken.class);
		String token = accessToken.getAccessToken();

		token = "d93a80eb230e78a9de7eff1236232b5a21b2ea37";
		return token;
	}*/
}
