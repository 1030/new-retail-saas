/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.TtInterestActionService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @ 广告账户 前端控制器
 * @author john
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/tt_interest_action")
@Api(value = "tt_interest_action", tags = "")
public class TtInterestActionController {

	private final TtInterestActionService ttInterestActionService;


	@RequestMapping("/action/category")
	public R getActionCategory(Integer actionDays, String actionScene) {
		return ttInterestActionService.getActionCategory(actionDays, actionScene);
	}

	@RequestMapping("/action/keyword")
	public R getActionKeyword(Integer actionDays, String actionScene) {
		return ttInterestActionService.getActionKeyword(actionDays, actionScene);
	}

	@RequestMapping("/interest/category")
	public R getInterestCategory() {
		return ttInterestActionService.getInterestCategory();
	}

	@RequestMapping("/interest/keyword")
	public R getInterestKeyword(String categoryId,String behaviors) {
		return ttInterestActionService.getInterestKeyword();
	}

}
