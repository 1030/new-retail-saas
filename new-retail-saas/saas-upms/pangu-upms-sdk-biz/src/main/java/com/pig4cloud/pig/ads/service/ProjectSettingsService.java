package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.ProjectSettingsDto;
import com.pig4cloud.pig.api.entity.ProjectSettings;
import com.pig4cloud.pig.api.vo.ProjectSettingsVo;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @description:
 * @author: nml
 * @time: 2020/11/4 13:35
 **/

public interface ProjectSettingsService extends IService<ProjectSettings> {

	//查询所有，包括资源数
	IPage<ProjectSettingsDto> selectAll(ProjectSettingsVo req);

	//修改
	R update(ProjectSettings req);

	//新增
	R add(ProjectSettings req);

}
