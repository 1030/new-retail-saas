package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.AdPromotionService;
import com.pig4cloud.pig.api.vo.AdPromotionReq;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 广告表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:21:02
 * table: ad_promotion
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/adPromotion")
public class AdPromotionController {
	
    private final AdPromotionService adPromotionService;
	
	@RequestMapping("/list")
	public R list(AdPromotionReq record){
		return R.ok();
	}
}


