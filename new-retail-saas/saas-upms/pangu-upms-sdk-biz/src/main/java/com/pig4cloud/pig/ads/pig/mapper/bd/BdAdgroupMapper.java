package com.pig4cloud.pig.ads.pig.mapper.bd;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.bd.BdAdgroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 百度推广单元表(bd_adgroup)数据Mapper
 *
 * @author zjz
 * @since 2022-12-27 18:35:43
 * @description 由 zjz 创建
*/
@Mapper
public interface BdAdgroupMapper extends BaseMapper<BdAdgroup> {

    List<BdAdgroup> getListByAdgroupIds(@Param("list") List<Long> list);

}
