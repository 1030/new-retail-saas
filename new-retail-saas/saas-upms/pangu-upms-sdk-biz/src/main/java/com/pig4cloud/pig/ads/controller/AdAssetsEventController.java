package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.AdAssetsEventService;
import com.pig4cloud.pig.api.vo.AdAssetsEventReq;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * 广告资产事件表
 * @author  chenxiang
 * @version  2022-04-01 16:25:19
 * table: ad_assets_event
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/adAssetsEvent")
public class AdAssetsEventController {
	
    private final AdAssetsEventService adAssetsEventService;

	/**
	 * 获取可创建事件列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/getList")
	public R getList(@RequestBody AdAssetsEventReq req){
		if (StringUtils.isBlank(req.getAdvertiserId())){
			return R.failed("未获取到广告账户");
		}
		if (StringUtils.isBlank(req.getAssetId())){
			return R.failed("未获取到资产ID");
		}
		return adAssetsEventService.getList(req);
	}
	/**
	 * 获取资产下已经创建的事件
	 * @param req
	 * @return
	 */
	@RequestMapping("/getEventList")
	public R getEventList(@RequestBody AdAssetsEventReq req){
		if (StringUtils.isBlank(req.getAdvertiserId())){
			return R.failed("未获取到广告账户");
		}
		if (StringUtils.isBlank(req.getAssetId())){
			return R.failed("未获取到资产ID");
		}
		return adAssetsEventService.getEventList(req);
	}
	/**
	 * 创建事件
	 * @param req
	 * @return
	 */
	@RequestMapping("/addEvent")
	public R addEvent(@RequestBody AdAssetsEventReq req){
		if (StringUtils.isBlank(req.getAdvertiserId())){
			return R.failed("未获取到广告账户");
		}
		if (StringUtils.isBlank(req.getAssetId())){
			return R.failed("未获取到资产ID");
		}
		if (Objects.isNull(req.getEvent())){
			return R.failed("未获取到事件JSON数据");
		}
		return adAssetsEventService.addEvent(req);
	}
}


