package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.AdAccountWaterService;
import com.pig4cloud.pig.api.dto.AdAccountWaterDto;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Objects;

/**
 * @ClassName TtAccountWaterController
 * @Description done
 * @Author nieml
 * @Time 2021/7/14 20:15
 * @Version 1.0
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/adAccountWater")
@Api(value = "adAccountWater", tags = "账户资金流水")
@Slf4j
public class AdAccountWaterController {

	@Autowired
	private AdAccountWaterService adAccountWaterService;

	/**
	 * 分页列表
	 *
	 * @param req
	 * @return
	 */
	@GetMapping("/page")
	public R getPage(AdAccountWaterDto req) {

		try {
			if (Objects.isNull(req.getAccountType())){
				throw new Exception("广告账户类型不能为空");
			}
			if (Objects.isNull(req.getAccountId())){
				throw new Exception("广告账户不能为空");
			}
			if (Objects.isNull(req.getStartDate())){
				throw new Exception("开始日期不能为空");
			}
			if (Objects.isNull(req.getEndDate())){
				throw new Exception("结束日期不能为空");
			}
		} catch (Exception e) {
			log.error("getPage数据校验:[{}]",e);
			return R.failed(e.getMessage());
		}
		return adAccountWaterService.queryAdAccountWaterPage(req);
	}

}
