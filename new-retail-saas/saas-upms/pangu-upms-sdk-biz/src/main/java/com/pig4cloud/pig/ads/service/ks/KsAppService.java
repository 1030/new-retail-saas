package com.pig4cloud.pig.ads.service.ks;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.KsApp;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 快手应用表
 * @author  chenxiang
 * @version  2022-03-26 11:17:47
 * table: ks_app
 */
public interface KsAppService extends IService<KsApp> {
	/**
	 * 快手创建应用
	 * @param app
	 * @return
	 */
	R create(KsApp app);
}


