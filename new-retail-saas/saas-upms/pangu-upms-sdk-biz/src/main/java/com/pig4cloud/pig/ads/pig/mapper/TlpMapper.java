package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.ThirdLandingPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * @author yk
 */
@Component
@Mapper
public interface TlpMapper extends BaseMapper<ThirdLandingPage> {

	ThirdLandingPage selectByPrimaryKey(@Param("id") Integer id);

	int deleteByPrimaryKey(@Param("id") Integer id);

	int insert(ThirdLandingPage record);

	int insertSelective(ThirdLandingPage record);

	int updateByPrimaryKey(ThirdLandingPage record);

	int updateByPrimaryKeySelective(ThirdLandingPage record);
}
