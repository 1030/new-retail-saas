package com.pig4cloud.pig.ads.pig.mapper.ks;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.KsAccesstoken;
import org.apache.ibatis.annotations.Mapper;

/**
 * 快手授权token表
 * @author  chenxiang
 * @version  2022-03-22 20:29:09
 * table: ks_accesstoken
 */
@Mapper
public interface KsAccesstokenMapper extends BaseMapper<KsAccesstoken> {
}


