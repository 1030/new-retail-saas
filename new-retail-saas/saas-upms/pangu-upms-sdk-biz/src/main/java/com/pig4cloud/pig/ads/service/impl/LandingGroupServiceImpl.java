package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.LandingGroupMapper;
import com.pig4cloud.pig.ads.service.LandingGroupService;
import com.pig4cloud.pig.api.entity.LandingGroup;
import org.springframework.stereotype.Service;


/**
 * @落地页 服务实现类
 * @author yk
 */
@Service
public class LandingGroupServiceImpl extends ServiceImpl<LandingGroupMapper, LandingGroup> implements LandingGroupService {



}
