package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.ToutiaoDistrict;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ToutiaoDistrictMapper extends BaseMapper<ToutiaoDistrict> {
    int updateByPrimaryKeySelective(ToutiaoDistrict record);

	List<ToutiaoDistrict> listByParentId(@Param("parentId") Integer parentId, @Param("level") Integer level);

	List<ToutiaoDistrict> provinceCapital(@Param("provinceId") Integer provinceId);

	List<ToutiaoDistrict> searchCity(@Param("searchStr") String  searchStr);

	List<ToutiaoDistrict> citiesByLevel(@Param("level") Integer level);

	List<ToutiaoDistrict> nameByIds(@Param("ids") String ids);

	List<ToutiaoDistrict> allEntities();
}