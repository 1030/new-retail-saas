package com.pig4cloud.pig.ads.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.pig4cloud.pig.ads.service.AdTitleLibService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.api.entity.AdTitleLib;
import com.pig4cloud.pig.api.entity.ResponseBean;
import com.pig4cloud.pig.api.vo.AdTitleLibVo;
import com.pig4cloud.pig.api.vo.AdTitleLibVo.Create;
import com.pig4cloud.pig.api.vo.AdTitleLibVo.Delete;
import com.pig4cloud.pig.api.vo.AdTitleLibVo.Update;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: nml
 * @time: 2020/11/7 18:22
 **/
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/adtitle")
@Api(value = "adtitle", tags = "标题库管理模块")
public class AdTitleLibController {

	private final AdTitleLibService adTitleLibService;

	/**
	 * 根据条件查询标题列表
	 *
	 * @param atl
	 * @return
	 */
	@PostMapping("/list")
	public R<List<AdTitleLib>> titleList(@RequestBody @Validated AdTitleLibVo atl) {
		List<AdTitleLib> titleLibList = adTitleLibService.titleTitleLibList(atl);
		return R.ok(titleLibList);
	}

	/**
	 * 根据条件查询标题分页列表
	 *
	 * @param atl
	 * @return
	 */
	@SysLog("文案库列表")
	@PostMapping("/page")
	public R<IPage<AdTitleLib>> titlePage(@RequestBody @Validated AdTitleLibVo atl) {
		IPage<AdTitleLib> titleLibList = adTitleLibService.titleTitleLibPage(atl, atl);
		return R.ok(titleLibList);
	}

	/**
	 * 查询头条创意词列表
	 *
	 * @return
	 */
	@SysLog("头条创意词列表")
	@GetMapping("/ttCreativeWordList")
	public R<Object> ttCreativeWordList(@RequestParam(value = "advertiserId",required = false) String advertiserId) {
		try {
			ResponseBean responseBean = adTitleLibService.ttCreativeWordList(advertiserId);
			if ("0".equals(responseBean.getCode())) {
				return R.ok(responseBean.getData());
			} else {
				return R.failed(responseBean.getCode() + " - " + responseBean.getMessage());
			}
		} catch (Exception e) {
			log.error("获取创意词失败", e);
			return R.failed("获取创意词失败");
		}
	}

	/**
	 * 根据主键获取标题信息
	 *
	 * @param titleId
	 * @return
	 */
	@SysLog("文案库标题详情")
	@GetMapping("/get")
	public R<AdTitleLib> getTitle(@RequestParam Long titleId) {
		AdTitleLib adTitleLib = adTitleLibService.getById(titleId);
		return R.ok(adTitleLib);
	}

	/**
	 * 创建标题
	 *
	 * @param atl
	 * @return
	 */
	@SysLog("创建标题")
	@PostMapping("/create")
	public R<Void> create(@RequestBody @Validated(Create.class) AdTitleLibVo atl) {
		try {
			adTitleLibService.createTitleLib(atl);
			return R.ok();
		} catch (Exception e) {
			log.error("保存标题信息异常", e);
			return R.failed(null, e.getMessage());
		}
	}

	/**
	 * 修改标题信息
	 *
	 * @param atl
	 * @return
	 */
	@SysLog("修改标题信息")
	@PostMapping("/update")
	public R<Void> update(@RequestBody @Validated(Update.class) AdTitleLibVo atl) {
		try {
			adTitleLibService.updateTitleLib(atl);
			return R.ok();
		} catch (Exception e) {
			log.error("修改标题信息异常", e);
			return R.failed(null, e.getMessage());
		}
	}

	/**
	 * 批量删除标题信息
	 *
	 * @param atl
	 * @return
	 */
	@SysLog("删除标题")
	@PostMapping("/delete")
	public R<Void> delete(@RequestBody @Validated(Delete.class) AdTitleLibVo atl) {
		Set<Long> titleIds = Arrays.stream(atl.getIdArr().split(",")).map(Long::valueOf).collect(Collectors.toSet());
		if (titleIds.isEmpty()) {
			return R.failed("请提供要删除的资质文件ID");
		}
		adTitleLibService.update(Wrappers.<AdTitleLib>lambdaUpdate().set(AdTitleLib::getIsdelete, 1).in(AdTitleLib::getId, titleIds));
		return R.ok();
	}


	//分页模糊查询所有
	@Deprecated
	@RequestMapping("/page1")
	public R page1(AdTitleLibVo req) {
		return adTitleLibService.findPage(req);
	}

	@Deprecated
	@RequestMapping("/adPage")
	public R adPage(AdTitleLibVo req) {
		return adTitleLibService.findAdPage(req);
	}

	@Deprecated
	@RequestMapping("/adList")
	public R adList(AdTitleLibVo req) {
		return adTitleLibService.findAdList(req);
	}

	@Deprecated
	@RequestMapping("/del")
	public R del(AdTitleLib req) {
		return adTitleLibService.del(req);
	}

	@Deprecated
	@RequestMapping("/delBatch")
	public R delBatch(AdTitleLibVo req) {
		return adTitleLibService.delBatch(req);
	}

	@Deprecated
	@RequestMapping("/modify")
	public R modify(AdTitleLib req) {
		return adTitleLibService.modify(req);
	}

	@Deprecated
	@RequestMapping("/save")
	public R save(@RequestBody String adTitleLib) {
		if (StringUtils.isBlank(adTitleLib)) {
			return R.failed("标题参数不能为空");
		}
		List<AdTitleLib> adTitleLibs = JSON.parseArray(JSON.parseObject(adTitleLib).getString("adTitleLib"), AdTitleLib.class);
		return adTitleLibService.add(adTitleLibs);
	}

	@Deprecated
	@RequestMapping("/findById")
	public R findById(AdTitleLib req) {
		Long id = req.getId();
		AdTitleLib adTitleLib = null;
		try {
			adTitleLib = adTitleLibService.getById(id);
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed("查询失败");
		}
		return R.ok(adTitleLib, "查询成功");
	}

	//find all Dynamic word pack
	@Deprecated
	@RequestMapping("/allWord")
	public ResponseBean findAllDynamicWordPack(AdTitleLibVo req) {
		return adTitleLibService.findAllDynamicWordPack(req);
	}

}
