package com.pig4cloud.pig.ads.gdt.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.dto.FilteringDto;
import com.pig4cloud.pig.api.gdt.dto.GdtCampaignDto;
import com.pig4cloud.pig.api.gdt.dto.GdtCreateCampaignDto;
import com.pig4cloud.pig.api.gdt.dto.GdtUpdateCampaignDto;
import com.pig4cloud.pig.api.gdt.entity.GdtCampaign;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.api.gdt.vo.GdtCampaignVo;

import java.util.List;
import java.util.Map;

/**
 * 广点通-推广计划 服务层
 *
 * @author hma
 * @date 2020-12-03
 */
public interface IGdtCampaignService extends IService<GdtCampaign> {

	/**
	 * 创建推广计划-推广计划信息
	 *
	 * @param createCampaignDto 广点通
	 * @return 广点通-推广计划信息
	 */
	R createGdtCampaign(GdtCreateCampaignDto createCampaignDto);


	/**
	 * 根据条件查询推广计划列表
	 *
	 * @param gdtCampaign
	 * @return
	 */
	public List<GdtCampaign> gdtCampaignList(GdtCampaign gdtCampaign);

	/**
	 * 获取已推广的 广告计划
	 *
	 * @param gtdCampaignDto
	 * @return
	 */
	R getGtdCampaign(GdtCampaignDto gtdCampaignDto);

	/**
	 * 获取推广目标
	 *
	 * @param gtdCampaignDto
	 * @return
	 */
	R getGtdCampaignPromote(GdtCampaignDto gtdCampaignDto);


	/**
	 * 调用广点通三方拉取列表接口
	 *
	 * @return
	 */
	 List<JSONArray> getThirdResponse(String accountId, String methodName, String interfaceUrl, List<FilteringDto> filteringDtos,Map<String,String> paramMap, String fields,Boolean isPage);


	/**
	 * 调用广点通创建接口(或者post 请求查询接口)
	 *
	 * @return
	 */
	 JSONObject createOrGetPostThirdResponse(String accountId, String methodName, String interfaceUrl, Map<String, Object> data);


	/**
	 * 修改推广计划信息
	 *
	 * @param gdtCampaignDto
	 * @return
	 */
	R updateGtdCampaignInfo(GdtUpdateCampaignDto gdtCampaignDto);


	/**
	 * 查询推广计划模板
	 * @param gdtCampaignDto
	 * @return
	 */
	public R getGtdCampaignTemplate(GdtUpdateCampaignDto gdtCampaignDto);


	/**
	 * 修改广点通-推广计划
	 * @param req 广点通-推广计划信息
	 * @return 结果
	 */
	int update(GdtCampaignVo req);


}
