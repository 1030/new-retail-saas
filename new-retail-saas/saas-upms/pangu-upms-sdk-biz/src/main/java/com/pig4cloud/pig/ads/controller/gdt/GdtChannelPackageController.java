package com.pig4cloud.pig.ads.controller.gdt;

import com.pig4cloud.pig.ads.gdt.service.GdtChannelPackageService;
import com.pig4cloud.pig.api.entity.GdtChannelPackage;
import com.pig4cloud.pig.api.vo.GdtChannelPackageReq;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName GdtChannelPackageController.java
 * @createTime 2021年07月12日 20:02:00
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/gdt/channelPackage")
@Api(value = "/gdt/channelPackage", tags = "广点通渠道包")
public class GdtChannelPackageController {
	private final GdtChannelPackageService gdtChannelPackageService;

	/**
	 * 上传渠道包
	 * @param record
	 * @return
	 */
	@Inner
	@RequestMapping("/create")
	public R create(@RequestBody GdtChannelPackage record) {
		return gdtChannelPackageService.create(record);
	}
	/**
	 * 获取渠道包列表
	 * @param record
	 * @return
	 */
	@RequestMapping("/getList")
	public R getList(@RequestBody GdtChannelPackage record) {
		return gdtChannelPackageService.getList(record);
	}
	/**
	 * 根据advertiser_monitor_info主键获取列表
	 * @param record
	 * @return
	 */
	@Inner
	@RequestMapping("/getPackageList")
	public R getPackageList(@RequestBody GdtChannelPackageReq record) {
		return gdtChannelPackageService.getPackageList(record);
	}

	/**
	 * 上传渠道包
	 * @param record
	 * @return
	 */
	@Inner
	@RequestMapping("/extendPackageAdd")
	public R extendPackageAdd(@RequestBody GdtChannelPackageReq record) {
		if (StringUtils.isBlank(record.getAdAccount())){
			return R.failed("未获取到广告账户");
		}
		if (StringUtils.isBlank(record.getUnionAppId())){
			return R.failed("未获取到AppId");
		}
		if (CollectionUtils.isEmpty(record.getAppChlList())){
			return R.failed("未获取到分包渠道标识");
		}
		return gdtChannelPackageService.extendPackage(record);
	}
}
