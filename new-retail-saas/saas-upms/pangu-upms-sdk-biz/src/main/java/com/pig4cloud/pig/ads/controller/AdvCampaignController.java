/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pig4cloud.pig.ads.controller;
import com.pig4cloud.pig.api.dto.AdvertiserDto;
import com.pig4cloud.pig.api.dto.BudgetDto;
import com.pig4cloud.pig.api.dto.CampaignDto;
import com.pig4cloud.pig.api.entity.AdCampaign;
import com.pig4cloud.pig.ads.service.AdvCampaignService;
import com.pig4cloud.pig.ads.service.AdvService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.api.enums.StatusEnum;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @ 广告推广组管理模块 前端控制器
 * @author hma
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/campaign")
@Api(value = "campaign", tags = "广告推广组管理模块")
public class AdvCampaignController {
	@Resource
	private AdvCampaignService advCampaignService;
	@Resource
	private AdvService advService;


	@SysLog("latestOne")
	@RequestMapping("/latestOne")
	public R findLatestOne(CampaignDto req) {
		return advCampaignService.findLatestOne(req);
	}

	/**
	 * 分页模糊查询所有广告组
	 * @param req
	 * @return
	 */
	@SysLog("page")
	@RequestMapping("/page")
	public R findPage(CampaignDto req) {
		return advCampaignService.findPage(req);
	}

	@SysLog("create")
	@RequestMapping(value = "/create")
	public R create(@RequestBody AdCampaign req) {
		return advCampaignService.createAdvCampaign(req);
	}

	/**
	 * 分页查询广告推广组信息
	 * @param campaignDto 分页对象
	 * @return 分页对象
	 */
	@SysLog("getAdvCampaignStatisticPage")
	@RequestMapping("/getAdvCampaignStatisticPage")
	public R getAdvCampaignStatisticPage(@RequestBody CampaignDto campaignDto) {
         return  advCampaignService.getAdvCampaignStatistic(campaignDto);
	}


	/**
	 * 修改广告推广组状态
	 * @param planDto 修改广告计划状态
	 * @return
	 */
	@RequestMapping("/updateCampaignStatus")
	@SysLog("updateCampaignStatus")
	public R updateCampaignStatus(AdvertiserDto planDto) {
		return  advCampaignService.updateCampaignStatus(planDto);
	}




	/**
	 * 修改广告推广组预算（定时修改）
	 * @param budgetDto
	 * @return
	 */
	@RequestMapping("/updateCampaignBudget")
	@SysLog("updateCampaignBudget")
	public R updateCampaignBudget(BudgetDto budgetDto) {
		if (null == budgetDto) {
			return R.failed( "param is empty");
		}
		String advertiserId=budgetDto.getAdvertiserId();
		if(StringUtils.isBlank(advertiserId)){
			return R.failed( "advertiserId is empty");
		}
		if(null == budgetDto.getCampaignId()){
			return R.failed( "campaignId is empty");
		}
		if(null == budgetDto.getType()){
			budgetDto.setType(StatusEnum.TYPE_ONE.getStatus());
		}
		return  advCampaignService.budgetUpdate(budgetDto, StatusEnum.OPERATE_TWO.getStatus());
	}

	/**
	 * 查询推广组预约当天定时设置预算值
	 * @param  campaignId
	 * @return success/false
	 */
	@SysLog("查询该账户设置的日预算")
	@RequestMapping("/getCampaignBudget")
	public R getCampaignBudget(Long campaignId) {
		return advService.getAdvertiserJobBudget(null,campaignId,null, StatusEnum.OPERATE_TYPE_TWO.getStatus());
	}




}
