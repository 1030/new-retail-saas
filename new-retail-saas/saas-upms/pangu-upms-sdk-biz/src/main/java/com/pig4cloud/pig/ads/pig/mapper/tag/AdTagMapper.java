package com.pig4cloud.pig.ads.pig.mapper.tag;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.entity.tag.AdTag;
import com.pig4cloud.pig.api.entity.tag.AdTagDto;
import com.pig4cloud.pig.api.entity.tag.AdTagList;
import com.pig4cloud.pig.api.entity.tag.AdTagVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 标签表(ad_tag)数据Mapper
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
*/
@Mapper
public interface AdTagMapper extends BaseMapper<AdTag> {


	IPage<AdTagVo> pageList(AdTagDto adTagDto);

	List<AdTagList> tagList(@Param(value = "tagName") String tagName);

	List<AdTagList> tagListByName(@Param(value = "tagName") String tagName);

	List<AdTagList> tagListByIds(@Param(value = "list") List<Long> list);
}
