package com.pig4cloud.pig.ads.task;

import com.pig4cloud.pig.ads.service.AdAccountWarningResultService;
import com.pig4cloud.pig.common.core.util.R;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description 统计不活跃广告账户并给指定用户发送邮件
 * @Author chengang
 * @Date 2021/7/15
 */
@Component
public class NoActiveAccountSendMailJob {
	private static Logger logger = LoggerFactory.getLogger(NoActiveAccountSendMailJob.class);

	@Autowired
	private AdAccountWarningResultService adAccountWarningResultService;


	@XxlJob("noActiveAccountSendMailJob")
	public ReturnT<String> noActiveAccountSendMailJob(String param) {
		try {
			XxlJobLogger.log("================开始执行不活跃广告账户邮件通知========================");
			R r = adAccountWarningResultService.noActiveAccountSendMail();
			if (r.getCode() == 0) {
				XxlJobLogger.log("不活跃广告账户邮件通知，已执行完成--{}", r.getMsg());
			} else {
				XxlJobLogger.log("不活跃广告账户邮件通知，执行失败--{}",r.getMsg());
			}
			XxlJobLogger.log("================执行结束不活跃广告账户邮件通知========================");
		} catch (Throwable e) {
			logger.error("不活跃广告账户邮件通知",e);
			return ReturnT.FAIL;
		}
		return ReturnT.SUCCESS;
	}
}
