package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.pig4cloud.pig.ads.pig.mapper.AdOauthSettingMapper;
import com.pig4cloud.pig.ads.service.AdOauthSettingService;
import com.pig4cloud.pig.api.entity.AdOauthSetting;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 媒体授权信息配置表
 * @author  chengang
 * @version  2022-12-13 14:32:04
 * table: ad_oauth_setting
 */
@Log4j2
@Service("adOauthSettingService")
@RequiredArgsConstructor
public class AdOauthSettingServiceImpl extends ServiceImpl<AdOauthSettingMapper, AdOauthSetting> implements AdOauthSettingService {

	/**
	 * 根据媒体类型获取授权配置
	 * @param mediaCode
	 * @return
	 */
	@Override
	public AdOauthSetting getOauthSetting(String mediaCode){
		//获取所有回调配置
		List<AdOauthSetting> oauthSettingList = this.list(Wrappers.<AdOauthSetting>lambdaQuery().eq(AdOauthSetting::getDeleted,0));
		if (CollectionUtils.isEmpty(oauthSettingList)) {
			return null;
		}
		Map<String,AdOauthSetting> oauthSettingMap = oauthSettingList.stream()
				.collect(Collectors.toMap(AdOauthSetting::getMediaCode, Function.identity()));
		AdOauthSetting setting = oauthSettingMap.get(mediaCode);
		return setting;
	}
	
}


