package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.dto.AdPlanTempDto;
import com.pig4cloud.pig.api.entity.AdPlanTemp;
import com.pig4cloud.pig.api.vo.AdPlanTempVo;

import java.util.List;

/**
 * 广告计划临时 数据层
 * 
 * @author hma
 * @date 2020-11-18
 */
public interface AdPlanTempMapper   extends BaseMapper<AdPlanTemp>
{

//	/**
//     * 查询广告计划临时信息
//     *
//     * @param id 广告计划临时ID
//     * @return 广告计划临时信息
//     */
//	public AdPlanTemp selectAdPlanTempById(Long id);
//
//	/**
//     * 查询广告计划临时列表
//     *
//     * @param adPlanTemp 广告计划临时信息
//     * @return 广告计划临时集合
//     */
//	public List<AdPlanTemp> selectAdPlanTempList(AdPlanTemp adPlanTemp);
//
//	/**
//     * 新增广告计划临时
//     *
//     * @param adPlanTemp 广告计划临时信息
//     * @return 结果
//     */
//	public int insertAdPlanTemp(AdPlanTemp adPlanTemp);
//
//	/**
//     * 修改广告计划临时
//     *
//     * @param adPlanTemp 广告计划临时信息
//     * @return 结果
//     */
//	public int updateAdPlanTemp(AdPlanTemp adPlanTemp);
//
//	int updateAdPlanTempAll(AdPlanTemp adPlanTemp);
//
//	/**
//     * 批量添加广告计划临时
//     *
//     * @param adPlanTempList 需要添加的数据集合
//     * @return 结果
//     */
//	public int batchInsertAdPlanTemp(List<AdPlanTemp> adPlanTempList);




	/**
	 *获取用户某个广告账号，广告组添加的广告计划数据（模板）
	 * @param adPlanTempDto
	 */
	List<AdPlanTempVo> getUserPlanTemp(AdPlanTempDto adPlanTempDto);

	/**
	 *获取用户某个广告账号，广告组添加的广告计划数据（模板）
	 * @param adPlanTempDto
	 */
	AdPlanTempVo getUserPlanTempById(AdPlanTempDto adPlanTempDto);

	AdPlanTempVo getUserPlanTempOne(AdPlanTempDto adPlanTempDto);
	
}