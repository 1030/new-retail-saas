package com.pig4cloud.pig.ads.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;


/**
 * @Description
 * @Author chengang
 * @Date 2021/12/27
 */
@RefreshScope
@Configuration
@Data
public class SdkProperties {

	@Value(value = "${refresh.demo:defaultValue}")
	private String refreshDemo;

	@Value(value = "${advertiser_list_url:https://ad.oceanengine.com/open_api/2/majordomo/advertiser/select/}")
	private String advertiserListUrl;

	@Value(value = "${ad_industry_list_url:https://ad.oceanengine.com/open_api/2/tools/industry/get/}")
	private String adIndustryListUrl;

	@Value("${dongxin_kafka_topic}")
	private String dongxinKafkaTopic;


	/**
	 * 快手相关配置
	 */
	@Value(value = "${ks.auth.url}")
	private String ksAuthUrl;

	@Value(value = "${ks.auth.redirect_uri}")
	private String ksRedirectUri;

	@Value(value = "${ks.auth.oauth_type}")
	private String ksOauthType;

	@Value(value = "${ks.auth.scope}")
	private String ksScope;

	@Value(value = "${ks.auth.app_id}")
	private String ksAppId;

	@Value(value = "${ks.auth.secret}")
	private String ksSecret;

	// app 隐私政策链接
	@Value(value = "${ks.api.app_privacy_url}")
	private String appPrivacyUrl;



	@Value(value = "${gdt.configured.status}")
	private String configuredStatus;

	@Value(value = "${gdt_extend_package_add:extend_package/add}")
	private String gdtExtendPackageAdd;

	@Value(value = "${gdt_extend_package_update:extend_package/update}")
	private String gdtExtendPackageUpdate;

	@Value(value = "${ad_plan_limit:200}")
	private Integer adPlanLimit;

	@Value(value = "${tag_limit:3}")
	private Integer tagLimit;
}
