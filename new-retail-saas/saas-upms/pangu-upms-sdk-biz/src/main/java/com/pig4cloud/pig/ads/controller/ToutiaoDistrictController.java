/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.ToutiaoDistrictService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @ 广告账户 前端控制器
 * @author john
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/tt_district")
@Api(value = "tt_district", tags = "头条地域")
public class ToutiaoDistrictController {

	private final ToutiaoDistrictService toutiaoDistrictService;

	@RequestMapping("/provice_list")
	public R proviceList() {
		return R.ok(toutiaoDistrictService.listByParentId(0, 1));
	}

	@RequestMapping("/city_list")
	public R citysByProvice(Integer provinceId) {
		return R.ok(toutiaoDistrictService.listByParentId(provinceId, 2));
	}

	@RequestMapping("/county_list")
	public R countysByCity(Integer cityId) {
		return R.ok(toutiaoDistrictService.listByParentId(cityId, 3));
	}

	@RequestMapping("/biz_district_list")
	public R bizDistrinctList(Integer cityId) {
		return R.ok(toutiaoDistrictService.listByParentId(cityId, 4));

	}

	@RequestMapping("/province_capital")
	public R provinceCapital(Integer provinceId) {
		return R.ok(toutiaoDistrictService.provinceCapital(provinceId));
	}


	//按名称搜索 省份/城市
	@RequestMapping("/search_city")
	public R searchCity(String  searchStr) {
		return R.ok(toutiaoDistrictService.searchCity(searchStr));
	}

	//按照级别获取城市列表
	@RequestMapping("/cities_by_level")
	public R citiesByLevel(Integer level) {
		return R.ok(toutiaoDistrictService.citiesByLevel(level));
	}

	//按照级别获取城市列表
	@RequestMapping("/name_by_id")
	public R nameByIds(String ids) {
		return R.ok(toutiaoDistrictService.nameByIds(ids));
	}


	//
	@RequestMapping("/cities")
	public R cities(String ids) {
		return toutiaoDistrictService.cities();
	}

	@RequestMapping("/level_cities")
	public R levelCities(String ids) {
		return toutiaoDistrictService.levelCityes();
	}

	@RequestMapping("/counties")
	public R counties(String ids) {
		return toutiaoDistrictService.counties();
	}

	@RequestMapping("/business_district")
	public R businessDistrict(String ids) {
		return toutiaoDistrictService.businessDistrict();
	}

	@RequestMapping("/generateData")
	public R generateData(){
		return toutiaoDistrictService.generateData();
	}

}
