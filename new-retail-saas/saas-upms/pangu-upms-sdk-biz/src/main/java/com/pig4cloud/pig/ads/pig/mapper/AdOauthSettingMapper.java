package com.pig4cloud.pig.ads.pig.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdOauthSetting;
import org.apache.ibatis.annotations.Mapper;

/**
 * 媒体授权信息配置表
 * @author  chengang
 * @version  2022-12-13 14:32:04
 * table: ad_oauth_setting
 */
@Mapper
public interface AdOauthSettingMapper extends BaseMapper<AdOauthSetting> {

}


