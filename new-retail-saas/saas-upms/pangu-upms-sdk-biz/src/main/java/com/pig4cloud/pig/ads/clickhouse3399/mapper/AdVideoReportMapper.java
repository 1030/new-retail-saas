package com.pig4cloud.pig.ads.clickhouse3399.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.dto.AdVideoReport;
import com.pig4cloud.pig.api.dto.AdVideoReportData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdVideoReportMapper.java
 * @createTime 2020年11月12日 17:42:00
 */
@Mapper
public interface AdVideoReportMapper extends BaseMapper<AdVideoReport> {
	@SqlParser(filter=true)
	List<AdVideoReport> selectTtVideoReport(Map<String,Object> param);
	@SqlParser(filter=true)
	List<AdVideoReport> selectGdtVideoReport(Map<String,Object> param);
	@SqlParser(filter=true)
	AdVideoReportData selectTtVideoReportData(Map<String,Object> param);
	@SqlParser(filter=true)
	AdVideoReportData selectGdtVideoReportData(Map<String,Object> param);
	@SqlParser(filter=true)
	List<String> selectGdtAdCreativeByList(Map<String,Object> param);
}
