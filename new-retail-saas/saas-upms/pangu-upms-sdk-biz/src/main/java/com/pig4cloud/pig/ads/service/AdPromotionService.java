package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdPromotion;

/**
 * 广告表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:21:02
 * table: ad_promotion
 */
public interface AdPromotionService extends IService<AdPromotion> {
}


