package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdCreative;

import java.util.List;


/**
 * 广告创意 数据层
 * 
 * @author hma
 * @date 2020-11-10
 */
public interface AdCreativeMapper extends BaseMapper<AdCreative>
{

	/**
	 * 修改广创意--根据创意id
	 * @param adCreative
	 * @return
     */
	Integer updateAdCreative(AdCreative adCreative);

	/**
	 * 批量添加广告创意数据
	 *
	 * @param adCreativeList 需要添加的数据集合
	 * @return 结果
	 */
	int batchInsertAdCreative(List<AdCreative> adCreativeList);

	
}