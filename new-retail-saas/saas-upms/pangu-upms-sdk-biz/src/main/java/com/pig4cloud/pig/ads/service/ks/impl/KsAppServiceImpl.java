package com.pig4cloud.pig.ads.service.ks.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.sdk.client.DyKsClient;
import com.dy.sdk.model.request.ks.KsAppCreateRequest;
import com.dy.sdk.model.response.ks.KsResponse;
import com.pig4cloud.pig.ads.config.SdkProperties;
import com.pig4cloud.pig.ads.pig.mapper.ks.KsAppMapper;
import com.pig4cloud.pig.ads.service.AdvertiserService;
import com.pig4cloud.pig.ads.service.ks.KsAccesstokenService;
import com.pig4cloud.pig.ads.service.ks.KsAppService;
import com.pig4cloud.pig.api.entity.AccountToken;
import com.pig4cloud.pig.api.entity.KsAccountToken;
import com.pig4cloud.pig.api.entity.KsApp;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

/**
 * 快手应用表
 * @author  chenxiang
 * @version  2022-03-26 11:17:47
 * table: ks_app
 */
@Log4j2
@Service("ksAppService")
@RequiredArgsConstructor
public class KsAppServiceImpl extends ServiceImpl<KsAppMapper, KsApp> implements KsAppService {

	private final SdkProperties sdkProperties;
	private final AdvertiserService advertiserService;

	/**
	 * 快手创建应用
	 * @param app
	 * @return
	 */
	@Override
	public R<KsApp> create(KsApp app){
		try {
			AccountToken ksAccountToken = advertiserService.getAdvertiserToken(PlatformTypeEnum.KS.getValue(),app.getAdvertiserId());
			if (Objects.isNull(ksAccountToken)){
				return R.failed("此广告账户Token已失效");
			}

			DyKsClient ksClient = new DyKsClient("");
			// app 隐私政策链接
			String appPrivacyUrl = sdkProperties.getAppPrivacyUrl();
			app.setAppPrivacyUrl(appPrivacyUrl);
			// 请求快手创建应用
			KsAppCreateRequest appCreateRequest = new KsAppCreateRequest();
			appCreateRequest.setAdvertiser_id(app.getAdvertiserId());
			appCreateRequest.setApp_version(app.getAppVersion());
			appCreateRequest.setApp_name(app.getAppName());
			appCreateRequest.setImage_token(app.getImageToken());
			appCreateRequest.setPackage_name(app.getPackageName());
			appCreateRequest.setPlatform(1);
			appCreateRequest.setKsUrl(app.getUrl());
			appCreateRequest.setUse_sdk(1);
			appCreateRequest.setApp_privacy_url(appPrivacyUrl);
			// 调用快手创建应用
			String result = ksClient.call4Form(appCreateRequest, ksAccountToken.getAccessToken());
			log.error(">>>>>>快手创建应用异常："+ result);
			KsResponse response = JSON.parseObject(result, KsResponse.class);
			if ("0".equals(response.getCode())){
				JSONObject jsonObject = response.getData();
				app.setAppId(jsonObject.getLong("app_id"));
				app.setUrl(jsonObject.getString("url"));
				// 保存
				this.save(app);
				return R.ok(app);
			}else {
				return R.failed(response.getMessage());
			}
		} catch (IOException e) {
			log.error(">>>>>>快手创建应用异常："+ e.getMessage());
			e.printStackTrace();
		}
		return R.failed();
	}
}


