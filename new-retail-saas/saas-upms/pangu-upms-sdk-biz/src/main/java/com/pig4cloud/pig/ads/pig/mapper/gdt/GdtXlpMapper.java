package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtXlp;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author yk
 */
@Component
@Mapper
public interface GdtXlpMapper extends BaseMapper<GdtXlp> {


}
