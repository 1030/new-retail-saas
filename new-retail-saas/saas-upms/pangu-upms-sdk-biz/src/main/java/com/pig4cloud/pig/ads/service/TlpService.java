package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.ThirdLandingPage;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @author yk
 */
public interface TlpService extends IService<ThirdLandingPage> {

	//修改落地页名称
	R updateSelective(ThirdLandingPage req);

	//添加落地页
	R create(ThirdLandingPage req);
}
