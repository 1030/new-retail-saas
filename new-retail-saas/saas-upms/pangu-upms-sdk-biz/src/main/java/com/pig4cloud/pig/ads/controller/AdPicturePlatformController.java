package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.ads.pig.mapper.AdPicturePlatformMapper;
import com.pig4cloud.pig.api.entity.AdPicturePlatform;
import com.pig4cloud.pig.api.vo.PictureLibVo;
import com.pig4cloud.pig.ads.service.AdPicturePlatformService;
import com.pig4cloud.pig.ads.service.AdvService;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * @description:
 * @author: nml
 * @time: 2020/11/6 15:51
 **/
@Log4j2
@RestController
@RequestMapping("/platformPicture")
@RequiredArgsConstructor
@Api(value = "platformPicture", tags = "投放平台上传图片信息")
public class AdPicturePlatformController {

	@Autowired
	private AdPicturePlatformService adPicturePlatformService;

	@Autowired
	private AdPicturePlatformMapper adPicturePlatformMapper;

	private final AdvService advService;

	//调用头条接口批量同步上传图片
	@RequestMapping("/synPicBatch")
	public R synPictureBatch(@RequestBody PictureLibVo pictureLibVo){
		return adPicturePlatformService.synPictureBatch(pictureLibVo);
	}

	//调用头条接口批量同步上传图片
/*	@RequestMapping("/synGdtPicBatch")
	public R synGdtPictureBatch(PictureLibVo pictureLibVo){
		return adPicturePlatformService.synGdtPictureBatch(pictureLibVo);
	}*/

	//调用头条接口批量同步上传图片
	@RequestMapping("/synAllPicBatch")
	public R synAllPictureBatch(@RequestBody PictureLibVo pictureLibVo){
		return adPicturePlatformService.pushPic(pictureLibVo);
	}

	/**
	 * 图片推送状态的分页列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/getPushAdList")
	public R getPushAdList(PictureLibVo req){
		//设置分页条件
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());

		//分页查询
		IPage<AdPicturePlatform> data = new Page<>();
		//设置where条件
		QueryWrapper<AdPicturePlatform> queryWrapper = new QueryWrapper<>();
		// 1、图片id
		if (null != req.getId()){
			queryWrapper.like("picture_id",req.getId());
		}
		//2、平台id
		if (null != req.getPlatformType()){
			queryWrapper.like("platform_id",req.getPlatformType());
		}
		//3、当前账户下广告账户列表
		//!!!获取当前账户下的广告账户集合
		Integer userId = SecurityUtils.getUser().getId();
		List<String> authAdvList = Lists.newArrayList();
		if (req.getPlatformType() == 1){
			authAdvList = advService.getOwnerAdv(userId, PlatformTypeEnum.GDT.getValue());
		}else if (req.getPlatformType() == 3){
			authAdvList = advService.getOwnerAdv(userId, PlatformTypeEnum.TT.getValue());
		}
		if (authAdvList.size()>0){
			queryWrapper.in("advertiser_id",authAdvList);
		}else {
			//当前账户下的广告账户为无时，直接返回空数据
			return R.ok(data, "查询成功");
		}

		try {
			data = adPicturePlatformMapper.selectPage(page, queryWrapper);
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed("查询失败");
		}
		return R.ok(data, "查询成功");
	}

}
