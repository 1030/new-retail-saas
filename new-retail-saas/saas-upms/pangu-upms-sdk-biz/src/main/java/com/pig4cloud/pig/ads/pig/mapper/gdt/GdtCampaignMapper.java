package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtCampaign;
import com.pig4cloud.pig.api.gdt.vo.GdtCampaignVo;

/**
 * 广点通-推广计划 数据层
 * 
 * @author hma
 * @date 2020-12-03
 */
public interface GdtCampaignMapper extends BaseMapper<GdtCampaign>
{

//	/**
//     * 查询广点通-推广计划信息
//     *
//     * @param campaignId 广点通-推广计划ID
//     * @return 广点通-推广计划信息
//     */
//	public GdtCampaign selectGtdCampaignById(Long campaignId);
//
//	/**
//     * 查询广点通-推广计划列表
//     *
//     * @param gtdCampaign 广点通-推广计划信息
//     * @return 广点通-推广计划集合
//     */
//	public List<GdtCampaign> selectGtdCampaignList(GdtCampaign gtdCampaign);
//
//	/**
//     * 新增广点通-推广计划
//     *
//     * @param gtdCampaign 广点通-推广计划信息
//     * @return 结果
//     */
//	public int insertGtdCampaign(GdtCampaign gtdCampaign);
//
//	/**
//     * 修改广点通-推广计划
//     *
//     * @param gtdCampaign 广点通-推广计划信息
//     * @return 结果
//     */
//	public int updateGtdCampaign(GdtCampaign gtdCampaign);
//
//
//
//	/**
//     * 批量添加广点通-推广计划
//     *
//     * @param gtdCampaignList 需要添加的数据集合
//     * @return 结果
//     */
//	public int batchInsertGtdCampaign(List<GdtCampaign> gtdCampaignList);
//
	/**
	 * 修改广点通-推广计划
	 * @param req 广点通-推广计划信息
	 * @return 结果
	 */
	int update(GdtCampaignVo req);
	
}