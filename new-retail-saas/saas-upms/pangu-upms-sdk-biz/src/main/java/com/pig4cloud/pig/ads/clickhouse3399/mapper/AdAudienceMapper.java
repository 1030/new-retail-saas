package com.pig4cloud.pig.ads.clickhouse3399.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.annotation.SqlParser;

import com.pig4cloud.pig.api.dto.AdAudienceDTO;
import com.pig4cloud.pig.api.dto.AdPTypeDTO;
import com.pig4cloud.pig.api.vo.AdAccountWarnVo;
import com.pig4cloud.pig.api.vo.AdAudienceVo;
import com.pig4cloud.pig.api.vo.AdPtypeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description 查询人群包统计数据
 * @Author chengang
 * @Date 2021/6/24
 */
@Mapper
@InterceptorIgnore(tenantLine = "1")
public interface AdAudienceMapper {

//	@SqlParser(filter=true)
	List<AdAudienceVo> selectCost(AdAudienceDTO dto);

//	@SqlParser(filter=true)
	List<AdAudienceVo> selectUsrNameNum(AdAudienceDTO dto);

//	@SqlParser(filter=true)
	List<AdAudienceVo> selectNewUuidNum(AdAudienceDTO dto);

//	@SqlParser(filter=true)
	List<AdAudienceVo> selectRetentionTwo(AdAudienceDTO dto);

	List<AdAccountWarnVo> selectWarnAccount(@Param("ids") List<String> ids);

	List<AdAccountWarnVo> selectNoCostCount(@Param("ids") List<String> ids);

	int insertAdPType(AdPTypeDTO dto);

	AdPtypeVo selectAdPTypeByAdId(AdPTypeDTO dto);

}
