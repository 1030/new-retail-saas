package com.pig4cloud.pig.ads.service;

import java.util.List;

import com.pig4cloud.pig.api.entity.AdMaterial;
import com.pig4cloud.pig.api.entity.AdMaterialPlatform;
import com.pig4cloud.pig.api.entity.AdMaterialPushBox;

public interface AdMaterialPushWorkerService {

	List<AdMaterialPlatform> syncPushAdMateria(String loginUserId, List<AdMaterialPushBox> entitys);

	AdMaterialPlatform pushVideoKs(String loginUserId,String string, AdMaterial adMaterial);

	AdMaterialPlatform pushVideoGdt(String loginUserId,String string, AdMaterial adMaterial);

	AdMaterialPlatform pushVideoToutiao(String loginUserId,String string, AdMaterial adMaterial);

	AdMaterialPlatform pushPictureToutiao(String loginUserId,String string, AdMaterial adMaterial);

	AdMaterialPlatform pushPictureGdt(String loginUserId,String string, AdMaterial adMaterial);

	AdMaterialPlatform pushPictureKs(String loginUserId,String string, AdMaterial adMaterial);

	void asyncPushAdMateria(String loginUserId, String advertiserId, Integer platformId, AdMaterial adMaterial);

}
