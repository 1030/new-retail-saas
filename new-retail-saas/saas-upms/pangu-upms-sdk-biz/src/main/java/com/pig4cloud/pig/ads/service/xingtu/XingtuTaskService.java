package com.pig4cloud.pig.ads.service.xingtu;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.xingtu.XingtuTask;
import com.pig4cloud.pig.common.core.util.R;

public interface XingtuTaskService extends IService<XingtuTask> {

	R getTaskList(String param);

	R getTaskOrderList(String param);

	R getTaskOrderDetail(String param);

}
