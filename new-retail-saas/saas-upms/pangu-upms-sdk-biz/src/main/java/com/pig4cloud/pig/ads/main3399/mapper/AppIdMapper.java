package com.pig4cloud.pig.ads.main3399.mapper;

import com.pig4cloud.pig.api.entity.AppIdWithPackName;

import java.util.List;

public interface AppIdMapper {
	List<AppIdWithPackName> appIdList(Integer plantformId);
}
