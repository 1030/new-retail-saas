package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.ads.service.AdCommonWordsService;
import com.pig4cloud.pig.api.dto.AdCommonWordsDto;
import com.pig4cloud.pig.api.entity.AdCommonWordsEntity;
import com.pig4cloud.pig.api.vo.AdCommonWordsVO;
import com.pig4cloud.pig.api.vo.AdCommonWordsVO.Add;
import com.pig4cloud.pig.api.vo.AdCommonWordsVO.Delete;
import com.pig4cloud.pig.api.vo.AdCommonWordsVO.Edit;
import com.pig4cloud.pig.common.core.util.ECollectionUtil;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping(value = "/words")
@RequiredArgsConstructor
@Validated
public class AdCommonWordsController {

	private final AdCommonWordsService adCommonWordsService;

	/**
	 * 常用语分页列表
	 *
	 * @param words
	 * @return
	 */
	@PostMapping("/getWordsList")
	public R<List<AdCommonWordsEntity>> getWordsList(@RequestBody @Validated AdCommonWordsVO words) {
		List<AdCommonWordsEntity> list = adCommonWordsService.getWordsList(words);
		return R.ok(list);
	}

	/**
	 * 常用语分页列表
	 *
	 * @param words
	 * @return
	 */
	@SysLog("常用语管理")
	@PostMapping("/getWordsPage")
	public R<Page<AdCommonWordsEntity>> getWordsPage(@RequestBody @Validated AdCommonWordsVO words) {
		Page<AdCommonWordsEntity> page = adCommonWordsService.getWordsPage(words);
		return R.ok(page);
	}

	/**
	 * 获取常用语信息
	 *
	 * @param id
	 * @return
	 */
	@GetMapping("/getWords")
	public R<AdCommonWordsEntity> getWords(@RequestParam Long id) {
		AdCommonWordsEntity words = adCommonWordsService.getById(id);
		return R.ok(words);
	}

	/**
	 * 添加常用语
	 *
	 * @param words
	 * @return
	 */
	@SysLog("添加常用语")
	@PostMapping("/addWords")
	public R addWords(@RequestBody @Validated(Add.class) AdCommonWordsVO words) {
		try {
			adCommonWordsService.addWords(words);
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 编辑常用语
	 *
	 * @param words
	 * @return
	 */
	@SysLog("编辑常用语")
	@PostMapping("/editWords")
	public R editWords(@RequestBody @Validated(Edit.class) AdCommonWordsVO words) {
		try {
			adCommonWordsService.editWords(words);
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 批量删除常用语
	 *
	 * @param words
	 * @return
	 */
	@SysLog("批量删除常用语")
	@PostMapping("/deleteWords")
	public R deleteWords(@RequestBody @Validated(Delete.class) AdCommonWordsVO words) {
		try {
			Set<Long> idSet = ECollectionUtil.stringToLongSet(words.getIdArr());
			if (!idSet.isEmpty()) {
				adCommonWordsService.update(Wrappers.<AdCommonWordsEntity>lambdaUpdate().set(AdCommonWordsEntity::getIsDeleted, 1).set(AdCommonWordsEntity::getUpdateDate, new Date()).in(AdCommonWordsEntity::getId, idSet));
			}
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}


	/**
	 * 分页列表
	 *
	 * @param req
	 * @return
	 */
	@Deprecated
	@RequestMapping("/getPage")
	public R getPage(AdCommonWordsDto req) {
		//设置分页条件
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());
		IPage<AdCommonWordsEntity> data = adCommonWordsService.selectPageList(req);
		return R.ok(data, "请求成功");
	}

	/**
	 * 新增
	 *
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/add")
	public R addWords(@RequestBody @Valid AdCommonWordsDto req, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return R.failed(bindingResult.getFieldError().getDefaultMessage());
		}
		return adCommonWordsService.insert(req);
	}

	/**
	 * 修改
	 *
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/edit")
	public R editWords(@RequestBody @Valid AdCommonWordsDto req, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return R.failed(bindingResult.getFieldError().getDefaultMessage());
		}
		if (null == req.getId()) {
			return R.failed("id为空");
		}
		return adCommonWordsService.update(req);
	}

	/**
	 * 删除
	 *
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/delete")
	public R deleteWords(@RequestBody AdCommonWordsDto req) {
		if (null == req.getId()) {
			return R.failed("id为空");
		}
		return adCommonWordsService.delete(req.getId());
	}

}
