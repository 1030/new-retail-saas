package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdTermBannedDto;
import com.pig4cloud.pig.api.entity.AdTermBannedEntity;
import com.pig4cloud.pig.api.vo.AdTermBannedVo;
import com.pig4cloud.pig.common.core.util.R;

public interface AdTermBannedService extends IService<AdTermBannedEntity> {

	/**
	 * 获取屏蔽词分页列表
	 *
	 * @param req
	 * @return
	 */
	Page<AdTermBannedEntity> getTermsPage(AdTermBannedVo req);

	/**
	 * 添加屏蔽词
	 *
	 * @param req
	 */
	void addTerms(AdTermBannedVo req);

	/**
	 * 修改屏蔽词
	 *
	 * @param req
	 */
	void editTerms(AdTermBannedVo req);

	/**
	 * 删除屏蔽词
	 *
	 * @param id
	 */
	void deleteTerms(Long id);


	@Deprecated
	IPage<AdTermBannedVo> selectPageList(AdTermBannedDto dto);

	@Deprecated
	R addTerms(AdTermBannedDto dto);

	@Deprecated
	R deleteTerms(AdTermBannedDto dto);

	@Deprecated
	R updateTerms(AdTermBannedDto dto);
}
