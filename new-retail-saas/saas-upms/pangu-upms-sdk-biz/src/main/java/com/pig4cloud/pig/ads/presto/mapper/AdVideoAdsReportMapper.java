package com.pig4cloud.pig.ads.presto.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.dto.AdVideoAdsReportDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
@InterceptorIgnore(tenantLine = "1")
public interface AdVideoAdsReportMapper extends BaseMapper<AdVideoAdsReportDTO> {


//	List<AdVideoAdsReportDTO> selectAdVideoAdsReport(Map<String,Object> param);
//
//	List<AdVideoAdsReportDTO> selectAdVideoAdsReportForCreative(Map<String,Object> param);
}