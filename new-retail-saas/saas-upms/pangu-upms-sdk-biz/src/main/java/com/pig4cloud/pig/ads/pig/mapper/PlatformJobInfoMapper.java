package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.PlatformJobInfo;

/**
 * 平台任务 数据层
 * 
 * @author hma
 * @date 2020-11-16
 */
public interface PlatformJobInfoMapper    extends BaseMapper<PlatformJobInfo>
{

//
//	/**
//     * 查询平台任务信息
//     *
//     * @param id 平台任务ID
//     * @return 平台任务信息
//     */
//	 PlatformJobInfo selectPlatformJobInfoById(Integer id);
//
//	/**
//     * 查询平台任务列表
//     *
//     * @param platformJobInfo 平台任务信息
//     * @return 平台任务集合
//     */
//	 List<PlatformJobInfo> selectPlatformJobInfoList(PlatformJobInfo platformJobInfo);
//
//	/**
//     * 新增平台任务
//     *
//     * @param platformJobInfo 平台任务信息
//     * @return 结果
//     */
//	 int insertPlatformJobInfo(PlatformJobInfo platformJobInfo);
//
//	/**
//     * 修改平台任务
//     *
//     * @param platformJobInfo 平台任务信息
//     * @return 结果
//     */
//	 int updatePlatformJobInfo(PlatformJobInfo platformJobInfo);
	

}