package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdVideoAdsReportDTO;
import com.pig4cloud.pig.api.dto.AdVideoReport;
import com.pig4cloud.pig.api.entity.AdVideo;
import com.pig4cloud.pig.api.vo.AdVideoPlatformVo;
import com.pig4cloud.pig.api.vo.AdVideoReportVo;
import com.pig4cloud.pig.api.vo.AdVideoSortVo;
import com.pig4cloud.pig.api.vo.AdVideoVo;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdVideoService.java
 * @createTime 2020年11月05日 14:01:00
 */
public interface AdVideoService extends IService<AdVideo> {

	R<Map<String,Object>> pushVideo(AdVideoVo req);

	R<List<AdVideoReport>> findReport(Map<String,Object> param);

	R getPushAdList(AdVideoPlatformVo req);

	R uploadVideo(MultipartFile[] file);

	IPage<AdVideoVo> selectAdVideoListPage(AdVideoVo req);

	List<AdVideoAdsReportDTO> selectAdVideoSortList(AdVideoSortVo req);

	R getAdVideoReport(AdVideoReportVo req);

	R getPrestoAdVideoReport(AdVideoReportVo req);

}
