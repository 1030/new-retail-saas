package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdCreativeComponent;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdCreativeComponentMapper extends BaseMapper<AdCreativeComponent> {
}
