package com.pig4cloud.pig.ads.service;

import com.pig4cloud.pig.common.core.util.R;

public interface SelectListService {
	R czSite(String advertiserId, Integer page, Integer pageSize);

	R actionTextList(String advertiserId, String landingType);

	R callToActionList(String advertiserId, String recommendType, String advancedCreativeType);

	R industryList(String advertiserId);
}
