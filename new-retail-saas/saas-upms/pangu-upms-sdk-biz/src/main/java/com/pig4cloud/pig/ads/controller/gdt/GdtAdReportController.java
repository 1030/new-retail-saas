package com.pig4cloud.pig.ads.controller.gdt;

import com.pig4cloud.pig.ads.gdt.service.GdtAdReportService;
import com.pig4cloud.pig.api.gdt.vo.GdtAdVo;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description:
 * @author: nml
 * @time: 2020/12/18 14:30
 **/

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/gdt/adc")
@Api(value = "/gdt/adc", tags = "广点通广告模板")
public class GdtAdReportController {

	@Resource
	private GdtAdReportService gdtAdReportService;

	@RequestMapping("/onOff")
	public R updateGdtAdOnOff(GdtAdVo req) {
		if(null == req){
			return R.failed( "参数不能为空");
		}
		if (req.getOnOff() == null || StringUtils.isBlank(req.getOnOff())) {
			return R.failed("请选择开关");
		}
		if (req.getAdId() == null) {
			return R.failed("请选择广告id");
		}
		return gdtAdReportService.updateGdtAdOnOff(req) ;
	}

	/*
	 * 获取广点通广告创意列表
	 *
	 * */
	@RequestMapping("/report")
	public R getGdtAdCreativeReport(GdtAdVo gdtAdVo) {

		if(null == gdtAdVo){
			return R.failed( "参数不能为空");
		}
		if (null == gdtAdVo.getSdate() || StringUtils.isBlank(gdtAdVo.getSdate())){
			return R.failed( "起始时间不能为空");
		}
		if (null == gdtAdVo.getEdate() || StringUtils.isBlank(gdtAdVo.getEdate())){
			return R.failed( "结束时间不能为空");
		}
		return gdtAdReportService.getGdtAdReport(gdtAdVo) ;
	}

}
