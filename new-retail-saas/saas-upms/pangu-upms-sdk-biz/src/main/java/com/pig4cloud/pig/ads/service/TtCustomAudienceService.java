package com.pig4cloud.pig.ads.service;

import javax.validation.Valid;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.api.dto.AllCustomAudienceDTO;
import com.pig4cloud.pig.api.entity.AdActionInterest;
import com.pig4cloud.pig.api.entity.AllCustomAudience;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.TtCustomAudiencesDTO;
import com.pig4cloud.pig.api.entity.TtCustomAudience;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface TtCustomAudienceService extends IService<TtCustomAudience> {

	R createDataSource(TtCustomAudiencesDTO ttCustomAudiencesDTO);

	R create(MultipartFile[] file, @Valid TtCustomAudiencesDTO ttCustomAudiencesDTO);

	IPage<AllCustomAudience> getAudienceList(AllCustomAudienceDTO audienceDTO);

	R deleteAudience(Long id);

	R pushSelfCustomAudience(@Valid TtCustomAudiencesDTO dto);

    List<TtCustomAudience> getAllThirdIdAndNames();
}
