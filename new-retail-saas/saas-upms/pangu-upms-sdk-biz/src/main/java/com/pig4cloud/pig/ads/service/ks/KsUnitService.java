/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.service.ks;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.ks.KsPlanReq;
import com.pig4cloud.pig.api.entity.ks.KsUnit;
import com.pig4cloud.pig.common.core.util.R;

import java.util.Set;

/**
 * 快手广告组信息表
 *
 * @author yuwenfeng
 * @date 2022-03-26 17:31:08
 */
public interface KsUnitService extends IService<KsUnit> {


	/**
	 * 广告计划修改状态
	 * @param req
	 * @return
	 */
	R updateStatus(KsPlanReq req);

	/**
	 * 广告计划修改预算
	 * @param req
	 * @return
	 */
	R updateDayBudget(KsPlanReq req);

	/**
	 * 广告计划修改出价
	 * @param req
	 * @return
	 */
	R updateBid(KsPlanReq req);

	/**
	 * 通过广告计划ID查询广告计划操作状态
	 * @param adIdSet
	 * @return
	 */
	R getPutStatus(Set<Long> adIdSet);
}
