package com.pig4cloud.pig.ads.datasource;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

//数据源1
@Configuration
//配置mybatis的接口类放的地方
@MapperScan(basePackages = "com.pig4cloud.pig.ads.main3399.mapper", sqlSessionFactoryRef = "main3399SqlSessionFactory")
public class Main3399DataSourceConfig {

	// 将这个对象放入Spring容器中
	@Bean(name = "main3399DataSource")
	// 表示这个数据源是默认数据源
	// 读取application.properties中的配置参数映射成为一个对象
	// prefix表示参数的前缀
	@ConfigurationProperties(prefix = "spring.datasource.main3399")
	public DataSource getDateSource1() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "main3399SqlSessionFactory")
	// 表示这个数据源是默认数据源
	// @Qualifier表示查找Spring容器中名字为main3399DataSource的对象
	public MybatisSqlSessionFactoryBean iProcessSqlSessionFactory(
			@Qualifier("main3399DataSource") DataSource datasource) throws Exception {
		MybatisSqlSessionFactoryBean sqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(getDateSource1());
		MybatisConfiguration configuration = new MybatisConfiguration(); 
		//configuration.setMapUnderscoreToCamelCase(false);
		sqlSessionFactoryBean.setConfiguration(configuration);
		sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/mapper/main3399/*Mapper.xml"));
		return sqlSessionFactoryBean;

	}

	@Bean("main3399SqlSessionTemplate")
	// 表示这个数据源是默认数据源
	public SqlSessionTemplate test1sqlsessiontemplate(
			@Qualifier("main3399SqlSessionFactory") SqlSessionFactory sessionfactory) {
		return new SqlSessionTemplate(sessionfactory);
	}
}