package com.pig4cloud.pig.ads.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdActionInterestDTO;
import com.pig4cloud.pig.api.dto.AdPlanTempDto;
import com.pig4cloud.pig.api.entity.AdPlanTemp;
import com.pig4cloud.pig.common.core.util.R;

import java.io.IOException;
import java.util.List;

/**
 * 广告计划临时 服务层
 *
 * @author hma
 * @date 2020-11-18
 */
public interface AdPlanTempService extends IService<AdPlanTemp> {

	/**
	 * 行为类目列表查询
	 *
	 * @param planTemp
	 * @return
	 */
	List<AdActionInterestDTO> selectActionCategoryList(AdPlanTempDto planTemp) throws IOException;

	/**
	 * 行为关键词模糊查询
	 *
	 * @param planTemp
	 * @return
	 */
	List<AdActionInterestDTO> selectActionKeywordList(AdPlanTempDto planTemp) throws IOException;

	/**
	 * 兴趣类目列表查询
	 *
	 * @param planTemp
	 * @return
	 */
	List<AdActionInterestDTO> selectInterestCategoryList(AdPlanTempDto planTemp) throws IOException;

	/**
	 * 兴趣关键词模糊查询
	 *
	 * @param planTemp
	 * @return
	 */
	List<AdActionInterestDTO> selectInterestKeywordList(AdPlanTempDto planTemp) throws IOException;

	/**
	 * 行为兴趣ID查询关键词列表
	 *
	 * @param planTemp
	 * @return
	 */
	List<AdActionInterestDTO> selectActionInterestKeywordList(AdPlanTempDto planTemp) throws IOException;

	/**
	 * 查询广告计划临时信息
	 *
	 * @param id 广告计划临时ID
	 * @return 广告计划临时信息
	 */
	AdPlanTemp selectAdPlanTempById(Long id);

	/**
	 * 插入广告计划数据
	 *
	 * @param adPlanTempDto
	 * @return
	 */
	R saveAdPlanTempOperate(AdPlanTempDto adPlanTempDto);

	/**
	 * 获取用户某个广告账号，广告组添加的广告计划数据（模板）
	 *
	 * @param adPlanTempDto
	 */
	R getUserPlanTemp(AdPlanTempDto adPlanTempDto);


}
