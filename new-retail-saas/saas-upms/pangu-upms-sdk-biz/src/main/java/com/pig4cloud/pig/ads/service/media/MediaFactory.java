package com.pig4cloud.pig.ads.service.media;

import com.pig4cloud.pig.api.annotation.Media;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author chengang
 * @Date 2022/12/13
 */
@Component
public class MediaFactory {

    @Autowired(required = false)
    private List<MediaEngine> mediaEngineList;

    private Map<String, MediaEngine> mediaEngineMap;

	public List<MediaEngine> getMediaEngineList() {
		return mediaEngineList;
	}

	public Map<String, MediaEngine> getMediaEngineMap() {
        if (mediaEngineMap == null) {
            mediaEngineMap = new HashMap<>();
            for (MediaEngine engine : mediaEngineList) {
                String key = engine.getClass().getAnnotation(Media.class).value();
                if (StringUtils.isNotBlank(key) && mediaEngineMap.get(key) != null) {
                    throw new RuntimeException("重复的媒体渠道KEY: " + key);
                }
                mediaEngineMap.put(key, engine);
            }
        }
        return mediaEngineMap;
    }
}
