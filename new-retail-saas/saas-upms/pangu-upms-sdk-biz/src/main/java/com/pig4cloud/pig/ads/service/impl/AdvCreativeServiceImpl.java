
/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdCreativeMapper;
import com.pig4cloud.pig.ads.service.*;
import com.pig4cloud.pig.ads.utils.CheckUtil;
import com.pig4cloud.pig.ads.utils.OEHttpUtils;
import com.pig4cloud.pig.api.dto.AdvertiserDto;
import com.pig4cloud.pig.api.dto.CampaignDto;
import com.pig4cloud.pig.api.dto.TtAdCreativeResponseNew;
import com.pig4cloud.pig.api.entity.AdCreative;
import com.pig4cloud.pig.api.entity.ResponseBean;
import com.pig4cloud.pig.api.entity.TtAdCreativeTemp;
import com.pig4cloud.pig.api.entity.TtCreativeDetail;
import com.pig4cloud.pig.api.enums.AdMaterialTypeEnum;
import com.pig4cloud.pig.api.enums.StatusEnum;
import com.pig4cloud.pig.api.util.JsonUtil;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;


/**
 * @author hma
 * @广告 报表查询 服务实现类
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class AdvCreativeServiceImpl extends ServiceImpl<AdCreativeMapper, AdCreative> implements AdvCreativeService {

	@Resource
	private AdvCampaignService advCampaignService;

	@Resource
	private AdvPlanService advPlanService;

	@Resource
	private AdCreativeMaterialService adCreativeMaterialService;

	private final TtAccesstokenService ttAccesstokenService;


	@Value("${adv_creative_create_url}")
	private String advCreativeCreateUrl;

	@Value("${adv_creative_url}")
	private String advCreativeUrl;

	@Value("${ad_third_url}")
	private String adThirdUrl;

	@Value("${adv_creative_custom_creative_url}")
	private String advCreativeCustomCreativeUrl;

	@Value("${adv_creative_procedural_creative_url}")
	private String advCreativeProceduralCreativeUrl;


	/**
	 * 查询广告创意报表数据
	 *
	 * @param campaignDto
	 * @return
	 */

	@Override
	public R getAdvCreativeStatistic(CampaignDto campaignDto) {
		campaignDto.setType(StatusEnum.TYPE_THREE.getStatus());
		campaignDto.setAdType(StatusEnum.AD_TYPE_ONE.getStatus());
		return advCampaignService.getAdvStatistic(campaignDto);
	}


	/**
	 * 修改广告创意状态
	 *
	 * @param advertiserDto
	 * @return
	 */
	@Override
	public R updateCreativeStatus(AdvertiserDto advertiserDto) {
		if (null == advertiserDto) {
			return R.failed("参数为空");
		}
		if (null == advertiserDto.getAdvertiserId()) {
			return R.failed("广告账号id不允许为空");
		}
		if (null == advertiserDto.getCreativeId()) {
			return R.failed("广告创意id不允许为空");
		}
		if (null == advertiserDto.getOptStatus()) {
			return R.failed("操作状态不允许为空");
		}
		R r = advCampaignService.updateThirdData(advertiserDto, StatusEnum.TYPE_THREE.getStatus());
		if (r.getCode() == 0) {
			String status = null;
			if (StringUtils.equals(advertiserDto.getOptStatus(), "enable")) {
				status = "CREATIVE_STATUS_ENABLE";
			} else if (StringUtils.equals(advertiserDto.getOptStatus(), "disable")) {
				status = "CREATIVE_STATUS_DISABLE";
			}
			String data = (String) r.getData();
			JSONObject js = JSON.parseObject(data);
			String errors = js.getString("errors");
			String success = js.getString("success");
			JSONArray errorsArr = JSONArray.parseArray(errors);
			for (int j = 0; j < errorsArr.size(); j++) {
				JSONObject jtem = errorsArr.getJSONObject(j);
			}


			baseMapper.updateAdCreative(new AdCreative(advertiserDto.getCreativeId(), status));
			return R.ok();
		} else {
			return r;
		}

	}


	public static void main(String[] args) {
		String bb = "{\"errors\": [null, null, {\"message\": \"计划不支持该类型组件[ID=7007398487239163935]\", \"code\": 40000}], \"creative_ids\": [1710862178721831, 1710862178721847, null]}";
		String aa = "{\"errors\": null, \"creative_ids\": [1710844415093779]}";
		String returnData = "{\"creative_ids\": [null, null], \"errors\": [{\"code\": 40000, \"message\": \"组件[ID=7007385432191139853]不可用\"}, {\"code\": 40000, \"message\": \"组件[ID=7007385432191139853]不可用\"}]}";
		TtAdCreativeResponseNew rep = JSON.parseObject(bb, TtAdCreativeResponseNew.class);
		System.out.println(rep);

	}

	/**
	 * 创建广告创意
	 *
	 * @param ttAdCreativeTemp
	 * @return
	 */
	@Override
	public TtAdCreativeResponseNew createAdvCreative(TtAdCreativeTemp ttAdCreativeTemp) {
		// 调用头条创建广告计划
		// 返回对应的广告计划id
		//字段不限的时候 设置值为空，前端不传值------当不限的时候库里面的数据为空
		try {
			String reqUrl = advCreativeCustomCreativeUrl;
			// userName-->user_name  且 过滤 null
			String paramJson = JsonUtil.toUnderlineJSONString(ttAdCreativeTemp);
			log.info("广告账户：{},广告计划：{}，创建创意的json:{}", ttAdCreativeTemp.getAdvertiserId(), ttAdCreativeTemp.getAdId(), paramJson);
			Map<String, Object> paramMap = JsonUtil.parseJSON2Map(paramJson);
			String creativeMaterialMode = ttAdCreativeTemp.getCreativeMaterialMode();

			Map<String, Object> lastParamMap = new HashMap<>();
			lastParamMap.put("advertiser_id", ttAdCreativeTemp.getAdvertiserId());
			lastParamMap.put("ad_id", ttAdCreativeTemp.getAdId());

			//构建创意信息
			if (paramMap.get("creatives") != null) {
				String jsonStr = (String) paramMap.get("creatives");
				if (!"STATIC_ASSEMBLE".equals(creativeMaterialMode)) {
					Object[] objArr = JSON.parseArray(jsonStr).toArray();
					lastParamMap.put("creative_list", objArr);

				} else {
					reqUrl = advCreativeProceduralCreativeUrl;
					Object objArr = JSON.parseObject(jsonStr);
					lastParamMap.put("creative", objArr);
				}
			}
			// 构建创意基础信息
			JSONObject adData = this.buildAdData(paramMap, creativeMaterialMode);
			lastParamMap.put("ad_data", adData);

			log.info("广告账户：{},广告计划：{}，创意类型，{}，创建创意的paramMap:{}", ttAdCreativeTemp.getAdvertiserId(), ttAdCreativeTemp.getAdId(), creativeMaterialMode, JSON.toJSONString(lastParamMap));
			String returnData = advPlanService.createThirdPlanOrCreative(lastParamMap, "createAdvCreative", reqUrl, ttAdCreativeTemp.getAdvertiserId().toString(), StatusEnum.TYPE_THREE.getStatus());
//			TtAdCreativeReponse rep =JsonUtil.toSnakeObject(returnDate,TtAdCreativeReponse.class);
			TtAdCreativeResponseNew rep = JSON.parseObject(returnData, TtAdCreativeResponseNew.class);
			return rep;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("createAdvPlan is error", e);
			throw new BusinessException(11, e.getMessage());
		}
	}

	/**
	 * 构建创意基础信息
	 *
	 * @param paramMap
	 */
	private JSONObject buildAdData(Map<String, Object> paramMap, String creativeMaterialMode) {
		JSONObject adData = new JSONObject();
		//三级行业ID
		this.setMapValue(adData, paramMap, "third_industry_id", false);
		//创意标签
		this.setMapValue(adData, paramMap, "ad_keywords", true);
		//广告来源
		this.setMapValue(adData, paramMap, "source", false);
		//品牌主页
		this.setMapValue(adData, paramMap, "ies_core_user_id", false);
		if ("STATIC_ASSEMBLE".equals(creativeMaterialMode)) {
			//自动生成视频素材
			this.setMapValue(adData, paramMap, "is_presented_video", false);
			//是否开启衍生计划
			this.setMapValue(adData, paramMap, "generate_derived_ad", false);
		}
		//搭配试玩素材URL
		this.setMapValue(adData, paramMap, "playable_url", false);
		//主页作品列表隐藏广告内容
		this.setMapValue(adData, paramMap, "is_feed_and_fav_see", false);
		//是否开启自动派生创意
		this.setMapValue(adData, paramMap, "creative_auto_generate_switch", false);
		//应用名
		this.setMapValue(adData, paramMap, "app_name", false);
		//Android应用下载详情页
		this.setMapValue(adData, paramMap, "web_url", false);
		//落地页链接
		this.setMapValue(adData, paramMap, "external_url", false);
		//是否关闭评论
		this.setMapValue(adData, paramMap, "is_comment_disable", false);
		//允许客户端下载视频功能
		this.setMapValue(adData, paramMap, "ad_download_status", false);
		//创意展现方式
		this.setMapValue(adData, paramMap, "creative_display_mode", false);
//		paramMap.put("ad_data",adData);
		return adData;
	}


	private void setMapValue(JSONObject adData, Map<String, Object> paramMap, String key, boolean strSplit) {
		//不采用map复制
		if (paramMap.get(key) != null) {
			if (strSplit) {
				String tmp = (String) paramMap.get(key);
				adData.put(key, tmp.split(","));
			} else {
				adData.put(key, paramMap.get(key));
			}
		}
	}


	@Override
	public void getAdCreativeByAdId(String advertiserId, Long adId) {
		getAdCreative(advertiserId, adId, null);
	}


	/**
	 * 创意详情接口没有变化-营销链路的创意的格式仍然是旧的格式，故复制时需要按照创建的格式构建
	 *
	 * @param ttCreativeDetail
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public TtAdCreativeResponseNew copyAdvCreative(TtCreativeDetail ttCreativeDetail) throws Exception {
		try {
			String accessToken = ttAccesstokenService.fetchAccesstoken(ttCreativeDetail.getAdvertiserId());
			String paramJson = JsonUtil.toUnderlineJSONString(ttCreativeDetail);
			log.info("复制创意JSON传参:" + paramJson);
			Map<String, Object> paramMap = JsonUtil.parseJSON2Map(paramJson);
			String reqUrl = advCreativeCustomCreativeUrl;
			String creativeMaterialMode = ttCreativeDetail.getCreativeMaterialMode();
			Map<String, Object> lastParamMap = new HashMap<>();
			lastParamMap.put("advertiser_id", ttCreativeDetail.getAdvertiserId());
			lastParamMap.put("ad_id", ttCreativeDetail.getAdId());
			//构建自定义创意信息
			if (!"STATIC_ASSEMBLE".equals(creativeMaterialMode)) {
				if(StringUtils.isNotBlank(CheckUtil.deal(ttCreativeDetail.getCreativeList(),2))){
					lastParamMap.put("creative_list",JSON.parseArray(ttCreativeDetail.getCreativeList()));
				} else if (paramMap.get("creatives") != null) {
					String jsonStr = (String) paramMap.get("creatives");
					JSONArray creatives = JSON.parseArray(jsonStr);
					JSONArray creativelist = new JSONArray();
					for (int i = 0; i < creatives.size(); i++) {
						//创意详情对象
						JSONObject item = creatives.getJSONObject(i);
						JSONObject creativeObj = this.getCreativeObj4custom(item, paramMap);
						creativelist.add(creativeObj);
					}
					lastParamMap.put("creative_list", creativelist);
				}
			} else {
				reqUrl = advCreativeProceduralCreativeUrl;
				if(StringUtils.isNotBlank(CheckUtil.deal(ttCreativeDetail.getCreative(),1))){
					lastParamMap.put("creative", JSON.parseObject(ttCreativeDetail.getCreative()));
				}else {
					lastParamMap.put("creative", this.getCreativeObj4procedural(paramMap));
				}

			}

			// 构建创意基础信息
			JSONObject adData = this.buildAdData(paramMap, creativeMaterialMode);
			lastParamMap.put("ad_data", adData);

			log.info("复制创意参数：广告账户：{},广告计划：{}，创意类型，{}，创建创意的paramMap:{}", ttCreativeDetail.getAdvertiserId(), ttCreativeDetail.getAdId(), creativeMaterialMode, JSON.toJSONString(lastParamMap));
			String resultStr = OEHttpUtils.doPost(adThirdUrl + reqUrl, lastParamMap, accessToken);
			log.info("复制创意返回:" + resultStr);
			ResponseBean resBean = JSON.parseObject(resultStr, ResponseBean.class);
			if (resBean != null && "0".equals(resBean.getCode())) {
				TtAdCreativeResponseNew rep = JSON.parseObject(resBean.getData().toJSONString(), TtAdCreativeResponseNew.class);
				return rep;
			} else {
				throw new BusinessException(11, "创建广告创意失败" + (resBean != null && StringUtils.isNotBlank(resBean.getMessage()) ? resBean.getMessage() : ""));
			}
		} catch (Throwable e) {
			log.error("copyAdvCreative is error", e);
			throw new BusinessException(11, "创建广告创意异常:"+e.getMessage());
		}
	}

	private JSONObject getCreativeObj4procedural(Map<String, Object> paramMap) {
		JSONObject creative = new JSONObject();
		//解析标题素材
		JSONArray title_material_arr = new JSONArray();
		String titleJsonStr = (String) paramMap.get("title_list");
		JSONArray titleList = JSON.parseArray(titleJsonStr);
		if (CollectionUtils.isNotEmpty(titleList)) {
			for (int i = 0; i < titleList.size(); i++) {
				JSONObject title_material_obj = new JSONObject();
				JSONObject titleTemp = titleList.getJSONObject(i);
				title_material_obj.put("title", titleTemp.getString("title"));

				JSONArray word_arr = new JSONArray();
				JSONArray wordList = titleTemp.getJSONArray("creative_word_ids");
				//没有词包的情况
				if (CollectionUtils.isNotEmpty(wordList)) {
					for (int j = 0; j < wordList.size(); j++) {
						JSONObject word_obj = new JSONObject();
						word_obj.put("word_id", Integer.parseInt(wordList.get(j).toString()));
						word_arr.add(word_obj);
					}
					title_material_obj.put("word_list", word_arr);
				}
				title_material_arr.add(title_material_obj);
			}
			creative.put("title_materials", title_material_arr);
		}

		String imageJsonStr = (String) paramMap.get("image_list");
		JSONArray imageList = JSON.parseArray(imageJsonStr);
		//解析图片
		JSONArray image_materials_arr = new JSONArray();
		//解析视频素材
		JSONArray video_material_arr = new JSONArray();
		if (CollectionUtils.isNotEmpty(titleList)) {
			for (int k = 0; k < imageList.size(); k++) {
				JSONObject materialTemp = imageList.getJSONObject(k);
				//判断是否为图片  image_ids != null 则为图片素材
				JSONArray image_arr = materialTemp.getJSONArray("image_ids");
				JSONObject image_obj = new JSONObject();
				JSONObject video_obj = new JSONObject();
				image_obj.put("image_mode", materialTemp.getString("image_mode"));
				video_obj.put("image_mode", materialTemp.getString("image_mode"));
				if (image_arr != null && image_arr.size() > 0) {
					//组图、橱窗类型传3张，其他图片类型传1张,目前盘古不支持组图、橱窗
					JSONArray image_info_arr = new JSONArray();
					for (int i = 0; i < image_arr.size(); i++) {
						JSONObject image_info = new JSONObject();
						image_info.put("image_id", image_arr.get(i));
						image_info.put("name", "");
						image_info_arr.add(image_info);
					}
					image_obj.put("image_info", image_info_arr);
					image_materials_arr.add(image_obj);
				} else {
					//视频素材
					JSONObject image_info = new JSONObject();
					image_info.put("image_id", materialTemp.getString("image_id"));
					video_obj.put("image_info", image_info);

					JSONObject video_info = new JSONObject();
					video_info.put("video_id", materialTemp.getString("video_id"));
					video_obj.put("video_info", video_info);

					video_material_arr.add(video_obj);
				}
			}
			creative.put("image_materials", image_materials_arr);
			creative.put("video_materials", video_material_arr);
		}

		//解析子标题
		if (paramMap.get("sub_title") != null) {
			JSONObject sub_title_material_obj = new JSONObject();
			sub_title_material_obj.put("sub_title", paramMap.get("sub_title"));
			creative.put("sub_title_material", sub_title_material_obj);
		}


		//解析试玩素材
		// 只有穿山甲激励视频可以使用试玩素材  目前试玩没有启用

		//解析基础创意组件
		JSONArray component_materials_arr = new JSONArray();
		String componentInfoStr = (String) paramMap.get("component_info");
		if (StringUtils.isNotBlank(componentInfoStr)) {
			JSONArray componentInfoList = JSON.parseArray(componentInfoStr);
			for (int m = 0; m < componentInfoList.size(); m++) {
				JSONObject component_materials_obj = new JSONObject();
				JSONObject itemComponent = componentInfoList.getJSONObject(m);
				component_materials_obj.put("component_id", itemComponent.getString("component_id"));

				component_materials_arr.add(component_materials_obj);
			}
			creative.put("component_materials", component_materials_arr);
		}
		return creative;
	}


	private JSONObject getCreativeObj4custom(JSONObject item, Map<String, Object> paramMap) {
		JSONObject creative_obj = new JSONObject();
		String imageMode = item.getString("image_mode");
		creative_obj.put("image_mode", imageMode);

		//解析标题素材
		JSONObject title_material_obj = new JSONObject();
		title_material_obj.put("title", item.getString("title"));
		JSONArray word_arr = new JSONArray();
		JSONArray wordList = item.getJSONArray("creative_word_ids");
		//可能没有使用词包
		if (CollectionUtils.isNotEmpty(wordList)) {
			for (int j = 0; j < wordList.size(); j++) {
				JSONObject word_obj = new JSONObject();
				word_obj.put("word_id", Integer.parseInt(wordList.get(j).toString()));
				word_arr.add(word_obj);
			}
			title_material_obj.put("word_list", word_arr);
		}

		creative_obj.put("title_material", title_material_obj);

		//解析图片素材
		//修复头条视频素材也返回了 image_ids 的bug
		//小图-CREATIVE_IMAGE_MODE_SMALL 大图(横版大图)-CREATIVE_IMAGE_MODE_LARGE 组图-CREATIVE_IMAGE_MODE_GROUP
		// 大图竖图-CREATIVE_IMAGE_MODE_LARGE_VERTICAL
		if ("image".equals(AdMaterialTypeEnum.codeByValue(imageMode))) {
			JSONArray image_materials_arr = new JSONArray();
			JSONArray imageIds = item.getJSONArray("image_ids");
			if (CollectionUtils.isNotEmpty(imageIds)) {
				for (int k = 0; k < imageIds.size(); k++) {
					JSONObject image_info_obj = new JSONObject();
					JSONObject image_materials_obj = new JSONObject();
					image_materials_obj.put("image_id", imageIds.get(k));
					image_materials_obj.put("name", "");
					image_info_obj.put("image_info", image_materials_obj);

					image_materials_arr.add(image_info_obj);
				}
				creative_obj.put("image_materials", image_materials_arr);
			}
		}


		//解析视频素材
		//竖版视频-CREATIVE_IMAGE_MODE_VIDEO_VERTICAL  横版视频-CREATIVE_IMAGE_MODE_VIDEO
		if ("video".equals(AdMaterialTypeEnum.codeByValue(imageMode))) {
			JSONObject video_material_obj = new JSONObject();

			if (item.getString("image_id") != null) {
				JSONObject image_obj = new JSONObject();
				image_obj.put("image_id", item.getString("image_id"));
				video_material_obj.put("image_info", image_obj);
			}

			if (item.getString("video_id") != null) {
				JSONObject video_obj = new JSONObject();
				video_obj.put("video_id", item.getString("video_id"));
				video_material_obj.put("video_info", video_obj);
			}
			if (video_material_obj.size() >= 0) {
				creative_obj.put("video_material", video_material_obj);
			}
		}

		//解析子标题
		if (paramMap.get("sub_title") != null) {
			JSONObject sub_title_material_obj = new JSONObject();
			sub_title_material_obj.put("sub_title", paramMap.get("sub_title"));
			creative_obj.put("sub_title_material", sub_title_material_obj);
		}

		//解析试玩素材
		// 只有穿山甲激励视频可以使用试玩素材  目前试玩没有启用

		//解析基础创意组件
		JSONArray component_materials_arr = new JSONArray();
		JSONArray componentInfoList = item.getJSONArray("component_info");
		if (CollectionUtils.isNotEmpty(componentInfoList)) {
			for (int m = 0; m < componentInfoList.size(); m++) {
				JSONObject component_materials_obj = new JSONObject();
				JSONObject itemComponent = componentInfoList.getJSONObject(m);
				component_materials_obj.put("component_id", itemComponent.getString("component_id"));

				component_materials_arr.add(component_materials_obj);
			}
			creative_obj.put("component_materials", component_materials_arr);
		}


		//解析摘要素材
		//摘要素材，使用搜索广告时必传


		//是否将视频的封面和标题同步到图片创意
		creative_obj.put("derive_poster_cid", item.getString("derive_poster_cid"));

		return creative_obj;
	}

	/*@Override
	public void getAdCreative(String advertiserId, String creativeIds) {
		getAdCreative(advertiserId,null,creativeIds);
	}*/

	/**
	 * 查询对应广告账号对应的创意
	 *
	 * @param advertiserId
	 * @param adId         广告计划id  根据广告id拉取
	 * @param creativeIds  （多个创意计划用逗号分隔）
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public void getAdCreative(String advertiserId, Long adId, String creativeIds) {
		try {
			List<AdCreative> adCreativeList = pullAdCreative(advertiserId, adId, creativeIds);
			if (CollectionUtils.isNotEmpty(adCreativeList)) {
				for (AdCreative adCreative : adCreativeList) {
					AdCreative entity = baseMapper.selectById(adCreative.getCreativeId());
					if (entity == null) {
						baseMapper.insert(adCreative);
					} else {
						baseMapper.updateById(adCreative);
					}
//					adCreativeMaterialService.batchSaveAdCreativeMaterial(adCreative);
				}
				//baseMapper.batchInsertAdCreative(adCreativeList);
			}
		} catch (Exception e) {
			log.error("getAdCreative is error", e);
			throw new BusinessException(11, "服务器异常，请稍后重试");
		}
	}


	private List<AdCreative> pullAdCreative(String advertiserId, Long adId, String ids) throws Exception {

		List<AdCreative> returnList = new ArrayList<>();

		if (StringUtils.isBlank(advertiserId)) {
			throw new Exception("推广账号不能为空");
		}
		String token = ttAccesstokenService.fetchAccesstoken(advertiserId);
		if (StringUtils.isBlank(token)) {
			throw new Exception("token获取为空");
		}
		int page = 1;
		while (true) {
			Map<String, Object> creativeParam = new HashMap<>();
			creativeParam.put("advertiser_id", advertiserId);
			creativeParam.put("page", page);
			creativeParam.put("page_size", "100");
			Map<String, Object> jsonParam = new HashMap<>();
			if (null != adId) {
				jsonParam.put("ad_id", adId);
			}
			if (StringUtils.isNotBlank(ids)) {
				long[] idArr = Arrays.stream(ids.split(",")).mapToLong(s -> Long.parseLong(s)).toArray();
				jsonParam.put("ids", idArr);
			}
			creativeParam.put("filtering", JSONObject.toJSON(jsonParam));
			log.info("广告创意列表request:" + JSONObject.toJSON(creativeParam));
			String resultStr = OEHttpUtils.doGet(adThirdUrl + advCreativeUrl, creativeParam, token);
			log.info("广告创意列表response:" + resultStr);
			ResponseBean resBean = JSON.parseObject(resultStr, ResponseBean.class);
			if (resBean != null && "0".equals(resBean.getCode())) {
				JSONObject jsonObj = resBean.getData();
				String listStr = jsonObj.getString("list");
				String page_info = jsonObj.getString("page_info");
				List<AdCreative> list = JSONObject.parseArray(listStr, AdCreative.class);
				returnList.addAll(list);
				JSONObject jsonObject = JSONObject.parseObject(page_info);
				Long total_page = jsonObject.getLong("total_page");
				if (page >= total_page) {
					break;
				}
				page++;
			} else {
				log.info("获取广告创意异常：{},广告账户：{}", resBean.getMessage(), advertiserId);
				break;
			}
		}
		return returnList;
	}


}
