package com.pig4cloud.pig.ads.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdAccountWarningSetting;

public interface AdAccountWarningSettingService extends IService<AdAccountWarningSetting> {


}
