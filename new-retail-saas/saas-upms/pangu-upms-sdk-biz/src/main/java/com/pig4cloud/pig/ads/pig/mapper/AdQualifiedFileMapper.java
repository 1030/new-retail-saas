package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.api.entity.AdQualifiedFileDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity AdQualifiedFileDO
 */
@Mapper
public interface AdQualifiedFileMapper extends BaseMapper<AdQualifiedFileDO> {

	IPage<AdQualifiedFileDO> selectPage(Page page, @Param("record") AdQualifiedFileDO record);

}




