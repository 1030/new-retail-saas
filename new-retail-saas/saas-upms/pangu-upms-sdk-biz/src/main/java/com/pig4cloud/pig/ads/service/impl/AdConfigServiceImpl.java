/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service.impl;

import com.pig4cloud.pig.ads.pig.mapper.AdAdverAuthMapper;
import com.pig4cloud.pig.ads.service.AdConfigService;
import com.pig4cloud.pig.ads.service.AdUserAdverService;
import com.pig4cloud.pig.api.vo.AdConfigReqVo;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


/**
 * @广告账户 服务实现类
 * @author john
 *
 */
@Service
@RequiredArgsConstructor
public class AdConfigServiceImpl implements AdConfigService {

	private final AdAdverAuthMapper adAdverAuthMapper;
	
	private final AdUserAdverService adUserAdverService;

	@Override
	public R save(AdConfigReqVo adConfigReqVo) {
		return null;
	}
}
