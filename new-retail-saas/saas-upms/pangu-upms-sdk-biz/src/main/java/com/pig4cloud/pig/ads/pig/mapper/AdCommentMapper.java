package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdCommentDTO;
import com.pig4cloud.pig.api.dto.OperateComment;
import com.pig4cloud.pig.api.entity.AdCommentEntity;
import com.pig4cloud.pig.api.vo.AdCommentVo;
import com.pig4cloud.pig.api.vo.TtTreeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdCommentMapper extends BaseMapper<AdCommentEntity> {

	/**
	 * 评论分页列表
	 *
	 * @param dto
	 * @return
	 */
	IPage<AdCommentVo> selectPageList(AdCommentDTO dto);

	/**
	 * 批量修改评论回复状态
	 *
	 * @param reply_status
	 * @return
	 */
	Integer batchUpdateReplyStatus(@Param("operateComments") List<OperateComment> operateComments, @Param("reply_status") String reply_status);

	/**
	 * 根据id修改回复状态
	 *
	 * @return
	 */
	Integer updateReplyStatusById(@Param("id") Long id, @Param("reply_status") String reply_status);

	/**
	 * 批量修改置顶标识
	 *
	 * @param operateComments
	 * @return
	 */
	Integer batchUpdateStick(@Param("operateComments") List<OperateComment> operateComments, @Param("stick") Integer stick);

	/**
	 * 根据id修改置顶
	 */
	Integer updateStickById(@Param("id") Long id, @Param("stick") Integer stick);


	/**
	 * 查询当前账号对应的广告计划列表
	 *
	 * @param advertiserIds
	 * @return
	 */
	List<TtTreeVo> getCommentAdPlan(@Param("advertiserIds") List<String> advertiserIds, @Param("name") String name);

}
