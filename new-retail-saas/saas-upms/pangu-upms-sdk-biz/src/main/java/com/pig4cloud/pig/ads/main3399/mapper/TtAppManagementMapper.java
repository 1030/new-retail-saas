package com.pig4cloud.pig.ads.main3399.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.TtAppManagement;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName TtAppManagementMapper.java
 * @createTime 2021年08月04日 16:55:00
 */
public interface TtAppManagementMapper extends BaseMapper<TtAppManagement> {
}
