package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.entity.GdtCustomAudiencesFile;


/**
 * @projectName:pig
 * @description:广点通人群文件信息
 * @author:Zhihao
 * @createTime:2020/12/5 21:00
 * @version:1.0
 */
public interface GdtCustomAudiencesFileService extends IService<GdtCustomAudiencesFile> {


}
