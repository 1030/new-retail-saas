/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service.impl;

import com.pig4cloud.pig.ads.pig.mapper.AdAdverAuthMapper;
import com.pig4cloud.pig.ads.pig.mapper.AdvertiserMapper;
import com.pig4cloud.pig.ads.service.AdAdverAuthService;
import com.pig4cloud.pig.ads.service.AdUserAdverService;
import com.pig4cloud.pig.api.enums.AuthTypeEnum;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.api.entity.AdAdverAuth;
import com.pig4cloud.pig.api.entity.AdUserAdver;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;

import lombok.RequiredArgsConstructor;


/**
 * @广告账户 服务实现类
 * @author john
 *
 */
@Service
@RequiredArgsConstructor
public class AdAdverAuthServiceImpl extends ServiceImpl<AdAdverAuthMapper, AdAdverAuth> implements AdAdverAuthService {

	private final AdAdverAuthMapper adAdverAuthMapper;
	
	private final AdvertiserMapper advertiserMapper;
	
	private final AdUserAdverService adUserAdverService;

	@Override
	public R auth(AdAdverAuth auth) {
		int num = adAdverAuthMapper.updateById(auth);
		if(num >0) {
			AdAdverAuth nauth = adAdverAuthMapper.selectById(auth.getId()); 
			Integer userId = nauth.getCreateuser();
			String advertiserId = nauth.getAdvertiserId();
			String platformId = nauth.getPlatformId();
			
			AdUserAdver entity = new AdUserAdver();
			entity.setUserId(userId);
			entity.setAdvertiserId(advertiserId);
			entity.setAuthType(AuthTypeEnum.AUTH_ADD.getValue());
			entity.setPlatformId(platformId);
			entity.setCreateuser(SecurityUtils.getUser().getId());
			return R.ok(adUserAdverService.saveOrUpdate(entity));
		}
		return R.failed("批准广告账户操作失败");
	}

}
