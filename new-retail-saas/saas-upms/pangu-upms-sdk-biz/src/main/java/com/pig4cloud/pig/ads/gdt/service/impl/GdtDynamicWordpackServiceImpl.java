package com.pig4cloud.pig.ads.gdt.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.gdt.service.GdtDynamicWordpackService;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtDynamicWordpackMapper;
import com.pig4cloud.pig.api.entity.GdtDynamicWordpack;
import com.pig4cloud.pig.api.vo.GdtDynamicWordpackVo;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @description:
 * @author: nml
 * @time: 2020/12/3 15:37
 **/
@Log4j2
@Service
@RequiredArgsConstructor
public class GdtDynamicWordpackServiceImpl extends ServiceImpl<GdtDynamicWordpackMapper, GdtDynamicWordpack> implements GdtDynamicWordpackService {
	@Autowired
	private GdtDynamicWordpackService gdtDynamicWordpackService;

	@Autowired
	private GdtDynamicWordpackMapper gdtDynamicWordpackMapper;

	//分页列表
	@Override
	public R findPage(GdtDynamicWordpackVo req) {
		//设置分页条件
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());
		//设置where条件
		QueryWrapper<GdtDynamicWordpack> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("isdelete",0);
		//请求 动态词包类型
		if (null != req.getType() && StringUtils.isNoneBlank(req.getType())){
			queryWrapper.eq("type",req.getType());
		}
		IPage<GdtDynamicWordpack> data = null;
		try {
			data = gdtDynamicWordpackMapper.selectPage(page, queryWrapper);
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed(data,"请求失败");
		}
		return R.ok(data,"请求成功");
	}

	//列表
	@Override
	public R findList(GdtDynamicWordpackVo req) {
		QueryWrapper<GdtDynamicWordpack> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("isdelete",0);
		//请求 动态词包类型
		if (null != req.getType() && StringUtils.isNoneBlank(req.getType())){
			queryWrapper.eq("type",req.getType());
		}
		List<GdtDynamicWordpack> data = null;
		try {
			data = gdtDynamicWordpackMapper.selectList(queryWrapper);
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed(data,"请求失败");
		}
		return R.ok(data,"请求成功");
	}
}
