package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.FreeCrowdPackDto;
import com.pig4cloud.pig.api.vo.FreeCrowdVo;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.api.dto.SelfCustomAudiencePushDTO;
import com.pig4cloud.pig.api.entity.SelfCustomAudience;

public interface SelfCustomAudienceService extends IService<SelfCustomAudience> {

	public R push(SelfCustomAudiencePushDTO dto);

	IPage<FreeCrowdVo> getFreeCrowdPackList(Page page, FreeCrowdPackDto dto);

	R downloadFreeCrowdPack(FreeCrowdPackDto dto);

	R getFreeCrowdList(FreeCrowdPackDto dto);
}
