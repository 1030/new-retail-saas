package com.pig4cloud.pig.ads.clickhouse3399.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.FreeCrowdPackDto;
import com.pig4cloud.pig.api.vo.FreeCrowdPackVo;
import com.pig4cloud.pig.api.vo.FreeCrowdVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdFreeCrowdPack extends BaseMapper<FreeCrowdVo> {

	IPage<FreeCrowdVo> getFreeCrowdPackListPage(IPage<FreeCrowdVo> pge, @Param("audienceDTO") FreeCrowdPackDto audienceDTO);

	List<FreeCrowdPackVo> getFreeCrowdPackList(@Param("audienceDTO") FreeCrowdPackDto dto);
}
