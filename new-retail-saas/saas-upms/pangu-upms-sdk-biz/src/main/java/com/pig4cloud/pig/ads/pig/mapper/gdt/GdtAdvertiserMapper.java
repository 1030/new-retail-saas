package com.pig4cloud.pig.ads.pig.mapper.gdt;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtAdvertiser;

@Mapper
public interface GdtAdvertiserMapper extends BaseMapper<GdtAdvertiser> {

}
