package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.GdtBrandImage;
import com.pig4cloud.pig.api.vo.GdtBrandImageVo;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

/**
 * @description:
 * @author: nml
 * @time: 2020/12/4 15:19
 **/
public interface GdtBrandImageService extends IService<GdtBrandImage> {

	//分页模糊查询所有
	R findPage(GdtBrandImageVo req);

	//推送一张图片到广点通：当前账户下的所有广告账户。并入库
	R pushBrandImage(String[] advertiserIds,String[] names,MultipartFile[] imgFiles);

	List<String> pushPicGdt(GdtBrandImage gbi, File imgFile, List<String> authAdvList);

	List<String> pushPicGdtCopy(GdtBrandImage gbi, File imgFile, List<String> authAdvList);

}
