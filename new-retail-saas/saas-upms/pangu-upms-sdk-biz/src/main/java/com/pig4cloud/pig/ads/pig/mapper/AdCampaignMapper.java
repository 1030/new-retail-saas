package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.CampaignDto;
import com.pig4cloud.pig.api.entity.AdCampaign;
import com.pig4cloud.pig.api.util.Page;
import com.pig4cloud.pig.api.vo.AdCampaignStatisticVo;
import com.pig4cloud.pig.api.vo.AdCreativeStatisticVo;
import com.pig4cloud.pig.api.vo.AdPlanStatisticVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 投放平台-广告组 数据层
 * 
 * @author hma
 * @date 2020-11-07
 */
@Component
@Mapper
public interface AdCampaignMapper  extends BaseMapper<AdCampaign>
{

	/**
	 * 查询投放广告组数据报列表
	 *
	 * @param campaignDto 投放广告组数据报信息
	 * @return 投放广告组数据报集合
	 */
	Page selectAdCampaignStatisticList(IPage<AdCampaignStatisticVo> page, @Param("campaignDto") CampaignDto campaignDto);



	Page selectAdPlanStatisticList(Page page, @Param("campaignDto") CampaignDto campaignDto);

	Page selectAdCreativeStatisticList(Page page, @Param("campaignDto") CampaignDto campaignDto);

	/**
	 * 广告组报表数据合计
	 * @param campaignDto
	 * @return
	 */
	AdCampaignStatisticVo  selectAdCampaignStatisticList(@Param("campaignDto") CampaignDto campaignDto);
	/**
	 * 广告计划报表数据合计
	 * @param campaignDto
	 * @return
	 */
	AdPlanStatisticVo selectAdPlanStatisticList(@Param("campaignDto") CampaignDto campaignDto);
	/**
	 * 广告创意报表数据合计
	 * @param campaignDto
	 * @return
	 */
	AdCreativeStatisticVo selectAdCreativeStatisticList( @Param("campaignDto") CampaignDto campaignDto);

	/**
	 * 获取视频对应的地址
	 * @param videoId
	 * @return
     */
	 String  getVideoUrl(@Param("videoId") String videoId);

	/**
	 * 获取图片对应地址
	 * @param imageId
	 * @return
     */
	String  getImageUrl(@Param("imageId") String imageId);

	/**
	 * 通过广告推广组id修改数据
	 * @param adCampaign
	 * @return
     */
	Integer updateAdCampaign(AdCampaign adCampaign);
}