package com.pig4cloud.pig.ads.service.bd;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.bd.BdAdgroup;
import com.pig4cloud.pig.api.entity.bd.BdAdgroupReq;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;
import java.util.Set;

public interface BdAdgroupService extends IService<BdAdgroup> {

    void saveUpdateList(List<BdAdgroup> list);

	/**
	 * 广告计划修改出价
	 * @param req
	 * @return
	 */
	R updateBid(BdAdgroupReq req);

	/**
	 * 通过广告计划ID查询广告计划操作状态
	 * @param adIdSet
	 * @return
	 */
	R getPutStatus(Set<Long> adIdSet);

}
