package com.pig4cloud.pig.ads.service.kafka;

import com.alibaba.fastjson.JSON;
import com.pig4cloud.pig.api.util.MapUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Description 公共kafka实例
 * @Author chengang
 * @Date 2021/9/23
 */
@Slf4j
@Service
public class CommonKafkaServiceImpl implements CommonKafkaService {

	@Autowired
	private KafkaTemplate<String, Object> kafkaTemplate;

	@Async
	@Override
	public void send(String topic, Map<String, Object> map) {
		log.info("创建广告计划-上报动心START => {}", JSON.toJSONString(map));
		kafkaTemplate.send(topic, MapUtils.convertMapToByte(map));
		log.info("创建广告计划-上报动心END => 消息发送成功");
	}
}
