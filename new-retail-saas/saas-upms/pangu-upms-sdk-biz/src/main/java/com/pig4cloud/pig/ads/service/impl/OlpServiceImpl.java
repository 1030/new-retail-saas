package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.OlpMapper;
import com.pig4cloud.pig.ads.service.OlpService;
import com.pig4cloud.pig.api.entity.OrangeLandingPage;
import org.springframework.stereotype.Service;




/**
 * @落地页 服务实现类
 * @author yk
 */
@Service
public class OlpServiceImpl extends ServiceImpl<OlpMapper, OrangeLandingPage> implements OlpService {



}
