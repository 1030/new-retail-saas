package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdActionInterest;
import com.pig4cloud.pig.api.entity.GdtActionInterest;
import com.pig4cloud.pig.api.gdt.dto.GdtLabelDto;

import java.util.List;

public interface GdtActionInterestService  extends IService<GdtActionInterest> {
    void batchSaveGdtLabel(List<GdtLabelDto> gdtLabelDtos);

    void saveGdtLabelDto(GdtLabelDto gdtLabelDto);

    void updateGdtLabelDto(GdtLabelDto gdtLabelDto);

    boolean checkExist(Long tdParentId, Long tdId, String type);
	List<GdtActionInterest> getAllThirdIdAndNames();
}
