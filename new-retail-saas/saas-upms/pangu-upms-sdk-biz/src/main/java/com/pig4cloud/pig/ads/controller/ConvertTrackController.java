/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.api.vo.PlanGdtAttrAnalyseReportVo;
import com.pig4cloud.pig.api.vo.SelectConvertTrackReq;
import com.pig4cloud.pig.common.security.annotation.Inner;
import com.pig4cloud.pig.api.dto.ConvertTrackDto;
import com.pig4cloud.pig.api.dto.DeepbidRead;
import com.pig4cloud.pig.api.entity.ConvertTrack;
import com.pig4cloud.pig.ads.service.ConvertTrackService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ 广告账户 前端控制器
 * @author john
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/convert_track")
@Api(value = "convert_track", tags = "转化")
public class ConvertTrackController {

	private final ConvertTrackService convertTrackService;



	//新建
	@Inner
	@RequestMapping("/create")
	public R create(@RequestBody ConvertTrack record) {
		return convertTrackService.create(record);
	}


	//详情
	@RequestMapping("/detail")
	public R detail(Integer id) {
		return R.ok(convertTrackService.detail(id));
	}

	/**
	 * 分页查询广告账户信息
	 * @param page 分页对象
	 * @return 分页对象
	 */
	@GetMapping("/page")
	public R getRolePage(Page page, ConvertTrackDto dto) {
		return R.ok(convertTrackService.pagedConvertTrack(page, dto));
	}

	/**
	 * 分页查询广告账户信息
	 * @param page 分页对象
	 * @return 分页对象
	 */
	@GetMapping("/page_actived")
	public R pagedConvertTrackActived(Page page, ConvertTrackDto dto) {
		return R.ok(convertTrackService.pagedConvertTrackActived(page, dto));
	}

	//详情
	@RequestMapping("/deepbidRead")
	public R deepbidRead(@RequestBody DeepbidRead deepbidRead) {
		if (StringUtils.isBlank(deepbidRead.getAdvertiserId())) {
			return R.failed("广告账户ID不能为空");
		}
		return R.ok(convertTrackService.fetchDeepbidRead(deepbidRead));
	}

	@Inner
	@PostMapping("/selectInfoByKey")
	public R<ConvertTrack> selectInfoByKey(@RequestBody SelectConvertTrackReq req) {
		return R.ok(convertTrackService.selectInfoByKey(req));
	}

	@Inner
	@PostMapping("/selectListByKey")
	public R<List<ConvertTrack>> selectListByKey(@RequestBody SelectConvertTrackReq req) {
		return R.ok(convertTrackService.selectListByKey(req));
	}
}
