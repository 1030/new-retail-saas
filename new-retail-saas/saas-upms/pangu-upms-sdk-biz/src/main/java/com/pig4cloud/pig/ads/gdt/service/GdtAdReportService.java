package com.pig4cloud.pig.ads.gdt.service;

import com.pig4cloud.pig.api.gdt.vo.GdtAdVo;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @description:
 * @author: nml
 * @time: 2020/12/18 14:17
 **/

public interface GdtAdReportService {

	/*
	 *  查询广告创意列表数据
	 * */
	R getGdtAdReport(GdtAdVo gdtAdVo);

	/*
	 *  调广点通接口设置广告开关configured_status
	 * */
	R updateGdtAdOnOff(GdtAdVo req);

}
