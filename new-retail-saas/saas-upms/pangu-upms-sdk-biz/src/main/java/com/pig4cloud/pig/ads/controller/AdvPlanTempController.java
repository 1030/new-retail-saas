package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.AdPlanTempService;
import com.pig4cloud.pig.api.dto.AdActionInterestDTO;
import com.pig4cloud.pig.api.dto.AdPlanTempDto;
import com.pig4cloud.pig.api.dto.AdPlanTempDto.*;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;


/**
 * @author hma
 * @ 广告计划临时管理模块 前端控制器
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/planTemp")
@Api(value = "planTemp", tags = "广告计划管理临时模块")
@Validated
public class AdvPlanTempController {

	@Resource
	private AdPlanTempService adPlanTempService;

	/**
	 * 行为类目列表查询
	 *
	 * @param planTemp
	 * @return
	 */
	@SysLog("selectActionCategoryList")
	@PostMapping("/selectActionCategoryList")
	public R<List<AdActionInterestDTO>> selectActionCategoryList(@RequestBody @Validated(ActionCategory.class) AdPlanTempDto planTemp) throws IOException {
		List<AdActionInterestDTO> actionInterestList = adPlanTempService.selectActionCategoryList(planTemp);
		return R.ok(actionInterestList);
	}

	/**
	 * 行为关键词模糊查询
	 *
	 * @param planTemp
	 * @return
	 */
	@SysLog("selectActionKeywordList")
	@PostMapping("/selectActionKeywordList")
	public R<List<AdActionInterestDTO>> selectActionKeywordList(@RequestBody @Validated(ActionKeyword.class) AdPlanTempDto planTemp) throws IOException {
		List<AdActionInterestDTO> actionInterestList = adPlanTempService.selectActionKeywordList(planTemp);
		return R.ok(actionInterestList);
	}

	/**
	 * 兴趣类目列表查询
	 *
	 * @param planTemp
	 * @return
	 */
	@SysLog("selectInterestCategoryList")
	@PostMapping("/selectInterestCategoryList")
	public R<List<AdActionInterestDTO>> selectInterestCategoryList(@RequestBody @Validated(InterestCategory.class) AdPlanTempDto planTemp) throws IOException {
		List<AdActionInterestDTO> actionInterestList = adPlanTempService.selectInterestCategoryList(planTemp);
		return R.ok(actionInterestList);
	}

	/**
	 * 兴趣关键词模糊查询
	 *
	 * @param planTemp
	 * @return
	 */
	@SysLog("selectInterestKeywordList")
	@PostMapping("/selectInterestKeywordList")
	public R<List<AdActionInterestDTO>> selectInterestKeywordList(@RequestBody @Validated(InterestKeyword.class) AdPlanTempDto planTemp) throws IOException {
		List<AdActionInterestDTO> actionInterestList = adPlanTempService.selectInterestKeywordList(planTemp);
		return R.ok(actionInterestList);
	}

	/**
	 * 行为兴趣ID查询关键词列表
	 *
	 * @param planTemp
	 * @return
	 */
	@SysLog("selectActionInterestKeywordList")
	@PostMapping("/selectActionInterestKeywordList")
	public R<List<AdActionInterestDTO>> selectActionInterestKeywordList(@RequestBody @Validated(ActionInterestKeyword.class) AdPlanTempDto planTemp) throws IOException {
		List<AdActionInterestDTO> actionInterestList = adPlanTempService.selectActionInterestKeywordList(planTemp);
		return R.ok(actionInterestList);
	}

	/**
	 * 保存广告计划临时数据
	 *
	 * @param adPlanTempDto 保存对象
	 * @return 分页对象
	 */
	@SysLog("saveAdvPlanTemp")
	@RequestMapping("/saveAdvPlanTemp")
	public R saveAdvPlanTemp(@RequestBody AdPlanTempDto adPlanTempDto) {
		return adPlanTempService.saveAdPlanTempOperate(adPlanTempDto);
	}

	/**
	 * 查询用户广告计划上次新建数据
	 *
	 * @param adPlanTempDto 查询对象
	 * @return
	 */
	@SysLog("getAdvPlanTemp")
	@RequestMapping("/getAdvPlanTemp")
	public R getAdvPlanTemp(AdPlanTempDto adPlanTempDto) {
		return adPlanTempService.getUserPlanTemp(adPlanTempDto);
	}

}
