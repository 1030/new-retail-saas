package com.pig4cloud.pig.ads.gdt.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.gdt.service.GdtDynamicCreativeService;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtDynamicCreativeMapper;
import com.pig4cloud.pig.api.entity.GdtDynamicCreative;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 广点通动态创意表服务接口实现
 *
 * @author zjz
 * @since 2022-07-11 22:26:02
 * @description 由 zjz 创建
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class GdtDynamicCreativeServiceImpl extends ServiceImpl<GdtDynamicCreativeMapper, GdtDynamicCreative> implements GdtDynamicCreativeService {


	@Override
	public void saveOrUpdateList(List<GdtDynamicCreative> list){
		this.saveOrUpdateBatch(list,1000);
	}

}