package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdTtTransferDto;
import com.pig4cloud.pig.api.entity.AdTtTransfer;
import com.pig4cloud.pig.api.vo.AdTtadvertiserVO;
import com.pig4cloud.pig.common.core.util.R;

public interface AdTtTransferService extends IService<AdTtTransfer> {
	R addTtAdTransfer(AdTtTransferDto ato);

	AdTtadvertiserVO getTtBalance(Long advertiserIdFrom);
}
