package com.pig4cloud.pig.ads.controller.ks;

import com.pig4cloud.pig.ads.service.ks.KsAppService;
import com.pig4cloud.pig.api.entity.KsApp;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 快手应用表
 * @author  chenxiang
 * @version  2022-03-26 11:17:47
 * table: ks_app
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/ks/app")
public class KsAppController {
	
    private final KsAppService ksAppService;
	
	/**
	 * 快手创建应用
	 * @param app
	 * @return
	 */
	@Inner
	@RequestMapping("/create")
	public R<KsApp> create(@RequestBody KsApp app){
		// 校验入参
		R result = checkParam(app);
		if (0 != result.getCode()){
			return result;
		}
		return ksAppService.create(app);
	}
	/**
	 * 校验入参
	 * @param app
	 * @return
	 */
	public R checkParam(KsApp app){
		if (StringUtils.isBlank(app.getAdvertiserId())){
			return R.failed("广告账户不能为空");
		}
		if (StringUtils.isBlank(app.getAppVersion())){
			return R.failed("应用标记不能为空");
		}
		if (StringUtils.isBlank(app.getAppName())){
			return R.failed("应用名称不能为空");
		}
		if (StringUtils.isBlank(app.getImageToken())){
			return R.failed("图片token不能为空");
		}
		if (StringUtils.isBlank(app.getPackageName())){
			return R.failed("应用包名不能为空");
		}
		if (StringUtils.isBlank(app.getUrl())){
			return R.failed("应用下载地址不能为空");
		}
		return R.ok();
	}
}


