/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.pig4cloud.pig.ads.main3399.mapper.WanGameChannelInfoMapper;
import com.pig4cloud.pig.ads.pig.mapper.ConvertTrackMapper;
import com.pig4cloud.pig.ads.service.*;
import com.pig4cloud.pig.ads.utils.OEHttpUtils;
import com.pig4cloud.pig.api.dto.ConvertTrackDto;
import com.pig4cloud.pig.api.dto.DeepbidRead;
import com.pig4cloud.pig.api.entity.*;
import com.pig4cloud.pig.api.enums.ToutiaoConvertTrackFieldsEnum;
import com.pig4cloud.pig.api.enums.TtConvertSourceTypeEnum;
import com.pig4cloud.pig.api.enums.TtConvertTypeEnum;
import com.pig4cloud.pig.api.enums.TtDeepExternalActionEnum;
import com.pig4cloud.pig.api.vo.ConvertTrackVo;
import com.pig4cloud.pig.api.vo.SelectConvertTrackReq;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author john
 * @广告账户 服务实现类
 */
@Service
@RequiredArgsConstructor
public class ConvertTrackServiceImpl extends ServiceImpl<ConvertTrackMapper, ConvertTrack> implements ConvertTrackService {

	private Logger logger = LoggerFactory.getLogger(TtAccesstokenServiceImpl.class);

	private final ConvertTrackMapper convertTrackMapper;

	private final TtAccesstokenService ttAccesstokenService;

	private final TtAppManagementService ttAppManagementService;

	private final WanGameChannelInfoMapper wanGameChannelInfoMapper;

	private final AdvService advService;

	private final AdAssetsEventService adAssetsEventService;

	@Value("${convert_track_url_tt}")
	private String convertTrackUrlTt;

	@Override
	public Boolean removeAdvById(Integer id) {
		return convertTrackMapper.deleteById(id) > 0;
	}

	@Override
	public R create(ConvertTrack entity) {
		if (StringUtils.isBlank(entity.getAppId())
				|| StringUtils.isBlank(entity.getName())
				|| StringUtils.isBlank(entity.getAdAccount())
				|| entity.getConvertType() == null
				|| Objects.isNull(entity.getGameId())
				|| StringUtils.isBlank(entity.getDownloadUrl())
				|| StringUtils.isBlank(entity.getPackageName())) {
			return R.failed("name,appId,adAccount,gameId,packageName,convertType,downloadUrl,packageName are all required");
		}

		if (entity.getName().length() > 100) {
			R.failed("name can' be longer than 100");
		}

		if (StringUtils.isNotBlank(entity.getActionTrackUrl())) {
			if (entity.getActionTrackUrl().length() > 1500) {
				R.failed("actionTrackUrl can' be longer than 1500");
			}
			if (!entity.getActionTrackUrl().contains("https://")) {
				R.failed("监测链接仅支持 https 协议");
			}
		}

		if (StringUtils.isNotBlank(entity.getDisplayTrackUrl())) {
			if (entity.getDisplayTrackUrl().length() > 1500) {
				R.failed("displayTrackUrl can' be longer than 1500");
			}
			if (!entity.getDisplayTrackUrl().contains("https://")) {
				R.failed("监测链接仅支持 https 协议");
			}
		}

		if (StringUtils.isNotBlank(entity.getVideoPlayEffectiveTrackUrl())) {
			if (entity.getVideoPlayEffectiveTrackUrl().length() > 1500) {
				R.failed("videoPlayEffectiveTrackUrl can' be longer than 1500");
			}
			if (!entity.getVideoPlayEffectiveTrackUrl().contains("https://")) {
				R.failed("监测链接仅支持 https 协议");
			}
		}

		if (StringUtils.isNotBlank(entity.getVideoPlayDoneTrackUrl())) {
			if (entity.getVideoPlayDoneTrackUrl().length() > 1500) {
				R.failed("videoPlayDoneTrackUrl can' be longer than 1500");
			}
			if (!entity.getVideoPlayDoneTrackUrl().contains("https://")) {
				R.failed("监测链接仅支持 https 协议");
			}
		}

		if (StringUtils.isNotBlank(entity.getVideoPlayTrackUrl())) {
			if (entity.getVideoPlayTrackUrl().length() > 1500) {
				R.failed("videoPlayTrackUrl is can' be longer than 1500");
			}
			if (!entity.getVideoPlayTrackUrl().contains("https://")) {
				R.failed("监测链接仅支持 https 协议");
			}
		}

		ConvertTrack record = convertTrackMapper.selectOne(Wrappers.<ConvertTrack>query().lambda().eq(ConvertTrack::getName, entity.getName()));
		if (record != null) {
			return R.failed("转化跟踪的名称不能重复");
		}

		// 根据appid获取appname
		QueryWrapper<TtAppManagement> wrapper = new QueryWrapper<>();
		wrapper.eq("app_cloud_id", entity.getAppId());
		wrapper.eq("advertiser_id", entity.getAdAccount());
		wrapper.eq("search_type", "CREATE_ONLY");
		wrapper.last("LIMIT 1");
		TtAppManagement ttAppManagement = ttAppManagementService.getOne(wrapper);
		if (Objects.isNull(ttAppManagement)) {
			return R.failed("未获取到APPNAME");
		}
		entity.setAppName(ttAppManagement.getAppName());

		entity.setAdPlatform(Short.valueOf("1"));  //广告平台，默认 头条
		entity.setSyncStatus(Short.valueOf("0"));    //同步状态，默认 未同步
		entity.setStatus(Short.valueOf("0"));        //status 默认 未激活
		entity.setConvertSourceType(Short.valueOf("7"));        //转化来源类型 默认 AD_CONVERT_SOURCE_TYPE_SDK（应用下载SDK）
		entity.setAppType(Short.valueOf("1"));        //app_type 默认 APP_ANDROID
		entity.setCreatetime(new Date());
		entity.setPlatformId(Short.valueOf("1"));        //目前只支持 头条

		if (!SqlHelper.retBool(this.getBaseMapper().insert(entity))) {
			return R.failed("插入数据库失败");
		}

		//同步转化到 投放平台
		String resultStr = this.sync2ThirdPart(entity);

		//若同步失败
		JSONObject obj = JSON.parseObject(resultStr);
		if (!obj.getInteger("code").equals(0)) {
			entity.setSyncStatus(Short.valueOf("2"));
			;       //同步失败
			this.getBaseMapper().updateById(entity);        //设置失败状态到数据库

			String msg = obj.getString("message");
			for (ToutiaoConvertTrackFieldsEnum field : ToutiaoConvertTrackFieldsEnum.values()) {
				String replaceStr = String.format("%s:", field.getType());
				if (msg.contains(replaceStr)) {
					msg = msg.replace(replaceStr, field.getName());
					break;
				}
			}
			return R.failed("同步到投放平台失败" + msg);
		}

		//若同步成功	跟更新状态到数据库
		String dataStr = obj.getString("data");
		JSONObject dataObj = JSON.parseObject(dataStr);
		String thirtPartId = dataObj.getString("id");

		entity.setIdAdPlatform(thirtPartId);
		entity.setSyncStatus(Short.valueOf("1"));        //同步成功
		if (!SqlHelper.retBool(this.getBaseMapper().updateById(entity))) {
			return R.failed("同步到投放平台后，更新状态失败");
		}

		return R.ok("成功添加转化跟踪");
	}

	@Override
	public ConvertTrack detail(Integer id) {
		ConvertTrack record = convertTrackMapper.selectById(id);
		//拉取并更新转化状态
		this.getAndUpdateStatus(record);
		return record;
	}

	@Override
	public boolean increaseAdPlanCouont(Long id) {
		ConvertTrack record = convertTrackMapper.selectOne(Wrappers.<ConvertTrack>query().lambda().eq(ConvertTrack::getIdAdPlatform, String.valueOf(id)));

		//如果为null不需要设置
		if (record == null) {
			logger.error("未找到转换跟踪:" + id);
			return false;
		}

		record.setPlanCount(record.getPlanCount() + 1);
		return SqlHelper.retBool(convertTrackMapper.updateById(record));
	}

	@Override
	public IPage<ConvertTrackVo> pagedConvertTrack(Page page, ConvertTrackDto dto) {
		//TODO判断该账号是否为本人名下
		PigUser usr = SecurityUtils.getUser();
		Integer id = SecurityUtils.getUser().getId();
		//获取当前账号下 广告账户列表
		//List<String> adAccountList = advService.getOwnerAdv(id, null);
		Map<String, String> adAccountMap = advService.getOwnerAdvMap(id, PlatformTypeEnum.TT.getValue());

		if (adAccountMap == null || adAccountMap.size() == 0) {
			return new Page();
		}

		QueryWrapper<ConvertTrack> wrapper = new QueryWrapper<>();
		wrapper.orderByDesc("id");
		if (adAccountMap.size() > 0) {
			wrapper.in("ad_account", adAccountMap.keySet());
		} else {
			wrapper.eq("ad_account", "-1");
		}

		//搜索
		if (dto.getIdSearch() != null) {
			wrapper.like("id_ad_platform", dto.getIdSearch());
		}
		if (StringUtils.isNotBlank(dto.getNameSearchStr())) {
			wrapper.like("name", dto.getNameSearchStr());
		}
		if (dto.getAdvertiserIds() != null && dto.getAdvertiserIds().length > 0) {
			wrapper.in("ad_account", dto.getAdvertiserIds());
		}

		IPage<ConvertTrack> result = this.getBaseMapper().selectPage(page, wrapper);

		List<ConvertTrackVo> voList = new ArrayList<>();
		List<ConvertTrack> resultList = result.getRecords();
		if (CollectionUtils.isNotEmpty(resultList)) {
			//创建广告时仅仅获取信息流类型分包,app_chl字段是星图需求新加的，历史数据没有修复为空
			// 存储一个包转化多次，需去重
			for (ConvertTrack item : resultList) {
				ConvertTrackVo itemVo = new ConvertTrackVo();
				BeanUtils.copyProperties(item, itemVo);

				if (item.getGameId() != null && StringUtils.isNotBlank(item.getAppChl())) {
					WanGameChannelInfo channelInfo = wanGameChannelInfoMapper.selectOne(Wrappers.<WanGameChannelInfo>lambdaQuery()
							.eq(WanGameChannelInfo::getGameid,item.getGameId())
							.eq(WanGameChannelInfo::getChl,item.getAppChl()));
					if (channelInfo != null && channelInfo.getSpreadType().intValue()==2) {
						continue;
					}
				}
				itemVo.setAdAccountName(adAccountMap.get(itemVo.getAdAccount()));

				//拉取并更新转化状态
				//this.getAndUpdateStatus(itemVo);

				voList.add(itemVo);
			}
		}

		IPage<ConvertTrackVo> voPage = new Page<ConvertTrackVo>(result.getCurrent(), result.getSize(), voList.size());
		voPage.setRecords(voList);
		return voPage;
	}

	@Override
	public IPage<ConvertTrackVo> pagedConvertTrackActived(Page page, ConvertTrackDto dto) {
		Integer id = SecurityUtils.getUser().getId();
		//获取当前账号下 广告账户列表
		Map<String, String> adAccountMap = advService.getOwnerAdvMap(id, PlatformTypeEnum.TT.getValue());

		QueryWrapper<ConvertTrack> wrapper = new QueryWrapper<>();
		if (adAccountMap == null || adAccountMap.size() == 0) {
			return new Page();
		} else {
			wrapper.in("ad_account", adAccountMap.keySet());
		}

		wrapper.orderByDesc("id");
		wrapper.eq("ad_account", dto.getAdvertiserId());
		wrapper.eq("sync_status", 1);
		wrapper.eq("status", 1);
		//wrapper.eq("plan_count", 0);

		//搜索
		if (dto.getIdSearch() != null) {
			wrapper.like("id_ad_platform", dto.getIdSearch());
		}
		if (StringUtils.isNotBlank(dto.getNameSearchStr())) {
			wrapper.like("name", dto.getNameSearchStr());
		}
		if (StringUtils.isNotBlank(dto.getGameId())) {
			wrapper.eq("game_id", dto.getGameId());
		}

		//复制的时候获取
		Long adId = dto.getAdId();
		Long convertId = dto.getConvertId();
		if (adId != null && convertId != null) {
			ConvertTrack convertTrack = this.getOne(Wrappers.<ConvertTrack>query().lambda().eq(ConvertTrack::getIdAdPlatform, convertId).last("limit 1"));
			if (convertTrack != null) {
				dto.setConvertSourceType(convertTrack.getConvertSourceTypeEName());
				dto.setConvertType(convertTrack.getConvertTypeEName());
				dto.setDeepExternalAction(convertTrack.getDeepExternalActionEName());
				dto.setConvertDataType(convertTrack.getConvertDataType());
			}
		}

		logger.info("dto====>{}", JSON.toJSONString(dto));

		//转化来源类型, 目前只有AD_CONVERT_SOURCE_TYPE_SDK 7 应用下载SDK
		if (StringUtils.isNotBlank(dto.getConvertSourceType())) {
			Integer value = TtConvertSourceTypeEnum.valueByType(dto.getConvertSourceType());
			wrapper.eq(value != null, "convert_source_type", value);
		}
//		8  AD_CONVERT_TYPE_ACTIVE 激活
//		13 AD_CONVERT_TYPE_ACTIVE_REGISTER 注册
//		14 AD_CONVERT_TYPE_PAY 付费
//		30 关键行为
		if (StringUtils.isNotBlank(dto.getConvertType())) {
			Integer value = TtConvertTypeEnum.valueByType(dto.getConvertType());
			wrapper.eq(value != null, "convert_type", value);
		}
		//深度转化目标
		//DEEP_BID_DEFAULT匹配为null。查询数据库为null的记录。
		if (StringUtils.isNotBlank(dto.getDeepExternalAction())) {
			Integer value = TtDeepExternalActionEnum.valueByType(dto.getDeepExternalAction());
			if (value == null) {
				wrapper.isNull("deep_external_action");
			} else {
				wrapper.eq("deep_external_action", value);
			}
		} else {
			wrapper.isNull("deep_external_action");
		}
		//转化统计方式
		if(StringUtils.isNotBlank(dto.getConvertDataType())){
			wrapper.eq("convert_data_type", dto.getConvertDataType());
		}

		//logger.info("wrapper1====>{}", JSON.toJSONString(wrapper.getTargetSql()));
		logger.info("wrapper2====>{}", JSON.toJSONString(wrapper.getParamNameValuePairs()));

		IPage<ConvertTrack> result = this.getBaseMapper().selectPage(page, wrapper);

		List<ConvertTrackVo> voList = new ArrayList<>();
		List<ConvertTrack> resultList = result.getRecords();
		if (CollectionUtils.isNotEmpty(resultList)) {
			//创建广告时仅仅获取信息流类型分包,app_chl字段是星图需求新加的，历史数据没有修复为空
			// 存储一个包转化多次，需去重
			for (ConvertTrack item : resultList) {
				ConvertTrackVo itemVo = new ConvertTrackVo();
				BeanUtils.copyProperties(item, itemVo);
				itemVo.setAdAccountName(adAccountMap.get(itemVo.getAdAccount()));
				if (item.getGameId() != null && StringUtils.isNotBlank(item.getAppChl())) {
					WanGameChannelInfo channelInfo = wanGameChannelInfoMapper.selectOne(Wrappers.<WanGameChannelInfo>lambdaQuery()
							.eq(WanGameChannelInfo::getGameid,item.getGameId())
							.eq(WanGameChannelInfo::getChl,item.getAppChl()));
					if (channelInfo != null && channelInfo.getSpreadType().intValue()==2) {
						continue;
					}
				}
				voList.add(itemVo);
			}
		}

		IPage<ConvertTrackVo> voPage = new Page<ConvertTrackVo>(result.getCurrent(), result.getSize(), result.getTotal());
		voPage.setRecords(voList);
		return voPage;
	}

	//拉取并更新转化状态
	private void getAndUpdateStatus(ConvertTrack item) {
		//如果没有第三方 转化id，不用考虑状态
		if (StringUtils.isBlank(item.getIdAdPlatform())) {
			return;
		}

		//只有状态为 未激活，才需要同步状态
		if (!item.getStatus().equals(Short.valueOf("0"))) {
			return;
		}

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", item.getAdAccount());
		data.put("convert_id", item.getIdAdPlatform());

		String token = ttAccesstokenService.fetchAccesstoken(item.getAdAccount());  //this.getAccesstoken(item);
		//String accessToken = "3e09cc34a5812083ad9bd2f9eca6659fe2aadaa6";

		String resultStr = OEHttpUtils.doGet(convertTrackUrlTt + "read/", data, token);

		JSONObject obj = JSON.parseObject(resultStr);
		String dataStr = obj.getString("data");
		if (StringUtils.isBlank(dataStr)) {
			return;
		}

		JSONObject dataObj = JSON.parseObject(dataStr);
		//dataObj.put("status", "AD_CONVERT_STATUS_ACTIVE");		//test
		if (StringUtils.isNotBlank(dataObj.getString("status"))
				&& dataObj.getString("status").equals("AD_CONVERT_STATUS_ACTIVE")) {
			item.setStatus(Short.valueOf("1"));

			ConvertTrackVo temp = new ConvertTrackVo();
			temp.setId(item.getId());
			item.setStatus(Short.valueOf("1"));
			convertTrackMapper.updateById(item);
		}

	}

	private String sync2ThirdPart(ConvertTrack entity) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("advertiser_id", entity.getAdAccount());
		data.put("name", entity.getName());
		data.put("download_url", entity.getDownloadUrl());
		data.put("app_type", entity.getappTypeEName());
		data.put("package_name", entity.getPackageName());

		data.put("app_id", entity.getAppId());
		data.put("app_name", entity.getAppName());

		data.put("convert_type", entity.getConvertTypeEName());
		data.put("convert_source_type", entity.getConvertSourceTypeEName());
		if (StringUtils.isNotBlank(entity.getConvertDataType())) {
			data.put("convert_data_type", entity.getConvertDataType());
		}

		if (Objects.nonNull(entity.getDeepExternalAction())) {
			data.put("deep_external_action", entity.getDeepExternalActionEName());
		}
		if (StringUtils.isNotBlank(entity.getActionTrackUrl())) {
			data.put("action_track_url", entity.getActionTrackUrl());
		}

		if (StringUtils.isNotBlank(entity.getDisplayTrackUrl())) {
			data.put("display_track_url", entity.getDisplayTrackUrl());
		}

		if (StringUtils.isNotBlank(entity.getVideoPlayEffectiveTrackUrl())) {
			data.put("video_play_effective_track_url", entity.getVideoPlayEffectiveTrackUrl());
		}

		if (StringUtils.isNotBlank(entity.getVideoPlayDoneTrackUrl())) {
			data.put("video_play_done_track_url", entity.getVideoPlayDoneTrackUrl());
		}

		if (StringUtils.isNotBlank(entity.getVideoPlayTrackUrl())) {
			data.put("video_play_track_url", entity.getVideoPlayTrackUrl());
		}

		String token = ttAccesstokenService.fetchAccesstoken(entity.getAdAccount());
		//String token = "de15a73cdea9757727999f6905e4d8b6349e64a1";

		logger.info("创建转化跟踪data：{}", JSON.toJSONString(data));

		String resultStr = OEHttpUtils.doPost(convertTrackUrlTt + "create/", data, token);
		logger.info("创建转化跟踪resultStr：{}", resultStr);
		return resultStr;
	}

	@Override
	public List<String> fetchDeepbidRead(DeepbidRead deepbidRead) {
		List<String> list = new ArrayList<>();
		String token = ttAccesstokenService.fetchAccesstoken(deepbidRead.getAdvertiserId());

		Map<String, Object> data = JSONObject.parseObject(JSON.toJSONString(deepbidRead), new TypeReference<Map<String, Object>>() {
		});

		logger.info("查询深度优化方式data：{}", JSON.toJSONString(data));

		String resultStr = OEHttpUtils.doGet(convertTrackUrlTt + "deepbid/read/", data, token);
		logger.info("查询深度优化方式resultStr：{}", resultStr);
		ResponseBean resBean = JSON.parseObject(resultStr, ResponseBean.class);
		//调用成功
		if (resBean != null && "0".equals(resBean.getCode())) {
			JSONObject dataJSON = resBean.getData();
			String success_list_str = dataJSON.getString("success_list");
			if (StringUtils.isNotBlank(success_list_str)) {
				list = JSON.parseArray(success_list_str, String.class);
			}
		}
		//每次付费接口不返回，工单结果：接口和媒体侧存在差异
		if (StringUtils.isNotBlank(deepbidRead.getAssetId())
				&& StringUtils.isNotBlank(deepbidRead.getEventType())
				&& "active_pay".equals(deepbidRead.getEventType())) {
			//获取事件对应的转化统计方式--20220518媒体创建事件已经不需要设置首次|每次，但是一个事件仍然只能添加一次
//			AdAssetsEvent adAssetsEvent = adAssetsEventService.getOne(Wrappers.<AdAssetsEvent>lambdaQuery()
//					.eq(AdAssetsEvent::getDeleted,0)
//					.eq(AdAssetsEvent::getAdvertiserId,deepbidRead.getAdvertiserId())
//					.eq(AdAssetsEvent::getAssetId,deepbidRead.getAssetId())
//					.eq(AdAssetsEvent::getEventType,deepbidRead.getEventType()));
//			if (adAssetsEvent != null && "EVERY_ONE".equals(adAssetsEvent.getStatisticalType())) {
//				list.add("BID_PER_ACTION");
//			}
			list.add("BID_PER_ACTION");
			//首次付费-枚举值头条还没有更新，自创一个先满足业务,前台选择首次付费是不传值即可
			list.add("BID_FIRST_ACTION");
		}
		return list;
	}

	@Override
	public ConvertTrack selectInfoByKey(SelectConvertTrackReq req) {
		ConvertTrack info = null;
		if (null != req && null != req.getMonitorId() && req.getMonitorId() > 0) {
			info = convertTrackMapper.selectOne(new QueryWrapper<ConvertTrack>().eq("monitor_id", req.getMonitorId()).last("LIMIT 1"));
		}
		if (null == info) {
			if (null != req && StringUtils.isNotBlank(req.getName())) {
				info = convertTrackMapper.selectOne(new QueryWrapper<ConvertTrack>().eq("name", req.getName()).last("LIMIT 1"));
			}
		}
		return info;
	}

	@Override
	public List<ConvertTrack> selectListByKey(SelectConvertTrackReq req) {
		return convertTrackMapper.selectList(Wrappers.<ConvertTrack>lambdaQuery().in(ConvertTrack::getMonitorId, req.getAdIds()));
	}

	/**
	 * 根据广告账号查询对应授权token
	 * @param entity
	 * @return
	 */
/*	public String getAccesstoken(ConvertTrack entity){
		AdvertiserInfo advertiserInfo=  advertiserMapper.selectAdvertiserByAdAccount(entity.getAdAccount());
		String housekeeper = advertiserInfo.getHousekeeper();

		//根据housekeeper获取accessToken
		String tokenContent = stringRedisTemplate.opsForValue().get(Constants.OCEANENGINE_REDIS_TOKEN_KEY_PRIX_ + housekeeper);
		Accesstoken accessToken = JSON.parseObject(tokenContent, Accesstoken.class);
		String token = accessToken.getAccessToken();
		return token;
	}*/

}
