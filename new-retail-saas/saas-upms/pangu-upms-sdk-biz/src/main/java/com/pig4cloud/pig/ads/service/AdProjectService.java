package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdProject;

/**
 * 项目表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:20:40
 * table: ad_project
 */
public interface AdProjectService extends IService<AdProject> {
}


