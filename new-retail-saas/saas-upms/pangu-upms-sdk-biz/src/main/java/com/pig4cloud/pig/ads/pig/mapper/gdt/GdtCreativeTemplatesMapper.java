package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtCreativeTemplates;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 创意形式 数据层
 * 
 * @author hma
 * @date 2020-12-16
 */
@Mapper
public interface GdtCreativeTemplatesMapper  extends BaseMapper<GdtCreativeTemplates>
{

	/**
     * 查询创意形式信息
     * 
     * @param adcreativeTemplateId 创意形式ID
     * @return 创意形式信息
     */
	public GdtCreativeTemplates selectGdtCreativeTemplatesById(Long adcreativeTemplateId);
	
	/**
     * 查询创意形式列表
     * 
     * @param gdtCreativeTemplates 创意形式信息
     * @return 创意形式集合
     */
	public List<GdtCreativeTemplates> selectGdtCreativeTemplatesList(GdtCreativeTemplates gdtCreativeTemplates);
	
	/**
     * 新增创意形式
     * 
     * @param gdtCreativeTemplates 创意形式信息
     * @return 结果
     */
	public int insertGdtCreativeTemplates(GdtCreativeTemplates gdtCreativeTemplates);
	
	/**
     * 修改创意形式
     * 
     * @param gdtCreativeTemplates 创意形式信息
     * @return 结果
     */
	public int updateGdtCreativeTemplates(GdtCreativeTemplates gdtCreativeTemplates);
	

	
	/**
     * 批量添加创意形式
     * 
     * @param gdtCreativeTemplatesList 需要添加的数据集合
     * @return 结果
     */
	public int batchInsertGdtCreativeTemplates(List<GdtCreativeTemplates> gdtCreativeTemplatesList);
	
}