package com.pig4cloud.pig.ads.service.ks;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.KsAccesstoken;
import com.pig4cloud.pig.api.entity.KsAccountToken;
import com.pig4cloud.pig.api.vo.KsAccesstokenReq;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 快手授权token表
 * @author  chenxiang
 * @version  2022-03-22 20:29:09
 * table: ks_accesstoken
 */
public interface KsAccesstokenService extends IService<KsAccesstoken> {
	/**
	 * 账户授权
	 * @param req
	 * @return
	 */
	R saveAuth(KsAccesstokenReq req);
}


