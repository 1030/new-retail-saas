package com.pig4cloud.pig.ads.clickhouse3399.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.gdt.dto.GdtAdCreativeReport;
import com.pig4cloud.pig.api.gdt.vo.GdtAdCreativeVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nml
 * @version 1.0.0
 * @Description
 * @createTime 2020年12月15日 17:42:00
 */
@Mapper
public interface GdtAdCreativeReportMapper {

	//广点通广告创意列表+报表数据
	IPage<GdtAdCreativeReport> selectGdtAdCreativeReport(GdtAdCreativeVo req);

}
