package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.AdProjectService;
import com.pig4cloud.pig.api.vo.AdProjectReq;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 项目表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:20:40
 * table: ad_project
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/adProject")
public class AdProjectController {
	
    private final AdProjectService adProjectService;
	
	@RequestMapping("/list")
	public R list(AdProjectReq record){
		return R.ok();
	}
}


