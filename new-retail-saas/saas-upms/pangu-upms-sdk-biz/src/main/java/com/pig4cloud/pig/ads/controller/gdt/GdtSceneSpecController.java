package com.pig4cloud.pig.ads.controller.gdt;

import cn.hutool.bloomfilter.bitMap.IntMap;
import com.alibaba.fastjson.JSON;
import com.pig4cloud.pig.ads.gdt.service.GdtSceneSpecService;
import com.pig4cloud.pig.ads.gdt.service.GdtSelectListService;
import com.pig4cloud.pig.api.enums.GdtScenPositionTagEnum;
import com.pig4cloud.pig.api.enums.GdtUnionPositionPackageTypeEnum;
import com.pig4cloud.pig.api.gdt.dto.GdtTargetingTags;
import com.pig4cloud.pig.api.gdt.dto.GdtUnionPackageDto;
import com.pig4cloud.pig.api.util.Constants;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author zhuxm
 * @广点通定向标签
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/sceneSpec")
@Api(value = "/sceneSpec", tags = "广点通优量汇场景定向")
public class GdtSceneSpecController {
	@Autowired
	private GdtSceneSpecService gdtSceneSpecService;
	@Autowired
	private final StringRedisTemplate stringRedisTemplate;

	/**
	 * v2.3版本暂时取消此功能
	 *
	 * @param accountId
	 * @return
	 */
	@RequestMapping("/mobile_union_category")
	public R getMobileUnionCategory(String accountId) {
		return gdtSceneSpecService.getGdtSenPositionTags(accountId, GdtScenPositionTagEnum.MOBILE_UNION_CATEGORY.getType());
	}

	/**
	 * 获取流量包
	 *
	 * @param accountId
	 * @param type      流量包类型  GdtUnionPositionPackageTypeEnum
	 * @return
	 */
	@RequestMapping("/union_position_packages")
	public R getUnionPositionPackagesInclude(String accountId, String type, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "25") int pageSize) {
		if (StringUtils.isBlank(type)) {
			return R.failed("流量包类型不能为空");
		}
		if (!GdtUnionPositionPackageTypeEnum.containsType(type)) {
			return R.failed(String.format("不支持流量包类型：%s", type));
		}

		return gdtSceneSpecService.getGdtUnionPositionPackages(accountId, type, page, pageSize);
	}

	/**
	 * 创建流量包
	 *
	 * @param gdtUnionPackageDto
	 * @return
	 */
	@PostMapping("/union_position_packages/create")
	public R getUnionPositionPackagesInclude(@Validated @RequestBody GdtUnionPackageDto gdtUnionPackageDto) {
		if (StringUtils.isBlank(gdtUnionPackageDto.getUnion_package_type())) {
			return R.failed("流量包类型不能为空");
		}
		if (!GdtUnionPositionPackageTypeEnum.containsType(gdtUnionPackageDto.getUnion_package_type())) {
			return R.failed(String.format("不支持流量包类型：%s", gdtUnionPackageDto.getUnion_package_type()));
		}

		return gdtSceneSpecService.createGdtUnionPositionPackages(gdtUnionPackageDto);
	}
}
