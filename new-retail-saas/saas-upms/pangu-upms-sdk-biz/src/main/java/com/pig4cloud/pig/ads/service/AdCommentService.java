package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdCommentDTO;
import com.pig4cloud.pig.api.dto.AdCommentOperateDto;
import com.pig4cloud.pig.api.dto.AdCommentStatisticsDTO;
import com.pig4cloud.pig.api.entity.AdCommentEntity;
import com.pig4cloud.pig.api.vo.AdCommentStatisticsVO;
import com.pig4cloud.pig.api.vo.AdCommentVo;
import com.pig4cloud.pig.common.core.util.R;

import java.text.ParseException;

public interface AdCommentService extends IService<AdCommentEntity> {

	/**
	 * 定时更新评论屏蔽词
	 *
	 * @param param
	 * @throws Exception
	 */
	void updateCommentTermsBanned(String param) throws Exception;

	/**
	 * 评论统计分页列表
	 *
	 * @param req
	 * @return
	 */
	Page<AdCommentStatisticsDTO> getCommentStatisticsPage(AdCommentStatisticsVO req);

	/**
	 * 获取评论列表
	 *
	 * @param comment
	 * @return
	 */
	Page<AdCommentEntity> getCommentPage(AdCommentVo comment);

	/**
	 * 回复评论
	 *
	 * @param comment
	 */
	void commentReply(AdCommentVo comment) throws ParseException;

	/**
	 * 隐藏评论
	 *
	 * @param comment
	 */
	void commentHide(AdCommentVo comment);

	/**
	 * 评论置顶操作
	 *
	 * @param comment
	 */
	void commentStickOnTop(AdCommentVo comment);

	/**
	 * 评论用户屏蔽
	 *
	 * @param comment
	 */
	void commentBlockUsers(AdCommentVo comment);


	@Deprecated
	IPage<AdCommentVo> selectPageList(AdCommentDTO dto);

	/**
	 * 评论回复
	 *
	 * @param dto
	 * @return
	 */
	@Deprecated
	R commentReply(AdCommentOperateDto dto);

	/**
	 * 评论隐藏
	 *
	 * @param dto
	 * @return
	 */
	@Deprecated
	R commentHide(AdCommentOperateDto dto);

	/**
	 * 置顶评论
	 *
	 * @param dto
	 * @return
	 */
	@Deprecated
	R commentStick(AdCommentOperateDto dto);

	/**
	 * 获取当前账号可查询的广告计划
	 *
	 * @return
	 */
	R getCommentAdPlanTree(String name);
}
