/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pig4cloud.pig.api.entity.AdIndustry;
import com.pig4cloud.pig.api.vo.AdIndustryRes;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 巨量行业列表
 *
 * @author yuwenfeng
 * @date 2022-02-08 15:28:03
 */
@Mapper
public interface AdIndustryMapper extends BaseMapper<AdIndustry> {

	List<AdIndustryRes> selectIndustryResList(@Param(Constants.WRAPPER) QueryWrapper<AdIndustry> queryWrapper);

	@Update("DELETE FROM ad_industry")
	void deleteAllIndustry();
}
