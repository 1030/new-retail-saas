package com.pig4cloud.pig.ads.controller.ks;

import com.pig4cloud.pig.ads.service.ks.KsUnitService;
import com.pig4cloud.pig.api.entity.KsApp;
import com.pig4cloud.pig.api.entity.ks.KsPlanReq;
import com.pig4cloud.pig.api.util.StringUtils;
import com.pig4cloud.pig.common.core.util.CommonUtils;
import com.pig4cloud.pig.common.core.util.ECollectionUtil;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

/**
 * 快手应用表
 * @author  leisw
 * @version  2022-08-08 10:17:47
 * table: ks_app
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/ksPlan")
public class KsPlanController {
	
    private final KsUnitService ksUnitService;
	
	/**
	 * 修改广告计划状态
	 * @param req
	 * @return
	 */
	@RequestMapping("/updateStatus")
	public R updateStatus(@RequestBody KsPlanReq req){
		// 校验入参
		if (null == req) {
			return R.failed("参数不能为空");
		}
		Long advertiserId = req.getAdvertiserId();
		if (Objects.isNull(advertiserId)) {
			return R.failed("广告账户id不能为空");
		}
		if (null == req.getUnitId()) {
			return R.failed("广告计划id不能为空");
		}
		if (null == req.getMediaType()) {
			return R.failed("媒体类型不能为空");
		}
		if (1 == req.getMediaType()){
			if (StringUtils.isBlank(req.getVersionType())){
				return R.failed("当为头条时新老广告计划判断字段不能为空");
			}
		}
		if (null == req.getOptStatus()){
			return R.failed("操作状态不允许为空");
		}
		return ksUnitService.updateStatus(req);
	}

	/**
	 * 修改广告计划的预算
	 * @param req
	 * @return
	 */
	@RequestMapping("/updateDayBudget")
	public R updateDayBudget(@RequestBody KsPlanReq req){
		// 校验入参
		if (null == req) {
			return R.failed("参数不能为空");
		}
		Long advertiserId = req.getAdvertiserId();
		if (Objects.isNull(advertiserId)) {
			return R.failed("广告账户id不能为空");
		}
		if (null == req.getUnitId()) {
			return R.failed("广告计划id不能为空");
		}
		if (null == req.getType()) {
			return R.failed("修改预算类型不能为空");
		}
		if (null == req.getBudget()) {
			return R.failed("预算金额不能为空");
		}
		if (!CommonUtils.isMoney(req.getBudget().toString(),2)) {
			return R.failed("预算金额必须是纯数字,且小数点后两位");
		}
		return ksUnitService.updateDayBudget(req);
	}


	/**
	 * 修改广告计划的出价
	 * @param req
	 * @return
	 */
	@RequestMapping("/updateBid")
	public R updateBid(@RequestBody KsPlanReq req){
		// 校验入参
		if (null == req) {
			return R.failed("参数不能为空");
		}
		Long advertiserId = req.getAdvertiserId();
		if (Objects.isNull(advertiserId)) {
			return R.failed("广告账户id不能为空");
		}
		if (null == req.getUnitId()) {
			return R.failed("广告计划id不能为空");
		}
		if (null == req.getCpaBid()) {
			return R.failed("出价值不能为空");
		}
		return ksUnitService.updateBid(req);
	}



	/**
	 * 通过广告计划ID查询广告计划操作状态
	 * @param adIds
	 * @returnput_status
	 */
	@GetMapping("/getPutStatus")
	public R getPutStatus(@RequestParam String adIds){
		// 校验入参
		Set<Long> adIdSet = ECollectionUtil.stringToLongSet(adIds);
		if (adIdSet.isEmpty()) {
			return R.ok(Collections.emptyList());
		}
		return ksUnitService.getPutStatus(adIdSet);
	}




}


