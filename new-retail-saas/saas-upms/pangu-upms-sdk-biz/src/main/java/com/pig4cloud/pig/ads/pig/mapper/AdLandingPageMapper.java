package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdLandingPage;
import org.apache.ibatis.annotations.Mapper;

/**
 * 落地页表
 * @author  chenxiang
 * @version  2022-03-18 10:59:34
 * table: ad_landing_page
 */
@Mapper
public interface AdLandingPageMapper extends BaseMapper<AdLandingPage> {
}


