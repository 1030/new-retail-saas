package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.ads.service.AdAwemeBannedService;
import com.pig4cloud.pig.api.dto.AdAwemeBannedDto;
import com.pig4cloud.pig.api.entity.AdAwemeBannedEntity;
import com.pig4cloud.pig.api.vo.AdAwemeBannedVo;
import com.pig4cloud.pig.api.vo.AdAwemeBannedVo.Add;
import com.pig4cloud.pig.api.vo.AdAwemeBannedVo.Delete;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/aweme")
@RequiredArgsConstructor
@Validated
public class AdAwemeBannedController {

	private final AdAwemeBannedService adAwemeBannedService;

	/**
	 * 屏蔽用户列表
	 *
	 * @param req
	 * @return
	 */
	@SysLog("屏蔽用户列表")
	@PostMapping("/getAwemePage")
	public R<Page<AdAwemeBannedEntity>> getAwemePage(@RequestBody @Validated AdAwemeBannedVo req) {
		Page<AdAwemeBannedEntity> page = adAwemeBannedService.getAwemePage(req);
		return R.ok(page);
	}

	/**
	 * 添加屏蔽用户
	 *
	 * @param req
	 * @return
	 */
	@SysLog("添加屏蔽用户")
	@PostMapping("/addAweme")
	public R addAweme(@RequestBody @Validated(Add.class) AdAwemeBannedVo req) {
		try {
			adAwemeBannedService.addAweme(req);
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 删除屏蔽用户
	 *
	 * @param req
	 * @return
	 */
	@SysLog("删除屏蔽用户")
	@PostMapping("/deleteAweme")
	public R deleteAweme(@RequestBody @Validated(Delete.class) AdAwemeBannedVo req) {
		try {
			adAwemeBannedService.deleteAweme(req.getId());
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}


	/**
	 * 分页列表
	 *
	 * @param req
	 * @return
	 */
	@Deprecated
	@RequestMapping("/getPage")
	public R getPage(AdAwemeBannedDto req) {
		//设置分页条件
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());
		IPage<AdAwemeBannedVo> data = adAwemeBannedService.selectPageList(req);
		return R.ok(data, "请求成功");
	}

	/**
	 * 添加屏蔽用户
	 *
	 * @param dto
	 * @param bindingResult
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/add")
	public R awemeAdd(@RequestBody @Valid AdAwemeBannedDto dto, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return R.failed(bindingResult.getFieldError().getDefaultMessage());
		}
		String banned_type = dto.getBanned_type();
		if (StringUtils.isEmpty(banned_type)) {
			return R.failed("banned_type为空");
		}
		if ("CUSTOM_TYPE".equals(banned_type)) {//自定义规则
			List<String> list = dto.getKeyword_list();
			if (!(null != list && list.size() > 0)) {
				return R.failed("keyword_list为空");
			}
		} else if ("AWEME_TYPE".equals(banned_type)) {//根据抖音id屏蔽
			if (StringUtils.isEmpty(dto.getAweme_id())) {
				return R.failed("aweme_id为空");
			}
		} else {
			return R.failed("banned_type不符合规则类型");
		}
		return adAwemeBannedService.addAweme(dto);
	}

	/**
	 * 删除屏蔽用户
	 *
	 * @param dto
	 * @param bindingResult
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/delete")
	public R awemeDelete(@RequestBody AdAwemeBannedDto dto, BindingResult bindingResult) {
		if (null == dto.getId()) {
			return R.failed("id为空");
		}
		return adAwemeBannedService.deleteAweme(dto);
	}
}
