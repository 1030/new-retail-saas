package com.pig4cloud.pig.ads.pig.mapper.xingtu;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.xingtu.XingtuOrderDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * 星图任务订单明细表
 * @author  chengang
 * @version  2021-12-13 16:48:32
 * table: xingtu_order_detail
 */
@Mapper
public interface XingtuOrderDetailMapper extends BaseMapper<XingtuOrderDetail> {
	
}


