package com.pig4cloud.pig.ads.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdvCreativeDetailMapper;
import com.pig4cloud.pig.ads.service.AdCreativeMaterialService;
import com.pig4cloud.pig.ads.service.AdvCreativeServiceDetailService;
import com.pig4cloud.pig.ads.service.TtAccesstokenService;
import com.pig4cloud.pig.ads.utils.CheckUtil;
import com.pig4cloud.pig.ads.utils.OEHttpUtils;
import com.pig4cloud.pig.api.entity.AdCreativeDetailNewDO;
import com.pig4cloud.pig.api.entity.DetailAdData;
import com.pig4cloud.pig.api.entity.ResponseBean;
import com.pig4cloud.pig.api.entity.TtCreativeDetail;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Service
@RequiredArgsConstructor
public class AdvCreativeServiceDetailServiceImpl extends ServiceImpl<AdvCreativeDetailMapper, TtCreativeDetail> implements AdvCreativeServiceDetailService {

	private final TtAccesstokenService ttAccesstokenService;

	private final AdCreativeMaterialService adCreativeMaterialService;

	@Value("${ad_third_url}")
	private String adThirdUrl;

	@Override
	public TtCreativeDetail SyncAdCreativeDetail(String advertiserId, Long adId) throws Exception {
		TtCreativeDetail ttCreativeDetail = null;
		String token = ttAccesstokenService.fetchAccesstoken(advertiserId);
		if (StringUtils.isBlank(token)) {
			throw new Exception("token获取为空");
		}
		Map<String, String> params = new HashMap<>();
		params.put("advertiser_id", advertiserId);
		params.put("ad_id", adId+"");
		log.info("广告创意详情request:" + JSONObject.toJSON(params));
		String resultStr = OEHttpUtils.doGetQueryString(adThirdUrl + "/open_api/v3.0/creative/detail/get/", params, token);
		log.info("广告创意详情结果=====>ttCreativeDetail={}", resultStr);
		ResponseBean resBean = JSON.parseObject(resultStr, ResponseBean.class);
		if (resBean != null && "0".equals(resBean.getCode())) {
			JSONObject jsonObj = resBean.getData();
			AdCreativeDetailNewDO adCreativeDetailNew = JSON.parseObject(jsonObj.toJSONString(), AdCreativeDetailNewDO.class);
			DetailAdData adData = adCreativeDetailNew.getAdData();
			ttCreativeDetail = new TtCreativeDetail();
			BeanUtils.copyProperties(adData,ttCreativeDetail);
			ttCreativeDetail.setAdId(adCreativeDetailNew.getAdId());
			ttCreativeDetail.setAdvertiserId(adCreativeDetailNew.getAdvertiserId());
			ttCreativeDetail.setCreativeList(adCreativeDetailNew.getCreativeList());
			ttCreativeDetail.setCreative(adCreativeDetailNew.getCreative());
			ttCreativeDetail.setCreativeMaterialMode(StringUtils.isBlank(CheckUtil.deal(adCreativeDetailNew.getCreativeList(),2))?"STATIC_ASSEMBLE":"INTERVENE");
		}
		return ttCreativeDetail;
	}

	@Override
	public void saveAdCreativeDetailList(String advertiserId, Long adId) {
		try {
			TtCreativeDetail ttCreativeDetail = SyncAdCreativeDetail(advertiserId, adId);
			if (ttCreativeDetail != null) {
				this.saveOrUpdate(ttCreativeDetail);
				// 更新创意与素材关系信息
				adCreativeMaterialService.batchUpsertOeCreativeMaterial(Collections.singletonList(ttCreativeDetail));
			}
		} catch (Throwable e) {
			log.error("保存广告创意详情失败", e);
		}
	}

}
