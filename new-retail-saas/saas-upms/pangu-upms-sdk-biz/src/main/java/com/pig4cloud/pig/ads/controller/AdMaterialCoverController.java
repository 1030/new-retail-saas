/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.AdMaterialCoverService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.api.vo.AdMaterialCoverVo;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


/**
 * 素材视频封面图
 *
 * @author pigx code generator
 * @date 2021-06-23 14:32:25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/materialCover" )
@Api(value = "materialCover", tags = "素材视频封面图管理")
public class AdMaterialCoverController {

    private final  AdMaterialCoverService adMaterialCoverService;

	/**
	 * 分页
	 * @param req
	 * @return
	 */
	@RequestMapping("/getPage")
	public R getPage(@RequestBody AdMaterialCoverVo req){
		if (StringUtils.isBlank(req.getMaterialId())){
			return R.failed(null,"未获取到素材ID");
		}
		return adMaterialCoverService.getPage(req);
	}
	/**
	 * 封面图片推送
	 * @param req
	 * @return
	 */
	@SysLog("视频封面同步")
	@RequestMapping("/pushCover")
    public R pushCover(@RequestBody AdMaterialCoverVo req) throws IOException {
		if (StringUtils.isBlank(req.getIds())){
			return R.failed(null,"未获取到封面图片ID");
		}
		if (StringUtils.isBlank(req.getPlatformId())){
			return R.failed(null,"未获取到平台类型");
		}
		if (StringUtils.isBlank(req.getAdvertiserIds())){
			return R.failed(null,"未获取到广告账户");
		}
    	return adMaterialCoverService.pushCover(req);
	}

}
