package com.pig4cloud.pig.ads.service.tag.impl;
import java.util.*;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.config.SdkProperties;
import com.pig4cloud.pig.ads.pig.mapper.tag.AdTagRelateMapper;
import com.pig4cloud.pig.ads.service.tag.AdTagRelateService;
import com.pig4cloud.pig.api.entity.tag.BatchSettingTagDto;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.pig4cloud.pig.api.entity.tag.AdTagRelate;

import java.util.stream.Collectors;

/**
 * 标签关联表服务接口实现
 *
 * @author zjz
 * @since 2023-02-23 11:39:32
 * @description 由 zjz 创建
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class AdTagRelateServiceImpl extends ServiceImpl<AdTagRelateMapper, AdTagRelate> implements AdTagRelateService {
    private final AdTagRelateMapper adTagRelateMapper;
	private final SdkProperties sdkProperties;
	@Override
	public  R batchSettingTag(BatchSettingTagDto dto){
		if(StringUtils.isBlank(dto.getRelateIds())){
			return R.failed("IDS不能为空");
		}
		Integer tagLimit = sdkProperties.getTagLimit();
		if(StringUtils.isNotBlank(dto.getTagIds()) && dto.getTagIds().split(",").length > tagLimit){
			return R.failed("最多选择"+tagLimit+"个标签");
		}
		if(dto.getType() == null){
			return R.failed("类型不能为空");
		}
		List<Long> idsList = Arrays.stream(dto.getRelateIds().split(",")).map(Long::valueOf).collect(Collectors.toList());

		List<AdTagRelate> oldList = adTagRelateMapper.selectAllListByIds(idsList,dto.getType());

		Map<String,Long> oldMap = oldList.stream().collect(Collectors.toMap(a-> a.getRelateId()+"-"+a.getTagId()+"-"+a.getType(),AdTagRelate::getId));
		Long userid = SecurityUtils.getUser().getId().longValue();
		List<AdTagRelate> addlist = new ArrayList<>();
		List<Long> upadteIds = new ArrayList<>();
		Date now = new Date();
		idsList.forEach(a->{
			if(StringUtils.isBlank(dto.getTagIds())){
				return;
			}
			Arrays.stream(dto.getTagIds().split(",")).forEach(b->{
				AdTagRelate adTagRelate = new AdTagRelate();
				adTagRelate.setTagId(Long.valueOf(b));
				adTagRelate.setRelateId(a);
				adTagRelate.setType(dto.getType());
				String mapkey = a+"-"+b+"-"+dto.getType();
				if(oldMap.containsKey(mapkey)){
					adTagRelate.setId(oldMap.get(mapkey));
					upadteIds.add(oldMap.get(mapkey));
				}else {
					adTagRelate.setCreateTime(now);
					adTagRelate.setCreateId(userid);
				}
				adTagRelate.setUpdateTime(now);
				adTagRelate.setUpdateId(userid);
				adTagRelate.setDeleted(0);
				addlist.add(adTagRelate);
			});
		});
		//将之前这次没有更新的数据 设置删除
		List<AdTagRelate> delList = oldList.stream().filter(a-> !upadteIds.contains(a.getId())&&a.getDeleted()==0).collect(Collectors.toList());
		delList.forEach(a->a.setDeleted(1));
		addlist.addAll(delList);
		this.saveOrUpdateBatch(addlist);
		return R.ok();
	}

}