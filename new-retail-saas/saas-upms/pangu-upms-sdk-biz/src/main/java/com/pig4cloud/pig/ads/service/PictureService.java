package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.UpdatePicTypeDto;
import com.pig4cloud.pig.api.entity.PictureLib;
import com.pig4cloud.pig.api.vo.PictureLibVo;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description:
 * @author: nml
 * @time: 2020/10/29 16:20
 **/
public interface PictureService extends IService<PictureLib> {

	//动态修改图片名称或修改标签，图片名称唯一。（20201210：改成名称可重复）
	R updateSelective(PictureLib pictureLib);

	//删除图片或批量删除图片
	R delSelective(PictureLibVo pictureLibVo);

	//图片库上传图片
	R filesUpload(MultipartFile[] file);

	//头条：广告创意选择一张图片：判断是否推送。推送 or 不推送
	R oneFileSyn(PictureLibVo req);

	//广点通：广告创意选择一张图片：判断是否推送
	R oneFileSynGdt(PictureLibVo req);

	/**
	 * 更新图片类型
	 */
	R updatePType(UpdatePicTypeDto updatePicTypeDto);
}
