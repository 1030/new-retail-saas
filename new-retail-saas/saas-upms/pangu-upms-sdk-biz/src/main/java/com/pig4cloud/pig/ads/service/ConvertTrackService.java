/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.ConvertTrackDto;
import com.pig4cloud.pig.api.dto.DeepbidRead;
import com.pig4cloud.pig.api.entity.ConvertTrack;
import com.pig4cloud.pig.api.vo.ConvertTrackVo;
import com.pig4cloud.pig.api.vo.SelectConvertTrackReq;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;


/**
 * @
 * @author john
 *
 */
public interface ConvertTrackService extends IService<ConvertTrack> {

	/**
	 * @删除广告
	 * @param id 广告账户ID
	 */
	Boolean removeAdvById(Integer id);

	IPage<ConvertTrackVo> pagedConvertTrack(Page page, ConvertTrackDto dto);

	IPage<ConvertTrackVo> pagedConvertTrackActived(Page page, ConvertTrackDto dto);

	R create(ConvertTrack entity);

	ConvertTrack detail(Integer id);

	boolean increaseAdPlanCouont(Long id);

	List<String> fetchDeepbidRead(DeepbidRead deepbidRead);

	ConvertTrack selectInfoByKey(SelectConvertTrackReq req);

	List<ConvertTrack> selectListByKey(SelectConvertTrackReq req);
}
