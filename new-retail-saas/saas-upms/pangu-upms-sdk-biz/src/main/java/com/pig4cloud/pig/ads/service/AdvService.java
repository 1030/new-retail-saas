/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.BudgetDto;
import com.pig4cloud.pig.api.entity.AdAccount;
import com.pig4cloud.pig.api.entity.Advertising;
import com.pig4cloud.pig.api.vo.NameValuePairVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;
import java.util.Map;


/**
 * @
 * @author john
 *
 */
public interface AdvService extends IService<Advertising> {

	/**
	 * @删除广告
	 * @param id 广告账户ID
	 */
	Boolean removeAdvById(Integer id);

	List<NameValuePairVo> options4AdAccounts(String searchStr);


	/**
	 * 修改日预算
	 * @param budgetDto
	 * @return
     */
	R  budgetUpdate(BudgetDto budgetDto);

	/**
	 * 查询广告账号(广告账号，推广组，广告计划)预约当天定时设置预算值
	 * @param advertiserId
	 * @return
	 */
	 R getAdvertiserJobBudget(Long advertiserId,Long campaignId,Long adId,Integer operateType);
	 
	 
	 
	 /**
	  * 获取自身的广告账户列表  
	 * @param platformId
	 * @return
	 */
	List<String> getOwnerAdv(Integer userId, String platformId);

	/**
	 * @可授权的账号列表
	 * @return
	 */
	List<String> getAuthAdvList();


	Map<String, String> getOwnerAdvMap(Integer userId, String platformId);

	List<AdAccount> getAccountList(Integer mediaType,String advertiserId);

	List<String> getThrowUserList();


}
