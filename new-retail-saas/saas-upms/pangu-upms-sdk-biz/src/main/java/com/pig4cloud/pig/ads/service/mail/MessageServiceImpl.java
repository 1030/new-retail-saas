package com.pig4cloud.pig.ads.service.mail;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.pig4cloud.pig.ads.utils.MailSendUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description 消息服务，暂仅提供邮件服务，其他项目有需要的可以有本服务提供fegin接口
 * @Author chengang
 * @Date 2021/7/16
 */
@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MailSendUtil mailSendUtil;

	@Override
	public void sendMail(String mailSubject, String emailContent, List<String> sendToList) {
			if (CollectionUtils.isNotEmpty(sendToList)) {
				String sendTo = String.join(";",sendToList);
				mailSendUtil.sendMail(mailSubject,emailContent,sendTo);
			}
	}
}
