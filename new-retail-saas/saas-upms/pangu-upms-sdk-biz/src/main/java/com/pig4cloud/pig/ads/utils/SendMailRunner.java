package com.pig4cloud.pig.ads.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
  *  发送邮箱线程
 * @author zxm
 */
public class SendMailRunner implements Runnable {

	private static Logger logger = LoggerFactory.getLogger(SendMailRunner.class);

	private MailSender mailSender;

	public SendMailRunner(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	@Override
	public void run() {
		try {
			boolean flag = mailSender.sendBatch();
	        if (flag) {
	        	logger.info("邮件发送成功！");
	        } else {
	        	logger.info("邮件发送失败！");
	        }
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
