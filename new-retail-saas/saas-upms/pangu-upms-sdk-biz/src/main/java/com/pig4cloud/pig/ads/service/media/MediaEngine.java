package com.pig4cloud.pig.ads.service.media;


import com.pig4cloud.pig.api.entity.ads.AdsCallBackResponse;
import com.pig4cloud.pig.common.core.util.R;

import java.util.Map;

/**
 * @Description 媒体渠道引擎
 * @Author chengang
 * @Date 2022/12/13
 */
public interface MediaEngine {


	/**
	 * 回调处理接口
	 * @param response 回调原始参数
	 * @param paramMap 回调扩展参数
	 * @return
	 */
    default R callback(AdsCallBackResponse response, Map<String, Object> paramMap) {return null;}

}
