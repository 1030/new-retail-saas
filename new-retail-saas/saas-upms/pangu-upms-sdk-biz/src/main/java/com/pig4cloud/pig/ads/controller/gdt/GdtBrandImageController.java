package com.pig4cloud.pig.ads.controller.gdt;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.pig4cloud.pig.ads.gdt.service.GdtBrandImageService;
import com.pig4cloud.pig.api.entity.GdtBrandImage;
import com.pig4cloud.pig.api.vo.GdtBrandImageVo;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description:
 * @author: nml
 * @time: 2020/12/4 20:14
 **/

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/gdt/brandImage")
@Api(value = "/gdt/brandImage", tags = "广点通品牌形象模块")
public class GdtBrandImageController {

	private final GdtBrandImageService gdtBrandImageService;

	//分页列表
	@RequestMapping("/page")
	public R findAll(GdtBrandImageVo req){
		return gdtBrandImageService.findPage(req);
	}

	//上传推送
	@RequestMapping("/push")
	public R push(String[] advertiserIds,String[] names,MultipartFile[] imgFiles){
		return gdtBrandImageService.pushBrandImage(advertiserIds,names,imgFiles);
	}

	/**
	 * 获取品牌形象下拉框
	 * @param platformId
	 * @param accountId
	 * @return
	 */
	@GetMapping("getList")
	public R getList(@RequestParam(value = "platformId") Integer platformId,@RequestParam(value = "accountId") String accountId) {
		return R.ok(gdtBrandImageService.list(Wrappers.<GdtBrandImage>lambdaQuery()
				.eq(GdtBrandImage::getIsdelete, 0)
				.eq(GdtBrandImage::getWidth, "512")
				.eq(GdtBrandImage::getHeight, "512")
				.eq(GdtBrandImage::getPlatformId, platformId)
				.eq(GdtBrandImage::getAccountId,accountId)
				.orderByDesc(GdtBrandImage::getId)));
	}


	@GetMapping("/getImageUrlByImageId")
	public R getImageUrlByImageId(@RequestParam(value = "imageId") String imageId,@RequestParam(value = "accountId") String accountId){
		return R.ok( gdtBrandImageService.getOne(Wrappers.<GdtBrandImage>lambdaQuery()
				.eq(GdtBrandImage::getIsdelete, 0)
				.eq(GdtBrandImage::getImageId, imageId)
				.eq(GdtBrandImage::getAccountId,accountId)
				.last("LIMIT 1")));
	}


}
