package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.api.dto.UserByMaterialResp;
import com.pig4cloud.pig.api.entity.AdTitleLib;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface AdTitleLibMapper extends BaseMapper<AdTitleLib> {

	/**
	 * 查询标题分页列表
	 *
	 * @param page
	 * @param record
	 * @return
	 */
	Page<AdTitleLib> selectTitlePage(Page page, @Param("record") AdTitleLib record);

	/**
	 * 查询标题列表
	 *
	 * @param record
	 * @return
	 */
	List<AdTitleLib> selectTitleList(@Param("record") AdTitleLib record);


	List<UserByMaterialResp> getCreateuser();

//    int deleteByPrimaryKey(@Param("id") Long id);
//    int insert(AdTitleLib record);
//    int insertSelective(AdTitleLib record);
//    AdTitleLib selectByPrimaryKey(@Param("id") Long id);
//    int updateByPrimaryKeySelective(AdTitleLib record);
//    int updateByPrimaryKey(AdTitleLib record);
}