/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdAgentMapper;
import com.pig4cloud.pig.ads.pig.mapper.AdAgentRebateMapper;
import com.pig4cloud.pig.ads.service.AdAgentRebateService;
import com.pig4cloud.pig.ads.service.AdAgentService;
import com.pig4cloud.pig.ads.utils.DateUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import com.pig4cloud.pig.api.entity.AdAgent;
import com.pig4cloud.pig.api.entity.AdAgentRebate;
import com.pig4cloud.pig.api.vo.AdAgentRebateVo;
import com.pig4cloud.pig.api.vo.AdAgentVo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jpedal.parser.shape.B;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 代理商表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
@Service
@RequiredArgsConstructor
public class AdAgentServiceImpl extends ServiceImpl<AdAgentMapper, AdAgent> implements AdAgentService {

	private final AdAgentRebateService adAgentRebateService;

	private final AdAgentMapper adAgentMapper;

	private final AdAgentRebateMapper adAgentRebateMapper;

	/**
	 * 分页查询
	 * @param req
	 * @return
	 */
	@Override
	public R getPage(AdAgentVo req){
		QueryWrapper<AdAgent> wrapper = new QueryWrapper<>();
		wrapper.eq("is_delete",0);
		IPage<AdAgent> iPage = this.page(req,wrapper);
		return R.ok(iPage);
	}
	/**
	 * 代理商列表
	 * @param req
	 * @return
	 */
	@Override
	public R getList(AdAgentVo req){
		QueryWrapper<AdAgent> wrapper = new QueryWrapper<>();
		wrapper.eq("is_delete",0);
		List<AdAgent> list = this.list(wrapper);
		return R.ok(list);
	}
	/**
	 * 添加
	 * @param req
	 * @return
	 */
	@Override
	public R add(AdAgentVo req){
		AdAgent adAgent = new AdAgent();
		adAgent.setAgentName(req.getAgentName());
		adAgent.setCreateUser(String.valueOf(SecurityUtils.getUser().getId()));
		adAgent.setCreateTime(new Date());
		adAgent.setUpdateUser(String.valueOf(SecurityUtils.getUser().getId()));
		adAgent.setUpdateTime(new Date());
		boolean flag = this.save(adAgent);
		if (flag){
			return R.ok();
		}
		return R.failed();
	}
	/**
	 * 编辑
	 * @param req
	 * @return
	 */
	@Override
	public R edit(AdAgentVo req){
		AdAgent adAgent = new AdAgent();
		adAgent.setId(Integer.parseInt(req.getId()));
		adAgent.setAgentName(req.getAgentName());
		adAgent.setUpdateUser(String.valueOf(SecurityUtils.getUser().getId()));
		adAgent.setUpdateTime(new Date());
		boolean flag = this.updateById(adAgent);
		if (flag){
			return R.ok();
		}
		return R.failed();
	}
	/**
	 * 删除
	 * @param req
	 * @return
	 */
	@Override
	public R del(AdAgentVo req){
		AdAgent adAgent = new AdAgent();
		adAgent.setId(Integer.parseInt(req.getId()));
		adAgent.setIsDelete(1);
		adAgent.setUpdateUser(String.valueOf(SecurityUtils.getUser().getId()));
		adAgent.setUpdateTime(new Date());
		boolean flag = this.updateById(adAgent);
		if (flag){
			return R.ok();
		}
		return R.failed();
	}
	/**
	 * 返点列表 - 分页
	 * @param req
	 * @return
	 */
	@Override
	public R getRebatePage(AdAgentRebateVo req){
		IPage<AdAgentRebate> iPage =  adAgentRebateMapper.selectByPage(req);
		return R.ok(iPage);
	}
	/**
	 * 返点列表
	 * @param req
	 * @return
	 */
	@Override
	public R getRebateList(AdAgentRebateVo req){
		QueryWrapper<AdAgentRebate> wrapper = new QueryWrapper<>();
		wrapper.eq("agent_id",req.getAgentId());
		wrapper.eq("is_delete",0);
		List<AdAgentRebate> rebateList = adAgentRebateService.list(wrapper);

		QueryWrapper<AdAgent> adAgentQueryWrapper = new QueryWrapper<>();
		adAgentQueryWrapper.eq("is_delete",0);
		// 查询代理商
		List<AdAgent> adAgentList = adAgentMapper.selectList(adAgentQueryWrapper);
		for (AdAgentRebate adAgentRebate : rebateList) {
			for (AdAgent adAgent: adAgentList) {
				if (adAgentRebate.getAgentId().intValue() == adAgent.getId().intValue()){
					adAgentRebate.setAgentName(adAgent.getAgentName());
					break;
				}
			}
		}

		return R.ok(rebateList);
	}
	/**
	 * 添加返点
	 * @param req
	 * @return
	 */
	@Override
	public R addRebate(AdAgentRebateVo req){
		QueryWrapper<AdAgentRebate> wrapper = new QueryWrapper<>();
		wrapper.eq("is_delete",0);
		wrapper.eq("agent_id",req.getAgentId());
		wrapper.eq("channel_code",req.getChannelCode());
		wrapper.apply("date_format(effective_time,'%Y-%m-%d') = date_format('"+req.getEffectiveTime()+"','%Y-%m-%d')");
		int num = adAgentRebateService.count(wrapper);
		if (num > 0){
			return R.failed("生效日期不能重叠");
		}

		AdAgentRebate rebate = new AdAgentRebate();
		rebate.setAgentId(Integer.parseInt(req.getAgentId()));
		rebate.setAgentName(req.getAgentName());
		rebate.setPlatformId(Integer.valueOf(req.getPlatformId()));
		rebate.setChannelCode(req.getChannelCode());
		rebate.setChannelName(req.getChannelName());
		rebate.setRebate(new BigDecimal(req.getRebate()));
		rebate.setEffectiveTime(DateUtils.stringToDate(req.getEffectiveTime(),DateUtils.YYYY_MM_DD_HH_MM_SS));
		rebate.setCreateUser(String.valueOf(SecurityUtils.getUser().getId()));
		rebate.setCreateTime(new Date());
		rebate.setUpdateUser(String.valueOf(SecurityUtils.getUser().getId()));
		rebate.setUpdateTime(new Date());
		boolean flag = adAgentRebateService.save(rebate);
		if (flag){
			return R.ok();
		}
		return R.failed();
	}
	/**
	 * 编辑返点
	 * @param req
	 * @return
	 */
	@Override
	public R editRebate(AdAgentRebateVo req){
		QueryWrapper<AdAgentRebate> wrapper = new QueryWrapper<>();
		wrapper.notIn("id",req.getId());
		wrapper.eq("is_delete",0);
		wrapper.eq("agent_id",req.getAgentId());
		wrapper.eq("channel_code",req.getChannelCode());
		wrapper.apply("date_format(effective_time,'%Y-%m-%d') = date_format('"+req.getEffectiveTime()+"','%Y-%m-%d')");
		int num = adAgentRebateService.count(wrapper);
		if (num > 0){
			return R.failed("生效日期不能重叠");
		}

		AdAgentRebate rebate = new AdAgentRebate();
		rebate.setId(Integer.valueOf(req.getId()));
		rebate.setPlatformId(Integer.valueOf(req.getPlatformId()));
		rebate.setChannelCode(req.getChannelCode());
		rebate.setChannelName(req.getChannelName());
		rebate.setRebate(new BigDecimal(req.getRebate()));
		rebate.setEffectiveTime(DateUtils.stringToDate(req.getEffectiveTime(),DateUtils.YYYY_MM_DD_HH_MM_SS));
		rebate.setUpdateUser(String.valueOf(SecurityUtils.getUser().getId()));
		rebate.setUpdateTime(new Date());
		boolean flag = adAgentRebateService.updateById(rebate);
		if (flag){
			return R.ok();
		}
		return R.failed();
	}
	/**
	 * 删除返点
	 * @param req
	 * @return
	 */
	@Override
	public R delRebate(AdAgentRebateVo req){
		AdAgentRebate rebate = new AdAgentRebate();
		rebate.setId(Integer.valueOf(req.getId()));
		rebate.setIsDelete(1);
		rebate.setUpdateUser(String.valueOf(SecurityUtils.getUser().getId()));
		rebate.setUpdateTime(new Date());
		boolean flag = adAgentRebateService.updateById(rebate);
		if (flag){
			return R.ok();
		}
		return R.failed();
	}

}
