package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdConvert;
import com.pig4cloud.pig.api.vo.AdConvertReq;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 广告转化表
 * @author  chenxiang
 * @version  2022-05-23 10:35:26
 * table: ad_convert
 */
public interface AdConvertService extends IService<AdConvert> {
	/**
	 * 新增转化归因
	 * @param record
	 * @return
	 */
	R createGdtConversions(AdConvert record);

	/**
	 * 下拉列表
	 * @param record
	 * @return
	 */
	R getList(AdConvert record);
	/**
	 * 根据advertiser_monitor_info主键获取列表
	 * @param record
	 * @return
	 */
	R getAdConvertListInner(AdConvertReq record);

	/**
	 * 百度-新增转化
	 * @return
	 */
	R addOcpcTransFeed(AdConvertReq record);
}


