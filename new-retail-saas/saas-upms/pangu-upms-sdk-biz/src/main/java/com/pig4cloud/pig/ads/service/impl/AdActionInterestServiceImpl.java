package com.pig4cloud.pig.ads.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdActionInterestMapper;
import com.pig4cloud.pig.ads.service.AdActionInterestService;
import com.pig4cloud.pig.api.entity.AdActionInterest;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * @广告账户 服务实现类
 * @author john
 *
 */
@Service
@RequiredArgsConstructor
public class AdActionInterestServiceImpl extends ServiceImpl<AdActionInterestMapper, AdActionInterest> implements AdActionInterestService {
	@Resource
	AdActionInterestMapper adActionInterestMapper;
	@Override
	public List<AdActionInterest> getAllThirdIdAndNames() {
		return adActionInterestMapper.getAllThirdIdAndNames();
	}
}
