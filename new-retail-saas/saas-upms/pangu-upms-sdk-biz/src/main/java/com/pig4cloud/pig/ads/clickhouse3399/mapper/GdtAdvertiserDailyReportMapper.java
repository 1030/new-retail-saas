package com.pig4cloud.pig.ads.clickhouse3399.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.gdt.dto.GdtAdvertiserDailyReportReq;
import com.pig4cloud.pig.api.gdt.entity.GdtAdvertiserDailyReport;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author yk
 * * @createTime:2020/12/10
 */
@Mapper
public interface GdtAdvertiserDailyReportMapper extends BaseMapper<GdtAdvertiserDailyReport> {

	/**
	 * 根据广告组ID分析查询统计报表数据
	 *
	 * @return
	 */
	IPage<GdtAdvertiserDailyReport> selectAdvertiserDailyReport(IPage<GdtAdvertiserDailyReport> page,
																@Param("req") GdtAdvertiserDailyReportReq req);
}
