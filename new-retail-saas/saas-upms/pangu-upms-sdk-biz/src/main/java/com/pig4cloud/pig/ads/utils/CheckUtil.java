package com.pig4cloud.pig.ads.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pig4cloud.pig.api.util.StringUtils;

import java.util.HashMap;
import java.util.List;

/**
 * 系统验证
 */
public class CheckUtil {
    /**
     * 验证集合是否为空
     *
     * @param list
     * @return
     */
    public static boolean checkListNotNull(List list) {
        if (list != null && list.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * 验证Map集合是否为空
     *
     * @param map
     * @return
     */
    public static boolean checkMapNotNull(HashMap map) {
        if (map != null && map.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * 整数非空验证
     *
     * @param obj
     * @return
     */
    public static Integer getInt(Integer obj) {
        if (obj == null) {
            return 0;
        }
        return obj;
    }

    /**
     * 获取正整数
     *
     * @param obj
     * @return
     */
    public static Integer getAddInt(Integer obj) {
        if (obj == null || obj < 0) {
            return 0;
        }
        return obj;
    }

    /**
     * 非空验证
     *
     * @param obj
     * @return
     */
    public static String getString(String obj) {
        if (obj == null) {
            return "";
        }
        return obj;
    }

    /**
     * 字符串转Integer
     *
     * @param obj
     * @return
     */
    public static Integer stringToInteger(String obj) {
        if (obj == null) {
            return 0;
        }
        Integer intObj = Integer.parseInt(obj);
        if (intObj != null) {
            return intObj;
        }
        return 0;
    }

	/**
	 * 验证正整数（包括0）
	 * @param str
	 * @return
	 */
    public static boolean checkInteger(String str){
    	if (StringUtils.isBlank(str)){
			return false;
		}
    	return str.matches("^[+]{0,1}\\d{0,2}(\\.\\d{0,2})?|100$");
	}

	/**
	 * @Description: 处理struct结构体，{}的去掉。或去掉 []
	 * type 1.处理 {} 2处理[]
	 * @Author: zjz
	 * @Date: 2022/7/21
	 */
	public static String deal(String str, int type){
		if (org.apache.commons.lang3.StringUtils.isBlank(str)) {
			return null;
		}
		switch (type){
			case 1:
				JSONObject jsonObject = JSONObject.parseObject(str);
				if (jsonObject.isEmpty()) {
					str = null;
				}
				break;
			case 2:
				JSONArray siteSetArr = JSONArray.parseArray(str);
				if (siteSetArr.isEmpty()) {
					str = null;
				}
				break;
			default:
				str = null;
				break;
		}
		return str;
	}

	/**
	 * 去掉后面无用的零
	 * @param str
	 * @return
	 */
	public static String delZero(String str){
		if(str.indexOf(".") > 0){
			str = str.replaceAll("0+?$", "");
			str = str.replaceAll("[.]$", "");
		}
		return str;
	}
}
