package com.pig4cloud.pig.ads.task;

import com.pig4cloud.pig.ads.service.AdCommentService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.ads.task
 * @Author 马嘉祺
 * @Date 2021/7/15 16:58
 * @Description
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class AdCommentTask {

	private final AdCommentService adCommentService;

	/**
	 * 定时更新评论屏蔽词
	 *
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@XxlJob("updateCommentTermsBanned")
	public ReturnT<String> updateCommentTermsBanned(String param) throws Exception {
		try {
			adCommentService.updateCommentTermsBanned(param);
			return ReturnT.SUCCESS;
		} catch (Throwable e) {
			log.error("获取屏蔽词列表出错", e);
			return ReturnT.FAIL;
		}
	}

}
