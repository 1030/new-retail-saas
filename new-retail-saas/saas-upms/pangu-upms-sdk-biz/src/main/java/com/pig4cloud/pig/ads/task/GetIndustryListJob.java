package com.pig4cloud.pig.ads.task;

import com.pig4cloud.pig.ads.service.AdIndustryService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description: 巨量-获取行业列表
 * @author yuwenfeng
 * @date 2022/2/8 17:04
 */
@Component
public class GetIndustryListJob {

	@Autowired
	private AdIndustryService adIndustryService;

	/**
	 * String param 必须要加，不加会出现数据源加载失败的问题
	 * @param param
	 * @return
	 */
	@XxlJob("getIndustryListJob")
	public ReturnT<String> getIndustryListJob(String param){
		XxlJobLogger.log("-----------------巨量-获取行业列表定时任务开始--------------------------");
		adIndustryService.getIndustryList(param);
		XxlJobLogger.log("-----------------巨量-获取行业列表定时任务结束--------------------------");
		return ReturnT.SUCCESS;
	}

}
