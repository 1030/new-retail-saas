package com.pig4cloud.pig.ads.service;

import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdMaterialPushBoxDTO;
import com.pig4cloud.pig.api.entity.AdMaterialPushBox;
import com.pig4cloud.pig.common.core.util.R;

public interface AdMaterialPushBoxService extends IService<AdMaterialPushBox>{

	/**
	 * 素材推送箱详情
	 * @return
	 */
	R getMaterialPushBoxInfo();

	/**
	 * 删除待推送素材
	 * @param id
	 */
	void deleteAdMaterialPushBox(Integer id);

	/**
	 * 清空推送箱
	 */
	void clearAdMaterialPushBox();

	/**
	 * 添加到素材推送-待推送
	 * @param materialId
	 */
	R saveAdMaterialPushBox(Long materialId);

	/**
	 * 单条推送
	 * @param req
	 * @return
	 */
	R pushAdMaterialPushBox(AdMaterialPushBoxDTO req);

	/**
	 * 推送所有
	 * @param req
	 * @return
	 */
	R pushAllAdMaterialPushBox(AdMaterialPushBoxDTO req);

	/**
	 * 获取待推送素材
	 * @param loginUserId
	 * @return
	 */
	Set<Long> getPreparePushMaterial(String loginUserId);

	/**
	 * 批量推送
	 * @param req
	 * @return
	 */
	R batchPushMaterial(AdMaterialPushBoxDTO req);
}
