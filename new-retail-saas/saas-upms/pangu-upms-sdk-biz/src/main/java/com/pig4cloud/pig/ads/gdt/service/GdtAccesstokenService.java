/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.gdt.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.entity.GdtAccesstoken;


/**
 * @用户账号申请授权表
 * @author john
 *
 */
public interface GdtAccesstokenService extends IService<GdtAccesstoken> {


	Map<String, String> fetchAccesstoken(String adAccount);


	/**
	 * @根据管家账户获取公共信息
	 */
	Map<String, String> findGdtCommonData(String accountId);



	void saveUpdateList(List<GdtAccesstoken> list);
	
}
