package com.pig4cloud.pig.ads.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AccountToken;
import com.pig4cloud.pig.api.entity.Advertising;

import java.util.List;
import java.util.Map;

/**
  *     头条广告账户
 *  @author zxm
 */
public interface AdvertiserService extends IService<Advertising> {

	AccountToken getAdvertiserToken(String platform,String advertiserId);

	void saveAdvertiser(List<Map> list,String platformId);

}
