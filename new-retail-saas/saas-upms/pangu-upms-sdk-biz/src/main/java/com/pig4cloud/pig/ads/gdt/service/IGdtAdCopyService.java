package com.pig4cloud.pig.ads.gdt.service;

import com.pig4cloud.pig.api.gdt.dto.GdtAdCopyDto;
import com.pig4cloud.pig.common.core.util.R;


/**
 * 广点通-广告组 服务层
 * 
 * @author hma
 * @date 2020-12-05
 */
public interface IGdtAdCopyService {

	/**
	 * 复制广告
	 * @param gdtAdCopyDto
	 * @return
	 */
	R copyAd(GdtAdCopyDto gdtAdCopyDto);


}
