package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.ads.pig.mapper.AdvertiserMapper;
import com.pig4cloud.pig.ads.service.AdvertiserService;
import com.pig4cloud.pig.api.entity.AccountToken;
import com.pig4cloud.pig.api.entity.AdvertiserInfo;
import com.pig4cloud.pig.api.entity.Advertising;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AdvertiserServiceImpl extends ServiceImpl<AdvertiserMapper, Advertising> implements AdvertiserService {

	@Autowired
	private AdvertiserMapper advertiserMapper;

	@Override
	public AccountToken getAdvertiserToken(String platform,String advertiserId) {
		AccountToken accountToken = new AccountToken();
		switch (PlatformTypeEnum.getEnumByKey(platform)){
			case TT:
				accountToken = advertiserMapper.getAdvertiser4tt(advertiserId);
				break;
			case GDT:
				accountToken = advertiserMapper.getAdvertiser4gdt(advertiserId);
				break;
			case KS:
				accountToken = advertiserMapper.getAdvertiser4ks(advertiserId);
				break;
			case BD:
				accountToken = advertiserMapper.getAdvertiser4bd(advertiserId);
				break;
			default:
				break;
		}
		// 测试环境存在3367加挂了3399的账户，token清除了，但是账户还保留的情况。故过滤token为空的数据
		return accountToken;
	}

	@Override
	public void saveAdvertiser(List<Map> list,String platformId){
		List<Advertising> saveAdvertiserList = Lists.newArrayList();
		// 已经加挂的广告账户
		QueryWrapper<Advertising> wrapper = new QueryWrapper<>();
		wrapper.eq("platform_id", platformId);
		wrapper.eq("deleted",0);
		List<Advertising> advertiserList = this.list(wrapper);
		for (Map map : list) {
			String advertiserId = String.valueOf(map.get("advertiser_id"));
			String advertiserName = String.valueOf(map.get("advertiser_name"));
			String housekeeper = String.valueOf(map.get("housekeeper"));
			BigDecimal balance = Objects.nonNull(map.get("balance")) ? new BigDecimal(String.valueOf(map.get("balance"))) : new BigDecimal("0");
			// 验证账户是否已经加挂
			List<Advertising> accounts = advertiserList.stream().filter(v -> v.getAdvertiserId().equals(advertiserId)).collect(Collectors.toList());
			if (CollectionUtils.isEmpty(accounts)) {
				Advertising ttAdvertiser = new Advertising();
				ttAdvertiser.setPlatformId(Integer.valueOf(platformId));
				ttAdvertiser.setAdvertiserId(advertiserId);
				ttAdvertiser.setName(advertiserName);
				ttAdvertiser.setBalance(balance);
				ttAdvertiser.setHousekeeper(housekeeper);
				ttAdvertiser.setCreatetime(new Date());
				ttAdvertiser.setDeleted(0);
				saveAdvertiserList.add(ttAdvertiser);
			} else {
				Advertising ttAdvertiser = accounts.get(0);
				ttAdvertiser.setName(advertiserName);
				ttAdvertiser.setBalance(balance);
				ttAdvertiser.setHousekeeper(housekeeper);
				ttAdvertiser.setDeleted(Integer.parseInt(String.valueOf(map.get("deleted"))));
				ttAdvertiser.setUpdatetime(new Date());
				saveAdvertiserList.add(ttAdvertiser);
			}
		}
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(saveAdvertiserList)){
			this.saveOrUpdateBatch(saveAdvertiserList,1000);
		}
	}


}
