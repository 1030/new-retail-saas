/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.ads.service.AdMaterialService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.api.entity.AdMaterial;
import com.pig4cloud.pig.api.util.Constants;
import com.pig4cloud.pig.api.util.StringUtils;
import com.pig4cloud.pig.api.vo.AdMaterialSearchVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * 素材库
 *
 * @date 2021-06-19 16:12:13
 */
@RestController
@AllArgsConstructor
@RequestMapping("/admaterial" )
@Api(value = "admaterial", tags = "素材库管理")
public class AdDisableMaterialController {

    private final AdMaterialService adMaterialService;
	/**
	 * 获取广告素材禁用列表分页查询
	 * @param searchVo 素材查询条件
	 * @return
	 */
	@SysLog("素材禁用列表" )
	@ApiOperation(value = "获取广告素材禁用列表分页查询", notes = "获取广告素材禁用列表分页查询")
	@GetMapping("/disable/page" )
	public R getDisableAdMaterialPage(AdMaterialSearchVo searchVo) {
		return R.ok(adMaterialService.pageList(new Page<>(searchVo.getCurrent(),searchVo.getSize()),searchVo));
	}
	/**
	 * 禁用素材
	 * @param adMaterialIds 素材Id
	 * @return R
	 */
	@ApiOperation(value = "禁用素材", notes = "禁用素材")
	@SysLog("禁用素材" )
	@PutMapping("/disableById")
	public R disableById(@ApiParam("素材Id") @RequestParam("ids") String adMaterialIds,
						 @ApiParam("素材类型") @RequestParam(value = "type",required = false)  Integer type,
						 @ApiParam("素材禁用原因") @RequestParam(value = "disableReason")  String disableReason) {

		if(StringUtils.isBlank(adMaterialIds)){
			return R.failed("禁用素材唯一标识不能为空");
		}
		List<Long> ids=null;
		try {
			ids=Arrays.stream(adMaterialIds.split(Constants.COMMA)).map(v->Long.parseLong(v)).collect(Collectors.toList());
		}catch (Exception e){
			return R.failed("禁用素材唯一标识必须为数字类型");
		}
		LambdaUpdateWrapper<AdMaterial> wrappe=new LambdaUpdateWrapper<>();
		if(ids.size()==1){
			wrappe.eq(AdMaterial::getId,ids.get(0));
		}else {
			wrappe.in(AdMaterial::getId,ids);
		}
		if(!Objects.isNull(type)){
			wrappe.eq(AdMaterial::getType,type);
		}
		wrappe.eq(AdMaterial::getIsDelete,Constants.ADMATERIAL_DEL.NODEL.V());
		wrappe.set(AdMaterial::getStatus, Constants.ADMATERIAL_STATUS.DISABLE.V());
		wrappe.set(AdMaterial::getDisableReason,disableReason);
		return R.ok(adMaterialService.update(wrappe));
	}
	/**
	 * 启用素材
	 * @param adMaterialIds 素材Id
	 * @return R
	 */
	@ApiOperation(value = "启用素材", notes = "启用素材")
	@SysLog("启用素材" )
	@PutMapping("/enableById")
	public R enableById(@ApiParam("素材Id") @RequestParam("ids") String adMaterialIds,
						@ApiParam("素材类型") @RequestParam(value = "type",required = false)  Integer type) {
		if(StringUtils.isBlank(adMaterialIds)){
			return R.failed("禁用素材唯一标识不能为空");
		}
		List<Long> ids=null;
		try {
			ids=Arrays.stream(adMaterialIds.split(Constants.COMMA)).map(v->Long.parseLong(v)).collect(Collectors.toList());
		}catch (Exception e){
			return R.failed("禁用素材唯一标识必须为数字类型");
		}
		LambdaUpdateWrapper<AdMaterial> wrappe=new LambdaUpdateWrapper<>();
		if(ids.size()==1){
			wrappe.eq(AdMaterial::getId,ids.get(0));
		}else {
			wrappe.in(AdMaterial::getId,ids);
		}
		if(!Objects.isNull(type)){
			wrappe.eq(AdMaterial::getType,type);
		}
		wrappe.eq(AdMaterial::getIsDelete,Constants.ADMATERIAL_DEL.NODEL.V());
		wrappe.set(AdMaterial::getStatus, Constants.ADMATERIAL_STATUS.ENABLE.V());
		return R.ok(adMaterialService.update(wrappe));
	}
}
