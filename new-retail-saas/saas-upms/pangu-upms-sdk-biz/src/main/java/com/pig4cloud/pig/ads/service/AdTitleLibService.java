package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdTitleLib;
import com.pig4cloud.pig.api.entity.ResponseBean;
import com.pig4cloud.pig.api.vo.AdTitleLibVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * @description:
 * @author: nml
 * @time: 2020/11/7 18:17
 **/
public interface AdTitleLibService extends IService<AdTitleLib> {

	/**
	 * 根据条件查询标题列表
	 *
	 * @param atl
	 * @return
	 */
	List<AdTitleLib> titleTitleLibList(AdTitleLibVo atl);

	/**
	 * 根据条件查询标题分页列表
	 *
	 * @param page
	 * @param atl
	 * @return
	 */
	IPage<AdTitleLib> titleTitleLibPage(Page page, AdTitleLibVo atl);

	/**
	 * 查询头条创意词列表
	 *
	 * @return
	 */
	ResponseBean ttCreativeWordList(String advertiserId);

	/**
	 * 保存标题
	 *
	 * @param atl
	 */
	void createTitleLib(AdTitleLibVo atl);

	/**
	 * 修改标题信息
	 *
	 * @param atl
	 */
	void updateTitleLib(AdTitleLibVo atl);


	//分页模糊查询所有
	@Deprecated
	R findPage(AdTitleLibVo req);

	//广告创意分页模糊查询所有标题
	@Deprecated
	R findAdPage(AdTitleLibVo req);

	//广告创意查询所有标题
	@Deprecated
	R findAdList(AdTitleLibVo req);

	//删除，即修改isdelete字段
	@Deprecated
	R del(AdTitleLib req);

	@Deprecated
	R delBatch(AdTitleLibVo req);

	@Deprecated
	R modify(AdTitleLib req);

	@Deprecated
	R add(List<AdTitleLib> req);

	@Deprecated
	ResponseBean findAllDynamicWordPack(AdTitleLibVo req);

}
