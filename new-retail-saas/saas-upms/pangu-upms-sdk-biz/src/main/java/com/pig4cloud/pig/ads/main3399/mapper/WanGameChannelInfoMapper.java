package com.pig4cloud.pig.ads.main3399.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.WanGameChannelInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * WanGameChannelInfo
 * @author  chengang
 * @version  2021-12-15 14:54:38
 * table: wan_game_channel_info
 */
@Mapper
public interface WanGameChannelInfoMapper extends BaseMapper<WanGameChannelInfo> {

}


