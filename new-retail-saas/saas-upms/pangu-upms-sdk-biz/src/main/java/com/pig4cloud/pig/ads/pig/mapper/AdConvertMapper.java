package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdConvert;
import org.apache.ibatis.annotations.Mapper;

/**
 * 广告转化表
 * @author  chenxiang
 * @version  2022-05-23 10:35:26
 * table: ad_convert
 */
@Mapper
public interface AdConvertMapper extends BaseMapper<AdConvert> {
}


