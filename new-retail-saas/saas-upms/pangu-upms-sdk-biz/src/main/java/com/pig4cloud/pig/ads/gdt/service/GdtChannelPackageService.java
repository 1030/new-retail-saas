package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.GdtChannelPackage;
import com.pig4cloud.pig.api.vo.GdtChannelPackageReq;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName GdtChannelPackageService.java
 * @createTime 2021年07月12日 20:34:00
 */
public interface GdtChannelPackageService extends IService<GdtChannelPackage> {
	/**
	 * 上传渠道包
	 * @param record
	 * @return
	 */
	R create(GdtChannelPackage record);
	/**
	 * 获取渠道包列表
	 * @param record
	 * @return
	 */
	R getList(GdtChannelPackage record);

	/**
	 * 根据advertiser_monitor_info主键获取列表
	 * @param record
	 * @return
	 */
	R getPackageList(GdtChannelPackageReq record);

	/**
	 * 创建应用分包
	 * @param record
	 * @return
	 */
	R extendPackage(GdtChannelPackageReq record);
}
