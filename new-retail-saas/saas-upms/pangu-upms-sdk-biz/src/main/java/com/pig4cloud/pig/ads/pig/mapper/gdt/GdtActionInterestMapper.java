package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdAccountWaterDto;
import com.pig4cloud.pig.api.entity.AdActionInterest;
import com.pig4cloud.pig.api.entity.GdtActionInterest;
import com.pig4cloud.pig.api.gdt.entity.GdtAccountWater;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * @description:
 * @author: nml
 * @time: 2021/7/16 10:05
 **/
@Component
public interface GdtActionInterestMapper extends BaseMapper<GdtActionInterest> {
	void save(GdtActionInterest convertGdtActionInterest);
	void update(GdtActionInterest convertGdtActionInterest);
	Integer checkExist(@Param("tdpId") Long tdParentId, @Param("tdId") Long tdId, @Param("type") String type);

	List<GdtActionInterest> getGdtActionInterestAll();
	void saveBatchActionInterest(@Param("gdtActionInterests") Set<GdtActionInterest> gdtActionInterests);
	void updateBatchActionInterest(@Param("gdtActionInterests") Set<GdtActionInterest> gdtActionInterests);

	List<GdtActionInterest> getAllThirdIdAndNames();
}
