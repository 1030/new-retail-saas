/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pig4cloud.pig.ads.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.pig4cloud.pig.api.util.Page;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pig4cloud.pig.api.dto.DailyStatDTO;
import com.pig4cloud.pig.ads.service.AdUserAdverService;
import com.pig4cloud.pig.ads.service.AdvService;
import com.pig4cloud.pig.ads.service.DailyStatService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;


/**
 * @ 广告账户 前端控制器
 * @author john
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/dailystat")
@Api(value = "dailystat", tags = "广告账户管理模块")
public class DailyStatController {

	private final DailyStatService dailyStatService;

	private final AdUserAdverService adUserAdverService;

	private final AdvService advService;

	/**
	 * 分页查询广告账户信息
	 * @param dto 分页对象
	 * @return 分页对象
	 */
	@PostMapping("/page")
	public R getAdverPage(@RequestBody DailyStatDTO dto) {

		Integer id = SecurityUtils.getUser().getId();

		List<String> allList = advService.getOwnerAdv(id, PlatformTypeEnum.TT.getValue());
		if(allList == null || allList.size() == 0) {
			return R.ok(new Page());
		}

		List<Long> advlist = allList.stream().filter(e ->  StringUtils.isBlank(dto.getAdvertiserId()) || e.equals(dto.getAdvertiserId())).map(l -> Long.valueOf(l)).collect(Collectors.toList());
		if(advlist == null || advlist.size() == 0) {
			return R.ok(new Page());
		}


		Map<String, Object> params = new HashMap<>();
		params.put("advlist", advlist);
		params.put("sdate", dto.getSdate());
		params.put("edate", dto.getEdate());
		//todo  日流水报表统计
		return R.ok(dailyStatService.selectByPage(dto.getPage(), params));
	}

}
