package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdAwemeBannedDto;
import com.pig4cloud.pig.api.entity.AdAwemeBannedEntity;
import com.pig4cloud.pig.api.vo.AdAwemeBannedVo;
import com.pig4cloud.pig.common.core.util.R;

public interface AdAwemeBannedService extends IService<AdAwemeBannedEntity> {

	/**
	 * 屏蔽用户列表
	 *
	 * @param req
	 * @return
	 */
	Page<AdAwemeBannedEntity> getAwemePage(AdAwemeBannedVo req);

	/**
	 * 添加屏蔽用户
	 *
	 * @param req
	 */
	void addAweme(AdAwemeBannedVo req);

	/**
	 * 删除屏蔽用户
	 *
	 * @param id
	 */
	void deleteAweme(Long id);


	/**
	 * 分页列表
	 *
	 * @param dto
	 * @return
	 */
	@Deprecated
	IPage<AdAwemeBannedVo> selectPageList(AdAwemeBannedDto dto);

	/**
	 * 添加
	 *
	 * @param dto
	 * @return
	 */
	@Deprecated
	R addAweme(AdAwemeBannedDto dto);

	/**
	 * 删除
	 *
	 * @param
	 * @return
	 */
	@Deprecated
	R deleteAweme(AdAwemeBannedDto dto);

}
