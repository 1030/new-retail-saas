package com.pig4cloud.pig.ads.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.pig4cloud.pig.ads.service.TtAppManagementService;
import com.pig4cloud.pig.ads.utils.OEHttpUtils;
import com.pig4cloud.pig.api.entity.ResponseBean;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName TtAppManagementTask.java
 * @createTime 2021年08月04日 15:07:00
 */
@Component
public class TtAppManagementTask {

	@Autowired
	private TtAppManagementService ttAppManagementService;

	/**
	 * 查询头条应用信息
	 * {"startDate":"2022-08-01","endDate":"2022-08-31","searchType":"CREATE_ONLY"}
	 * {"startDate":"2022-08-01","endDate":"2022-08-31","searchType":"SHARED_ONLY"}
	 * {"searchType":"CREATE_ONLY"}
	 * {"searchType":"SHARED_ONLY"}
	 * @param param
	 * @return
	 */
	@XxlJob("getAppManagement")
	public ReturnT<String> getAppManagement(String param){
		try {
			final JSONObject object = StringUtils.isNotBlank(param) ? JSON.parseObject(param) : new JSONObject();
			ttAppManagementService.getAppManagement(object);
		} catch (Exception e) {
			e.printStackTrace();
			XxlJobLogger.log("-----------------查询头条应用信息异常---------------------");
		}
		return ReturnT.SUCCESS;
	}
}
