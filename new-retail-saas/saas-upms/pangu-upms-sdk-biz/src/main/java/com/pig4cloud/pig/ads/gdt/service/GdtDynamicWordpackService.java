package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.GdtDynamicWordpack;
import com.pig4cloud.pig.api.vo.GdtDynamicWordpackVo;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @description:
 * @author: nml
 * @time: 2020/12/3 15:36
 **/
public interface GdtDynamicWordpackService extends IService<GdtDynamicWordpack> {

	//分页模糊查询所有
	R findPage(GdtDynamicWordpackVo req);

	//模糊查询所有
	R findList(GdtDynamicWordpackVo req);

}
