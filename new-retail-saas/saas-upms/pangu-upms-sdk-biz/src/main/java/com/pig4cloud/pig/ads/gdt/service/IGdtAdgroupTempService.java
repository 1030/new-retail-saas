package com.pig4cloud.pig.ads.gdt.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.pig4cloud.pig.api.gdt.dto.GdtAdCreativeCreateDto;
import com.pig4cloud.pig.api.gdt.dto.GdtAdgroupCreateTempDto;
import com.pig4cloud.pig.api.gdt.dto.GdtAdgroupTempDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAdgroupTemp;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 广点通-广告组临时 服务层
 *
 * @author hma
 * @date 2020-12-05
 */
public interface IGdtAdgroupTempService  extends IService<GdtAdgroupTemp>
{

	/**
	 * 获取广告组广告版位
	 * @param gdtAdgroupTempDto
	 * @return
     */
	R getGroupSiteSet(GdtAdgroupTempDto gdtAdgroupTempDto);

	/**
	 * 创建广告组临时数据
	 * @param gdtAdgroupCreateTempDto
	 * @return
     */

	R createGdtGroupTemp(GdtAdgroupCreateTempDto gdtAdgroupCreateTempDto);

	GdtAdgroupTemp getCreateGdtGroupById(Long adgroupId);

	GdtAdgroupCreateTempDto createGdtGroup(GdtAdCreativeCreateDto gdtAdCreativeCreateDto, List<Long> creativeIdList) throws JsonProcessingException;



	/**
	 * 获取广告组模板
	 * @param gdtAdgroupTempDto
	 * @return
	 */
	 R  getGroupTemp(GdtAdgroupTempDto gdtAdgroupTempDto);


	/**
	 * 目标详情----------查询广告组对应的转化归因（精准匹配）
	 */

	/**
	 * 每一个推广目标只对应一个推广对象id
	 * @return
	 */
	 R getPromoteObjectId(GdtAdgroupTempDto gdtAdgroupTempDto);



	/**
	 * 应用对应的转换归因数据查询
	 * @return
	 */
	// R getUserActionSets(GdtAdgroupTempDto gdtAdgroupTempDto);


	/**
	 *
	 * @return
	 * 广告优化目标权限查询
	 */
	 R getOptimizationPermission(GdtAdgroupTempDto gdtAdgroupTempDto);


	/**
	 * 获取出价方式
	 * @return
	 */
	 R getBidMode(GdtAdgroupTempDto gdtAdgroupTempDto);

	/**
	 * 获取优化目标信息
	 * @param gdtAdgroupTempDto
	 * @return
	 */
	R getOptimizationGoal(GdtAdgroupTempDto gdtAdgroupTempDto);
}
