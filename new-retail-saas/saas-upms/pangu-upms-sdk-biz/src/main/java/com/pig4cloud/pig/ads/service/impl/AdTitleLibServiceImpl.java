package com.pig4cloud.pig.ads.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.feign.RemotePGameService;
import com.dy.yunying.api.feign.RemoteRoleGameService;
import com.dy.yunying.api.req.ParentGameReq;
import com.pig4cloud.pig.ads.pig.mapper.AdAccountMapper;
import com.pig4cloud.pig.ads.pig.mapper.AdTitleLibMapper;
import com.pig4cloud.pig.ads.service.AdTitleLibService;
import com.pig4cloud.pig.ads.service.AdvService;
import com.pig4cloud.pig.ads.service.TtAccesstokenService;
import com.pig4cloud.pig.ads.utils.IsChineseCharacter;
import com.pig4cloud.pig.ads.utils.OEHttpUtils;
import com.pig4cloud.pig.api.entity.AdAccount;
import com.pig4cloud.pig.api.entity.AdTitleLib;
import com.pig4cloud.pig.api.entity.ResponseBean;
import com.pig4cloud.pig.api.vo.AdTitleLibVo;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: nml
 * @time: 2020/11/7 18:19
 **/
@Log4j2
@Service
@RequiredArgsConstructor
public class AdTitleLibServiceImpl extends ServiceImpl<AdTitleLibMapper, AdTitleLib> implements AdTitleLibService {

	private final AdTitleLibMapper adTitleLibMapper;

//	private final AdTitleLibService adTitleLibService;

	private final TtAccesstokenService ttAccesstokenService;

	private final AdvService advService;

	private final AdAccountMapper adAccountMapper;

	private final RemoteRoleGameService remoteRoleGameService;

	private final RemotePGameService remotePGameService;

	/**
	 * 根据条件查询标题列表
	 *
	 * @param atl
	 * @return
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	@Override
	public List<AdTitleLib> titleTitleLibList(AdTitleLibVo atl) {
		AdTitleLib record = new AdTitleLib().setTitleName(atl.getTitleName()).setPlatformId(atl.getPlatformId()).setCreateuser(atl.getCreateuser());
		// 获取当前账户下主游戏信息
		List<Long> pGameIds = Optional.ofNullable(remoteRoleGameService.getOwnerRolePGameIds()).map(R::getData).orElse(Collections.emptyList());
		if (pGameIds.isEmpty()) { // 当前用户所属角色下没有子游戏
			return Collections.emptyList();
		}
		record.setPGameIds(pGameIds);

		List<AdTitleLib> titleList = adTitleLibMapper.selectTitleList(record);
		assemblyPGname(titleList);
		return titleList;
	}

	private void assemblyPGname(List<AdTitleLib> titleList) {
		if (!titleList.isEmpty()) {
			// 远程调用查询父游戏名称
			R<List<ParentGameDO>> pgnameList = remotePGameService.getCacheablePGameList(new ParentGameReq().setIds(titleList.stream().map(AdTitleLib::getProjectId).filter(Objects::nonNull).map(Integer::toUnsignedLong).collect(Collectors.toSet())));
			if (pgnameList != null && CollectionUtils.isNotEmpty(pgnameList.getData())) {
				Map<Long, String> pgnameMap = pgnameList.getData().stream().collect(Collectors.toMap(ParentGameDO::getId, ParentGameDO::getGname, (k1, k2) -> k1));
				// 为标题填充主游戏名称
				titleList.stream().filter(e -> Objects.nonNull(e.getProjectId())).forEach(e -> e.setPgname(pgnameMap.get(Integer.toUnsignedLong(e.getProjectId()))));
			}
		}
	}

	/**
	 * 根据条件查询标题分页列表
	 *
	 * @param page
	 * @param atl
	 * @return
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	@Override
	public IPage<AdTitleLib> titleTitleLibPage(Page page, AdTitleLibVo atl) {
		AdTitleLib record = new AdTitleLib().setTitleName(atl.getTitleName()).setPlatformId(atl.getPlatformId()).setCreateuser(atl.getCreateuser());
		// 获取当前账户下主游戏信息
		List<Long> pGameIds = Optional.ofNullable(remoteRoleGameService.getOwnerRolePGameIds()).map(R::getData).orElse(Collections.emptyList());
		if (pGameIds.isEmpty()) { // 当前用户所属角色下没有子游戏
			return page.setTotal(0).setRecords(Collections.emptyList());
		}
		record.setPGameIds(pGameIds);

		IPage<AdTitleLib> iPage = adTitleLibMapper.selectTitlePage(page, record);
		List<AdTitleLib> records = iPage.getRecords();
		assemblyPGname(records);
		return iPage;
	}

	/**
	 * 查询头条创意词列表
	 *
	 * @return
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	@Override
	public ResponseBean ttCreativeWordList(String accountId) {
		// 头条动态词包查询接口地址
		String ttUrl = "https://ad.oceanengine.com/open_api/2/tools/creative_word/select/";
		// 当前用户主键
		Integer currentUserId = Objects.requireNonNull(SecurityUtils.getUser()).getId();

		// 获取当前账号的广告账户
		List<AdAccount> accountList = adAccountMapper.selectList(Wrappers.<AdAccount>lambdaQuery().
				select(AdAccount::getAdvertiserId, AdAccount::getHousekeeper)
				.eq(AdAccount::getMediaCode, 1)
				.eq(AdAccount::getThrowUser, currentUserId)
				.eq(StringUtils.isNotBlank(accountId),AdAccount::getAdvertiserId,accountId));
		if (accountList.isEmpty()) {
			ResponseBean bean = new ResponseBean();
			bean.setCode("0");
			bean.setMessage("当前账户下没有广告账户");
			bean.setData(new JSONObject());
			return bean;
		}
		// 默认使用第一个广告账号获取词包接口
		String advertiserId = accountList.get(0).getAdvertiserId();
		// 公共方法获取accessToken
		String accessToken = ttAccesstokenService.fetchAccesstoken(advertiserId);
		if (StringUtils.isEmpty(accessToken)) {
			ResponseBean bean = new ResponseBean();
			bean.setCode("1");
			bean.setMessage("未找到有效token");
			return bean;
		}
		String resultStr = OEHttpUtils.doGet(ttUrl, Collections.singletonMap("advertiser_id", Long.valueOf(advertiserId)), accessToken);
		return JSON.parseObject(resultStr, ResponseBean.class);
	}

	/**
	 * 保存标题
	 *
	 * @param atl
	 */
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ, rollbackFor = Exception.class)
	@Override
	public void createTitleLib(AdTitleLibVo atl) {
		Collection<AdTitleLibVo.Title> titles = atl.getTitles();
		if (null == titles || titles.isEmpty()) {
			throw new NullPointerException("标题信息不能为空");
		}
		titles.stream().map(AdTitleLibVo.Title::getTitleName).forEach(AdTitleLibServiceImpl::assertTitleNameContent);
		// 获取当前用户id
		String currentUserId = String.valueOf(Objects.requireNonNull(SecurityUtils.getUser()).getId());
		Date now = new Date();
		for (AdTitleLibVo.Title title : titles) {
			String titleName = assertTitleNameUnique(title.getTitleName(), atl.getPlatformId());
			adTitleLibMapper.insert(new AdTitleLib().setTitleName(titleName).setRemark(title.getRemark()).setPlatformId(atl.getPlatformId()).setProjectId(atl.getProjectId()).setCreateuser(currentUserId).setCreatetime(now).setUpdateuser(currentUserId).setUpdatetime(now));
		}
	}

	/**
	 * 校验标题名称是否合法
	 *
	 * @param titleName
	 * @return
	 */
	private static String assertTitleNameContent(String titleName) {
		if (StringUtils.isEmpty(titleName)) {
			throw new NullPointerException("标题名称不能为空");
		}

		char c1 = '{', c2 = '}';
		boolean placeholder = false;
		boolean existChineseCharacter = false;

		int halfCount = 0;
		for (char c : titleName.toCharArray()) {
			if (c == c2) {
				if (!placeholder) {
					throw new RuntimeException("标题 '" + titleName + "' 中，无效的'}'");
				}
				placeholder = false;
			} else if (!placeholder) {
				if (c == c1) {
					halfCount += 4;
					placeholder = true;
				} else if (isChineseCharacter(c)) {
					halfCount += 2;
					existChineseCharacter = true;
				} else {
					++halfCount;
				}
			}
		}
		if (placeholder) {
			throw new RuntimeException("请补全 '" + titleName + "' 中，缺失的'}'");
		}
		if (!existChineseCharacter || halfCount < 10 || halfCount > 60) {
			throw new RuntimeException("标题名称 '" + titleName + "' 最少要有一个中文字符，并且长度必须为5-30（中文字符占1个，占位符占2个，其他占0.5个）");
		}
		return titleName;
	}

	private static boolean isChineseCharacter(char c) {
		int codeValue = c & 0xffff;
		if (codeValue >= 0x4e00 && codeValue <= 0x9fa5) {
			return true;
		}
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		return ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
				|| ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
				|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
				|| ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS;
	}

	private String assertTitleNameUnique(String titleName, Integer platformId) {
		LambdaQueryWrapper<AdTitleLib> wrapper = Wrappers.<AdTitleLib>lambdaQuery().eq(AdTitleLib::getIsdelete, 0).eq(AdTitleLib::getPlatformId, platformId).eq(AdTitleLib::getTitleName, titleName);
		if (adTitleLibMapper.selectCount(wrapper) > 0) {
			throw new RuntimeException("标题 '" + titleName + "' 已存在");
		}
		return titleName;
	}

	/**
	 * 修改标题信息
	 *
	 * @param atl
	 */
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ, rollbackFor = Exception.class)
	@Override
	public void updateTitleLib(AdTitleLibVo atl) {
		Optional<AdTitleLibVo> atlOps = Optional.of(atl);

		Long titleId = atlOps.map(AdTitleLibVo::getId).orElseThrow(() -> new NullPointerException("主键不能为空"));
		String titleName = atlOps.map(AdTitleLibVo::getTitleName).filter(StringUtils::isNotEmpty).orElseThrow(() -> new NullPointerException("标题不能为空"));

		AdTitleLib res = getById(atl.getId());
		if (titleName.equals(res.getTitleName())) {
			return;
		}

		titleName = assertTitleNameContent(titleName);
		titleName = assertTitleNameUnique(titleName, res.getPlatformId());

		adTitleLibMapper.updateById(new AdTitleLib().setId(titleId).setTitleName(titleName).setRemark(atl.getRemark()).setUpdateuser(String.valueOf(Objects.requireNonNull(SecurityUtils.getUser()).getId())).setUpdatetime(new Date()));
	}


	//调用第三方投放平台接口：头条接口，获取动态词包
	@Override
	@Deprecated
	public ResponseBean findAllDynamicWordPack(AdTitleLibVo req) {
		//定义头条 查询动态创意词包接口url
		String url = "https://ad.oceanengine.com/open_api/2/tools/creative_word/select/";
		//通过creator去查询多个广告主id
		Integer creator = SecurityUtils.getUser().getId();
		//获取当前账号下 广告账户列表
		List<String> allList = advService.getOwnerAdv(creator, null);

		if (allList == null || allList.size() == 0) {
			ResponseBean responseBean = new ResponseBean();
			responseBean.setCode("1");
			responseBean.setMessage("当前账户下无广告账户，无法获取词包");
			return responseBean;
		}

		if (allList.size() > 0) {
			String advertiser_id = allList.get(0);
			//公共方法获取accessToken
			String accessToken = ttAccesstokenService.fetchAccesstoken(advertiser_id);
			//组合请求参数：data
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("advertiser_id", advertiser_id);
			//请求接口带上参数creative_word_ids，返回就为空
		/*final Long advertiser_id = 1673067551680526L;
		final Long[] creative_word_ids = new Long[]{4L,1736L,1737L,1750L};*/
			//data.put("creative_word_ids",creative_word_ids);
			//调第三方平台（头条）接口
			String resultStr = OEHttpUtils.doGet(url, data, accessToken);
			ResponseBean resBean = JSON.parseObject(resultStr, ResponseBean.class);

			//请求结果的json格式返回给前端
			//log.info(">>>调用头条接口结果：{}",resultStr);
			return resBean;
		}
		//当前账户下无广告账户
		ResponseBean responseBean = new ResponseBean();
		responseBean.setCode("1");
		responseBean.setMessage("当前账户下无广告账户，无法获取词包");
		return responseBean;
	}

	//保存当前登录账户下第一个广告账户id下的所有动态词包
	@Deprecated
	public void saveAdDynamicWordpack(ResponseBean resBean) {
		JSONObject data = resBean.getData();
		Object creative_word = data.get("creative_word");
		//List list = (List)creative_word;
	}

	//广告创意分页模糊查询所有标题
	@Override
	@Deprecated
	public R findAdPage(AdTitleLibVo req) {
		//设置分页条件
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());
		//设置where条件
		QueryWrapper<AdTitleLib> queryWrapper = new QueryWrapper<>();
		if (null != req.getTitleName()) {
			queryWrapper.like("title_name", req.getTitleName());
		}
		if (null != req.getPlatformId()) {
			queryWrapper.inSql("platform_id", req.getPlatformId() + "," + 0);
		}
		queryWrapper.eq("isdelete", 0);
		//根据修改时间降序
		queryWrapper.orderByDesc("updatetime");
		IPage<AdTitleLib> data = adTitleLibMapper.selectPage(page, queryWrapper);
		return R.ok(data, "请求成功");
	}

	@Override
	@Deprecated
	public R findAdList(AdTitleLibVo req) {
		//设置where条件
		QueryWrapper<AdTitleLib> queryWrapper = new QueryWrapper<>();
		if (null != req.getTitleName()) {
			queryWrapper.like("title_name", req.getTitleName());
		}
		if (null != req.getPlatformId()) {
			queryWrapper.inSql("platform_id", req.getPlatformId() + "," + 0);
		}
		queryWrapper.eq("isdelete", 0);
		//根据修改时间降序
		queryWrapper.orderByDesc("updatetime");
		List<AdTitleLib> data = adTitleLibMapper.selectList(queryWrapper);
		return R.ok(data, "请求成功");
	}

	//分页模糊查询所有
	@Override
	@Deprecated
	public R findPage(AdTitleLibVo req) {
		//设置分页条件
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());
		//设置where条件
		QueryWrapper<AdTitleLib> queryWrapper = new QueryWrapper<>();
		if (null != req.getTitleName()) {
			queryWrapper.like("title_name", req.getTitleName());
		}
		if (null != req.getPlatformId()) {
			queryWrapper.eq("platform_id", req.getPlatformId());
		}
		if (null != req.getProjectId()) {
			queryWrapper.eq("project_id", req.getProjectId());
		}
		//标题库表加创建人
		if (null != req.getCreateuser()) {
			queryWrapper.eq("createuser", req.getCreateuser());
		}
		queryWrapper.eq("isdelete", 0);
		//根据修改时间降序
		queryWrapper.orderByDesc("updatetime");
		//queryWrapper.orderBy(true,false,"createtime","updatetime");
		IPage<AdTitleLib> data = adTitleLibMapper.selectPage(page, queryWrapper);
		return R.ok(data, "请求成功");
	}

	//删除标题
	@Override
	@Deprecated
	public R del(AdTitleLib req) {
		req.setIsdelete(1);
		int i = adTitleLibMapper.updateById(req);
		if (i == 1) {
			return R.ok(i, "删除成功");
		}
		return R.failed(i, "删除失败");
	}

	//批量删除标题
	@Override
	@Deprecated
	public R delBatch(AdTitleLibVo req) {
		AdTitleLib adTitleLib = new AdTitleLib();
		if (StringUtils.isNotBlank(req.getPids())) {
			String pids = req.getPids();
			//多个id
			String[] pidsArr = pids.split(",");
			for (int i = 0; i < pidsArr.length; i++) {
				Long id = Long.parseLong(pidsArr[i]);
				adTitleLib.setId(id);
				adTitleLib.setIsdelete(1);//将图片状态设置为1，即为删除
				int del = adTitleLibMapper.updateById(adTitleLib);
				if (del == 0) {
					return R.failed(0, "删除失败");
				}
			}
			return R.ok(1, "删除成功");
		}
		return R.failed("pids 参数为空");
	}

	//修改标题（标题只有一个）
	@Override
	@Deprecated
	public R modify(AdTitleLib req) {
		//验证标题名称：标题需要校验一下：至少有一个中文字符   长度 5~30
		if (StringUtils.isBlank(req.getTitleName())) {
			return R.failed("标题名不能为空");
		}
		if (!isContainChinese(req.getTitleName())) {
			return R.failed("标题名至少有一个中文字符");
		}
		if (!isCorrectLength(req.getTitleName())) {
			return R.failed("标题名字符长度5-30个。中文字符:1个；{xxxx}:2个；其他：0.5个");
		}
		//获取到当前登录用户
		Integer updateuser = SecurityUtils.getUser().getId();
		req.setUpdateuser(updateuser + "");
		req.setUpdatetime(new Date());
		Long id = req.getId();
		AdTitleLib adTitleLib = adTitleLibMapper.selectById(id);
		if (StringUtils.isNotBlank(req.getTitleName()) && Objects.nonNull(req.getPlatformId())) {
			//如果标题名称和平台id都和当前记录id的相等，那么直接修改
			if (req.getTitleName().equals(adTitleLib.getTitleName()) && req.getPlatformId() == adTitleLib.getPlatformId()) {
				int i = adTitleLibMapper.updateById(req);
				if (i == 1) {
					return R.ok(i, "修改成功");
				}
				return R.failed(i, "修改失败");
			}
			//标题名and平台id 和当前记录id的 标题名and平台id 都不同，
			// 那么判断 标题名and平台id 是否和其他 标题名and平台id 相同
			boolean exist = isExistTitle(req);
			//如果不存在，执行修改操作
			if (!exist) {
				int i = adTitleLibMapper.updateById(req);
				if (i == 1) {
					return R.ok(i, "修改成功");
				}
				return R.failed(i, "修改失败");
			}
			//如果存在
			return R.failed(req.getTitleName(), "该平台下此名称{" + req.getTitleName() + "}已存在，请重新输入");
		}
		//如果只修改项目id,那么直接修改
		if (StringUtils.isBlank(req.getTitleName()) && Objects.isNull(req.getPlatformId())) {
			int i = adTitleLibMapper.updateById(req);
			if (i == 1) {
				return R.ok(i, "修改成功");
			}
			return R.failed(i, "修改失败");
		}
		return R.failed(0, "修改失败，前端参数必须带上标题名和平台id");
	}

	//添加标题（标题可以有多个，前端实现多个标题不重复）
	@Override
	@Deprecated
	public R add(List<AdTitleLib> req) {
		if (Objects.isNull(req) || req.size() <= 0) {
			return R.failed("标题不能为空");
		}
		//获取到创建人。去掉application.yml中接口配置，不直接访问接口时，才可通过下面公用方法获取到创建人
		String creator = SecurityUtils.getUser().getId() + "";
		//isEmpty和isBlank的区别在于
		//isEmpty仅仅是判断空和长度为0字符串
		//isBlank判断的是空，长度为0，空白字符（包括空格，制表符\t，换行符\n，换页符\f，回车\r）组成的字符串。
		//单个标题名时,或者
		//多个标题名时
		List<AdTitleLib> adTitleLibs = handleTitles(req, creator);
		for (int i = 0; i < adTitleLibs.size(); i++) {
			//验证标题名称：标题需要校验一下：至少有一个中文字符   长度 5~30
			if (!isContainChinese(adTitleLibs.get(i).getTitleName())) {
				return R.failed("标题名至少有一个中文字符");
			}
			if (!isCorrectLength(adTitleLibs.get(i).getTitleName())) {
				return R.failed("标题名字符长度5-30个。中文字符:1个；{xxxx}:2个；其他：0.5个");
			}
			boolean exist = isExistTitle(adTitleLibs.get(i));
			//如果数据库存在当前标题名
			if (exist) {
				return R.failed(adTitleLibs.get(i).getTitleName(), "该平台下，此标题名{" + adTitleLibs.get(i).getTitleName() + "}已存在");
			}
		}
		//如果数据库不存在当前的多个标题名
		boolean b = this.saveBatch(adTitleLibs);
		if (b) {
			return R.ok(1, "添加成功");
		}
		return R.failed(0, "添加失败");
	}

	//处理多个标题名
	@Deprecated
	public List<AdTitleLib> handleTitles(List<AdTitleLib> req, String creator) {
		//List<AdTitleLib> list = Lists.newArrayList();
		for (int i = 0; i < req.size(); i++) {
			req.get(i).setIsdelete(0);
			//获取到创建人
			req.get(i).setCreateuser(creator + "");
			req.get(i).setCreatetime(new Date());
			req.get(i).setUpdatetime(new Date());
		}
		return req;
	}

	//判断标题名是否存在
	@Deprecated
	public boolean isExistTitle(AdTitleLib req) {
		QueryWrapper<AdTitleLib> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("title_name", req.getTitleName());
		queryWrapper.eq("isdelete", 0);
		queryWrapper.eq("platform_id", req.getPlatformId());
		AdTitleLib adTitleLib = adTitleLibMapper.selectOne(queryWrapper);
		if (Objects.nonNull(adTitleLib)) {
			return true;
		}
		return false;
	}

	//判断字符串是否包含汉字
	@Deprecated
	public static boolean isContainChinese(String str) {
		Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
		Matcher m = p.matcher(str);
		if (m.find()) {
			return true;
		}
		return false;
	}

	//判断字符串长度是否符合要求
	@Deprecated
	public static boolean isCorrectLength(String str) {
		// 1 匹配{xxxx}
		Pattern p = Pattern.compile("\\{(.*?)}");
		// 匹配】
		Matcher matcher = p.matcher(str);
		// 处理匹配到的值
		while (matcher.find()) {
			String group = matcher.group();
			str = str.replace(group, "aaaa");
		}

		//2 匹配中文，并将每个中文替换成aa
// 注意[\u4E00-\u9FA5]里面的斜杠字符，千万不可省略，不区分大小写
		int chineseCharNums = IsChineseCharacter.ChineseCharNums(str);
		int strLength = str.length() + chineseCharNums;
/*		Pattern pat = Pattern.compile("[\u4E00-\u9FA5]");
		Matcher mat = pat.matcher(str);
		while(mat.find()) {
			String group = mat.group();
			str = str.replace(group,"aa");
		}*/
		System.out.println("strLength:" + strLength);
		System.out.println(str);

		if (strLength >= 10 && strLength <= 60) {
			return true;
		}
		return false;
	}

/*	public static void main(String[] a) {
		// 待处理字符串
		String wpp = "；{wpp1}aa{wpp2}aa中国人aa";
		//\u0024\u007B\u0028\u002E\u002A\u003F\u0029}
		// 匹配方式
		Pattern p = Pattern.compile("\\{(.*?)}");
		// 匹配】
		Matcher matcher = p.matcher(wpp);
		// 处理匹配到的值
		String result ="";
		while (matcher.find()) {
			String group = matcher.group();
			System.out.println("woo: " + matcher.group());
			wpp = wpp.replace(group,"aaaa");
			System.out.println("result:"+wpp);
		}
		//匹配中文，并将每个中文替换成aa
// 注意[\u4E00-\u9FA5]里面的斜杠字符，千万不可省略，不区分大小写
		Pattern pat = Pattern.compile("[\u4E00-\u9FA5]");
		Matcher mat = pat.matcher(wpp);
		while(mat.find()) {
			String group = mat.group();
			wpp = wpp.replace(group,"aa");
		}
		System.out.println(wpp);
		System.out.println(wpp.length());
	}*/

/*	public static void main(String[] args) {
		String access_token = "1d4291e0b80d7d2dc716c6dfb810ef4da0651457";
		final Long advertiser_id = 1673067551680526L;
		//final Long[] creative_word_ids = new Long[]{1L,2L,3L,4L,1736L,1737L,1750L};
		// 请求地址
		String open_api_url_prefix = "https://ad.oceanengine.com/open_api/2/";
		String uri = "tools/creative_word/select/";
		// 请求参数
		//List<Long> creative_word_ids = Lists.newArrayList();
		//creative_word_ids.add(1736L);
		Map<String,Object> data = new HashMap(){
			{
				put("advertiser_id", advertiser_id);
				//put("creative_word_ids", creative_word_ids);
			}
		};
		// 构造请求
		HttpEntityEnclosingRequestBase httpEntity = new HttpEntityEnclosingRequestBase() {
			@Override
			public String getMethod() {
				return "GET";
			}
		};
		httpEntity.setHeader("Access-Token",access_token);
		CloseableHttpResponse response = null;
		CloseableHttpClient client = null;
		try {
			client = HttpClientBuilder.create().build();
			httpEntity.setURI(URI.create(open_api_url_prefix + uri));
			httpEntity.setEntity(new StringEntity(JSONObject.toJSONString(data), ContentType.APPLICATION_JSON));
			response = client.execute(httpEntity);
			if (response != null && response.getStatusLine().getStatusCode() == 200) {
				BufferedReader bufferedReader  = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String line = "";
				while((line = bufferedReader.readLine()) != null) {
					result.append(line);
				}
				bufferedReader.close();
				System.out.println(JSONObject.parseObject(result.toString()));
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (response != null) {
					response.close();
				}
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}*/

}
