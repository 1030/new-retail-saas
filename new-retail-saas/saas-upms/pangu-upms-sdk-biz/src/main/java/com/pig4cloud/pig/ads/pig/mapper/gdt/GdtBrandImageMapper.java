package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.GdtBrandImage;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nml
 *
 */
@Mapper
@InterceptorIgnore(tenantLine = "1")
public interface GdtBrandImageMapper extends BaseMapper<GdtBrandImage> {

}