package com.pig4cloud.pig.ads.service.kafka;

import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.Map;

public interface CommonKafkaService {

	void send(String topic, Map<String, Object> map);

}
