package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import com.pig4cloud.pig.api.gdt.dto.GdtAudiencePackagePageReq;
import com.pig4cloud.pig.api.gdt.entity.GdtAudiencePackage;
import com.pig4cloud.pig.api.gdt.vo.GdtAudiencePackageVo;
import com.pig4cloud.pig.common.core.util.R;

public interface GdtAudiencePackageService extends IService<GdtAudiencePackage> {
	void test();

	R create(GdtAudiencePackage entity);

	<E extends IPage<GdtAudiencePackage>> IPage<GdtAudiencePackageVo> pagedAudiencePackage(Page page, GdtAudiencePackagePageReq req);

	//<E extends IPage<GdtAudiencePackage>> IPage<GdtAudiencePackageVo> pagedAudiencePackageSycSuc(E page, String advertiserId);

	GdtAudiencePackage detail(Integer id);

	R remove(Integer id);

/*	R bindAdplan(Integer audiencePackageId, List<Long> ads);

	R unbindAdplan(Integer audiencePackageId, List<Long> ads);*/

	R update(GdtAudiencePackage entity);

}
