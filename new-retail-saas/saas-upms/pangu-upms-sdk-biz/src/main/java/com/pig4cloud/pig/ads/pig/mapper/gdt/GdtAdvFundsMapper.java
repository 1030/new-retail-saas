package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtAdvFunds;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GdtAdvFundsMapper extends BaseMapper<GdtAdvFunds> {

}
