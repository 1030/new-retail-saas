/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.api.entity.AdAgent;
import com.pig4cloud.pig.api.vo.AdAgentRebateVo;
import com.pig4cloud.pig.api.vo.AdAgentVo;

/**
 * 代理商表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
public interface AdAgentService extends IService<AdAgent> {
	/**
	 * 分页查询
	 * @param req
	 * @return
	 */
	R getPage(AdAgentVo req);
	/**
	 * 代理商列表
	 * @param req
	 * @return
	 */
	R getList(AdAgentVo req);
	/**
	 * 添加
	 * @param req
	 * @return
	 */
	R add(AdAgentVo req);
	/**
	 * 编辑
	 * @param req
	 * @return
	 */
	R edit(AdAgentVo req);
	/**
	 * 删除
	 * @param req
	 * @return
	 */
	R del(AdAgentVo req);
	/**
	 * 返点列表 - 分页
	 * @param req
	 * @return
	 */
	R getRebatePage(AdAgentRebateVo req);
	/**
	 * 返点列表
	 * @param req
	 * @return
	 */
	R getRebateList(AdAgentRebateVo req);
	/**
	 * 添加返点
	 * @param req
	 * @return
	 */
	R addRebate(AdAgentRebateVo req);
	/**
	 * 编辑返点
	 * @param req
	 * @return
	 */
	R editRebate(AdAgentRebateVo req);
	/**
	 * 删除返点
	 * @param req
	 * @return
	 */
	R delRebate(AdAgentRebateVo req);

}
