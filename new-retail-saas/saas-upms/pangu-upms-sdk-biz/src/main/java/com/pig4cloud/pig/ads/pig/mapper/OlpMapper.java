package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.OrangeLandingPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * @author yk
 */
@Component
@Mapper
public interface OlpMapper extends BaseMapper<OrangeLandingPage> {

	OrangeLandingPage selectByPrimaryKey(@Param("id") Integer id);

	int deleteByPrimaryKey(@Param("id") Integer id);

	int insert(OrangeLandingPage record);

	int insertSelective(OrangeLandingPage record);

	int updateByPrimaryKey(OrangeLandingPage record);

	int updateByPrimaryKeySelective(OrangeLandingPage record);
}
