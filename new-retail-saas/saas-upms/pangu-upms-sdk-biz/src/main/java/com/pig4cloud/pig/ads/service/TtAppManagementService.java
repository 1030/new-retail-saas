package com.pig4cloud.pig.ads.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.TtAppManagement;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName TtAppManagementService.java
 * @createTime 2021年08月04日 16:55:00
 */
public interface TtAppManagementService extends IService<TtAppManagement> {

	void getAppManagement(JSONObject param);

	TtAppManagement getPackageIdByAppId(String appId);
}
