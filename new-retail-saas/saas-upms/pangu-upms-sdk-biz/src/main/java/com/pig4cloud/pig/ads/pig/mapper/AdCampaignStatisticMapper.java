package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdCampaignStatistic;

/**
 * 投放广告组数据报 数据层
 * 
 * @author hma
 * @date 2020-11-07
 */
public interface AdCampaignStatisticMapper  extends BaseMapper<AdCampaignStatistic>
{





}