package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.dto.GdtAdgroupCreateTempDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAdgroup;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;


/**
 * 广点通-广告组 服务层
 * 
 * @author hma
 * @date 2020-12-05
 */
public interface IGdtAdgroupService   extends IService<GdtAdgroup>
{

	/**
	 * 创建广告组
	 * @param gdtAdgroupCreateTempDto
	 * @return
     */
	 GdtAdgroupCreateTempDto createGdtGroup(GdtAdgroupCreateTempDto gdtAdgroupCreateTempDto, List<Long> creativeIdList);


	/**
	 * 拉取推广计划，并存储数据
	 * @param gdtAdgroupCreateTempDto
	 */
	public void getGdtGroup(GdtAdgroupCreateTempDto gdtAdgroupCreateTempDto) throws Exception;


	/**
	 * 更新广告组
	 * @param gdtAdgroupCreateTempDto
	 * @return
	 */
	GdtAdgroupCreateTempDto updateGdtGroup(GdtAdgroupCreateTempDto gdtAdgroupCreateTempDto);




	/*查询广点通详情：参数 广告组id*/
	R findGdtAdgroupDetailById(GdtAdgroup gdtAdgroup);
	


}
