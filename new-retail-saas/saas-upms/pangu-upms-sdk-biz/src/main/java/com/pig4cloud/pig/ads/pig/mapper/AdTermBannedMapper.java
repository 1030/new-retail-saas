package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdTermBannedDto;
import com.pig4cloud.pig.api.entity.AdTermBannedEntity;
import com.pig4cloud.pig.api.vo.AdTermBannedVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdTermBannedMapper extends BaseMapper<AdTermBannedEntity> {
	/**
	 * 批量插入
	 * @param list
	 * @return
	 */
	Integer batchInsert(List<AdTermBannedEntity> list);

	/**
	 * 分页列表
	 */
	IPage<AdTermBannedVo> selectPageList(AdTermBannedDto dto);

}
