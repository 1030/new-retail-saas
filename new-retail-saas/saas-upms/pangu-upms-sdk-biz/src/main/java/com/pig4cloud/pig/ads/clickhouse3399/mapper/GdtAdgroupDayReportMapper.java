package com.pig4cloud.pig.ads.clickhouse3399.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.gdt.dto.GdtAdgroupDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAdgroup;
import com.pig4cloud.pig.api.gdt.entity.GdtAdgroupDayReport;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author yk
 * * @createTime:2020/12/10
 */
@Mapper
public interface GdtAdgroupDayReportMapper extends BaseMapper<GdtAdgroup> {

	/**
	 * 根据广告组ID分析查询统计报表数据
	 *
	 * @return
	 */
	//IPage<GdtAdgroupDayReport> selectGdtAdgroupDailyReport(IPage<GdtAdgroupDayReport> page, @Param("gdtAdgroupDto") GdtAdgroupDto gdtAdgroupDto);
	IPage<GdtAdgroupDayReport> selectGdtAdgroupReport( @Param("gdtAdgroupDto") GdtAdgroupDto gdtAdgroupDto);



}
