/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.AdvertiserDto;
import com.pig4cloud.pig.api.dto.BudgetDto;
import com.pig4cloud.pig.api.dto.CampaignDto;
import com.pig4cloud.pig.api.entity.AdCampaign;
import com.pig4cloud.pig.api.entity.TtStatistic;
import com.pig4cloud.pig.common.core.util.R;



/**
 * 推广组接口类
 * @author hma
 *
 */
public interface AdvCampaignService extends IService<AdCampaign> {



	/**
	 * 获取推广组统计数据
	 * @param campaignDto
	 * @return
     */
	R  getAdvCampaignStatistic(CampaignDto campaignDto);

	/**
	 *分页查询广告（推广组，广告计划，广告创意）报表数据
	 * @param campaignDto
	 * @return
	 */
	public  R getAdvStatistic(CampaignDto campaignDto);

/*******************************************/
//新建广告组功能：
	//调用头条平台接口创建广告组
	R createAdvCampaign(AdCampaign req);

	//分页查询 广告组id或名称模糊查询所有广告组
	R findPage(CampaignDto req);

	//查询广告组最新一条记录
	R findLatestOne(CampaignDto req);
/*******************************************/

	/**
	 * 修改广告推广告组状态
	 * @param advertiserDto
	 * @return
	 */
	public R updateCampaignStatus(AdvertiserDto advertiserDto);

	/**
	 *修改广告(广告组，广告)数据
	 * @param planDto
	 * @return
	 */
	public  R updateThirdData(AdvertiserDto planDto, Integer type);



	/**修改日预算（定时修改日预算【广告组、广告计划】）
	 * @param budgetDto
	 * @return
	 */
	 R budgetUpdate(BudgetDto budgetDto, Integer operate);


	/**
	 * 统计计算对应的点击均价，千次展示进价，点击率
	 * @param ttStatistic
	 */
	 void statisticCalculate(TtStatistic ttStatistic);




}
