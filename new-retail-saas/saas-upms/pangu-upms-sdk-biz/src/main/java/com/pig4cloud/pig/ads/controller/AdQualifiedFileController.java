package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.pig4cloud.pig.ads.service.AdQualifiedFileService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.api.entity.AdQualifiedFileDO;
import com.pig4cloud.pig.api.vo.AdQualifiedFileVO;
import com.pig4cloud.pig.api.vo.AdQualifiedFileVO.Create;
import com.pig4cloud.pig.api.vo.AdQualifiedFileVO.Delete;
import com.pig4cloud.pig.api.vo.AdQualifiedFileVO.Update;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 资质文件
 *
 * @Title null.java
 * @Package com.pig4cloud.pigx.ads.controller
 * @Author 马嘉祺
 * @Date 2021/6/21 20:07
 * @Description
 */
@Slf4j
@RestController
@RequestMapping("/qualified")
@AllArgsConstructor
@Validated
public class AdQualifiedFileController {

	private final AdQualifiedFileService adQualifiedFileService;

	/**
	 * 资质文件分页列表
	 *
	 * @param aqf
	 * @return
	 */
	@SysLog("资质文件分页列表")
	@PostMapping("/page")
	public R<IPage<AdQualifiedFileDO>> listPage(@RequestBody @Validated AdQualifiedFileVO aqf) {
		IPage<AdQualifiedFileDO> fileList = adQualifiedFileService.qualifiedFilePage(aqf, aqf);
		return R.ok(fileList);
	}

	/**
	 * 获取资质文件详细信息
	 *
	 * @param qualifiedId
	 * @return
	 */
	@SysLog("获取资质文件详细信息")
	@GetMapping("/get")
	public R<AdQualifiedFileDO> getQualifiedFile(@RequestParam Long qualifiedId) {
		AdQualifiedFileDO qualifiedFile = adQualifiedFileService.getQualifiedFile(qualifiedId);
		return R.ok(qualifiedFile);
	}

	/**
	 * 创建资质文件信息
	 *
	 * @param aqf
	 * @return
	 */
	@SysLog("创建资质文件信息")
	@PostMapping("/create")
	public R<Void> create(@Validated(Create.class) AdQualifiedFileVO aqf) {
		try {
			adQualifiedFileService.createQualifiedFile(aqf);
			return R.ok();
		} catch (Exception e) {
			log.error("创建资质文件异常", e);
			return R.failed(null, e.getMessage());
		}
	}

	/**
	 * 编辑资质文件
	 *
	 * @param aqf
	 * @return
	 */
	@SysLog("编辑资质文件")
	@PostMapping("/matUpdate")
	public R<Void> matUpdate(@Validated(Update.class) AdQualifiedFileVO aqf) {
		try {
			adQualifiedFileService.updateQualifiedFile(aqf);
			return R.ok();
		} catch (IOException e) {
			log.error("编辑资质文件异常", e);
			return R.failed("编辑资质文件异常");
		}
	}

	/**
	 * 下载资质文件
	 *
	 * @param qualifiedId
	 * @return
	 */
	@SysLog("下载资质文件")
	@GetMapping("/download")
	public ResponseEntity<byte[]> download(@RequestParam Long qualifiedId) throws IOException {
		LambdaQueryWrapper<AdQualifiedFileDO> wrapper = Wrappers.<AdQualifiedFileDO>query().lambda().select(AdQualifiedFileDO::getFileName, AdQualifiedFileDO::getFileType, AdQualifiedFileDO::getFilePath).eq(AdQualifiedFileDO::getQualifiedId, qualifiedId);
		AdQualifiedFileDO qualifiedFile = adQualifiedFileService.getBaseMapper().selectOne(wrapper);
		if (null == qualifiedFile) {
			throw new NullPointerException("资质文件不存在");
		}
		byte[] bytes = Files.readAllBytes(new File(qualifiedFile.getFilePath()).toPath());
		String fileName = qualifiedFile.getFileName() + "." + qualifiedFile.getFileType();
		HttpHeaders httpHeaders = new HttpHeaders();
		ContentDisposition disposition = ContentDisposition.builder("form-data").name("attachment").filename(URLEncoder.encode(fileName, StandardCharsets.UTF_8.displayName())).build();
		httpHeaders.setContentDisposition(disposition);
		httpHeaders.setContentType(MediaType.IMAGE_PNG);
		return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);
	}

	/**
	 * 删除资质文件信息
	 *
	 * @param aqf
	 * @return
	 */
	@SysLog("删除资质文件信息")
	@PostMapping("/delete")
	public R<Void> delete(@RequestBody @Validated(Delete.class) AdQualifiedFileVO aqf) {
		Set<Long> qualifiedIds = Arrays.stream(aqf.getQualifiedIdArr().split(",")).map(Long::valueOf).collect(Collectors.toSet());
		if (qualifiedIds.isEmpty()) {
			return R.failed("请提供要删除的资质文件ID");
		}
		adQualifiedFileService.update(Wrappers.<AdQualifiedFileDO>lambdaUpdate().set(AdQualifiedFileDO::getIsdelete, 1).in(AdQualifiedFileDO::getQualifiedId, qualifiedIds));
		return R.ok();
	}

}
