package com.pig4cloud.pig.ads.pig.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.dto.AdPlanDto;
import com.pig4cloud.pig.api.entity.AdPlan;
import com.pig4cloud.pig.api.vo.MaterialVo;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * 广告计划 数据层
 *
 * @author hma
 * @date 2020-11-10
 */
public interface AdPlanMapper extends BaseMapper<AdPlan>
{
//	int updateByPrimaryKeySelective(AdPlan record);
//
//	AdPlan selectByPrimaryKey(Long adId);
//
//	void unbindAudiencePackage(Long adId);
//
//	Integer countByAudienceId(Long audienceId);
//
//	List<AdPlan> adplansByAudienceId(Long audienceId);
//
//	/**
//	 * 批量添加广告计划
//	 *
//	 * @param adPlanList 需要添加的数据集合
//	 * @return 结果
//	 */
//	int batchInsertAdPlan(List<AdPlan> adPlanList);

	/**
	 * 查询广告计划列表
	 * @param adPlanDto
	 * @return
	 */
	List<AdPlan> selectAdPlanList(AdPlanDto adPlanDto);

	List<AdPlan> selectAdPlanListByadvertiserIds(AdPlanDto adPlanDto);

	/**
	 * 查询广告计划列表
	 * @author hejiale
	 * @param adPlanDto
	 * @return
	 */
	List<AdPlan> getByName(AdPlanDto adPlanDto);


	List<AdPlan> selectAdPlanListByAdids(@Param("adids") String adids);
	List<MaterialVo> selectMaterialVosByMaterialIds(@Param("materialIds") Collection<String> materialIds);
}
