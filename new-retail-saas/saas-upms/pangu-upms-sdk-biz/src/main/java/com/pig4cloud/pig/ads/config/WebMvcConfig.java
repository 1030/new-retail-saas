package com.pig4cloud.pig.ads.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * 转换fastjson返回值为null情况 ，根据不同类型设置默认值
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	//todo  返回long类型超过前端接收数值最大值，Long类型转换为字符串
//
//    @Autowired
//    private HttpMessageConverters httpMessageConverters;
//
//    /**
//     * MappingJackson2HttpMessageConverter 实现了HttpMessageConverter 接口，
//     * httpMessageConverters.getConverters() 返回的对象里包含了MappingJackson2HttpMessageConverter
//     * @return
//     */
//    @Bean
//    public MappingJackson2HttpMessageConverter getMappingJackson2HttpMessageConverter() {
//        return new MappingJackson2HttpMessageConverter(new JacksonMapper());
//    }
//
//
//    @Override
//    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
//        converters.addAll(httpMessageConverters.getConverters());
//    }
}

