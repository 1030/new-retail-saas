package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdAccountWaterDto;
import com.pig4cloud.pig.api.entity.TtAccountWater;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: nml
 * @time: 2021/7/14 17:41
 **/
@Component
public interface TtAccountWaterMapper extends BaseMapper<TtAccountWater> {

	IPage<TtAccountWater> selectPageByParams(AdAccountWaterDto req);

}
