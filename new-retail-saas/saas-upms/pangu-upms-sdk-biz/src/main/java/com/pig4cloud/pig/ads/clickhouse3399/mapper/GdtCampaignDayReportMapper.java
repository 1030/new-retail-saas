package com.pig4cloud.pig.ads.clickhouse3399.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.gdt.entity.GdtCampaign;
import com.pig4cloud.pig.api.gdt.entity.GdtCampaignDayReport;
import com.pig4cloud.pig.api.gdt.vo.GdtCampaignVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @projectName:pig
 * @description:
 * @author:yk
 * @createTime:2020/12/16
 * @version:1.0
 */
@Mapper
public interface GdtCampaignDayReportMapper extends BaseMapper<GdtCampaign> {

	/**
	 * 根据推广计划ID分析查询统计报表数据
	 *
	 * @return
	 */
	IPage<GdtCampaignDayReport> selectGdtCampaignDayReport(GdtCampaignVo gdtCampaignVo);
}
