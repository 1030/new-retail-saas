/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.pig4cloud.pig.ads.service.AdAccountService;
import com.pig4cloud.pig.ads.utils.DateUtils;
import com.pig4cloud.pig.api.dto.AdAccountAgentDto;
import com.pig4cloud.pig.api.dto.AdAccountTreeResp;
import com.pig4cloud.pig.api.entity.AdAccount;
import com.pig4cloud.pig.api.vo.AdAccountAgentVo;
import com.pig4cloud.pig.api.vo.AdAccountAgentVo.BatchAccountAgent;
import com.pig4cloud.pig.api.vo.AdAccountAgentVo.SaveAccountAgent;
import com.pig4cloud.pig.api.vo.AdAccountAgentVo.UpdateAccountAgent;
import com.pig4cloud.pig.api.vo.AdAccountRequest;
import com.pig4cloud.pig.api.vo.AdAccountVo;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.annotation.Inner;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * 广告账户表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/account")
@Api(value = "account", tags = "广告账户管理")
public class AdAccountController {

	private final AdAccountService adAccountService;


	/**
	 * 目标账户下拉框 主要用于转账  此管家账户下的广告账户
	 */
	@GetMapping("/list")
	public R queryAccountByHousekeeper(String housekeeper, String adAccount) {
		return adAccountService.queryAccountByHousekeeper(housekeeper, adAccount);
	}


	/**
	 * 分页列表(2022-01-06，后台一次返回数据，前端进行排序及分页)
	 *
	 * @param req
	 * @return
	 */
	@SysLog("账户管理")
	@ResponseBody
	@RequestMapping("/getPage")
	public R getPage(@RequestBody AdAccountRequest req) {
		return adAccountService.getPage(req);
	}

	/**
	 * 查询列表
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping("/getList")
	public R getList(@RequestBody AdAccountVo req) {
		return adAccountService.getList(req);
	}

	/**
	 * 编辑信息
	 *
	 * @param req
	 * @return
	 */
	@SysLog("广告账户编辑")
	@RequestMapping("/edit")
	public R edit(@RequestBody AdAccountVo req) {
		if (StringUtils.isBlank(req.getId())) {
			return R.failed("ID不能为空");
		}
		// 存在相同管家账户下的母账号 判断
		if (req.getIsPaccount() != 0) {
			AdAccount adAccount = adAccountService.queryAdByHousekeeper(req);
			if (adAccount != null || ObjectUtils.isNotEmpty(adAccount)) {
				return R.failed("存在相同管家账户下的母账号");
			}
		}
		return adAccountService.edit(req);
	}

	/**
	 * 代理列表
	 *
	 * @param req
	 * @return
	 */
	@SysLog("代理列表")
	@RequestMapping("/getAccountAgentList")
	public R getAccountAgentList(@RequestBody AdAccountAgentVo req) {
		if (StringUtils.isBlank(req.getAdvertiserId())) {
			return R.failed("广告账户ID不能为空");
		}
		return adAccountService.getAccountAgentList(req);
	}


	/**
	 * 设置代理商
	 *
	 * @param req
	 * @return
	 */
	@SysLog("设置代理商")
	@RequestMapping("/setupAccountAgent")
	public R setupAccountAgent(@RequestBody AdAccountAgentVo req) {
		if (StringUtils.isBlank(req.getAdvertiserId())) {
			return R.failed("广告账户不能为空");
		}
		if (StringUtils.isBlank(req.getAgentId())) {
			return R.failed("代理商不能为空");
		}
		if (StringUtils.isBlank(req.getEffectiveTime())) {
			return R.failed("生效时间不能为空");
		}
		if (StringUtils.isBlank(req.getInvalidTime())) {
			return R.failed("失效时间不能为空");
		}
		if (DateUtils.stringToDate(req.getEffectiveTime(), DateUtils.YYYY_MM_DD_HH_MM_SS).getTime() > DateUtils.stringToDate(req.getInvalidTime(), DateUtils.YYYY_MM_DD_HH_MM_SS).getTime()) {
			return R.failed("生效时间不能大于失效时间");
		}
		return adAccountService.setupAccountAgent(req);
	}

	/**
	 * 设置代理商
	 *
	 * @param req
	 * @return
	 */
	@SysLog("保存广告账户代理商")
	@PostMapping("/saveAccountAgent")
	public R saveAccountAgent(@RequestBody @Validated(SaveAccountAgent.class) AdAccountAgentVo req) {
		try {
			adAccountService.saveAccountAgent(req);
			return R.ok();
		} catch (Exception e) {
			log.error("保存广告账户代理商信息失败", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 修改代理商
	 *
	 * @param req
	 * @return
	 */
	@SysLog("修改广告账户代理商")
	@PostMapping("/updateAccountAgent")
	public R updateAccountAgent(@RequestBody @Validated(UpdateAccountAgent.class) AdAccountAgentVo req) {
		try {
			adAccountService.updateAccountAgent(req);
			return R.ok();
		} catch (Exception e) {
			log.error("修改广告账户代理商信息失败", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 批量设置代理商   req.getAdvertiserArrr()
	 */
	@SysLog("批量设置代理商")
	@RequestMapping("/batchInsert")
	public R setBatchAccountAgent(@RequestBody AdAccountAgentDto req) {
		if (req.getAdvertiserArr() == null || req.getAdvertiserArr().length == 0) {
			return R.failed("广告账户不能为空");
		}
		if (Objects.isNull(req.getAgentId())) {
			return R.failed("代理商不能为空");
		}
		if (StringUtils.isBlank(req.getEffectiveTime())) {
			return R.failed("生效时间不能为空");
		}
		if (StringUtils.isBlank(req.getInvalidTime())) {
			return R.failed("失效时间不能为空");
		}
		if (DateUtils.stringToDate(req.getEffectiveTime(), DateUtils.YYYY_MM_DD_HH_MM_SS).getTime() > DateUtils.stringToDate(req.getInvalidTime(), DateUtils.YYYY_MM_DD_HH_MM_SS).getTime()) {
			return R.failed("生效时间不能大于失效时间");
		}
		return adAccountService.setBatchAccountAgent(req);
	}

	/**
	 * 批量添加代理商
	 *
	 * @param req
	 * @return
	 */
	@SysLog("批量添加代理商")
	@PostMapping("/batchAddAccountAgent")
	public R batchAddAccountAgent(@RequestBody @Validated(BatchAccountAgent.class) AdAccountAgentVo req) {
		List<String> list = adAccountService.batchAddAccountAgent(req, SecurityUtils.getUser().getId());
		return R.ok(list);
	}

	/**
	 * 删除账户代理商
	 *
	 * @param req
	 * @return
	 */
	@SysLog("删除代理商")
	@RequestMapping("/delAccountAgent")
	public R delAccountAgent(@RequestBody AdAccountAgentVo req) {
		if (StringUtils.isBlank(req.getId())) {
			return R.failed("ID不能为空");
		}
		return adAccountService.delAccountAgent(req);
	}

	/**
	 * 广告账户树形
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping("/getAccountList")
	public R getTree(@RequestBody AdAccountVo req) {
		return adAccountService.getTree(req);
	}


	/**
	 * 根据用户列表查询绑定的的广告账号列表
	 *
	 * @param userIdArr
	 * @return
	 */
	@Inner
	@RequestMapping("/getAccountListByUserList")
	public R<List<AdAccount>> getAccountListByUserList(@RequestParam("userIdArr") List<Integer> userIdArr) {
		return adAccountService.getAccountList(userIdArr);
	}

	@Inner
	@RequestMapping("/getAccountList2")
	public List<String> getAccountList2(@RequestParam("userIdArr") List<Integer> userIdArr) {
		return adAccountService.getAccountList2(userIdArr);
	}

	/**
	 * 根据用户列表查询绑定的的广告账号树形列表
	 *
	 * @param userIdArr
	 * @return
	 */
	@Inner
	@RequestMapping("/getAccountTreeByUserList")
	public R<List<AdAccountTreeResp>> getAccountTreeByUserList(@RequestParam("userIdArr") List<String> userIdArr, @RequestParam("isAdmin") Boolean isAdmin, @RequestParam("mediaCode") String mediaCode) {
		AdAccountVo req = new AdAccountVo();
		req.setUserArr(userIdArr);
		req.setIsAdmin(isAdmin);
		if (StringUtils.isNotBlank(mediaCode)) {
			req.setMediaCode(mediaCode);
		}
		return adAccountService.getTree(req);
	}

	/**
	 * 查询列表-所有广告账户
	 *
	 * @param req
	 * @return
	 */
	@Inner
	@RequestMapping("/getAllList")
	public List<AdAccount> getAllList(@RequestBody AdAccountVo req) {
		return adAccountService.getAllList(req);
	}

	/**
	 * 根据授权查看广告账户列表
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping("/getAccountsByAuthorize")
	public R<List<AdAccount>> getAccountsByAuthorize(@RequestBody AdAccountVo req) {
		return R.ok(adAccountService.getAccountsByAuthorize(req));
	}

}
