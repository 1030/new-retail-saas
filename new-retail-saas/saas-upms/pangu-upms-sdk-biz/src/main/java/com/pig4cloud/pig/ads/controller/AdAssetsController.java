package com.pig4cloud.pig.ads.controller;

import com.pig4cloud.pig.ads.service.AdAssetsService;
import com.pig4cloud.pig.api.vo.AdAssetsReq;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 广告资产表
 * @author  chenxiang
 * @version  2022-04-01 15:02:28
 * table: ad_assets
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/adAssets")
public class AdAssetsController {
	
    private final AdAssetsService adAssetsService;

	/**
	 * 分页列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/getPage")
	public R getPage(@RequestBody AdAssetsReq req){
		return adAssetsService.getPage(req);
	}
	/**
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping("/addAssets")
	public R addAssets(@RequestBody AdAssetsReq req){
		if (StringUtils.isBlank(req.getAdvertiserId())){
			return R.failed("请选择广告账户");
		}
		if (StringUtils.isBlank(req.getAppCloudId())){
			return R.failed("请选择应用");
		}
		return adAssetsService.addAssets(req);
	}
}


