/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.api.client.util.Lists;
import com.pig4cloud.pig.ads.pig.mapper.AdLabelMapper;
import com.pig4cloud.pig.ads.service.AdLabelRelateService;
import com.pig4cloud.pig.ads.service.AdLabelService;
import com.pig4cloud.pig.api.entity.AdLabel;
import com.pig4cloud.pig.api.entity.AdLabelRelate;
import com.pig4cloud.pig.api.vo.AdLabelVo;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 素材标签
 *
 * @author cx
 * @date 2021-06-29 14:27:26
 */
@Service
@RequiredArgsConstructor
public class AdLabelServiceImpl extends ServiceImpl<AdLabelMapper, AdLabel> implements AdLabelService {

	private final AdLabelRelateService adLabelRelateService;

	/**
	 * 标签列表
	 * @param req
	 * @return
	 */
	@Override
	public R getList(AdLabelVo req){
		QueryWrapper<AdLabel> wrapper = new QueryWrapper<>();
		wrapper.eq("is_delete",0);
		if (StringUtils.isNotBlank(req.getType())){
			wrapper.eq("type",req.getType());
		}
		List<AdLabel> list = this.list(wrapper);
		return R.ok(list);
	}
	/**
	 * 标签列表
	 * @param req
	 * @return
	 */
	@Override
	public R addLabelRelate(AdLabelVo req){
		// 删除关系标签
		QueryWrapper<AdLabelRelate> wrapper = new QueryWrapper<>();
		wrapper.eq("relate_id",req.getRelateId());
		wrapper.eq("type",req.getType());
		adLabelRelateService.remove(wrapper);
		// 增加标签关系
		List<AdLabelRelate> paramList = Lists.newArrayList();
		String[] labelArr = req.getLabelIds().split(",");
		for (String label : labelArr){
			AdLabelRelate adLabelRelate = new AdLabelRelate();
			adLabelRelate.setLabelId(Integer.valueOf(label));
			adLabelRelate.setRelateId(Integer.valueOf(req.getRelateId()));
			adLabelRelate.setType(Integer.valueOf(req.getType()));
			adLabelRelate.setCreateTime(new Date());
			adLabelRelate.setUpdateTime(new Date());
			paramList.add(adLabelRelate);
		}
		boolean flag = adLabelRelateService.saveBatch(paramList);
		if (flag){
			return R.ok();
		}
		return R.failed();
	}

}
