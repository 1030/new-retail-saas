/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.*;
import com.pig4cloud.pig.api.entity.AdPlan;
import com.pig4cloud.pig.api.vo.PlanGdtAttrAnalyseReportVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;
import java.util.Map;


/**
 * 推广组接口类
 *
 * @author hma
 */
public interface AdvPlanService extends IService<AdPlan> {


	/**
	 * 获取广告计划统计数据
	 *
	 * @param campaignDto
	 * @return
	 */
	R getAdvPlanStatistic(CampaignDto campaignDto);

	/**
	 * 修改广告计划状态
	 *
	 * @param planDto
	 * @return
	 */
	R updatePlanStatus(AdvertiserDto planDto);


	/**
	 * 创建广告计划
	 *
	 * @param adPlanCreateDto
	 * @return
	 */
	AdPlanCreateDto createAdvPlan(AdPlanCreateDto adPlanCreateDto);


	/**
	 * 查询对应广告账号对应的计划
	 *
	 * @param advertiserId
	 * @param adIds        （多个广告计划用逗号分隔）
	 * @return
	 */
	void getAdPlan(String advertiserId, String adIds);


	/**
	 * 调用头条创建广告计划(广告创意)
	 *
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	String createThirdPlanOrCreative(Map<String, Object> paramMap, String methodName, String url, String advertiserId, Integer type);

	/**
	 * 按广告组、广告计划名称查询
	 * @author hejiale
	 * @param adPlanDto
	 * @return
	 */
	List<AdPlan> getByName(AdPlanDto adPlanDto);

    List<AdPlan> getByAdvertiserIds(AdPlanTempDto adPlanTempDto);

    /**
	 * 修改广告计划预算
	 * @param budgetDto
	 * @return
	 */
	 R updatePlanBudget(BudgetDto budgetDto);


	/**
	 * 修改广告计划出价
	 * @param budgetDto
	 * @return
	 */
	 R updatePlanCpaBid(BudgetDto budgetDto);
	/**
	 * 根据广告ID获取广告计划 ,头条
	 * @param adids
	 * @return
	 */
	List<AdPlan> getByAdids(String adids);
	/**
	 * 根据广告ID获取广告计划，广点通
	 * @param adids
	 * @return
	 */
	List<PlanGdtAttrAnalyseReportVo> getGdtByAdids(String adids);
}
