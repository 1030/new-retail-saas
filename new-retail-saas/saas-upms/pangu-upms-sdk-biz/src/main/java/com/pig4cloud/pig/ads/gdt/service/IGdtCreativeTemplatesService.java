package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.entity.GdtCreativeTemplates;

import java.util.List;


/**
 * 创意形式 服务层
 * 
 * @author hma
 * @date 2020-12-16
 */
public interface IGdtCreativeTemplatesService extends IService<GdtCreativeTemplates>
{
	/**
	 * 查询创意形式（内容）列表
	 * @param gdtCreativeTemplates
	 * @return
	 */
	public List<GdtCreativeTemplates> selectGdtCreativeTemplates(GdtCreativeTemplates gdtCreativeTemplates);

	
}
