package com.pig4cloud.pig.ads.pig.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdVideoPlatform;
import com.pig4cloud.pig.api.vo.AdVideoAdVo;
import com.pig4cloud.pig.api.vo.AdVideoPlatformVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ClassName AdVideoMapper.java
 * @author cx
 * @version 1.0.0
 * @Desc TODO
 * @Time 2020/11/5 13:46
 */
@Mapper
public interface AdVideoPlatformMapper extends BaseMapper<AdVideoPlatform> {

	List<AdVideoPlatformVo> selectAdVideoPlatformByList(AdVideoPlatformVo req);
	AdVideoAdVo selectByKey(@Param("platformVid") String platformVid);
}