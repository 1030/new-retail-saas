package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.ads.service.AdTermBannedService;
import com.pig4cloud.pig.api.dto.AdTermBannedDto;
import com.pig4cloud.pig.api.entity.AdTermBannedEntity;
import com.pig4cloud.pig.api.vo.AdTermBannedVo;
import com.pig4cloud.pig.api.vo.AdTermBannedVo.Add;
import com.pig4cloud.pig.api.vo.AdTermBannedVo.Delete;
import com.pig4cloud.pig.api.vo.AdTermBannedVo.Edit;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/terms")
@RequiredArgsConstructor
@Validated
public class AdTermBannerController {

	private final AdTermBannedService adTermBannedService;

	/**
	 * 获取屏蔽词分页列表
	 *
	 * @param req
	 * @return
	 */
	@SysLog("屏蔽词管理")
	@PostMapping("/getTermsPage")
	public R<Page<AdTermBannedEntity>> getTermsPage(@RequestBody AdTermBannedVo req) {
		Page<AdTermBannedEntity> page = adTermBannedService.getTermsPage(req);
		return R.ok(page);
	}

	/**
	 * 获取屏蔽词信息
	 *
	 * @param id
	 * @return
	 */
	@GetMapping("/getTerms")
	public R<AdTermBannedEntity> getTerms(@RequestParam Long id) {
		AdTermBannedEntity terms = adTermBannedService.getById(id);
		return R.ok(terms);
	}

	/**
	 * 添加屏蔽词
	 *
	 * @param req
	 * @return
	 */
	@SysLog("添加屏蔽词")
	@PostMapping("/addTerms")
	public R addTerms(@RequestBody @Validated(Add.class) AdTermBannedVo req) {
		try {
			adTermBannedService.addTerms(req);
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 编辑屏蔽词
	 *
	 * @param req
	 * @return
	 */
	@SysLog("编辑屏蔽词")
	@PostMapping(value = "/editTerms")
	public R editTerms(@RequestBody @Validated(Edit.class) AdTermBannedVo req) {
		try {
			adTermBannedService.editTerms(req);
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 删除屏蔽词
	 *
	 * @param req
	 * @return
	 */
	@SysLog("删除屏蔽词")
	@PostMapping(value = "/deleteTerms")
	public R deleteTerms(@RequestBody @Validated(Delete.class) AdTermBannedVo req) {
		try {
			adTermBannedService.deleteTerms(req.getId());
			return R.ok();
		} catch (Exception e) {
			log.error("", e);
			return R.failed(e.getMessage());
		}
	}


	/**
	 * 分页列表
	 *
	 * @param req
	 * @return
	 */
	@Deprecated
	@RequestMapping("/getPage")
	public R getPage(AdTermBannedDto req) {
		//设置分页条件
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());
		IPage<AdTermBannedVo> data = adTermBannedService.selectPageList(req);
		return R.ok(data, "请求成功");
	}

	/**
	 * 添加屏蔽词
	 *
	 * @param dto
	 * @param bindingResult
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/add")
	public R termsAdd(@RequestBody @Valid AdTermBannedDto dto, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return R.failed(bindingResult.getFieldError().getDefaultMessage());
		}
		if (StringUtils.isEmpty(dto.getAdvertiser_name())) {
			return R.failed("advertiser_name为空");
		}
		List<String> termsList = dto.getTerms_list();
		if (!(null != termsList && termsList.size() > 0)) {
			return R.failed("terms_list为空");
		}
		return adTermBannedService.addTerms(dto);
	}

	/**
	 * 删除屏蔽词
	 *
	 * @param dto
	 * @param bindingResult
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/delete")
	public R termsDelete(@RequestBody AdTermBannedDto dto, BindingResult bindingResult) {
		if (null == dto.getId()) {
			return R.failed("id为空");
		}
		return adTermBannedService.deleteTerms(dto);
	}

	/**
	 * 修改屏蔽词
	 *
	 * @param dto
	 * @param bindingResult
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/update")
	public R termsUpdate(@RequestBody AdTermBannedDto dto, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return R.failed(bindingResult.getFieldError().getDefaultMessage());
		}
		if (null == dto.getId()) {
			return R.failed("id为空");
		}
		if (StringUtils.isEmpty(dto.getTerms())) {
			return R.failed("terms为空");
		}
		return adTermBannedService.updateTerms(dto);
	}
}
