package com.pig4cloud.pig.ads.gdt.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Sets;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtActionInterestMapper;
import com.pig4cloud.pig.ads.service.GdtActionInterestService;
import com.pig4cloud.pig.api.entity.AdActionInterest;
import com.pig4cloud.pig.api.entity.GdtActionInterest;
import com.pig4cloud.pig.api.gdt.dto.GdtLabelDto;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GdtActionInterestServiceImpl extends ServiceImpl<GdtActionInterestMapper, GdtActionInterest> implements GdtActionInterestService {
    @Autowired
    private GdtActionInterestMapper gdtActionInterestMapper;

    @Override
    public void batchSaveGdtLabel(List<GdtLabelDto> gdtLabelDtos) {
        if (CollectionUtils.isEmpty(gdtLabelDtos)) {
            return;
        }
        List<GdtActionInterest> gdtActionInterests = getGdtLabelDtoAll();
        Set<Integer> uniqFlags = null;
        if (CollectionUtils.isNotEmpty(gdtActionInterests)) {
            uniqFlags = gdtActionInterests.stream().map(v -> v.hashCode()).collect(Collectors.toSet());
        }
        if (CollectionUtils.isEmpty(uniqFlags)) {
			gdtActionInterestMapper.saveBatchActionInterest(gdtLabelDtos.stream().map(v -> v.convertGdtActionInterest()).collect(Collectors.toSet()));
            return;
        }
        Set<GdtActionInterest> saveGdtAI = Sets.newHashSet();
        Set<GdtActionInterest> updateGdtAI= Sets.newHashSet();
        Set<Integer> finalUniqFlags = uniqFlags;
        gdtLabelDtos.stream().forEach(v -> {
            if (finalUniqFlags.contains(v.hashCode())) {
                updateGdtAI.add(v.convertGdtActionInterest());
            } else {
                saveGdtAI.add(v.convertGdtActionInterest());
            }
        });
        if (CollectionUtils.isNotEmpty(updateGdtAI)) {
			gdtActionInterestMapper.updateBatchActionInterest(updateGdtAI);
        }
        if (CollectionUtils.isNotEmpty(saveGdtAI)) {
			gdtActionInterestMapper.saveBatchActionInterest(saveGdtAI);
        }
    }

    private List<GdtActionInterest> getGdtLabelDtoAll() {
        return gdtActionInterestMapper.getGdtActionInterestAll();
    }

    @Override
    public void saveGdtLabelDto(GdtLabelDto gdtLabelDto) {
		gdtActionInterestMapper.save(gdtLabelDto.convertGdtActionInterest());
    }

    @Override
    public void updateGdtLabelDto(GdtLabelDto gdtLabelDto) {
		gdtActionInterestMapper.update(gdtLabelDto.convertGdtActionInterest());
    }

    @Override
    public boolean checkExist(Long tdParentId, Long tdId, String type) {
        Integer count = gdtActionInterestMapper.checkExist(tdParentId, tdId, type);
        return !Objects.isNull(count) && count > 0 ? true : false;
    }

	@Override
	public List<GdtActionInterest> getAllThirdIdAndNames() {
		return gdtActionInterestMapper.getAllThirdIdAndNames();
	}
}
