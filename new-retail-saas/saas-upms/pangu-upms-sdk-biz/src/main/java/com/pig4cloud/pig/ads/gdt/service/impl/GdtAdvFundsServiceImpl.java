package com.pig4cloud.pig.ads.gdt.service.impl;

import com.pig4cloud.pig.ads.gdt.service.GdtAdvFundsService;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtAdvFundsMapper;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.api.gdt.entity.GdtAdvFunds;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@RequiredArgsConstructor
public class GdtAdvFundsServiceImpl  extends ServiceImpl<GdtAdvFundsMapper, GdtAdvFunds> implements GdtAdvFundsService {

}
