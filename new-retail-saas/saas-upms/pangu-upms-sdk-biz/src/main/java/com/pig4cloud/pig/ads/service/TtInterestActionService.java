package com.pig4cloud.pig.ads.service;

import com.pig4cloud.pig.common.core.util.R;

public interface TtInterestActionService {
	R getActionCategory(Integer actionDays, String actionScene);

	R getActionKeyword(Integer actionDays, String actionScene);

	R getInterestCategory();

	R getInterestKeyword();
}
