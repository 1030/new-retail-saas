/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AudiencePackage;
import com.pig4cloud.pig.api.vo.AudiencePackageVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;


/**
 * @
 * @author john
 *
 */
public interface AudiencePackageService extends IService<AudiencePackage> {

	R create(AudiencePackage entity);

	<E extends IPage<AudiencePackage>> IPage<AudiencePackageVo> pagedAudiencePackage(E page, String[] advertiserIds);

	IPage<AudiencePackageVo> pagedAudiencePackageSycSuc(Page page, QueryWrapper<AudiencePackage> wrapper);

	AudiencePackage detail(Integer id);

	//int updateSelective(AudiencePackage entity);

	R remove(Integer id);

	R bindAdplan(Integer audiencePackageId, List<Long> ads);

	R unbindAdplan(Integer audiencePackageId, List<Long> ads);

	R update(AudiencePackage entity);
}
