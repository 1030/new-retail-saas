package com.pig4cloud.pig.ads.clickhouse3399.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.gdt.dto.GdtAdReport;
import com.pig4cloud.pig.api.gdt.vo.GdtAdVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nml
 * @version 1.0.0
 * @Description
 * @createTime 2020年12月15日 17:42:00
 */
@Mapper
public interface GdtAdReportMapper {

	//广点通广告创意列表+报表数据
	IPage<GdtAdReport> selectGdtAdReport(GdtAdVo req);

	/*ch 不建议update 且 该方法有问题*/
	/*int updateByAdIdSelective(GdtAd req);*/

}
