package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.SelfCustomAudienceDTO;
import com.pig4cloud.pig.api.entity.SelfCustomAudience;
import com.pig4cloud.pig.api.vo.FreeCrowdPackVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SelfCustomAudienceMapper extends BaseMapper<SelfCustomAudience> {

}
