package com.pig4cloud.pig.ads.controller;

import com.alibaba.fastjson.JSON;
import com.pig4cloud.pig.ads.service.AdConvertService;
import com.pig4cloud.pig.api.entity.AdConvert;
import com.pig4cloud.pig.api.vo.AdConvertReq;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * 广告转化表
 * @author  chenxiang
 * @version  2022-05-23 10:35:26
 * table: ad_convert
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/adConvert")
public class AdConvertController {
	
    private final AdConvertService adConvertService;
	
	@RequestMapping("/list")
	public R list(@RequestBody AdConvert record){
		if (StringUtils.isBlank(record.getAdvertiserId())) {
			return R.failed("广告账户ID不能为空");
		}
		return adConvertService.getList(record);
	}
	/**
	 * 新增转化归因
	 * @param record
	 * @return
	 */
	@Inner
	@RequestMapping("/createGdtConversions")
	public R createGdtConversions(@RequestBody AdConvert record){
		log.info(">>>createGdtConversions param:{}", JSON.toJSONString(record));
		if (Objects.isNull(record.getPgameId())
				|| Objects.isNull(record.getAdId())
				|| Objects.isNull(record.getGameId())
				|| StringUtils.isBlank(record.getChl())
				|| StringUtils.isBlank(record.getMediaCode())
				|| StringUtils.isBlank(record.getAdvertiserId())
				|| StringUtils.isBlank(record.getConvertName())
				|| StringUtils.isBlank(record.getPackageName())
				|| StringUtils.isBlank(record.getActionTrackUrl())
				|| StringUtils.isBlank(record.getConversionScene())
				|| StringUtils.isBlank(record.getClaimType())
				|| StringUtils.isBlank(record.getConvertTarget())){
			return R.failed("缺少必要参数");
		}
		return adConvertService.createGdtConversions(record);
	}

	/**
	 * 根据advertiser_monitor_info主键获取列表
	 * @param record
	 * @return
	 */
	@Inner
	@RequestMapping("/getAdConvertListInner")
	public R getAdConvertListInner(@RequestBody AdConvertReq record){
		return adConvertService.getAdConvertListInner(record);
	}

	/**
	 * 百度-新增转化
	 * @return
	 */
	@Inner
	@RequestMapping("/addOcpcTransFeed")
	public R addOcpcTransFeed(@RequestBody AdConvertReq record){
		log.info(">>>addOcpcTransFeed()param:{}", JSON.toJSONString(record));
		if (StringUtils.isBlank(record.getPgameId())
				|| Objects.isNull(record.getAdId())
				|| StringUtils.isBlank(record.getGameId())
				|| StringUtils.isBlank(record.getChl())
				|| StringUtils.isBlank(record.getMediaCode())
				|| StringUtils.isBlank(record.getAdvertiserId())
				|| StringUtils.isBlank(record.getConvertName())
				|| StringUtils.isBlank(record.getDownloadUrl())
				|| StringUtils.isBlank(record.getPackageName())
				|| StringUtils.isBlank(record.getActionTrackUrl())
				|| StringUtils.isBlank(record.getAppId())
				|| StringUtils.isBlank(record.getAppName())
				|| StringUtils.isBlank(record.getConvertTarget())){
			return R.failed("缺少必要参数");
		}
		return adConvertService.addOcpcTransFeed(record);
	}
}


