package com.pig4cloud.pig.ads.controller.gdt;

import com.pig4cloud.pig.ads.gdt.service.GdtCampaignDayReportService;
import com.pig4cloud.pig.ads.gdt.service.IGdtCampaignService;
import com.pig4cloud.pig.api.dto.BudgetDto;
import com.pig4cloud.pig.api.enums.GdtSpeedModelEnum;
import com.pig4cloud.pig.api.enums.StatusEnum;
import com.pig4cloud.pig.api.gdt.dto.GdtCampaignDto;
import com.pig4cloud.pig.api.gdt.dto.GdtCreateCampaignDto;
import com.pig4cloud.pig.api.gdt.dto.GdtUpdateCampaignDto;
import com.pig4cloud.pig.api.gdt.vo.GdtCampaignVo;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author hma
 * @广点通推广计划管理模板
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/gdt/campaign")
@Api(value = "/gdt/campaign", tags = "广点通推广计划管理模块")
public class GdtAdvCampaignController {

	@Resource
	private IGdtCampaignService gdtCampaignService;

	@Autowired
	private GdtCampaignDayReportService campaignDayReportService;
	/*
	 * 获取广点通推广列表
	 *
	 * */
	@RequestMapping("/getCampaignDayReport")
	public R getCampaignDayReport(GdtCampaignVo req) {
		return campaignDayReportService.getCampaignDayReport(req) ;
	}

	@RequestMapping("/updateOnOff")
	public R updateOnOff(GdtCampaignVo req) {
		return  campaignDayReportService.updateOnOff(req);
	}

	@RequestMapping("/updateDailyBudget")
	public R updateDailyBudget(GdtCampaignVo req) {
		return  campaignDayReportService.updateDailyBudget(req);
	}

	@RequestMapping("/orderDailyBudget")
	public R orderDailyBudget(BudgetDto budgetDto) {
		return  campaignDayReportService.orderDailyBudget(budgetDto, StatusEnum.OPERATE_TYPE_FIVE.getStatus());
	}


	@RequestMapping("/getDailyBudget")
	public R getCampaignBudget(BudgetDto budgetDto) {
		return campaignDayReportService.getAdvertiserJobBudget(budgetDto, StatusEnum.OPERATE_TYPE_FIVE.getStatus());
	}
	/**
	 * 获取推广目标
	 *
	 * @param gtdCampaignDto
	 * @return
	 */
	@SysLog("获取推广目标")
	@RequestMapping("/getGtdCampaignPromote")
	public R getGtdCampaignPromote(GdtCampaignDto gtdCampaignDto) {
		//todo 获取推广目标类型
		//todo 处理对应的推广目标存储  ，先换成，后面换成数据库存储，
		if (null == gtdCampaignDto) {
			return R.failed("参数不能为空");
		}
		if (StringUtils.isBlank(gtdCampaignDto.getAccountId())) {
			return R.failed("广告投放账号不允许为空");
		}
		return gdtCampaignService.getGtdCampaignPromote(gtdCampaignDto);
	}


	/**
	 * 创建推广计划  定义post结构 @json格式
	 *
	 * @return
	 */
	@SysLog("创建推广计划")
	@RequestMapping("/createGdtCampaign")
	public R createGdtCampaign(@RequestBody GdtCreateCampaignDto createCampaignDto) {
		// 推广计划 默认名称为 ：推广目标_日期_时分
		// 参数校验  【计划类型，推广目标，投放形式，计划日预算，推广计划名称】
		// 日元算传值为0 代表不限
		if (null == createCampaignDto) {
			return R.failed("参数不能为空");
		}
		if (null == createCampaignDto.getAccountId()) {
			return R.failed("广告投放账号不允许为空");
		}
		if (StringUtils.isBlank(createCampaignDto.getCampaignType())) {
			return R.failed("计划类型不允许空");
		}
		if (StringUtils.isBlank(createCampaignDto.getPromotedObjectType())) {
			return R.failed("推广目标不允许空");
		}
		if (StringUtils.isBlank(createCampaignDto.getSpeedMode())) {
			return R.failed("投放形式不允许空");
		}
		if(!GdtSpeedModelEnum.containsType(createCampaignDto.getSpeedMode())){
			return R.failed(String.format("不支持[%s]投放方式",createCampaignDto.getSpeedMode()));
		}

		if (StringUtils.isBlank(createCampaignDto.getCampaignName())) {
			return R.failed("推广计划名称不允许空");
		}

		if(null != createCampaignDto.getDailyBudget() && createCampaignDto.getDailyBudget().compareTo(new BigDecimal(0)) > 0){
			createCampaignDto.setDailyBudget(createCampaignDto.getDailyBudget().multiply(new BigDecimal(100)));
			if(createCampaignDto.getDailyBudget().compareTo(new BigDecimal("5000")) < 0 || createCampaignDto.getDailyBudget().compareTo(new BigDecimal("4000000000")) > 0){
				return R.failed("日预算要求介于 50 元-40,000,000 元");
			}
		}
		return gdtCampaignService.createGdtCampaign(createCampaignDto);
	}

	/**
	 * 获取已存在的推广计划
	 *
	 * @return
	 */
	@SysLog("获取已存在的推广计划")
	@RequestMapping("/getGtdCampaign")
	public R getGtdCampaign(GdtCampaignDto gtdCampaignDto) {
		//  去掉删除的推广计划
		//  返回对应的推广目标三方id，推广计划名称，推广计划目标类型
		if (null == gtdCampaignDto) {
			return R.failed("参数不能为空");
		}
		if (StringUtils.isBlank(gtdCampaignDto.getAccountId())) {
			return R.failed("广告投放账号不允许为空");
		}
		if (0 == gtdCampaignDto.getSize()) {
			return R.failed("size  is  null ");
		}
		if (0 == gtdCampaignDto.getCurrent()) {
			return R.failed("current  is  null ");
		}
		return gdtCampaignService.getGtdCampaign(gtdCampaignDto);

	}


	/**
	 * 更新推广计划客户状态，是否开启预警
	 *
	 * @return
	 */
	@SysLog("获取推广计划模板")
	@RequestMapping("/getGtdCampaignTemplate")
	public R getGtdCampaignTemplate(GdtUpdateCampaignDto gdtCampaignDto) {
		if(null == gdtCampaignDto){
			return R.failed( "参数不能为空");
		}
		if(StringUtils.isBlank(gdtCampaignDto.getAccountId())){
			return R.failed( "广告账号不允许为空");
		}
		return gdtCampaignService.getGtdCampaignTemplate(gdtCampaignDto);
	}

}
