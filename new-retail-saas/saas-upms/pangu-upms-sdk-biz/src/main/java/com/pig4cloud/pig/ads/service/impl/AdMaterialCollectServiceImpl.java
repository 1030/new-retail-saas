/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.AdMaterialCollectMapper;
import com.pig4cloud.pig.ads.service.AdMaterialCollectService;
import com.pig4cloud.pig.api.entity.AdMaterialCollect;
import org.springframework.stereotype.Service;

/**
 * 用户收藏素材表
 *
 * @author cx
 * @date 2021-06-24 19:31:09
 */
@Service
public class AdMaterialCollectServiceImpl extends ServiceImpl<AdMaterialCollectMapper, AdMaterialCollect> implements AdMaterialCollectService {

}
