package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdQualifiedFileDO;
import com.pig4cloud.pig.api.vo.AdQualifiedFileVO;

import java.io.IOException;

/**
 * 资质文件
 */
public interface AdQualifiedFileService extends IService<AdQualifiedFileDO> {

	/**
	 * 资质文件分页列表
	 *
	 * @param page
	 * @param aqf
	 * @return
	 */
	IPage<AdQualifiedFileDO> qualifiedFilePage(Page page, AdQualifiedFileVO aqf);

	/**
	 * 获取资质文件详细信息
	 */
	AdQualifiedFileDO getQualifiedFile(Long qualifiedId);

	/**
	 * 创建资质文件信息
	 *
	 * @param aqf
	 */
	void createQualifiedFile(AdQualifiedFileVO aqf) throws IOException;

	/**
	 * 编辑资质文件
	 *
	 * @param aqf
	 * @throws IOException
	 */
	void updateQualifiedFile(AdQualifiedFileVO aqf) throws IOException;

}
