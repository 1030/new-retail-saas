package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtCustomAudiencesFile;
import org.apache.ibatis.annotations.Mapper;

/**
 * @projectName:pig
 * @description:广点通人群文件信息
 * @author:Zhihao
 * @createTime:2020/12/5 21:00
 * @version:1.0
 */
@Mapper
public interface GdtCustomAudiencesFileMapper extends BaseMapper<GdtCustomAudiencesFile> {

}