package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;

public interface GdtSelectListService {
	R czSite(String advertiserId, Integer page, Integer pageSize);

	R industryList(String advertiserId);

	R maritalStatus() ;
	R workingStatus() ;
	R customAudience(Page page, String accountId ,String searchName) ;
	R userOs() ;

	public R behaviorInterest(String accountId, String tag);

	public R getParentLevels(String accountId, String Ids, String tag);
}
