/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdCreativeMaterialEntity;
import com.pig4cloud.pig.api.entity.TtCreativeDetail;
import com.pig4cloud.pig.api.gdt.entity.GdtAd;

import java.util.List;

/**
 * 素材同步平台信息表
 *
 * @author pigx code generator
 * @date 2021-06-23 14:32:25
 */
public interface AdCreativeMaterialService extends IService<AdCreativeMaterialEntity> {

	/**
	 * 批量保存或更新巨量引擎创意与素材关系
	 *
	 * @param creativeDetailList
	 */
	void batchUpsertOeCreativeMaterial(List<TtCreativeDetail> creativeDetailList);

	/**
	 * 批量保存或更新腾讯广告创意与素材关系
	 *
	 * @param gdtAdList
	 */
	void batchUpsertTxCreativeMaterial(List<GdtAd> gdtAdList);


	/**
	 * 批量保存
	 *
	 * @param entityList
	 * @return
	 */
//	boolean batchSave(List<AdCreativeMaterialEntity> entityList);

	/**
	 * 保存
	 *
	 * @param entity
	 * @return
	 */
//	boolean singleSave(AdCreativeMaterialEntity entity);

	/**
	 * 批量保存
	 *
	 * @param adCreative
	 * @return
	 */
//	boolean batchSaveAdCreativeMaterial(AdCreative adCreative);

	/**
	 * 批量保存
	 *
	 * @param gdtAdCreative
	 * @return
	 */
//	boolean batchSaveAdCreativeMaterial(GdtAdCreative gdtAdCreative);

}
