package com.pig4cloud.pig.ads.runner;

import com.pig4cloud.pig.ads.gdt.service.IGdtAdCreativeService;
import com.pig4cloud.pig.api.gdt.dto.GdtAdDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
public class SynGdtAdvCreativeWorker implements Runnable {

	private IGdtAdCreativeService iGdtAdCreativeService;

	private String accountId;

	private List<Long> adcreativeIds;

	//最大重试次数
	private static final Integer tryTimes = 6;
	//重试间隔时间单位秒
	private static final Integer[] intervalTime = {15, 60, 120, 150, 300, 600};

	public SynGdtAdvCreativeWorker(IGdtAdCreativeService iGdtAdCreativeService, String accountId, List<Long> adcreativeIds) {
		this.iGdtAdCreativeService = iGdtAdCreativeService;
		this.accountId = accountId;
		this.adcreativeIds = adcreativeIds;
	}


	@Override
	public void run() {
		try {
			if(CollectionUtils.isNotEmpty(adcreativeIds)){
				Integer retryNum = 1;
				while (retryNum <= tryTimes) {
					try {
						String ids = StringUtils.join(adcreativeIds.toArray(), ",");
						GdtAdDto gdtAdDto = new GdtAdDto();
						gdtAdDto.setAdcreativeIds(ids);
						gdtAdDto.setAccountId(accountId);
						iGdtAdCreativeService.getAdCreativeData(gdtAdDto);

						TimeUnit.SECONDS.sleep(intervalTime[retryNum-1]);
						retryNum++;
					} catch (Throwable e) {
						retryNum++;
						TimeUnit.SECONDS.sleep(intervalTime[retryNum - 1]);
						continue;
					}
				}
			}
		}catch(Throwable e){
			log.error("拉取第三方广告创意失败", e);
		}
	}
}
