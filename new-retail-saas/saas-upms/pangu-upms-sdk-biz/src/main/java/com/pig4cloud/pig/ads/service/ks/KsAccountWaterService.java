package com.pig4cloud.pig.ads.service.ks;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.KsAccountWater;

/**
 * 快手账号流水明细
 * @author  chenxiang
 * @version  2022-03-24 13:56:21
 * table: ks_account_water
 */
public interface KsAccountWaterService extends IService<KsAccountWater> {
}


