package com.pig4cloud.pig.ads.service;

import com.pig4cloud.pig.common.core.util.R;

public interface ToutiaoAwemeService {
	R getAwemeCategory(String advertiserId);

	R getAwemeAuthor(String advertiserId, String categoryId,String behaviors);
}
