package com.pig4cloud.pig.ads.controller.gdt;


import com.pig4cloud.pig.ads.gdt.service.GdtAudiencePackageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.api.gdt.dto.GdtAudiencePackagePageReq;
import com.pig4cloud.pig.api.gdt.entity.GdtAudiencePackage;
import com.pig4cloud.pig.common.core.util.R;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * @广点通广告账户管理模块
 * @author zhuxm
 * 
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/gdt/audience_pack")
@Api(value = "/gdt/audience_pack", tags = "广点通-定向包")
public class GdtAudiencePackageController {
	
	private final GdtAudiencePackageService baseService;
	
	
	
	//@Value(value = "${gdt_client_id}") private String gdtClientId;


	@RequestMapping("/test")
	public R test() {
		baseService.test();
		return R.ok();
	}

	//新建
	@RequestMapping("/create")
	public R create(@RequestBody GdtAudiencePackage record) {
		return baseService.create(record);
	}

	//详情
	@RequestMapping("/detail")
	public R detail(Integer id) {
		GdtAudiencePackage ap = baseService.detail(id);
		if (ap != null) {
            return R.ok(ap);
        } else {
            return R.failed("未找到记录");
        }
	}

	//更新
	@RequestMapping("/update")
	public R update(@RequestBody GdtAudiencePackage record) {
		return baseService.update(record);
	}

	//删除
	@RequestMapping("/delete")
	public R delete(Integer id) {
		return baseService.remove(id);
	}


	//分页查询
	@GetMapping("/page")
	public R getApPage(Page page,GdtAudiencePackagePageReq req) {
		return R.ok(baseService.pagedAudiencePackage(page, req));
	}

/*	//绑定 广告计划
	@RequestMapping("/ad/bind")
	public R bindAdplan(Integer audiencePackageId, String ads) {
		String[] adsStr = ads.split(",");
		List<Long> adids = new ArrayList<>();
		for (String str : adsStr){
			if (!com.alibaba.druid.util.StringUtils.isNumber(str)){
				R.failed("广告id不合法");
			}
			adids.add(Long.valueOf(str));
		}
		return baseService.bindAdplan(audiencePackageId, adids);
	}

	//解绑 广告计划
	@RequestMapping("/ad/unbind")
	public R unbindAdplan(Integer audiencePackageId, String ads) {
		String[] adsStr = ads.split(",");
		List<Long> adids = new ArrayList<>();
		for (String str : adsStr){
			if (!StringUtils.isNumber(str)){
				R.failed("广告id不合法");
			}
			adids.add(Long.valueOf(str));
		}
		return baseService.unbindAdplan(audiencePackageId, adids);
	}*/



/*	//分页查询
	@GetMapping("/page_syc_suc")
	public R getApPageSycSuc(Page page, String advertiserId) {
		return R.ok(baseService.pagedAudiencePackageSycSuc(page,advertiserId));
	}*/


}
