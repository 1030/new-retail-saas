package com.pig4cloud.pig.ads.pig.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AccountToken;
import com.pig4cloud.pig.api.entity.AdvertiserInfo;
import com.pig4cloud.pig.api.entity.Advertising;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AdvertiserMapper extends BaseMapper<Advertising> {

	List<AdvertiserInfo> selectAdvertiserInfoList(Map<String, Object> map);

	void insert(AdvertiserInfo advertiserInfo);

	void update(AdvertiserInfo advertiserInfo);

	AdvertiserInfo selectAdvertiserByAdAccount(@Param("advertiser_id") String advertiser_id);

	AccountToken getAdvertiser4tt(@Param(value = "advertiserId") String advertiserId);

	AccountToken getAdvertiser4gdt(@Param(value = "advertiserId") String advertiserId);

	AccountToken getAdvertiser4ks(@Param(value = "advertiserId") String advertiserId);

	AccountToken getAdvertiser4bd(@Param(value = "advertiserId") String advertiserId);

}
