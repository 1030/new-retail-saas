package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdAssetsEvent;
import com.pig4cloud.pig.api.vo.AdAssetsEventReq;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 广告资产事件表
 * @author  chenxiang
 * @version  2022-04-01 16:25:19
 * table: ad_assets_event
 */
public interface AdAssetsEventService extends IService<AdAssetsEvent> {
	/**
	 * 获取可创建事件列表
	 * @param req
	 * @return
	 */
	R getList(AdAssetsEventReq req);
	/**
	 * 获取资产下已经创建的事件
	 * @param req
	 * @return
	 */
	R getEventList(AdAssetsEventReq req);
	/**
	 * 创建事件
	 * @param req
	 * @return
	 */
	R addEvent(AdAssetsEventReq req);
}


