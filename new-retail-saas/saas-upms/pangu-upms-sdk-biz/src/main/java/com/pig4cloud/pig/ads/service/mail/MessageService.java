package com.pig4cloud.pig.ads.service.mail;

import java.util.List;

public interface MessageService {

	void sendMail(String mailSubject, String emailContent, List<String> sendToList);

}
