package com.pig4cloud.pig.ads.dao;

import com.pig4cloud.pig.api.vo.AdAccountVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author ：lile
 * @date ：2021/7/12 14:14
 * @description：
 * @modified By：
 */
@Component
@Slf4j
public class AdAccountDao {
	@Resource(name = "clickhouseTemplate")
	private JdbcTemplate clickhouseTemplate;


	public List<Map<String, Object>> selectCostAdAccount(AdAccountVo req) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT \n ");
		sql.append(" ad_account adAccount, \n ");
		sql.append(" SUM(cost) cost \n ");
		sql.append(" from  v_adid_rebate_day \n ");
		sql.append(" where 1=1 \n ");
		if (Objects.nonNull(req.getStartTime()) && Objects.nonNull(req.getEndTime())) {
			sql.append(" and  date >=").append(req.getStartTime());
			sql.append(" and  date <=").append(req.getEndTime());
		}
		sql.append(" group by ad_account \n ");
		List<Map<String, Object>> list = clickhouseTemplate.queryForList(sql.toString());
		return list;
	}
}
