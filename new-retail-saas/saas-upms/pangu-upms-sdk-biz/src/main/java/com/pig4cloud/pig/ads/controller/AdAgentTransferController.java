package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.pig4cloud.pig.ads.service.AdAgentTransferService;
import com.pig4cloud.pig.api.dto.AdAgentTransferDto;
import com.pig4cloud.pig.api.entity.AdAgentTransfer;
import com.pig4cloud.pig.api.vo.AdAgentTransferVo;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Objects;

/**
 * @author ：lile
 * @date ：2021/7/13 16:18
 * @description：
 * @modified By：
 */
@RestController
@AllArgsConstructor
@RequestMapping("/agenttransfer")
@Api(value = "agenttransfer", tags = "代理商设置ID")
public class AdAgentTransferController {

	@Autowired
	private AdAgentTransferService adAgentTransferService;


	/**
	 * 查询
	 *
	 * @param req
	 * @return
	 */
	@PostMapping("/list")
	public R list(@RequestBody AdAgentTransferDto req) {
		if (Objects.isNull(req.getAgentId())) {
			return R.failed("代理商id不能为空");
		}
		IPage<AdAgentTransferVo> data = adAgentTransferService.queryList(req);
		return R.ok(data);
	}

	/**
	 * 添加
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping("/add")
	public R add(@RequestBody AdAgentTransferDto req) {

		if (Objects.isNull(req.getAgentId())) {
			return R.failed("代理商id不能为空");
		}
		if (Objects.isNull(req.getPlatform())) {
			return R.failed("渠道类型不能为空");
		}
		if (StringUtils.isBlank(req.getChncode())) {
			return R.failed("主渠道不为空");
		}
		if (Objects.isNull(req.getAdagentId())) {
			return R.failed("代理商ID设置不能为空");
		}

		// 同一个渠道类型下不能再设置代理商Id
		AdAgentTransferVo adAgentTransferVo = adAgentTransferService.queryPtypeByid(req);

		if (adAgentTransferVo != null || ObjectUtils.isNotEmpty(adAgentTransferVo)) {
			return R.failed("存在相同渠道类型下的代理商ID");
		}

		AdAgentTransfer adAgentTransfer = new AdAgentTransfer();
		BeanUtils.copyProperties(req, adAgentTransfer);
		adAgentTransfer.setCreateTime(new Date());
		adAgentTransfer.setCreateUser(SecurityUtils.getUser().getId().toString());
		boolean result = adAgentTransferService.save(adAgentTransfer);
		if (result) {
			return R.ok("添加成功");
		} else {
			return R.failed("添加失败");
		}
	}

	/**
	 * 编辑
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping("/edit")
	public R edit(@RequestBody AdAgentTransferDto req) {
		if (Objects.isNull(req.getId())) {
			return R.failed("编辑ID不能为空");
		}
		if (Objects.isNull(req.getAgentId())) {
			return R.failed("代理商id不能为空");
		}
		if (Objects.isNull(req.getPlatform())) {
			return R.failed("渠道类型不能为空");
		}
		if (StringUtils.isBlank(req.getChncode())) {
			return R.failed("主渠道不为空");
		}
		if (Objects.isNull(req.getAdagentId())) {
			return R.failed("代理商ID设置不能为空");
		}
		// 同一个渠道类型下不能再设置代理商Id
		AdAgentTransferVo adAgentTransferVo = adAgentTransferService.queryPtypeByid(req);

		if (adAgentTransferVo != null || ObjectUtils.isNotEmpty(adAgentTransferVo)) {
			return R.failed("存在相同渠道类型下的代理商ID");
		}

		AdAgentTransfer adAgentTransfer = new AdAgentTransfer();
		BeanUtils.copyProperties(req, adAgentTransfer);
		adAgentTransfer.setUpdateTime(new Date());
		adAgentTransfer.setUpdateUser(SecurityUtils.getUser().getId().toString());
		boolean result = adAgentTransferService.updateById(adAgentTransfer);
		if (result) {
			return R.ok("编辑成功");
		} else {
			return R.failed("编辑失败");
		}
	}

	/**
	 * 删除
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping("/del")
	public R del(@RequestBody AdAgentTransferDto req) {
		if (Objects.isNull(req.getId())) {
			return R.failed("删除ID不能为空");
		}
		AdAgentTransfer adAgentTransfer = new AdAgentTransfer();
		adAgentTransfer.setIsDelete(1);
		adAgentTransfer.setId(Integer.valueOf(req.getId()));
		boolean result = adAgentTransferService.updateById(adAgentTransfer);
		if (result) {
			return R.ok("删除成功");
		} else {
			return R.failed("删除失败");
		}
	}
}
