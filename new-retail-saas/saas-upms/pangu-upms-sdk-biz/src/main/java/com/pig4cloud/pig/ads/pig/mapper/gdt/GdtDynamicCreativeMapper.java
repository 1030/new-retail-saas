package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.GdtDynamicCreative;
import org.apache.ibatis.annotations.Mapper;

/**
 * 广点通动态创意表(gdt_dynamic_creative)数据Mapper
 *
 * @author zjz
 * @since 2022-07-11 22:26:02
 * @description 由 zjz 创建
*/
@Mapper
public interface GdtDynamicCreativeMapper extends BaseMapper<GdtDynamicCreative> {

}
