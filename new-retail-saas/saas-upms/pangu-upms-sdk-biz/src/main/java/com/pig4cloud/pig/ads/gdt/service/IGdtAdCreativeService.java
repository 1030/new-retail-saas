package com.pig4cloud.pig.ads.gdt.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.gdt.dto.CreativetoolsDto;
import com.pig4cloud.pig.api.gdt.dto.GdtAdCreativeCreateDto;
import com.pig4cloud.pig.api.gdt.dto.GdtAdCreativeDto;
import com.pig4cloud.pig.api.gdt.dto.GdtAdDto;
import com.pig4cloud.pig.api.gdt.dto.GdtAdgroupTempDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAdCreative;
import com.pig4cloud.pig.api.gdt.vo.GdtAdCreativeVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 广点通广告创意 服务层
 * 
 * @author hma
 * @date 2020-12-10
 */
public interface IGdtAdCreativeService  extends IService<GdtAdCreative>
{

	/**
	 * 创建创意
	 * @param creativeCreateDtoList
	 * @return
	 */
	 R createGdtCreativeTemp(List<GdtAdCreativeCreateDto> creativeCreateDtoList);

	/**
	 * 获取广告创意形式
	 * @param gdtAdCreativeDto
	 * @return
     */
	 R getCreativeTemplates(GdtAdCreativeDto gdtAdCreativeDto,String fields,Integer  operateType);


	/**
	 * 创意预览
	 * @param GdtAdCreativeDto
	 * @return
     */
	R  creativePreviews(GdtAdCreativeDto GdtAdCreativeDto);


	/**
	 * 查询按钮文案下拉框内容
	 * @return
	 */
	 R getButtonText();

	 /*
	 *  查询广告创意列表数据
	 * */
	R getGdtAdCreativeReport(GdtAdCreativeVo gdtAdCreativeVo);

	/*
	 *  调广点通接口设置广告创意开关：广告开关configured_status
	 * */
	R updateGdtAdOnOff(GdtAdCreativeVo gdtAdCreativeVo);

	/**
	 * 拉取广告创意并存储
	 * @param gdtAdDto
	 */
	 void getAdCreativeData(GdtAdDto gdtAdDto);

	void getAdCreativeData(GdtAdDto gdtAdDto,boolean isDyCreative);


	/**
	 * 获取落地页
	 * @param gdtAdgroupTempDto
	 */
	R getGdtPage(GdtAdgroupTempDto gdtAdgroupTempDto);


	GdtAdCreativeCreateDto createGdtCreative(GdtAdCreativeCreateDto creativeCreateDto);

	/**
	 * 创意形式，创意内容【拆开，合并】
	 * @param gdtAdCreativeDto
	 * @param fields
	 * @param operateType
     * @return
     */
	 R getCreativeTemplatesUpdate(GdtAdCreativeDto gdtAdCreativeDto,String fields,Integer operateType);

	/**
	 * 查询广告文案
	 * @param creativetoolsDto
	 * @return
	 */

	  R  getCreativetoolsText(CreativetoolsDto creativetoolsDto);


}
