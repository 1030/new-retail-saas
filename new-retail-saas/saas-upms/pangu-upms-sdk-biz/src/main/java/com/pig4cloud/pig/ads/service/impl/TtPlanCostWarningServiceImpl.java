package com.pig4cloud.pig.ads.service.impl;

import com.pig4cloud.pig.ads.service.TtPlanCostWarningService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.api.entity.TtPlanCostWarning;
import com.pig4cloud.pig.ads.pig.mapper.TtPlanCostWarningMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@RequiredArgsConstructor
public class TtPlanCostWarningServiceImpl extends ServiceImpl<TtPlanCostWarningMapper, TtPlanCostWarning> implements TtPlanCostWarningService {

}
