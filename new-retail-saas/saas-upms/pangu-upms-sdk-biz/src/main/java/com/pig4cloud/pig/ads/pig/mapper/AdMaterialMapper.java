/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdMaterialResp;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.api.dto.AdMaterialSynResp;
import com.pig4cloud.pig.api.dto.UserByMaterialResp;
import com.pig4cloud.pig.api.entity.AdMaterial;
import com.pig4cloud.pig.api.vo.AdMaterialPageVo;
import com.pig4cloud.pig.api.vo.AdMaterialVo;
import com.pig4cloud.pig.api.vo.AdMaterialSearchVo;
import com.pig4cloud.pig.api.vo.MaterialUserVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 素材库
 * @date 2021-06-19 16:12:13
 */
@InterceptorIgnore(tenantLine = "1")
public interface AdMaterialMapper extends BaseMapper<AdMaterial> {

	/**
	 * 素材 - 页面
	 * @param req
	 * @return
	 */
	IPage<AdMaterialResp> selectByPage(AdMaterialPageVo req);
	/**
	 * 素材 - 页面
	 * @param req
	 * @return
	 */
	List<AdMaterialResp> selectByPage(AdMaterialVo req);
	/**
	 * 根据ID查素材信息
	 * @param req
	 * @return
	 */
	AdMaterialSynResp selectByPlatformFileId(AdMaterialVo req);

	@SqlParser(filter=true)
	IPage<AdMaterial> pageList(Page<AdMaterial> objectPage,@Param("searchVo") AdMaterialSearchVo searchVo);


	List<UserByMaterialResp> getUserByMaterial(MaterialUserVo req);
}
