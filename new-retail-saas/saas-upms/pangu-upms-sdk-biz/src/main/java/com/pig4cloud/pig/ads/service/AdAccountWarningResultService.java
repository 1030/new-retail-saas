package com.pig4cloud.pig.ads.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdAccountWarningResult;
import com.pig4cloud.pig.api.entity.Advertising;
import com.pig4cloud.pig.api.gdt.entity.GdtAdvFunds;
import com.pig4cloud.pig.api.vo.AdAccountWarnVo;
import com.pig4cloud.pig.common.core.util.R;

import java.math.BigDecimal;
import java.util.List;

public interface AdAccountWarningResultService extends IService<AdAccountWarningResult> {

		R noActiveAccountSendMail();

		R getNoActiveAccountList(Integer userId);

		List<Advertising> getTtAdAccountListByIds(List<String> ids);

		List<GdtAdvFunds> getGdtAdAccountListByIds(List<String> ids);

		List<AdAccountWarnVo> getNoCostCount(List<String> ids);

		List<AdAccountWarningResult> getNoActiveList();

		BigDecimal setNameAndAmount(List<AdAccountWarningResult> list);

		R refund(Integer mediaType,String advertiserId);
}
