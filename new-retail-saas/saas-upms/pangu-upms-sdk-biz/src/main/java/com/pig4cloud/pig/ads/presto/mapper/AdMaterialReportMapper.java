package com.pig4cloud.pig.ads.presto.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.dto.AdMaterialReportResp;
import com.pig4cloud.pig.api.dto.AdVideoAdsReportDTO;
import com.pig4cloud.pig.api.vo.AdMaterialReportVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdMaterialReportMapper extends BaseMapper<AdVideoAdsReportDTO> {

//	List<AdMaterialReportResp> selectAdMaterialReport(AdMaterialReportVo record);

}