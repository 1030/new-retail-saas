package com.pig4cloud.pig.ads.pig.mapper.gdt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.gdt.entity.GdtCustomAudiences;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @projectName:pig
 * @description:广点通人群信息
 * @author:Zhihao
 * @createTime:2020/12/4 17:56
 * @version:1.0
 */
@Mapper
public interface GdtCustomAudiencesMapper extends BaseMapper<GdtCustomAudiences> {
	@Select("select audience_id audienceId, name from gdt_custom_audiences")
	List<GdtCustomAudiences> getAllThirdIdAndNames();
}
