package com.pig4cloud.pig.ads.pig.mapper.ks;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.KsApp;
import org.apache.ibatis.annotations.Mapper;

/**
 * 快手应用表
 * @author  chenxiang
 * @version  2022-03-26 11:17:47
 * table: ks_app
 */
@Mapper
public interface KsAppMapper extends BaseMapper<KsApp> {
}


