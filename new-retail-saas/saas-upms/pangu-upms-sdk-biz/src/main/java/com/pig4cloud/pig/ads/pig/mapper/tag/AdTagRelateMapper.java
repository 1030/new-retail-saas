package com.pig4cloud.pig.ads.pig.mapper.tag;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import com.pig4cloud.pig.api.entity.tag.AdTagRelate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 标签关联表(ad_tag_relate)数据Mapper
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
*/
@Mapper
public interface AdTagRelateMapper extends BaseMapper<AdTagRelate> {

	List<AdTagRelate> selectAllListByIds(@Param("list") List<Long> idsList, @Param("type")Integer type);

}
