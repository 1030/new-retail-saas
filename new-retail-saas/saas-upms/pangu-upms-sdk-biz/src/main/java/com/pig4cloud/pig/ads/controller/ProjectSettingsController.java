package com.pig4cloud.pig.ads.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.ads.pig.mapper.ProjectSettingsMapper;
import com.pig4cloud.pig.api.dto.ProjectSettingsDto;
import com.pig4cloud.pig.api.entity.ProjectSettings;
import com.pig4cloud.pig.api.vo.ProjectSettingsVo;
import com.pig4cloud.pig.ads.service.ProjectSettingsService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author: nml
 * @time: 2020/11/4 14:25
 **/

@RestController
@RequiredArgsConstructor
@RequestMapping("/projectSet")
@Api(value = "projectSet", tags = "项目设置管理模块")
public class ProjectSettingsController {

	@Autowired
	private ProjectSettingsService projectSettingsService;

	@Autowired
	private ProjectSettingsMapper projectSettingsMapper;

	//查询所有，包括资源数
	@RequestMapping("/findAll")
	public R findAll(ProjectSettingsVo req){
		//设置分页条件
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());
		IPage<ProjectSettingsDto> projectSettingsDtoIPage = null;
		try {
			projectSettingsDtoIPage = projectSettingsService.selectAll(req);
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed(0,"查询失败");
		}
		return R.ok(projectSettingsDtoIPage,"查询成功");
	}

	//查询表列表
	@RequestMapping("/list")
	public R list(){
		List<ProjectSettings> projectSettingsDtos = null;
		try {
			projectSettingsDtos = projectSettingsMapper.selectList(new QueryWrapper<ProjectSettings>());
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed(0,"查询失败");
		}
		return R.ok(projectSettingsDtos,"查询成功");
	}

	@RequestMapping("/update")
	public R update(ProjectSettings req){
		req.setUpdateTime(new Date());
		try {
			return projectSettingsService.update(req);
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed(0,"操作失败");
		}
	}

	@RequestMapping("/add")
	public R add(ProjectSettings req){
		req.setCreateTime(new Date());
		req.setUpdateTime(new Date());
		try {
			return projectSettingsService.add(req);
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed(0,"操作失败");
		}
	}

}
