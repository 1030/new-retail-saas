/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.ads.service.ks.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.pig.mapper.ks.KsCampaignMapper;
import com.pig4cloud.pig.ads.service.ks.KsCampaignService;
import com.pig4cloud.pig.api.entity.ks.KsCampaign;
import org.springframework.stereotype.Service;

/**
 * 快手广告计划表
 *
 * @author yuwenfeng
 * @date 2022-03-26 17:31:08
 */
@Service
public class KsCampaignServiceImpl extends ServiceImpl<KsCampaignMapper, KsCampaign> implements KsCampaignService {

}
