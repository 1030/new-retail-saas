/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdMaterialPlatform;
import com.pig4cloud.pig.api.entity.AdMaterialPlatformToken;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 素材同步平台信息表
 *
 * @author pigx code generator
 * @date 2021-06-23 14:32:25
 */
@InterceptorIgnore(tenantLine = "1")
public interface AdMaterialPlatformMapper extends BaseMapper<AdMaterialPlatform> {

	List<AdMaterialPlatform> getListByMid(@Param(value = "adMaterialIds") List<Long> adMaterialIds);

	List<AdMaterialPlatformToken> getListByMidToken(@Param(value = "adMaterialIds") List<Long> adMaterialIds);

	List<AdMaterialPlatform> getListByMidAndIds(@Param(value = "advertiserIds") List<Long> advertiserIds,@Param(value = "ids") List<Long> ids);


}
