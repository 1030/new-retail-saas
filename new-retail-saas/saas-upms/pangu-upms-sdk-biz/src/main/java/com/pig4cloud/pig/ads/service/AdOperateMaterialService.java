package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdOperateMaterialDO;
import com.pig4cloud.pig.api.vo.AdOperateMaterialVO;
import com.pig4cloud.pig.api.vo.PushIconVO;
import sun.awt.image.ImageFormatException;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;

/**
 * 运营素材管理Service
 */
public interface AdOperateMaterialService extends IService<AdOperateMaterialDO> {

	/**
	 * 获取运营素材列表
	 *
	 * @param aom
	 * @return
	 */
	List<AdOperateMaterialDO> materialList(AdOperateMaterialVO aom);

	/**
	 * 获取运营素材分页列表
	 *
	 * @param page
	 * @param aom
	 * @return
	 */
	IPage<AdOperateMaterialDO> materialPage(Page<Object> page, AdOperateMaterialVO aom);

	/**
	 * 根据主键获取运营素材信息
	 *
	 * @param materialId
	 * @return
	 */
	AdOperateMaterialDO getMaterial(Long materialId);

	/**
	 * 上传运营素材
	 *
	 * @param aom
	 */
	void uploadMaterial(AdOperateMaterialVO aom) throws Exception;

	/**
	 * 编辑素材信息
	 *
	 * @param aom
	 */
	void updateMaterial(AdOperateMaterialVO aom);

	/**
	 * 推送Icon到头条或者广点通
	 *
	 * @param platformId
	 * @param materialId
	 * @param advertiserIds
	 * @return
	 * @throws IOException
	 * @throws ImageFormatException
	 */
	List<PushIconVO> pushIcon(Integer platformId, Long materialId, Collection<Long> advertiserIds) throws IOException, ImageFormatException;

	/**
	 * 将运营素材打包
	 *
	 * @param materialId
	 */
	Path materialPackZip(Long materialId) throws IOException;

	/**
	 * 删除运营素材
	 *
	 * @param materialIds
	 */
	void delete(Collection<Long> materialIds);

}
