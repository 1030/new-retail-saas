package com.pig4cloud.pig.ads.service.tag;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.tag.AdTag;
import com.pig4cloud.pig.api.entity.tag.AdTagDto;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 标签表服务接口
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
 */
public interface AdTagService extends IService<AdTag> {

	R tagList(AdTagDto dto);

	R pageList(AdTagDto dto);

	R add(AdTagDto dto);

	R edit(AdTagDto dto);

	R del(AdTagDto dto);
}
