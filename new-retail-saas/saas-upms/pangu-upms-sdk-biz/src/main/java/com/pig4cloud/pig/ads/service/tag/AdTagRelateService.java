package com.pig4cloud.pig.ads.service.tag;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.tag.AdTagRelate;
import com.pig4cloud.pig.api.entity.tag.BatchSettingTagDto;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 标签关联表服务接口
 *
 * @author zjz
 * @since 2023-02-23 11:39:32
 * @description 由 zjz 创建
 */
public interface AdTagRelateService extends IService<AdTagRelate> {


	R batchSettingTag(BatchSettingTagDto dto);
}
