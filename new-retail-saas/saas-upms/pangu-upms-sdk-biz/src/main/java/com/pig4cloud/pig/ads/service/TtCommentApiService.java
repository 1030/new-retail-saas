package com.pig4cloud.pig.ads.service;

import com.pig4cloud.pig.api.vo.TtResultVo;

import java.util.List;

public interface TtCommentApiService {

	/**
	 * 回复
	 *
	 * @param comment_id 评论id
	 * @param reply_text 回复内容
	 * @param reply_id   回复的id
	 * @return
	 */
	TtResultVo reply(Long advertiser_id, String access_token, Long comment_id, String reply_text, Long reply_id);

	/**
	 * 隐藏
	 *
	 * @param comment_id 评论id
	 * @return
	 */
	TtResultVo hide(Long advertiser_id, String access_token, Long comment_id);

	/**
	 * 置顶评论
	 *
	 * @param comment_id
	 * @return
	 */
	TtResultVo stickOnTop(Long advertiser_id, String access_token, Long comment_id);

	/**
	 * 屏蔽用户
	 *
	 * @param advertiser_id
	 * @param access_token
	 * @param comment_id
	 * @return
	 */
	TtResultVo blockUsers(Long advertiser_id, String access_token, Long comment_id);

	/**
	 * 屏蔽词添加
	 *
	 * @param advertiser_id
	 * @param access_token
	 * @param terms
	 * @return
	 */
	TtResultVo termsBannedAdd(Long advertiser_id, String access_token, List<String> terms);

	/**
	 * 删除屏蔽词
	 *
	 * @param advertiser_id
	 * @param access_token
	 * @param terms
	 * @return
	 */
	TtResultVo termsBannedDelete(Long advertiser_id, String access_token, List<String> terms);

	/**
	 * 更新屏蔽词
	 *
	 * @param advertiser_id
	 * @param access_token
	 * @return
	 */
	TtResultVo termsBannedUpdate(Long advertiser_id, String access_token,
								 String origin_terms, String new_terms);

	/**
	 * 添加屏蔽用户
	 */
	TtResultVo awemeBannedCreate(Long advertiser_id, String access_token,
								 String banned_type, List<String> aweme_ids, List<String> nickname_keywords);

	/**
	 * 删除屏蔽用户
	 */
	TtResultVo awemeBannedDelete(Long advertiser_id, String access_token,
								 String banned_type, String aweme_ids, String nickname_keywords);
}
