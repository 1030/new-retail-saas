package com.pig4cloud.pig.ads.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.ads.pig.mapper.TtAccountWaterMapper;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtAccountWaterMapper;
import com.pig4cloud.pig.ads.service.AdAccountWaterService;
import com.pig4cloud.pig.api.dto.AdAccountWaterDto;
import com.pig4cloud.pig.api.entity.TtAccountWater;
import com.pig4cloud.pig.api.gdt.entity.GdtAccountWater;
import com.pig4cloud.pig.common.core.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName AdAccountWaterServiceImpl
 * @Description done
 * @Author nieml
 * @Time 2021/7/14 17:44
 * @Version 1.0
 **/
@Service
@Slf4j
public class AdAccountWaterServiceImpl implements AdAccountWaterService {

	@Autowired
	private TtAccountWaterMapper ttAccountWaterMapper;

	@Autowired
	private GdtAccountWaterMapper gdtAccountWaterMapper;

	@Override
	public R queryAdAccountWaterPage(AdAccountWaterDto req) {
		try {
			Integer accountType = req.getAccountType();
			//账号类型为头条
			if (accountType.equals(1)){
				IPage<TtAccountWater> ttAccountWaterIPage = ttAccountWaterMapper.selectPageByParams(req);
				return R.ok(ttAccountWaterIPage);
			}
			//账号类型为广点通
			IPage<GdtAccountWater> gdtAccountWaterIPage = gdtAccountWaterMapper.selectPageByParams(req);
			return R.ok(gdtAccountWaterIPage);
		} catch (Exception e) {
			log.error("账户资金流水查询失败：{}",e);
			return R.failed("账户资金流水查询失败");
		}
	}
}
