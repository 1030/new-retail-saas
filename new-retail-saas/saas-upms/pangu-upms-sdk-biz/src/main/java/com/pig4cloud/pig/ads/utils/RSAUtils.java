package com.pig4cloud.pig.ads.utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * @projectName:pig
 * @description:RSA加解密工具类
 * @author:Mr.Chen
 * @createTime:2020/11/23 14:09
 * @version:1.0
 */
public final class RSAUtils {


	public static final String CHARSET = "UTF-8";
	/**
	 * 密钥算法
	 */
	public static final String ALGORITHM_RSA = "RSA";
	/**
	 * RSA 签名算法
	 */
	public static final String ALGORITHM_RSA_SIGN = "SHA256WithRSA";
	public static final int ALGORITHM_RSA_PRIVATE_KEY_LENGTH = 2048;
	/**
	 * 使用私钥解密运营平台传递的用户名和密码
	 */
	public static final String PRIVATE_KEY = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCZ6MO7QlkU6CpS8iEeMX+pgQ2/Udu+QVcrjvl8jYK+9v6r8gZe+atb6r51MzK2ePE0w6lcf9N91NMNGgZvkZ88gJ4gMrj6YZukPQ7SBJ5194Kcz9+FLgNcFpdyJ+C+4ICCtcMS4iohyvQl4FPrkS+qGYs+YeWuF0RSD7f+1UuUklGmhPzHJjq23fSm5UCVUMEp9h1Z3+ACB3hELU0W3CJzt7MJ55GrrwhiNoImBC91ls+ixaHGFGAdoHkW8j4pjuLEKONCj/6vmON7fdSlPPpzwvjTmfzvzvuonCurpJJJB/mo2af4jB0EZ1lF/JdeS7AbemZEHeNQJInrkN7ey/x1AgMBAAECggEBAIqzZrk6SLd7prDLficMqqTiJbZ2GdcyYVFi7tldWieoxcfFPBKZY+FZArzyH9Cr24wWjGlM1RbAFlbnTkd7b4qByn7v1x3lmvL/B30jJn7fSHjzKNeHqQVzYpZA89xwy79/+EXrRdbgZPhBWr+fPtpHXOIiLW+fjjV57vIb3JoNdaVXskso7avxImOReNJu/umsWUuzvLm/JLsRrI07QLRf6Vu+HlkGqhcJLv81cSgOQ36H4URfx99TgtbIARRqlw0orNpYZxBt7NjLAl5faHbQ0KabPnEC4scCZ8PsjL0w06Pb8TFTxhhbd/lgNK/5Izw9Q0dFTElkTKx6DtQxSh0CgYEA/ypXXzrBx6cqZs/S7VfgCRxG0Ho9cAhyZgIQJGqZoH58+/4eWnY1aFh7VwcocM/d1rbs9V9zf0FnTBAi/sgwk8mUoiS2djE0XNcVBbibUBdcVl4P9jYSgIf/RL/spyTY4ZU3VoeQHeG9km9tvr4beyDrxs5873P8c3U6Tk9lgrsCgYEAmmmjVnV4d4fgU/nPEtwsxRJHOGoc7+6A/wVUVZrQdEVsc0O87mkJzICsSbdRd9vDZzI7lDe1Dezz7ncpF/7IFe3gOaF95UzrMYFSflt4Ut447N4hksAg01ov8lc8ADvxeJ7TTDroQoKk2e2NPxnrUVwDC5yVsGqe1eEQO1NPgo8CgYEA6ZV/KkQFhJl47Wkuc+tPwcdK5Coi994jq1IUKOIVBKEGFUDJ1JIFziHBtydsU+lV8Oq//NeGSWY8A5t6DbaBEaklEVF+XrAoeRzDt9sJAsZ/1uiAwCk7wsG8Ytk28MOAbvFEGUg8G7685cJvun8ZdRvr+rCoGiQZZDBaUbnI+WUCgYEAheENo4raVhjTITw4lAueLYf5hCpMyKKr6XC/fDFsh0MiEpYqmzMrMcpmlsuAsUEznKfrDEl4i4hkvt9GG+3loU3L2WvxcfxM93E/CxtXqdHO3WCDD2/Rhi5t39pOxY0zgfeDEFcKxY/sSp8ylgFtC++RHGI5TupCqQIoRqenCnUCgYA6cF+/p2E59qS8GbxNGaFSINcmXWECRLLBPP0tX53p1oEHSuAT6tsgQfqsYHKzieFEnQtitbEC1XKxVPv933uu/yNU5VAw5NmGdzENzM/7pMpvyaHGKbGBTnUFFBpLHGyibBs+1NW4/1UbdKtpTI522LMk65a8yelJMm/iSPxTcw==";


	private RSAUtils() {
	}

	/**
	 * 初始化RSA算法密钥对
	 *
	 * @param keysize RSA1024已经不安全了,建议2048
	 * @return 经过Base64编码后的公私钥Map, 键名分别为publicKey和privateKey
	 */
	public static Map<String, String> initRSAKey(int keysize) {
		if (keysize != ALGORITHM_RSA_PRIVATE_KEY_LENGTH) {
			throw new IllegalArgumentException("RSA1024已经不安全了,请使用" + ALGORITHM_RSA_PRIVATE_KEY_LENGTH + "初始化RSA密钥对");
		}
		//为RSA算法创建一个KeyPairGenerator对象
		KeyPairGenerator kpg;
		try {
			kpg = KeyPairGenerator.getInstance(ALGORITHM_RSA);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException("No such algorithm-->[" + ALGORITHM_RSA + "]");
		}
		//初始化KeyPairGenerator对象,不要被initialize()源码表面上欺骗,其实这里声明的size是生效的
		kpg.initialize(ALGORITHM_RSA_PRIVATE_KEY_LENGTH);
		//生成密匙对
		KeyPair keyPair = kpg.generateKeyPair();
		//得到公钥
		Key publicKey = keyPair.getPublic();
		String publicKeyStr = Base64.getEncoder().encodeToString(publicKey.getEncoded());
		//得到私钥
		Key privateKey = keyPair.getPrivate();
		String privateKeyStr = Base64.getEncoder().encodeToString(privateKey.getEncoded());
		Map<String, String> keyPairMap = new HashMap<String, String>();
		keyPairMap.put("publicKey", publicKeyStr);
		keyPairMap.put("privateKey", privateKeyStr);
		return keyPairMap;
	}

	/**
	 * RSA算法公钥加密数据
	 *
	 * @param data 待加密的明文字符串
	 * @param key  RSA公钥字符串
	 * @return RSA公钥加密后的经过Base64编码的密文字符串
	 */
	public static String buildRSAEncryptByPublicKey(String data, String key) {
		try {
			//通过X509编码的Key指令获得公钥对象
			X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(key));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_RSA);
			Key publicKey = keyFactory.generatePublic(x509KeySpec);
			Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return Base64.getEncoder().encodeToString(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET)));
		} catch (Exception e) {
			throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
		}
	}

	/**
	 * RSA算法公钥解密数据
	 *
	 * @param data 待解密的经过Base64编码的密文字符串
	 * @param key  RSA公钥字符串
	 * @return RSA公钥解密后的明文字符串
	 */
	public static String buildRSADecryptByPublicKey(String data, String key) {
		try {
			//通过X509编码的Key指令获得公钥对象
			X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(key));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_RSA);
			Key publicKey = keyFactory.generatePublic(x509KeySpec);
			Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
			cipher.init(Cipher.DECRYPT_MODE, publicKey);
			return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64.getDecoder().decode(data)), CHARSET);
		} catch (Exception e) {
			throw new RuntimeException("解密字符串[" + data + "]时遇到异常", e);
		}
	}

	/**
	 * RSA算法私钥加密数据
	 *
	 * @param data 待加密的明文字符串
	 * @param key  RSA私钥字符串
	 * @return RSA私钥加密后的经过Base64编码的密文字符串
	 */
	public static String buildRSAEncryptByPrivateKey(String data, String key) {
		try {
			//通过PKCS#8编码的Key指令获得私钥对象
			PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(key));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_RSA);
			Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
			Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
			cipher.init(Cipher.ENCRYPT_MODE, privateKey);
			return Base64.getEncoder().encodeToString(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET)));
		} catch (Exception e) {
			throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
		}
	}

	/**
	 * RSA算法私钥解密数据
	 *
	 * @param data 待解密的经过Base64编码的密文字符串
	 * @param key  RSA私钥字符串
	 * @return RSA私钥解密后的明文字符串
	 */
	public static String buildRSADecryptByPrivateKey(String data, String key) {
		try {
			//通过PKCS#8编码的Key指令获得私钥对象
			PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(key));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_RSA);
			Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
			Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64.getDecoder().decode(data)), CHARSET);
		} catch (Exception e) {
			throw new RuntimeException("解密字符串[" + data + "]时遇到异常", e);
		}
	}

	/**
	 * RSA算法使用私钥对数据生成数字签名
	 *
	 * @param data 待签名的明文字符串
	 * @param key  RSA私钥字符串
	 * @return RSA私钥签名后的经过Base64编码的字符串
	 */
	public static String buildRSASignByPrivateKey(String data, String key) {
		try {
			//通过PKCS#8编码的Key指令获得私钥对象
			PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(key));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_RSA);
			PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
			Signature signature = Signature.getInstance(ALGORITHM_RSA_SIGN);
			signature.initSign(privateKey);
			signature.update(data.getBytes(CHARSET));
			return Base64.getEncoder().encodeToString(signature.sign());
		} catch (Exception e) {
			throw new RuntimeException("签名字符串[" + data + "]时遇到异常", e);
		}
	}

	/**
	 * RSA算法使用公钥校验数字签名
	 *
	 * @param data 参与签名的明文字符串
	 * @param key  RSA公钥字符串
	 * @param sign RSA签名得到的经过Base64编码的字符串
	 * @return true--验签通过,false--验签未通过
	 */
	public static boolean buildRSAverifyByPublicKey(String data, String key, String sign) {
		try {
			//通过X509编码的Key指令获得公钥对象
			X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(key));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_RSA);
			PublicKey publicKey = keyFactory.generatePublic(x509KeySpec);
			Signature signature = Signature.getInstance(ALGORITHM_RSA_SIGN);
			signature.initVerify(publicKey);
			signature.update(data.getBytes(CHARSET));
			return signature.verify(Base64.getDecoder().decode(sign));
		} catch (Exception e) {
			throw new RuntimeException("验签字符串[" + data + "]时遇到异常", e);
		}
	}

	/**
	 * RSA算法分段加解密数据
	 *
	 * @param cipher 初始化了加解密工作模式后的javax.crypto.Cipher对象
	 * @param opmode 加解密模式,值为javax.crypto.Cipher.ENCRYPT_MODE/DECRYPT_MODE
	 * @return 加密或解密后得到的数据的字节数组
	 */
	private static byte[] rsaSplitCodec(Cipher cipher, int opmode, byte[] datas) {
		int maxBlock = 0;
		if (opmode == Cipher.DECRYPT_MODE) {
			maxBlock = ALGORITHM_RSA_PRIVATE_KEY_LENGTH / 8;
		} else {
			maxBlock = ALGORITHM_RSA_PRIVATE_KEY_LENGTH / 8 - 11;
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] buff;
		int i = 0;
		try {
			while (datas.length > offSet) {
				if (datas.length - offSet > maxBlock) {
					buff = cipher.doFinal(datas, offSet, maxBlock);
				} else {
					buff = cipher.doFinal(datas, offSet, datas.length - offSet);
				}
				out.write(buff, 0, buff.length);
				i++;
				offSet = i * maxBlock;
			}
		} catch (Exception e) {
			throw new RuntimeException("加解密阀值为[" + maxBlock + "]的数据时发生异常", e);
		}
		byte[] resultDatas = out.toByteArray();
		IOUtils.closeQuietly(out);
		return resultDatas;
	}

	/**
	 * 跨平台登录密钥
	 *
	 * @param userName 用户昵称
	 */
	/*public static String getRSAKey(String userName) {
		//需要加密的字符串
		String name = userName + "_" + System.currentTimeMillis();
		return buildRSAEncryptByPublicKey(name, PUBLIC_KEY);
	}*/

	/**
	 * 跨平台私钥解密
	 *
	 * @param rsaKey 公钥加密后的字符串
	 * @return
	 */
	public static String getKey(String rsaKey) {
		//需要解密的字符串
		String key = buildRSADecryptByPrivateKey(rsaKey, PRIVATE_KEY);
		//私钥解密
		if (StringUtils.isNotEmpty(key)) {
			String[] keys = key.split("_");
			if (keys != null && keys.length == 2) {
				return keys[0];
			}
		}
		return null;
	}

	/*public static void main(String[] args) {
		 Map<String, String> map = initRSAKey(ALGORITHM_RSA_PRIVATE_KEY_LENGTH);
		 String privateKey = map.get("privateKey");
		 String publicKey = map.get("publicKey");
		 System.out.println(privateKey);
		 System.out.println(publicKey);

		//System.out.println("签名:" + buildRSASignByPrivateKey("admin123456", PRIVATE_KEY));
		//System.out.println("校验:" + buildRSAverifyByPublicKey("admin123456", PUBLIC_KEY, buildRSASignByPrivateKey("admin123456", PRIVATE_KEY)));
		//公钥加密
		*//*String key = getRSAKey("chenzh");
		System.out.println("加密后：");
		System.out.println(key);*//*

		//私钥解密
		String jmKey = getKey("UjSgJ1V8y5YfZD//AQ21p3765pF1gWTcAysw9wL0jckY+tO76cW9nhmKKDhrMb6DoBl8D7pHDqv7HnOdXVU6nOeR30Xg+iU1uEcSWefwg2BgxdTA3gtF1xdX/bhIjeTvyafoiZuVfWazJ0SsPn8/NmCQXDJUrUKJvTMzyhw5cJGN1HE+KVb4Re4fTjPcQ37IqXtHV4kUOHvsilc0NqhJAT0IWOUttsOzeqcFYoeKxulIfqg99+mkMV64H+aVoymyL0oEEkFGLSsDrjptBHyUbSeCO3+RUzseLcN1W1CqHDXe/l3/3+Urhdlalo85htCMzKtlAnITNxB2hp7Ac4a6jQ==");
		System.out.println("解密后：");
		System.out.println(jmKey);

		//String key1 = RSAUtils.getKey(key);

		//String key2="RJhi/IXkaAIEZsts+hWNPyj48lNnVYgaXyUTaBQwL0/rKPqj0BtvXulXBIkNPIspsznG4M4sprRuTscdvCfEowGwKAzHL/YeqWVBZcD1w2a67s0fha3argBe+KTWAZnwBdr4g9H9Ic4MFeXF8BXJ4u/QRtQldbMloOl1oJYWhwg961e68Tq7DhkyIsj4sYnEv/+da+PzxIDlTrySbpPl9OO66J3KbdgRmvJIJ7vuNKS3O5msGDWMmTksVGZf/NdHwOUkhU6xwAXQlVyPWNACO6b2nG1MeZsdkQQoZUzgO3XpNcgYPrMZnS8K6oO8S16Y8si12ejS3ggkyyBlisvw+A==";
		//System.out.println(decodeKey(key2));
	}*/

}
