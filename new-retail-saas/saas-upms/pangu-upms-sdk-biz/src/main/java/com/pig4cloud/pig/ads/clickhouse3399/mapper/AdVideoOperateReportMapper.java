package com.pig4cloud.pig.ads.clickhouse3399.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.dto.AdVideoOperateData;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @ClassName AdVideoReportMapper.java
 * @author cx
 * @version 1.0.0
 * @Time 2021/1/20 16:35
 */
@Mapper
public interface AdVideoOperateReportMapper extends BaseMapper<AdVideoOperateData> {
	@SqlParser(filter=true)
	AdVideoOperateData selectAdVideoOperateData(Map<String,Object> param);

}
