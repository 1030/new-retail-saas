package com.pig4cloud.pig.ads.gdt.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.ads.gdt.service.IGdtAdCreativeTempService;
import com.pig4cloud.pig.ads.pig.mapper.gdt.GdtAdCreativeTempMapper;
import com.pig4cloud.pig.api.gdt.dto.GdtAdCreativeDto;
import com.pig4cloud.pig.api.gdt.entity.GdtAdCreativeTemp;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 广点通广告创意临时 服务层实现
 * 
 * @author hma
 * @date 2020-12-11
 */
@Service
public class GdtAdCreativeTempServiceImpl  extends ServiceImpl<GdtAdCreativeTempMapper, GdtAdCreativeTemp> implements IGdtAdCreativeTempService
{

	/**
	 * 查询创意模板
	 * @param gdtAdCreativeDto
	 * @return
     */
	@Override
	public R getCreativeTemp(GdtAdCreativeDto gdtAdCreativeDto){
		Integer userId= SecurityUtils.getUser().getId();
		gdtAdCreativeDto.setUserId(userId.longValue());
		LambdaQueryWrapper<GdtAdCreativeTemp> queryWrapper= Wrappers.<GdtAdCreativeTemp>query().lambda().and(wrapper -> wrapper.eq(GdtAdCreativeTemp::getAccountId, gdtAdCreativeDto.getAccountId())).and(StringUtils.isNotBlank(gdtAdCreativeDto.getAccountId()), wrapper -> wrapper.eq(GdtAdCreativeTemp::getUserId, gdtAdCreativeDto.getUserId())).orderByDesc(GdtAdCreativeTemp::getId).last("LIMIT 1");
		List<GdtAdCreativeTemp> gdtAdCreativeTempList =baseMapper.selectList(queryWrapper);
		if(CollectionUtils.isNotEmpty(gdtAdCreativeTempList)){
			return R.ok(gdtAdCreativeTempList.get(0));
		}else{
			return R.ok();
		}
	}
	
}
