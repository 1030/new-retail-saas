package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdPicturePlatform;
import com.pig4cloud.pig.api.entity.PictureLib;
import com.pig4cloud.pig.api.vo.PictureLibVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * @description: 投放平台上传图片信息
 * @author: nml
 * @time: 2020/11/6 15:45
 **/
public interface AdPicturePlatformService extends IService<AdPicturePlatform> {
	//批量同步图片到头条方法
	R synPictureBatch(PictureLibVo pictureLibVo);

	//批量同步图片到广点通方法
	R uploadPicGdt(PictureLibVo req);

	R pushPic(PictureLibVo req);

	//同步头条图片方法
	boolean synPicture(PictureLib pictureLib, List<String> allList);
}
