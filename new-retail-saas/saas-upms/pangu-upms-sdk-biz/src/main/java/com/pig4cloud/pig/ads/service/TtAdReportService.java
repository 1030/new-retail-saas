package com.pig4cloud.pig.ads.service;

import com.pig4cloud.pig.api.vo.TtAdReportVO;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.ads.service
 * @Author 马嘉祺
 * @Date 2021/9/6 16:33
 * @Description
 */
public interface TtAdReportService {

	/**
	 * 同步头条创意日报表数据
	 *
	 * @param report
	 */
	void syncCreativeDayList(TtAdReportVO report);

	/**
	 * 同步头条创意小时报表数据
	 *
	 * @param report
	 */
	void syncCreativeHourList(TtAdReportVO report);

}
