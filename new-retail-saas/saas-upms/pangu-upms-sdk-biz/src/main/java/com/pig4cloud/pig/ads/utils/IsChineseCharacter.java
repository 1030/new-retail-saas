package com.pig4cloud.pig.ads.utils;

/**
 * @description:
 * @author: nml
 * @time: 2020/12/5 11:19
 **/
public class IsChineseCharacter {

	private static final boolean isChinese(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
				|| ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
				|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
				|| ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
			return true;
		}
		return false;
	}

	public static int ChineseCharNums(String str) {
		int count = 0;
		for (char c : str.toCharArray()) {
			if ((c >= 0x4E00 &&  c <= 0x9FA5) || isChinese(c)){
				count++;
			}
		}
		System.out.println("字符串包含中文字符数："+count);
		return count;
	}

}
