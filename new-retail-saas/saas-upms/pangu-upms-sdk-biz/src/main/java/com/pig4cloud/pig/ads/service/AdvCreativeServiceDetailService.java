/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.TtCreativeDetail;


/**
 * 广告创意接口类
 *
 * @author hma
 */
public interface AdvCreativeServiceDetailService extends IService<TtCreativeDetail> {

	TtCreativeDetail SyncAdCreativeDetail(String advertiserId, Long adId) throws Exception;

	void saveAdCreativeDetailList(String advertiserId, Long adId);

}
