package com.pig4cloud.pig.ads.pig.mapper.gdt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.GdtDynamicWordpack;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author nml
 *
 */
@Component
@Mapper
public interface GdtDynamicWordpackMapper extends BaseMapper<GdtDynamicWordpack> {

}