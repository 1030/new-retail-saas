

package com.pig4cloud.pig.ads.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdOauthSetting;

/**
 * 媒体授权信息配置表
 * @author  chengang
 * @version  2022-12-13 14:32:04
 * table: ad_oauth_setting
 */
public interface AdOauthSettingService extends IService<AdOauthSetting> {

	/**
	 * 根据媒体类型获取授权配置
	 * @param mediaCode
	 * @return
	 */
	AdOauthSetting getOauthSetting(String mediaCode);

}


