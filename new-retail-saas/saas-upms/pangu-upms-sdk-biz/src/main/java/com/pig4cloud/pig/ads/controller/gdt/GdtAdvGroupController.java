package com.pig4cloud.pig.ads.controller.gdt;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.pig4cloud.pig.ads.gdt.service.*;
import com.pig4cloud.pig.ads.service.*;
import com.pig4cloud.pig.ads.utils.DateUtils;
import com.pig4cloud.pig.api.dto.BudgetDto;
import com.pig4cloud.pig.api.entity.AdMaterialPlatform;
import com.pig4cloud.pig.api.enums.GdtBidModelEnum;
import com.pig4cloud.pig.api.enums.GdtSmartBidTypeEnum;
import com.pig4cloud.pig.api.enums.StatusEnum;
import com.pig4cloud.pig.api.gdt.dto.*;
import com.pig4cloud.pig.api.gdt.entity.*;
import com.pig4cloud.pig.api.gdt.vo.GdtAdvFundsVO;
import com.pig4cloud.pig.api.gdt.vo.GdtGroupInfoVo;
import com.pig4cloud.pig.api.util.Constants;
import com.pig4cloud.pig.api.util.NoRepeatSubmit;
import com.pig4cloud.pig.api.vo.AdMaterialVo;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import com.pig4cloud.pig.common.core.util.ECollectionUtil;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author hma
 * 2020-12-05
 * @广点通广告组临时管理模板
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/gdt/group")
@Api(value = "/gdt/group", tags = "广点通广告组管理模块")
public class GdtAdvGroupController {

	@Resource
	private IGdtAdgroupTempService gdtAdgroupTempService;


	@Autowired
	private GdtAdgroupDayReportService gdtAdgroupDayReportService;

	@Autowired
	private IGdtAdgroupService gdtAdgroupService;


	private final AdvService advService;

	private final StringRedisTemplate stringRedisTemplate;

	private final GdtAdvFundsService gdtAdvFundsService;

	private final IGdtAdService iGdtAdService;

	private final IGdtAdCreativeService iGdtAdCreativeService;

	private final GdtAudiencePackageService gdtAudiencePackageService;

	private final TaskExecutor taskExecutor;

	private final AdMaterialService adMaterialService;

	private final AdMaterialPlatformService adMaterialPlatformService;

	private final IGdtAdCopyService iGdtAdCopyService;

	//	@Value(value = "${spring.profiles.active}")
//	private String springProfilesActive;
	@Value(value = "${gdt.configured.status}")
	private String configuredStatus;


	@SysLog("获取广告组详情")
	@RequestMapping("/getAdGroupDetail")
	public R findGdtAdgroupDetailById(GdtAdgroup gdtAdgroup) {
		if (null == gdtAdgroup) {
			return R.ok("请传广告组id参数");
		}
		if (null == gdtAdgroup.getAdgroupId()) {
			return R.ok("请传广告组id参数");
		}

		return gdtAdgroupService.findGdtAdgroupDetailById(gdtAdgroup);
	}


	/**
	 * 获取广告版本列表
	 *
	 * @param gdtAdgroupTempDto
	 * @return
	 */
	@SysLog("获取广告版位")
	@RequestMapping("/getGroupSiteSet")
	public R getGroupSiteSet(GdtAdgroupTempDto gdtAdgroupTempDto) {
		if (null == gdtAdgroupTempDto) {
			return R.failed("参数不能为空");
		}
		if (StringUtils.isBlank(gdtAdgroupTempDto.getAccountId())) {
			return R.failed("广告账号不允许为空");
		}
		if (null == gdtAdgroupTempDto.getCampaignId()) {
			return R.failed("推广计划不允许为空");
		}
		return gdtAdgroupTempService.getGroupSiteSet(gdtAdgroupTempDto);
	}


	/**
	 * 创建临时广告组  定义post结构 @json格式
	 *
	 * @return
	 */
	@SysLog("创建临时广告组")
	@RequestMapping("/createGdtGroupTemp")
	public R createGdtGroupTemp(@RequestBody GdtAdgroupCreateTempDto gdtAdgroupCreateTempDto) {
		log.info("createGdtGroupTemp param" + JSONObject.toJSON(gdtAdgroupCreateTempDto));
		if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getAccountId())) {
			return R.failed("广告账号不允许为空");
		}
		if (null == gdtAdgroupCreateTempDto.getCampaignId()) {
			return R.failed("推广计划不允许为空");
		}
		if (!"1".equals(gdtAdgroupCreateTempDto.getAutomaticSiteEnabled()) && StringUtils.isBlank(gdtAdgroupCreateTempDto.getSiteSets())) {
			return R.failed("广告版位:自动版位和特定版位不允许同时为空");
		}
		if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getPromotedObjectId())) {
			return R.failed("应用ID不允许为空");
		}
		if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getBeginDate())) {
			return R.failed("起始时间不允许为空");
		}
		if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getCampaignType())) {
			return R.failed("广告形式不允许为空");
		}
		if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getBeginDate())) {
			return R.failed("起始时间不允许为空");
		}
		if (gdtAdgroupCreateTempDto.getBeginDate().length() > 10) {
			return R.failed("时间格式为:YYYY-MM-DD");
		}
		if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getTimeSeries())) {
			return R.failed("投放时间段不允许为空");
		}

		if (StringUtils.isNotBlank(gdtAdgroupCreateTempDto.getFirstDayBeginTime()) && gdtAdgroupCreateTempDto.getFirstDayBeginTime().length() > 8) {
			return R.failed("首日开始投放时间格式:HH:mm:ss");
		}
		String bidMode = gdtAdgroupCreateTempDto.getBidMode();

		if (StringUtils.isBlank(bidMode)) {
			return R.failed("出价方式不允许为空");
		}
		if (!GdtBidModelEnum.containsType(bidMode)) {
			return R.failed(String.format("不支持[%s]此类出价方式", bidMode));
		}
		if (StringUtils.equals(bidMode, GdtBidModelEnum.BID_MODE_CPA.getType())
				|| StringUtils.equals(bidMode, GdtBidModelEnum.BID_MODE_CPC.getType())
				|| StringUtils.equals(bidMode, GdtBidModelEnum.BID_MODE_CPM.getType())) {
			gdtAdgroupCreateTempDto.setBidStrategy(null);
			gdtAdgroupCreateTempDto.setAutoAcquisitionEnabled(null);
			gdtAdgroupCreateTempDto.setAutoAcquisitionBudget(null);
			gdtAdgroupCreateTempDto.setBidAdjustment(null);
			gdtAdgroupCreateTempDto.setSmartBidType(null);
			if (Objects.isNull(gdtAdgroupCreateTempDto.getBidAmount())) {
				return R.failed("出价不允许为空");
			}
		}
		if (StringUtils.equals(bidMode, GdtBidModelEnum.BID_MODE_OCPC.getType())
				|| StringUtils.equals(bidMode, GdtBidModelEnum.BID_MODE_OCPM.getType())) {
			if (GdtSmartBidTypeEnum.SMART_BID_TYPE_SYSTEMATIC.getType().equals(gdtAdgroupCreateTempDto.getSmartBidType())) {
				//当出价类型为自动出价时，bid_amount 不支持指定值
				gdtAdgroupCreateTempDto.setBidAmount(null);
				gdtAdgroupCreateTempDto.setAutoAcquisitionEnabled(null);
				gdtAdgroupCreateTempDto.setAutoAcquisitionBudget(null);
				gdtAdgroupCreateTempDto.setBidAdjustment(null);
				if (Objects.isNull(gdtAdgroupCreateTempDto.getDailyBudget()) || gdtAdgroupCreateTempDto.getDailyBudget().compareTo(new BigDecimal("0")) < 1) {
					return R.failed("选择自动出价后，广告日预算必填且不能为不限");
				}
			} else if (Objects.isNull(gdtAdgroupCreateTempDto.getBidAmount())) {
				return R.failed("出价不允许为空");
			}
			JSONObject jSONObject ;
			JSONArray ssp ;
			if (StringUtils.isNotBlank(gdtAdgroupCreateTempDto.getBidAdjustment())
					&& (!Objects.isNull(jSONObject = JSON.parseObject(gdtAdgroupCreateTempDto.getBidAdjustment())))
					&& !Objects.isNull(ssp = jSONObject.getJSONArray("site_set_package")) && ssp.size() > 0
					&& ssp.stream().map(v -> {
				JSONObject val = (JSONObject) v;
				Double vre = Objects.isNull(val.getDouble("bid_coefficient")) ?
						Objects.isNull(val.getDouble("deep_bid_coefficient")) ? 0D : val.getDouble("deep_bid_coefficient") : val.getDouble("bid_coefficient");
				return vre;
			}).filter(v -> v.doubleValue() < 0.5 || v.doubleValue() > 1.5).collect(Collectors.toList()).size() > 0) {
				return R.failed("版位出价系数或深度版位出价系数必须在[0.5,1.5]之间");
			}
			if (gdtAdgroupCreateTempDto.getConversionId()==null) {
				return R.failed("出价方式为oCPM和oCPC,转化不允许为空");
			}
		}

		if (!Objects.isNull(gdtAdgroupCreateTempDto.getBidAmount())) {
			//单位转为为分
			gdtAdgroupCreateTempDto.setBidAmount(gdtAdgroupCreateTempDto.getBidAmount().multiply(new BigDecimal(100)));
			if (StringUtils.equals(gdtAdgroupCreateTempDto.getBidMode(), GdtBidModelEnum.BID_MODE_OCPM.getType()) || StringUtils.equals(gdtAdgroupCreateTempDto.getBidMode(), GdtBidModelEnum.BID_MODE_OCPC.getType())) {
				if (gdtAdgroupCreateTempDto.getBidAmount().compareTo(new BigDecimal("10")) == -1 || gdtAdgroupCreateTempDto.getBidAmount().compareTo(new BigDecimal("500000")) == 1) {
					return R.failed("oCPC/oCPM 出价限制:0.1 元-5000 元");
				}
			}
			if (StringUtils.equals(gdtAdgroupCreateTempDto.getBidMode(), "BID_MODE_CPC")) {
				if (gdtAdgroupCreateTempDto.getBidAmount().compareTo(new BigDecimal("10")) == -1 || gdtAdgroupCreateTempDto.getBidAmount().compareTo(new BigDecimal("10000")) == 1) {
					return R.failed("CPC 出价限制：0.1 元-100 元");
				}
			}
			if (StringUtils.equals(gdtAdgroupCreateTempDto.getBidMode(), "BID_MODE_CPM")) {
				if (gdtAdgroupCreateTempDto.getBidAmount().compareTo(new BigDecimal("150")) == -1 || gdtAdgroupCreateTempDto.getBidAmount().compareTo(new BigDecimal("99900")) == 1) {
					return R.failed("CPM 出价限制：1.5 元-999 元");
				}
			}
			if (StringUtils.equals(gdtAdgroupCreateTempDto.getBidMode(), "BID_MODE_CPA")) {
				if (gdtAdgroupCreateTempDto.getBidAmount().compareTo(new BigDecimal("100")) == -1 || gdtAdgroupCreateTempDto.getBidAmount().compareTo(new BigDecimal("50000")) == 1) {
					return R.failed("CPA 出价限制：1 元-500 元");
				}
			}
		}
		if ("1".equals(gdtAdgroupCreateTempDto.getAutomaticSiteEnabled())
				&& StringUtils.equals("true", StringUtils.lowerCase(gdtAdgroupCreateTempDto.getAutoAcquisitionEnabled()))
				&& Objects.isNull(gdtAdgroupCreateTempDto.getAutoAcquisitionBudget())
		) {
			return R.failed("起量预算不能为空");
		}
		if (!Objects.isNull(gdtAdgroupCreateTempDto.getAutoAcquisitionBudget())) {
			gdtAdgroupCreateTempDto.setAutoAcquisitionBudget(gdtAdgroupCreateTempDto.getAutoAcquisitionBudget() * 100);
			if (new BigDecimal(gdtAdgroupCreateTempDto.getAutoAcquisitionBudget()).compareTo(gdtAdgroupCreateTempDto.getBidAmount()) == -1 || new BigDecimal(gdtAdgroupCreateTempDto.getAutoAcquisitionBudget()).compareTo(new BigDecimal("20000")) == -1 || new BigDecimal(gdtAdgroupCreateTempDto.getAutoAcquisitionBudget()).compareTo(new BigDecimal("10000000")) == 1) {
				return R.failed("起量预算范围:200-100000元，并且不能低于出价");
			}
		}
		//转换单位：分
		if (null != gdtAdgroupCreateTempDto.getDailyBudget()) {
			gdtAdgroupCreateTempDto.setDailyBudget(gdtAdgroupCreateTempDto.getDailyBudget().multiply(new BigDecimal("100")));
		}

		if (StringUtils.isBlank(gdtAdgroupCreateTempDto.getAdgroupName())) {
			return R.failed("广告名称不允许为空");
		}
		if (gdtAdgroupCreateTempDto.getAdgroupName().length() > 60) {
			return R.failed("广告名称长度不允许超过60个长度");
		}


		return gdtAdgroupTempService.createGdtGroupTemp(gdtAdgroupCreateTempDto);
	}


	/**
	 * 获取广告版本列表
	 *
	 * @param gdtAdgroupTempDto
	 * @return
	 */
	@SysLog("获取广告组模板")
	@RequestMapping("/getGroupTemplate")
	public R getGroupTemp(GdtAdgroupTempDto gdtAdgroupTempDto) {
		if (null == gdtAdgroupTempDto) {
			return R.failed("参数不能为空");
		}
		if (null == gdtAdgroupTempDto.getAccountId()) {
			return R.failed("广告账号不允许为空");
		}
		return gdtAdgroupTempService.getGroupTemp(gdtAdgroupTempDto);
	}

	/**
	 * 每一个推广目标只对应一个推广对象id
	 *
	 * @return
	 */
	@SysLog("获取广告推广目标")
	@RequestMapping("/getPromoteObjectId")
	public R getPromoteObjectId(GdtAdgroupTempDto gdtAdgroupTempDto) {
		//todo  字典表处理
		return gdtAdgroupTempService.getPromoteObjectId(gdtAdgroupTempDto);
	}


	/**
	 * 应用对应的转换归因数据查询
	 *
	 * @return
	 */
	/*@SysLog("转换归因数据查询")
	@RequestMapping("/getUserActionSets")
	public R getUserActionSets(GdtAdgroupTempDto gdtAdgroupTempDto) {
		//todo  字典表处理
		return gdtAdgroupTempService.getUserActionSets(gdtAdgroupTempDto);
	}*/

	/**
	 * 广告优化目标权限查询
	 *
	 * @return
	 */
	@SysLog("广告优化目标权限查询")
	@RequestMapping("/getOptimizationPermission")
	public R getOptimizationPermission(GdtAdgroupTempDto gdtAdgroupTempDto) {
		//todo  字典表处理
		if (null == gdtAdgroupTempDto) {
			return R.failed("参数不能为空");
		}
		if (StringUtils.isBlank(gdtAdgroupTempDto.getAccountId())) {
			return R.failed("广告账号不允许为空");
		}
		if (null == gdtAdgroupTempDto.getPromotedObjectType()) {
			return R.failed("推广目标类型不允许为空");
		}

		return gdtAdgroupTempService.getOptimizationPermission(gdtAdgroupTempDto);
	}

	/**
	 * 广告优化目标权限查询
	 *
	 * @return
	 */
	@SysLog("广告优化目标查询")
	@RequestMapping("/getOptimizationGoal")
	public R getOptimizationGoal(GdtAdgroupTempDto gdtAdgroupTempDto) {
		if (null == gdtAdgroupTempDto) {
			return R.failed("参数不能为空");
		}
		if (StringUtils.isBlank(gdtAdgroupTempDto.getAccountId())) {
			return R.failed("广告账号不允许为空");
		}
		if (null == gdtAdgroupTempDto.getPromotedObjectType()) {
			return R.failed("推广目标类型不允许为空");
		}
		return gdtAdgroupTempService.getOptimizationGoal(gdtAdgroupTempDto);
	}

	/**
	 * 获取广告版本列表
	 *
	 * @param req
	 * @return
	 */

	@RequestMapping("/getAdgroupDayReport")
	public R getAdgroupDayReport(GdtAdgroupDto req) {
		return gdtAdgroupDayReportService.getAdgroupDayReport(req);
	}

	/**
	 * 根据广告ID查询广告操作状态
	 *
	 * @param adgroupIds
	 * @return
	 */
	@GetMapping("/getAdgroupConfiguredStatus")
	public R getAdgroupConfiguredStatus(@RequestParam String adgroupIds) {
		Set<Long> adgroupIdSet = ECollectionUtil.stringToLongSet(adgroupIds);
		if (adgroupIdSet.isEmpty()) {
			return R.ok(Collections.emptyList());
		}
		List<GdtAdgroup> list = gdtAdgroupService.list(Wrappers.<GdtAdgroup>lambdaQuery().select(GdtAdgroup::getAdgroupId, GdtAdgroup::getConfiguredStatus).in(GdtAdgroup::getAdgroupId, adgroupIdSet));
		return R.ok(list);
	}

	@RequestMapping("/updateOnOff")
	public R updateOnOff(GdtAdgroupDto req) {
		R r = gdtAdgroupDayReportService.updateOnOff(req);
		if (r.getCode() == 0) {
			if ("Y".equals(req.getIgnoreWarning())) {

				GdtAdgroup gdtAdgroup = gdtAdgroupService.getBaseMapper().selectById(req.getAdgroupId());
				if (gdtAdgroup == null) {
					return R.failed("参数错误");
				}

				stringRedisTemplate.opsForValue().set(Constants.GDT_REDIS_ADGROUP_COST_WARNING_KEY_PRIX_ + req.getAdgroupId(), "N");
				GdtAdvFundsVO vo = new GdtAdvFundsVO();
				vo.setAccountId(gdtAdgroup.getAccountId());
				vo.setGroupCostWarning(null);
				return this.submitWarning(vo);
			}
			return r;
		} else {
			return r;
		}
	}

	@RequestMapping("/updateBidAmount")
	public R updateBidAmount(GdtAdgroupDto req) {
		return gdtAdgroupDayReportService.updateBidAmount(req);
	}

	@RequestMapping("/updateDailyBudget")
	public R updateDailyBudget(GdtAdgroupDto req) {
		return gdtAdgroupDayReportService.updateDailyBudget(req);
	}

	@RequestMapping("/orderDailyBudget")
	public R orderDailyBudget(BudgetDto budgetDto) {
		return gdtAdgroupDayReportService.orderDailyBudget(budgetDto, StatusEnum.OPERATE_TYPE_FOUR.getStatus());
	}

	/**
	 * 查询推广组预约当天定时设置预算值
	 *
	 * @param budgetDto
	 * @return success/false
	 */

	@RequestMapping("/getDailyBudget")
	public R getCampaignBudget(BudgetDto budgetDto) {
		return gdtAdgroupDayReportService.getAdvertiserJobBudget(budgetDto, StatusEnum.OPERATE_TYPE_FOUR.getStatus());
	}

	/**
	 * 获取出价方式
	 *
	 * @param gdtAdgroupTempDto
	 * @return
	 */
	@SysLog("获取出价方式")
	@RequestMapping("/getBidMode")
	public R getBidMode(GdtAdgroupTempDto gdtAdgroupTempDto) {
		if (null == gdtAdgroupTempDto) {
			return R.failed("参数不能为空");
		}
		if (StringUtils.isBlank(gdtAdgroupTempDto.getAccountId())) {
			return R.failed("广告账号不允许为空");
		}
		if (null == gdtAdgroupTempDto.getPromotedObjectType()) {
			return R.failed("推广目标类型不允许为空");
		}
		return gdtAdgroupTempService.getBidMode(gdtAdgroupTempDto);
	}


	/**
	 * @zhuxm
	 * @获取广告组账号预警阈值
	 */
	@SysLog("获取广告组账号预警阈值")
	@RequestMapping("/getWarning")
	public R getWarning(String accountId) {
		if (StringUtils.isBlank(accountId)) {
			return R.failed("广告账号不允许为空");
		}
		List<String> adList = advService.getOwnerAdv(SecurityUtils.getUser().getId(), PlatformTypeEnum.GDT.getValue());
		if (adList == null || adList.size() == 0 || !adList.contains(accountId)) {
			return R.failed("请传属于自己的账号");
		}

		return R.ok(gdtAdvFundsService.getById(accountId));
	}


	/**
	 * @zhuxm
	 * @获取广告组账号预警阈值
	 */
	@SysLog("提交广告组账号预警阈值")
	@RequestMapping("/submitWarning")
	public R submitWarning(@Valid GdtAdvFundsVO vo) {

		if (vo == null) {
			return R.failed("参数不能为空");
		}

		if (StringUtils.isBlank(vo.getAccountId())) {
			return R.failed("广告账号不允许为空");
		}

		UpdateWrapper<GdtAdvFunds> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("account_id", vo.getAccountId());
		updateWrapper.set("group_cost_warning", vo.getGroupCostWarning());

		return R.ok(gdtAdvFundsService.update(null, updateWrapper));
	}


	/**
	 * 广点通复制获取广告信息
	 *
	 * @return
	 */
	@SysLog("广点通复制获取广告信息")
	@RequestMapping("/getGroupInfo")
	public R getGroupInfo(@RequestParam(value = "adgroupId", required = false) String adgroupId) {
		GdtAdgroup gdtAdgroup = gdtAdgroupService.getById(adgroupId);
		if(gdtAdgroup==null){
			return R.failed("广告为空");
		}
		if(StringUtils.isBlank(gdtAdgroup.getConversionId())||"0".equals(gdtAdgroup.getConversionId())){
			return R.failed("广告转化ID为空，不能被复制");
		}
		GdtGroupInfoVo vo = new GdtGroupInfoVo();
		vo.setAdgroupName(gdtAdgroup.getAdgroupName());
		vo.setAdgroupId(gdtAdgroup.getAdgroupId());
		String sitsetString = gdtAdgroup.getAutomaticSiteEnabled()!=null&&gdtAdgroup.getAutomaticSiteEnabled()?"[\"SITE_SET_MOBILE_UNION\",\"SITE_SET_TENCENT_NEWS\",\"SITE_SET_TENCENT_VIDEO\",\"SITE_SET_KANDIAN\",\"SITE_SET_QQ_MUSIC_GAME\"]":gdtAdgroup.getSiteSet();
		vo.setSiteSet(JSONArray.parseArray(sitsetString,String.class));
		vo.setAppId(gdtAdgroup.getPromotedObjectId());
		vo.setAutomaticSiteEnabled(gdtAdgroup.getAutomaticSiteEnabled());
		vo.setPromotedObjectType(gdtAdgroup.getPromotedObjectType());
		if(StringUtils.isBlank(gdtAdgroup.getDynamicCreativeIdSet())){
			vo.setDynamicCreativeId("0");
		}else {
			String dyid = gdtAdgroup.getDynamicCreativeIdSet().replace("[", "").replace("]", "");
			vo.setDynamicCreativeId(StringUtils.isBlank(dyid)?"0":dyid);
		}
		return R.ok(vo);
	}


	/**
	 * 复制广点通广告
	 *
	 * @return
	 */
	@SysLog("广点通复制获取广告信息")
	@RequestMapping("/copyAd")
	public R copyAd(@Valid @RequestBody GdtAdCopyDto gdtAdCopyDto) {
		return iGdtAdCopyService.copyAd(gdtAdCopyDto);
	}
}
