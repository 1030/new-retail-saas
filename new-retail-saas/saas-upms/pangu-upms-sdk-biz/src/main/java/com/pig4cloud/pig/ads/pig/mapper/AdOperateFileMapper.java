package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdOperateFileDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity com.pig4cloud.pigx.mapper.AdOperateFileDO
 */
@Mapper
public interface AdOperateFileMapper extends BaseMapper<AdOperateFileDO> {

	/**
	 * 根据条件查询总数
	 *
	 * @param materialType
	 * @param fileName
	 * @return
	 */
	int selectCountByFileName(@Param("materialType") Integer materialType, @Param("fileName") String fileName);

}




