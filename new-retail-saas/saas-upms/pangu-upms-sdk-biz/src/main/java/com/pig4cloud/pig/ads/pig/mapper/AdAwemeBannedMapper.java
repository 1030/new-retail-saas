package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdAwemeBannedDto;
import com.pig4cloud.pig.api.entity.AdAwemeBannedEntity;
import com.pig4cloud.pig.api.entity.AdAwemeEntity;
import com.pig4cloud.pig.api.vo.AdAwemeBannedVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdAwemeBannedMapper extends BaseMapper<AdAwemeBannedEntity> {

	/**
	 * 批量插入
	 */
	Integer batchInsertAweme(List<AdAwemeBannedEntity> list);

	/**
	 * 插入
	 */
	Integer insertAweme(AdAwemeBannedEntity entity);

	/**
	 * 分页列表
	 * @param dto
	 * @return
	 */
	IPage<AdAwemeBannedVo> selectPageList(AdAwemeBannedDto dto);

	/**
	 * 根据id删除
	 * @param aweme_id
	 * @return
	 */
	Integer deleteByAweme_id(@Param("aweme_id") String aweme_id);

	Integer deleteAwemeById(@Param("id") Long id);

	/**
	 * 根据id查询
	 * @param
	 * @return
	 */
	AdAwemeEntity queryAwemeById(@Param("id") Long id);
}
