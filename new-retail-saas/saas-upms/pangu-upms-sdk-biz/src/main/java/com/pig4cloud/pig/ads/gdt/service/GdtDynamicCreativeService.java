package com.pig4cloud.pig.ads.gdt.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.GdtDynamicCreative;

import java.util.List;

/**
 * 广点通动态创意表服务接口
 *
 * @author zjz
 * @since 2022-07-11 22:26:02
 * @description 由 zjz 创建
 */
public interface GdtDynamicCreativeService extends IService<GdtDynamicCreative> {

	void saveOrUpdateList(List<GdtDynamicCreative> list);
}
