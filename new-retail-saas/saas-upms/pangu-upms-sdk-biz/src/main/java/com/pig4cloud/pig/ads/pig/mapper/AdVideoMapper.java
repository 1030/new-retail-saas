package com.pig4cloud.pig.ads.pig.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdVideoReportData;
import com.pig4cloud.pig.api.entity.AdVideo;
import com.pig4cloud.pig.api.vo.AdVideoReportVo;
import com.pig4cloud.pig.api.vo.AdVideoVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @ClassName AdVideoMapper.java
 * @author cx
 * @version 1.0.0
 * @Desc TODO
 * @Time 2020/11/5 13:46
 */
@Mapper
public interface AdVideoMapper extends BaseMapper<AdVideo> {

	IPage<AdVideoVo> selectAdVideoListPage(AdVideoVo req);

	List<String> selectUserIdListByAdmin(Map<String,Object> param);

	List<String> selectUserIdListByUser(Map<String,Object> param);

	IPage<AdVideoReportData> selectAdVideoReportList(AdVideoReportVo req);

	List<String> selectAdCreativeByList(Map<String,Object> param);

}