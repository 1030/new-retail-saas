package com.pig4cloud.pig.ads.pig.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pig.api.dto.AdPlanPictureLibDto;
import com.pig4cloud.pig.api.dto.PictureLibDto;
import com.pig4cloud.pig.api.vo.PictureLibVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.PictureLib;

/**
 * @description:
 * @author: nml
 * @time: 2020/10/29 19:42
 **/

@Component
@Mapper
@Primary
public interface PictureMapper extends BaseMapper<PictureLib> {
	int deleteByPrimaryKey(@Param("id") Long id);

	int insertSelective(PictureLib record);

	PictureLib selectByPrimaryKey(@Param("id") Integer id);

	int updateByPrimaryKeySelective(PictureLib record);

	int updateByPrimaryKey(PictureLib record);

	//分页模糊查询图片列表+同步状态
	IPage<PictureLibDto> selectAdPicListPage(PictureLibVo req);

	//根据图片类型和当前账户入参来分页查询图片
	IPage<AdPlanPictureLibDto> selectAdPlanPicListPage(PictureLibVo req);

	//根据图片id和广告账户id来查询：一条图片推送记录
	AdPlanPictureLibDto selectAdPlanPicSynData(PictureLibVo req);

	AdPlanPictureLibDto selectByKey(@Param("platformPictureId") String platformPictureId);

	Integer updatePTypeById(@Param("type") Integer type,@Param("id") Integer id);
}
