package com.pig4cloud.pig.ads.gdt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.dto.BudgetDto;
import com.pig4cloud.pig.api.gdt.entity.GdtCampaign;
import com.pig4cloud.pig.api.gdt.vo.GdtCampaignVo;

import com.pig4cloud.pig.common.core.util.R;

/**
 * @projectName:pig
 * @description:推广计划数据报表数据分析(clickhouse操作)
 * @author:yk
 * @createTime:2020/12/16
 * @version:1.0
 */
public interface GdtCampaignDayReportService  extends IService<GdtCampaign> {


	//分页查询 推广计划id或名称模糊查询所有广告组
	R getCampaignDayReport(GdtCampaignVo req);

	R updateOnOff(GdtCampaignVo req);

	R updateDailyBudget(GdtCampaignVo req);

	R orderDailyBudget(BudgetDto budgetDto, Integer operate);

	R getAdvertiserJobBudget(BudgetDto budgetDto, Integer operate);

}
