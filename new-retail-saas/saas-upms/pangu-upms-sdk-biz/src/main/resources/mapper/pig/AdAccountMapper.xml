<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~
  ~      Copyright (c) 2018-2025, lengleng All rights reserved.
  ~
  ~  Redistribution and use in source and binary forms, with or without
  ~  modification, are permitted provided that the following conditions are met:
  ~
  ~ Redistributions of source code must retain the above copyright notice,
  ~  this list of conditions and the following disclaimer.
  ~  Redistributions in binary form must reproduce the above copyright
  ~  notice, this list of conditions and the following disclaimer in the
  ~  documentation and/or other materials provided with the distribution.
  ~  Neither the name of the pig4cloud.com developer nor the names of its
  ~  contributors may be used to endorse or promote products derived from
  ~  this software without specific prior written permission.
  ~  Author: lengleng (wangiegie@gmail.com)
  ~
  -->

<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.pig4cloud.pig.ads.pig.mapper.AdAccountMapper">

	<resultMap id="adAccountMap" type="com.pig4cloud.pig.api.entity.AdAccount">
		<id property="id" column="id"/>
		<result property="mediaCode" column="media_code"/>
		<result property="mediaName" column="media_name"/>
		<result property="advertiserId" column="advertiser_id"/>
		<result property="advertiserName" column="advertiser_name"/>
		<result property="throwUser" column="throw_user"/>
		<result property="isDelete" column="is_delete"/>
		<result property="createUser" column="create_user"/>
		<result property="createTime" column="create_time"/>
		<result property="updateUser" column="update_user"/>
		<result property="updateTime" column="update_time"/>
	</resultMap>

	<resultMap id="AdAccountRespMapResult" type="com.pig4cloud.pig.api.dto.AdAccountResp">
		<result column="id" property="id"/>
		<result column="media_code" property="mediaCode"/>
		<result column="media_name" property="mediaName"/>
		<result column="advertiser_id" property="advertiserId"/>
		<result column="advertiser_name" property="advertiserName"/>
		<result column="housekeeper" property="housekeeper"/>
		<result column="throw_user" property="throwUser"/>
		<result column="is_paccount" property="isPaccount"/>
		<result column="throw_user_name" property="throwUserName"/>
		<result column="agent_name" property="agentName"/>
		<result column="rebate" property="rebate"/>
		<result column="advertiserType" property="advertiserType"/>
	</resultMap>


	<select id="queryAdByHousekeeper" resultType="com.pig4cloud.pig.api.entity.AdAccount"
			parameterType="com.pig4cloud.pig.api.vo.AdAccountVo">
		SELECT
		t.id,
		t.media_code mediaCode,
		t.media_name mediaName,
		t.advertiser_id advertiserId,
		t.housekeeper housekeeper,
		t.advertiser_name advertiserName,
		t.throw_user throwUser,
		t.is_paccount is_paccount
		FROM ad_account t
		where 1=1
		and t.is_delete = 0
		and t.is_paccount =1
		<if test="id != null and id !=''">
			and t.id != #{id}
		</if>
		<!-- 角色权限 -->
		<!--		<if test="isSys == null or isSys != 1 ">-->
		<!--			and t.throw_user in (${userIds})-->
		<!--		</if>-->
		<if test="housekeeper != null and housekeeper !=''">
			and t.housekeeper = #{housekeeper}
		</if>
	</select>

	<select id="selectByPage" resultMap="AdAccountRespMapResult">
		SELECT
		t.id,
		t.media_code,
		t.media_name,
		t.advertiser_id,
		t.housekeeper,
		t.advertiser_name,
		t.throw_user,
		t.is_paccount,
		atk.housekeeper advertiserType,
		c.real_name as throw_user_name,
		a.agent_name,
		b.rebate
		FROM ad_account t
		LEFT JOIN (
		SELECT a.advertiser_id,a.agent_id,d.agent_name
		FROM ad_account_agent a
		LEFT JOIN ad_agent d on a.agent_id = d.id
		WHERE a.is_delete = 0
		AND date_format(a.effective_time,'%Y-%m-%d') <![CDATA[ <= ]]> date_format(SYSDATE(),'%Y-%m-%d')
		AND date_format(a.invalid_time,'%Y-%m-%d') <![CDATA[ >= ]]> date_format(SYSDATE(),'%Y-%m-%d')
		GROUP BY a.advertiser_id
		) a on t.advertiser_id = a.advertiser_id
		LEFT JOIN (
		SELECT b.effective_time,b.agent_id,b.rebate,b.platform_id as media_code
		FROM ad_agent_rebate b
		WHERE b.id in (
		SELECT MAX(b.id)
		FROM ad_agent_rebate b
		WHERE b.is_delete = 0
		AND date_format(b.effective_time,'%Y-%m-%d') <![CDATA[ <= ]]> date_format(SYSDATE(),'%Y-%m-%d')
		GROUP BY b.agent_id,b.channel_code
		)
		) b on b.agent_id = a.agent_id and t.media_code = b.media_code
		LEFT JOIN sys_user c on c.user_id = t.throw_user
		LEFT JOIN tt_accesstoken atk ON atk.ad_account = t.housekeeper
		where 1=1
		AND IF(atk.housekeeper = 6,1=1,t.advertiser_id != t.housekeeper)
		and t.is_delete = 0
		<if test="advertiserId != null and advertiserId !=''">
			and t.advertiser_id = #{advertiserId}
		</if>
		<if test="advertiserName != null and advertiserName !=''">
			and t.advertiser_name like concat('%', #{advertiserName}, '%')
		</if>
		<if test="throwUser != null and throwUser !=''">
			and t.throw_user in (${throwUser})
		</if>
		<!-- 角色权限 -->
		<if test="isSys == null or isSys != 1 ">
			and t.throw_user in (${userIds})
		</if>
		<if test="mediaCode != null and mediaCode !=''">
			and t.media_code = #{mediaCode}
		</if>
		<if test="agentId != null and agentId !=''">
			and a.agent_id = #{agentId}
		</if>
	</select>

	<!--查询头条 流水 -->
	<select id="selectTtAdAccountBalance" resultType="java.util.Map"
			parameterType="java.util.List">
		select
		advertiser_id as adAccount,
		platform_id as platformId,
		balance,
		valid_balance,
		cash,
		valid_cash,
		`grant`,
		valid_grant
		from tt_advertiser
		where deleted = 0
		<if test="records!=null and records.size() > 0 ">
			AND advertiser_id in
			<foreach collection="records" index="index" item="item"
					 open="(" close=")" separator=",">
				#{item}
			</foreach>
		</if>
	</select>

	<!--查询广点通 余额 -->
	<select id="selectGdtAdAccountBalance" resultType="java.util.Map"
			parameterType="java.util.List">
		select
		account_id as adAccount,
		funds
		from gdt_adv_funds
		where 1=1
		<if test="records!=null and  records.size() > 0">
			AND account_id in
			<foreach collection="records" index="index" item="item"
					 open="(" close=")" separator=",">
				#{item}
			</foreach>
		</if>
	</select>


	<!--目标账户下拉框，显示相同管家账号下的其他普通广告账户 -->
	<select id="queryAccountByHousekeeper" resultType="java.util.Map"
			parameterType="java.lang.String">
		select
		advertiser_id as adAccount,
		advertiser_name as adAccountName
		from
		ad_account
		where 1=1
		and media_code =1
		<if test="housekeeper!=null and  housekeeper!=''">
			and housekeeper = #{housekeeper}
		</if>
		<if test="adAccount!=null and adAccount!=''">
			and advertiser_id != #{adAccount}
		</if>
	</select>

	<select id="selectDataByList" resultMap="AdAccountRespMapResult" parameterType="com.pig4cloud.pig.api.vo.AdAccountRequest">
		SELECT
		t.id,
		t.media_code,
		t.media_name,
		t.advertiser_id,
		t.housekeeper,
		t.advertiser_name,
		t.throw_user,
		t.is_paccount,
		atk.housekeeper advertiserType,
		c.real_name as throw_user_name,
		a.agent_name,
		b.rebate
		FROM ad_account t
		LEFT JOIN (
		SELECT a.advertiser_id,a.agent_id,d.agent_name
		FROM ad_account_agent a
		LEFT JOIN ad_agent d on a.agent_id = d.id
		WHERE a.is_delete = 0
		AND date_format(a.effective_time,'%Y-%m-%d') <![CDATA[ <= ]]> date_format(SYSDATE(),'%Y-%m-%d')
		AND date_format(a.invalid_time,'%Y-%m-%d') <![CDATA[ >= ]]> date_format(SYSDATE(),'%Y-%m-%d')
		GROUP BY a.advertiser_id
		) a on t.advertiser_id = a.advertiser_id
		LEFT JOIN (
		SELECT b.effective_time,b.agent_id,b.rebate,b.platform_id as media_code
		FROM ad_agent_rebate b
		WHERE b.id in (
		SELECT MAX(b.id)
		FROM ad_agent_rebate b
		WHERE b.is_delete = 0
		AND date_format(b.effective_time,'%Y-%m-%d') <![CDATA[ <= ]]> date_format(SYSDATE(),'%Y-%m-%d')
		GROUP BY b.agent_id,b.channel_code
		)
		) b on b.agent_id = a.agent_id and t.media_code = b.media_code
		LEFT JOIN sys_user c on c.user_id = t.throw_user
		LEFT JOIN tt_accesstoken atk ON atk.ad_account = t.housekeeper
		where 1=1
		AND IF(atk.housekeeper = 6,1=1,t.advertiser_id != t.housekeeper)
		and t.is_delete = 0 and t.effective_status = 1
		<if test="advertiserId != null and advertiserId !=''">
			and t.advertiser_id = #{advertiserId}
		</if>
		<if test="advertiserName != null and advertiserName !=''">
			and (t.advertiser_name like concat('%', #{advertiserName}, '%') or t.advertiser_id like concat('%', #{advertiserName}, '%'))
		</if>
		<if test="throwUser != null and throwUser !=''">
			and t.throw_user in (${throwUser})
		</if>
		<!-- 角色权限 -->
		<if test="isSys == null or isSys != 1 ">
			and t.throw_user in (${userIds})
		</if>
		<if test="mediaCode != null and mediaCode !=''">
			and t.media_code = #{mediaCode}
		</if>
		<if test="agentId != null and agentId !=''">
			and a.agent_id = #{agentId}
		</if>
		<if test="housekeeperType != null and housekeeperType !=''">
			and atk.housekeeper = #{housekeeperType}
		</if>
	</select>


</mapper>
