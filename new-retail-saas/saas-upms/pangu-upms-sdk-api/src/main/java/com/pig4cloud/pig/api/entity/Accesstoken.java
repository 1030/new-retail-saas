package com.pig4cloud.pig.api.entity;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * 广告主授权码
 * @author zhuxm
 *
 */
@Data
@NoArgsConstructor
public class Accesstoken implements Serializable {

	public Accesstoken(String accessToken, BigInteger expiresIn, String refreshToken, BigInteger refreshTokenExpires) {
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
		this.refreshToken = refreshToken;
		this.refreshTokenExpires = refreshTokenExpires;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7340672002737759135L;

	/**
     * 广告主账号
     */
	private String adAccount;
	
	
	private String appId;
	
	
	private String secret;
	
	/**
     * 广告主账号名称
     */
	private String authCode;
	
	/**
     * 访问令牌，用于业务接口调用。有效期24小时
     */
	private String accessToken;	
	/**
     * 当前时间，时间戳格式：1551663377887 
     */
	private BigInteger time;	
	/**
     * access_token的有效期，单位：秒，有效期24小时
     */
	private BigInteger expiresIn;	
	
	/**
	 * 刷新token,有效期30天
	 */
	private String refreshToken;
	
	/**
	 * 刷新token过期时间
	 */
	private BigInteger refreshTokenExpires;
	
	
	
	
	/**
     * 创建时间
     */
	private Date createtime;
	
	/**
     * 修改时间
     */
	private Date updatetime;
	
	/**
	 *  是否管家账户 
	 */
	private Integer housekeeper;
	
	
	
}