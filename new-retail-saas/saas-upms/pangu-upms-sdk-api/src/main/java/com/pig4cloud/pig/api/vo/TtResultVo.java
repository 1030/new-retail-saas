package com.pig4cloud.pig.api.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

@Data
public class TtResultVo {
	private Boolean success;//是否成功 true成 false失败
	private String message;//信息
	private JSONObject data;
}
