package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.entity.AudiencePackage;
import lombok.Data;


/**
 * 定向包
 * audience_package
 * @author kongyanfang
 * @date 2020-11-08 15:48:26
 */
@Data
public class AudiencePackageVo extends AudiencePackage {
	private int adPlanCount;

	private String advertiserName;

/*	private String launchPriceRange;*/
}