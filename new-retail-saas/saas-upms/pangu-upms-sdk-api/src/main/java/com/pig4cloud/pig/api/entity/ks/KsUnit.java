/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.api.entity.ks;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 快手广告组信息表
 *
 * @author yuwenfeng
 * @date 2022-03-26 17:31:08
 */
@Data
@TableName("ks_unit")
@ApiModel(value = "快手广告组信息表")
public class KsUnit implements Serializable {

    /**
     * 主键ID
     */
    @TableId
    @ApiModelProperty(value="主键ID")
    private Long id;

    /**
     * 广告组ID
     */
    @ApiModelProperty(value="广告组ID")
    private Long unitId;

    /**
     * 广告主ID
     */
    @ApiModelProperty(value="广告主ID")
    private Long advertiserId;

    /**
     * 计划ID
     */
    @ApiModelProperty(value="计划ID")
    private Long campaignId;

    /**
     * 广告组名称
     */
    @ApiModelProperty(value="广告组名称")
    private String unitName;

    /**
     * 广告组状态()-1：不限，1：计划已暂停，3：计划超预算，6：余额不足，，11：审核中，12：审核未通过，14：已结束，15：已暂停，17：组超预算，19：未达投放时间，20：有效
     */
    @ApiModelProperty(value="广告组状态()-1：不限，1：计划已暂停，3：计划超预算，6：余额不足，，11：审核中，12：审核未通过，14：已结束，15：已暂停，17：组超预算，19：未达投放时间，20：有效")
    private Integer status;

    /**
     * 投放状态(1：投放中；2：暂停 3：删除)
     */
    @ApiModelProperty(value="投放状态(1：投放中；2：暂停 3：删除)")
    private Integer putStatus;

    /**
     * 创建渠道(0：投放后台创建；1：Marketing API 创建)
     */
    @ApiModelProperty(value="创建渠道(0：投放后台创建；1：Marketing API 创建)")
    private Integer createChannel;

    /**
     * 审核拒绝理由
     */
    @ApiModelProperty(value="审核拒绝理由")
    private String reviewDetail;

    /**
     * 学习期(1:学习中,2:学习成功,3:学习失败)
     */
    @ApiModelProperty(value="学习期(1:学习中,2:学习成功,3:学习失败)")
    private Integer studyStatus;

    /**
     * 赔付状态(0=不需要赔付(不需要展示赔付标志)，1=成本保障生效中，2=成本保证确认中，3=已赔付完成，4=已失效)
     */
    @ApiModelProperty(value="赔付状态(0=不需要赔付(不需要展示赔付标志)，1=成本保障生效中，2=成本保证确认中，3=已赔付完成，4=已失效)")
    private Integer compensateStatus;

    /**
     * 出价类型(1：CPM，2：CPC，6：OCPC(使用 OCPC 代表 OCPX)，10：OCPM，20：eCPC)
     */
    @ApiModelProperty(value="出价类型(1：CPM，2：CPC，6：OCPC(使用 OCPC 代表 OCPX)，10：OCPM，20：eCPC)")
    private Integer bidType;

    /**
     * 出价(单位：厘)
     */
    @ApiModelProperty(value="出价(单位：厘)")
    private Long bid;

    /**
     * OCPC出价(单位：厘)
     */
    @ApiModelProperty(value="OCPC出价(单位：厘)")
    private Long cpaBid;

    /**
     * 优先低成本是否自动出价(0：手动出价，1：自动出价)
     */
    @ApiModelProperty(value="优先低成本是否自动出价(0：手动出价，1：自动出价)")
    private Integer smartBid;

    /**
     * 优化目标(0：未知，2：点击转化链接，10：曝光，11：点击，31：下载完成，53：提交线索，109：电话卡激活，137：量房，180：激活，190: 付费，191：首日 ROI，348：有效线索，383: 授信，384: 完件 715：微信复制;739:7 日付费次数)
     */
    @ApiModelProperty(value="优化目标(0：未知，2：点击转化链接，10：曝光，11：点击，31：下载完成，53：提交线索，109：电话卡激活，137：量房，180：激活，190: 付费，191：首日 ROI，348：有效线索，383: 授信，384: 完件 715：微信复制;739:7 日付费次数)")
    private Integer ocpxActionType;

    /**
     * 深度转化目标(3: 付费，7: 次日留存，10: 完件, 11: 授信 ，0：无)
     */
    @ApiModelProperty(value="深度转化目标(3: 付费，7: 次日留存，10: 完件, 11: 授信 ，0：无)")
    private Integer deepConversionType;

    /**
     * 深度转化目标出价(白名单功能，单位：厘)
     */
    @ApiModelProperty(value="深度转化目标出价(白名单功能，单位：厘)")
    private Long deepConversionBid;

    /**
     * 单日预算(单位：厘)
     */
    @ApiModelProperty(value="单日预算(单位：厘)")
    private Long dayBudget;

    /**
     * 分日预算(单位：厘，单日预算和分日预算同时存在时，以分日预算为准)
     */
    @ApiModelProperty(value="分日预算(单位：厘，单日预算和分日预算同时存在时，以分日预算为准)")
    private String dayBudgetSchedule;

    /**
     * 付费 ROI 系数(优化目标为「首日 ROI」时必填：ROI 系数取值范围 (0,100 ] 最多支持到三位小数（如：0.066）)
     */
    @ApiModelProperty(value="付费 ROI 系数(优化目标为「首日 ROI」时必填：ROI 系数取值范围 (0,100 ] 最多支持到三位小数（如：0.066）)")
    private BigDecimal roiRatio;

    /**
     * 投放方式(0：未知，1：正常投放，2：平滑投放，3：优先低成本)
     */
    @ApiModelProperty(value="投放方式(0：未知，1：正常投放，2：平滑投放，3：优先低成本)")
    private Integer speed;

    /**
     * 投放开始时间(格式：yyyy-MM-dd)
     */
    @ApiModelProperty(value="投放开始时间(格式：yyyy-MM-dd)")
    private LocalDateTime beginTime;

    /**
     * 投放结束时间(格式：yyyy-MM-dd,排期不限为 null)
     */
    @ApiModelProperty(value="投放结束时间(格式：yyyy-MM-dd,排期不限为 null)")
    private LocalDateTime endTime;

    /**
     * 投放时段(不投放的时段为 null)
     */
    @ApiModelProperty(value="投放时段(不投放的时段为 null)")
    private String schedule;

    /**
     * 周一时间段(时间段范围0, 23)
     */
    @ApiModelProperty(value="周一时间段(时间段范围0, 23)")
    private String scheduleMon;

    /**
     * 周二时间段(时间段范围0, 23)
     */
    @ApiModelProperty(value="周二时间段(时间段范围0, 23)")
    private String scheduleTues;

    /**
     * 周三时间段(时间段范围0, 23)
     */
    @ApiModelProperty(value="周三时间段(时间段范围0, 23)")
    private String scheduleWed;

    /**
     * 周四时间段(时间段范围0, 23)
     */
    @ApiModelProperty(value="周四时间段(时间段范围0, 23)")
    private String scheduleThur;

    /**
     * 周五时间段(时间段范围0, 23)
     */
    @ApiModelProperty(value="周五时间段(时间段范围0, 23)")
    private String scheduleFri;

    /**
     * 周六时间段(时间段范围0, 23)
     */
    @ApiModelProperty(value="周六时间段(时间段范围0, 23)")
    private String scheduleSat;

    /**
     * 周日时间段(时间段范围0, 23)
     */
    @ApiModelProperty(value="周日时间段(时间段范围0, 23)")
    private String scheduleSun;

    /**
     * 投放时段(24*7 的字符串，0 为不投放，1 为投放，例如：0010000000001....0000)
     */
    @ApiModelProperty(value="投放时段(24*7 的字符串，0 为不投放，1 为投放，例如：0010000000001....0000)")
    private String scheduleTime;

    /**
     * 广告位(1：优选广告位；2：按场景选择广告位-信息流广告（旧广告位，包含上下滑大屏广告）6：上下滑大屏广告；7：信息流广告（不包含上下滑大屏广告）24：激励视频；11：快看点场景)
     */
    @ApiModelProperty(value="广告位(1：优选广告位；2：按场景选择广告位-信息流广告（旧广告位，包含上下滑大屏广告）6：上下滑大屏广告；7：信息流广告（不包含上下滑大屏广告）24：激励视频；11：快看点场景)")
    private String sceneId;

    /**
     * 创意展现方式(0：未知，1：轮播，2：优选)
     */
    @ApiModelProperty(value="创意展现方式(0：未知，1：轮播，2：优选)")
    private Integer showMode;

    /**
     * 创意制作方式(4: 自定义; 7：程序化创意 2.0)
     */
    @ApiModelProperty(value="创意制作方式(4: 自定义; 7：程序化创意 2.0)")
    private Integer unitType;

    /**
     * 转化目标
     */
    @ApiModelProperty(value="转化目标")
    private String convertId;

    /**
     * 优先从系统应用商店下载(0：未设置 1：优先从系统应用商店下载使用)
     */
    @ApiModelProperty(value="优先从系统应用商店下载(0：未设置 1：优先从系统应用商店下载使用)")
    private Integer useAppMarket;

    /**
     * 应用商店列表
     */
    @ApiModelProperty(value="应用商店列表")
    private String appStore;

    /**
     * url 类型(当计划类型为 3 时（获取电商下单）时有返回。1：淘宝商品短链 2：淘宝商品 itemID)
     */
    @ApiModelProperty(value="url 类型(当计划类型为 3 时（获取电商下单）时有返回。1：淘宝商品短链 2：淘宝商品 itemID)")
    private Integer urlType;

    /**
     * url 类型(当计划类型为 5（收集销售线索）&使用建站时有返回：需使用魔力建站 不传默认 1，2：落地页)
     */
    @ApiModelProperty(value="url 类型(当计划类型为 5（收集销售线索）&使用建站时有返回：需使用魔力建站 不传默认 1，2：落地页)")
    private Integer webUriType;

    /**
     * 落地页链接(	计划类型是 2（提升应用安装）：返回应用下载地址；计划类型是 3（获取电商下单）：根据 url_type 返回相应信息；计划类型是 4（推广品牌活动）：返回落地页 url 计划类型是 5（收集销售线索）：返回落地页 url 计划类型是 5（收集销售线索）：建站 idl)
     */
    @ApiModelProperty(value="落地页链接(	计划类型是 2（提升应用安装）：返回应用下载地址；计划类型是 3（获取电商下单）：根据 url_type 返回相应信息；计划类型是 4（推广品牌活动）：返回落地页 url 计划类型是 5（收集销售线索）：返回落地页 url 计划类型是 5（收集销售线索）：建站 idl)")
    private String url;

    /**
     * 调起链接(提升应用活跃营销目标的调起链接)
     */
    @ApiModelProperty(value="调起链接(提升应用活跃营销目标的调起链接)")
    private String schemaUri;

    /**
     * 应用ID
     */
    @ApiModelProperty(value="应用ID")
    private String appId;

    /**
     * APP图标存储地址
     */
    @ApiModelProperty(value="APP图标存储地址")
    private String appIconUrl;

    /**
     * 应用名称
     */
    @ApiModelProperty(value="应用名称")
    private String appName;

    /**
     * 应用包名
     */
    @ApiModelProperty(value="应用包名")
    private String appPackageName;

    /**
     * 应用操作系统类型(0：未知，1：ANDROID，2：IO)
     */
    @ApiModelProperty(value="应用操作系统类型(0：未知，1：ANDROID，2：IO)")
    private Integer deviceOsType;

    /**
     * 数据总数
     */
    @ApiModelProperty(value="数据总数")
    private Integer totalCount;

    /**
     * 地域
     */
    @ApiModelProperty(value="地域")
    private String region;

    /**
     * 商圈定向
     */
    @ApiModelProperty(value="商圈定向")
    private String districtIds;

    /**
     * 年龄最小限制(18)
     */
    @ApiModelProperty(value="年龄最小限制(18)")
    private Integer ageMin;

    /**
     * 年龄最大限制(55)
     */
    @ApiModelProperty(value="年龄最大限制(55)")
    private Integer ageMax;

    /**
     * 固定年龄段(【18：表示 18-23 岁】【24：表示 24-30 岁】【31：表示 31-40 岁】【41：表示 41-49 岁】【50：表示 50-100 岁】)
     */
    @ApiModelProperty(value="固定年龄段(【18：表示 18-23 岁】【24：表示 24-30 岁】【31：表示 31-40 岁】【41：表示 41-49 岁】【50：表示 50-100 岁】)")
    private String agesRange;

    /**
     * 性别(1：女性, 2：男性，0 表示不限)
     */
    @ApiModelProperty(value="性别(1：女性, 2：男性，0 表示不限)")
    private Integer gender;

    /**
     * 操作系统(1：Android，2：iOS，0 表示不限)
     */
    @ApiModelProperty(value="操作系统(1：Android，2：iOS，0 表示不限)")
    private Integer platformOs;

    /**
     * Android版本(3：不限，4：4.x+，5：5.x+，6：6.x+，7：7.x+，8：8.x+，9：9.x+，10：10.x+)
     */
    @ApiModelProperty(value="Android版本(3：不限，4：4.x+，5：5.x+，6：6.x+，7：7.x+，8：8.x+，9：9.x+，10：10.x+)")
    private Integer androidOsv;

    /**
     * iOS版本(6：不限，7：7.x+，8：8.x+，9：9.x+，10：10.x+；)
     */
    @ApiModelProperty(value="iOS版本(6：不限，7：7.x+，8：8.x+，9：9.x+，10：10.x+；)")
    private Integer iosOsv;

    /**
     * 网络环境(1：Wi-Fi，2：移动网络，0：表示不限)
     */
    @ApiModelProperty(value="网络环境(1：Wi-Fi，2：移动网络，0：表示不限)")
    private Integer network;

    /**
     * 设备品牌(1：OPPO，2：VIVO，3：华为，4：小米，5：荣耀，6：三星，7：金立，8：魅族，9：乐视，10：其他，11：苹果)
     */
    @ApiModelProperty(value="设备品牌(1：OPPO，2：VIVO，3：华为，4：小米，5：荣耀，6：三星，7：金立，8：魅族，9：乐视，10：其他，11：苹果)")
    private String deviceBrand;

    /**
     * 设备价格(1：1500 元以下，2：1501~2000，3：2001~2500，4：2501~3000，5：3001~3500，6：3501~4000，7：4001~4500，8：4501~5000，9： 5001~5500，10：5500 元以上)
     */
    @ApiModelProperty(value="设备价格(1：1500 元以下，2：1501~2000，3：2001~2500，4：2501~3000，5：3001~3500，6：3501~4000，7：4001~4500，8：4501~5000，9： 5001~5500，10：5500 元以上)")
    private String devicePrice;

    /**
     * 网红粉丝
     */
    @ApiModelProperty(value="网红粉丝")
    private String fansStar;

    /**
     * 兴趣视频用户
     */
    @ApiModelProperty(value="兴趣视频用户")
    private String interestVideo;

    /**
     * APP行为-按分类
     */
    @ApiModelProperty(value="APP行为-按分类")
    private String appInterest;

    /**
     * APP行为-按 APP 名称
     */
    @ApiModelProperty(value="APP行为-按 APP 名称")
    private String appIds;

    /**
     * app名称(和app_ids 对应，是 app_id 对应的名称)
     */
    @ApiModelProperty(value="app名称(和app_ids 对应，是 app_id 对应的名称)")
    private String appNames;

    /**
     * 过滤已转化人群纬度(0(默认)：不限 1：广告组 2：广告计划 3：本账户 4：公司主体 5：APP)
     */
    @ApiModelProperty(value="过滤已转化人群纬度(0(默认)：不限 1：广告组 2：广告计划 3：本账户 4：公司主体 5：APP)")
    private Integer filterConvertedLevel;

    /**
     * 人群包定向
     */
    @ApiModelProperty(value="人群包定向")
    private String population;

    /**
     * 人群包排除
     */
    @ApiModelProperty(value="人群包排除")
    private String excludePopulation;

    /**
     * 媒体定向包
     */
    @ApiModelProperty(value="媒体定向包")
    private String media;

    /**
     * 媒体定向排除包(media 和 exclude_media 只可选其一)
     */
    @ApiModelProperty(value="媒体定向排除包(media 和 exclude_media 只可选其一)")
    private String excludeMedia;

    /**
     * 媒体包来源(默认为 0，0-不限，未指定，1-行业优质流量包，2-广告主自定义)
     */
    @ApiModelProperty(value="媒体包来源(默认为 0，0-不限，未指定，1-行业优质流量包，2-广告主自定义)")
    private Integer mediaSourceType;

    /**
     * 开启智能扩量(0：关闭智能扩量，1：打开智能扩量)
     */
    @ApiModelProperty(value="开启智能扩量(0：关闭智能扩量，1：打开智能扩量)")
    private Integer intelliIsOpen;

    /**
     * 不可突破年龄(0：可突破/无需控制，1：不可突破)
     */
    @ApiModelProperty(value="不可突破年龄(0：可突破/无需控制，1：不可突破)")
    private Integer intelliNoAgeBreak;

    /**
     * 不可突破性别(0：可突破/无需控制，1：不可突破)
     */
    @ApiModelProperty(value="不可突破性别(0：可突破/无需控制，1：不可突破)")
    private Integer intelliNoGenderBreak;

    /**
     * 不可突破地域(0：可突破/无需控制，1：不可突破)
     */
    @ApiModelProperty(value="不可突破地域(0：可突破/无需控制，1：不可突破)")
    private Integer intelliNoAreaBreak;

    /**
     * 行为定向关键词
     */
    @ApiModelProperty(value="行为定向关键词")
    private String keyword;

    /**
     * 行为定向，类目词
     */
    @ApiModelProperty(value="行为定向，类目词")
    private String behaviorLabel;

    /**
     * 在多少天内发生行为的用户(0:7 天 1：15 天 2：30 天 3：90 天 4：180 天)
     */
    @ApiModelProperty(value="在多少天内发生行为的用户(0:7 天 1：15 天 2：30 天 3：90 天 4：180 天)")
    private Integer timeType;

    /**
     * 行为强度(0：不限 1：高强度)
     */
    @ApiModelProperty(value="行为强度(0：不限 1：高强度)")
    private Integer strengthType;

    /**
     * 行为场景(1：社区 2：APP 4：推广)
     */
    @ApiModelProperty(value="行为场景(1：社区 2：APP 4：推广)")
    private String sceneType;

    /**
     * 兴趣定向类目词
     */
    @ApiModelProperty(value="兴趣定向类目词")
    private String interestLabel;

    /**
     * 种子人群包(当账户开通种子人群包功能后方可使用此功能)
     */
    @ApiModelProperty(value="种子人群包(当账户开通种子人群包功能后方可使用此功能)")
    private String seedPopulation;

    /**
     * 预约广告
     */
    @ApiModelProperty(value="预约广告")
    private String siteType;

    /**
     * 礼包码(最多 20 字符，只能字母+数字)
     */
    @ApiModelProperty(value="礼包码(最多 20 字符，只能字母+数字)")
    private String giftCode;

    /**
     * 发送时机(android:30： 开始下载后 31：下载完成后 32：安装完成后 ios：2：检测到用户行为)
     */
    @ApiModelProperty(value="发送时机(android:30： 开始下载后 31：下载完成后 32：安装完成后 ios：2：检测到用户行为)")
    private Integer giftTargetActionType;

    /**
     * 是否使用落地页前置功能(true: 使用 false：不使用)
     */
    @ApiModelProperty(value="是否使用落地页前置功能(true: 使用 false：不使用)")
    private Integer videoLandingPage;

    /**
     * 智能定向(若开启智能定向，定向部分仅保留地域+年龄+性别+排除人群+系统纬度的定向，其他定向纬度暂不支持（报错处理）true 表示开启，false 表示未开启)
     */
    @ApiModelProperty(value="智能定向(若开启智能定向，定向部分仅保留地域+年龄+性别+排除人群+系统纬度的定向，其他定向纬度暂不支持（报错处理）true 表示开启，false 表示未开启)")
    private Integer autoTarget;

    /**
     * 程序化创意 2.0 智能抽帧(是否开启智能抽帧)
     */
    @ApiModelProperty(value="程序化创意 2.0 智能抽帧(是否开启智能抽帧)")
    private Integer smartCover;

    /**
     * 程序化创意 2.0 素材挖掘(是否开启历史素材挖掘)
     */
    @ApiModelProperty(value="程序化创意 2.0 素材挖掘(是否开启历史素材挖掘)")
    private Integer assetMining;

    /**
     * 是否开启自动生成视频
     */
    @ApiModelProperty(value="是否开启自动生成视频")
    private Integer autoCreatePhoto;

    /**
     * 最后修改时间
     */
    @ApiModelProperty(value="最后修改时间")
    private LocalDateTime lastUpdateTime;

    /**
     * 电商关联Id (小店通)(1. merchantItemType 为 0 时表示小店商品 id; 2. merchantItemType 为 2 时表示快手 id)
     */
    @ApiModelProperty(value="电商关联Id (小店通)(1. merchantItemType 为 0 时表示小店商品 id; 2. merchantItemType 为 2 时表示快手 id)")
    private Long itemId;

    /**
     * 小说ID
     */
    @ApiModelProperty(value="小说ID")
    private Long fictionId;

    /**
     * 电商广告投放类型（小店通）(0: 商品; 2: 个人主页)
     */
    @ApiModelProperty(value="电商广告投放类型（小店通）(0: 商品; 2: 个人主页)")
    private Integer merchantItemPutType;

    /**
     * 是否使用了咨询组件(0=未使用，1=使用)
     */
    @ApiModelProperty(value="是否使用了咨询组件(0=未使用，1=使用)")
    private Integer consultId;

    /**
     * 高级创意开关(0：关闭 1:开启)
     */
    @ApiModelProperty(value="高级创意开关(0：关闭 1:开启)")
    private Integer advCardOption;

    /**
     * 高级创意列表
     */
    @ApiModelProperty(value="高级创意列表")
    private String advCardList;

    /**
     * 卡片id
     */
    @ApiModelProperty(value="卡片id")
    private Long advCardId;

    /**
     * 卡片类型(100:图片卡片 101:多利益卡-图文 102：多利益卡-多标签 103：电商促销样式)
     */
    @ApiModelProperty(value="卡片类型(100:图片卡片 101:多利益卡-图文 102：多利益卡-多标签 103：电商促销样式)")
    private Integer cardType;

    /**
     * 图片url
     */
    @ApiModelProperty(value="图片url")
    private String advCardUrl;

    /**
     * 标题
     */
    @ApiModelProperty(value="标题")
    private String title;

    /**
     * 副标题
     */
    @ApiModelProperty(value="副标题")
    private String subTitle;

    /**
     * 原价格(单位：分)
     */
    @ApiModelProperty(value="原价格(单位：分)")
    private Long price;

    /**
     * 售卖价(单位：分)
     */
    @ApiModelProperty(value="售卖价(单位：分)")
    private Long salePrice;

    /**
     * backflow_cv_lower：回流预估值的下限；backflow_cv_upper：回流预估值的上限；backflow_timestamp：本次回流预估数据的时间戳，13 位毫秒时间戳;backflow_payment：回流预估总金额;backflow_roi：回流首日预估 ROI
     */
    @ApiModelProperty(value="backflow_cv_lower：回流预估值的下限；backflow_cv_upper：回流预估值的上限；backflow_timestamp：本次回流预估数据的时间戳，13 位毫秒时间戳;backflow_payment：回流预估总金额;backflow_roi：回流首日预估 ROI")
    private String backflowForecast;

    /**
     * 商品ID，且一旦绑定，不可修改
     */
    @ApiModelProperty(value="商品ID，且一旦绑定，不可修改")
    private Long merchandiseId;

    /**
     * 课程类型(与 merchandise_id 共同使用，merchandise_type=2，merchandise_id 为课程 ID，仅支持“收集销售线索”计划类型，且一旦绑定不可修改)
     */
    @ApiModelProperty(value="课程类型(与 merchandise_id 共同使用，merchandise_type=2，merchandise_id 为课程 ID，仅支持“收集销售线索”计划类型，且一旦绑定不可修改)")
    private Integer merchandiseType;

    /**
     * 试玩素材的横竖适配(默认值为-1；0:横竖均可；1:竖屏；2:横屏)
     */
    @ApiModelProperty(value="试玩素材的横竖适配(默认值为-1；0:横竖均可；1:竖屏；2:横屏)")
    private Integer playableOrientation;

    /**
     * 试玩的 url
     */
    @ApiModelProperty(value="试玩的 url")
    private String playableUrl;

    /**
     * 试玩广告的文件名
     */
    @ApiModelProperty(value="试玩广告的文件名")
    private String playableFileName;

    /**
     * 试玩广告的开关(默认值为 0；1:关闭；2:开启)
     */
    @ApiModelProperty(value="试玩广告的开关(默认值为 0；1:关闭；2:开启)")
    private Integer playableSwitch;

    /**
     * 试玩ID(可选字段，开启试玩时存在，否则不存在。当用户上传试玩素材成功时返回，用于之后对于广告组中试玩创意的编辑操作。)
     */
    @ApiModelProperty(value="试玩ID(可选字段，开启试玩时存在，否则不存在。当用户上传试玩素材成功时返回，用于之后对于广告组中试玩创意的编辑操作。)")
    private Long playableId;

    /**
     * 试玩按钮文字内容(开启试玩时存在，否则不存在，示例按钮内容如下：1：立即试玩；2：试玩一下；3：立即体验；4：免装试玩；5：免装体验。启用试玩时：默认“立即试玩”，未启用试玩时：内容为空。)
     */
    @ApiModelProperty(value="试玩按钮文字内容(开启试玩时存在，否则不存在，示例按钮内容如下：1：立即试玩；2：试玩一下；3：立即体验；4：免装试玩；5：免装体验。启用试玩时：默认“立即试玩”，未启用试玩时：内容为空。)")
    private String playButton;

    /**
     * 是否投放开屏广告位(true:投放，false：不投放)
     */
    @ApiModelProperty(value="是否投放开屏广告位(true:投放，false：不投放)")
    private Integer splashAdSwitch;

    /**
     * 商品库ID(sdpa 类型广告组才会存在)
     */
    @ApiModelProperty(value="商品库ID(sdpa 类型广告组才会存在)")
    private Long libraryId;

    /**
     * 商品ID(sdpa 类型广告组才会存在)
     */
    @ApiModelProperty(value="商品ID(sdpa 类型广告组才会存在)")
    private String outerId;

    /**
     * 商品名称(sdpa 类型广告组才会存在)
     */
    @ApiModelProperty(value="商品名称(sdpa 类型广告组才会存在)")
    private String productName;

    /**
     * 商品价格(单位：元，sdpa 类型广告组才会存在)
     */
    @ApiModelProperty(value="商品价格(单位：元，sdpa 类型广告组才会存在)")
    private BigDecimal productPrice;

    /**
     * 商品主图(sdpa 类型广告组才会存在)
     */
    @ApiModelProperty(value="商品主图(sdpa 类型广告组才会存在)")
    private String productImage;

    /**
     * 行为意向-系统优选
     */
    @ApiModelProperty(value="行为意向-系统优选")
    private Integer intentionTarget;

    /**
     * 程序化落地页信息
     */
    @ApiModelProperty(value="程序化落地页信息")
    private String pageGroupDetail;

    /**
     * 小铃铛组件id
     */
    @ApiModelProperty(value="小铃铛组件id")
    private Long jingleBellId;

    /**
     * 主播id
     */
    @ApiModelProperty(value="主播id")
    private Long liveUserId;

    /**
     * 广告计划类型(0:信息流，1:搜索)
     */
    @ApiModelProperty(value="广告计划类型(0:信息流，1:搜索)")
    private Integer adType;

    /**
     * 智能扩词开启状态(false：关闭，true：开启)
     */
    @ApiModelProperty(value="智能扩词开启状态(false：关闭，true：开启)")
    private Integer extendSearch;

    /**
     * 来源(1:API,2:渠道后台)
     */
    @ApiModelProperty(value="来源(1:API,2:渠道后台)")
    private Integer soucreStatus;

    /**
     * 平台状态(1:草稿,2:已推送)
     */
    @ApiModelProperty(value="平台状态(1:草稿,2:已推送)")
    private Integer dyStatus;

    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 是否删除(0否 1是)
     */
    @ApiModelProperty(value="是否删除(0否 1是)")
    private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

}
