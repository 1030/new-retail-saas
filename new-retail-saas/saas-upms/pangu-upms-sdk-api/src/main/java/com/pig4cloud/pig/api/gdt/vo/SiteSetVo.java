package com.pig4cloud.pig.api.gdt.vo;

import lombok.Getter;
import lombok.Setter;


/**
 * @广告版本vo
 * @author hma
 *
 */
@Getter
@Setter
public class SiteSetVo  {
	/**
	 * 广告版本
	 */
	private String siteSet;
	/**
	 * 广告版本中文名称
	 */
	private String siteSetValue;
}
