package com.pig4cloud.pig.api.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@Setter
public class AdAwemeBannedDto extends Page {
	private Long id;
	private String aweme_id;//抖音id
	private String aweme_name;//抖音昵称
	private String banned_type;//屏蔽类型 CUSTOM_TYPE：自定义规则，根据昵称关键词屏蔽；AWEME_TYPE：根据抖音id屏蔽
	private String nickname_keyword;//昵称关键词
	@NotEmpty(message = "advertiser_id为空")
	private String advertiser_id;//广告账户id
	private String advertiser_name;//广告主账户名称
	private List<String> keyword_list;//昵称关键词列表
	List<String> advertiserIds;
}
