package com.pig4cloud.pig.api.gdt.dto;

import com.pig4cloud.pig.api.gdt.entity.GdtCampaign;
import lombok.Getter;
import lombok.Setter;

/**
 * 广点通-推广计划表 参数 dto
 * 
 * @author hma
 * @date 2020-12-03
 */
@Setter
@Getter
public class GdtCreateCampaignDto extends GdtCampaign
{
	private static final long serialVersionUID = 1L;






}
