package com.pig4cloud.pig.api.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @Description
 * @Author chengang
 * @Date 2022/12/13
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Media {

    String value();

}