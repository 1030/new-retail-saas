package com.pig4cloud.pig.api.entity.bd;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 百度信息流计划表(bd_campaign)实体类
 *
 * @author zjz
 * @since 2022-12-27 18:35:43
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("bd_campaign")
public class BdCampaign extends Model<BdCampaign> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
	private Long id;
    /**
     * 信息流计划Id
     */
    private Long campaignFeedId;

    private String advertiserId;
    /**
     * 信息流计划名称
     */
    private String campaignFeedName;
    /**
     * 推广对象(1 - 网站链接 2 - 应用下载（IOS）3 - 应用下载（Android）7 - 电商店铺)
     */
    private Integer subject;
    /**
     * 推广app信息
     */
    private String appinfo;
    /**
     * 预算
     */
    private BigDecimal budget;
    /**
     * 推广开始时间
     */
    private String starttime;
    /**
     * 推广结束时间
     */
    private String endtime;
    /**
     * 推广计划暂停时段
     */
    private String schedule;
    /**
     * 计划启停
     */
    private Integer pause;
    /**
     * 推广计划状态 0 - 有效 1 - 处于暂停时段 2 - 暂停推广 3 - 推广计划预算不足 4 - 账户待激活 11 - 账户预算不足 20 - 账户余额为零 23 - 被禁推 24 - app已下线 25 - 应用审核中
     */
    private Integer status;
    /**
     * 物料类型 1 - 普通计划 3 - 商品计划 7 - 原生RTA
     */
    private Integer bstype;
    /**
     * 信息流计划类型 1： 普通模式 4：放量模式
     */
    private Integer campaignType;
    /**
     * 添加时间
     */
    private String addtime;
    /**
     * RTA状态 1 - 已开通 0 - 未开通
     */
    private Integer rtaStatus;
    /**
     * 来源(1:API,2:渠道后台)
     */
    private Integer soucreStatus;
    /**
     * 是否删除  0否 1是
     */
    private Integer deleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(update = "now()")
	private Date updateTime;
    /**
     * 创建人
     */
    private Long createId;
    /**
     * 修改人
     */
    private Long updateId;

}