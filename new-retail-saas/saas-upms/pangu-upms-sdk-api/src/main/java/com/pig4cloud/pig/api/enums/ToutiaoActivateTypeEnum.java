package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoActivateTypeEnum {

	//新用户    1一个月以内/ 2一到三个月/ 3三个月以上
	WITH_IN_A_MONTH          (  "WITH_IN_A_MONTH"           ,      "一个月以内"),
	ONE_MONTH_2_THREE_MONTH  (  "ONE_MONTH_2_THREE_MONTH"   ,      "一到三个月"),
	THREE_MONTH_EAILIER      (  "THREE_MONTH_EAILIER"       ,      "三个月以上");



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoActivateTypeEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoActivateTypeEnum item : ToutiaoActivateTypeEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoActivateTypeEnum item : ToutiaoActivateTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoActivateTypeEnum item : ToutiaoActivateTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
