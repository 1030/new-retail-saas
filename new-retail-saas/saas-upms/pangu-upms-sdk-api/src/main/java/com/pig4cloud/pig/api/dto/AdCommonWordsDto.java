package com.pig4cloud.pig.api.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
public class AdCommonWordsDto extends Page {
	private Long id;
	@Size(max=255,min=1)
	@NotEmpty(message = "text为空")
	private String text;
	@NotEmpty(message = "advertiser_id为空")
	private String advertiser_id;
	@NotEmpty(message = "advertiser_name为空")
	private String advertiser_name;
	List<String> advertiserIds;
}
