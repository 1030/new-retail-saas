/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @用户账号绑定表
 * @author john
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="tt_dailystat")
public class DailyStat extends Model<DailyStat> {

	private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "广告账户id")
	@TableField(value = "advertiser_id")
	private Long advertiserId;
	
	@TableField(exist = false)
	private String name;
	
	@ApiModelProperty(value = "日期")
	@TableField(value = "date")
	private String date;
	
	/**
	 * 日终结余
	 */
	@ApiModelProperty(value = "日终结余")
	private BigDecimal balance;
	
	
	/**
	 * 现金支出
	 */
	@ApiModelProperty(value = "现金支出")
	@TableField(value = "cash_cost")
	private BigDecimal cashCost;
	
	/**
	 * 总支出
	 */
	@ApiModelProperty(value = "总支出")
	private BigDecimal cost;
	
	/**
	 * 冻结
	 */
	@ApiModelProperty(value = "冻结")
	private BigDecimal frozen;
	
	
	/**
	 * 总存入
	 */
	@ApiModelProperty(value = "总存入")
	private BigDecimal income;
	
	
	/**
	 * 赠款支出
	 */
	@ApiModelProperty(value = "赠款支出")
	@TableField(value = "reward_cost")
	private BigDecimal rewardCost;
	
	/**
	 * 返货支出
	 */
	@ApiModelProperty(value = "返货支出")
	@TableField(value = "return_goods_cost")
	private BigDecimal returnGoodsCost;
	
	
	/**
	 * 总转入
	 */
	@ApiModelProperty(value = "总转入")
	@TableField(value = "transfer_in")
	private BigDecimal transferIn;
	
	
	/**
	 * 总转出
	 */
	@ApiModelProperty(value = "总转出")
	@TableField(value = "transfer_out")
	private BigDecimal transferOut;
}
