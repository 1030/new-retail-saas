package com.pig4cloud.pig.api.gdt.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 广点通日流水公共信息
 *
 * @author zxm
 */
@Getter
@Setter
public class GdtDailyReport implements Serializable {
	private static final long serialVersionUID = 1576408853774481676L;
	/**
	 * 广告账户ID
	 */
	private Integer account_id;
	/**
	 * 日期，日期格式：YYYY-MM-DD
	 */
	private String date;
	/**
	 * 点击
	 */
	private Integer valid_click_count;
	/**
	 * 曝光量(展示量)
	 */
	private Integer view_count;
	/**
	 * APP下载完成量
	 */
	private Integer download_count;

	/**
	 * APP激活总量
	 */
	private Integer activated_count;

	/**
	 * APP下载激活率
	 */
	private BigDecimal activated_rate;

	/**
	 * 点击均价
	 */
	private Integer cpc;
	/**
	 * 消耗
	 */
	private Integer cost;

	/**
	 * 注册量（APP）
	 */
	private Integer app_register_count;

	/**
	 * 注册成本（APP）
	 */
	private Integer app_register_cost;

	/**
	 * 注册率（APP）
	 */
	private BigDecimal app_register_rate;

	/**
	 * APP激活成本
	 */
	private Integer activated_cost;

	/**
	 * 点击率
	 */
	private BigDecimal ctr;

	/**
	 * APP点击激活率(激活量/点击量*100%)
	 */
	private BigDecimal click_activated_rate;

	/**
	 * 下单量（APP）
	 */
	private Integer app_order_count;
	/**
	 * 下单成本（APP）
	 */
	private Integer app_order_cost;
	/**
	 * 付费行为量（APP）
	 */
	private Integer app_checkout_count;
	/**
	 * 付费金额（APP）
	 */
	private Integer app_checkout_amount;
	/**
	 * 付费行为成本（APP）
	 */
	private Integer app_checkout_cost;

	 /**
	 * 目标转化量
	 */
	private Integer conversions_count;

	/**
	 * 转化率
	 */
	private BigDecimal conversions_rate;

	/**
	 * 转化成本
	 */
	private Integer conversions_cost;


	/**
	 * 版本号，查询最大的记录
	 */
	private long iver;


}