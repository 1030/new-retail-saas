/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.api.feign;

import com.pig4cloud.pig.api.entity.AdMaterial;
import com.pig4cloud.pig.api.feign.factory.RemoteAdMaterialServiceFallbackFactory;
import com.pig4cloud.pig.api.vo.AdMaterialVo;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Set;

/**
 * @author cx
 * @version 1.0.0
 * @Time 2021/10/14 15:30
 */
@FeignClient(contextId = "remoteAdMaterialService", value = ServiceNameConstants.UPMS_SDK_SERVICE, fallbackFactory = RemoteAdMaterialServiceFallbackFactory.class)
public interface RemoteAdMaterialService {

	/**
	 * 炎帝推送素材使用
	 *
	 * @param adMaterial 转化实体
	 * @param from       是否内部调用
	 * @return succes、false
	 */
	@PostMapping(value = "/material/addYanDiMaterial", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	R addYanDiMaterial(AdMaterialVo adMaterial, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 根据素材ID列表查询素材信息
	 *
	 * @param from
	 * @param materialIds
	 * @return
	 */
	@PostMapping(value = "/material/getMaterialListByIds")
	R<List<AdMaterial>> getMaterialListByIds(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody List<Long> materialIds);
	
	/**
	 * 根据用户，查询待推送消息
	 * @param from
	 * @param loginUserId
	 * @return
	 */
	@PostMapping(value = "/materialPushBox/getPreparePushMaterial")
	R<Set<Long>> getPreparePushMaterial(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody String loginUserId);

}
