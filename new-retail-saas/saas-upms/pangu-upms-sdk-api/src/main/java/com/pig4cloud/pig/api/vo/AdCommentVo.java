package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.util.Page;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AdCommentVo extends Page {

	@NotBlank(message = "评论ID不能为空", groups = {CommentReplay.class, CommentHide.class, CommentStickOnTop.class, CommentBlockUsers.class})
	private String id;

	private String userScreenName;

	private String awemeAccount;

	private Long adId;

	private String adName;

	@NotBlank(message = "评论内容不能为空", groups = {CommentReplay.class})
	private String text;

	private Integer likeCount;

	private String createTime;

	private Integer stick;

	private Integer replyCount;

	private String replyStatus;

	@NotNull(message = "创意ID不能为空", groups = {CommentPage.class})
	private Long creativeId;

	private Integer rank;

	private String advertiserId;

	private String advertiserName;

	private String adAccount;

	private Long replyCommentId;

	private Integer hide;

	public interface CommentPage {
	}

	public interface CommentReplay {
	}

	public interface CommentHide {
	}

	public interface CommentStickOnTop {
	}

	public interface CommentBlockUsers {
	}

}
