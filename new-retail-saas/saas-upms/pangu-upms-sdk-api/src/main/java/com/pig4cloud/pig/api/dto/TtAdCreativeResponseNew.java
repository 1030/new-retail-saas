package com.pig4cloud.pig.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pig4cloud.pig.api.vo.TtCreativeErrorResult;
import lombok.Data;

import java.util.List;

/**
 * @Description 新营销链路创建广告创意返回对象
 * @Author chengang
 * @Date 2021/9/14
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TtAdCreativeResponseNew {

	private String[] creative_ids;
	private List<TtCreativeErrorResult> errors;


}
