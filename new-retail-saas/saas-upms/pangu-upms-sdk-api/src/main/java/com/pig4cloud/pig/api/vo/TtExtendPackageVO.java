package com.pig4cloud.pig.api.vo;

import lombok.Data;

/**
 * @author ：lile
 * @date ：2021/8/5 17:07
 * @description：
 * @modified By：
 */
@Data
public class TtExtendPackageVO {
	private String channel_id;
	private String chlCode;
}
