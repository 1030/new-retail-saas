package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.api.gdt.vo.GdtAdvFundsVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@TableName(value="gdt_adv_funds")
public class GdtAdvFunds {
	
	@ApiModelProperty(value = "授权的推广帐号 id")
	@TableId(value = "account_id")
	private String accountId;

	@ApiModelProperty(value = "余额")
	@TableField(value = "funds")
	private String funds;

	@ApiModelProperty(value = "日预算")
	@TableField(value = "daily_budget")
	private BigDecimal dailyBudget;

	@ApiModelProperty(value = "创建时间")
	@TableField(value = "createtime")
	private Date createtime;

	@ApiModelProperty(value = "更新时间")
	@TableField(value = "updatetime")
	private Date updatetime;

	@ApiModelProperty(value = "预警余额")
	@TableField(value = "alert_fund")
	private BigDecimal alertFund;

	@ApiModelProperty(value = "预算预警值")
	@TableField(value = "order_daily_budget")
	private String orderDailyBudget;
	
	
	@ApiModelProperty(value = "广告组消耗预警值")
	@TableField(value = "group_cost_warning")
	private BigDecimal groupCostWarning;

	@TableField(exist = false)
	@ApiModelProperty(value = "总余额")
	private BigDecimal totalBalance;

	@TableField(exist = false)
	@ApiModelProperty(value = "各种资金集合")
	private List<GdtAdvFundsVO> fundList;

}
