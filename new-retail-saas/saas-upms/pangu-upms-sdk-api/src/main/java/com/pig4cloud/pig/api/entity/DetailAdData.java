package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

@Data
public class DetailAdData implements Serializable {

    private static final long serialVersionUID = 1928145920330911062L;
    //三级行业ID
    @JSONField(name = "third_industry_id")
    private Long thirdIndustryId;

    //创意标签
    @JSONField(name = "ad_keywords")
    private String adKeywords;

    //链接类型
    @JSONField(name = "params_type")
    private String paramsType;

    //落地页链接字段选择
    @JSONField(name = "dpa_external_url_field")
    private String dpaExternalUrlField;

    //落地页链接
    @JSONField(name = "external_url")
    private String externalUrl;

    //落地页检测参数
    @JSONField(name = "external_url_params")
    private String externalUrlParams;

    //广告来源
    @JSONField(name = "source")
    private String source;

    //是否开启来源智能生成
    @JSONField(name = "enable_smart_source")
    private String enableSmartSource;

    //绑定抖音号
    @JSONField(name = "ies_core_user_id")
    private String iesCoreUserId;

    //主页作品列表隐藏广告内容
    @JSONField(name = "is_feed_and_fav_see")
    private Long isFeedAndFavSee;

    //搭配试玩素材URL
    @JSONField(name = "playable_url")
    private String playableUrl;

    //是否开启自动派生创意
    @JSONField(name = "creative_auto_generate_switch")
    private Integer creativeAutoGenerateSwitch;

    //应用名
    @JSONField(name = "app_name")
    private String appName;

    //Android应用下载详情页
    @JSONField(name = "web_url")
    private String webUrl;

    //自动生成视频素材
    @JSONField(name = "is_presented_video")
    private Integer isPresentedVideo;

    //是否关闭评论
    @JSONField(name = "is_comment_disable")
    private Integer isCommentDisable;

    //是否允许客户端下载视频
    @JSONField(name = "ad_download_status")
    private Integer adDownloadStatus;

    //是否优先调起试玩
    @JSONField(name = "priority_trial")
    private Integer priorityTrial;

    //云游戏素材
    @JSONField(name = "supplements")
    private String supplements;

    //启用动态创意类型
    @JSONField(name = "dynamic_creative_switch")
    private String dynamicCreativeSwitch;

    //直达链接
    @JSONField(name = "open_url")
    private String openUrl;

    //字节小程序信息
    @JSONField(name = "mini_program_info")
    private String miniProgramInfo;



}
