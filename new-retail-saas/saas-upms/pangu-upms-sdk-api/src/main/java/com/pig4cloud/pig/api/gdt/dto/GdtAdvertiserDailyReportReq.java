package com.pig4cloud.pig.api.gdt.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @description:广告组设置请求参数
 * @author:yk
 * @createTime:2020/12/14
 */
@Setter
@Getter
public class GdtAdvertiserDailyReportReq extends Page {
	/** 开始时间*/
	private String sdate;
	/** 结束时间*/
	private String edate;

	private List<String> accountIds;

	private String accountId;

}
