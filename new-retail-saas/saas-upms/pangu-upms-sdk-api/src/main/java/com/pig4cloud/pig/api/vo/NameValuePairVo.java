package com.pig4cloud.pig.api.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NameValuePairVo {
	private String name;
	private Object value;

	public NameValuePairVo(String name, Object value) {
		this.name = name;
		this.value = value;
	}
}
