package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "self_custom_audiences")
public class SelfCustomAudience extends Model<SelfCustomAudience> {

	private static final long serialVersionUID = -4846463548424420828L;

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键id")
	private Long id;

	@TableField(value = "name")
	@ApiModelProperty(value = "人群包名称")
	private String name;

	@TableField(value = "type")
	@ApiModelProperty(value = "打包字段  0：IMEI 1:IDFA 2:UID 4:IMEI_MD5 5:IDFA_MD5 6:MOBILE_HASH_SHA256  7:OAID 8:OAID_MD5 9:MOBILE_MD5 10:MAC地址")
	private Integer type;


	@TableField(value = "data_count")
	@ApiModelProperty(value = "文件行数")
	private Integer dataCount;


	@TableField(value = "download_url")
	@ApiModelProperty(value = "下载URL")
	private String downloadUrl;

	@TableField(value = "is_deleted")
	@ApiModelProperty(value = "是否删除  0否 1是")
	private Integer isDeleted;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(value = "update_time")
	private Date updateTime;

	@TableField(value = "create_id")
	@ApiModelProperty(value = "创建人")
	private Long createId;

	@TableField(value = "update_id")
	@ApiModelProperty(value = "修改人")
	private Long updateId;

	@TableField(exist = false)
	private String createName;


}
