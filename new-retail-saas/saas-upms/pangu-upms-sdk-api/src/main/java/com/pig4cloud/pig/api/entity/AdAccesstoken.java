package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


/**
 * 媒体授权TOKEN表
 * @author  chengang
 * @version  2022-12-19 15:51:30
 * table: ad_accesstoken
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_accesstoken")
public class AdAccesstoken extends Model<AdAccesstoken>{

		//主键id
		@TableId(value = "id",type = IdType.AUTO)
		private Long id;


		//渠道code 1头条 8广点通 9百度 10快手
		@TableField(value = "media_code")
		private String mediaCode;


		//开发者APPID
		@TableField(value = "app_id")
		private String appId;


		//授权回调CODE
		@TableField(value = "auth_code")
		private String authCode;


		//授权令牌
		@TableField(value = "access_token")
		private String accessToken;


		//刷新令牌
		@TableField(value = "refresh_token")
		private String refreshToken;


		//授权令牌剩余有效时间，单位：秒
		@TableField(value = "expires_in")
		private Long expiresIn;


		//更新令牌剩余有效时间，单位：秒
		@TableField(value = "refreshExpires_in")
		private Long refreshexpiresIn;


		//授权令牌到期时间
		@TableField(value = "expires_time")
		private String expiresTime;


		//授权令牌到期时间
		@TableField(value = "refreshExpires_time")
		private String refreshexpiresTime;


		//授权账号
		@TableField(value = "account_id")
		private String accountId;


		//账号角色
		@TableField(value = "account_type")
		private String accountType;


		//获取授权用户信息标识
		@TableField(value = "open_id")
		private String openId;


		//已授权账户所有的 account_id
		@TableField(value = "advertiser_ids")
		private String advertiserIds;


		//获取token的最新时间
		@TableField(value = "token_time")
		private Date tokenTime;


		//是否删除  0否 1是
		@TableField(value = "deleted")
		private Integer deleted;


		//创建时间
		@TableField(value = "create_time")
		private Date createTime;


		//修改时间
		@TableField(value = "update_time")
		private Date updateTime;


		//创建人
		@TableField(value = "create_id")
		private Long createId;


		//修改人
		@TableField(value = "update_id")
		private Long updateId;

	
}

	

	
	

