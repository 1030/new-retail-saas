package com.pig4cloud.pig.api.gdt.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

/**
 * @projectName:pig
 * @description:广点通人群信息
 * @author:Zhihao
 * @createTime:2020/12/5 17:56
 * @version:1.0
 */
@Data
public class GdtCustomAudiencesDto implements Serializable {
	private static final long serialVersionUID = 1L; 
	
	
	/**
	 * 人群归属的推广帐号 id
	 */
	@NotBlank(message = "推广帐号 id不能为空")
	private String accountId;
	    
	/**
	 * 人群名称
	 */
	@NotBlank(message = "人群名称不能为空")
	private String name;
	
	/**
	 * 广告主对人群在自己系统里的编码
	 */
	private String externalAudienceId;
	
	/**
	 * 人群描述
	 */
	private String description;
	
	/**
	 * 人群类型
	 * 号码文件人群=CUSTOMER_FILE，
	 * 拓展人群=LOOKALIKE，
	 * 用户行为人群=USER_ACTION，
	 * 地理位置人群=LBS，
	 * 关键词人群=KEYWORD，
	 * 广告人群=AD，
	 * 组合人群=COMBINE，
	 * 标签人群=LABEL
	 */
	@NotBlank(message = "人群类型不能为空")
	private String type;

	/**
	 * 号码包用户 id 类型，[枚举详情]
	 * 枚举列表：{ GDT_OPENID, HASH_IDFA, HASH_IMEI, HASH_MAC, HASH_MOBILE_PHONE, HASH_QQ, IDFA, IMEI, MAC, MOBILE_QQ_OPENID, QQ, WX_OPENID, WECHAT_OPENID, SALTED_HASH_IMEI, SALTED_HASH_IDFA, OAID, HASH_OAID }
	 */
	@NotBlank(message = "号码包类型不能为空")
	private String userIdType;
	
	 

}