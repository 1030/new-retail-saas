package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;


import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * project_settings
 * @author nml
 * @date 2020-11-04 11:06:33
 */

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_project_settings")
public class ProjectSettings extends Model<ProjectSettings> {

	private static final long serialVersionUID = 1L;

	/*
	 *  唯一id
	 * */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value = "id")
	private Integer id;

    /**
     * 项目名称
     */
	@NotBlank(message = "项目名称不能为空")
	@ApiModelProperty(value = "项目名称")
	@TableField(value = "project_name")
    private String projectName;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "创建时间")
	@TableField(value = "create_time",fill= FieldFill.INSERT)
	private Date createTime;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "修改时间")
	@TableField(value = "update_time",fill = FieldFill.UPDATE)
	private Date updateTime;
}