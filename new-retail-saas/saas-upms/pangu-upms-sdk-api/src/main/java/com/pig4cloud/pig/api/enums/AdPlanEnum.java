package com.pig4cloud.pig.api.enums;

/**
 * @projectName:pig
 * @description:广点通推广计划枚举类型
 * @author:nml
 * @createTime:2020/12/9 15:06
 */
public enum AdPlanEnum {
	/**
	 * 广告计划客户设置类型
	 */
	GDT_AD_STATUS_NORMAL("AD_STATUS_NORMAL", "广点通有效"),
	GDT_AD_STATUS_SUSPEND("AD_STATUS_SUSPEND", "广点通暂停"),
	TT_AD_STATUS_ENABLE("enable", "头条有效"),
	TT_AD_STATUS_DISABLE("disable", "头条暂停"),
	TT_AD_STATUS_ENABLE_NEW("ENABLE", "头条有效新版"),
	TT_AD_STATUS_DISABLE_NEW("DISABLE", "头条暂停新版"),
	KS_AD_STATUS_ENABLE("1", "快手有效"),
	KS_AD_STATUS_DISABLE("2", "快手暂停");

	/**
	 * 类型
	 */
	private String type;
	/**
	 * 名称
	 */
	private String name;

	AdPlanEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
