package com.pig4cloud.pig.api.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import lombok.Data;

/**
 * 素材推送箱dto
 * @author chengj
 *
 */
@Data
public class AdMaterialPushBoxDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6991728415198653596L;
	
	/**
	 * 素材id
	 */
	private Long materialId;
	
	/**
     * 平台ID（1：头条，2：广点通）
     */
	private Integer platformId;
	
	/**
     * 平台广告账户
     */
	private List<Long> advertiserIds;
	
	/**
	 * 批量推送 素材id集合
	 */
	private List<Long> materialIdList;

}
