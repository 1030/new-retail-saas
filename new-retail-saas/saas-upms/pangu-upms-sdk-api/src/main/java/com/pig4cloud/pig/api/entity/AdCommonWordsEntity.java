package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName(value = "ad_common_words")
public class AdCommonWordsEntity {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 常用语
	 */
	private String text;

	/**
	 * 广告账户ID
	 */
	@TableField(value = "advertiser_id")
	private String advertiserId;

	/**
	 * 广告账户名称
	 */
	@TableField(value = "advertiser_name")
	private String advertiserName;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;

	/**
	 * 是否删除，0：否；1：是
	 */
	private Integer isDeleted;

	public AdCommonWordsEntity setId(Long id) {
		this.id = id;
		return this;
	}

	public AdCommonWordsEntity setText(String text) {
		this.text = text;
		return this;
	}

	public AdCommonWordsEntity setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
		return this;
	}

	public AdCommonWordsEntity setAdvertiserName(String advertiserName) {
		this.advertiserName = advertiserName;
		return this;
	}

	public AdCommonWordsEntity setCreateDate(Date createDate) {
		this.createDate = createDate;
		return this;
	}

	public AdCommonWordsEntity setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
		return this;
	}

	public AdCommonWordsEntity setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
		return this;
	}
}
