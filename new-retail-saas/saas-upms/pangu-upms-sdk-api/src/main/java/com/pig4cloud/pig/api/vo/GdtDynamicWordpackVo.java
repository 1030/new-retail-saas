package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 广点通动态词包
 * gdt_dynamic_wordpack
 * @author nml
 * @date 2020-12-03 15:21:39
 */
@Getter
@Setter
public class GdtDynamicWordpackVo extends Page {
    /**
     */
    private Long id;

    /**
     * 词包名称
     */
    private String name;

    /**
     * 通配符
     */
    private String wildcard;

    /**
     * 词包长度
     */
    private String length;

    /**
     * 词包类型：根据广告版位和创意形式ID可用性定义;1:系统动态词包
     */
    private String type;

    /**
     * 广告版位
     */
    private String siteset;

    /**
     * 创意形式ID
     */
    private String creativeFormId;
/*
    *//**
     * 备注
     *//*
    private String remark;

    *//**
     * 是否删除：0正常，1删除
     *//*
    private Integer isdelete;

    *//**
     * 创建人
     *//*
    private String createuser;

    *//**
     * 修改人
     *//*
    private String updateuser;

    *//**
     * 创建时间
     *//*
    private Date createtime;

    *//**
     * 修改时间
     *//*
    private Date updatetime;*/
}