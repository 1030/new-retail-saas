package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class SelfCustomAudienceDTO implements Serializable {

	private static final long serialVersionUID = -4709678072731116081L;

	private String name;

	private String startDate;

	private String endDate;
}
