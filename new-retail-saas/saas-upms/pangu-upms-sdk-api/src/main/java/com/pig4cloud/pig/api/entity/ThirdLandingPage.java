package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @第三方落地页
 * @author yk
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="third_landing_page")
public class ThirdLandingPage extends Model<ThirdLandingPage> {

	@ApiModelProperty(value = "自增ID")
	@TableId(value = "id",type= IdType.AUTO)
	private Integer id;

	/**
	 * 落地页缩略图
	 */
	@ApiModelProperty(value = "落地页缩略图地址：落地页缩略图")
	@TableField(value = "page_url")
	private String pageUrl;

	/**
	 * 落地页标题
	 */
	@ApiModelProperty(value = "落地页标题")
	@TableField(value = "page_name")
	private String pageName;

	/**
	 * 落地页ID
	 */
	@ApiModelProperty(value = "落地页ID")
	@TableField(value = "page_id")
	private String pageId;

	/**
	 * 状态
	 */
	@ApiModelProperty(value = "状态")
	@TableField(value = "audit_status")
	private String auditStatus;

	/**
	 * 创建人
	 */
	@ApiModelProperty(value = "创建人")
	@TableField(value = "create_user")
	private String createUser;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField(value = "create_time",fill= FieldFill.INSERT)
	private Date createTime;

	@ApiModelProperty(value = "落地页链接")
	@TableField(value = "url")
	private String url;

	@ApiModelProperty(value = "广告主id")
	@TableField(value = "advertiser_id")
	private String advertiserId;
}
