/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;


/**
 * @日流水汇总
 * @author hma
 *
 */
@Data
public class DailyStatCountVo  {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 日终结余
	 */
	@ApiModelProperty(value = "日终结汇总")
	private BigDecimal balance;
	
	
	/**
	 * 现金支出
	 */
	@ApiModelProperty(value = "现金支出汇总")
	private BigDecimal cashCost;
	
	/**
	 * 总支出
	 */
	@ApiModelProperty(value = "总支出汇总")
	private BigDecimal cost;
	
	/**
	 * 冻结
	 */
	@ApiModelProperty(value = "冻结汇总")
	private BigDecimal frozen;
	
	
	/**
	 * 总存入
	 */
	@ApiModelProperty(value = "总存入汇总")
	private BigDecimal income;
	
	
	/**
	 * 赠款支出
	 */
	@ApiModelProperty(value = "赠款支出汇总")
	private BigDecimal rewardCost;
	
	/**
	 * 返货支出
	 */
	@ApiModelProperty(value = "返货支出汇总")
	private BigDecimal returnGoodsCost;
	
	
	/**
	 * 总转入
	 */
	@ApiModelProperty(value = "总转入汇总")
	private BigDecimal transferIn;
	
	
	/**
	 * 总转出
	 */
	@ApiModelProperty(value = "总转出汇总")
	private BigDecimal transferOut;
}
