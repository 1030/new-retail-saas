package com.pig4cloud.pig.api.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @description: 查询转化ID
 * @author yuwenfeng
 * @date 2021/12/14 15:00
 */
@Data
@Accessors(chain = true)
public class SelectConvertTrackReq implements Serializable {

	private static final long serialVersionUID = 522727844502361899L;

	@ApiModelProperty(value = "广告监测主键ID")
	private Long monitorId;

	@ApiModelProperty(value = "转化名称")
	private String name;

	@ApiModelProperty(value = "ID集合")
	private List<Long> adIds;
}
