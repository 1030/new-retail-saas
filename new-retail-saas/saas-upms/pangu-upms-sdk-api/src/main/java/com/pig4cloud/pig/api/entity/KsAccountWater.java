package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 快手账号流水明细
 * @author  chenxiang
 * @version  2022-03-24 13:56:21
 * table: ks_account_water
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ks_account_water")
public class KsAccountWater extends Model<KsAccountWater>{
	/**
	 * 主键id
	 */
	@TableField(value = "id")
	private Long id;
	/**
	 * 广告账户
	 */
	@TableField(value = "advertiser_id")
	private Long advertiserId;
	/**
	 * 日期
	 */
	@TableField(value = "date")
	private String date;
	/**
	 * 总花费
	 */
	@TableField(value = "daily_charge")
	private BigDecimal dailyCharge;
	/**
	 * 充值花费
	 */
	@TableField(value = "real_charged")
	private BigDecimal realCharged;
	/**
	 * 框返花费
	 */
	@TableField(value = "contract_rebate_real_charged")
	private BigDecimal contractRebateRealCharged;
	/**
	 * 激励花费
	 */
	@TableField(value = "direct_rebate_real_charged")
	private BigDecimal directRebateRealCharged;
	/**
	 * 转入
	 */
	@TableField(value = "daily_transfer_in")
	private BigDecimal dailyTransferIn;
	/**
	 * 转出
	 */
	@TableField(value = "daily_transfer_out")
	private BigDecimal dailyTransferOut;
	/**
	 * 日终结余
	 */
	@TableField(value = "balance")
	private BigDecimal balance;
	/**
	 * 充值转入
	 */
	@TableField(value = "real_recharged")
	private BigDecimal realRecharged;
	/**
	 * 框返转入
	 */
	@TableField(value = "contract_rebate_real_recharged")
	private BigDecimal contractRebateRealRecharged;
	/**
	 * 激励转入
	 */
	@TableField(value = "direct_rebate_real_recharged")
	private BigDecimal directRebateRealRecharged;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

