package com.pig4cloud.pig.api.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.Data;

@Data
public class AdAdverAuthDTO {
	
	private Integer authStatus;//0-批准，1-拒绝
	private Integer status;//0-未审核，1-已审核
	
	private Page page;
}
