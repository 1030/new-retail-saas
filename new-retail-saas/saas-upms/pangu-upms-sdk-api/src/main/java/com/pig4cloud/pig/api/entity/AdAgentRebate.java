/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 代理商返点表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
@Data
@TableName("ad_agent_rebate")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "代理商返点表")
public class AdAgentRebate extends Model<AdAgentRebate> {
private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id",type= IdType.AUTO)
    @ApiModelProperty(value="主键")
    private Integer id;
    /**
     * 代理商ID
     */
    @ApiModelProperty(value="代理商ID")
    private Integer agentId;
    /**
     * 代理商名称
     */
    @ApiModelProperty(value="代理商名称")
    private String agentName;
    /**
     * 平台ID
     */
    @ApiModelProperty(value="平台ID")
    private Integer platformId;
    /**
     * 渠道编码
     */
    @ApiModelProperty(value="渠道编码")
    private String channelCode;
    /**
     * 渠道名称
     */
    @ApiModelProperty(value="渠道名称")
    private String channelName;
    /**
     * 返点
     */
    @ApiModelProperty(value="返点")
    private BigDecimal rebate;
    /**
     * 生效时间
     */
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
    @ApiModelProperty(value="生效时间")
    private Date effectiveTime;
    /**
     * 删除状态：0正常，1删除
     */
    @ApiModelProperty(value="删除状态：0正常，1删除")
    private Integer isDelete;
    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private String createUser;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    /**
     * 修改人
     */
    @ApiModelProperty(value="修改人")
    private String updateUser;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private Date updateTime;
    }
