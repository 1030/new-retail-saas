package com.pig4cloud.pig.api.dto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 广告表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:21:02
 * table: ad_promotion
 */
@Data
public class AdPromotionRes implements Serializable {

	@ApiModelProperty(value = "主键id")
	private Long id;

	@ApiModelProperty(value = "广告ID")
	private Long promotionId;

	@ApiModelProperty(value = "广告名称")
	private String promotionName;

	@ApiModelProperty(value = "项目ID")
	private Long projectId;

	@ApiModelProperty(value = "广告账户ID")
	private Long advertiserId;

	@ApiModelProperty(value = "广告创建时间，格式yyyy-mm-dd，表示过滤出当天创建的广告项目")
	private String promotionCreateTime;

	@ApiModelProperty(value = "广告更新时间，格式yyyy-mm-dd，表示过滤出当天更新的广告项目")
	private String promotionModifyTime;

	@ApiModelProperty(value = "广告状态")
	private String status;

	@ApiModelProperty(value = "操作状态")
	private String optStatus;

	@ApiModelProperty(value = "广告素材组合")
	private String promotionMaterials;

	@ApiModelProperty(value = "预算")
	private BigDecimal budget;

	@ApiModelProperty(value = "目标转化出价/预期成本")
	private BigDecimal cpaBid;

	@ApiModelProperty(value = "深度优化出价")
	private BigDecimal deepCpabid;

	@ApiModelProperty(value = "深度转化ROI系数")
	private BigDecimal roiGoal;

	@ApiModelProperty(value = "素材评级信息")
	private String materialScoreInfo;

	@ApiModelProperty(value = "来源(1:API,2:渠道后台)")
	private Integer soucreStatus;

	@ApiModelProperty(value = "是否删除  0否 1是")
	private Integer deleted;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

	@ApiModelProperty(value = "创建人")
	private Long createId;

	@ApiModelProperty(value = "修改人")
	private Long updateId;
}


