package com.pig4cloud.pig.api.gdt.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class GdtAuthCodeReqVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2174489075624045949L;

	private String client_id;
	
	private String client_secret;
	
	private Integer userId;
	
	private String state;
	
	private String authorization_code;
	
	private String redirect_uri;

	private Integer agentId;

}
