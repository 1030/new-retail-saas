package com.pig4cloud.pig.api.enums;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum ToutiaoAgeEnum {

	//广告投放范围  DEFAULT 默认/UNION 只投放到资讯联盟（穿山甲）/UNIVERSAL 通投智选

	AGE_BETWEEN_18_23    ( "AGE_BETWEEN_18_23"       ,      "18-23"),
	AGE_BETWEEN_24_30    ( "AGE_BETWEEN_24_30"       ,      "24-30"),
	AGE_BETWEEN_31_40    ( "AGE_BETWEEN_31_40"       ,      "31-40"),
	AGE_BETWEEN_41_49    ( "AGE_BETWEEN_41_49"       ,      "41-49"),
	AGE_ABOVE_50         ( "AGE_ABOVE_50"            ,      ">=50" );



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoAgeEnum(String type,String name){
		this.type = type;
		this.name = name;
	}


	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoAgeEnum item : ToutiaoAgeEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoAgeEnum item : ToutiaoAgeEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}


	public static boolean containsType(Short type){
		if (type ==null){
			return false;
		}

		for (ToutiaoAgeEnum item : ToutiaoAgeEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
