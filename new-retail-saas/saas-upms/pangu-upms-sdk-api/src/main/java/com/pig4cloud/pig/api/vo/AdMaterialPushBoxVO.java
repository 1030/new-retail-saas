/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.vo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 素材推送箱表VO
 * @author chengj
 */
@Data
public class AdMaterialPushBoxVO implements Serializable {
	
	private static final long serialVersionUID = 6016302916096961401L;
	/**
     * 主键
     */
    private Integer id;
    /**
     * 素材ID
     */
    private Long materialId;
    
    /**
     * 素材名称
     */
    private String name;
    
    /**
     * 素材类型（1：视频，2：图片）
     */
    private Integer type;
    
    /**
     * 平台ID（1：头条，2：广点通）
     */
    private Integer platformId;
    
    /**
     * 平台名称
     */
    private String platformName;
    /**
     * 平台广告账户
     */
    private String advertiserId;
    
    /**
     * 平台推广账户名称
     */
    private String advertiserName;
   
    /**
     * 推送状态
     */
    private Integer pushStatus;
    
    /**
     * 失败原因
     */
    private String failMsg;
    
    /**
     * 是否删除
     */
    private Integer isDeleted;
    
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 创建时间
     */
    private Date createTime;
    
    /**
     * 修改
     */
    private Date updateTime;
    /**
     * 修改人
     */
    private String updateUser;
    
    }
