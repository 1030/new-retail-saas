package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 运营素材表
 *
 * @TableName ad_operate_material
 */
@Data
@EqualsAndHashCode
@TableName(value = "ad_operate_material")
public class AdOperateMaterialDO implements Serializable {

	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/**
	 * 运营素材ID
	 */
	@TableId(type = IdType.AUTO)
	private Long materialId;

	/**
	 * 素材类型；0：icon；1:五图；2：loading图
	 */
	private Integer materialType;

	/**
	 * 父游戏主键
	 */
	private Long pgid;

	/**
	 * 创意用户主键
	 */
	private Integer createUserId;

	/**
	 * 制作用户主键
	 */
	private Integer makeUserId;

	/**
	 * 是否删除：0正常，1删除
	 */
	private Integer isdelete;

	/**
	 * 修改者用户ID
	 */
	private Integer updateuser;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm.ss")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updatetime;

	/**
	 * 运营素材文件列表
	 */
	@TableField(exist = false)
	private List<AdOperateFileDO> fileList;

	/**
	 * 父游戏主键集合
	 */
	@TableField(exist = false)
	private Collection<Long> pgids;

	/**
	 * 子游戏主键
	 */
	@TableField(exist = false)
	private Long gameid;

	/**
	 * 父游戏名称
	 */
	@TableField(exist = false)
	private String pgname;

	/**
	 * 运营素材拥有的子游戏
	 */
	@TableField(exist = false)
	private List<AdOperateGameDO> gameList;

	/**
	 * 子游戏主键集合
	 */
	@TableField(exist = false)
	private Collection<Long> gameids;

	/**
	 * 子游戏名称
	 */
	@TableField(exist = false)
	private String gname;

	/**
	 * 创意人姓名
	 */
	@TableField(exist = false)
	private String createUserName;

	/**
	 * 制作人姓名
	 */
	@TableField(exist = false)
	private String makeUserName;

	/**
	 * 更新人姓名
	 */
	@TableField(exist = false)
	private String updateuserName;

	public AdOperateMaterialDO setMaterialId(Long materialId) {
		this.materialId = materialId;
		return this;
	}

	public AdOperateMaterialDO setMaterialType(Integer materialType) {
		this.materialType = materialType;
		return this;
	}

	public AdOperateMaterialDO setPgid(Long pgid) {
		this.pgid = pgid;
		return this;
	}

	public AdOperateMaterialDO setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
		return this;
	}

	public AdOperateMaterialDO setMakeUserId(Integer makeUserId) {
		this.makeUserId = makeUserId;
		return this;
	}

	public AdOperateMaterialDO setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
		return this;
	}

	public AdOperateMaterialDO setUpdateuser(Integer updateuser) {
		this.updateuser = updateuser;
		return this;
	}

	public AdOperateMaterialDO setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
		return this;
	}

	public AdOperateMaterialDO setFileList(List<AdOperateFileDO> fileList) {
		this.fileList = fileList;
		return this;
	}

	public AdOperateMaterialDO setPgids(Collection<Long> pgids) {
		this.pgids = pgids;
		return this;
	}

	public AdOperateMaterialDO setPgname(String pgname) {
		this.pgname = pgname;
		return this;
	}

	public AdOperateMaterialDO setGameList(List<AdOperateGameDO> gameList) {
		this.gameList = gameList;
		return this;
	}

	public AdOperateMaterialDO setGameid(Long gameid) {
		this.gameid = gameid;
		return this;
	}

	public AdOperateMaterialDO setGameids(Collection<Long> gameids) {
		this.gameids = gameids;
		return this;
	}

	public AdOperateMaterialDO setGname(String gname) {
		this.gname = gname;
		return this;
	}

	public AdOperateMaterialDO setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
		return this;
	}

	public AdOperateMaterialDO setMakeUserName(String makeUserName) {
		this.makeUserName = makeUserName;
		return this;
	}

	public AdOperateMaterialDO setUpdateuserName(String updateuserName) {
		this.updateuserName = updateuserName;
		return this;
	}

}