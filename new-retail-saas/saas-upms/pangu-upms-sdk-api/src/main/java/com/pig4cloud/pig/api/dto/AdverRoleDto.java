package com.pig4cloud.pig.api.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class AdverRoleDto implements Serializable{
	private String advertiser_id;//广告主id	
	private String advertiser_name;//广告主名称
	private Integer advertiser_role;//广告主角色，1-普通广告主，2-账号管家，3-一级代理商，4-二级代理商	
	private String is_valid;
	private String account_role;//新版授权账号角色  ADVERTISER广告主, CUSTOMER_ADMIN管家-管理员,	 AGENT代理商, CHILD_AGENT二级代理商, CUSTOMER_OPERATOR管家-操作者
}
