package com.pig4cloud.pig.api.util;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @description: 读取配置文件 pig-upms-biz-ad-dev.yml 中的信息
 * @author: nml
 * @time: 2020/11/2 19:44
 **/
@Component
@ConfigurationProperties(prefix="upload")
@Getter
@Setter
public class BizAdParamInit {

	/* 图片上传地址 D:/svn_checkout/bigdata/code/pig-ui/public/upload  */
	private String uploadUrl;

	/* 图片下载地址 http://localhost:8080/upload/ */
	private String downloadUrl;

	private String imgSize;

}
