package com.pig4cloud.pig.api.vo;

import lombok.Data;

/**
 * @author ：lile
 * @date ：2021/7/13 16:37
 * @description：
 * @modified By：
 */
@Data
public class AdAgentTransferVo {

	private Integer id;

	private String agentName;

	private String platformName;

	private Long adagentId;

	private Integer agentId;

	private Integer platform;

	private String chncode;

	private String chnname;
}
