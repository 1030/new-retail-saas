/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 巨量行业列表
 *
 * @author yuwenfeng
 * @date 2022-02-08 15:28:03
 */
@Data
@TableName("ad_industry")
@ApiModel(value = "巨量行业列表")
public class AdIndustry implements Serializable {

    /**
     * 行业ID
     */
	@TableId(value = "industry_id",type = IdType.INPUT)
    @ApiModelProperty(value="行业ID")
    private Long industryId;

    /**
     * 行业名称
     */
    @ApiModelProperty(value="行业名称")
    private String industryName;

    /**
     * 级别(1:一级行业 2:二级行业 3:三级行业)
     */
    @ApiModelProperty(value="级别(1:一级行业 2:二级行业 3:三级行业)")
    private Integer level;

    /**
     * 一级行业ID
     */
    @ApiModelProperty(value="一级行业ID")
    private Long firstIndustryId;

    /**
     * 一级行业名称
     */
    @ApiModelProperty(value="一级行业名称")
    private String firstIndustryName;

    /**
     * 二级行业ID
     */
    @ApiModelProperty(value="二级行业ID")
    private Long secondIndustryId;

    /**
     * 二级行业名称
     */
    @ApiModelProperty(value="二级行业名称")
    private String secondIndustryName;

    /**
     * 三级行业ID
     */
    @ApiModelProperty(value="三级行业ID")
    private Long thirdIndustryId;

    /**
     * 三级行业名称
     */
    @ApiModelProperty(value="三级行业名称")
    private String thirdIndustryName;

	/**
	 * 是否删除(0否 1是)
	 */
	@ApiModelProperty(value = "是否删除(0否 1是)")
	@TableLogic
	private Integer deleted;

	/**
	 * 创建者
	 */
	@TableField(fill = FieldFill.INSERT)
	private Long createId;

	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 更新者
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updateId;

	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}
