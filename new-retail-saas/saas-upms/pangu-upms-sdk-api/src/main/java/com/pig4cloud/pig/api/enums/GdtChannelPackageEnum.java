/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;


@Getter
@RequiredArgsConstructor
public enum GdtChannelPackageEnum {
	CHANNEL_PACKAGE_STATUS_PASSED("CHANNEL_PACKAGE_STATUS_PASSED",  "审核通过"),
	CHANNEL_PACKAGE_STATUS_REVIEWING("CHANNEL_PACKAGE_STATUS_REVIEWING",  "审核中"),
	CHANNEL_PACKAGE_STATUS_DENIED("CHANNEL_PACKAGE_STATUS_DENIED",  "审核拒绝"),
	CHANNEL_PACKAGE_STATUS_DENIED_AGAIN("CHANNEL_PACKAGE_STATUS_DENIED_AGAIN",  "审核拒绝，使用更新前的数据"),
	CHANNEL_PACKAGE_STATUS_REVIEWING_AGAIN("CHANNEL_PACKAGE_STATUS_REVIEWING_AGAIN",  "再次审核中"),
	CHANNEL_PACKAGE_STATUS_ON_OFFLINE("CHANNEL_PACKAGE_STATUS_ON_OFFLINE",  "下线中"),
	CHANNEL_PACKAGE_STATUS_OFFLINE("CHANNEL_PACKAGE_STATUS_OFFLINE",  "下线"),
	CHANNEL_PACKAGE_STATUS_DRAFT("CHANNEL_PACKAGE_STATUS_DRAFT",  "草稿");

	private final String value;
	/**
	 * 描述
	 */
	private final String name;

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String getName(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (GdtChannelPackageEnum item : GdtChannelPackageEnum.values()) {
			if (item.getValue().equals(value)) {
				return item.getName();
			}
		}
		return null;

	}
}
