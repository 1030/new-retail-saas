package com.pig4cloud.pig.api.gdt.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;


/**
 * 广点通广告组级别报表日流水
 *
 * @author yk
 */
@Getter
@Setter
public class GdtAdgroupDayReport implements Serializable {

	/**
	 * 客户设置的状态:AD_STATUS_NORMAL有效，AD_STATUS_SUSPEND暂停
	 */
	private String configuredStatus;

	/**
	 * 广告组主键id
	 */
	private Long adgroupId;

	/**
	 * 广告组名称
	 */
	private String adgroupName;

	/**
	 * 广告组在系统中的状态
	 */
	private String systemStatus;
	/**
	 * 状态
	 */
	private String status;

	/**
	 * 推广计划id
	 */
	private String campaignId;
	/**
	 * 推广目标
	 */
	private String promotedObjectType;

	/**
	 * 广告出价，单位为分，出价限制：
	 * CPC 出价限制：介于 10 分-10,000 分之间（0.1 元-100 元，单位为人民币）
	 * CPM 出价限制：介于 150 分-99,900 之间（1.5 元-999 元，单位为人民币）
	 * CPA 出价限制：介于 100 分-50,000 分之间（1 元-500 元，单位为人民币）
	 * oCPC/oCPM 出价限制：介于 10 分-500,000 分之间（0.1 元-5000 元，单位为人民币）
	 */
	private BigDecimal bidAmount;

	/**
	 * 广告组日预算，单位为分，设置为 0 表示不设预算（即不限）
	 */
	private BigDecimal dailyBudget;

	/**
	 * 出价方式
	 */
	private String bidMode;

	/*曝光量*/
	private String viewCount;

	/*点击量*/
	private String validClickCount;

	/*点击率*/
	private String ctr;

	/*点击均价*/
	private String cpc;

	/*消耗*/
	private String cost;

	/**
	 * 广告账号
	 */
	private String accountId;
	
	/**
	 *   忽略关闭预警
	 */
	private String costWarning;
	

	public String getCost() {
		int a = Integer.parseInt(this.cost);
		DecimalFormat df=new DecimalFormat("0.00");
		this.cost = df.format((float)a/(float)100);
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	//计算点击率
	public String getCtr() {
		//曝光量为0
		if ("0".equals(this.viewCount)){
			return ctr;
		}else {//曝光量不为0
			int a = Integer.parseInt(this.validClickCount);
			int b = Integer.parseInt(this.viewCount);
			NumberFormat nf = NumberFormat.getPercentInstance();
			nf.setMinimumFractionDigits( 2 );
			this.ctr = nf.format((float) a / (float) b);
			return ctr;
		}
	}

	public void setCtr(String ctr) {
		this.ctr = ctr;
	}

	//计算点击均价
	public String getCpc() {
		//点击量为0
		if ("0".equals(this.validClickCount)){
			return cpc;
		}else {//点击量不为0
			int a = Integer.parseInt(this.cost);
			int b = Integer.parseInt(this.validClickCount);
			DecimalFormat df=new DecimalFormat("0.00");
			this.cpc = df.format((float)a/((float)b*100));
			return cpc;
		}
	}

	public void setCpc(String cpc) {
		this.cpc = cpc;
	}


}