/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 素材库
 * @date 2021-06-19 16:12:13
 */
@Data
public class AdMaterialDownResp implements Serializable {

	private static final long serialVersionUID = 3958282681369875648L;
	/**
     * 主键ID
     */
    private Long id;
    /**
     * 素材名称
     */
    private String name;
}
