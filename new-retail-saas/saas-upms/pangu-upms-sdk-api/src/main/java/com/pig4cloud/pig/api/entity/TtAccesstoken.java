package com.pig4cloud.pig.api.entity;


import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *   授权码
 * @author zhuxm
 *
 */
@Data
@NoArgsConstructor
@TableName(value="tt_accesstoken")
public class TtAccesstoken extends Model<TtAccesstoken> {

	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "广告主ID不能为空")
	@ApiModelProperty(value = "广告主ID")
	@TableId(value = "ad_account")
	private String adAccount;
	
	@ApiModelProperty(value = "应用ID")
	@TableField(value = "app_id")
	private String appId;
	
	@ApiModelProperty(value = "接入方私钥")
	private String secret;
	
	@ApiModelProperty(value = "授权认证码")
	@TableField(value = "auth_code")
	private String authCode;
	
	@ApiModelProperty(value = "访问令牌")
	@TableField(value = "access_token")
	private String accessToken;
	
	
	@ApiModelProperty(value = "当前时间")
	@TableField(value = "time")
	private Long time;
	
	
	@ApiModelProperty(value = "应用ID")
	@TableField(value = "expires_in")
	private Long expiresIn;
	
	@ApiModelProperty(value = "刷新token")
	@TableField(value = "refresh_token")
	private String refreshToken;
	
	@ApiModelProperty(value = "刷新token过期时间")
	@TableField(value = "refresh_token_expires")
	private Long refreshTokenExpiresIn;
	
	@ApiModelProperty(value = "创建时间")
	@TableField( fill = FieldFill.INSERT)
	private Date createtime;
	
	@ApiModelProperty(value = "修改时间")
	@TableField( fill = FieldFill.UPDATE)
	private Date updatetime;
	
	@ApiModelProperty(value = "账号角色，1-普通广告主，2-账号管家，3-一级代理商，4-二级代理商")
	@TableField(value = "housekeeper")
	private Integer housekeeper;
	
	@ApiModelProperty(value = "新版授权账号角色  ADVERTISER广告主, CUSTOMER_ADMIN管家-管理员,         AGENT代理商, CHILD_AGENT二级代理商, CUSTOMER_OPERATOR管家-操作者")
	@TableField(value = "account_role")
	private String accountRole;
	
	
	@ApiModelProperty(value = "同步其他平台标识,  N：还未同步，Y:已同步")
	@TableField(value = "issyn")
	private String issyn;

	@TableField(exist = false)
	private String advertiserIds;
	
	
}