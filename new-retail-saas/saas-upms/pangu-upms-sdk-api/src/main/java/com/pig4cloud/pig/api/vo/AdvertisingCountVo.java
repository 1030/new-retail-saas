package com.pig4cloud.pig.api.vo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;


/**
 * @广告账户
 * @author john
 *
 */
@Data
public class AdvertisingCountVo {

	private static final long serialVersionUID = 1L;

	/**
	 * 账户总余额(单位元)
	 */
	@ApiModelProperty(value = "账户总余额汇总(单位元)")
	private BigDecimal balance=new BigDecimal(0);
	
	/**
	 * 账户可用总余额(单位元)
	 */
	@ApiModelProperty(value = "账户可用总余额汇总(单位元)")
	private BigDecimal validBalance=new BigDecimal(0);
	
	/**
	 * 现金余额(单位元)
	 */
	@ApiModelProperty(value = "现金余额汇总(单位元)")
	private BigDecimal cash=new BigDecimal(0);
	
	/**
	 * 现金可用余额(单位元)
	 */
	@ApiModelProperty(value = "现金可用余额汇总(单位元)")
	private BigDecimal validCash=new BigDecimal(0);
	
	
	/**
	 * 赠款余额(单位元)
	 */
	@ApiModelProperty(value = "赠款余额汇总(单位元)")
	private BigDecimal grant=new BigDecimal(0);
	
	/**
	 * 赠款可用余额(单位元)
	 */
	@ApiModelProperty(value = "赠款可用余额汇总(单位元)")
	private BigDecimal validGrant=new BigDecimal(0);
	




}
