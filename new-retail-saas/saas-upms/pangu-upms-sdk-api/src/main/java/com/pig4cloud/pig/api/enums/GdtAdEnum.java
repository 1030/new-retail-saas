package com.pig4cloud.pig.api.enums;

/**
 * @projectName:pig
 * @description:广点通推广计划枚举类型
 * @author:nml
 * @createTime:2020/12/9 15:06
 */
public enum GdtAdEnum {
	/**
	 * 广告计划客户设置类型
	 */
	AD_STATUS_NORMAL("AD_STATUS_NORMAL", "有效"),
	AD_STATUS_SUSPEND("AD_STATUS_SUSPEND", "暂停");

	/**
	 * 类型
	 */
	private String type;
	/**
	 * 名称
	 */
	private String name;

	GdtAdEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
