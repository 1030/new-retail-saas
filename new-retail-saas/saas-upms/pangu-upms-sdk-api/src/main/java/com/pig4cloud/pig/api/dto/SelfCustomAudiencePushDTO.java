package com.pig4cloud.pig.api.dto;

import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;
import java.util.List;

@Data
public class SelfCustomAudiencePushDTO implements Serializable {

	private static final long serialVersionUID = -4709678072731116081L;

	//自有人群包主键
	private List<Long> ids;

	private Integer mediaType;

	private List<String> advertiserIds;



}
