package com.pig4cloud.pig.api.gdt.dto;

import lombok.Data;

@Data
public class GdtResult<T> {
	private Integer code;
	private String message;
	private String message_cn;
	private T data;
}
