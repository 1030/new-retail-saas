package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @Description 广点通转化场景
 * @Author chengang
 * @Date 2022/5/16
 */
public enum GdtConversionSceneEnum {
	/**
	 * 广点通转化场景枚举类
	 */
	CONVERSION_SCENE_ANDROID("CONVERSION_SCENE_ANDROID", "安卓转化场景"),
	CONVERSION_SCENE_IOS("CONVERSION_SCENE_IOS", "iOS 转化场景"),
	CONVERSION_SCENE_WEB("CONVERSION_SCENE_WEB", "WEB 转化场景"),
	CONVERSION_SCENE_WECHAT_MINI_PROGRAM("CONVERSION_SCENE_WECHAT_MINI_PROGRAM", "微信小程序转化场景"),
	CONVERSION_SCENE_WECHAT_MINI_GAME("CONVERSION_SCENE_WECHAT_MINI_GAME", "微信小游戏PT46转化场景"),
	CONVERSION_SCENE_QQ_MINI_GAME("CONVERSION_SCENE_QQ_MINI_GAME", "QQ小游戏PT49转化场景"),
	CONVERSION_SCENE_QUICK_APP("CONVERSION_SCENE_QUICK_APP", "快应用转化场景"),
	CONVERSION_SCENE_WE_COM("CONVERSION_SCENE_WE_COM", "企业微信转化场景");

	/**
	 * 类型
	 */
	private String type;
	/**
	 * 名称
	 */
	private String name;

	public static String getNameByType(String type) {
		if (StringUtils.isBlank(type)) {
			return null;
		}

		for (GdtConversionSceneEnum item : GdtConversionSceneEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	GdtConversionSceneEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
