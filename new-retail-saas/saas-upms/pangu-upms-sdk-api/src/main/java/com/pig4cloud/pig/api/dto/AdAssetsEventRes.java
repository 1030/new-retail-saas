package com.pig4cloud.pig.api.dto;

import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 广告资产事件表
 * @author  chenxiang
 * @version  2022-04-01 16:25:19
 * table: ad_assets_event
 */
@Data
public class AdAssetsEventRes implements Serializable {
	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 资产ID
	 */
	private Long assetId;
	/**
	 * 事件ID
	 */
	private Long eventId;
	/**
	 * 广告账户
	 */
	private String advertiserId;
	/**
	 * 统计方式， 允许值：ONLY_ONE仅一次、EVERY_ONE每一次（仅付费ROI时可用）
	 */
	private String statisticalType;
	/**
	 * 事件回传方式列表，允许值:落地页支持:JSSDK JS埋码 、EXTERNAL_API API回传、XPATH XPath圈选应用支持：APPLICATION_API 应用API、APPLICATION_SDK 应用SDK、快应用支持：QUICK_APP_API 快应用API
	 */
	private String trackTypes;
	/**
	 * 事件类型
	 */
	private String eventType;
	/**
	 * 事件中文名称
	 */
	private String eventCnName;
	/**
	 * 事件描述
	 */
	private String description;
	/**
	 * 创建时间
	 */
	private Date thirdCreateTime;
	/**
	 * 激活免联调状态，枚举值：Active 已激活、Inactive 未激活
	 */
	private String debuggingStatus;
	/**
	 * 事件属性
	 */
	private String properties;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 创建人
	 */
	private Long createId;
	/**
	 * 修改人
	 */
	private Long updateId;
}


