/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.util.Page;
import lombok.Data;

/**
 * 代理商返点表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
@Data
public class AdAgentRebateVo extends Page {
    /**
     * 主键
     */
    private String id;
    /**
     * 代理商ID
     */
    private String agentId;
    /**
     * 代理商名称
     */
    private String agentName;
	/**
	 * 平台ID
	 */
	private String platformId;
	/**
	 * 渠道编码
	 */
	private String channelCode;
	/**
	 * 渠道名称
	 */
	private String channelName;
    /**
     * 返点
     */
    private String rebate;
    /**
     * 生效时间
     */
    private String effectiveTime;
}
