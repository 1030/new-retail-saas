package com.pig4cloud.pig.api.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.api.entity
 * @Author 马嘉祺
 * @Date 2021/8/27 10:53
 * @Description
 */
@Data
public class AdMaterialReportDO {

	/**
	 * 素材ID
	 */
	private Long materialId;

	/**
	 * 素材类型
	 */
	private Integer materialType;

	/**
	 * 素材名称
	 */
	private String materialName;

	/**
	 * 文件URL
	 */
	private String fileUrl;

	/**
	 * 视频缩略图
	 */
	private String imageUrl;

	/**
	 * 创意者ID
	 */
	private Integer creatorId;

	/**
	 * 创意者姓名
	 */
	private String creatorName;

	/**
	 * 制作者ID
	 */
	private Integer makerId;

	/**
	 * 制作者姓名
	 */
	private String makerName;

	/**
	 * 宽
	 */
	private String width;

	/**
	 * 高
	 */
	private String height;

	/**
	 * 素材像素
	 */
	private String materialPixel;

	/**
	 * 视频时长
	 */
	private String duration;

	/**
	 * 主游戏ID
	 */
	private Long pgid;

	/**
	 * 主游戏名称
	 */
	private String pgname;

	/**
	 * 制作类型
	 */
	private Integer makeType;

	/**
	 * 渠道编码
	 */
	private String chncode;

	/**
	 * 渠道名称
	 */
	private String chnname;

	/**
	 * 系统类型
	 */
	private Integer osType;

	/**
	 * 消耗
	 */
	private BigDecimal cost;

	/**
	 * 返点后消耗
	 */
	private BigDecimal rudecost;

	/**
	 * 展示数
	 */
	private Long showCount;

	/**
	 * 点击数
	 */
	private Long clickCount;

	/**
	 * 点击率，百分比
	 */
	private BigDecimal showClickRate;

	/**
	 * 激活数
	 */
	private Long activeCount;

	/**
	 * 注册数
	 */
	private Long registerCount;

	/**
	 * 点击注册率，百分比
	 */
	private BigDecimal clickRegisterRate;

	/**
	 * 点击激活率，百分比
	 */
	private BigDecimal clickActiveRate;

	/**
	 * 注册成本
	 */
	private BigDecimal registerReducost;

	/**
	 * 新增付费人数
	 */
	private Long newPayUsers;

	/**
	 * 新增付费实付金额
	 */
	private BigDecimal newPayFee;

	/**
	 * 新增付费抵扣金额
	 */
	private BigDecimal newPayGivemoney;

	/**
	 * 累计实际付费金额
	 */
	private BigDecimal totalFee;

	/**
	 * 累计抵扣付费金额
	 */
	private BigDecimal totalGivemoney;

	/**
	 * 新增付费率，百分比
	 */
	private BigDecimal newPayRate;

	/**
	 * 新增付费成本
	 */
	private BigDecimal newPayReducost;

	/**
	 * 首日ROI，百分比
	 */
	private BigDecimal firstDayRoi;

	/**
	 * 累计ROI，百分比
	 */
	private BigDecimal totalRoi;

	/**
	 * 次留人数
	 */
	private Long stay2UserCount;

	/**
	 * 次留，百分比
	 */
	private BigDecimal activeStay2Rate;

	/**
	 * 7留人数
	 */
	private Long stay7UserCount;

	/**
	 * 7留，百分比
	 */
	private BigDecimal activeStay7Rate;

	/**
	 * 30留人数
	 */
	private Long stay30UserCount;

	/**
	 * 30留，百分比
	 */
	private BigDecimal activeStay30Rate;

	/**
	 * 素材持续使用天数
	 */
	private Long usedDayCount;

	/**
	 * 素材使用次数
	 */
	private Long usedCreativeCount;

	/**
	 * 总创意数
	 */
	private Long totalCreativeCount;

	/**
	 * 素材使用率，百分比
	 */
	private BigDecimal usedRate;

	/**
	 * 10%播放数
	 */
	private Long play10perCount;

	/**
	 * 25%播放数
	 */
	private Long play25perCount;

	/**
	 * 50%播放数
	 */
	private Long play50perCount;

	/**
	 * 75%播放数
	 */
	private Long play75perCount;

	/**
	 * 95%播放数
	 */
	private Long play95perCount;

	/**
	 * 99%播放数
	 */
	private Long play99perCount;

	/**
	 * 总播放数
	 */
	private Long playTotalCount;

	/**
	 * 有效播放数
	 */
	private Long playValidCount;

	/**
	 * 完播数
	 */
	private Long playOverCount;

	/**
	 * 10%播放率，百分比
	 */
	private BigDecimal play10perRate;

	/**
	 * 25%播放率，百分比
	 */
	private BigDecimal play25perRate;

	/**
	 * 50%播放率，百分比
	 */
	private BigDecimal play50perRate;

	/**
	 * 75%播放率，百分比
	 */
	private BigDecimal play75perRate;

	/**
	 * 95%播放率，百分比
	 */
	private BigDecimal play95perRate;

	/**
	 * 99%播放率，百分比
	 */
	private BigDecimal play99perRate;

	/**
	 * 完成播放率，百分比
	 */
	private BigDecimal playOverRate;

	/**
	 * 有效播放成本
	 */
	private BigDecimal playValidCost;

	/**
	 * 有效播放率，百分比
	 */
	private BigDecimal playValidRate;

}
