package com.pig4cloud.pig.api.gdt.dto;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
public class GdtUnionPackageDto {
	/**
	 * 广告主 id，有操作权限的广告主 id
	 */
	@NotNull(message = "广告主ID不能为空")
	private Integer account_id;
	/**
	 * 流量包名称，同一帐号下的流量包名称不允许重复，最多可创建 300 个流量包（字段长度最小 1 个等宽字符，长度最大 60 等宽字符（即字段最大长度为 60 个中文字或全角标点，120 个英文字或半角标点。一个等宽字符等价于一个中文，等价于两个英文。）
	 * 字段长度最小 1 字节，长度最大 180 字节
	 */
	@NotEmpty(message = "流量包名称不能为空")
	@Length(min = 1)
	private String union_package_name;
	/**
	 * 推广目标类型 { PROMOTED_OBJECT_TYPE_APP_ANDROID, PROMOTED_OBJECT_TYPE_APP_IOS, PROMOTED_OBJECT_TYPE_ECOMMERCE, PROMOTED_OBJECT_TYPE_LINK_WECHAT, PROMOTED_OBJECT_TYPE_APP_ANDROID_MYAPP, PROMOTED_OBJECT_TYPE_APP_ANDROID_UNION, PROMOTED_OBJECT_TYPE_LOCAL_ADS_WECHAT, PROMOTED_OBJECT_TYPE_QQ_BROWSER_MINI_PROGRAM, PROMOTED_OBJECT_TYPE_LINK, PROMOTED_OBJECT_TYPE_QQ_MESSAGE, PROMOTED_OBJECT_TYPE_LEAD_AD }
	 */

	private String promoted_object_type;
	/**
	 * 推广目标 id
	 */
	private String promoted_object_id;
	/**
	 * 流量包类型 枚举列表：UNION_PACKAGE_TYPE_INCLUDE（定投）、UNION_PACKAGE_TYPE_EXCLUDE（屏蔽），流量包类型,UNION_PACKAGE_TYPE_INCLUDE, UNION_PACKAGE_TYPE_EXCLUDE
	 */
	@NotEmpty
	private String union_package_type;
	/**
	 * 优量汇广告位 id 列表
	 * 数组最小长度 1，最大长度 2000
	 * 此ID列表需要到广点通平台人工获取 2021-07-29
	 */
	@NotNull(message = "优量汇广告位ID列表不能为空")
	@Size(min = 1, max = 2000,message = "优量汇广告位ID个数在[1,2000]区间")
	private Integer[] union_position_id_list;
	/**
	 * 创建时间，unix 时间戳，精确到秒
	 */
	private Integer created_time;
	/**
	 * 最后修改时间，unix 时间戳，精确到秒
	 */
	private Integer last_modified_time;
}
