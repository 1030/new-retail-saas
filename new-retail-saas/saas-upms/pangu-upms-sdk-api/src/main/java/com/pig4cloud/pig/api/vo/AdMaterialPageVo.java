/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.util.Page;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.List;

/**
 * 素材库
 * @date 2021-06-19 16:12:13
 */
@Data
public class AdMaterialPageVo extends Page {
    /**
     * 主键ID
     */
    private String id;
    /**
     * 素材名称
     */
    private String name;
    /**
     * 素材名称(批量)
     */
    private String names;
    /**
     * 素材类型（1：视频，2：图片）
     */
    private String type;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 主游戏ID
     */
    private String mainGameId;
	/**
	 * 子游戏ID
	 */
	private String gameId;
    /**
     * 创意者
     */
    private String creatorId;
    /**
     * 制作者
     */
    private String makerId;
    /**
     * 制作类型（1：原创，2：原创衍生，3：抄本，4：抄本衍生）
     */
    private String makeType;
    /**
     * md5值
     */
    private String md5;
    /**
     * 素材宽
     */
    private String width;
    /**
     * 素材高
     */
    private String height;
    /**
     * 文件大小
     */
    private String fileSize;
    /**
     * 尺寸类型：（1:横屏视频，2:竖屏视频，3:小图，4:横版大图，5:竖版大图，6:gif图，7:卡片主图，0:其他）
     */
    private String screenType;
    /**
     * 素材格式
     */
    private String format;
    /**
     * 素材时长
     */
    private String duration;
    /**
     * 标签
     */
    private String labels;
	/**
	 * 状态：（0：启用，1：禁用）
	 */
	private String status;
	/**
	 * 访问权限(0公开、1本人、2小组)
	 */
	private String readpop;
	/**
	 * 是否删除：0正常，1删除
	 */
	private String isDelete;
	/**
	 * 素材文件
	 */
	private Collection<MultipartFile> files;
	/**
	 * 平台ID：1，头条，2广点通
	 */
	private String platformId;
	/**
	 * 素材id（批量）
	 */
	private String ids;
	/**
	 * 广告账户（批量）
	 */
	private String advertiserIds;
	/**
	 * 三方平台文件ID
	 */
	private String platformFileId;
	/**
	 * 当前登录用户ID
	 */
	private String loginUserId;
	/**
	 * 是否为收藏：true：false
	 */
	private String isCollect;
	/**
	 * 用户ID
	 */
	private String userId;
	/**
	 * 卖点
	 */
	private Integer sellingPointId;
	/**
	 * 素材来源：0：盘古；1：炎帝
	 */
	private Integer origin;
	/**
	 * 业务ID
	 */
	private Integer originId;

	private String tagIds;

	private List<Long> tagIdList;

}
