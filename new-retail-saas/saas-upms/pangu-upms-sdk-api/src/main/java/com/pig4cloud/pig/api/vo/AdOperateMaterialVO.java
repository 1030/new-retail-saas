package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

/**
 * @Title null.java
 * @Package com.pig4cloud.pigx.vo
 * @Author 马嘉祺
 * @Date 2021/6/17 19:18
 * @Description
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AdOperateMaterialVO extends Page<Object> {

	/**
	 * 运营素材ID集合
	 */
	@NotBlank(message = "素材ID集合不能为空", groups = {Delete.class})
	private String materialIdArr;

	/**
	 * 主键
	 */
	@NotNull(message = "素材ID不能为空", groups = {Update.class, PushIcon.class})
	private Long materialId;

	/**
	 * 父游戏主键
	 */
	@NotNull(message = "父游戏ID不能为空", groups = {Create.class})
	private Long pgid;

	/**
	 * 父游戏主键
	 */
	private String pgidArr;

	/**
	 * 子游戏主键
	 */
	private Long gameid;

	/**
	 * 子游戏主键
	 */
	@NotBlank(message = "子游戏ID不能为空", groups = {Create.class})
	private String gameidArr;

	/**
	 * 创意者主键
	 */
	@NotNull(message = "创意者用户ID不能为空", groups = {Create.class})
	private Integer createUserId;

	/**
	 * 制作者主键
	 */
	@NotNull(message = "制作者用户ID不能为空", groups = {Create.class})
	private Integer makeUserId;

	/**
	 * 运营素材类型；0：icon；1:五图；2：loading图
	 */
	@NotNull(message = "运营素材类型不能为空", groups = {Create.class})
	private Integer materialType;

	/**
	 * 平台ID，1:头条；8：广点通
	 */
	@NotNull(message = "平台ID不能为空", groups = {PushIcon.class})
	private Integer platformId;

	/**
	 * 上传文件
	 */
	@NotEmpty(message = "运营素材文件名称不能为空", groups = {Create.class})
	private List<String> fileNames;
	@NotEmpty(message = "运营素材文件不能为空", groups = {Create.class})
	private List<MultipartFile> fileRaws;

	private Collection<MaterialFile> materialFiles;

	@NotBlank(message = "广告账户ID不能为空", groups = {PushIcon.class})
	private String advertiserIdArr;

	@Data
	@EqualsAndHashCode
	public static class MaterialFile {

		@NotNull(message = "文件ID不能为空", groups = {Update.class})
		private Long id;

		@NotNull(message = "运营素材文件不能为空", groups = {Create.class})
		private MultipartFile file;

		@NotBlank(message = "文件名称不能为空", groups = {Update.class})
		private String name;

		private String format;

	}

	public interface Create {

	}

	public interface Update {

	}

	public interface Delete {

	}

	public interface PushIcon {
	}

}
