package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName AdVideoPlatform.java
 * @author cx
 * @version 1.0.0
 * @Desc TODO
 * @Time 2020/11/12 16:10
 */
@Data
public class AdVideoAdVo  {



    /**
     * 台视频id
     */
    private String vid;

	/**
	 * 广告账户ID
	 */
	private String advertiserId;

    /**
     * 三方平台视频ID
     */
    private String platformVid;

    /**
     * 三方素材ID
     */
    private String materialId;

    /**
     * 修改时间
     */
    private Date updatetime;
	/**
	 * 视频名称
	 */
	private String vName;


	private String imageUrl;

	private String fileUrl;

}