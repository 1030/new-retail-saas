package com.pig4cloud.pig.api.gdt.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 广点通广告组级别报表日流水
 * @author yk
 *
 */
@Getter
@Setter
public class AdvertiserDailyReport implements Serializable {


}