package com.pig4cloud.pig.api.dto;

import lombok.Data;

@Data
public class TtPromotionCard {
	private Boolean	enableStorePack	;//	是否使用门店包，true为使用，false为不使用，推广目的非门店推广时会忽略该字段。若选择使用，则卡片标题为{最近门店名称}
	private String[]	productSellingPoints	;//	商品卖点
	private String	productDescription	;//	商品描述
	private String	callToAction	;//	行动号召
	private Boolean	enablePersonalAction	;//	是否使用智能优选，true为使用，false为不使用
	private String	productImageId	;//	商品图片ID
}
