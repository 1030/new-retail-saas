package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.util.List;

/**
 * @Description
 * @Author chengang
 * @Date 2021/6/24
 */
@Data
public class AdAudienceDTO {

	//20210624 clickhouse 需要 int
	private Integer startDate;

	//20210624 clickhouse 需要 int
	private Integer endDate;

	// 平台类型:0-3399平台,1-头条,2-百度,3-新数,4-公会,7-UC,8-广点通
	private Integer cType;

	//广告计划集合
	private List<String> ids;

	/**
	 * 平台权限：当前登录人所关联角色对应的投放人集合
	 */
	List<Integer> throwUserList;

}
