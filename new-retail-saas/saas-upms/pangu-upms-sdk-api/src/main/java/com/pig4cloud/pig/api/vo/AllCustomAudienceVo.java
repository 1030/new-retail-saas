package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description 二合一人群包应答字段
 * @Author chengang
 * @Date 2021/6/18
 */
@Data
public class AllCustomAudienceVo implements Serializable {

	private Long id;
	private Integer mediaType;
	private String accountId;
	private String accountName;
	private Long customAudienceId;
	private String name;
	private String typeName;
	private String sourceName;
	private Integer pushStatus;
	private String statusName;
	private String modifyTime;
	private String createTime;
	private Long coverNum;



}
