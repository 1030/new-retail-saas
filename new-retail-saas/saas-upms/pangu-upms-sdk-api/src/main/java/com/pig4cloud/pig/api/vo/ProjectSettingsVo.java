package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;

/**
 * @description:
 * @author: nml
 * @time: 2020/11/18 19:50
 **/
@Getter
@Setter
public class ProjectSettingsVo extends Page {

	/*
	 *  唯一id
	 * */
	private Integer id;

	/**
	 * 项目名称
	 */
	private String projectName;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 修改时间
	 */
	private Date updateTime;

}
