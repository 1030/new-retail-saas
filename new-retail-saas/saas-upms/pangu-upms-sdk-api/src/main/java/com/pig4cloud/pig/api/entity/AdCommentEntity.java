package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_commnet")
public class AdCommentEntity extends Model<AdCommentEntity> {

	/**
	 * 评论id
	 */
	@TableId(value = "id")
	private Long id;

	/**
	 * 给前端返回值得序列化，防止精度丢失
	 */
	@TableField(exist = false)
	private String idStr;

	/**
	 * app名，西瓜、抖音等
	 */
	private String appName;

	/**
	 * 评论用户昵称
	 */
	private String userScreenName;

	/**
	 * 评论用户抖音号
	 */
	private String awemeAccount;

	/**
	 * 广告计划id
	 */
	private Long adId;

	/**
	 * 广告计划名
	 */
	private String adName;

	/**
	 * 评论内容
	 */
	private String text;

	/**
	 * 点赞数
	 */
	private Integer likeCount;

	/**
	 * 评论创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 评论创建日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date createDay;

	/**
	 * 是否置顶，0：表示不置顶，1：表示置顶
	 */
	private Integer stick;

	/**
	 * 评论的回复数量
	 */
	private Integer replyCount;

	/**
	 * 评论发布者是否在黑名单内，0表示不在，1表示在
	 */
	private Integer inBlackList;

	/**
	 * 评论回复状态，REPLIED表示已回复，NO_REPLY表示未回复
	 */
	private String replyStatus;

	/**
	 * 评论所属创意id
	 */
	private Long creativeId;

	/**
	 * 层级，1：评论；2：回复
	 */
	@TableField(value = "`rank`")
	private Integer rank;

	/**
	 * 广告账户id
	 */
	private String advertiserId;

	/**
	 * 广告账户名称
	 */
	private String advertiserName;

	/**
	 * 广告账户 对应tt_accesstoken 的ad_account
	 */
	private String adAccount;

	/**
	 * 二级回复父评论id
	 */
	private Long replyCommentId;

	@TableField(exist = false)
	private String replyCommentIdStr;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;

	/**
	 * 是否删除
	 */
	private Integer isDelete;

	/**
	 * 是否隐藏：0：未隐藏；1：隐藏；
	 */
	private Integer hide;

	/**
	 * 是否包含屏蔽词，0：不包含；1：包含
	 */
	private Integer isTermsBanned;

	/**
	 * 屏蔽词列表，多个屏蔽词使用','分隔
	 */
	private String termsBannedList;

	/**
	 * 回复数据列表
	 */
	@TableField(exist = false)
	private List<AdCommentEntity> replyList;

	public AdCommentEntity setId(Long id) {
		this.id = id;
		return this;
	}

	public AdCommentEntity setIdStr(String idStr) {
		this.idStr = idStr;
		return this;
	}

	public AdCommentEntity setAppName(String appName) {
		this.appName = appName;
		return this;
	}

	public AdCommentEntity setUserScreenName(String userScreenName) {
		this.userScreenName = userScreenName;
		return this;
	}

	public AdCommentEntity setAwemeAccount(String awemeAccount) {
		this.awemeAccount = awemeAccount;
		return this;
	}

	public AdCommentEntity setAdId(Long adId) {
		this.adId = adId;
		return this;
	}

	public AdCommentEntity setAdName(String adName) {
		this.adName = adName;
		return this;
	}

	public AdCommentEntity setText(String text) {
		this.text = text;
		return this;
	}

	public AdCommentEntity setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
		return this;
	}

	public AdCommentEntity setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}

	public AdCommentEntity setCreateDay(Date createDay) {
		this.createDay = createDay;
		return this;
	}

	public AdCommentEntity setStick(Integer stick) {
		this.stick = stick;
		return this;
	}

	public AdCommentEntity setReplyCount(Integer replyCount) {
		this.replyCount = replyCount;
		return this;
	}

	public AdCommentEntity setInBlackList(Integer inBlackList) {
		this.inBlackList = inBlackList;
		return this;
	}

	public AdCommentEntity setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
		return this;
	}

	public AdCommentEntity setCreativeId(Long creativeId) {
		this.creativeId = creativeId;
		return this;
	}

	public AdCommentEntity setRank(Integer rank) {
		this.rank = rank;
		return this;
	}

	public AdCommentEntity setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
		return this;
	}

	public AdCommentEntity setAdvertiserName(String advertiserName) {
		this.advertiserName = advertiserName;
		return this;
	}

	public AdCommentEntity setAdAccount(String adAccount) {
		this.adAccount = adAccount;
		return this;
	}

	public AdCommentEntity setReplyCommentId(Long replyCommentId) {
		this.replyCommentId = replyCommentId;
		return this;
	}

	public AdCommentEntity setReplyCommentIdStr(String replyCommentIdStr) {
		this.replyCommentIdStr = replyCommentIdStr;
		return this;
	}

	public AdCommentEntity setCreateDate(Date createDate) {
		this.createDate = createDate;
		return this;
	}

	public AdCommentEntity setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
		return this;
	}

	public AdCommentEntity setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
		return this;
	}

	public AdCommentEntity setHide(Integer hide) {
		this.hide = hide;
		return this;
	}

	public AdCommentEntity setIsTermsBanned(Integer isTermsBanned) {
		this.isTermsBanned = isTermsBanned;
		return this;
	}

	public AdCommentEntity setTermsBannedList(String termsBannedList) {
		this.termsBannedList = termsBannedList;
		return this;
	}

	public AdCommentEntity setReplyList(List<AdCommentEntity> replyList) {
		this.replyList = replyList;
		return this;
	}
}
