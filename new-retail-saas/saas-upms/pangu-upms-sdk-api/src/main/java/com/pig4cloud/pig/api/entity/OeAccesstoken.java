package com.pig4cloud.pig.api.entity;


import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *   3399授权码
 * @author zhuxm
 *
 */
@Data
@NoArgsConstructor
@TableName(value="oe_accesstoken")
public class OeAccesstoken extends Model<OeAccesstoken> {

	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "广告主ID不能为空")
	@ApiModelProperty(value = "图片名称")
	@TableId(value = "ad_account")
	private String ad_account;
	
	@ApiModelProperty(value = "由投放同步")
	@TableField(value = "refresh_by_pig")
	private String refreshByPig;
}