package com.pig4cloud.pig.api.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 广点通动态创意表(gdt_dynamic_creative)实体类
 *
 * @author zjz
 * @since 2022-07-11 22:26:02
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("gdt_dynamic_creative")
public class GdtDynamicCreative extends Model<GdtDynamicCreative> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 广告主帐号id，有操作权限的帐号id，不支持代理商id
     */
    private Long accountId;
    /**
     * 广告创意 id
     */
	@TableId
    private Long dynamicCreativeId;
    /**
     * 外部广告素材 id，小于 2^63
     */
    private Long outerAdcreativeId;
    /**
     * 广告创意名称（字段长度最小 1 个等宽字符，长度最大 60 等宽字符（即字段最大长度为 60 个中文字或全角标点，120 个英文字或半角标点。一个等宽字符等价于一个中文，等价于两个英文。）
     */
    private String dynamicCreativeName;
    /**
     * 创意规格 id，具体请咨询您的运营接口人，也可通过 [创意规格查询工具] 进行查询
     */
    private Long dynamicCreativeTemplateId;
    /**
     * 创意元素
     */
    private String dynamicCreativeElements;
    /**
     * 落地页类型，[枚举详情]
     */
    private String pageType;
    /**
     * 落地页信息，根据不同 promoted_object_type 和 page_type，要求的 page_spec 信息不同
     */
    private String pageSpec;
    /**
     * 应用直达页 URL
     */
    private String deepLinkUrl;
    /**
     * 是否开启自动版位功能（暂不包含 SITE_SET_WECHAT 和 SITE_SET_MOMENTS），此字段与 site_set 字段至少需要写入一个。
     */
    private String automaticSiteEnabled;
    /**
     * 投放版位集合，当前单版位或者 SITE_SET_TENCENT_NEWS+SITE_SET_TENCENT_VIDEO+SITE_SET_MOBILE_UNION+SITE_SET_KANDIAN+SITE_SET_QQ_MUSIC_GAME 的组合，[枚举详情]
     */
    private String siteSet;
    /**
     * 推广目标类型，[枚举详情]
     */
    private String promotedObjectType;
    /**
     * 推广目标 id，详见 [查询推广目标 id]
     */
    private String promotedObjectId;
    /**
     * 朋友圈头像昵称跳转页 id，仅微信朋友圈广告（site_set=SITE_SET_MOMENTS）的创意形式支持
     */
    private Long profileId;
    /**
     * 创建时间（时间戳）
     */
    private Long createdTime;
    /**
     * 最后修改时间（时间戳）
     */
    private Long lastModifiedTime;
    /**
     * 动态商品广告属性，当创意为动态商品广告创意时，该字段必填
     */
    private String dynamicAdcreativeSpec;
    /**
     * 是否已删除，true：1，false：0
     */
    private String isDeleted;
    /**
     * 推广计划类型，[枚举详情]
     */
    private String campaignType;
    /**
     * 曝光监控地址，监控地址主域只允许白名单内的域名，详见 [第三方监控]
     */
    private String impressionTrackingUrl;
    /**
     * 监控链接，监控地址主域只允许白名单内的域名，详见 [第三方监控]
     */
    private String clickTrackingUrl;
    /**
     * 视频广告评论开关
     */
    private String feedsVideoCommentSwitch;
    /**
     * 联盟下载器开关
     */
    private String unionMarketSwitch;
    /**
     * 视频播放结束页，（当前仅支持互动推广页）
     */
    private String videoEndPage;
    /**
     * 弹幕列表
     */
    private String barrageList;
    /**
     * 动态创意分组，使用方法详见[动态创意广告]默认为不使用组合，功能灰度开放中，如需使用可联系您的客户运营，[枚举详情]
     */
    private String dynamicCreativeGroupUsed;
    /**
     * 礼包码，微信朋友圈广告创意形式需要填写，礼包码
     */
    private String appGiftPackCode;
    /**
     * 是否支持版位突破，广告版位选择“微信朋友圈”，当广告有机会获得更多曝光与转化时，将可能投放到微信公众号与小程序版位，若不传值默认为 true
     */
    private String enableBreakthroughSiteset;
    /**
     * 数据版本类型 0:历史数据 1:旧版本 2:新版本，[枚举详情]
     */
    private String creativeTemplateVersionType;
    /**
     * 厂商下载信息，厂商下载信息，仅可在跳转厂商应用商店（union_market_switch）值为 true 时使用。
     */
    private String unionMarketSpec;
    /**
     * 创意形式类型
     */
    private String creativeTemplateCategory;
    /**
     * 头像点击跳转信息，[枚举详情]
     */
    private String headClickType;
    /**
     * 头像点击跳转信息，头像点击跳转信息，目前支持跳转搜一搜品牌专区，白名单开放中，如果需要使用请联系您的运营经理
     */
    private String headClickSpec;
    /**
     * 来源(1:API,2:媒体后台)
     */
    private Integer soucreStatus;
    /**
     * deleted
     */
    private Integer deleted;
    /**
     * createTime
     */
    private Date createTime;
    /**
     * updateTime
     */
    @TableField(update = "now()")
	private Date updateTime;
    /**
     * createId
     */
    private Long createId;
    /**
     * updateId
     */
    private Long updateId;

}