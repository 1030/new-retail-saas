package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoAudiencePackageFieldsEnum {

	//平台  1 ANDROID/ 2 IOS/ 3 PC/4 IPAD/ 5 WAP
	ADVERTISER_ID           ("advertiser_id"            ,"广告投放账号"       ),
	NAME                    ("name"                     ,"定向包名称"         ),
	DESCRIPTION             ("description"              ,"定向包描述"         ),
	LANDING_TYPE            ("landing_type"             ,"定向包类型"         ),
	DELIVERY_RANGE          ("delivery_range"           ,"投放范围"           ),
	DISTRICT                ("district"                 ,"地域类型"           ),
	CITY                    ("city"                     ,"省市or区县"         ),
	BUSINESS_IDS            ("business_ids"             ,"商圈"               ),
	LOCATION_TYPE           ("location_type"            ,"受众位置类型"       ),
	GENDER                  ("gender"                   ,"性别"               ),
	AGE                     ("age"                      ,"年龄"               ),
	RETARGETING_TAGS        ("retargeting_tags"         ,"自定义人群-定向人群"),
	RETARGETING_TAGS_EXCLUDE("retargeting_tags_exclude" ,"自定义人群-排除人群"),
	INTEREST_ACTION_MODE    ("interest_action_mode"     ,"行为兴趣"           ),
	AWEME_FAN_BEHAVIORS     ("aweme_fan_behaviors"      ,"抖音用户行为类型"   ),
	AWEME_FAN_CATEGORIES    ("aweme_fan_categories"     ,"抖音类目"           ),
	AWEME_FAN_ACCOUNTS      ("aweme_fan_accounts"       ,"抖音号"             ),
	PLATFORM                ("platform"                 ,"平台"               ),
	AC                      ("ac"                       ,"网络类型"           ),
	HIDE_IF_EXISTS          ("hide_if_exists"           ,"已安装用户"         ),
	ANDROID_OSV             ("android_osv"              ,"安卓版本"           ),
	CARRIER                 ("carrier"                  ,"运营商"             ),
	ACTIVATE_TYPE           ("activate_type"            ,"新用户"             ),
	DEVICE_BRAND            ("device_brand"             ,"手机品牌"           ),
	LAUNCH_PRICE            ("launch_price"             ,"手机价格"           ),
	AUTO_EXTEND_ENABLED     ("auto_extend_enabled"      ,"智能放量"           );



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoAudiencePackageFieldsEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoAudiencePackageFieldsEnum item : ToutiaoAudiencePackageFieldsEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoAudiencePackageFieldsEnum item : ToutiaoAudiencePackageFieldsEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoAudiencePackageFieldsEnum item : ToutiaoAudiencePackageFieldsEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
