package com.pig4cloud.pig.api.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="tt_custom_audience")
public class TtCustomAudience extends Model<TtCustomAudience> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4846463548424420828L;

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键id")
	private Long id;
	
	@TableField(value = "custom_audience_id")
	@ApiModelProperty(value = "人群包ID")
	private Long customAudienceId;

	@TableField(value = "media_type")
	@ApiModelProperty(value = "媒体类型")
	private Integer mediaType;
	
	@TableField(value = "account_id")
	@ApiModelProperty(value = "推广账户ID")
	private String accountId;
	
	@TableField(value = "isdel")
	@ApiModelProperty(value = "人群包是否删除，枚举值：\"1\"：已删除，\"0\"：未删除	")
	private Integer isdel;
	
	@TableField(value = "data_source_id")
	@ApiModelProperty(value = "数据源ID")
	private String dataSourceId;
	
	@TableField(value = "name")
	@ApiModelProperty(value = "人群包名称")
	private String name;
	
	@TableField(value = "source")
	@ApiModelProperty(value = "人群包来源")
	private String source;
	
	@TableField(value = "status")
	@ApiModelProperty(value = "人群包状态")
	private Integer status;
	
	@TableField(value = "delivery_status")
	@ApiModelProperty(value = "可投放状态")
	private String deliveryStatus;
	
	@TableField(value = "cover_num")
	@ApiModelProperty(value = "人群包覆盖人群数目")
	private Long coverNum;
	
	
	@TableField(value = "upload_num")
	@ApiModelProperty(value = "上传数据源包含的人群数目")
	private Long uploadNum;
	
	@TableField(value = "tag")
	@ApiModelProperty(value = "人群包标签")
	private String tag;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "本地创建时间")
	@TableField( fill = FieldFill.INSERT)
	private Date createtime;
	
	
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "本地修改时间")
	@TableField( fill = FieldFill.UPDATE)
	private Date updatetime;
	
	
	@TableField(value = "push_status")
	@ApiModelProperty(value = "推送状态")
	private Integer pushStatus;
	
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField(value = "create_time")
	private String ttCreateTime;
	
	
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(value = "modify_time")
	private String modifyTime;
	
	/**
	 * 是否为三方包，均返回非三方包
	 */
	@ApiModelProperty(value = "是否为三方包，均返回非三方包")
	@TableField(value = "third_party_info")
	private String thirdPartyInfo;

	@ApiModelProperty(value = "广点通人群包状态")
	@TableField(value = "gdt_status")
	private String gdtStatus;

	@TableField(value = "external_audience_id")
	private String externalAudienceId;

	@TableField(value = "description")
	private String description;

	@TableField(value = "gdt_type")
	private String gdtType;

	@TableField(value = "error_code")
	private Integer errorCode;

}
