package com.pig4cloud.pig.api.gdt.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @蹊径落地页
 * @author yk
 *
 */
@Getter
@Setter
public class GdtXlpVo extends Page {

	private Integer id;

	/**
	 * 落地页缩略图
	 */
	private String previewUrl;

	/**
	 * 落地页标题
	 */
	private String pageName;

	/**
	 * 落地页ID
	 */
	private String pageId;

	/**
	 * 状态（蹊径落地页发布状态）
	 */
	private String pagePublishStatus;

	/**
	 * 状态（蹊径落地页审核状态）
	 */
	private String pageStatus;

	/**
	 * 创建人
	 */
	private String createUser;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 落地页服务id，主要用于广告投放、落地页送审及删除
	 */
	private String pageServiceId;

	/**
	 * 蹊径落地页类型
	 */
	private String pageType;

	/**
	 * 蹊径落地页最近更新时间
	 */
	private String pageLastModifyTime;

	/**
	 * 推广帐号 id，有操作权限的帐号 id，包括代理商和广告主帐号 id
	 */
	private String accountId;

	/**
	 * 蹊径落地页发布类型
	 */
	private String pagePublishType;

	/**
	 * 推广目标 id
	 */
	private String promotedObjectId;

	/**
	 * 商品库 id
	 */
	private String productCatalogId;


	/**
	 * 多个广告主id
	 */
	private String[] accountIds;
}
