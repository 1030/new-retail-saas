package com.pig4cloud.pig.api.gdt.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 广点通-广告组临时参数dto
 * 
 * @author zjz
 * @date 2022-07-21
 */
@Data
public class GdtAdCopyDto implements Serializable {

	private static final long serialVersionUID = 8612322506653153316L;

	@NotBlank(message = "动态创意不能为空")
	private String dynamicCreativeId;

	@NotBlank(message = "计划ID不能为空")
	private String campaignId;

	@NotBlank(message = "广告组ID不能为空")
	private String adgroupId;
	/**
	 * 广告组名称（广告名称）
	 */
	@NotBlank(message = "广告名称不能为空")
	@Size(max=60, message = "广告名称长度不允许超过60个长度")
	private String adgroupName;

	@NotBlank(message = "广告转化不能为空")
	private String conversionId;

	@NotBlank(message = "落地页类型不能为空")
	private String pageType;

	private String pageId;

	@NotBlank(message = "投放账号不能为空")
	private String accountId;

}
