package com.pig4cloud.pig.api.gdt.dto;

import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * 广点通-广告创意对象dto
 *
 * @author hma
 * @date 2020-12-09
 */
@Data
public class GdtAdCreativeAddDto
{
	private static final long serialVersionUID = 1L;

	private List<GdtAdCreativeCreateDto> gdtAdCreativeCreateDtoList;

}
