package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 广点通广告创意表 gdt_ad_creative
 * 
 * @author hma
 * @date 2020-12-10
 */
@Setter
@Getter
public class GdtAdCreative
{
	private static final long serialVersionUID = 1L;

	/** 主键ID */
	@TableId(value = "id")
	private Long id;
	/** 推广计划 id */
	private Long campaignId;
	/** 广告创意 id */
	private Long adcreativeId;
	/** 广告创意名称 */
	private String adcreativeName;
	/** 创意形式 id */
	private Long adcreativeTemplateId;
	/** 创意元素 */
	private String adcreativeElements;
	/** 落地页类型 */
	private String pageType;
	/** 落地页信息 */
	private String pageSpec;
	/** 文字链跳转类型类型 */
	private String linkPageType;
	/** 链接名称类型 */
	private String linkNameType;
	/** 文字链跳转信息 */
	private String linkPageSpec;
	/** 数据展示的数据类型 */
	private String conversionDataType;
	/** 数据展示转化行为 */
	private String conversionTargetType;
	/** QQ 小游戏监控参数 */
	private String qqMiniGameTrackingQueryString;
	/** 应用直达页 URL */
	private String deepLinkUrl;
	/** 安卓应用直达 AppId */
	private String androidDeepLinkAppId;
	/** IOS 应用直达 AppId */
	private String iosDeepLinkAppId;
	/** 通用链接页 URL */
	private String universalLinkUrl;
	/** 投放版位集合 */
	private String siteSet;
	/** 是否开启自动版位功能 */
	private Boolean automaticSiteEnabled;
	/** 推广目标类型 */
	private String promotedObjectType;
	/** 推广目标 id */
	private String promotedObjectId;
	/** 朋友圈头像昵称跳转页 id */
	private Long profileId;
	/** 创建时间 */
	private Long createdTime;
	/** 最后修改时间 */
	private Long lastModifiedTime;
	/** 分享信息 */
	private String shareContentSpec;
	/** 动态商品广告属性 */
	private String dynamicAdcreativeSpec;
	/** 是否已删除，true：是，false：否 */
	private String isDeleted;
	/** 是否是动态创意广告自动生成的，true：是，false：否 */
	private String isDynamicCreative;
	/** 创意元素自动优化排序 */
	private String multiShareOptimizationEnabled;
	/** 附加创意组件 id */
	private Long componentId;
	/** 是否开启“创意修改、广告不下线”功能 */
	private String onlineEnabled;
	/** 修改后的创意内容，当且仅当 online_enabled = true 时，此字段允许写入 */
	private String revisedAdcreativeSpec;
	/** 创意分类 */
	private String category;
	/** 创意标签 */
	private String label;
	/** 跳转厂商应用商店 */
	private String unionMarketSwitch;
	/** 互动推广页落地页 id */
	private String playablePageMaterialId;
	/** 视频播放结束页 */
	private String videoEndPage;
	/** 	简易原生页嵌入 Webview url,和 simple_canvas_sub_type(灰度) 配合使用 */
	private String webviewUrl;
	/** 简版原生页子类型(灰度中) */
	private String simpleCanvasSubType;
	/** 浮层卡片创意内容 */
	private String floatingZone;
	/** 挂件图，悬浮展示在竖版视频上以展示营销卖点 */
	private String marketingPendantImageId;
	/** 倒计时组件开关 */
	private String countdownSwitch;
	/** 版本号 */
	private String version;
	/** 创建时间 */
	private Date createtime;
	/** 创建人 */
	private String createuser;
	/** 修改时间 */
	private Date updatetime;
	/** 修改人 */
	private String updateuser;
	/*广告账户id*/
	private Long accountId;
	/** 评论视频开关 true,false */
	private String  feedsVideoCommentSwitch;

	
}
