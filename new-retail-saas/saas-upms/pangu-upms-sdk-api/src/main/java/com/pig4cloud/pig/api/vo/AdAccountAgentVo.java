/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.util.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;

/**
 * 账户代理商关系表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
@Data
public class AdAccountAgentVo extends Page {
	/**
	 * 主键
	 */
	@NotBlank(message = "主键不能为空", groups = {UpdateAccountAgent.class})
	private String id;

	/**
	 * 广告账户ID
	 */
	@NotBlank(message = "广告账户ID不能为空", groups = {SaveAccountAgent.class, UpdateAccountAgent.class})
	private String advertiserId;

	/**
	 * 广告账户名称
	 */
	@NotBlank(message = "广告账户名称不能为空", groups = {SaveAccountAgent.class, UpdateAccountAgent.class})
	private String advertiserName;

	/**
	 * 代理商ID
	 */
	@NotBlank(message = "代理商ID不能为空", groups = {SaveAccountAgent.class, BatchAccountAgent.class})
	private String agentId;

	/**
	 * 代理商名称
	 */
	@NotBlank(message = "代理商名称不能为空", groups = {SaveAccountAgent.class, BatchAccountAgent.class})
	private String agentName;

	/**
	 * 生效时间
	 */
	@NotBlank(message = "生效时间不能为空", groups = {SaveAccountAgent.class, BatchAccountAgent.class})
	private String effectiveTime;

	/**
	 * 失效时间
	 */
	private String invalidTime;

	/**
	 * 广告账户列表信息
	 */
	@NotEmpty(message = "广告账户列表为空", groups = {BatchAccountAgent.class})
	private Collection<Advertiser> advertisers;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Advertiser {

		@NotBlank(message = "广告账户ID不能为空", groups = {BatchAccountAgent.class})
		private String advertiserId;

		@NotBlank(message = "广告账户名称不能为空", groups = {BatchAccountAgent.class})
		private String advertiserName;

	}

	public interface SaveAccountAgent {
	}

	public interface UpdateAccountAgent {
	}

	public interface BatchAccountAgent {
	}

}
