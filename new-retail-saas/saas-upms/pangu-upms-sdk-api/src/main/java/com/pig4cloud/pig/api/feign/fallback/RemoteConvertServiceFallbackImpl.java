/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.feign.fallback;

import com.pig4cloud.pig.api.entity.AdAssetsTrack;
import com.pig4cloud.pig.api.entity.AdConvert;
import com.pig4cloud.pig.api.entity.ConvertTrack;
import com.pig4cloud.pig.api.entity.GdtChannelPackage;
import com.pig4cloud.pig.api.entity.KsApp;
import com.pig4cloud.pig.api.feign.RemoteConvertService;
import com.pig4cloud.pig.api.vo.AdConvertReq;
import com.pig4cloud.pig.api.vo.GdtChannelPackageReq;
import com.pig4cloud.pig.api.vo.SelectConvertTrackReq;
import com.pig4cloud.pig.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lengleng
 * @date 2019/2/1
 */
@Slf4j
@Component
public class RemoteConvertServiceFallbackImpl implements RemoteConvertService {

	@Setter
	private Throwable cause;

	@Override
	public R create(ConvertTrack record, String from) {
		return R.failed(this.cause.getMessage());
	}

	@Override
	public R channelPackage(GdtChannelPackage record, String from) {
		return R.failed(this.cause.getMessage());
	}

	@Override
	public R<ConvertTrack> selectInfoByKey(SelectConvertTrackReq req, String from) {
		return R.failed(this.cause.getMessage());
	}

	@Override
	public R<List<ConvertTrack>> selectListByKey(SelectConvertTrackReq req, String from) {
		return R.failed(this.cause.getMessage());
	}

	@Override
	public R<AdAssetsTrack> createEventTrackUrl(AdAssetsTrack record, String from) {
		return R.failed(this.cause.getMessage());
	}

	@Override
	public R createGdtConversions(AdConvert record, String from) {
		return R.failed(this.cause.getMessage());
	}

	@Override
	public R<List<GdtChannelPackage>> getPackageList(GdtChannelPackageReq req, String from) {
		return R.failed(this.cause.getMessage());
	}
	@Override
	public R<List<AdConvert>> getAdConvertListInner(AdConvertReq req, String from) {
		return R.failed(this.cause.getMessage());
	}

	@Override
	public R<KsApp> createKsApp(KsApp ksApp, String from) {
		return null;
	}

	@Override
	public R<AdConvert> addOcpcTransFeed(AdConvertReq record, String from) {
		return null;
	}

	@Override
	public R extendPackageAdd(GdtChannelPackageReq record, String from) {
		return R.failed(this.cause.getMessage());
	}
}
