/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 广告账户表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
@Data
public class AdAccountResp {
	/**
	 * 主键
	 */
	private Integer id;
	/**
	 * 媒体编码
	 */
	private String mediaCode;
	/**
	 * 媒体名称
	 */
	private String mediaName;
	/**
	 * 广告账户ID
	 */
	private String advertiserId;
	/**
	 * 广告账户名称
	 */
	private String advertiserName;
	/**
	 * 投放人
	 */
	private String throwUser;
	/**
	 * 投放人名称
	 */
	private String throwUserName = "-";
	/**
	 * 代理商
	 */
	private String agentName = "-";
	/**
	 * 返点
	 */
	private Integer rebate = 0;

	/**
	 * 管家账户
	 */
	private String housekeeper;

	/**
	 * 消耗
	 */
	private BigDecimal cost = BigDecimal.ZERO;

	/**
	 * 可用总余额
	 */
	private BigDecimal validBalance = BigDecimal.ZERO;

	/**
	 * 可用现金总余额
	 */
	private BigDecimal validCash = BigDecimal.ZERO;

	/**
	 * 可用赠款总余额
	 */
	private BigDecimal validGrant = BigDecimal.ZERO;


	/**
	 * 是否为母账号
	 */
	private Integer isPaccount;

	/**
	 * 账号角色
	 */
	private Integer advertiserType;
}
