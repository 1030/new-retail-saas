package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.pig4cloud.pig.api.enums.ToutiaoTransactionTypeEnum;
import com.pig4cloud.pig.api.util.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

/**
 * 头条账号流水明细
 * tt_account_water
 * @author nml
 * @date 2021-07-14 14:49:54
 */
@Data
@NoArgsConstructor
@TableName(value="tt_account_water")
public class TtAccountWater extends Model<TtAccountWater> {
    /**
     * 主键
     */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 交易流水号
     */
	@ApiModelProperty(value = "交易流水号")
	@TableField(value = "transaction_seq")
    private Long transactionSeq;

    /**
     * 广告主ID
     */
	@ApiModelProperty(value = "广告主ID")
	@TableField(value = "advertiser_id")
    private Long advertiserId;

    /**
     * 流水类型，转账 TRANSFER、充值 RECHARGE
     */
	@ApiModelProperty(value = "流水类型，转账 TRANSFER、充值 RECHARGE")
	@TableField(value = "transaction_type")
    private String transactionType;

    private String transactionTypeName;

	public String getTransactionTypeName() {
		if (StringUtils.isBlank(transactionType)){
			return "未知";
		}
		return ToutiaoTransactionTypeEnum.valueByType(transactionType);
	}

	/**
     * 流水产生时间
     */
	@ApiModelProperty(value = "流水产生时间")
	@TableField(value = "create_time")
    private String createTime;

    /**
     * 交易总金额(单位元)
     */
	@ApiModelProperty(value = "交易总金额(单位元)")
	@TableField(value = "amount")
    private BigDecimal amount;

    /**
     * 现金总金额(单位元)
     */
	@ApiModelProperty(value = "现金总金额(单位元)")
	@TableField(value = "cash")
    private BigDecimal cash;

    /**
     * 赠款总金额(单位元）
     */
	@ApiModelProperty(value = "赠款总金额(单位元）")
	@TableField(value = "grant")
    private BigDecimal grant;

    /**
     * 返货总金额(单位元)
     */
	@ApiModelProperty(value = "返货总金额(单位元)")
	@TableField(value = "return_goods")
    private BigDecimal returnGoods;

    /**
     * 付款方，即广告主id
     */
	@ApiModelProperty(value = "付款方，即广告主id")
	@TableField(value = "remitter")
    private Long remitter;

    /**
     * 收款方，即广告主id
     */
	@ApiModelProperty(value = "收款方，即广告主id")
	@TableField(value = "payee")
    private Long payee;

    /**
     * 返点
     */
	@ApiModelProperty(value = "返点")
	@TableField(value = "dealbase")
    private BigDecimal dealbase;

}