package com.pig4cloud.pig.api.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AllCustomAudience implements Serializable {

	private Long id;
	private Long customAudienceId;
	private String accountId;
	private Integer isdel;
	private String dataSourceId;
	private String name;
	private String source;
	private Integer status;
	private String deliveryStatus;
	private Long coverNum;
	private Long uploadNum;
	private String tag;
	private Integer pushStatus;
	private Date createtime;
	private Date updatetime;
	private String ttCreateTime;
	private String modifyTime;
	private String thirdPartyInfo;
	/**
	 * 媒体类型  1头条 8广点通
	 */
	private Integer mediaType;

	/**
	 * 广告主对人群在自己系统里的编码
	 */
	private String externalAudienceId;
	/**
	 * 人群描述
	 */
	private String description;

	/**
	 * 人群错误码, 1 表示系统错误； 101 表示种子人群活跃用户低于 2K ； 102 表示种子人群无共同特征； 201 表示人群上传的号码包文件格式错误； 202 表示解析人群上传的号码包文件失败； 203 表示号码包文件人群匹配失败。
	 */
	private Integer errorCode;

	/**
	 * 人群类型
	 * 号码文件人群=CUSTOMER_FILE，
	 * 拓展人群=LOOKALIKE，
	 * 用户行为人群=USER_ACTION，
	 * 地理位置人群=LBS，
	 * 关键词人群=KEYWORD，
	 * 广告人群=AD，
	 * 组合人群=COMBINE，
	 * 标签人群=LABEL
	 */
	private String gdtType;

	/**
	 * 处理状态
	 * 待处理=PENDING，
	 * 处理中=PROCESSING，
	 * 成功可用=SUCCESS，
	 * 错误=ERROR
	 */
	private String gdtStatus;

	private String typeName;
	private String sourceName;
	private String statusName;
	private String accountName;

	//返点后消耗
	private Double cost = 0.00;
	//曝光量
	private Long showNum = 0L;
	//点击人数
	private Long clickNum = 0L;
	//点击率 = 点击人数/曝光量
	private Double clickRate = 0.00;
	//新增设备注册数
	private Long usrNameNum = 0L;
	//注册成本 = 返点后消耗/新增设备注册数
	private Double registerCost = 0.00;
	// 新增设备付费数
	private Long newUuidNum = 0L;
	// 付费率 = 新增设备付费数/新增设备注册数
	private Double costRate = 0.00;
	// 新增设备在次日有登录行为的设备数
	private Long retention2 = 0L;
	//次留 = 新增设备在次日有登录行为的设备数/新增设备注册数
	private Double nextDayRate = 0.00;




}
