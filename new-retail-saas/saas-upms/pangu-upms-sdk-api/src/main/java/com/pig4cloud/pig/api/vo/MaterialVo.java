package com.pig4cloud.pig.api.vo;

import lombok.Data;

/**
 * 此类专用于计划统计分析，素材信息：包括图片+视频
 */
@Data
public class MaterialVo {
	//素材唯一标识,第三方素材id
	private String mId;
	//素材名称
	private String mName;
	//素材封面图
	private String smallPicUrl;
	//素材资源地址
	private String fileUrl;
	//素材类型 1为视频，2为图片
	private Integer type;
	//素材宽度
	private String width;
	//素材高度
	private String height;
}
