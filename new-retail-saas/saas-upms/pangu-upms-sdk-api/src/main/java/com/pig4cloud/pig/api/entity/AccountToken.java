package com.pig4cloud.pig.api.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class AccountToken implements Serializable {
    private static final long serialVersionUID = 6618752925976815322L;

	private String advertiserId;

	private String advertiserName;

	private String housekeeper;

	private String accessToken;

	private BigDecimal balance;
}
