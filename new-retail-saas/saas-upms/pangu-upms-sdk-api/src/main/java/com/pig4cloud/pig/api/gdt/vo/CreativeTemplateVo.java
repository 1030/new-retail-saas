package com.pig4cloud.pig.api.gdt.vo;

import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;


/**
 * @广告创意形式vo
 * @author hma
 *
 */
@Getter
@Setter
public class CreativeTemplateVo {
	/**
	 * 推广目标类型
	 */
	private String promotedObjectType;
	/**
	 * 广告版本
	 */
	private String siteSets;
	private String siteSet;
	/**
	 * 创意形式id
	 */
	private Long adcreativeTemplateId;
	/**
	 * 		创意形式名称
	 */
	private String adcreativeTemplateAppellation;
	/**
	 * 创意形式类型
	 */
	private String adcreativeTemplateStyle;
	/**
	 * 广告属性信息
	 */
	private List<Map<String,Object>> adAttributes;
	/**
	 * 	广告创意属性信息
	 */
	private List<Map<String,Object>>  adcreativeAttributes;
	/**
	 * 	广告创意元素列表
	 */
	private List<Map<String,Object>>  adcreativeElements;
	/**
	 * 	创意规格示意图数组
	 */
	private List<Map<String,Object>> adcreativeSampleImageList;

	private String[]  supportBidModeList;

	public String getSiteSets(){
		return StringUtils.isNotBlank(siteSets)? siteSets = siteSets.replaceAll("&quot;","\""):siteSets;
	}

}
