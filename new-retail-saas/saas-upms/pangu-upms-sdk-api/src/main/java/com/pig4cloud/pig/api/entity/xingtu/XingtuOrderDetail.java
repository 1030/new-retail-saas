package com.pig4cloud.pig.api.entity.xingtu;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 星图任务订单明细表
 * @author  chengang
 * @version  2021-12-13 16:48:32
 * table: xingtu_order_detail
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "xingtu_order_detail")
public class XingtuOrderDetail extends Model<XingtuOrderDetail>{

	//columns START
			//主键id
			@TableId(value = "id",type = IdType.AUTO)
			private Long id;
			

			//星图广告账户ID
			@TableField(value = "advertiser_id")
			private String advertiserId;
			

			//任务ID
			@TableField(value = "demand_id")
			private String demandId;
			

			//订单id
			@TableField(value = "order_id")
			private String orderId;
			

			//热词top10
			@TableField(value = "high_frequency_words")
			private String highFrequencyWords;
			

			//负向评论率（neg_rate/100）%
			@TableField(value = "neg_rate")
			private String negRate;
			

			//中立评论率（neu_rate/100）%
			@TableField(value = "neu_rate")
			private String neuRate;
			

			//正向评论率（pos_rate/100）%
			@TableField(value = "pos_rate")
			private String posRate;
			

			//组件点击量
			@TableField(value = "convert_click")
			private String convertClick;
			

			//组件点击率 (ctr/100)%
			@TableField(value = "convert_ctr")
			private String convertCtr;
			

			//组件展示量
			@TableField(value = "convert_show")
			private String convertShow;
			

			//千次播放成本(分)
			@TableField(value = "cpm")
			private String cpm;
			

			//播放次数
			@TableField(value = "play")
			private String play;
			

			//订单金额(分)
			@TableField(value = "price")
			private String price;
			

			//完播率 (finish_rate/100)%
			@TableField(value = "finish_rate")
			private String finishRate;
			

			//有效播放率（播放5s以上记为有效播放）(five_s_play_rate/100)%
			@TableField(value = "five_s_play_rate")
			private String fiveSPlayRate;

			//平均播放率（=用户观看该任务视频的平均观看时长/视频总时长）(play_rate/100)%
			@TableField(value = "play_rate")
			private String playRate;

			//播放次数 x 完播率 = 完成播放次数
			@TableField(value = "finish_number")
			private String finishNumber;

			//播放次数 x 有效播放率 = 有效播放数
			@TableField(value = "five_s_play_number")
			private String fiveSPlayNumber;

			//播放次数 x 平均播放率 = 平均播放数
			@TableField(value = "play_number")
			private String playNumber;
			

			//评论量
			@TableField(value = "spread_comment")
			private String spreadComment;
			

			//点赞量
			@TableField(value = "spread_like")
			private String spreadLike;
			

			//播放量
			@TableField(value = "spread_play")
			private String spreadPlay;
			

			//分享量
			@TableField(value = "spread_share")
			private String spreadShare;
			

			//数据更新时间，格式%Y-%m-%d %H:%M:%S
			@TableField(value = "order_update_time")
			private String orderUpdateTime;
			

			//是否删除  0否 1是
			@TableField(value = "deleted")
			private Integer deleted;
			

			//创建时间
			@TableField(value = "create_time")
			private Date createTime;
			

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;
			

			//创建人
			@TableField(value = "create_id")
			private Long createId;
			

			//修改人
			@TableField(value = "update_id")
			private Long updateId;
			

	//columns END 数据库字段结束
	

	
}

	

	
	

