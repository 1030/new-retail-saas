package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public enum ToutiaoActionInterestEnum {

	ACTIONCATEGORIES(1  , "行为类目"     ),
	INTERESTCATEGORIES(2        , "兴趣类目"     ),
	ACTIONWORDS(3        , "行为关键词"     ),
	INTERESTWORDS(4        , "兴趣关键词"  );



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private Integer type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoActionInterestEnum(Integer type, String name){
		this.type = type;
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static Integer typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoActionInterestEnum item : ToutiaoActionInterestEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(Integer type){
		if (Objects.isNull(type)){
			return null;
		}

		for (ToutiaoActionInterestEnum item : ToutiaoActionInterestEnum.values()) {
			if (item.getType() == type) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(Integer type){
		if (Objects.isNull(type)){
			return false;
		}

		for (ToutiaoActionInterestEnum item : ToutiaoActionInterestEnum.values()) {
			if (item.getType() == type) {
				return true;
			}
		}
		return false;
	}

}
