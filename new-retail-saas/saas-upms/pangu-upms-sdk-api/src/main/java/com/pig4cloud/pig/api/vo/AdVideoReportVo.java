package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdVideoReportVo.java
 * @createTime 2021年01月18日 15:31:00
 */
@Data
public class AdVideoReportVo extends Page {
	/**
	 * 开始日期
	 */
	@NotBlank(message = "开始时间不能为空")
	private String sTime;

	/**
	 * 结束日期
	 */
	@NotBlank(message = "结束时间不能为空")
	private String eTime;
	/**
	 * 视频ID
	 */
	private Integer id;
	/**
	 * 平台ID
	 */
	private Integer platformId;
	/**
	 * 制作人ID
	 */
	private Integer makeId;
	/**
	 * 视频名称
	 */
	private String videoName;
	/**
	 * 广告列表
	 */
	private List<String> adList;
	/**
	 * 头条广告账户列表
	 */
	private List<String> ttAdList;
	/**
	 * 广点通广告账户列表
	 */
	private List<String> gdtAdList;
	/**
	 * 创建人
	 */
	private String createUser;

	private String labelname;//标签名称

}
