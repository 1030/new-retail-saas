package com.pig4cloud.pig.api.dto;

import lombok.Data;

@Data
public class TtImage {
	private String imageMode  ;
	private String imageId    ;
	private String videoId    ;
	private String[] imageIds   ;
	private Long[] templateIds;
}
