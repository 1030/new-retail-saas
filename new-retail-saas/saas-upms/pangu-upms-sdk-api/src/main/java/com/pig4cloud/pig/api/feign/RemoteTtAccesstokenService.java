package com.pig4cloud.pig.api.feign;

import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(contextId = "remoteTtAccesstokenService", value = ServiceNameConstants.UPMS_SDK_SERVICE)
public interface RemoteTtAccesstokenService {

	@PostMapping("/select_list/getToken")
	R fetchAccesstoken(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody String adAccount);
}
