package com.pig4cloud.pig.api.entity.bd;


import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 快手广告计划状态,预算,出价修改
 *
 * @author leisw
 * @date 2022-08-08 10:31:08
 */
@Data
@ApiModel(value = "快手修改预算,出价修改")
public class BdAdgroupReq implements Serializable {

	/**
	 * 广告账户ID
	 */
	private Long advertiserId;

	/**
	 * 广告组id(广告计划id)
	 */
	private Long adgroupFeedId;

	/**
	 * 1 修改日预算  2 定时修改日预算
	 */
	private Integer type;

	/**
	 * 日预算金额：50<=X <= 100000000、仅支持最多2位小数据
	 */
	private BigDecimal budget;

	/**
	 * 目标转换出价
	 */
	private Double cpaBid;


}
