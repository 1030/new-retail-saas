/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.entity;

import java.math.BigInteger;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @用户账号绑定表
 * @author john
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_user_adver")
public class AdUserAdver extends Model<AdUserAdver> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键id")
	private BigInteger id;

	/**
	 * 用户ID
	 */
	@TableField(value = "user_id")
	@ApiModelProperty(value = "用户id")
	private Integer userId;

	
	@ApiModelProperty(value = "广告账户id")
	@TableField(value = "advertiser_id")
	private String advertiserId;
	
	@ApiModelProperty(value = "平台id")
	@TableField(value = "platform_id")
	private String platformId;
	
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField( fill = FieldFill.INSERT)
	private Date createtime;
	
	/**
	 * 批准人
	 */
	@ApiModelProperty(value = "批准人")
	private Integer createuser;
	
	
	/**
	 * 批准时间
	 */
	@ApiModelProperty(value = "批准时间")
	@TableField( fill = FieldFill.UPDATE)
	private Date updatetime;
	
	/**
	 * 修改人
	 */
	@ApiModelProperty(value = "修改人")
	private Integer updateuser;
	
	
	/**
	 * 状态0-正常，1-锁定
	 */
	@ApiModelProperty(value = "状态0-正常，1-锁定")
	private Integer status;
	
	@ApiModelProperty(value = "添加类型  1 加挂管家，  2申请加挂")
	@TableField(value = "auth_type")
	private Integer authType;

}
