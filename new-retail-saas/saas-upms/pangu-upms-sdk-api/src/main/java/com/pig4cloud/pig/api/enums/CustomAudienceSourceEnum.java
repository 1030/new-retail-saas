/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;


@Getter
@RequiredArgsConstructor
public enum CustomAudienceSourceEnum {
	CUSTOM_AUDIENCE_TYPE_UPLOAD("CUSTOM_AUDIENCE_TYPE_UPLOAD",  0,"上传"),
	CUSTOM_AUDIENCE_TYPE_EXTEND("CUSTOM_AUDIENCE_TYPE_EXTEND",  1,"扩展"),
	CUSTOM_AUDIENCE_TYPE_OPERATE("CUSTOM_AUDIENCE_TYPE_OPERATE",  2,"运算"),
	CUSTOM_AUDIENCE_TYPE_RULE("CUSTOM_AUDIENCE_TYPE_RULE",  3,"规则"),
	CUSTOM_AUDIENCE_TYPE_DATA_SOURCE("CUSTOM_AUDIENCE_TYPE_DATA_SOURCE",  4,"文件数据源上传"),
	CUSTOM_AUDIENCE_TYPE_THIRD_PARTY("CUSTOM_AUDIENCE_TYPE_THIRD_PARTY",  5,"第三方数据规则"),
	CUSTOM_AUDIENCE_TYPE_BRAND("CUSTOM_AUDIENCE_TYPE_BRAND",  6,"品牌DMP规则包"),
	CUSTOM_AUDIENCE_TYPE_FRIEND("CUSTOM_AUDIENCE_TYPE_FRIEND",  7,"好友扩展"),
	CUSTOM_AUDIENCE_TYPE_THEME("CUSTOM_AUDIENCE_TYPE_THEME",  8,"运营主题"),
	CUSTOM_AUDIENCE_TYPE_FINANCE("CUSTOM_AUDIENCE_TYPE_FINANCE",  9,"金融数据"),
	CUSTOM_AUDIENCE_TYPE_PACK_RULE("CUSTOM_AUDIENCE_TYPE_PACK_RULE",  10,"pack_rule运算"),
	CUSTOM_AUDIENCE_TYPE_ONE_KEY("CUSTOM_AUDIENCE_TYPE_ONE_KEY",  11,"一键拓展"),
	CUSTOM_AUDIENCE_TYPE_DOU_PLUS("CUSTOM_AUDIENCE_TYPE_DOU_PLUS",  12,"抖+粉丝合并包");


	private final String key;
	private final Integer value;
	private final String name;

	public static String getNameByKey(String key){
		if (StringUtils.isBlank(key)){
			return null;
		}
		for (CustomAudienceSourceEnum item : CustomAudienceSourceEnum.values()) {
			if (key.equals(item.getKey())) {
				return item.getName();
			}
		}
		return null;

	}

}
