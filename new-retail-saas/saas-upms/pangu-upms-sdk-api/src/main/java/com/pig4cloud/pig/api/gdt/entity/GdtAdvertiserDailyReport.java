package com.pig4cloud.pig.api.gdt.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;


/**
 * 广点通广告组级别报表日流水
 * @author yk
 *
 */
@Getter
@Setter
public class GdtAdvertiserDailyReport implements Serializable {
	/**
	 * 广告账户id
	 */
	private Integer accountId;
	/**
	 * 日期，日期格式：yyyy-mm-dd
	 */
	private String date;
	/**
	 * 点击
	 */
	private Integer validClickCount;
	/**
	 * 曝光量(展示量)
	 */
	private Integer viewCount;
	/**
	 * app下载完成量
	 */
	private Integer downloadCount;

	/**
	 * app激活总量
	 */
	private Integer activatedCount;

	/**
	 * app下载激活率
	 */
	private BigDecimal activatedRate;

	/**
	 * 点击均价
	 */
	private BigDecimal cpc;
	/**
	 * 消耗
	 */
	private BigDecimal cost;

	/**
	 * 注册量（app）
	 */
	private Integer appRegisterCount;

	/**
	 * 注册成本（app）
	 */
	private BigDecimal appRegisterCost;

	/**
	 * 注册率（app）
	 */
	private BigDecimal appRegisterRate;

	/**
	 * app激活成本
	 */
	private BigDecimal activatedCost;

	/**
	 * 点击率
	 */
	private BigDecimal ctr;

	/**
	 * app点击激活率(激活量/点击量*100%)
	 */
	private BigDecimal clickActivatedRate;

	/**
	 * 下单量（app）
	 */
	private Integer appOrderCount;
	/**
	 * 下单成本（app）
	 */
	private BigDecimal appOrderCost;
	/**
	 * 付费行为量（app）
	 */
	private Integer appCheckoutCount;
	/**
	 * 付费金额（app）
	 */
	private BigDecimal appCheckoutAmount;
	/**
	 * 付费行为成本（app）
	 */
	private BigDecimal appCheckoutCost;

	/**
	 * 目标转化量
	 */
	private BigDecimal conversionsCount;

	/**
	 * 转化率
	 */
	private BigDecimal conversionsRate;

	/**
	 * 转化成本
	 */
	private BigDecimal conversionsCost;


	/**
	 * 版本号，查询最大的记录
	 */
	private long iver;





	public void setActivatedRate(BigDecimal activatedRate) {
		this.activatedRate = activatedRate.multiply(new BigDecimal(100));
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost.divide(new BigDecimal(100));
	}

	public void setCtr(BigDecimal ctr) {
		this.ctr = ctr.multiply(new BigDecimal(100));
	}


	public void setCpc(BigDecimal cpc) {
		this.cpc = cpc.divide(new BigDecimal(100));
	}



	public void setAppRegisterCost(BigDecimal appRegisterCost) {
		this.appRegisterCost = appRegisterCost.divide(new BigDecimal(100));
	}


	public void setAppRegisterRate(BigDecimal appRegisterRate) {
		this.appRegisterRate = appRegisterRate.multiply(new BigDecimal(100));
	}



	public void setActivatedCost(BigDecimal activatedCost) {
		this.activatedCost = activatedCost.divide(new BigDecimal(100));
	}

	public void setClickActivatedRate(BigDecimal clickActivatedRate) {
		this.clickActivatedRate = clickActivatedRate.multiply(new BigDecimal(100));
	}


	public void setAppOrderCost(BigDecimal appOrderCost) {
		this.appOrderCost = appOrderCost.divide(new BigDecimal(100));
	}


	public void setAppCheckoutAmount(BigDecimal appCheckoutAmount) {
		this.appCheckoutAmount = appCheckoutAmount.divide(new BigDecimal(100));
	}
	public void setAppCheckoutCost(BigDecimal appCheckoutCost) {
		this.appCheckoutCost = appCheckoutCost.divide(new BigDecimal(100));
	}


	public void setConversionsRate(BigDecimal conversionsRate) {
		this.conversionsRate =conversionsRate.multiply(new BigDecimal(100));
	}

	public void setConversionsCost(BigDecimal conversionsCost) {
		this.conversionsCost =conversionsCost.divide(new BigDecimal(100));
	}

}