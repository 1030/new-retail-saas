package com.pig4cloud.pig.api.gdt.dto;

import lombok.Data;

import java.util.List;

@Data
public class GdtResultData<T> {
	private List<T> list;
	private GdtResultPageInfo page_info;
}
