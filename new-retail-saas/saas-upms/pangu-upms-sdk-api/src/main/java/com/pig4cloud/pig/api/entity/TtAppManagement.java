package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName TtAppManagement.java
 * @createTime 2021年08月04日 16:39:00
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="tt_app")
public class TtAppManagement extends Model<TtAppManagement> {
	/**
	 * 自增的 转化ID
	 */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/**
	 * 广告账户ID
	 */
	private String advertiserId;
	/**
	 * 应用包ID
	 */
	private String packageId;
	/**
	 * 包名
	 */
	private String packageName;
	/**
	 * appId
	 */
	private String appCloudId;
	/**
	 * 类型
	 */
	private String searchType;
	/**
	 * 应用名
	 */
	private String appName;
	/**
	 * 版本号
	 */
	private String version;
	/**
	 * 下载地址
	 */
	private String downloadUrl;
	/**
	 * icon地址
	 */
	private String iconUrl;
	/**
	 * 发布时间
	 */
	private Date publishTime;
	/**
	 * 是否删除  0否 1是
	 */
	private Integer isDeleted;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 创建人
	 */
	private String createId;
	/**
	 * 修改人
	 */
	private String updateId;
}
