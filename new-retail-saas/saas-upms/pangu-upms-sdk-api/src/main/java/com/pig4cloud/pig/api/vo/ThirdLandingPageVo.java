package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @第三方落地页
 * @author yk
 *
 */
@Getter
@Setter
public class ThirdLandingPageVo extends Page {

	private Integer id;

	/**
	 * 落地页缩略图
	 */
	private String pageUrl;

	/**
	 * 落地页标题
	 */
	private String pageName;

	/**
	 * 落地页ID
	 */
	private String pageId;

	/**
	 * 状态
	 */
	private String auditStatus;

	/**
	 * 创建人
	 */
	private String createUser;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 落地页链接
	 */
	private String url;

	/**
	 * 广告主id
	 */
	private String advertiserId;

	/**
	 * 多个广告主id
	 */
	private String[] advertiserIds;
}
