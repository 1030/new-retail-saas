package com.pig4cloud.pig.api.feign.fallback;

import com.pig4cloud.pig.api.feign.RemoteTtAccesstokenService;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @author ：lile
 * @date ：2021/8/5 16:36
 * @description：
 * @modified By：
 */
public class RemoteTtAccesstokenServiceFallbackImpl implements RemoteTtAccesstokenService {

	@Override
	public R fetchAccesstoken(String from, String adAccount) {
		return R.ok();
	}
}
