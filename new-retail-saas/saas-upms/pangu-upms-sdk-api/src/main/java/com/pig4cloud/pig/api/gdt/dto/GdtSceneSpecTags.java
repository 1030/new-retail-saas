package com.pig4cloud.pig.api.gdt.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Set;
import java.util.TreeSet;

/**
 * 场景描述定向标签
 */
@Data
public class GdtSceneSpecTags implements Serializable{
	/**
	 *  唯一标识ID
	 */
	private Integer id;
	/**
	 *  父唯一标识ID
	 */
	private Integer parent_id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 描述
	 */
    private String description;
	/**
	 * 定向名称
	 */
	private String targeting_name;
}
