package com.pig4cloud.pig.api.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.api.dto
 * @Author 马嘉祺
 * @Date 2021/7/10 9:27
 * @Description
 */
@Data
public class AdCommentStatisticsDTO {

	private Integer offset;

	private Integer pageSize;

	/**
	 * 广告账户ID集合
	 */
	private Collection<String> advertiserIds;

	/**
	 * 子游戏ID集合
	 */
	private Collection<Long> gameIds;

	/**
	 * 评论创建日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd", iso = DateTimeFormat.ISO.DATE)
	private Date createDay;

	/**
	 * 主游戏ID
	 */
	private Long pgid;

	/**
	 * 主游戏名称
	 */
	private String pgname;

	/**
	 * 子游戏ID
	 */
	private Long gameid;

	/**
	 * 子游戏名称
	 */
	private String gname;

	/**
	 * 平台类型，1：头条，8：广点通；
	 */
	private Integer platformId;

	/**
	 * 广告计划id
	 */
	private Long adId;

	/**
	 * 广告计划名
	 */
	private String adName;

	/**
	 * 评论所属创意id
	 */
	private Long creativeId;

	/**
	 * 评论所属创意标题
	 */
	private String creativeTitle;

	/**
	 * 素材素材ID
	 */
	private Long materialId;

	/**
	 * 素材名称
	 */
	private String materialName;

	/**
	 * 视频链接地址
	 */
	private String videoUrl;

	/**
	 * 图片连接地址
	 */
	private String imageUrl;

	/**
	 * 评论总数
	 */
	private Integer commentCount;

	/**
	 * 包含屏蔽词的评论总数
	 */
	private Integer termsBannedCount;

	/**
	 * 包含屏蔽词的评论占比
	 */
	private BigDecimal termsBannedRatio;

	/**
	 * 点赞数
	 */
	private Integer likeCount;

	/**
	 * 开始创建时间
	 */
	private String startTimeStr;

	/**
	 * 结束创建时间
	 */
	private String endTimeStr;

	/**
	 * 模糊查询时的评论内容
	 */
	private String commentBlur;

	/**
	 * 汇总方式：0：汇总；1：按天
	 */
	private Integer summaryType;

	/**
	 * 分组字段，可选值：pgname，gname，adId，materialName
	 */
	private Collection<String> groupBys;

	public AdCommentStatisticsDTO setOffset(Integer offset) {
		this.offset = offset;
		return this;
	}

	public AdCommentStatisticsDTO setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
		return this;
	}

	public AdCommentStatisticsDTO setAdvertiserIds(Collection<String> advertiserIds) {
		this.advertiserIds = advertiserIds;
		return this;
	}

	public AdCommentStatisticsDTO setGameIds(Collection<Long> gameIds) {
		this.gameIds = gameIds;
		return this;
	}

	public AdCommentStatisticsDTO setCreateDay(Date createDay) {
		this.createDay = createDay;
		return this;
	}

	public AdCommentStatisticsDTO setPgid(Long pgid) {
		this.pgid = pgid;
		return this;
	}

	public AdCommentStatisticsDTO setPgname(String pgname) {
		this.pgname = pgname;
		return this;
	}

	public AdCommentStatisticsDTO setGameid(Long gameid) {
		this.gameid = gameid;
		return this;
	}

	public AdCommentStatisticsDTO setGname(String gname) {
		this.gname = gname;
		return this;
	}

	public AdCommentStatisticsDTO setPlatformId(Integer platformId) {
		this.platformId = platformId;
		return this;
	}

	public AdCommentStatisticsDTO setAdId(Long adId) {
		this.adId = adId;
		return this;
	}

	public AdCommentStatisticsDTO setAdName(String adName) {
		this.adName = adName;
		return this;
	}

	public AdCommentStatisticsDTO setCreativeId(Long creativeId) {
		this.creativeId = creativeId;
		return this;
	}

	public AdCommentStatisticsDTO setCreativeTitle(String creativeTitle) {
		this.creativeTitle = creativeTitle;
		return this;
	}

	public AdCommentStatisticsDTO setMaterialId(Long materialId) {
		this.materialId = materialId;
		return this;
	}

	public AdCommentStatisticsDTO setMaterialName(String materialName) {
		this.materialName = materialName;
		return this;
	}

	public AdCommentStatisticsDTO setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
		return this;
	}

	public AdCommentStatisticsDTO setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
		return this;
	}

	public AdCommentStatisticsDTO setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
		return this;
	}

	public AdCommentStatisticsDTO setTermsBannedCount(Integer termsBannedCount) {
		this.termsBannedCount = termsBannedCount;
		return this;
	}

	public AdCommentStatisticsDTO setTermsBannedRatio(BigDecimal termsBannedRatio) {
		this.termsBannedRatio = termsBannedRatio;
		return this;
	}

	public AdCommentStatisticsDTO setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
		return this;
	}

	public AdCommentStatisticsDTO setStartTimeStr(String startTimeStr) {
		this.startTimeStr = startTimeStr;
		return this;
	}

	public AdCommentStatisticsDTO setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
		return this;
	}

	public AdCommentStatisticsDTO setCommentBlur(String commentBlur) {
		this.commentBlur = commentBlur;
		return this;
	}

	public AdCommentStatisticsDTO setSummaryType(Integer summaryType) {
		this.summaryType = summaryType;
		return this;
	}

	public AdCommentStatisticsDTO setGroupBys(Collection<String> groupBys) {
		this.groupBys = groupBys;
		return this;
	}
}
