package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName AdVideo.java
 * @author cx
 * @version 1.0.0
 * @Desc TODO
 * @Time 2020/11/5 13:38
 */
@Data
public class AdVideoVo extends Page {
	/**
	 * 主键ID
	 */
	private String id;
	/**
	 * 视频名称
	 */
	private String vName;

	/**
	 * 文件名称
	 */
	private String fileName;

	/**
	 * 文件访问地址
	 */
	private String fileUrl;
	/**
	 * 文件存放真实路径
	 */
	private String realPath;
	/**
	 * 图片地址
	 */
	private String imageUrl;
	/**
	 * md5
	 */
	private String md5;

	/**
	 * 视频宽
	 */
	private String width;

	/**
	 * 视频高
	 */
	private String height;

	/**
	 * 视频大小
	 */
	private String vSize;

	/**
	 * 视频格式
	 */
	private String format;

	/**
	 * 视频时长
	 */
	private String duration;
	/**
	 * 视频时长min
	 */
	private String minDuration;
	/**
	 * 视频时长max
	 */
	private String maxDuration;

	/**
	 * 项目ID
	 */
	private Integer projectId;

	/**
	 * 是否删除：0正常，1删除
	 */
	private Integer isdelete;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建时间
	 */
	private Date createtime;

	/**
	 * 创建人
	 */
	private String createuser;
	/**
	 * 创建人名称
	 */
	private String createUserName;

	/**
	 * 创建人id
	 */
	private String userId;

	/**
	 * 修改时间
	 */
	private Date updatetime;

	/**
	 * 修改人
	 */
	private String updateuser;

	/**
	 * 1横屏，2竖屏
	 */
	private Integer screenType;
	/**
	 * 主键IDS
	 */
	private String ids;
	/**
	 * 平台类型1，头条，2广点通
	 */
	private Integer platformType;
	//是否已经同步头条  1是
	private Integer ttSyn;
	//是否已经同步广点通  1是
	private Integer gdtSyn;
	//是否只展示同步的视频。1是
	private Integer isSyn;
	//广告账户
	private String advertiserId;
	//广告账户
	private String advertiserIds;
	//广告列表
	private List<String> adList;
	//头条广告账户列表
	private List<String> ttAdList;
	//广点通广告账户列表
	private List<String> gdtAdList;
	//已经推送头条的账户
	private String ttSynAds;
	//已经推送广点通的账户
	private String gdtSynAds;
	//视频同步广告账户信息列表-------头条
	private List<Map<String,Object>> pushTtAdStatusList;
	//视频同步广告账户信息列表-------广点通
	private List<Map<String,Object>> pushGdtAdStatusList;
	private String labelname;



}