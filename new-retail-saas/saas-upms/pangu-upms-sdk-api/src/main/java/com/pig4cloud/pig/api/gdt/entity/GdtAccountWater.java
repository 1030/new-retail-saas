package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.pig4cloud.pig.api.enums.GdtTradeTypeExtEnum;
import com.pig4cloud.pig.api.util.StringUtils;
import com.pig4cloud.pig.common.core.util.BigDecimalUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * 广点通账号流水明细
 * gdt_account_water
 * @author nml
 * @date 2021-07-15 14:17:45
 */
@Data
@NoArgsConstructor
@TableName(value="gdt_account_water")
public class GdtAccountWater extends Model<GdtAccountWater> {
    /**
     * 主键
     */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 调用方订单号
     */
	@ApiModelProperty(value = "调用方订单号")
	@TableField(value = "external_bill_no")
    private String externalBillNo;

    /**
     * 推广帐号 id
     */
	@ApiModelProperty(value = "推广帐号 id")
	@TableField(value = "account_id")
    private Long accountId;

    /**
     * 资金账户类型:FUND_TYPE_CASH 现金账户;FUND_TYPE_GIFT 赠送账户;FUND_TYPE_SHARED 分成账户;
     */
	@ApiModelProperty(value = "资金账户类型:FUND_TYPE_CASH 现金账户;FUND_TYPE_GIFT 赠送账户;FUND_TYPE_SHARED 分成账户;")
	@TableField(value = "fund_type")
    private String fundType;

    /**
     * 交易类型：CHARGE 充值;TRANSFER_IN 转入;PAY 消费;TRANSFER_BACK 回划;EXPIRE 过期;
     */
	@ApiModelProperty(value = "交易类型：CHARGE 充值;TRANSFER_IN 转入;PAY 消费;TRANSFER_BACK 回划;EXPIRE 过期;")
	@TableField(value = "trade_type_ext")
    private String tradeTypeExt;

	private String tradeTypeExtName;

	public String getTradeTypeExtName() {
		if (StringUtils.isBlank(tradeTypeExt)){
			return "未知";
		}
		return GdtTradeTypeExtEnum.valueByType(tradeTypeExt);
	}

	/**
     * 余额，单位为分
     */
	@ApiModelProperty(value = "余额，单位为分")
	@TableField(value = "balance")
    private Long balance;

    private BigDecimal balanceBD;

	public BigDecimal getBalanceBD() {
		if (Objects.isNull(balance) || balance.equals(0L)){
			return BigDecimal.ZERO;
		}
		return BigDecimalUtil.divide(balance,100L);
	}

	/**
     * 金额，单位为分
     */
	@ApiModelProperty(value = "金额，单位为分")
	@TableField(value = "amount")
    private Long amount;

	private BigDecimal amountBD;

	public BigDecimal getAmountBD() {
		if (Objects.isNull(amount) || amount.equals(0L)){
			return BigDecimal.ZERO;
		}
		return BigDecimalUtil.divide(amount,100L);
	}

	/**
     * 交易时间（时间戳）
     */
	@ApiModelProperty(value = "交易时间（时间戳）")
	@TableField(value = "time")
    private Long time;

	private transient String timeStr;

    /**
     * 描述信息
     */
	@ApiModelProperty(value = "描述信息")
	@TableField(value = "description")
    private String description;

}