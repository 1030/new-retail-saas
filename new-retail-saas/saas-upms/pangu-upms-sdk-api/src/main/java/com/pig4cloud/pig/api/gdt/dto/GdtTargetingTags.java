package com.pig4cloud.pig.api.gdt.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Set;
import java.util.TreeSet;

import lombok.Data;

@Data
public class GdtTargetingTags implements Serializable, Comparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1682833058412509865L;
	
	private BigInteger id;
	private String name;
    private BigInteger parent_id;
    private String parent_name;
    private String city_level;
    private boolean business = false;
    
    private Set<GdtTargetingTags> children = new TreeSet<>();
	
	
	@Override
	public int compareTo(Object o) {
	    if (!(o instanceof GdtTargetingTags))
	        throw new RuntimeException("不是GdtTargetingTags对象");
	    GdtTargetingTags p = (GdtTargetingTags) o;
	    
	    return this.id.compareTo(p.id);
	}

	public Set<GdtTargetingTags> getChildren() {
		if (children == null){
			children = new TreeSet<>();
		}
		return children;
	}
}
