package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 落地页表
 * @author  chenxiang
 * @version  2022-03-18 10:59:34
 * table: ad_landing_page
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_landing_page")
public class AdLandingPage extends Model<AdLandingPage>{
	/**
	 * 主键id
	 */
	@TableField(value = "id")
	private Long id;
	/**
	 * 平台ID（1:头条,8:广点通,10:快手）
	 */
	@TableField(value = "platform_id")
	private Integer platformId;
	/**
	 * 广告账户
	 */
	@TableField(value = "advertiser_id")
	private String advertiserId;
	/**
	 * 落地页名称
	 */
	@TableField(value = "page_name")
	private String pageName;
	/**
	 * 落地页ID
	 */
	@TableField(value = "page_id")
	private String pageId;
	/**
	 * 落地页缩略图
	 */
	@TableField(value = "page_url")
	private String pageUrl;
	/**
	 * 发布状态
	 */
	@TableField(value = "publish_status")
	private String publishStatus;
	/**
	 * 审核状态
	 */
	@TableField(value = "status")
	private String status;
	/**
	 * 落地页包含的组件
	 */
	@TableField(value = "comps")
	private String comps;
	/**
	 * 落地页绑定的小说 ID
	 */
	@TableField(value = "fiction_id")
	private Long fictionId;
	/**
	 * 落地页服务 id，主要用于广告投放、落地页送审及删除
	 */
	@TableField(value = "page_service_id")
	private String pageServiceId;
	/**
	 * 推广目标 id
	 */
	@TableField(value = "promoted_object_id")
	private String promotedObjectId;
	/**
	 * 商品库 id
	 */
	@TableField(value = "product_catalog_id")
	private Integer productCatalogId;
	/**
	 * 落地页类型：TT_ORANGE橙子落地页，TT_THIRD三方落地页，TT_GROUP程序化落地页，TT_CUSTOM自定义落地页
	 */
	@TableField(value = "page_type")
	private String pageType;
	/**
	 * 蹊径落地页试玩类型
	 */
	@TableField(value = "playable_type")
	private String playableType;
	/**
	 * 状态，状态定义
	 */
	@TableField(value = "page_status")
	private String pageStatus;
	/**
	 * 枫页落地页子类型
	 */
	@TableField(value = "fengye_sub_type")
	private String fengyeSubType;
	/**
	 * 授权方 uid
	 */
	@TableField(value = "owner_uid")
	private Integer ownerUid;
	/**
	 * 流量分配方式
	 */
	@TableField(value = "group_flow_type")
	private String groupFlowType;
	/**
	 * 建站类型
	 */
	@TableField(value = "site_type")
	private String siteType;
	/**
	 * 建站类别
	 */
	@TableField(value = "function_type")
	private String functionType;
	/**
	 * 排序
	 */
	@TableField(value = "order_number")
	private Integer orderNumber;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 最新更新时间
	 */
	@TableField(value = "last_modify_time")
	private Date lastModifyTime;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

