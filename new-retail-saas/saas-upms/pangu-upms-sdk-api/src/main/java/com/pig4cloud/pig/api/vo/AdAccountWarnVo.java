package com.pig4cloud.pig.api.vo;

import lombok.Data;

/**
 * @Author chengang
 * @Date 2021/7/13
 */
@Data
public class AdAccountWarnVo {

	private Integer mediaType;

	private String adAccount;

	/**
	 * 最大的活跃时间
	 */
	private String maxDay;

	/**
	 * 无消耗累计天数
	 */
	private String noCostCount;

}
