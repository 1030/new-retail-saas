package com.pig4cloud.pig.api.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Description 基础创意组件DTO
 * @Author chengang
 * @Date 2021/8/24
 */
@Data
public class CreativeComponentSearchDto implements Serializable {


	@NotNull(message = "广告账户不能为空")
	private String advertiser_id;

	/**
	 * 组件名称或ID模糊查询
	 */
	private String component_name;

	/**
	 * 状态 (PASS 通过,UNDER 审核中,REJECT 未通过)
	 */
	private String status;


}
