package com.pig4cloud.pig.api.entity;

import lombok.Data;

@Data
public class AdAwemeEntity {
	private Long id;
	private String awemeId;
	private String awemeName;
	private String bannedType;
	private String nicknameKeyword;
	private String advertiserId;
	private String advertiserName;
}
