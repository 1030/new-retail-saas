package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum TdtAdStatusEnum {
	//广点通
	STATUS_UNKNOWN("STATUS_UNKNOWN","未知状态"),
	STATUS_DENIED("STATUS_DENIED","审核不通过"),
	STATUS_FROZEN("STATUS_FROZEN","冻结"),
	STATUS_SUSPEND("STATUS_SUSPEND","暂停中"),
	STATUS_READY("STATUS_READY","未到投放时间"),
	STATUS_ACTIVE("STATUS_ACTIVE","投放中"),
	STATUS_STOP("STATUS_STOP","投放结束"),
	STATUS_PREPARE("STATUS_PREPARE","准备中"),
	STATUS_DELETED("STATUS_DELETED","已删除"),

	STATUS_ACTIVE_ACCOUNT_FROZEN("STATUS_ACTIVE_ACCOUNT_FROZEN","广告被暂停（账户资金被冻结）"),
	STATUS_ACTIVE_ACCOUNT_EMPTY("STATUS_ACTIVE_ACCOUNT_EMPTY","广告被暂停（账户余额不足）"),
	STATUS_ACTIVE_ACCOUNT_LIMIT("STATUS_ACTIVE_ACCOUNT_LIMIT","广告被暂停（账户达日限额）"),
	STATUS_ACTIVE_CAMPAIGN_LIMIT("STATUS_ACTIVE_CAMPAIGN_LIMIT","广告被暂停（推广计划达日限额）"),
	STATUS_ACTIVE_CAMPAIGN_SUSPEND("STATUS_ACTIVE_CAMPAIGN_SUSPEND","广告被暂停（推广计划暂停）"),
	STATUS_PART_READY("STATUS_PART_READY","部分待投放"),
	STATUS_PART_ACTIVE("STATUS_PART_ACTIVE","部分投放中") ;
	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	TdtAdStatusEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (TdtAdStatusEnum item : TdtAdStatusEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (TdtAdStatusEnum item : TdtAdStatusEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (TdtAdStatusEnum item : TdtAdStatusEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
