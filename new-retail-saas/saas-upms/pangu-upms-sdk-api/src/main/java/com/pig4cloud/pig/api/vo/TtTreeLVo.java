package com.pig4cloud.pig.api.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TtTreeLVo {
	private String label;
	private String value;
	private List<TtTreeLLVo> children;


}
