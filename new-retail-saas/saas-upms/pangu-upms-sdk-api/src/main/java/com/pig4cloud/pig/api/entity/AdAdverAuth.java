/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.entity;

import java.math.BigInteger;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @用户账号申请授权表
 * @author john
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_adver_auth")
public class AdAdverAuth extends Model<AdAdverAuth> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键id")
	private BigInteger id;
		
	@ApiModelProperty(value = "广告账户id")
	@TableField(value = "advertiser_id")
	private String advertiserId;
	
	@ApiModelProperty(value = "广告账户名称")
	@TableField(exist = false)
	private String name;
	
	@ApiModelProperty(value = "名下账号数量")
	@TableField(exist = false)
	private Integer adnum;
	
	
	@ApiModelProperty(value = "平台id")
	@TableField(value = "platform_id")
	private String platformId;
	
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "授权请求时间")
	@TableField( fill = FieldFill.INSERT)
	private Date createtime;
	
	/**
	 * 创建人
	 */
	@ApiModelProperty(value = "授权请求人")
	private Integer createuser;
	
	@ApiModelProperty(value = "授权请求人名称")
	@TableField(exist = false)
	private String createusername;
	
	
	/**
	 * 授权时间
	 */
	@ApiModelProperty(value = "审核时间")
	private Date authtime;
	
	/**
	 * 授权人
	 */
	@ApiModelProperty(value = "审核人")
	private Integer authuser;
	
	
	@ApiModelProperty(value = "审核人名称")
	@TableField(exist = false)
	private String authusername;
	
	
	/**
	 * 状态0-未审核，1-已审核
	 */
	@ApiModelProperty(value = "状态0-未审核，1-已审核")
	private Integer status;
	
	
	/**
	 * 状态0-批准，1-拒绝
	 */
	@ApiModelProperty(value = "状态0-批准，1-拒绝")
	@TableField(value = "auth_status")
	private Integer authStatus;
	
	
	/**
	 * 审核信息
	 */
	@ApiModelProperty(value = "审核信息")
	@TableField(value = "auth_msg")
	private String authMsg;
	

}
