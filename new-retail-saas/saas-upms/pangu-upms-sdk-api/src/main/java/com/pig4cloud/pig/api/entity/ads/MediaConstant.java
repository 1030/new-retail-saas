package com.pig4cloud.pig.api.entity.ads;

/**
 *
 * @author chengang
 * @time 2022/12/13
 */
public class MediaConstant {

	public static final String BD = "bd";
	public static final String TT = "tt";
	public static final String GDT = "gdt";
	public static final String KS = "ks";
}
