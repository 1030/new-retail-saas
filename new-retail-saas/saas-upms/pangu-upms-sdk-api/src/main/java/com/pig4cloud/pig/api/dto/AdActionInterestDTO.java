package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.util.List;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.api.dto
 * @Author 马嘉祺
 * @Date 2021/7/12 16:31
 * @Description
 */
@Data
public class AdActionInterestDTO {

	private Long id;

	private String name;

	private String num;

	private List<AdActionInterestDTO> children;

}
