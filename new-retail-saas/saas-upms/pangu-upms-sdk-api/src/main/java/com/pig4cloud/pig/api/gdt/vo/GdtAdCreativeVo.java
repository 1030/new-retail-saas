package com.pig4cloud.pig.api.gdt.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @description: 广点通广告创意列表及报表过滤条件请求参数
 * @author: nml
 * @time: 2020/12/14 17:46
 **/

@Setter
@Getter
public class GdtAdCreativeVo extends Page {

	/**
	 * 开始时间
	 */
	private String sdate;

	/**
	 * 结束时间
	 */
	private String edate;

	/*接收广告账户ids*/
	private String[] advertiserIds;

	/*广告账户ids*/
	private List<String> accountIds;

	/*开关*/
	private String onOff;

	/*广告创意名称*/
	private String adcreativeName;

	/** 广告创意 id */
	private Long adcreativeId;

}
