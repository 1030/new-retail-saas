package com.pig4cloud.pig.api.entity;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 投放广告数据报表 ad_statistic
 * 
 * @author hma
 * @date 2020-11-07
 */
@Setter
@Getter
public class AdCampaignStatistic extends TtStatistic
{
	private static final long serialVersionUID = 1L;
	
	/**  */
	private Long id;
	/** 广告主id */
	private Long advertiserId;
	/** 广告组id */
	private Long campaignId;

	/** 广告组name */
	private String campaignName;
	/** 广告计划id */
	private Long 	adId;
	/** 广告name */
	private String 	adName;
	/** 展现数据-平均千次展现费用 */
	private BigDecimal avgShowCost;
	/** 平台 */
	private String platform;
	/** 数据起始时间 */
	private String statDatetime;

	/** 类型  1 ：广告组  2  广告计划 */
	private Integer type;

	private Long  creativeId;

	/**
	 * 创意素材标题
	 */
	private String creativeTitle;

	/**
	 * 操作状态【对应广告组，创意，计划】
	 */
	private String optStatus;


	
}
