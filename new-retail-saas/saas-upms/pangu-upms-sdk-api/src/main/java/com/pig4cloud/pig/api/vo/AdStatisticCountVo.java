package com.pig4cloud.pig.api.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 投放广告数据报表 ad_statistic
 * 
 * @author hma
 * @date 2020-11-07
 */
@Setter
@Getter
public class AdStatisticCountVo
{
	private static final long serialVersionUID = 1L;

	/** 展现数据-展示数汇总 */
	private Long show=0L;
	/** 展现数据-总花费汇总 */
	private BigDecimal cost=new BigDecimal(0);
	/** 展现数据-平均千次展现费用汇总 */
	private BigDecimal avgShowCost=new BigDecimal(0);

	/** 展现数据-点击数汇总 */
	private Long click=0L;
	/** 展现数据-点击率汇总 */
	private BigDecimal ctr=new BigDecimal(0);
	/** 转化数据-转化数汇总 */
	private Integer convert=0;
	/** 转化数据-转化成本汇总 */
	private BigDecimal convertCost=new BigDecimal(0);
	/**  */
	private Integer deepConvert=0;
	/** 转化数据-转化率汇总 */
	private BigDecimal convertRate=new BigDecimal(0);
	/** 转化数据-深度转化成本 汇总*/
	private BigDecimal deepConvertCost=new BigDecimal(0);
	/** 转化数据-深度转化率 */
	private BigDecimal deepConvertRate=new BigDecimal(0);
	/** 点击均价汇总 */
	private BigDecimal  avgClickCost=new BigDecimal(0);




}
