package com.pig4cloud.pig.api.vo;

import lombok.Data;

/**
 * @Author chengang
 * @Date 2021/7/13
 */
@Data
public class AdPtypeVo {

	/**
	 * 根据监测链接获取的值，目前和分包ID一致
	 */
	private String ptype;
	/**
	 * 子游戏ID
	 */
	private Integer gameid;
	/**
	 * 广告计划ID
	 */
	private String adid;
	/**
	 * 分包ID
	 */
	private String appchl;
	/**
	 * 平台类型:0-3399平台,1-头条,2-百度,3-新数,4-公会,7-UC,8-广点通
	 */
	private Integer ctype;
	/**
	 * 点击ID
	 */
	private String clickid;
	/**
	 * 创建时间
	 */
	private String createtime;
	/**
	 * 主渠道
	 */
	private String parentchl;
	/**
	 * 子渠道
	 */
	private String chl;
	/**
	 * 广告账户
	 */
	private String adaccount;
	/**
	 * 广告位ID，旧版V3运营后台使用
	 */
	private Integer advert_id;


}
