package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author ：lile
 * @date ：2021/7/15 14:40
 * @description：
 * @modified By：
 */
@Data
@TableName("tt_advertiser_transfer")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "头条账号转账表")
public class AdTtTransfer extends Model<AdTtTransfer> {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键")
	private Integer id;

	/**
	 * 转账广告主ID
	 */
	@ApiModelProperty(value = "转账广告主ID")
	private Long advertiserIdFrom;

	/**
	 * 目标广告主ID
	 */
	@ApiModelProperty(value = "目标广告主ID")
	private Long advertiserId;

	/**
	 * 代理商ID
	 */
	@ApiModelProperty(value = "代理商ID")
	private Long agentId;


	/**
	 * 转账类型CASH：现金，GRANT：赠款
	 */
	@ApiModelProperty(value = "转账类型CASH：现金，GRANT：赠款")
	private String transferType;

	/**
	 * 账户余额(单位元)
	 */
	@ApiModelProperty(value = "账户余额(单位元)")
	private BigDecimal balance;


	/**
	 * 转账金额,单位(元),整数
	 */
	@ApiModelProperty(value = "转账金额,单位(元),整数")
	private Integer amount;

	/**
	 * 返回码
	 */
	@ApiModelProperty(value = "返回码")
	private Integer code;

	/**
	 * 返回信息
	 */
	@ApiModelProperty(value = "返回信息")
	private String message;

	/**
	 * 交易序列号
	 */
	@ApiModelProperty(value = "交易序列号")
	private String data;

	/**
	 * 请求日志id
	 */
	@ApiModelProperty(value = "请求日志id")
	private String requestId;

	/**
	 * 删除状态：0正常，1删除
	 */
	@ApiModelProperty(value = "删除状态：0正常，1删除")
	@TableLogic
	private Integer isDelete;
	/**
	 * 创建人
	 */
	@ApiModelProperty(value = "创建人")
	private String createUser;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**
	 * 修改人
	 */
	@ApiModelProperty(value = "修改人")
	private String updateUser;
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

}
