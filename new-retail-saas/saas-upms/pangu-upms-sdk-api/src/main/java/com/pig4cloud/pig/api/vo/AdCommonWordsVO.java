package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.api.vo
 * @Author 马嘉祺
 * @Date 2021/7/17 11:23
 * @Description
 */
@Data
public class AdCommonWordsVO extends Page {

	/**
	 * 常用语主键ID，多个ID使用','分隔
	 */
	@NotBlank(message = "常用语主键ID", groups = {Delete.class})
	private String idArr;

	/**
	 * 常用语主键
	 */
	@NotNull(message = "常用语主键不能为空", groups = {Edit.class})
	private Long id;

	/**
	 * 广告账号ID
	 */
	@NotBlank(message = "广告账号ID不能为空", groups = {Add.class})
	private String advertiserId;

	/**
	 * 广告账号名称
	 */
	@NotBlank(message = "广告账号名称不能为空", groups = {Add.class})
	private String advertiserName;

	/**
	 * 常用语内容
	 */
	@NotBlank(message = "常用语内容不能为空", groups = {Add.class})
	private String text;

	public interface Add {
	}

	public interface Edit {
	}

	public interface Delete {
	}

}
