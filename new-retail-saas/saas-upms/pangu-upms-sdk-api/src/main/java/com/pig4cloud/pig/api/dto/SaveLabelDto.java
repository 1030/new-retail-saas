package com.pig4cloud.pig.api.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class SaveLabelDto {
	@NotNull(message = "relateid不能为空")
	private Integer relateid;
	private List<Integer> labelidList;
	@NotNull(message = "type不能为空")
	private Integer type;
}
