package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Title null.java
 * @Package com.pig4cloud.pigx.vo
 * @Author 马嘉祺
 * @Date 2021/6/21 20:07
 * @Description
 */
@Data
@EqualsAndHashCode
public class AdQualifiedFileVO extends Page {

	/**
	 * 资质文件ID, 多个ID使用','分隔
	 */
	@NotEmpty(message = "ID不能为空", groups = {Delete.class})
	private String qualifiedIdArr;

	/**
	 * 资质文件ID
	 */
	@NotNull(message = "资质文件ID不能为空", groups = {Update.class})
	private Long qualifiedId;

	/**
	 * 资质文件类目，0：软著；1:版号
	 */
	@NotNull(message = "资质文件类目不能为空", groups = {Create.class,Update.class})
	private Integer qualifiedType;

	/**
	 * 资质文件名称
	 */
	private String fileName;

	/**
	 * 主游戏ID，多个使用','分隔
	 */
	private String pgidArr;

	/**
	 * 主游戏ID
	 */
	@NotNull(message = "主游戏ID不能为空", groups = {Create.class,Update.class})
	private Long pgid;

	@NotNull(message = "子游戏ID不能为空", groups = {Create.class,Update.class})
	private Long gameId;

	/**
	 * 文件名称
	 */
	@NotNull(message = "资质文件不能为空", groups = {Create.class})
	private MultipartFile qualifiedFile;

	public interface Create {
	}

	public interface Update {
	}

	public interface Delete {
	}

}
