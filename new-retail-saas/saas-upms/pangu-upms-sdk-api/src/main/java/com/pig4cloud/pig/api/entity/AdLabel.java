/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 素材标签
 *
 * @author cx
 * @date 2021-06-29 14:27:26
 */
@Data
@TableName("ad_label")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "素材标签")
public class AdLabel extends Model<AdLabel> {
private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id",type= IdType.AUTO)
    @ApiModelProperty(value="id")
    private Long id;
    /**
     * 标签名称
     */
    @ApiModelProperty(value="标签名称")
    private String labelName;
    /**
     * 类型
     */
    @ApiModelProperty(value="类型")
    private Integer type;
    /**
     * 说明
     */
    @ApiModelProperty(value="说明")
    private String description;
    /**
     * 状态
     */
    @ApiModelProperty(value="状态")
    private Integer status;
    /**
     * 删除状态：0正常 ，1删除
     */
    @ApiModelProperty(value="删除状态：0正常 ，1删除")
    private Integer isDelete;
    /**
     * 
     */
    @ApiModelProperty(value="")
    private Date createTime;
    /**
     * 
     */
    @ApiModelProperty(value="")
    private Date updateTime;
    }
