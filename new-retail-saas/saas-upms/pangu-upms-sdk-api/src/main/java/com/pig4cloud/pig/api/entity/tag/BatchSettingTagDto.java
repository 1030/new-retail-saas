package com.pig4cloud.pig.api.entity.tag;

import lombok.Data;

@Data
public class BatchSettingTagDto {
	/**
	 * 素材id 集合 ,隔开
	 */
	private String relateIds;

	/**
	 * 标签id集合 ,隔开
	 */
	private String tagIds;

	private Integer type;
}
