package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chengang
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_account_warning_result")
public class AdAccountWarningResult extends Model<AdAccountWarningResult> {

	private static final long serialVersionUID = -4846463548424420828L;

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键id")
	private Long id;

	@TableField(value = "media_type")
	@ApiModelProperty(value = "媒体类型 0-3399平台,1-头条,2-百度,3-新数,4-公会,7-UC,8-广点通")
	private Integer mediaType;

	@TableField(value = "advertiser_id")
	@ApiModelProperty(value = "广告账户")
	private String advertiserId;

	@TableField(value = "active_date")
	@ApiModelProperty(value = "最后活跃时间")
	private Date activeDate;


	@TableField(value = "active")
	@ApiModelProperty(value = "指定周期是否活跃  1活跃 2不活跃")
	private Integer active;

	@TableField(exist = false)
	@ApiModelProperty(value = "广告账户名称")
	private String advertiserName;

	@TableField(exist = false)
	@ApiModelProperty(value = "广告账户总余额")
	private BigDecimal balance = BigDecimal.ZERO;

	@TableField(exist = false)
	@ApiModelProperty(value = "现金余额")
	private BigDecimal cashBalance = BigDecimal.ZERO;

	@TableField(exist = false)
	@ApiModelProperty(value = "赠款余额")
	private BigDecimal grantBalance = BigDecimal.ZERO;

	@TableField(exist = false)
	@ApiModelProperty(value = "投放人")
	private String throwUser;

	@TableField(exist = false)
	@ApiModelProperty(value = "无消耗累计天数")
	private String noCostCount;

//	@TableField(exist = false)
//	@ApiModelProperty(value = "账号总数")
//	private Long advertiserCount;
//
//
//	@TableField(exist = false)
//	@ApiModelProperty(value = "余额总和")
//	private BigDecimal sumBalance;


	@TableField(value = "is_deleted")
	@ApiModelProperty(value = "是否删除  0否 1是")
	private Integer isDeleted;


	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(value = "update_time")
	private Date updateTime;

	@TableField(value = "create_id")
	@ApiModelProperty(value = "创建人")
	private Long createId;

	@TableField(value = "update_id")
	@ApiModelProperty(value = "修改人")
	private Long updateId;


}
