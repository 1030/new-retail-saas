package com.pig4cloud.pig.api.dto;


import com.pig4cloud.pig.api.util.Page;
import lombok.Data;

@Data
public class AdverDayReportDTO{
	private String advertiserId;//广告主id	
	private String sdate;//开始日期
	private String edate;//结束日期
	private Page page;
}
