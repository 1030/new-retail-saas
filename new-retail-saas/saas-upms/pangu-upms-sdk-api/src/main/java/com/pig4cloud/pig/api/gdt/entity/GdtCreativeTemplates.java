package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;


/**
 * 创意形式表  gdt_creative_templates
 * 
 * @author hma
 * @date 2020-12-16
 */
@Setter
@Getter
public class GdtCreativeTemplates
{
	private static final long serialVersionUID = 1L;
	
	/** 创意形式 id */
	@TableId(value = "adcreative_template_id",type = IdType.AUTO)
	private Long adcreativeTemplateId;
	/** 创意形式类型 */
	private String adcreativeTemplateStyle;
	/** 创意形式名称 */
	private String adcreativeTemplateAppellation;
	/** 投放版位 */
	private String siteSet;
	/** 推广目标类型 */
	private String promotedObjectType;
	/** 创意规格示意图数组 */
	private String adcreativeSampleImageList;
	/** 广告属性 */
	private String adAttributes;
	/** 广告创意属性 */
	private String adcreativeAttributes;
	/** 广告创意元素列表 */
	private String adcreativeElements;
	/** 支持的落地页类型 */
	private String supportPageType;
	/** 支持的计费信息 */
	private String supportBillingSpecList;
	/** 动态广告支持情况 */
	private String supportDynamicAbilitySpecList;
	/** 动态广告投放能力类型 */
	private String dynamicAbilityType;
	/** 	在该规格位置上单个广告上的创意可展示商品数量 */
	private String productItemDisplayQuantity;
	/**  */
	private String supportBidModeList;
	private Integer type;

	private Object[] siteSetArr;

	private Integer length;

	private String isDynamicCreative;

	private String accountId;

	public GdtCreativeTemplates(){}
	public GdtCreativeTemplates(String  promotedObjectType,Object[]  siteSetArr,Long adcreativeTemplateId,Integer type,String isDynamicCreative,String accountId){
		this.promotedObjectType=promotedObjectType;
		this.siteSetArr=siteSetArr;
		this.adcreativeTemplateId=adcreativeTemplateId;
		this.type=type;
		this.isDynamicCreative=isDynamicCreative;
		this.accountId=accountId;
	}

}
