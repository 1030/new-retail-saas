package com.pig4cloud.pig.api.entity.ads;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author chengang
 * @Date 2022/12/13
 */
@Data
public class AdsCallBackResponse implements Serializable {

	private static final long serialVersionUID = 1408659716575648777L;

	/**
	 * ################## 百度回调参数 START
	 */
	private String appId;
	private String authCode;
	/**
	 * 授权用户ID
	 */
	private String userId;
	private String timestamp;
	private String signature;
	private String state;

	/**
	 * ################## 百度回调参数 END
	 */



}
