package com.pig4cloud.pig.api.entity;
import lombok.Data;

import java.io.Serializable;

@Data
public class Filtering implements Serializable{
	private String siteId ;
	private String siteName ;
	private String startTime ;
	private String endTime;
}
