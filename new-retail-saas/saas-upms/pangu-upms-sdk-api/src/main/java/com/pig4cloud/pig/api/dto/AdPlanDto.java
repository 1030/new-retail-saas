package com.pig4cloud.pig.api.dto;

import com.pig4cloud.pig.api.entity.AdPlan;
import lombok.Getter;
import lombok.Setter;


/**
 * 广告计划表dto  前端传参对象
 * 
 * @author hma
 * @date 2020-11-10
 */
@Setter
@Getter
public class AdPlanDto extends AdPlan
{
	private static final long serialVersionUID = 1L;
	public  AdPlanDto(){}

	private String adName;

	private Integer adPlanLimit;
	
}
