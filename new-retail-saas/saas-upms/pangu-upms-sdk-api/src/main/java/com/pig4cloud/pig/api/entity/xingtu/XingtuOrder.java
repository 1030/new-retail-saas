package com.pig4cloud.pig.api.entity.xingtu;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 星图任务订单表
 * @author  chengang
 * @version  2021-12-13 16:48:01
 * table: xingtu_order
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "xingtu_order")
public class XingtuOrder extends Model<XingtuOrder>{

	//columns START
			//主键id
			@TableId(value = "id",type = IdType.AUTO)
			private Long id;
			

			//星图广告账户ID
			@TableField(value = "advertiser_id")
			private String advertiserId;
			

			//任务ID
			@TableField(value = "demand_id")
			private String demandId;
			

			//达人id
			@TableField(value = "author_id")
			@JSONField(name = "author_id")
			private String authorId;
			

			//达人名称
			@TableField(value = "author_name")
			@JSONField(name = "author_name")
			private String authorName;
			

			//达人头像
			@TableField(value = "avatar_uri")
			@JSONField(name = "avatar_uri")
			private String avatarUri;
			

			//订单创建时间
			@TableField(value = "order_create_time")
			@JSONField(name = "create_time")
			private String orderCreateTime;
			

			//封面图
			@TableField(value = "head_image_uri")
			@JSONField(name = "head_image_uri")
			private String headImageUri;
			

			//订单id
			@TableField(value = "order_id")
			@JSONField(name = "order_id")
			private String orderId;
			

			//作品名称
			@TableField(value = "title")
			@JSONField(name = "title")
			private String title;
			

			//订单状态
			@TableField(value = "universal_order_status")
			@JSONField(name = "universal_order_status")
			private String universalOrderStatus;
			

			//视频id，每个视频唯一（建议使用item_id）
			@TableField(value = "video_id")
			@JSONField(name = "video_id")
			private String videoId;
			

			//视频id，与星图平台前端video_url中展现的视频id一致，每个视频唯一
			@TableField(value = "item_id")
			@JSONField(name = "item_id")
			private String itemId;
			

			//视频链接
			@TableField(value = "video_url")
			@JSONField(name = "video_url")
			private String videoUrl;
			

			//是否删除  0否 1是
			@TableField(value = "deleted")
			private Integer deleted;
			

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;
			

			//创建人
			@TableField(value = "create_id")
			private Long createId;
			

			//修改人
			@TableField(value = "update_id")
			private Long updateId;
			

	//columns END 数据库字段结束
	

	
}

	

	
	

