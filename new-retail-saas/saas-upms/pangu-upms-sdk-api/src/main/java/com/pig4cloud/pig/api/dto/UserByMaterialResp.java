package com.pig4cloud.pig.api.dto;

import lombok.Data;

/**
 * 素材库
 * @date 2021-06-19 16:12:13
 */
@Data
public class UserByMaterialResp {

	private Integer userId;

	private String realName;

}
