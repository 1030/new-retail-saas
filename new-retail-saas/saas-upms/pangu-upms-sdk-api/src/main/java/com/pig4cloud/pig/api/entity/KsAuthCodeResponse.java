package com.pig4cloud.pig.api.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description  快手回调
 * @Author chengang
 * @Date 2022/3/21
 */
@Data
public class KsAuthCodeResponse implements Serializable {

	private static final long serialVersionUID = 7691978788204549476L;

	private String state;

	private String auth_code;


}
