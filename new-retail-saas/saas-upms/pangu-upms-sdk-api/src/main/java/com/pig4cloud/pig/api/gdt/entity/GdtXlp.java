package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @蹊径落地页
 * @author yk
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="gdt_xjlanding_page")
public class GdtXlp extends Model<GdtXlp> {

	@ApiModelProperty(value = "自增ID")
	@TableId(value = "id",type= IdType.AUTO)
    private Long id;

    /**
     * 落地页缩略图
     */
	@ApiModelProperty(value = "落地页缩略图地址：落地页缩略图")
	@TableField(value = "preview_url")
    private String previewUrl;

    /**
     * 落地页名称
     */
	@ApiModelProperty(value = "落地页名称")
	@TableField(value = "page_name")
    private String pageName;

    /**
     * 落地页id
     */
	@ApiModelProperty(value = "落地页ID")
	@TableField(value = "page_id")
    private String pageId;

    /**
     * 状态（蹊径落地页发布状态）
     */
	@ApiModelProperty(value = "状态")
	@TableField(value = "page_publish_status")
    private String pagePublishStatus;

    /**
     * 状态（蹊径落地页审核状态）
     */
	@ApiModelProperty(value = "状态")
	@TableField(value = "page_status")
    private String pageStatus;

    /**
     * 创建人
     */
	@ApiModelProperty(value = "创建人")
	@TableField(value = "create_user")
    private String createUser;

    /**
     * 创建时间
     */
	@ApiModelProperty(value = "创建时间")
	@TableField(value = "create_time",fill= FieldFill.INSERT)
    private String createTime;

    /**
     * 落地页服务id，主要用于广告投放、落地页送审及删除
     */
	@ApiModelProperty(value = "落地页服务id")
	@TableField(value = "page_service_id")
    private String pageServiceId;

    /**
     * 蹊径落地页类型
     */
	@ApiModelProperty(value = "落地页类型")
	@TableField(value = "page_type")
    private String pageType;

    /**
     * 蹊径落地页最近更新时间
     */
	@ApiModelProperty(value = "最近更新时间")
	@TableField(value = "page_last_modify_time")
    private String pageLastModifyTime;

	/**
     * 推广帐号 id，有操作权限的帐号 id，包括代理商和广告主帐号 id
     */
	@ApiModelProperty(value = "推广帐号id")
	@TableField(value = "account_id")
    private String accountId;

	/**
	 * 蹊径落地页发布类型
	 */
	@ApiModelProperty(value = "蹊径落地页发布类型")
	@TableField(value = "page_publish_type")
	private String pagePublishType;

    /**
     * 推广目标 id
     */
	@ApiModelProperty(value = "推广目标 id")
	@TableField(value = "promoted_object_id")
    private String promotedObjectId;

    /**
     * 商品库 id
     */
	@ApiModelProperty(value = "商品库 id")
	@TableField(value = "product_catalog_id")
    private String productCatalogId;

}
