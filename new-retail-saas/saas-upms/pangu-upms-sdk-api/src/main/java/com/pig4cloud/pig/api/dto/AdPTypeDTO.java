package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.util.List;

/**
 * @Description
 * @Author chengang
 * @Date 2021/9/08
 */
@Data
public class AdPTypeDTO {

	public AdPTypeDTO(Integer ctype, String clickid, String createtime,Integer advert_id){
		this.ctype = ctype;
		this.clickid = clickid;
		this.createtime = createtime;
		this.advert_id = advert_id;
	}

	/**
	 * 根据监测链接获取的值，目前和分包ID一致
	 */
	private String ptype;
	/**
	 * 子游戏ID
	 */
	private Integer gameid;
	/**
	 * 广告计划ID
	 */
	private String adid;
	/**
	 * 分包ID
	 */
	private String appchl;
	/**
	 * 平台类型:0-3399平台,1-头条,2-百度,3-新数,4-公会,7-UC,8-广点通
	 */
	private Integer ctype;
	/**
	 * 点击ID has_cost_but_no_click_default_value
	 */
	private String clickid;
	/**
	 * 创建时间
	 */
	private String createtime;
	/**
	 * 主渠道
	 */
	private String parentchl;
	/**
	 * 子渠道
	 */
	private String chl;
	/**
	 * 广告账户
	 */
	private String adaccount;
	/**
	 * 广告位ID，旧版V3运营后台使用defaultValue = 999
	 */
	private Integer advert_id;


}
