package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 项目表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:20:40
 * table: ad_project
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_project")
public class AdProject extends Model<AdProject>{
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 项目ID
	 */
	@TableField(value = "project_id")
	private Long projectId;
	/**
	 * 项目名称
	 */
	@TableField(value = "name")
	private String name;
	/**
	 * 广告账户ID
	 */
	@TableField(value = "advertiser_id")
	private Long advertiserId;
	/**
	 * 推广目的，枚举值：APP 应用推广
	 */
	@TableField(value = "landing_type")
	private String landingType;
	/**
	 * 子目标，枚举值：DOWNLOAD 应用下载、LAUNCH 应用调用、RESERVE 预约下载
	 */
	@TableField(value = "app_promotion_type")
	private String appPromotionType;
	/**
	 * 营销场景，枚举值：VIDEO_AND_IMAGE 短视频/图片
	 */
	@TableField(value = "marketing_goal")
	private String marketingGoal;
	/**
	 * 广告类型，枚举值：ALL
	 */
	@TableField(value = "ad_type")
	private String adType;
	/**
	 * 目标操作，枚举值：ENABLE 启用项目、DISABLE暂停项目。
	 */
	@TableField(value = "opt_status")
	private String optStatus;
	/**
	 * 项目创建时间，格式yyyy-mm-dd
	 */
	@TableField(value = "project_create_time")
	private String projectCreateTime;
	/**
	 * 项目更新时间，格式yyyy-mm-dd
	 */
	@TableField(value = "project_modify_time")
	private String projectModifyTime;
	/**
	 * 项目状态
	 */
	@TableField(value = "status")
	private String status;
	/**
	 * 出价方式
	 */
	@TableField(value = "pricing")
	private String pricing;
	/**
	 * 关联产品投放相关(product_setting,product_platform_id,product_id)
	 */
	@TableField(value = "related_product")
	private String relatedProduct;
	/**
	 * 下载链接
	 */
	@TableField(value = "download_url")
	private String downloadUrl;
	/**
	 * 下载方式，枚举值：DOWNLOAD_URL 直接下载、EXTERNAL_URL 落地页下载
	 */
	@TableField(value = "download_type")
	private String downloadType;
	/**
	 * 优先从系统应用商店下载（下载模式），枚举值：APP_STORE_DELIVERY 优先商店下载、 DEFAULT 默认下载
	 */
	@TableField(value = "download_mode")
	private String downloadMode;
	/**
	 * 调起方式，枚举值： DIRECT_OPEN 直接调起、EXTERNAL_OPEN 落地页调起
	 */
	@TableField(value = "launch_type")
	private String launchType;
	/**
	 * Deeplink直达链接
	 */
	@TableField(value = "open_url")
	private String openUrl;
	/**
	 * ulink直达链接
	 */
	@TableField(value = "ulink_url")
	private String ulinkUrl;
	/**
	 * 预约下载链接
	 */
	@TableField(value = "subscribe_url")
	private String subscribeUrl;
	/**
	 * 优化目标(asset_ids,convert_id,external_action,deep_external_action)
	 */
	@TableField(value = "optimize_goal")
	private String optimizeGoal;
	/**
	 * 广告版位(inventory_catalog,inventory_type,union_video_type)
	 */
	@TableField(value = "delivery_range")
	private String deliveryRange;
	/**
	 * 定向设置
	 */
	@TableField(value = "audience")
	private String audience;
	/**
	 * 投放设置
	 */
	@TableField(value = "delivery_setting")
	private String deliverySetting;
	/**
	 * 监测链接设置
	 */
	@TableField(value = "track_url_setting")
	private String trackUrlSetting;
	/**
	 * 来源(1:API,2:渠道后台)
	 */
	@TableField(value = "soucre_status")
	private Integer soucreStatus;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

