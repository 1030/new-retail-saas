package com.pig4cloud.pig.api.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @Description
 * @Author chengang
 * @Date 2022/5/19
 */
@Data
public class OptimizationGoalDto implements Serializable {

	private static final long serialVersionUID = -2904236532588753523L;

	/**
	 * 广告主帐号 id
	 */
	@NotNull(message = "广告账户ID不能为空")
	private Long accountId;

	/**
	 * 出价方式
	 */
//	@NotNull(message = "出价方式不能为空",groups = {CreateAdGroup.class})
	private String bidMode;

	/**
	 * 推广目标 id
	 */
	private String promotedObjectId;

	/**
	 * 推广目标类型
	 */
	private String promotedObjectType;

	/**
	 * 投放版位集合
	 */
	@NotNull(message = "投放版位不能为空")
	@NotEmpty(message = "投放版位不能为空")
	private List<String> siteSet;


//	public interface CreateAdGroup {
//
//	}
}
