package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @程序化落地页
 * @author yk
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="tt_landing_group")
public class LandingGroup extends Model<LandingGroup> {

	/**
	 * 落地页组ID
	 */
	@ApiModelProperty
	@TableId(value = "group_id")
    private String groupId;

    /**
     * 落地页组名称
     */
	@ApiModelProperty
	@TableField(value = "group_title")
    private String groupTitle;

    /**
     * 落地页组 URL
     */
	@ApiModelProperty
	@TableField(value = "group_url")
    private String groupUrl;

	/**
	 * 落地页组状态
	 */
	@ApiModelProperty
	@TableField(value = "group_status")
    private String groupStatus;

    /**
     * 流量分配方式
     */
	@ApiModelProperty
	@TableField(value = "group_flow_type")
    private String groupFlowType;

    /**
     * 站点列表
     */
	@ApiModelProperty
	@TableField(value = "sites")
    private String sites;

	/**
	 * 广告主id
     */
	@ApiModelProperty
	@TableField(value = "advertiser_id")
	private String advertiserId;


    /**
     * 创建时间
     */
	@ApiModelProperty
	@TableField(value = "createtime")
    private String createTime;

	/**
	 * 更新时间
	 */
	@ApiModelProperty
	@TableField(value = "updatetime")
	private String  updateTime;

	/**
	 * 排序
	 */
	@ApiModelProperty
	@TableField(value = "order_number")
	private Integer orderNumber;

	/**
	 * 落地页数
	 */
	@TableField(exist = false)
	private Integer number;

}
