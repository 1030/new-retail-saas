package com.pig4cloud.pig.api.vo;
import io.swagger.annotations.ApiModelProperty;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 广告转化表
 * @author  chenxiang
 * @version  2022-05-23 10:35:26
 * table: ad_convert
 */
@Data
@Accessors(chain = true)
public class AdConvertReq extends Page {

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "对应advertiser_monitor_info主键")
	private Long adId;

	@ApiModelProperty(value = "父游戏ID")
	private String pgameId;

	@ApiModelProperty(value = "子游戏ID")
	private String gameId;

	@ApiModelProperty(value = "渠道code")
	private String chl;

	@ApiModelProperty(value = "渠道code 1头条 8广点通 10快手")
	private String mediaCode;

	@ApiModelProperty(value = "广告账户")
	private String advertiserId;

	@ApiModelProperty(value = "第三方渠道转化ID")
	private String convertId;

	@ApiModelProperty(value = "转化名称")
	private String convertName;

	@ApiModelProperty(value = "转化目标")
	private String convertTarget;

	@ApiModelProperty(value = "深度转化目标")
	private String deepConversionType;

	@ApiModelProperty(value = "转化目标次数")
	private String convertCount;

	@ApiModelProperty(value = "深度转化目标次数")
	private String deepConversionCount;

	@ApiModelProperty(value = "点击监测链接")
	private String actionTrackUrl;

	@ApiModelProperty(value = "点击监测链接")
	private String xingtuActionTrackUrl;

	@ApiModelProperty(value = "应用ID")
	private String appId;

	@ApiModelProperty(value = "应用名称")
	private String appName;

	@ApiModelProperty(value = "应用包名")
	private String packageName;

	@ApiModelProperty(value = "第三方创建时间")
	private String thirdCreateTime;

	@ApiModelProperty(value = "第三方创建时间")
	private String thirdUpdateTime;

	@ApiModelProperty(value = "转化来源")
	private String convertSourceType;

	@ApiModelProperty(value = "转化统计方式：ONLY_ONE（仅一次），EVERY_ONE（每一次）")
	private String convertDataType;

	@ApiModelProperty(value = "归因方式")
	private String claimType;

	@ApiModelProperty(value = "数据源 id")
	private String userActionSetId;

	@ApiModelProperty(value = "当前站点是否可用，true：是，false：否")
	private String siteSetEnable;

	@ApiModelProperty(value = "安卓应用渠道包 id")
	private String appAndroidChannelPackageId;

	@ApiModelProperty(value = "推广目标 id")
	private String promotedObjectId;

	@ApiModelProperty(value = "转化场景")
	private String conversionScene;

	@ApiModelProperty(value = "转化状态")
	private String status;

	@ApiModelProperty(value = "转化操作状态")
	private String optStatus;

	@ApiModelProperty(value = "来源(1:API,2:渠道后台)")
	private String soucreStatus;

	@ApiModelProperty(value = "创建人")
	private Long createId;

	@ApiModelProperty(value = "ID集合")
	private List<Long> adIds;

	@ApiModelProperty(value = "下载地址")
	private String downloadUrl;

}
