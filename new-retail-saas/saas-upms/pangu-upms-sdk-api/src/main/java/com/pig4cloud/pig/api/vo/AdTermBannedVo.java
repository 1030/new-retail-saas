package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class AdTermBannedVo extends Page {

	/**
	 * 屏蔽词ID
	 */
	@NotNull(message = "屏蔽词ID不能为空", groups = {Edit.class, Delete.class})
	private Long id;

	/**
	 * 屏蔽词内容
	 */
	@NotBlank(message = "屏蔽词不能为空", groups = {Edit.class})
	private String terms;

	/**
	 * 屏蔽词内容
	 */
	@NotEmpty(message = "屏蔽词内容不能为空", groups = {Add.class})
	@Size(min = 1, max = 500, message = "屏蔽词数量必须不小于1不大于500", groups = {Add.class})
	private List<String> termsList;

	/**
	 * 广告账号ID
	 */
	@NotBlank(message = "广告账号ID不能为空", groups = {Add.class})
	private String advertiserId;

	/**
	 * 广告账号名称
	 */
	@NotBlank(message = "广告账号名称不能为空", groups = {Add.class})
	private String advertiserName;

	public interface Add {
	}

	public interface Edit {
	}

	public interface Delete {
	}

}
