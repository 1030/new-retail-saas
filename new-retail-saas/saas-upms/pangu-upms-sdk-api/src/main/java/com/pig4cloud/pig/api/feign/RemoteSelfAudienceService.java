package com.pig4cloud.pig.api.feign;

import com.pig4cloud.pig.api.entity.SelfCustomAudience;
import com.pig4cloud.pig.api.feign.factory.RemoteSelfAudienceServiceFallbackFactory;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@FeignClient(contextId = "remoteSelfAudienceService", value = ServiceNameConstants.UPMS_SDK_SERVICE, fallbackFactory = RemoteSelfAudienceServiceFallbackFactory.class)
public interface RemoteSelfAudienceService {


	/**
	 * 根据名称获取集合
	 * @param from
	 * @param entity
	 * @return
	 */
	@PostMapping(value = "/selfCustomAudience/getListByDto")
	List<SelfCustomAudience> getListByDto(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody SelfCustomAudience entity);

	/**
	 * 保存
	 * @param from
	 * @param entity
	 * @return
	 */
	@PostMapping(value = "/selfCustomAudience/saveEntity")
	boolean saveEntity (@RequestHeader(SecurityConstants.FROM) String from, @RequestBody SelfCustomAudience entity);

}
