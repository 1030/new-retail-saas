/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.api.entity.ks;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 快手广告计划表
 *
 * @author yuwenfeng
 * @date 2022-03-26 17:31:08
 */
@Data
@TableName("ks_campaign")
@ApiModel(value = "快手广告计划表")
public class KsCampaign implements Serializable {

    /**
     * 主键ID
     */
    @TableId
    @ApiModelProperty(value="主键ID")
    private Long id;

    /**
     * 广告主ID
     */
    @ApiModelProperty(value="广告主ID")
    private Long advertiserId;

    /**
     * 计划ID
     */
    @ApiModelProperty(value="计划ID")
    private Long campaignId;

    /**
     * 计划名称
     */
    @ApiModelProperty(value="计划名称")
    private String campaignName;

    /**
     * 计划状态(1：投放中；2：暂停 3：删除)
     */
    @ApiModelProperty(value="计划状态(1：投放中；2：暂停 3：删除)")
    private Integer putStatus;

    /**
     * 创建渠道
     */
    @ApiModelProperty(value="创建渠道")
    private String createChannel;

    /**
     * 当日预算(单位：厘)
     */
    @ApiModelProperty(value="当日预算(单位：厘)")
    private Long dayBudget;

    /**
     * 分日预算(单位：厘)
     */
    @ApiModelProperty(value="分日预算(单位：厘)")
    private String dayBudgetSchedule;

    /**
     * 计划类型(2：提升应用安装 3：获取电商下单 4：推广品牌活动 5：收集销售线索 7：提高应用活跃 9：商品库推广；16：粉丝/直播推广)
     */
    @ApiModelProperty(value="计划类型(2：提升应用安装 3：获取电商下单 4：推广品牌活动 5：收集销售线索 7：提高应用活跃 9：商品库推广；16：粉丝/直播推广)")
    private Integer campaignType;

    /**
     * 广告创建时间
     */
    @ApiModelProperty(value="广告创建时间")
    private LocalDateTime adCreateTime;

    /**
     * 广告更新时间
     */
    @ApiModelProperty(value="广告更新时间")
    private LocalDateTime adUpdateTime;

    /**
     * 计划子类型(4：DPA，5：SDPA)
     */
    @ApiModelProperty(value="计划子类型(4：DPA，5：SDPA)")
    private Integer campaignSubType;

    /**
     * 广告计划类型(0:信息流，1:搜索)
     */
    @ApiModelProperty(value="广告计划类型(0:信息流，1:搜索)")
    private Integer adType;

    /**
     * 来源(1:API,2:渠道后台)
     */
    @ApiModelProperty(value="来源(1:API,2:渠道后台)")
    private Integer soucreStatus;

    /**
     * 平台状态(1:草稿,2:已推送)
     */
    @ApiModelProperty(value="平台状态(1:草稿,2:已推送)")
    private Integer dyStatus;

    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 是否删除(0否 1是)
     */
    @ApiModelProperty(value="是否删除(0否 1是)")
    private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

}
