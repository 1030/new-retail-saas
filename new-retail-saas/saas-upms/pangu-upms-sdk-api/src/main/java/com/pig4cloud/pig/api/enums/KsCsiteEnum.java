package com.pig4cloud.pig.api.enums;

import com.pig4cloud.pig.common.core.util.NumberUtil;
import org.apache.commons.lang3.StringUtils;

public enum KsCsiteEnum {

	SMART_INVENTORY("1" ,"优选广告"),
	INFORMATION_FLOW_OLD("2" ,"信息流广告（旧投放场景，含上下滑大屏广告）"),
	VIDEO_PLAYPAGE("3" ,"视频播放页广告"),
	ALLIANCE("5" ,"联盟广告"),
	UP_AND_DOWN_LARGE_SCREEN("6" ,"上下滑大屏广告"),
	INFORMATION_FLOW_NEW("7" ,"信息流广告（不含上下滑大屏广告）"),
	SCREEN_OPENING_POSITION("27" ,"开屏位置"),
	SEARCH("39" ,"搜索广告"),
	;

	KsCsiteEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	/**
	 * 类型
	 */
	private String type;
	/**
	 * 名称
	 */
	private String name;


	public static String getNameByType(String type){
		if (StringUtils.isBlank(type) || !NumberUtil.isNumeric(type)){
			return null;
		}
		for(KsCsiteEnum ksCsiteEnum: KsCsiteEnum.values()){
			if(ksCsiteEnum.getType().equals(type)){
				return ksCsiteEnum.getName();
			}
		}
		return null;
	}


	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
