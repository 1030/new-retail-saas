package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 广点通品牌形象表
 * gdt_brand_image
 *
 * @author nml
 * @date 2020-12-04 15:06:12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "gdt_brand_image")
public class GdtBrandImage extends Model<GdtBrandImage> {
	/**
	 *
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "id")
	private Long id;

	/**
	 * 运营素材主键
	 */
	@TableField(value = "material_id")
	@ApiModelProperty(value = "materialId")
	private Long materialId;

	/**
	 * 广点通图片id
	 */
	@ApiModelProperty
	@TableField(value = "image_id")
	private String imageId;

	/**
	 * 广点通广告账户id
	 */
	@ApiModelProperty
	@TableField(value = "account_id")
	private String accountId;

	/**
	 * 广点通图片名称
	 */
	@ApiModelProperty
	@TableField(value = "name")
	private String name;

	/**
	 * 广点通图片宽
	 */
	@ApiModelProperty
	@TableField(value = "width")
	private String width;

	/**
	 * 广点通图片高
	 */
	@ApiModelProperty
	@TableField(value = "height")
	private String height;

	/**
	 * 广点通图片访问地址
	 */
	@ApiModelProperty
	@TableField(value = "image_url")
	private String imageUrl;

	/**
	 * 广点通创建时间
	 */
	@ApiModelProperty
	@TableField(value = "created_time")
	private String createdTime;

	/**
	 * 图片文件存储名称
	 */
	@ApiModelProperty
	@TableField(value = "pic_filename")
	private String picFilename;

	/**
	 * 真实路径
	 */
	@ApiModelProperty
	@TableField(value = "real_path")
	private String realPath;

	/**
	 * 图片本地访问地址
	 */
	@ApiModelProperty
	@TableField(value = "pic_url")
	private String picUrl;

	/**
	 * 图片md5值
	 */
	@ApiModelProperty
	@TableField(value = "md5")
	private String md5;

	/**
	 * 图片大小
	 */
	@ApiModelProperty
	@TableField(value = "pic_size")
	private String picSize;

	/**
	 * 图片格式
	 */
	@ApiModelProperty
	@TableField(value = "format")
	private String format;

	/**
	 * 平台ID，1:头条；8：广点通
	 */
	@ApiModelProperty
	@TableField(value = "platform_id")
	private Integer platformId;

	/**
	 * 项目ID
	 */
	@ApiModelProperty
	@TableField(value = "project_id")
	private Integer projectId;

	/**
	 * 头条平台素材ID
	 */
	@ApiModelProperty
	@TableField(value = "tt_material_id")
	private Integer ttMaterialId;

	/**
	 * 是否删除：0正常，1删除
	 */
	@ApiModelProperty
	@TableField(value = "isdelete")
	private Integer isdelete;

	/**
	 * 备注
	 */
	@ApiModelProperty
	@TableField(value = "remark")
	private String remark;

	/**
	 * 创建时间
	 */
	@ApiModelProperty
	@TableField(value = "createtime", fill = FieldFill.INSERT)
	private Date createtime;

	/**
	 * 创建人
	 */
	@ApiModelProperty
	@TableField(value = "createuser")
	private String createuser;

	/**
	 * 修改时间
	 */
	@ApiModelProperty
	@TableField(value = "updatetime", fill = FieldFill.UPDATE)
	private Date updatetime;

	/**
	 * 修改人
	 */
	@ApiModelProperty
	@TableField(value = "updateuser")
	private String updateuser;

	public GdtBrandImage setId(Long id) {
		this.id = id;
		return this;
	}

	public GdtBrandImage setMaterialId(Long materialId) {
		this.materialId = materialId;
		return this;
	}

	public GdtBrandImage setImageId(String imageId) {
		this.imageId = imageId;
		return this;
	}

	public GdtBrandImage setAccountId(String accountId) {
		this.accountId = accountId;
		return this;
	}

	public GdtBrandImage setName(String name) {
		this.name = name;
		return this;
	}

	public GdtBrandImage setWidth(String width) {
		this.width = width;
		return this;
	}

	public GdtBrandImage setHeight(String height) {
		this.height = height;
		return this;
	}

	public GdtBrandImage setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
		return this;
	}

	public GdtBrandImage setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
		return this;
	}

	public GdtBrandImage setPicFilename(String picFilename) {
		this.picFilename = picFilename;
		return this;
	}

	public GdtBrandImage setRealPath(String realPath) {
		this.realPath = realPath;
		return this;
	}

	public GdtBrandImage setPicUrl(String picUrl) {
		this.picUrl = picUrl;
		return this;
	}

	public GdtBrandImage setMd5(String md5) {
		this.md5 = md5;
		return this;
	}

	public GdtBrandImage setPicSize(String picSize) {
		this.picSize = picSize;
		return this;
	}

	public GdtBrandImage setFormat(String format) {
		this.format = format;
		return this;
	}

	public GdtBrandImage setPlatformId(Integer platformId) {
		this.platformId = platformId;
		return this;
	}

	public GdtBrandImage setProjectId(Integer projectId) {
		this.projectId = projectId;
		return this;
	}

	public GdtBrandImage setTtMaterialId(Integer ttMaterialId) {
		this.ttMaterialId = ttMaterialId;
		return this;
	}

	public GdtBrandImage setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
		return this;
	}

	public GdtBrandImage setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	public GdtBrandImage setCreatetime(Date createtime) {
		this.createtime = createtime;
		return this;
	}

	public GdtBrandImage setCreateuser(String createuser) {
		this.createuser = createuser;
		return this;
	}

	public GdtBrandImage setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
		return this;
	}

	public GdtBrandImage setUpdateuser(String updateuser) {
		this.updateuser = updateuser;
		return this;
	}

}