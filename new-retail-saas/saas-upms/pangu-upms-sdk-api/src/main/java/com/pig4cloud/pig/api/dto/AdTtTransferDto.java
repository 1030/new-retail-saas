package com.pig4cloud.pig.api.dto;

import lombok.Data;
import lombok.NonNull;

import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/7/15 11:33
 * @description：
 * @modified By：
 */
@Data
public class AdTtTransferDto {

	/**
	 * 目标账户id
	 */
	private Long advertiserId;

	/**
	 * 转出账户id
	 */
	private Long advertiserIdFrom;

	/**
	 * 授权的代理商id
	 */
	private Long agentId;


	/**
	 * 转账类型CASH：现金，GRANT：赠款
	 */
	private String transferType;

	/**
	 * 金额,单位(元),整数
	 */
	private Integer amount;


	/**
	 * 余额
	 */
	private BigDecimal balance;


}
