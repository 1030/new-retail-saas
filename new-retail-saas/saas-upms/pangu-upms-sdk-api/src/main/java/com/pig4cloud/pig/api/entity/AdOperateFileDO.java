package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 运营素材文件表
 *
 * @TableName ad_operate_file
 */
@Data
@EqualsAndHashCode
@TableName(value = "ad_operate_file")
public class AdOperateFileDO implements Serializable {

	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 文件名称
	 */
	private String fileName;

	/**
	 * 文件大小
	 */
	private Long fileSize;

	/**
	 * 文件类型
	 */
	private String fileType;

	/**
	 * 文件路径
	 */
	private String filePath;

	/**
	 * 文件散列值
	 */
	private String fileMd5;

	/**
	 * 运营素材主键
	 */
	private Long materialId;

	/**
	 * 修改者用户ID
	 */
	private Integer updateuser;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm.ss")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updatetime;

	public AdOperateFileDO setId(Long id) {
		this.id = id;
		return this;
	}

	public AdOperateFileDO setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}

	public AdOperateFileDO setFileSize(Long fileSize) {
		this.fileSize = fileSize;
		return this;
	}

	public AdOperateFileDO setFileType(String fileType) {
		this.fileType = fileType;
		return this;
	}

	public AdOperateFileDO setFilePath(String filePath) {
		this.filePath = filePath;
		return this;
	}

	public AdOperateFileDO setFileMd5(String fileMd5) {
		this.fileMd5 = fileMd5;
		return this;
	}

	public AdOperateFileDO setMaterialId(Long materialId) {
		this.materialId = materialId;
		return this;
	}

	public AdOperateFileDO setUpdateuser(Integer updateuser) {
		this.updateuser = updateuser;
		return this;
	}

	public AdOperateFileDO setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
		return this;
	}
}