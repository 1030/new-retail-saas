package com.pig4cloud.pig.api.util;

public interface AdvConstants {
	/**
	 * 设备 最低价格
	 */
	Integer LaunchPriceMin = 0;

	/**
	 * 设备 最高价格
	 */
	Integer LaunchPriceMax = 11000;


	final static String AD_HIDE_COLS_ = "ad_hide_cols_";

}
