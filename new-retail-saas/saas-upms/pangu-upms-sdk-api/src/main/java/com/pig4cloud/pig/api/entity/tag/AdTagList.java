package com.pig4cloud.pig.api.entity.tag;


import lombok.Data;

import java.io.Serializable;

@Data
public class AdTagList implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * id
	 */
	private Long id;
	/**
	 * 标签名称
	 */
	private String tagName;
	/**
	 * 标签颜色
	 */
	private String tagColor;

}
