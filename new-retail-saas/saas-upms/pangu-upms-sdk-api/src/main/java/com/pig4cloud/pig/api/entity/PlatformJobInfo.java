package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 平台任务表 platform_job_info
 * 
 * @author hma
 * @date 2020-11-16
 */
@Setter
@Getter
public class PlatformJobInfo
{
	private static final long serialVersionUID = 1L;
	
	/**  */
	@TableId(value="id",type= IdType.AUTO)
	private Long id;
	/** 唯一标识（对应不同操作类型的唯一标识-------例如说修改账号日限额对应的账号主键id） */
	private Long uniqueId;
	/** 任务执行CRON */
	private String jobCron;
	/**  */
	private String jobDesc;
	/**  */
	private Date createTime;
	/**  */
	private Date updateTime;
	/** 执行时间（针对执行一次） */
	private Date executeTime;
	/** 执行器任务参数 */
	private String executorParam;

	/** 调度状态：0-停止，1-运行 */
	private Integer triggerStatus;
	/** 类型：1 头条  2 广点通 */
	private Integer type;
	/** 操作类型： 1 修改广告账号日限额  2 头条 广告组日限额 修改 3 头条 广告计划 日限额修改 */
	private Integer operateType;



	public PlatformJobInfo(){}

	public PlatformJobInfo(Long uniqueId,Integer operateType,Integer type){
		this.uniqueId=uniqueId;
		this.operateType=operateType;
		this.type=type;

	}


	public PlatformJobInfo(Long uniqueId,String jobDesc,String jobCron,Date executeTime,String executorParam,Integer triggerStatus,Integer operateType,Integer type){
		this.uniqueId=uniqueId;
		this.jobDesc=jobDesc;
		this.jobCron=jobCron;
		this.executeTime=executeTime;
		this.executorParam=executorParam;
		this.triggerStatus=triggerStatus;
		this.operateType=operateType;
		this.type=type;
	}

	public PlatformJobInfo(Long uniqueId,String executorParam,String jobCron,Date executeTime,Integer triggerStatus,Long id){
		this.uniqueId=uniqueId;
		this.executorParam=executorParam;
		this.jobCron=jobCron;
		this.executeTime=executeTime;
		this.triggerStatus=triggerStatus;
		this.id=id;
	}

	
}
