package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 广告计划临时表 ad_plan_temp
 * 
 * @author hma
 * @date 2020-11-18
 */
@Setter
@Getter
public class AdPlanTemp
{
	private static final long serialVersionUID = 1L;
	@TableId(value = "id",type = IdType.AUTO)
	/** 主键id */
	private Long id;
	/** 计划id */
	private Long adId;
	/** 用户id */
	private Long userId;
	/** 广告计划名称 */
	private String adName;
	/** 推广组主键id */
	private Long campaignId;
	/** 状态：AD_STATUS_DELIVERY_OK投放中AD_STATUS_DISABLE计划暂停AD_STATUS_AUDIT新建审核中AD_STATUS_REAUDIT修改审核中AD_STATUS_DONE已完成（投放达到结束时间）AD_STATUS_CREATE计划新建AD_STATUS_AUDIT_DENY审核不通过AD_STATUS_BALANCE_EXCEED账户余额不足AD_STATUS_BUDGET_EXCEED超出预算AD_STATUS_NOT_START未到达投放时间AD_STATUS_NO_SCHEDULE不在投放时段AD_STATUS_CAMPAIGN_DISABLE已被广告组暂停AD_STATUS_CAMPAIGN_EXCEED广告组超出预算AD_STATUS_DELETE已删除AD_STATUS_ALL所有包含已删除AD_STATUS_NOT_DELETE */
	private String status;
	/** 投放范围 ： 通投智选(UNIVERSAL)  默认(DEFAULT) 穿山甲(UNION) */
	private String deliveryRange;
	/** 投放形式： 激励视频(REWARDED_VIDEO) 原生(ORIGINAL_VIDEO) 开屏(SPLASH_VIDEO) */
	private String unionVideoType;

	/** 下载方式：DOWNLOAD_URL下载链接，QUICK_APP_URL快应用+下载链接，EXTERNAL_URL落地页链接 */
	private String downloadType;
	/** 下载地址 */
	private String downloadUrl;
	/** 落地页链接 */
	private String externalUrl;
	/** 应用下载详情链接 */
	private String quickAppUrl;
	/** 定向包主键id */
	@TableField(updateStrategy = FieldStrategy.IGNORED)
	private Long audienceId;
	/** 转化目标主键id */
	private Long convertId;
	/** 投放时间类型："SCHEDULE_FROM_NOW"从今天起长期投放, "SCHEDULE_START_END"设置开始和结束日期 */
	private String scheduleType;
	/** 投放开始时间 */
	private String startTime;
	/** 投放结束时间 */
	private String endTime;

	/** 竞价策略 FLOW_CONTROL_MODE_FAST 优先跑量, FLOW_CONTROL_MODE_SMOOTH  优先低成本,FLOW_CONTROL_MODE_BALANCE"均衡投放 */
	private String flowControlMode;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date modifyTime;
	/** 投放场景：SMART_BID_CUSTOM常规投放，SMART_BID_CONSERVATIVE放量投放 */
	private String smartBidType;
	/** 预算类型：取值: "BUDGET_MODE_DAY"日预算, "BUDGET_MODE_TOTAL"总预算 */
	private String budgetMode;
	/** 预算(出价方式为CPC、CPM、CPV时，不少于100元；出价方式为OCPM、OCPC时，不少于300元；取值范围: ≥ 0 */
	private BigDecimal budget;
	/** 营销链路存储广告投放位置（首选媒体），非营销链路存储 创意首选投放位置(营销链路的创意已经移除) */
	private String inventoryType;
	/**
	 * 广告位大类。允许值 MANUAL首选媒体 SCENE场景广告位，SMART优选广告位，UNIVERSAL通投智选
	 */
	private String inventoryCatalog;
	/**
	 * 优选广告位 允许值NORMAL表示不使用优选，SMART表示使用优选，UNIVERSAL表示通投
	 */
	private String smartInventory;
	/**
	 * 场景广告位
	 */
	private String sceneInventory;
	/** 投放时段 */
	private String scheduleTime;

	// target作废-已迁移至广告组表 ad_campaign.marketing_purpose
	/** 投放目标：CPC 转化量 */
	private String target;

	/** 付费方式 ：按展示付费 OCPM, 按点击付费  OCPC */
	private String pricing;
	/** 点击出价/展示出价，当pricing为"CPC"、"CPM"、"CPA"出价方式时必填 */
	private BigDecimal bid=new BigDecimal("0.00");
	/** 目标转化出价/预期成本， 当pricing为"OCPM"、"OCPC"出价方式时必填） */
	private BigDecimal cpaBid=new BigDecimal("0.00");
	/** 地域定向省市或者区县列表，number[] 数组字符串  */
	private String city;
	/** 商圈ID数组 number[] 数组字符串  */
	private String businessIds;
	/** 地域允许值: "CITY"省市, "COUNTY"区县, "BUSINESS_DISTRICT"商圈,"NONE"不限 */
	private String district;
	/** 位置类型允许值：CURRENT正在该地区的用户，HOME居住在该地区的用户，TRAVEL到该地区旅行的用户，ALL该地区内的所有用户当city和district有值时必填 */
	private String locationType;
	/** 性别："GENDER_FEMALE", "GENDER_MALE", "NONE"  */
	private String gender;
	/** 受众年龄区间, 允许值: AGE_BETWEEN_18_23,AGE_BETWEEN_24_30,AGE_BETWEEN_31_40, AGE_BETWEEN_41_49,AGE_ABOVE_50 */
	private String age;
	/** 自定义人群 ：1 不限 2  自定义人群  */
	private Integer retargetingTags;
	/** 定向人群包列表（自定义人群），内容为人群包id */
	private String retargetingTagsInclude;
	/** 排除人群包列表（自定义人群），内容为人群包id */
	private String retargetingTagsExclude;
	/** 行为兴趣允许值："UNLIMITED"不限,"CUSTOM"自定义,"RECOMMEND"系统推荐 */
	private String interestActionMode;
	/** 行为场景 */
	private String actionScene;
	/** 用户发生行为天数，当interest_action_mode传CUSTOM时有效允许值：7, 15, 30, 60, 90, 180, 365 */
	private String actionDays;
	/** 行为类目词 */
	private String actionCategories;
	/** 行为关键词 */
	private String actionWords;
	/** 兴趣类目词，当interest_action_mode传CUSTOM时有效 */
	private String interestCategories;
	/** 兴趣关键词, 传入具体的词id，非兴趣词包id， */
	private String interestWords;
	/** 平台  1 ANDROID/ 2 IOS/ 3 PC/4 IPAD/ 5 WAP */
	private String platform;
	/** 受众网络类型：0 unknown 不限 /1 WIFI/2 2G/3 3G/4 4G/5 5G */
	private String ac;
	/** 已安装用户:0 表示不限，1表示过滤，2表示定向 */
	private Integer hideIfExists;
	/** 过滤已转化用户           广告计划       广告组       广告账户        公司账户 */
	private String  hideIfConverted="NO_EXCLUDE";
	/** 手机品牌类型：1 不限  ，2 按品牌 */
	private Integer deviceBrandType=1;
	/** 手机品牌，用逗号隔开的手机品牌列表 */
	private String deviceBrand;
	/** 手机价格类型 1 不限 2 自定义 */
	private Integer launchPriceType=1;
	/** 手机价格定向,  区间起始 */
	private Integer launchPriceFrom;
	/** 手机价格定向,  区间上线 */
	private Integer launchPriceTo;
	/** 是否启用智能放量 ：0表示关闭，1表示开启 */
	private Integer autoExtendEnabled=0;
	/** 设备类型 允许值是："MOBILE", "PAD"。缺省表示不限设备类型 */
	private String deviceType;
	/** 媒体定向 :NONE 不限,  GAME  游戏优质媒体 ,CONSTOM 自定义 */
	@TableField(updateStrategy = FieldStrategy.IGNORED)
	private String superiorPopularityType;
	/** 定向逻辑  */
	private Integer flowPackage;
	/** 排除定向逻辑 */
	private Integer excludeFlowPackage;

	@TableField("package")
	private String downPackage;

	private String  landingType="APP";
	private String appType="APP_ANDROID";

	/**
	 * 	是否调整自动出价 默认值为：0 （cpa_bid由系统自动计算） ，1 （当cpaBid 不为空的时候值为1 ）  todo 关于数据库新增该字段
	 */
	private String  adjustCpa="0";

	//转化来源类型
	private String convertSourceType;

	//转化类型
	private String convertType;

	//深度转化类型(不启用，无深度优化)
	@TableField(updateStrategy = FieldStrategy.IGNORED)
	private String deepExternalAction;

	//深度优化方式
	@TableField(updateStrategy = FieldStrategy.IGNORED)
	private String deepBidType;

	//深度优化出价，deep_bid_type为"DEEP_BID_MIN"时必填。当对应的转化convert_id，设定深度转化目标时才会有效。
	//DEEP_BID_MIN即自定义双出价（手动出价方式下），  即smartBidType传SMART_BID_CUSTOM常规投放
	//取值范围：0.1~10000元
	private BigDecimal deepCpabid;

	//深度转化ROI系数, 范围(0,5]，精度：保留小数点后四位, deep_bid_type为"ROI_COEFFICIENT"时必填
	private BigDecimal roiGoal;

	/**
	 * 子游戏ID
	 */
	private Long gameId;

	/**
	 * 应用ID
	 */
	private Long appId;

	/**
	 * 监测链接组ID集合，逗号分割
	 */
	private String trackUrlGroupId;

	/**
	 * 资产ID集合,逗号分割
	 */
	private String assetIds;


	public AdPlanTemp(){}
	public AdPlanTemp(Long id){
		this.id=id;
	}
	
}
