package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoAndroidVersionEnum {

	//安卓版本：受众最低android版本  允许值: "0.0", "2.0", "2.1", "2.2", "2.3", "3.0", "3.1", "3.2", "4.0","4.1","4.2", "4.3", "4.4", "4.5", "5.0", "NONE"
	v0_0( "0.0"  ,"不限"),
	v2_0( "2.0"  ,"2.0"),
	v2_1( "2.1"  ,"2.1"),
	v2_2( "2.2"  ,"2.2"),
	v2_3( "2.3"  ,"2.3"),
	v3_0( "3.0"  ,"3.0"),
	v3_1( "3.1"  ,"3.1"),
	v3_2( "3.2"  ,"3.2"),
	v4_0( "4.0"  ,"4.0"),
	v4_1( "4.1"  ,"4.1"),
	v4_2( "4.2"  ,"4.2"),
	v4_3( "4.3"  ,"4.3"),
	v4_4( "4.4"  ,"4.4"),
	v4_5( "4.5"  ,"4.5"),
	v5_0( "5.0"  ,"5.0"),
	v5_1( "5.1",  "5.1" ),
	v6_0( "6.0",  "6.0" ),
	v7_0( "7.0",  "7.0" ),
	v7_1( "7.1",  "7.1" ),
	v8_0( "8.0",  "8.0" ),
	v8_1( "8.1",  "8.1" ),
	v9_0( "9.0",  "9.0" );



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoAndroidVersionEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoAndroidVersionEnum item : ToutiaoAndroidVersionEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoAndroidVersionEnum item : ToutiaoAndroidVersionEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoAndroidVersionEnum item : ToutiaoAndroidVersionEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
