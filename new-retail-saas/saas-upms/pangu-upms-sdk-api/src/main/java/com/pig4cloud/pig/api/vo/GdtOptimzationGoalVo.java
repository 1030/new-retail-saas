package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.util.List;
@Data
public class GdtOptimzationGoalVo {
	private String key;
	private String value;
	private List<GdtOptimzationGoalVo> options;
}
