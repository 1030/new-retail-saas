package com.pig4cloud.pig.api.gdt.vo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * @projectName:pig
 * @description:广点通推广计划列表返回数据
 * @author:yk
 * @createTime:2020/12/16
 * @version:1.0
 */
@Getter
@Setter
public class GdtCampaignVo extends Page
{
	/** 开始时间*/
	private String sdate;
	/** 结束时间*/
	private String edate;

	private String campaignName;
	/**
	 * 多个广告主id
	 */
	private String[] accountIds;

	private List<String> accountList;

	/*客户设置的状态*/
	private String configuredStatus;

	/**
	 * 推广计划主键id
	 */
	private Long campaignId;

	/** 推广计划日预算，单位为分，设置为 0 表示不设预算（即不限） */
	private BigDecimal dailyBudget;

}
