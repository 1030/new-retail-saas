package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoDevicePriceEnum {

	//500,1000,1500,2000,2500,3000,3500,4000,5000,6000,7000,8000,9000,10000,11000
	P500  ("500"  , "500"     ),
	P1000 ("1000" , "1000"    ),
	P1500 ("1500" , "1500"    ),
	P2000 ("2000" , "2000"    ),
	P2500 ("2500" , "2500"    ),
	P3000 ("3000" , "3000"    ),
	P3500 ("3500" , "3500"    ),
	P4000 ("4000" , "4000"    ),
	P5000 ("5000" , "5000"    ),
	P6000 ("6000" , "6000"    ),
	P7000 ("7000" , "7000"    ),
	P8000 ("8000" , "8000"    ),
	P9000 ("9000" , "9000"    ),
	P10000("10000", "10000"   ),
	P11000("11000", "11000"   );



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoDevicePriceEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoDevicePriceEnum item : ToutiaoDevicePriceEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoDevicePriceEnum item : ToutiaoDevicePriceEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoDevicePriceEnum item : ToutiaoDevicePriceEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
