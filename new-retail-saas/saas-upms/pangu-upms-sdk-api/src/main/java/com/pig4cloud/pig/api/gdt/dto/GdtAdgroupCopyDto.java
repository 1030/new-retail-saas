package com.pig4cloud.pig.api.gdt.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 广点通-广告组临时参数dto
 * 
 * @author zxm
 * @date 2021-01-21
 */
@Data
public class GdtAdgroupCopyDto implements Serializable {

	private static final long serialVersionUID = 8612322506653153316L;

	@NotBlank(message = "广告账户不能为空")
	private String accountId;

	@NotBlank(message = "应用ID不能为空")
	private String promotedObjectId;

	/** 推广计划 id */
	@NotNull(message = "推广计划ID不能为空")
	private Long campaignId;

	/**
	 * 广告组名称（广告名称）
	 */
	@NotBlank(message = "广告名称不能为空")
	@Size(max=60, message = "广告名称长度不允许超过60个长度")
	private String adgroupName;

}
