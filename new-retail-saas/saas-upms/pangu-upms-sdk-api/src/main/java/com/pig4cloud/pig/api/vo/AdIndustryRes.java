package com.pig4cloud.pig.api.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 返回值
 * @date 2022/2/8 16:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdIndustryRes implements Serializable {

	@ApiModelProperty(value = "行业ID")
	private Long industryId;

	@ApiModelProperty(value = "行业名称")
	private String industryName;

	@ApiModelProperty(value = "级别(1:一级行业 2:二级行业 3:三级行业)")
	private Integer level;

	@ApiModelProperty(value = "一级行业ID")
	private Long firstIndustryId;

	@ApiModelProperty(value = "一级行业名称")
	private String firstIndustryName;

	@ApiModelProperty(value = "二级行业ID")
	private Long secondIndustryId;

	@ApiModelProperty(value = "二级行业名称")
	private String secondIndustryName;

	@ApiModelProperty(value = "三级行业ID")
	private Long thirdIndustryId;

	@ApiModelProperty(value = "三级行业名称")
	private String thirdIndustryName;

	@ApiModelProperty(value = "子数据集合")
	private List<AdIndustryRes> children = null;
}
