package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName(value = "ad_aweme_banned")
public class AdAwemeBannedEntity {

	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	@TableField("aweme_id")
	private String aweme_id;

	@TableField("aweme_name")
	private String aweme_name;

	@TableField("banned_type")
	private String banned_type;

	@TableField("nickname_keyword")
	private String nickname_keyword;

	@TableField("advertiser_id")
	private String advertiser_id;

	@TableField("advertiser_name")
	private String advertiser_name;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField("create_date")
	private Date create_date;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField("update_date")
	private Date update_date;

	@TableField("is_deleted")
	private Integer is_deleted;

	public AdAwemeBannedEntity setId(Long id) {
		this.id = id;
		return this;
	}

	public AdAwemeBannedEntity setAweme_id(String aweme_id) {
		this.aweme_id = aweme_id;
		return this;
	}

	public AdAwemeBannedEntity setAwemeId(String aweme_id) {
		this.aweme_id = aweme_id;
		return this;
	}

	public AdAwemeBannedEntity setAweme_name(String aweme_name) {
		this.aweme_name = aweme_name;
		return this;
	}

	public AdAwemeBannedEntity setAwemeName(String aweme_name) {
		this.aweme_name = aweme_name;
		return this;
	}

	public AdAwemeBannedEntity setBanned_type(String banned_type) {
		this.banned_type = banned_type;
		return this;
	}

	public AdAwemeBannedEntity setBannedType(String banned_type) {
		this.banned_type = banned_type;
		return this;
	}

	public AdAwemeBannedEntity setNickname_keyword(String nickname_keyword) {
		this.nickname_keyword = nickname_keyword;
		return this;
	}

	public AdAwemeBannedEntity setNicknameKeyword(String nickname_keyword) {
		this.nickname_keyword = nickname_keyword;
		return this;
	}

	public AdAwemeBannedEntity setAdvertiser_id(String advertiser_id) {
		this.advertiser_id = advertiser_id;
		return this;
	}

	public AdAwemeBannedEntity setAdvertiserId(String advertiser_id) {
		this.advertiser_id = advertiser_id;
		return this;
	}

	public AdAwemeBannedEntity setAdvertiser_name(String advertiser_name) {
		this.advertiser_name = advertiser_name;
		return this;
	}

	public AdAwemeBannedEntity setAdvertiserName(String advertiser_name) {
		this.advertiser_name = advertiser_name;
		return this;
	}

	public AdAwemeBannedEntity setCreate_date(Date create_date) {
		this.create_date = create_date;
		return this;
	}

	public AdAwemeBannedEntity setCreateDate(Date create_date) {
		this.create_date = create_date;
		return this;
	}

	public AdAwemeBannedEntity setUpdate_date(Date update_date) {
		this.update_date = update_date;
		return this;
	}

	public AdAwemeBannedEntity setUpdateDate(Date update_date) {
		this.update_date = update_date;
		return this;
	}

	public AdAwemeBannedEntity setIs_deleted(Integer is_deleted) {
		this.is_deleted = is_deleted;
		return this;
	}

	public AdAwemeBannedEntity setIsDeleted(Integer is_deleted) {
		this.is_deleted = is_deleted;
		return this;
	}
}
