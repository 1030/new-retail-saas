package com.pig4cloud.pig.api.vo;

import com.alibaba.fastjson.JSONArray;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

/**
 * 监测链接组表
 * @author  chenxiang
 * @version  2022-04-06 13:31:02
 * table: ad_assets_track
 */
@Data
public class AdAssetsTrackReq extends Page {
	/**
	 * 广告账户ID
	 */
	private String advertiserId;
	/**
	 * 应用ID
	 */
	private String appCloudId;

}
