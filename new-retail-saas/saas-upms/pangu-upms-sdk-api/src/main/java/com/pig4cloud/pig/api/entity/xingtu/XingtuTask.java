package com.pig4cloud.pig.api.entity.xingtu;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 星图任务表
 * @author  chengang
 * @version  2021-12-13 16:47:48
 * table: xingtu_task
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "xingtu_task")
public class XingtuTask extends Model<XingtuTask>{

	//columns START
			//主键id
			@TableId(value = "id",type = IdType.AUTO)
			private Long id;
			

			//星图广告账户ID
			@TableField(value = "advertiser_id")
			private String advertiserId;
			

			//组件类型
			@TableField(value = "component_type")
			@JSONField(name = "component_type")
			private String componentType;
			

			//任务创建时间
			@TableField(value = "task_create_time")
			@JSONField(name = "create_time")
			private String taskCreateTime;
			

			//任务ID
			@TableField(value = "demand_id")
			@JSONField(name = "demand_id")
			private String demandId;
			

			//任务名称
			@TableField(value = "demand_name")
			@JSONField(name = "demand_name")
			private String demandName;
			

			//结算方式
			@TableField(value = "universal_settlement_type")
			@JSONField(name = "universal_settlement_type")
			private String universalSettlementType;
			

			//是否删除  0否 1是
			@TableField(value = "deleted")
			private Integer deleted;
			

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;
			

			//创建人
			@TableField(value = "create_id")
			private Long createId;
			

			//修改人
			@TableField(value = "update_id")
			private Long updateId;
			

	//columns END 数据库字段结束
	

	
}

	

	
	

