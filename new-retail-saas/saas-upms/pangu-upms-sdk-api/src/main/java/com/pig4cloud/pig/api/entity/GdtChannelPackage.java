/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Objects;

/**
 * 广点通渠道包
 * @date 2021年7月12日20:12:42
 */
@Data
@TableName("gdt_channel_packages")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "广点通渠道包")
public class GdtChannelPackage extends Model<GdtChannelPackage> {
private static final long serialVersionUID = 1L;
    /**
     * 主键ID
     */
	@TableId(value="id",type= IdType.AUTO)
    @ApiModelProperty(value="主键ID")
    private Long id;
    /**
     * 广告账户
     */
    @ApiModelProperty(value="广告账户")
    private Long adId;
	/**
	 * 广告账户
	 */
	@ApiModelProperty(value="分包编码")
	private String appChl;
    /**
     * 广告账户
     */
    @ApiModelProperty(value="广告账户")
    private String adAccount;
    /**
     * 广告包 APP id
     */
    @ApiModelProperty(value="广告包 APP id")
    private String unionAppId;
    /**
     * 渠道包名称
     */
    @ApiModelProperty(value="渠道包名称")
    private String packageName;
    /**
     * 渠道包下载 url(必须以.apk 后缀结尾)
     */
    @ApiModelProperty(value="渠道包下载 url(必须以.apk 后缀结尾)")
    private String packageOriginUrl;
    /**
     * 用户自定义渠道包
     */
    @ApiModelProperty(value="用户自定义渠道包")
    private String customizedChannelId;
    /**
     * 安卓应用渠道包 id
     */
    @ApiModelProperty(value="安卓应用渠道包 id")
    private String channelPackageId;
    /**
     * 渠道包在系统中的状态
     */
    @ApiModelProperty(value="渠道包在系统中的状态")
    private String packageStatus;
    /**
     * 同步状态翻译信息
     */
    @ApiModelProperty(value="同步状态翻译信息")
    private String packageSynMsg;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value="修改时间")
    private Date updateTime;
}
