package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 广点通-推广计划表 gtd_campaign
 * 
 * @author hma
 * @date 2020-12-03
 */
@Setter
@Getter
public class GdtCampaign
{
	private static final long serialVersionUID = 1L;
	@TableId(value = "campaign_id")
	/** 推广计划id */
	private Long campaignId;
	/** 推广计划名称 */
	private String campaignName;
	/** 广告账号id */
	private Long accountId;
	/** 客户设置的状态 */
	private String configuredStatus;
	/** 推广计划类型 */
	private String campaignType;
	/** 业务类型（企业类型）：001 动游科技 */
	private String businessType;
	/** 日元算单位为分：值为0代表不限 */
	private BigDecimal dailyBudget;
	/** 日预算到达日期，值为最近一次触达日预算的日期，如无触达记录则为空，如：20170501 */
	private Long budgetReachDate;
	/** 推广目标类型【录入字段表】 */
	private String promotedObjectType;
	/**  */
	private Long createdTime;
	/**  */
	private Long lastModifiedTime;
	/** 投放速度模式：微信公众账号逻辑 仅支持 SPEED_MODE_STANDARD，不支持加速 */
	private String speedMode;
	/** 是否已删除，true：是，false：否 */
	private String isDeleted;
	private Long userId;

	public GdtCampaign(){}
	public GdtCampaign(Long campaignId){
		this.campaignId=campaignId;

	}
	public GdtCampaign(String campaignName){
		this.campaignName=campaignName;

	}

	public GdtCampaign(String campaignName,Long accountId){
		this.campaignName=campaignName;
		this.accountId=accountId;

	}


	public GdtCampaign(Long accountId,String isDeleted){
		this.accountId=accountId;
		this.isDeleted=isDeleted;
	}
}
