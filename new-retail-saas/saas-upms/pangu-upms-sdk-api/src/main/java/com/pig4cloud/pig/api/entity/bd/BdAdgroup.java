package com.pig4cloud.pig.api.entity.bd;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 百度推广单元表(bd_adgroup)实体类
 *
 * @author zjz
 * @since 2022-12-27 18:35:43
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("bd_adgroup")
public class BdAdgroup extends Model<BdAdgroup> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
	private Long id;
    /**
     * 信息流计划Id
     */
    private Long campaignFeedId;


    private String advertiserId;
    /**
     * 推广单元ID
     */
    private Long adgroupFeedId;
    /**
     * 推广单元名称
     */
    private String adgroupFeedName;
    /**
     * 暂停/启用推广单元
     */
    private Integer pause;
    /**
     * 推广单元状态 0 - 有效 1 - 暂停推广 2 -  推广计划暂停推广 3 -  直播结束后暂停
     */
    private Integer status;
    /**
     * 定向设置
     */
    private String audience;
    /**
     * 出价
     */
    private BigDecimal bid;
    /**
     * 投放范围 1 - 自定义类-百度信息流 2 - 自定义类-贴吧 4 - 百青藤 8 - 自定义类-好看视频 64 - 自定义类-百度小说
     */
    private String ftypes;
    /**
     * 优化目标和付费模式
     */
    private Integer bidtype;
    /**
     * 定向设置
     */
    private String ocpc;
    /**
     * 定向包ID
     */
    private Long atpFeedId;
    /**
     * 投放场景 0 - 不限 1 - 开屏
     */
    private String deliveryType;
    /**
     * 低效单元标识 1 - 低效单元 0 - 非低效单元
     */
    private Integer unefficientAdgroup;
    /**
     * 商品组id
     */
    private Long productSetId;
    /**
     * 来源(1:API,2:渠道后台)
     */
    private Integer soucreStatus;
    /**
     * 是否删除  0否 1是
     */
    private Integer deleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(update = "now()")
	private Date updateTime;
    /**
     * 创建人
     */
    private Long createId;
    /**
     * 修改人
     */
    private Long updateId;

}