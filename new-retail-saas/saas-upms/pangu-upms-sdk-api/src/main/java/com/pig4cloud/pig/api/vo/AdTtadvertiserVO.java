package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/7/17 11:03
 * @description：
 * @modified By：
 */
@Data
public class AdTtadvertiserVO {

	private String advertiserId;

	//现金可用余额
	private BigDecimal validCash;

	//赠款可用余额
	private BigDecimal validGrant;
}
