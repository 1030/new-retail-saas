/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.util.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 素材标签
 *
 * @author cx
 * @date 2021-06-29 14:27:26
 */
@Data
public class AdLabelVo extends Page {
    /**
     * id
     */
    private String id;
	/**
	 * 标签ID（批量）
	 */
	@NotNull(message = "未获取到标签")
	private String labelIds;
    /**
     * 标签名称
     */
    private String labelName;
	/**
	 * 标签关联素材id
	 */
	@NotNull(message = "未获取到素材ID")
	private String relateId;
    /**
     * 类型：1视频，2图片
     */
	@NotNull(message = "未获取到类型")
    private String type;
}
