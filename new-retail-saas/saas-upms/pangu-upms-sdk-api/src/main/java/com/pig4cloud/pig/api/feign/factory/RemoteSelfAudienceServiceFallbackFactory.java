package com.pig4cloud.pig.api.feign.factory;

import com.pig4cloud.pig.api.feign.RemoteSelfAudienceService;
import com.pig4cloud.pig.api.feign.fallback.RemoteSelfAudienceServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author chengang
 * @Date 2022/7/18
 */
@Component
public class RemoteSelfAudienceServiceFallbackFactory implements FallbackFactory<RemoteSelfAudienceService> {

	@Override
	public RemoteSelfAudienceService create(Throwable throwable) {
		RemoteSelfAudienceServiceFallbackImpl selfAudienceServiceFallback = new RemoteSelfAudienceServiceFallbackImpl();
		selfAudienceServiceFallback.setCause(throwable);
		return selfAudienceServiceFallback;
	}
}
