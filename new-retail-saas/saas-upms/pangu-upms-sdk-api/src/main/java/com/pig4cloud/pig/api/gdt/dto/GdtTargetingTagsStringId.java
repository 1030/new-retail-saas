package com.pig4cloud.pig.api.gdt.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

@Data
public class GdtTargetingTagsStringId implements Serializable, Comparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1682833058412509865L;
	
	private String id;
	private String name;
    private String parent_id;
    private String parent_name;
    private String city_level;
    private boolean business = false;
    
    private Set<GdtTargetingTagsStringId> children = new TreeSet<>();
	
	
	@Override
	public int compareTo(Object o) {
	    if (!(o instanceof GdtTargetingTagsStringId))
	        throw new RuntimeException("不是GdtTargetingTags对象");
	    GdtTargetingTagsStringId p = (GdtTargetingTagsStringId) o;
	    
	    return this.id.compareTo(p.id);
	}

	public Set<GdtTargetingTagsStringId> getChildren() {
		if (children == null){
			children = new TreeSet<>();
		}
		return children;
	}
}
