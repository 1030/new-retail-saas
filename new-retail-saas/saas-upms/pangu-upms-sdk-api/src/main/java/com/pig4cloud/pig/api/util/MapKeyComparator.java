package com.pig4cloud.pig.api.util;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by hezz on 2019/5/20.
 */
public class MapKeyComparator implements Comparator<String>, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -851703926874329229L;

	@Override
    public int compare(String str1, String str2) {

        return str1.compareTo(str2);
    }
}
