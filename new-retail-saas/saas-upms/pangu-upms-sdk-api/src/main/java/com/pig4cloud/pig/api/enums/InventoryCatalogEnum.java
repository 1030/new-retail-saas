/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public enum InventoryCatalogEnum {

	// MANUAL首选媒体 SCENE场景广告位，SMART优选广告位，UNIVERSAL通投智选 UNIVERSAL_SMART新版通投
	MANUAL(0,"MANUAL"),
	SCENE(1,"SCENE"),
	SMART(2,"SMART"),
	UNIVERSAL(3,"UNIVERSAL"),
	UNIVERSAL_SMART(4,"UNIVERSAL_SMART")
	;


	private final Integer id;
	private final String value;

	public static String getValueById(Integer id){
		if (id == null){
			return null;
		}
		for (InventoryCatalogEnum item : InventoryCatalogEnum.values()) {
			if (id.equals(item.getId())) {
				return item.getValue();
			}
		}
		return null;

	}

}
