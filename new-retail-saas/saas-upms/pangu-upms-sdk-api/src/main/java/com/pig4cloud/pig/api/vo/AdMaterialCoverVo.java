/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.util.Page;
import lombok.Data;

/**
 * 素材视频封面图
 *
 * @author pigx code generator
 * @date 2021-06-23 14:32:25
 */
@Data
public class AdMaterialCoverVo extends Page {
    /**
     * 主键
     */
    private String id;
    /**
     * 关联的素材ID
     */
    private String materialId;
    /**
     * 图片名称
     */
    private String fileName;
    /**
     * 访问地址
     */
    private String fileUrl;
    /**
     * 真实路径
     */
    private String realPath;
    /**
     * 是否封面：1：是，0：否
     */
    private String isCover;
    /**
     * 图片md5值
     */
    private String md5;
    /**
     * 是否删除：0:正常；1：删除
     */
    private String status;
	/**
	 * 平台ID：1，头条，2广点通
	 */
	private String platformId;
	/**
	 * 封面id（批量）
	 */
	private String ids;
	/**
	 * 广告账户（批量）
	 */
	private String advertiserIds;
}
