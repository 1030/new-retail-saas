/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @用户账号绑定表
 * @author john
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="tt_adver_day_report")
public class AdverDayReport extends Model<AdverDayReport> {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "广告主ID")
	@TableField(value = "advertiser_id")
	private Long advertiserId;
	
	@TableField(exist = false)
	private String name;
	
	
	@ApiModelProperty(value = "日期")
	@TableField(value = "stat_datetime")
	private String statDatetime;
	
	
	@ApiModelProperty(value = "消耗")
	private BigDecimal cost;
	
	@ApiModelProperty(value = "展示数")
	private Long show;
	
	@ApiModelProperty(value = "展现数据-平均千次展现费用")
	@TableField(value = "avg_show_cost")
	private BigDecimal avgShowCost;
	
	@ApiModelProperty(value = "点击数")
	private Long click;
	
	@ApiModelProperty(value = "点击率")
	private BigDecimal ctr;
	
	@ApiModelProperty(value = "平均点击单价")
	@TableField(value = "avg_click_cost")
	private BigDecimal avgClickCost;
	
	@ApiModelProperty(value = "转化数")
	@TableField(value = "convert")
	private Long convert;
	
	
	@ApiModelProperty(value = "转化成本")
	@TableField(value = "convert_cost")
	private BigDecimal convertCost;
	
	
	@ApiModelProperty(value = "转化率")
	@TableField(value = "convert_rate")
	private BigDecimal convertRate;
	
	
	@ApiModelProperty(value = "最新版本号")
	private Long iver;
}
