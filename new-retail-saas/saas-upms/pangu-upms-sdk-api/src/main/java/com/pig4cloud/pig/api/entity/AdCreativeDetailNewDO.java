package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 广告创意详情表
 *
 * @author zxm
 * @date 2021-01-23
 */
@Data
@Accessors(chain = true)
public class AdCreativeDetailNewDO implements Serializable {

    private static final long serialVersionUID = 1928145920330911062L;

    @JSONField(name = "advertiser_id")
    private String advertiserId;

    @JSONField(name = "ad_id")
    private String adId;

    @JSONField(name = "ad_data")
    private DetailAdData adData;

    @JSONField(name = "creative_list")
    private String creativeList; //

    @JSONField(name = "creative")
    private String creative; //

}
