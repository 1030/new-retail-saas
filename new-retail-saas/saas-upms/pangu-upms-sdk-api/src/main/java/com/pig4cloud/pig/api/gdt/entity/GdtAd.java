package com.pig4cloud.pig.api.gdt.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 广告表 ad
 *
 * @author hma
 * @date 2020-12-05
 */
@Data
@Accessors(chain = true)
@TableName("gdt_ad")
public class GdtAd extends Model<GdtAd> implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 推广计划id
	 */
	@JSONField(name = "campaign_id")
	private Long campaignId;

	/**
	 * 广告组id
	 */
	@JSONField(name = "adgroup_id")
	private Long adgroupId;

	/**
	 * 广告id
	 */
	@JSONField(name = "ad_id")
	@TableId(value = "ad_id", type = IdType.NONE)
	private Long adId;

	/**
	 * 广告名称
	 */
	@JSONField(name = "ad_name")
	private String adName;

	/**
	 * 广告创意 id
	 */
	@JSONField(name = "adcreative_id")
	private Long adcreativeId;

	/**
	 * 广告创意
	 */
	@JSONField(name = "adcreative")
	private String adcreative;

	/**
	 * 客户设置的状态
	 */
	@JSONField(name = "configured_status")
	private String configuredStatus;

	/**
	 * 系统状态
	 */
	@JSONField(name = "system_status")
	private String systemStatus;

	/**
	 * 多版位的审核结果信息
	 */
	@JSONField(name = "audit_spec")
	private String auditSpec;

	/**
	 * 曝光监控地址
	 */
	@JSONField(name = "impression_tracking_url")
	private String impressionTrackingUrl;

	/**
	 * 点击监控地址
	 */
	@JSONField(name = "click_tracking_url")
	private String clickTrackingUrl;

	/**
	 * 是否支持赞转评
	 */
	@JSONField(name = "feeds_interaction_enabled")
	private String feedsInteractionEnabled;

	/**
	 * 审核消息
	 */
	@JSONField(name = "reject_message")
	private String rejectMessage;

	/**
	 * 是否已删除
	 */
	@JSONField(name = "is_deleted")
	private String isDeleted;

	/**
	 * 是否是动态创意广告自动生成的
	 */
	@JSONField(name = "is_dynamic_creative")
	private String isDynamicCreative;

	/**
	 * 创建时间
	 */
	@JSONField(name = "created_time")
	private String createdTime;

	/**
	 * 最后修改时间
	 */
	@JSONField(name = "last_modified_time")
	private String lastModifiedTime;

	/**
	 * 广告账号id
	 */
	@JSONField(name = "account_id")
	private String accountId;

	public GdtAd() {
	}

	public GdtAd(String accountId, Long campaignId, Long adgroupId, Long adcreativeId, String adName, String configuredStatus,
				 String impressionTrackingUrl, String clickTrackingUrl, String feedsInteractionEnabled) {
		this.accountId = accountId;
		this.campaignId = campaignId;
		this.adgroupId = adgroupId;
		this.adcreativeId = adcreativeId;
		this.adName = adName;
		this.configuredStatus = configuredStatus;
		this.impressionTrackingUrl = impressionTrackingUrl;
		this.clickTrackingUrl = clickTrackingUrl;
		this.feedsInteractionEnabled = feedsInteractionEnabled;
	}


}
