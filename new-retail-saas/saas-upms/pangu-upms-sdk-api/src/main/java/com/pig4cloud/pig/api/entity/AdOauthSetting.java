package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 媒体授权信息配置表
 * @author  chengang
 * @version  2022-12-13 14:32:04
 * table: ad_oauth_setting
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_oauth_setting")
public class AdOauthSetting extends Model<AdOauthSetting>{

		//主键id
		@TableId(value = "id",type = IdType.AUTO)
		private Long id;


		//渠道code 1头条 8广点通 9百度 10快手
		@TableField(value = "media_code")
		private String mediaCode;


		//开发者APPID
		@TableField(value = "app_id")
		private String appId;


		//开发者SECRET
		@TableField(value = "app_secret")
		private String appSecret;


		//应用状态机
		@TableField(value = "app_state")
		private String appState;


		//应用权限
		@TableField(value = "app_scope")
		private String appScope;


		//授权URL
		@TableField(value = "auth_url")
		private String authUrl;


		//回调URL
		@TableField(value = "redirect_uri")
		private String redirectUri;


		//授权类型
		@TableField(value = "oauth_type")
		private String oauthType;


		//预留字段1
		@TableField(value = "column_1")
		private String column1;


		//是否删除  0否 1是
		@TableField(value = "deleted")
		private Integer deleted;


		//创建时间
		@TableField(value = "create_time")
		private Date createTime;


		//修改时间
		@TableField(value = "update_time")
		private Date updateTime;


		//创建人
		@TableField(value = "create_id")
		private Long createId;


		//修改人
		@TableField(value = "update_id")
		private Long updateId;
	

	
}

	

	
	

