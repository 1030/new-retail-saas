package com.pig4cloud.pig.api.gdt.dto;

import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Getter;
import lombok.Setter;


/**
 * 广点通-广告组临时参数dto
 *
 * @author hma
 * @date 2020-12-05
 */
@Setter
@Getter
public class GdtAdgroupTempDto
{
	private static final long serialVersionUID = 1L;

	private Long userId;
	/**
	 * 广告账号
	 */
	private String accountId;

	/**
	 * 推广计划
	 */
	private Long campaignId;
	/**
	 * 推广目标
	 */
	private  String promotedObjectType;
	/**
	 * 广告版位 多个有逗号分隔
	 */
	private String siteSets;
	/**
	 * 广告版位 多个有逗号分隔
	 */
	private String siteSet;
	/**
	 * 投放目标id
	 */
	private String promotedObjectId;


	/**
	 * 投放目标id PAGE_TYPE_TSA_APP 、PAGE_TYPE_XIJING_QUICK
	 */
	private String pageType;

	/**
	 * 类型  1 ：查询条数返回 1
	 */
	private Integer type;
	public String getSiteSets(){
		return StringUtils.isNotBlank(siteSets)? siteSets = siteSets.replaceAll("&quot;","\""):siteSets;
	}
	public GdtAdgroupTempDto(){

	}

	public GdtAdgroupTempDto(Long userId,String accountId,Integer type){
		this.userId=userId;
		this.accountId=accountId;
		this.type=type;

	}
	/**
	 * 主键id
	 */
	private Long id;

	private Long adgroupId;
}
