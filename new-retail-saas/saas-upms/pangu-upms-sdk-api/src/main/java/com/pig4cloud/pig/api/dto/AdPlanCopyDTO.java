package com.pig4cloud.pig.api.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class AdPlanCopyDTO implements Serializable {

	private static final long serialVersionUID = -4566899623839439869L;


	@NotBlank(message = "广告账户不能为空")
	private String advertiserId;

	@NotBlank(message = "广告组不能为空")
	private String campaignId;

	@NotBlank(message = "广告计划不能为空")
	private String adId;

	@NotBlank(message = "广告计划名称不能为空")
	private String adName;

	/**
	 * 转化变更为事件，可以不存
	 */
//	@NotBlank(message = "转化跟踪不能为空")
	private String convertId;

	@NotBlank(message = "落地页不能为空")
	private String webUrl;

	/**
	 * 不启用，无深度优化  DEEP_BID_DEFAULT
	 */
	private String deepExternalAction;

	/**
	 * 转化目标
	 */
	@NotBlank(message = "转化目标不能为空")
	private String externalAction;

	/**
	 * 监测链接ID,单选
	 */
	@NotBlank(message = "转化不能为空")
	private String trackUrlGroupId;
}
