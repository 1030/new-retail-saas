package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 广告转化表
 * @author  chenxiang
 * @version  2022-05-23 10:35:26
 * table: ad_convert
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_convert")
public class AdConvert extends Model<AdConvert>{
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 对应advertiser_monitor_info主键
	 */
	@TableField(value = "ad_id")
	private Long adId;
	/**
	 * 父游戏ID
	 */
	@TableField(value = "pgame_id")
	private Long pgameId;
	/**
	 * 子游戏ID
	 */
	@TableField(value = "game_id")
	private Long gameId;
	/**
	 * 渠道code
	 */
	@TableField(value = "chl")
	private String chl;
	/**
	 * 渠道code 1头条 8广点通 10快手
	 */
	@TableField(value = "media_code")
	private String mediaCode;
	/**
	 * 广告账户
	 */
	@TableField(value = "advertiser_id")
	private String advertiserId;
	/**
	 * 第三方渠道转化ID
	 */
	@TableField(value = "convert_id")
	private String convertId;
	/**
	 * 转化名称
	 */
	@TableField(value = "convert_name")
	private String convertName;
	/**
	 * 转化目标
	 */
	@TableField(value = "convert_target")
	private String convertTarget;
	/**
	 * 深度转化目标
	 */
	@TableField(value = "deep_conversion_type")
	private String deepConversionType;
	/**
	 * 转化目标次数
	 */
	@TableField(value = "convert_count")
	private Long convertCount;
	/**
	 * 深度转化目标次数
	 */
	@TableField(value = "deep_conversion_count")
	private Long deepConversionCount;
	/**
	 * 点击监测链接
	 */
	@TableField(value = "action_track_url")
	private String actionTrackUrl;
	/**
	 * 点击监测链接
	 */
	@TableField(value = "xingtu_action_track_url")
	private String xingtuActionTrackUrl;
	/**
	 * 应用ID
	 */
	@TableField(value = "app_id")
	private Long appId;
	/**
	 * 应用名称
	 */
	@TableField(value = "app_name")
	private String appName;
	/**
	 * 应用包名
	 */
	@TableField(value = "package_name")
	private String packageName;
	/**
	 * 第三方创建时间
	 */
	@TableField(value = "third_create_time")
	private String thirdCreateTime;
	/**
	 * 第三方创建时间
	 */
	@TableField(value = "third_update_time")
	private String thirdUpdateTime;
	/**
	 * 转化来源
	 */
	@TableField(value = "convert_source_type")
	private String convertSourceType;
	/**
	 * 转化统计方式：ONLY_ONE（仅一次），EVERY_ONE（每一次）
	 */
	@TableField(value = "convert_data_type")
	private String convertDataType;
	/**
	 * 归因方式
	 */
	@TableField(value = "claim_type")
	private String claimType;
	/**
	 * 数据源 id
	 */
	@TableField(value = "user_action_set_id")
	private Long userActionSetId;
	/**
	 * 当前站点是否可用，true：是，false：否
	 */
	@TableField(value = "site_set_enable")
	private String siteSetEnable;
	/**
	 * 安卓应用渠道包 id
	 */
	@TableField(value = "app_android_channel_package_id")
	private String appAndroidChannelPackageId;
	/**
	 * 推广目标 id
	 */
	@TableField(value = "promoted_object_id")
	private String promotedObjectId;
	/**
	 * 转化场景
	 */
	@TableField(value = "conversion_scene")
	private String conversionScene;
	/**
	 * 转化状态
	 */
	@TableField(value = "status")
	private String status;
	/**
	 * 转化操作状态
	 */
	@TableField(value = "opt_status")
	private String optStatus;
	/**
	 * 来源(1:API,2:渠道后台)
	 */
	@TableField(value = "soucre_status")
	private Integer soucreStatus;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;

	public interface Status {
		public static final String ACCESS_STATUS_COMPLETED = "ACCESS_STATUS_COMPLETED";
		public static final String ACCESS_STATUS_PENDING = "ACCESS_STATUS_PENDING";
	}
}

	

	
	

