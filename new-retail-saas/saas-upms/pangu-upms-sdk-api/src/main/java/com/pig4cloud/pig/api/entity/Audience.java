package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * 	author:zhuxm
 *  广告计划广告受众
 */
@Data
public class Audience implements Serializable {
	private static final long serialVersionUID = 5139563421433133754L;

	/**
	 * 地域
	 */
	private String district;

	/**
	 * 地域定向省市或者区县列表(当传递省份ID时,旗下市县ID可省略不传),当district为"CITY"或"COUNTY"时有值
	 */
	private String city;

	/**
	 * 商圈ID数组，district为"BUSINESS_DISTRICT"时有值
	 */
	private String business_ids;

	/**
	 * 位置类型
	 */
	private String location_type;

	/**
	 * 性别
	 */
	private String gender;

	/**
	 * 年龄, 详见【附录-受众年龄区间】
	 * 取值: "AGE_BETWEEN_18_23", "AGE_BETWEEN_24_30","AGE_BETWEEN_31_40", "AGE_BETWEEN_41_49", "AGE_ABOVE_50"
	 */
	private String age;

	/**
	 * 定向人群包列表（自定义人群），内容为人群包id。如果选择"同时定向与排除"，需传入retargeting_tags_include和retargeting_tags_exclude
	 */
	private String retargeting_tags_include;

	/**
	 * 排除人群包列表（自定义人群），内容为人群包id。如果选择"同时定向与排除"，需传入retargeting_tags_include和retargeting_tags_exclude
	 */
	private String retargeting_tags_exclude;

	/**
	 * 行为兴趣
	 * 取值："UNLIMITED"不限,"CUSTOM"自定义,"RECOMMEND"系统推荐。若与自定义人群同时使用，系统推荐("RECOMMEND")不生效
	 * 仅推广范围为默认时可填，且不可与老版行为兴趣定向同时填写，否则会报错
	 */
	private String interest_action_mode;

	/**
	 * 解决选择新建定向时行为内容没有解析的BUG
	 */
	@TableField(exist = false)
	private String action;

	private String action_scene;

	private Integer action_days;

	private String action_categories;

	private String action_words;

	private String interest_categories;

	private String interest_words;

	private String platform;

	private String ac;

	private String device_type;

	private String device_brand;

	private String launch_price;

	private Integer auto_extend_enabled;

	private String superior_popularity_type;

	private String flow_package;

	private String exclude_flow_package;


}
