package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoAdStatusEnum {
	AD_STATUS_ENABLE("AD_STATUS_ENABLE","启用"),
	AD_STATUS_DISABLE("AD_STATUS_DISABLE","计划暂停"),
	AD_STATUS_CAMPAIGN_DISABLE("AD_STATUS_CAMPAIGN_DISABLE","已被广告组暂停"),
	AD_STATUS_DELIVERY_OK("AD_STATUS_DELIVERY_OK","投放中"),
	AD_STATUS_REAUDIT("AD_STATUS_REAUDIT","修改审核中"),
	AD_STATUS_AUDIT("AD_STATUS_AUDIT","新建审核中"),
	AD_STATUS_DONE("AD_STATUS_DONE","已完成（投放达到结束时间）"),
	AD_STATUS_CREATE("AD_STATUS_CREATE","计划新建"),
	AD_STATUS_AUDIT_DENY("AD_STATUS_AUDIT_DENY","审核不通过"),

	AD_STATUS_BALANCE_EXCEED("AD_STATUS_BALANCE_EXCEED","账户余额不足"),
	AD_STATUS_BUDGET_EXCEED("AD_STATUS_BUDGET_EXCEED","超出预算"),
	AD_STATUS_NOT_START("AD_STATUS_NOT_START","未到达投放时间"),
	AD_STATUS_NO_SCHEDULE("AD_STATUS_NO_SCHEDULE","不在投放时段"),
	AD_STATUS_DELETE("AD_STATUS_DELETE","已删除"),
	AD_STATUS_CAMPAIGN_EXCEED("AD_STATUS_CAMPAIGN_EXCEED","广告组超出预算"),
	AD_STATUS_ADVERTISER_BUDGET_EXCEED("AD_STATUS_ADVERTISER_BUDGET_EXCEED",
			"超出广告主日预算"),
	AD_STATUS_ALL("AD_STATUS_ALL","所有状态包含已删除"),
	AD_STATUS_NOT_DELETE("AD_STATUS_NOT_DELETE","所有不包含已删除"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_LIVE_CANNOT_LANUCH("AD_STATUS_LIVE_CANNOT_LANUCH",
			"关联直播间不可投放"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_CAMPAIGN_PRE_OFFLINE_BUDGET("AD_STATUS_CAMPAIGN_PRE_OFFLINE_BUDGET",
			"广告组接近预算"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_PRE_OFFLINE_BUDGET("AD_STATUS_PRE_OFFLINE_BUDGET",
			"广告接近预算"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_PRE_ONLINE("AD_STATUS_PRE_ONLINE",
			"预上线"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_DSP_AD_DISABLE("AD_STATUS_DSP_AD_DISABLE",
			"已被广告计划暂停"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_AUDIT_STATUS_ERROR("AD_STATUS_AUDIT_STATUS_ERROR",
			"异常，请联系审核人员"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_DRAFT("AD_STATUS_DRAFT",
			"草稿"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_ADVERTISER_PRE_OFFLINE_BUDGET("AD_STATUS_ADVERTISER_PRE_OFFLINE_BUDGET",
			"账户接近预算"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_CANNOT_EDIT("AD_STATUS_CANNOT_EDIT",
			"不可编辑"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_EXTERNAL_URL_DISABLE("AD_STATUS_EXTERNAL_URL_DISABLE",
			"落地页暂不可用"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_LIVE_ROOM_OFF("AD_STATUS_LIVE_ROOM_OFF",
			"关联直播间未开播"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_AWEME_ACCOUNT_PUNISHED("AD_STATUS_AWEME_ACCOUNT_PUNISHED",
			"关联抖音账号封禁不可投放"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_AWEME_ACCOUNT_DISABLED("AD_STATUS_AWEME_ACCOUNT_DISABLED",
			"关联抖音号不可投"),
	//第三方文档说明：该枚举值仅用作广告计划投放状态展示，不可在状态过滤时使用
	AD_STATUS_PRODUCT_OFFLINE("AD_STATUS_PRODUCT_OFFLINE",
			"关联商品不可投");
	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoAdStatusEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoAdStatusEnum item : ToutiaoAdStatusEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoAdStatusEnum item : ToutiaoAdStatusEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoAdStatusEnum item : ToutiaoAdStatusEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
