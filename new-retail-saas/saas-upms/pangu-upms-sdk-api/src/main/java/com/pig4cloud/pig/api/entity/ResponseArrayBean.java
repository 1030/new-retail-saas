package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.JSONArray;

/**
 * 巨量引擎api接口返回消息类
 *
 * @author gaozhi0
 * 2019-04-25 14:55
 **/
public class ResponseArrayBean {

    /**
     * 0  成功
     */
    private String  code;

    /**
     * 
     */
    private  String message;

    /**
    *  返回数据,多个接口返回值的此字段结构不同，取值时用JSONObject
    **/
    protected JSONArray data;
    
    private String request_id;
    
    public ResponseArrayBean() {}
    
    public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public JSONArray getData() {
		return data;
	}


	public void setData(JSONArray data) {
		this.data = data;
	}


	public String getRequest_id() {
		return request_id;
	}


	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}


	
}
