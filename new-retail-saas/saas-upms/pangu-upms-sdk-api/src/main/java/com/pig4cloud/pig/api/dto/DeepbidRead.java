package com.pig4cloud.pig.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

@Data
public class DeepbidRead implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//广告账号
	@JSONField(name = "advertiser_id")
	private String advertiserId;

	//广告组
	@JSONField(name = "campaign_id")
	private String campaignId;

	//深度转化目标  DEEP_BID_DEFAULT不能传
	@JSONField(name = "deep_external_action")
	private String deepExternalAction;

	//广告位置 （穿山甲等）
	@JSONField(name = "delivery_range")
	private String deliveryRange;

	//转化类型（目前传空）
	@JSONField(name = "external_action")
	private String externalAction;

	//投放场景：FLOW_CONTROL_MODE_FAST优先跑量（对应CPC的加速投放）FLOW_CONTROL_MODE_SMOOTH优先低成本（对应CPC的标准投放）FLOW_CONTROL_MODE_BALANCE均衡投放（新增字段）
	@JSONField(name = "flow_control_mode")
	private String flowControlMode;

	//转化来源类型  投放场景：常规投放SMART_BID_CUSTOM 放量投放 SMART_BID_CONSERVATIVE
	@JSONField(name = "smart_bid_type")
	private String smartBidType;

	//资产ID
	private String assetId;

	//事件类型
	private String eventType;

}
