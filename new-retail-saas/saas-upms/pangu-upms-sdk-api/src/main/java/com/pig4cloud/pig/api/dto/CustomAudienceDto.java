package com.pig4cloud.pig.api.dto;

import lombok.Data;

@Data
public class CustomAudienceDto {
	private String advertiserId;
	
	/**
	 * @人群包名称或id
	 */
	private String customAudienceId;
	

	private Integer pageNum;

	private Integer pageSize = 100;
}
