package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * picture_lib
 * @author nml
 * @date 2020-11-03 19:20:33
 */
@Getter
@Setter
public class PictureLibVo extends Page {

	/*
	*  批量删除图片pids
	* */
	private String pids;

    /**
	 * 主键id
     */
    private Integer id;

    /**
     * 图片名称：前端显示名称 可修改
     */
    private String pictureName;

    /**
     * 图片文件存储名称
     */
    private String pictureFilename;

	/*图片类型*/
	private Integer pictureType;

	private Integer sourceType;

	private Integer videoId;

    /**
     * 图片访问地址
     */
    private String pictureUrl;

	private String realPath;

	private String md5;

	private String width;

	private String height;

	private Integer screenType;

	private String pSize;

	private String format;

    /**
     * 图片尺寸
     */
    private String pictureSize;

    /**
     * 图片是否删除：0:删除；1：未删除
     */
    private String pictureStatus;

    /**
     * 标签id 可修改
     */
    private String labelId;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /*平台id*/
	private String platformId;

	/**
	 * 平台类型1，头条，2广点通
	 */
	private Integer platformType;

	//广告账户id
	private String advertiserId;

	//广告账户ids
	private String[] advertiserIds;

	// 广告账户列表
	private List<String> adList;

	//广点通广告账户列表
	private List<String> gdtAdverList;

	//头条广告账户列表
	private List<String> ttAdverList;

	private String labelname;//标签名称

	private Integer p_type;//类型
}