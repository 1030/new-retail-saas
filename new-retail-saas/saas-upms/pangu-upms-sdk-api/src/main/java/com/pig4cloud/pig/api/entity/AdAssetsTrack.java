package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 监测链接组表
 * @author  chenxiang
 * @version  2022-04-06 13:31:02
 * table: ad_assets_track
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_assets_track")
public class AdAssetsTrack extends Model<AdAssetsTrack>{
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 渠道code 1头条 8广点通 10快手
	 */
	@TableField(value = "media_code")
	private Integer mediaCode;
	/**
	 * 广告账户
	 */
	@TableField(value = "advertiser_id")
	private String advertiserId;
	/**
	 * 资产ID
	 */
	@TableField(value = "asset_id")
	private Long assetId;
	/**
	 * 监测链接组ID
	 */
	@TableField(value = "track_url_group_id")
	private Long trackUrlGroupId;
	/**
	 * 监测链接组名称
	 */
	@TableField(value = "track_url_group_name")
	private String trackUrlGroupName;
	/**
	 * 包名
	 */
	@TableField(value = "package_name")
	private String packageName;
	/**
	 * 应用ID
	 */
	@TableField(value = "app_cloud_id")
	private Long appCloudId;
	/**
	 * 应用名
	 */
	@TableField(value = "app_name")
	private String appName;
	/**
	 * 父游戏ID
	 */
	@TableField(value = "pgame_id")
	private Long pgameId;
	/**
	 * 子游戏ID
	 */
	@TableField(value = "game_id")
	private Long gameId;
	/**
	 * 渠道code
	 */
	@TableField(value = "chl")
	private String chl;
	/**
	 * 对应maindb中advertiser_monitor_info主键
	 */
	@TableField(value = "ad_id")
	private Long adId;
	/**
	 * 应用下载链接
	 */
	@TableField(value = "download_url")
	private String downloadUrl;
	/**
	 * 点击监测链接
	 */
	@TableField(value = "action_track_url")
	private String actionTrackUrl;
	/**
	 * 展示监测链接
	 */
	@TableField(value = "track_url")
	private String trackUrl;
	/**
	 * 视频播放监测链接
	 */
	@TableField(value = "video_play_track_url")
	private String videoPlayTrackUrl;
	/**
	 * 视频播完监测链接
	 */
	@TableField(value = "video_play_done_track_url")
	private String videoPlayDoneTrackUrl;
	/**
	 * 视频有效播放监测链接
	 */
	@TableField(value = "video_play_effective_track_url")
	private String videoPlayEffectiveTrackUrl;
	/**
	 * 来源(1:API,2:渠道后台)
	 */
	@TableField(value = "soucre_status")
	private Integer soucreStatus;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

