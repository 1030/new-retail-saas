/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.util.Collection;

/**
 * 素材库 - 报表
 *
 * @date 2021年7月17日14:56:54
 */
@Data
public class AdMaterialReportVo {
	/**
	 * 素材ID集合
	 */
	private String materialIds;

	private String pig;

	private String clickhouse;

	private String sdate;

	private String edate;

	private String parentchl;

	private Integer osType;

	private Integer materialType;

	private Integer creatorId;

	private Integer makerId;

	private Long pgid;

	private Integer makeType;

	private String materialName;

	private String videoWidth;

	private String videoHeight;

	private Integer period;

	private Collection<String> groupBys;

}
