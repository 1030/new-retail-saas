/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.util.List;

/**
 * 广告账户表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
@Data
public class AdAccountTreeResp {
    /**
     * 广告账户ID
     */
    private String advertiserId;
    /**
     * 广告账户名称
     */
    private String advertiserName;
	/**
	 * 媒体编码
	 */
	private String mediaCode;
	/**
	 * 子级
	 */
    private List<AdAccountTreeResp> children;

}
