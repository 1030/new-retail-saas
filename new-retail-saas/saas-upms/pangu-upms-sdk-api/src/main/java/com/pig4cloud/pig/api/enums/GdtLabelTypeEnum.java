package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum  GdtLabelTypeEnum {
    BEHAVIOR("BEHAVIOR","行为"),
    INTEREST("INTEREST","兴趣"),
    INTENTION("INTENTION","意向");
    private String type;
    private String name;
    GdtLabelTypeEnum(String type,String name){
        this.type = type;
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    /**
     * 通过value    取type
     *
     * @return
     */
    public static String typeByValue(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        for (GdtLabelTypeEnum item : GdtLabelTypeEnum.values()) {
            if (item.getName().equals(value)) {
                return item.getType();
            }
        }
        return null;

    }

    /**
     * 通过type   取value
     *
     * @return
     */
    public static String valueByType(String type) {
        if (StringUtils.isBlank(type)) {
            return null;
        }

        for (GdtLabelTypeEnum item : GdtLabelTypeEnum.values()) {
            if (item.getType().equals(type)) {
                return item.getName();
            }
        }
        return null;

    }

    public static boolean containsType(String type) {
        if (StringUtils.isBlank(type)) {
            return false;
        }

        for (GdtLabelTypeEnum item : GdtLabelTypeEnum.values()) {
            if (item.getType().equals(type)) {
                return true;
            }
        }
        return false;
    }
}
