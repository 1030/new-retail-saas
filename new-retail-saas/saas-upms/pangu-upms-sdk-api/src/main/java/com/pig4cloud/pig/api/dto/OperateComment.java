package com.pig4cloud.pig.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OperateComment {
	private Long comment_id;//评论id
	private Long advertiser_id;//广告主id
	private String ad_account;//账户id
	private Long reply_comment_id;//二级评论父评论id
	private Integer rank;//级别
}
