package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoArticleCategoryEnum {

	ENTERTAINMENT ("ENTERTAINMENT"  ,  "娱乐"     ),
	SOCIETY       ("SOCIETY"        ,  "社会"     ),
	CARS          ("CARS"           ,  "汽车"     ),
	INTERNATIONAL ("INTERNATIONAL"  ,  "国际"     ),
	HISTORY       ("HISTORY"        ,  "历史"     ),
	SPORTS        ("SPORTS"         ,  "体育"     ),
	HEALTH        ("HEALTH"         ,  "健康"     ),
	MILITARY      ("MILITARY"       ,  "军事"     ),
	EMOTION       ("EMOTION"        ,  "情感"     ),
	FASHION       ("FASHION"        ,  "时尚"     ),
	PARENTING     ("PARENTING"      ,  "育儿"     ),
	FINANCE       ("FINANCE"        ,  "财经"     ),
	AMUSEMENT     ("AMUSEMENT"      ,  "搞笑"     ),
	DIGITAL       ("DIGITAL"        ,  "数码"     ),
	EXPLORE       ("EXPLORE"        ,  "探索"     ),
	TRAVEL        ("TRAVEL"         ,  "旅游"     ),
	CONSTELLATION ("CONSTELLATION"  ,  "星座"     ),
	STORIES       ("STORIES"        ,  "故事"     ),
	TECHNOLOGY    ("TECHNOLOGY"     ,  "科技"     ),
	GOURMET       ("GOURMET"        ,  "美食"     ),
	CULTURE       ("CULTURE"        ,  "文化"     ),
	HOME          ("HOME"           ,  "家居"     ),
	MOVIE         ("MOVIE"          ,  "电影"     ),
	PETS          ("PETS"           ,  "宠物"     ),
	GAMES         ("GAMES"          ,  "游戏"     ),
	WEIGHT_LOSING ("WEIGHT_LOSING"  ,  "瘦身"     ),
	FREAK         ("FREAK"          ,  "奇葩"     ),
	EDUCATION     ("EDUCATION"      ,  "教育"     ),
	ESTATE        ("ESTATE"         ,  "房产"     ),
	SCIENCE       ("SCIENCE"        ,  "科学"     ),
	PHOTOGRAPHY   ("PHOTOGRAPHY"    ,  "摄影"     ),
	REGIMEN       ("REGIMEN"        ,  "养生"     ),
	ESSAY         ("ESSAY"          ,  "美文"     ),
	COLLECTION    ("COLLECTION"     ,  "收藏"     ),
	ANIMATION     ("ANIMATION"      ,  "动漫"     ),
	COMICS        ("COMICS"         ,  "漫画"     ),
	TIPS          ("TIPS"           ,  "小窍门"   ),
	DESIGN        ("DESIGN"         ,  "设计"     ),
	LOCAL         ("LOCAL"          ,  "本地"     ),
	LAWS          ("LAWS"           ,  "法制"     ),
	GOVERNMENT    ("GOVERNMENT"     ,  "政务"     ),
	BUSINESS      ("BUSINESS"       ,  "商业"     ),
	WORKPLACE     ("WORKPLACE"      ,  "职场"     ),
	RUMOR_CRACKER ("RUMOR_CRACKER"  ,  "辟谣"     ),
	GRADUATES     ("GRADUATES"      ,  "毕业生"   );



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoArticleCategoryEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoArticleCategoryEnum item : ToutiaoArticleCategoryEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoArticleCategoryEnum item : ToutiaoArticleCategoryEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoArticleCategoryEnum item : ToutiaoArticleCategoryEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
