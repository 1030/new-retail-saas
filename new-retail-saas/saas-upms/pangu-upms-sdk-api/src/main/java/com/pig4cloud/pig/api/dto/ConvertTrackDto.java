package com.pig4cloud.pig.api.dto;

import java.io.Serializable;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ConvertTrackDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idSearch;

	private String nameSearchStr;

	private String[] advertiserIds;

	private String advertiserId;

	/**
	 * �ƻ�id
	 *
	 * */
	private Long adId;

	private String gameId;


	private Long convertId;

	//ת����Դ����
	private String convertSourceType;

	//ת������
	private String convertType;

	//���ת��Ŀ��
	private String deepExternalAction;

	//转化统计方式
	private String convertDataType;

}
