package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * ad_launch_platform
 * @author nml
 * @date 2020-11-06 15:32:05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_picture_platform")
public class AdPicturePlatform extends Model<AdPicturePlatform> {

	/*
	 *  唯一id
	 * */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value = "id")
    private Long id;

    /**
     * 图片id
     */
	@ApiModelProperty(value = "图片id")
	@TableField(value = "picture_id")
    private Long pictureId;

	/**
	 * 图片类型: 1:小图;2:横版大图;3:竖版大图;4:gif图;0:其他类型;
	 */
	@ApiModelProperty
	@TableField(value = "picture_type")
	private Integer pictureType;

    /**
     * 平台id:1:tt;2:uc;3:gdt 
     */
	@ApiModelProperty
	@TableField(value = "platform_id")
    private Integer platformId;

    /**
     * 平台名称
     */
	@ApiModelProperty
	@TableField(value = "platform_name")
    private String platformName;

    /**
     * 广告账户id
     */
	@ApiModelProperty
	@TableField(value = "advertiser_id")
    private String advertiserId;

    /**
     * 第三方平台图片id
     */
	@ApiModelProperty
	@TableField(value = "platform_picture_id")
    private String platformPictureId;

    /**
     * 第三方平台图片size
     */
	@ApiModelProperty
	@TableField(value = "platform_picture_size")
    private Integer platformPictureSize;

    /**
     * 第三方平台图片width
     */
	@ApiModelProperty
	@TableField(value = "platform_picture_width")
    private Integer platformPictureWidth;

    /**
     * 第三方平台图片height
     */
	@ApiModelProperty
	@TableField(value = "platform_picture_height")
    private Integer platformPictureHeight;

    /**
     * 第三方平台图片预览地址(1h有效)
     */
	@ApiModelProperty
	@TableField(value = "platform_picture_url")
    private String platformPictureUrl;

    /**
     * 第三方平台图片格式
     */
	@ApiModelProperty
	@TableField(value = "platform_picture_format")
    private String platformPictureFormat;

    /**
     * 第三方平台图片md5
     */
	@ApiModelProperty
	@TableField(value = "platform_picture_signature")
    private String platformPictureSignature;

    /**
     * 第三方平台图片素材id
     */
	@ApiModelProperty
	@TableField(value = "platform_picture_material_id")
    private String platformPictureMaterialId;

    /**
     * 创建时间
     */
	@ApiModelProperty
	@TableField(value = "create_time",fill= FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
	@ApiModelProperty
	@TableField(value = "update_time",fill= FieldFill.UPDATE)
    private Date updateTime;
}