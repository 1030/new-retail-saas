package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoAdvCreativeFieldsEnum {

	//平台  1 ANDROID/ 2 IOS/ 3 PC/4 IPAD/ 5 WAP
	SMART_INVENTORY					("smart_inventory","优选广告位"),
	INVENTORY_TYPE 					("inventory_type","按媒体指定位置"),
	SCENE_INVENTORY					("scene_inventory","按场景指定位置"),
	CREATIVE_MATERIAL_MODE			("creative_material_mode","创意方式"),
	IMAGE_MODE							("image_mode","素材类型"),
	IMAGE_ID							("image_id","图片ID"),
	VIDEO_ID							("video_id","视频ID"),
	IMAGE_IDS							("image_ids","图片ID列表"),
	TITLE								("title","创意标题"),
	DERIVE_POSTER_CID					("derive_poster_cid","是否将视频的封面和标题同步到图片创意"),
	PRODUCT_IMAGE_ID					("product_image_id","卡片主图"),
	PRODUCT_DESCRIPTION				("product_description","卡片标题"),
	PRODUCT_SELLING_POINTS			("product_selling_points","推广卖点"),
	ENABLE_PERSONAL_ACTION			("enable_personal_action","是否使用智能优选"),
	CALL_TO_ACTION  					("call_to_action","推广卡片的行动号召"),
	IS_FEED_AND_FAV_SEE				("is_feed_and_fav_see","主页作品列表隐藏广告内容"),
	IES_CORE_USER_ID					("ies_core_user_id","推广抖音号"),
	WEB_URL								("web_url","应用下载详情页"),
	ACTION_TEXT 						("action_text","行动号召"),
	APP_NAME							("app_name","应用名"),
	SUB_TITLE							("sub_title","副标题"),
	IS_PRESENTED_VIDEO				("is_presented_video","自动生成视频素材"),
	GENERATE_DERIVED_AD				("generate_derived_ad","最优创意衍生计划"),
	IS_COMMENT_DISABLE				("is_comment_disable","广告评论"),
	REATIVE_DISPLAY_MODE				("reative_display_mode","创意展现"),
	THIRD_INDUSTRY_ID					("third_industry_id","创意分类"),
	AD_KEYWORDS						("ad_keywords","创意标签"),
	TRACK_URL							("track_url","展示"),
	ACTION_TRACK_URL					("action_track_url","点击"),
	VIDEO_PLAY_TRACK_URL				("video_play_track_url","视频播放"),
	VIDEO_PLAY_DONE_TRACK_URL		("video_play_done_track_url","视频播完"),
	VIDEO_PLAY_EFFECTIVE_TRACK_URL	("video_play_effective_track_url","视频有效播放");


	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoAdvCreativeFieldsEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoAdvCreativeFieldsEnum item : ToutiaoAdvCreativeFieldsEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoAdvCreativeFieldsEnum item : ToutiaoAdvCreativeFieldsEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoAdvCreativeFieldsEnum item : ToutiaoAdvCreativeFieldsEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
