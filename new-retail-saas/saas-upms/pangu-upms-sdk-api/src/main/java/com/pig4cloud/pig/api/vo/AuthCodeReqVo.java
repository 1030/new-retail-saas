package com.pig4cloud.pig.api.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class AuthCodeReqVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 522727844502361899L;
	
	private String auth_code;
	
	private String state;
	
	private String app_id;
	
	private String secret;
	
	private Integer userId;
	
	private Integer agentId;

}
