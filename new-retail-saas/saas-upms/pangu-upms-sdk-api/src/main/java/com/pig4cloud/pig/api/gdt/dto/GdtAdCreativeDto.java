package com.pig4cloud.pig.api.gdt.dto;

import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Getter;
import lombok.Setter;


/**
 * 广点通-广告创意对象dto
 *
 * @author hma
 * @date 2020-12-09
 */
@Setter
@Getter
public class GdtAdCreativeDto
{
	private static final long serialVersionUID = 1L;

	private Long userId;
	/**
	 * 广告账号
	 */
	private String accountId;

	/**
	 * 推广计划
	 */
	private Long campaignId;
	/**
	 * 推广目标
	 */
	private  String promotedObjectType;

	/**
	 * 广告版本 jonsarr数组
	 */
	private String siteSets;
	/**
	 * 广告版本 jonsarr数组
	 */
	private String siteSet;

	/**
	 * 创意形式主键id
	 */
	private Long  adcreativeTemplateId;

	/**
	 *设置为 1 时查询动态创意广告相关信息，设置为 0 时返回全部信息
	 */
	private String dynamicCreative="0";

	private Long adgroupId;

	/**
	 * 多个广告用逗号分隔
	 */
	private String adIds;

	private String adName;
	/**
	 * 创意对应内容
	 */
	private String adCreative;

	/**
	 * 1  查询已编辑完成的 创意预览 2  查询正在创建，编辑的创意预览
	 */
	private Integer type=2;
	/**
	 * 出价方式
	 */
	private String bidMode;

	private String isDynamicCreative;

	public String getSiteSets(){
		return StringUtils.isNotBlank(siteSets)? siteSets = siteSets.replaceAll("&quot;","\""):siteSets;
	}


	public GdtAdCreativeDto(){}
	public GdtAdCreativeDto(String promotedObjectType){
		this.promotedObjectType=promotedObjectType;
	}

}
