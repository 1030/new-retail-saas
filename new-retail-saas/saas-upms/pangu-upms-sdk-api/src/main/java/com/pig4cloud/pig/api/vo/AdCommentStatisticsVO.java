package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.util.Collection;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.api.vo
 * @Author 马嘉祺
 * @Date 2021/7/10 10:46
 * @Description
 */
@Data
public class AdCommentStatisticsVO extends Page {

	/**
	 * 开始创建时间
	 */
	private String startTimeStr;

	/**
	 * 结束创建时间
	 */
	private String endTimeStr;

	/**
	 * 媒体类型：1: 头条
	 */
	private Integer platformId;

	/**
	 * 子游戏ID集合
	 */
	private String gameidArr;

	/**
	 * 素材素材ID
	 */
	private Long materialId;

	/**
	 * 素材名称
	 */
	private String materialName;

	/**
	 * 模糊查询时的评论内容
	 */
	private String commentBlur;

	/**
	 * 汇总方式：0：汇总；1：按天
	 */
	private Integer summaryType;

	/**
	 * 分组字段，可选值：pgname，gname，adId，materialName
	 */
	private Collection<String> groupBys;

}
