package com.pig4cloud.pig.api.gdt.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

/**
 * @projectName:pig
 * @description:推广计划设置请求参数
 * @author:Zhihao
 * @createTime:2020/12/9 14:42
 * @version:1.0
 */
@Setter
@Getter
public class GdtUpdateCampaignDto extends Page {
	/**
	 * 广告账户Id
	 */
	private String accountId;
	/**
	 * 推广计划id
	 */
	private String campaignId;

	/**
	 * 推广预算开关 0--开启(AD_STATUS_NORMAL 有效)  1--关闭(AD_STATUS_SUSPEND 暂停)
	 */
	private Integer configuredStatus;

	/**
	 * 用户id
	 */
	private Long userId;
}
