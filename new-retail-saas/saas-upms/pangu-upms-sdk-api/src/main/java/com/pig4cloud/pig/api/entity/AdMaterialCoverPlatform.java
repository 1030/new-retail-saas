/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 封面图同步平台信息表
 *
 * @author pigx code generator
 * @date 2021-06-23 14:32:25
 */
@Data
@TableName("ad_material_cover_platform")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "封面图同步平台信息表")
public class AdMaterialCoverPlatform extends Model<AdMaterialCoverPlatform> {
private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id",type= IdType.AUTO)
    @ApiModelProperty(value="主键")
    private Integer id;
    /**
     * 封面图ID
     */
    @ApiModelProperty(value="封面图ID")
    private Integer coverId;
    /**
     * 平台ID（1：头条，2：广点通）
     */
    @ApiModelProperty(value="平台ID（1：头条，2：广点通）")
    private Integer platformId;
    /**
     * 平台广告账户
     */
    @ApiModelProperty(value="平台广告账户")
    private String advertiserId;
    /**
     * 平台文件ID
     */
    @ApiModelProperty(value="平台文件ID")
    private String platformFileId;
    /**
     * 平台素材ID
     */
    @ApiModelProperty(value="平台素材ID")
    private String platformMaterialId;
    /**
     * md5值
     */
    @ApiModelProperty(value="md5值")
    private String md5;
    /**
     * 同步人
     */
    @ApiModelProperty(value="同步人")
    private String createUser;
    /**
     * 同步时间
     */
    @ApiModelProperty(value="同步时间")
    private Date createTime;
    }
