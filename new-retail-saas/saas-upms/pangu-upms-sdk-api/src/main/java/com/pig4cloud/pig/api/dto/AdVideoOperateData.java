package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName AdVideoOperateData.java
 * @author cx
 * @version 1.0.0
 * @Time 2021/1/21 14:27
 */
@Data
public class AdVideoOperateData implements Serializable {

	private String active;

	private String register;

	private String newPayUserNumber;

	private String payNumber;

	private String newPayMoney;

	private String payMoney;

	private String worth0;

	private String worth3;

	private String worth7;

	private String worth15;

	private String worth30;

	private String num2;

	private String sumnum2;

	private String num3;

	private String sumnum3;

	private String num7;

	private String sumnum7;

	private String num15;

	private String sumnum15;

	private String num30;

	private String sumnum30;

}
