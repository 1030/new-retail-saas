package com.pig4cloud.pig.api.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @Author chengang
 * @Date 2021/7/13
 */
@Data
public class AdAccountWarningSettingDTO {

	@NotNull(message = "不活跃周期天数不能为空")
	@Min(value = 1,message = "最小值不能小于1")
	@Max(value = 999,message = "最大值不能大于999")
	private Integer days;

	@NotNull(message = "通知人员不能为空")
	private String users;

	@NotNull(message = "邮件发送时间不能为空")
	private String times;

}
