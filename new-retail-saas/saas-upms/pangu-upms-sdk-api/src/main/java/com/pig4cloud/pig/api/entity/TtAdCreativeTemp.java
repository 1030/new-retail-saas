package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 头条-广告创意-草稿表
 * tt_ad_creative_draft
 * @author kongyanfang
 * @date 2020-11-20 12:38:30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="tt_ad_creative_temp")
public class TtAdCreativeTemp extends Model<TtAdCreativeTemp> {
    /**
     * 自增的ID
     */
	@TableId(value="id",type= IdType.AUTO)
    private Long id;

    /**
     * 广告计划ID，计划ID要属于广告主ID
     */
    private Long adId;

    /**
     * 广告主ID
     */
    private Long advertiserId;

	/**
	 * 广告位大类。允许值 MANUAL首选媒体 SCENE场景广告位，SMART优选广告位，UNIVERSAL通投智选 UNIVERSAL_SMART新版通投
	 */
	@TableField(exist = false)
	private String inventoryCatalog;

	/**
	 * 已迁移至广告计划 -- NORMAL表示不使用优选，SMART表示使用优选，UNIVERSAL表示通投
	 *
     * 是否使用优选广告位     0表示不使用优选，1表示使用，2表示标记该创意隶属的计划投放范围是通投智选
     */
    private Byte smartInventory;

    /**
	 * 已迁移至广告计划
     * string[] 广告位置
     */
    private String inventoryType;

    /**
	 * 已迁移至广告计划
     * 场景广告位
     */
    private String sceneInventory;

	/**
	 * 新营销链路-落地页
	 */
	private String externalUrl;

    /**
     * 创意方式，当值为"STATIC_ASSEMBLE"表示程序化创意，其他情况不传字段
     */
    private String creativeMaterialMode;

    /**
     * 程序化创意包ID   不做
     */
    private Integer proceduralPackageId;

    /**
     * 是否开启衍生计划，  1为开启，0为不开启
     */
    private Byte generateDerivedAd;

    /**
     * 启用图片生成视频，  允许值：0（不启用），1（启用）
     */
    private Byte isPresentedVideo;

    /**
	 * 新营销链路统一存 creatives
     * object[]  json数组    最多包含12张图和10个视频 
     */
    private String imageList;

    /**
	 * 新营销链路统一存 creatives
     * object[]  json数组    最多包含10个标题 
     */
    private String titleList;

    /**
     * object[]  json数组    自定义创意 
     */
    private String creatives;

    /**
     * 广告来源
     */
    private String source;

    /**
     * 推广抖音号
     */
    private String iesCoreUserId;

    /**
     * 主页作品列表隐藏广告内容，默认值：0            允选值：0（不隐藏），1（隐藏）
     */
    private Byte isFeedAndFavSee;

    /**
     * 是否开启自动派生创意，大通投时可填             默认值: 1       允许值: 0(不启用), 1(启用)
     */
    private Byte creativeAutoGenerateSwitch;

    /**
     * 应用名  应用名 4到20个字符，必填
     */
    private String appName;

    /**
	 * 新营销链路统一存 creatives
     * APP 副标题     4到24个字符
     */
    private String subTitle;

    /**
     * 应用下载详情页
     */
    private String webUrl;

    /**
     * 行动号召（仅应用下载推广类型有效）
     */
    private String actionText;

    /**
     * 试玩素材URL
     */
    private String playableUrl;

    /**
     * 广告评论   0为开启，1为关闭，默认值：0
     */
    private Byte isCommentDisable;

    /**
     * 是否使用门店包，true为使用，false为不使用，推广目的非门店推广时会忽略该字段
     */
    private Byte enableStorePack;

    /**
     * string[] 推广卖点   商品卖点，   长度6-9个字，最多可填10个卖点
     */
    private String productSellingPoints;

    /**
     * 卡片标题  商品描述，长度1-7个字
     */
    private String productDescription;

    /**
     * 推广卡片的行动号召
     */
    private String callToAction;

    /**
     * 商品图片ID。对应广告投放平台推广卡片的卡片主图
     */
    private String productImageId;

    /**
     * 创意展现方式，默认为轮播模式     轮播模式  优选模式
     */
    private String creativeDisplayMode;

    /**
     * 三级行业ID
     */
    private Integer thirdIndustryId;

    /**
     * string[] 创意标签。最多20个标签，且每个标签长度不超过10个字符
     */
    private String adKeywords;

    /**
     * 展示（监测链接）
     */
    private String trackUrl;

    /**
     * 点击（监测链接）
     */
    private String actionTrackUrl;

    /**
     * 视频有效播放（监测链接）
     */
    private String videoPlayEffectiveTrackUrl;

    /**
     * 视频播完（监测链接）
     */
    private String videoPlayDoneTrackUrl;

    /**
     * 视频播放（监测链接）
     */
    private String videoPlayTrackUrl;

    /**
     * 数据发送方式，不可修改,默认值: SERVER_SEND
     */
    private String trackUrlSendType;

    /**
     * 平台类型 1 头条
     */
    private Short platformId;

    /**
     * 同步状态 0未同步/1同步成功/2同步失败
     */
    private Short syncStatus;

    /**
     * 转化跟踪在 第三方平台中的ID (头条/UC 的转化id)
     */
    private Long idAdPlatform;

    /**
     * 创建时间
     */
    private Date createtime;

	/**
	 * 是否使用智能优选，true为使用，false为不使用
	 */
	private String enablePersonalAction;

	/**
	 * 创建者 投放平台id
	 */
	private Integer creatorId;

	//默认为从临时表来的
	@TableField(exist = false)
	private Boolean fromTemp = true;

	private Long projectId;


	/**
	 * 判断新增还是更新：1 更新，0或其他 新增
	 *//*
	@TableField(exist = false)
	private String idStr;*/
	/*private String adName;
	private Integer adNum;
	private Integer adCreativeNum;
	private List<Long> convertTrackIdList;*/
}