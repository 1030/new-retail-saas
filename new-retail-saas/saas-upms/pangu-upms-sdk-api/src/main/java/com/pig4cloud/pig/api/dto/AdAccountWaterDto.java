package com.pig4cloud.pig.api.dto;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

/**
 * @ClassName TtAccountWaterDto
 * @Description todo
 * @Author nieml
 * @Time 2021/7/14 17:50
 * @Version 1.0
 **/
@Data
public class AdAccountWaterDto extends Page {

	/**
	 * 账号类型：头条：1 ；广点通账号：2
	 * */
	private Integer accountType;

	/**
	 * 广告主ID
	 */
	private Long accountId;

	/**
	 * 开始日期
	 */
	private String startDate;

	/**
	 * 截止日期
	 */
	private String endDate;

	/**
	 * 头条：流水类型，转账 TRANSFER、充值 RECHARGE
	 * 广点通：交易类型：CHARGE 充值;TRANSFER_IN 转入;PAY 消费;TRANSFER_BACK 回划;EXPIRE 过期;
	 */
	private String transactionType;

}
