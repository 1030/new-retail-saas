package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoCareerEnum {

	//受众运营商，允许值: MOBILE 移动/UNICOM 联通 / TELCOM 电信
	COLL_STUDENT  (  "926968303"   ,      "大学生"),
	TEACHER  (  "926968304"   ,      "教师"),
	IT  (  "927051338"   ,      "IT"),
	CIVIL_SERVANT  (  "926968305"   ,      "公务员"),
	FINANCIAL  (  "927138917"   ,      "金融"),
	MEDICAL_STAFF  (  "927138916"   ,      "医务人员"),;



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoCareerEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoCareerEnum item : ToutiaoCareerEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoCareerEnum item : ToutiaoCareerEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoCareerEnum item : ToutiaoCareerEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
