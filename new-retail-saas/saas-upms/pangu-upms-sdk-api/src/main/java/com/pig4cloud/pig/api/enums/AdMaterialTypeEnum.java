/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;


public enum AdMaterialTypeEnum {
	CREATIVE_IMAGE_MODE_SMALL("CREATIVE_IMAGE_MODE_SMALL", "小图", "image"),
	CREATIVE_IMAGE_MODE_LARGE("CREATIVE_IMAGE_MODE_LARGE", "大图", "image"),
	CREATIVE_IMAGE_MODE_GROUP("CREATIVE_IMAGE_MODE_GROUP", "组图", "image"),
	CREATIVE_IMAGE_MODE_GIF("CREATIVE_IMAGE_MODE_GIF", "GIF图", "image"),
	CREATIVE_IMAGE_MODE_LARGE_VERTICAL("CREATIVE_IMAGE_MODE_LARGE_VERTICAL", "大图竖图", "image"),
	TOUTIAO_SEARCH_AD_IMAGE("TOUTIAO_SEARCH_AD_IMAGE", "搜索大图", "image"),
	SEARCH_AD_SMALL_IMAGE("SEARCH_AD_SMALL_IMAGE", "搜索小图", "image"),
	CREATIVE_IMAGE_MODE_UNION_SPLASH("CREATIVE_IMAGE_MODE_UNION_SPLASH", "穿山甲开屏图片", "image"),
	CREATIVE_IMAGE_MODE_DISPLAY_WINDOW("CREATIVE_IMAGE_MODE_DISPLAY_WINDOW", "搜索橱窗", "image"),
	CREATIVE_IMAGE_MODE_VIDEO("CREATIVE_IMAGE_MODE_VIDEO", "横版视频", "video"),
	CREATIVE_IMAGE_MODE_VIDEO_VERTICAL("CREATIVE_IMAGE_MODE_VIDEO_VERTICAL", "竖版视频", "video"),
	CREATIVE_IMAGE_MODE_UNION_SPLASH_VIDEO("CREATIVE_IMAGE_MODE_UNION_SPLASH_VIDEO", "穿山甲开屏视频", "video");
	/**
	 * 平台ID
	 */
	private final String value;
	/**
	 * 描述
	 */
	private final String description;

	private final String code;

	//构造方法
	AdMaterialTypeEnum(String value, String description, String code) {
		this.value = value;
		this.description = description;
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * 通过value    取code
	 * @return
	 */
	public static String codeByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (AdMaterialTypeEnum item : AdMaterialTypeEnum.values()) {
			if (item.getValue().equals(value) ) {
				return item.getCode();
			}
		}
		return null;
	}
	/**
	 * 通过value    取描述
	 *
	 * @return
	 */
	public static String descByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (AdMaterialTypeEnum item : AdMaterialTypeEnum.values()) {
			if (item.getValue().equals(value)) {
				return item.getDescription();
			}
		}
		return null;

	}
	/**
	 * value 是否存在枚举中
	 *
	 * @return
	 */
	public static boolean containsType(String value) {
		if (StringUtils.isBlank(value)) {
			return false;
		}

		for (AdMaterialTypeEnum item : AdMaterialTypeEnum.values()) {
			if (item.getValue().equals(value)) {
				return true;
			}
		}
		return false;
	}
}
