/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @广告计预算预警阈值
 * @author john
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="tt_plan_cost_warning")
public class TtPlanCostWarning extends Model<TtPlanCostWarning> {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "广告账户id")
	@TableId(value = "advertiser_id")
	private String advertiserId;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField( fill = FieldFill.INSERT)
	private Date createtime;
	
	
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField( fill = FieldFill.UPDATE)
	private Date updatetime;
	
	
	@ApiModelProperty(value = "广告计划预算预警阈值")
	@TableField(value = "plan_cost_warning")
	private BigDecimal planCostWarning;
	
	@ApiModelProperty(value = "总越预警值 NULL不预警")
	@TableField(value = "alert_balance")
	private BigDecimal alertBalance;
	
}
