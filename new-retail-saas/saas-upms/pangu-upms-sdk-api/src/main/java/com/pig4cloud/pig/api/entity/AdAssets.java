package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 广告资产表
 * @author  chenxiang
 * @version  2022-04-01 15:02:28
 * table: ad_assets
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_assets")
public class AdAssets extends Model<AdAssets>{
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 资产ID
	 */
	@TableField(value = "asset_id")
	private Long assetId;
	/**
	 * 广告账户
	 */
	@TableField(value = "advertiser_id")
	private String advertiserId;
	/**
	 * 资产类型 THIRD_EXTERNAL 三方落地页、APP 应用、QUICK_APP 快应用
	 */
	@TableField(value = "asset_type")
	private String assetType;
	/**
	 * 应用包ID
	 */
	@TableField(value = "package_id")
	private String packageId;
	/**
	 * 包名
	 */
	@TableField(value = "package_name")
	private String packageName;
	/**
	 * appId
	 */
	@TableField(value = "app_cloud_id")
	private String appCloudId;
	/**
	 * 应用名
	 */
	@TableField(value = "app_name")
	private String appName;
	/**
	 * 应用类型，允许值：IOS、Android
	 */
	@TableField(value = "app_type")
	private String appType;
	/**
	 * 下载地址
	 */
	@TableField(value = "download_url")
	private String downloadUrl;
	/**
	 * 应用资产名
	 */
	@TableField(value = "asset_name")
	private String assetName;
	/**
	 * 来源(1:API,2:渠道后台)
	 */
	@TableField(value = "soucre_status")
	private Integer soucreStatus;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

