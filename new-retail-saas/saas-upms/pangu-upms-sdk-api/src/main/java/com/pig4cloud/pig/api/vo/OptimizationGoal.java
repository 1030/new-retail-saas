package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author chengang
 * @Date 2022/5/19
 */
@Data
public class OptimizationGoal implements Serializable {

	private String name;

	private String value;


}
