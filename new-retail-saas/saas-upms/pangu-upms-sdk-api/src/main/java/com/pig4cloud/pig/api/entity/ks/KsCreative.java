/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.api.entity.ks;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 快手创意信息表
 *
 * @author yuwenfeng
 * @date 2022-03-26 17:31:08
 */
@Data
@TableName("ks_creative")
@ApiModel(value = "快手创意信息表")
public class KsCreative implements Serializable {

    /**
     * 主键ID
     */
    @TableId
    @ApiModelProperty(value="主键ID")
    private Long id;

    /**
     * 广告创意ID
     */
    @ApiModelProperty(value="广告创意ID")
    private Long creativeId;

    /**
     * 广告主ID
     */
    @ApiModelProperty(value="广告主ID")
    private Long advertiserId;

    /**
     * 广告组ID
     */
    @ApiModelProperty(value="广告组ID")
    private Long unitId;

    /**
     * 计划ID
     */
    @ApiModelProperty(value="计划ID")
    private Long campaignId;

    /**
     * 广告创意名称
     */
    @ApiModelProperty(value="广告创意名称")
    private String creativeName;

    /**
     * 素材类型(0：历史创意未作区分 1：竖版视频 2：横版视频 3：后贴片单图图片创意（历史类型，已下线）4：便利贴单图图片创意 11：开屏视频 12：开屏图片)
     */
    @ApiModelProperty(value="素材类型(0：历史创意未作区分 1：竖版视频 2：横版视频 3：后贴片单图图片创意（历史类型，已下线）4：便利贴单图图片创意 11：开屏视频 12：开屏图片)")
    private Integer creativeMaterialType;

    /**
     * 视频作品ID
     */
    @ApiModelProperty(value="视频作品ID")
    private String photoId;

    /**
     * 单图创意url
     */
    @ApiModelProperty(value="单图创意url")
    private String materialUrl;

    /**
     * 单图创意image_token
     */
    @ApiModelProperty(value="单图创意image_token")
    private String imageTokens;

    /**
     * 广告创意状态(-1：不限，1：计划已暂停，3：计划超预算，6：余额不足，11：组审核中，12：组审核未通过，14：已结束，15：组已暂停，17：组超预算，19：未达投放时间，40：已删除，41：审核中，42：审核未通过，46：已暂停，52：投放中，53：作品异常，54：视频审核通过可投放滑滑场景，55：部分素材审核失败)
     */
    @ApiModelProperty(value="广告创意状态(-1：不限，1：计划已暂停，3：计划超预算，6：余额不足，11：组审核中，12：组审核未通过，14：已结束，15：组已暂停，17：组超预算，19：未达投放时间，40：已删除，41：审核中，42：审核未通过，46：已暂停，52：投放中，53：作品异常，54：视频审核通过可投放滑滑场景，55：部分素材审核失败)")
    private Integer status;

    /**
     * 投放状态(1：投放中；2：暂停 3：删除)
     */
    @ApiModelProperty(value="投放状态(1：投放中；2：暂停 3：删除)")
    private Integer putStatus;

    /**
     * 创建渠道(0：投放后台创建；1：Marketing API 创建)
     */
    @ApiModelProperty(value="创建渠道(0：投放后台创建；1：Marketing API 创建)")
    private Integer createChannel;

    /**
     * 审核拒绝理由
     */
    @ApiModelProperty(value="审核拒绝理由")
    private String reviewDetail;

    /**
     * 审核拒绝图片集
     */
    @ApiModelProperty(value="审核拒绝图片集")
    private String rejectVideoSnapshot;

    /**
     * 封面URL
     */
    @ApiModelProperty(value="封面URL")
    private String coverUrl;

    /**
     * 视频封面token
     */
    @ApiModelProperty(value="视频封面token")
    private String imageToken;

    /**
     * 视频封面来源(1：首帧，0：非首帧)
     */
    @ApiModelProperty(value="视频封面来源(1：首帧，0：非首帧)")
    private Integer firstFrameType;

    /**
     * 封面图宽度
     */
    @ApiModelProperty(value="封面图宽度")
    private Long coverWidth;

    /**
     * 封面图高度
     */
    @ApiModelProperty(value="封面图高度")
    private Long coverHeight;

    /**
     * 动态词包原始封面图片URL
     */
    @ApiModelProperty(value="动态词包原始封面图片URL")
    private String overlayBgUrl;

    /**
     * 动态词包原始封面图片token
     */
    @ApiModelProperty(value="动态词包原始封面图片token")
    private String overlayBgImageToken;

    /**
     * 封面广告语标题
     */
    @ApiModelProperty(value="封面广告语标题")
    private String stickerTitle;

    /**
     * 贴纸样式类型
     */
    @ApiModelProperty(value="贴纸样式类型")
    private String overlayType;

    /**
     * 广告语
     */
    @ApiModelProperty(value="广告语")
    private String description;

    /**
     * 行动号召按钮文案
     */
    @ApiModelProperty(value="行动号召按钮文案")
    private String actionBarText;

    /**
     * 便利贴创意短广告语
     */
    @ApiModelProperty(value="便利贴创意短广告语")
    private String shortSlogan;

    /**
     * 广告标签
     */
    @ApiModelProperty(value="广告标签")
    private String exposeTag;

    /**
     * 广告标签 2 期
     */
    @ApiModelProperty(value="广告标签 2 期")
    private String newExposeTag;

    /**
     * 安卓下载中间页 ID
     */
    @ApiModelProperty(value="安卓下载中间页 ID")
    private Long siteId;

    /**
     * 点击监测链接
     */
    @ApiModelProperty(value="点击监测链接")
    private String clickTrackUrl;

    /**
     * 第三方开始播放监测链接
     */
    @ApiModelProperty(value="第三方开始播放监测链接")
    private String impressionUrl;

    /**
     * 第三方有效播放监测链接
     */
    @ApiModelProperty(value="第三方有效播放监测链接")
    private String adPhotoPlayedT3sUrl;

    /**
     * 物料信息
     */
    @ApiModelProperty(value="物料信息")
    private String materials;

    /**
     * 封面贴纸类型 ID
     */
    @ApiModelProperty(value="封面贴纸类型 ID")
    private String stickerStyleIds;

    /**
     * 广告语
     */
    @ApiModelProperty(value="广告语")
    private String descriptionTitles;

    /**
     * 封面广告语
     */
    @ApiModelProperty(value="封面广告语")
    private String stickerTitles;

    /**
     * 是否为僵尸创意(1：僵尸 0：非僵尸)
     */
    @ApiModelProperty(value="是否为僵尸创意(1：僵尸 0：非僵尸)")
    private Integer creativestatustype;

    /**
     * 数据总数
     */
    @ApiModelProperty(value="数据总数")
    private Long totalCount;

    /**
     * 创意信息创建时间(格式样例："2019-06-11 15:17:25")
     */
    @ApiModelProperty(value="创意信息创建时间")
    private LocalDateTime campaignCreateTime;

    /**
     * 创意信息最后修改时间(格式样例："2019-06-11 15:17:25")
     */
    @ApiModelProperty(value="创意信息最后修改时间")
    private LocalDateTime campaignUpdateTime;

    /**
     * 图片库图片
     */
    @ApiModelProperty(value="图片库图片")
    private String picId;

    /**
     * 审核分级类型(0：默认；1：审核降级(当创意发生降级时，会限制部分流量无法投放))
     */
    @ApiModelProperty(value="审核分级类型(0：默认；1：审核降级(当创意发生降级时，会限制部分流量无法投放))")
    private Integer appGradeType;

    /**
     * 开屏视频id(creative_material_type 为 11 时)
     */
    @ApiModelProperty(value="开屏视频id(creative_material_type 为 11 时)")
    private String splashPhotoIds;

    /**
     * 粉丝直播推广创意类型(3：直投直播；4：作品引流)
     */
    @ApiModelProperty(value="粉丝直播推广创意类型(3：直投直播；4：作品引流)")
    private Integer liveCreativeType;

    /**
     * 开屏图片 token(creative_material_type 为 12 时)
     */
    @ApiModelProperty(value="开屏图片 token(creative_material_type 为 12 时)")
    private String splashPictures;

    /**
     * 点击监测链接(计划 campaignType=16 粉丝直播推广时填写)
     */
    @ApiModelProperty(value="点击监测链接(计划 campaignType=16 粉丝直播推广时填写)")
    private String liveTrackUrl;

    /**
     * 广告计划类型(0:信息流，1:搜索)
     */
    @ApiModelProperty(value="广告计划类型(0:信息流，1:搜索)")
    private Integer adType;

    /**
     * 来源(1:API,2:渠道后台)
     */
    @ApiModelProperty(value="来源(1:API,2:渠道后台)")
    private Integer soucreStatus;

    /**
     * 平台状态(1:草稿,2:已推送)
     */
    @ApiModelProperty(value="平台状态(1:草稿,2:已推送)")
    private Integer dyStatus;

    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 是否删除(0否 1是)
     */
    @ApiModelProperty(value="是否删除(0否 1是)")
    private Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;


}
