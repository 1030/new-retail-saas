/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 素材标签
 *
 * @author cx
 * @date 2021-06-29 14:27:26
 */
@Data
@TableName("ad_label_relate")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "素材标签")
public class AdLabelRelate extends Model<AdLabelRelate> {
private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id",type= IdType.AUTO)
    @ApiModelProperty(value="id")
    private Long id;
    /**
     * 标签id
     */
    @ApiModelProperty(value="标签id")
    private Integer labelId;
    /**
     * 标签关联id
     */
    @ApiModelProperty(value="标签关联id")
    private Integer relateId;
    /**
     * 1.视频 2.图片
     */
    @ApiModelProperty(value="1.视频 2.图片")
    private Integer type;
    /**
     * 
     */
    @ApiModelProperty(value="")
    private Integer isDelete;
    /**
     * 
     */
    @ApiModelProperty(value="")
    private Date createTime;
    /**
     * 
     */
    @ApiModelProperty(value="")
    private Date updateTime;
    }
