package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 广点通优量汇行业精选流量包
 */
public enum GdtMobileUnionIndustryEnum {
	MOBILE_UNION_GAME_ADVERTISER("MOBILE_UNION_GAME_ADVERTISER",
			"重度游戏广告主定制"),
	MOBILE_UNION_LIGHT_GAME_ADVERTISER("MOBILE_UNION_LIGHT_GAME_ADVERTISER",
			"轻度游戏广告主定制"),
	MOBILE_UNION_READ_ADVERTISER("MOBILE_UNION_READ_ADVERTISER",
			"阅读行业广告主定制"),
	MOBILE_UNION_EDUCATION_ADVERTISER("MOBILE_UNION_EDUCATION_ADVERTISER",
			"教育广告主定制"),
	MOBILE_UNION_LONGTAIL_ADVERTISER("MOBILE_UNION_LONGTAIL_ADVERTISER",
			"行业优质流量包"),
	MOBILE_UNION_FINANCE_ADVERTISER("MOBILE_UNION_FINANCE_ADVERTISER",
			"金融广告主定制"),
	MOBILE_UNION_TOURISM_ADVERTISER("MOBILE_UNION_TOURISM_ADVERTISER",
			"旅游广告主定制"),
	MOBILE_UNION_EC_ADVERTISER("MOBILE_UNION_EC_ADVERTISER",
			"电商广告主定制");
	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	GdtMobileUnionIndustryEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 *
	 * @return
	 */
	public static String typeByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (GdtMobileUnionIndustryEnum item : GdtMobileUnionIndustryEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 *
	 * @return
	 */
	public static String valueByType(String type) {
		if (StringUtils.isBlank(type)) {
			return null;
		}

		for (GdtMobileUnionIndustryEnum item : GdtMobileUnionIndustryEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type) {
		if (StringUtils.isBlank(type)) {
			return false;
		}

		for (GdtMobileUnionIndustryEnum item : GdtMobileUnionIndustryEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
