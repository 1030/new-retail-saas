package com.pig4cloud.pig.api.dto;

import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 落地页表
 * @author  chenxiang
 * @version  2022-03-18 10:59:34
 * table: ad_landing_page
 */
@Data
public class AdLandingPageRes implements Serializable {
	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 平台ID（1:头条,8:广点通,10:快手）
	 */
	private Integer platformId;
	/**
	 * 广告账户
	 */
	private String advertiserId;
	/**
	 * 落地页名称
	 */
	private String pageName;
	/**
	 * 落地页ID
	 */
	private String pageId;
	/**
	 * 落地页缩略图
	 */
	private String pageUrl;
	/**
	 * 发布状态
	 */
	private String publishStatus;
	/**
	 * 审核状态
	 */
	private String status;
	/**
	 * 落地页包含的组件
	 */
	private String comps;
	/**
	 * 落地页绑定的小说 ID
	 */
	private Long fictionId;
	/**
	 * 落地页服务 id，主要用于广告投放、落地页送审及删除
	 */
	private String pageServiceId;
	/**
	 * 推广目标 id
	 */
	private String promotedObjectId;
	/**
	 * 商品库 id
	 */
	private Integer productCatalogId;
	/**
	 * 落地页类型：TT_ORANGE橙子落地页，TT_THIRD三方落地页，TT_GROUP程序化落地页，TT_CUSTOM自定义落地页
	 */
	private String pageType;
	/**
	 * 蹊径落地页试玩类型
	 */
	private String playableType;
	/**
	 * 状态，状态定义
	 */
	private String pageStatus;
	/**
	 * 枫页落地页子类型
	 */
	private String fengyeSubType;
	/**
	 * 授权方 uid
	 */
	private Integer ownerUid;
	/**
	 * 流量分配方式
	 */
	private String groupFlowType;
	/**
	 * 建站类型
	 */
	private String siteType;
	/**
	 * 建站类别
	 */
	private String functionType;
	/**
	 * 排序
	 */
	private Integer orderNumber;
	/**
	 * 是否删除  0否 1是
	 */
	private Integer deleted;
	/**
	 * 最新更新时间
	 */
	private Date lastModifyTime;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 创建人
	 */
	private Long createId;
	/**
	 * 修改人
	 */
	private Long updateId;
}


