package com.pig4cloud.pig.api.entity.ks;


import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 快手广告计划状态,预算,出价修改
 *
 * @author leisw
 * @date 2022-08-08 10:31:08
 */
@Data
@ApiModel(value = "快手广告计划状态,预算,出价修改")
public class KsPlanReq  implements Serializable {

	/**
	 * 广告账户ID
	 */
	private Long advertiserId;

	/**
	 * 广告组id(广告计划id)
	 */
	private Long unitId;

	/**
	 * 状态开关 1:开启   0:关闭
	 */
	private Integer optStatus;

	/**
	 * 媒体类型 1.头条,8.广点通,10快手
	 */
	private Integer mediaType;

	/**
	 *弹窗关闭提示
	 */
	private String ignoreWarning;

	/**
	 * 1 修改日预算  2 定时修改日预算
	 */
	private Integer type;

	/**
	 * 日预算金额：100<=X <= 100000000、仅支持最多2位小数据
	 */
	private BigDecimal budget;

	/**
	 * 日元算类型：BUDGET_MODE_DAY（日预算）
	 BUDGET_MODE_INFINITE（不限）
	 */
	private String budgetMode;

	/**
	 * 目标转换出价
	 */
	private BigDecimal cpaBid;

	/**
	 * 判断新老广告计划,1:表示老广告计划 2:表示新广告计划
	 */
	private String versionType;

}
