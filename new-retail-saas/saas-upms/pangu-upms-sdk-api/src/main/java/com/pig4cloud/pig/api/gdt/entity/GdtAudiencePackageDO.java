package com.pig4cloud.pig.api.gdt.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 广点通-定向包
 * gdt_audience_package
 * @author kongyanfang
 * @date 2020-12-03 17:38:07
 */
@Data
public class GdtAudiencePackageDO implements Serializable{

	private static final long serialVersionUID = 1L;

    /**
     * 广告主帐号id，有操作权限的帐号id，不支持代理商id
     */
    private Long accountId;

    /**
     * 定向名称，
     */
    private String targetingName;

    /**
     * 年龄定向-
     */
    private String age;

    /**
     * 性别定向（仅单选）枚举列表：{MALE,FEMALE}-
     */
    private String gender;

    /**
     * 用户学历,-
     */
    private String education;

    /**
     * 婚恋育儿状态，-
     */
    private String maritalStatus;

    /**
     * 工作状态，-
     */
    private String workingStatus;

    /**
     * 地理位置定向，-
     */
    private String geoLocation;

    /**
     * 操作系统定向,-
     */
    private String userOs;

    /**
     * 新设备，最近三个月第一次使用该设备的用户，枚举列表：{IOS,ANDROID}-
     */
    private String newDevice;

    /**
     * 设备价格定向，-
     */
    private String devicePrice;

    /**
     * 设备品牌型号定向-
     */
    private String deviceBrandModel;

    /**
     * 联网方式定向，-
     */
    private String networkType;

    /**
     * 移动运营商定向，-
     */
    private String networkOperator;

    /**
     * 上网场景，-
     */
    private String networkScene;

    /**
     * 穿衣指数，-
     */
    private String dressingIndex;

    /**
     * 紫外线指数，-
     */
    private String uvIndex;

    /**
     * 化妆指数，-
     */
    private String makeupIndex;

    /**
     * 气象，-
     */
    private String climate;

    /**
     * 温度-
     */
    private String temperature;

    /**
     * 空气质量指数，-
     */
    private String airQualityIndex;

    /**
     * 应用安装（应用用户），-
     */
    private String appInstallStatus;

    /**
     * 消费水平，枚举列表：{HIGH,LOW}-
     */
    private String consumptionStatus;

    /**
     * 游戏消费能力，-
     */
    private String gameConsumptionLevel;

    /**
     * 居住社区价格（仅支持单价格段，范围为：1~100000，单位是“元/m²”）-
     */
    private String residentialCommunityPrice;

    /**
     * 财产状态，-
     */
    private String financialSituation;

    /**
     * 消费类型定向，-
     */
    private String consumptionType;

    /**
     * 自定义定向用户群， varchar(10)-
     */
    private String customAudience;

    /**
     * 自定义排除用户群， varchar(10)-
     */
    private String excludedCustomAudience;

    /**
     * 行为兴趣意向定向   varchar(10)-
     */
    private String behaviorOrInterest;

    /**
     * QQ小游戏使用定向，-
     */
    private String miniGameQqStatus;

    /**
     * 微信再营销，-
     */
    private String wechatAdBehavior;

    /**
     * 微信流量类型定向，-
     */
    private String wechatOfficialAccountCategory;

    /**
     * 移动媒体类型定向，-
     */
    private String mobileUnionCategory;

    /**
     * 商业兴趣定向，通过 [targeting_tags/get] 接口获取
     */
    private String businessInterest;

    /**
     * 关键词定向
     */
    private String keyword;

    /**
     * app行为定向
     */
    private String appBehavior;

    /**
     * 付费用户
     */
    private String paidUser;

    /**
     * 自定义人群，
     */
    private String deprecatedCustomAudience;

    /**
     * 排他人群，
     */
    private String deprecatedExcludedCustomAudience;

    /**
     * 地域定向，
     */
    private String deprecatedRegion;
}