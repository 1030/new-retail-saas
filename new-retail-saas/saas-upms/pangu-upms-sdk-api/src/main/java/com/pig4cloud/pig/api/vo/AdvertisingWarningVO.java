package com.pig4cloud.pig.api.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.Digits;

import lombok.Data;

@Data
public class AdvertisingWarningVO implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = -4461043411812823076L;
	
	private String advertiserId;
	
	@Digits(integer = 18, fraction = 2, message = "金额格式错误")
	private BigDecimal planCostWarning;
	
	
	@Digits(integer = 18, fraction = 2, message = "金额格式错误")
	private BigDecimal alertBalance;
}
