package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @projectName:pig
 * @description: 广点通优化目标
 * @author:Zhihao
 * @createTime:2020/12/9 15:06
 * @version:1.0
 */
public enum GdtBidStrategyEnum {

	BID_STRATEGY_AVERAGE_COST("BID_STRATEGY_AVERAGE_COST","稳定拿量"),
	BID_STRATEGY_TARGET_COST("BID_STRATEGY_TARGET_COST","优先拿量"),
	BID_STRATEGY_PRIORITY_LOW_COST("BID_STRATEGY_PRIORITY_LOW_COST","优先低成本");
	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	GdtBidStrategyEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 *
	 * @return
	 */
	public static String typeByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (GdtAutoExpanEnableEnum item : GdtAutoExpanEnableEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 *
	 * @return
	 */
	public static String valueByType(String type) {
		if (StringUtils.isBlank(type)) {
			return null;
		}

		for (GdtBidStrategyEnum item : GdtBidStrategyEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type) {
		if (StringUtils.isBlank(type)) {
			return false;
		}

		for (GdtBidStrategyEnum item : GdtBidStrategyEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
