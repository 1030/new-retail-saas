package com.pig4cloud.pig.api.vo;

import cn.hutool.core.collection.ListUtil;
import com.alibaba.excel.annotation.ExcelProperty;
import com.pig4cloud.pig.api.util.Constants;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Data
public class PlanTtAttrAnalyseReportVo extends PlanBaseAttrVo {
	@ExcelProperty("状态")
	private String adStatus = Constants.EMPTTYSTR;
	@ExcelProperty("素材")
	private List<MaterialVo> materialVos = ListUtil.empty();
	@ExcelProperty("投放天数")
	private Long throwDays = 0l;
	@ExcelProperty("性别")
	private String gender = Constants.EMPTTYSTR;
	@ExcelProperty("年龄")
	private String age = Constants.EMPTTYSTR;
	@ExcelProperty("人群包定向")
	private String retargetingTagsInclude = Constants.EMPTTYSTR;
	@ExcelProperty("人群包排除")
	private String retargetingTagsExclude = Constants.EMPTTYSTR;
	@ExcelProperty("ROI系数")
	private BigDecimal roiGoal;
	public BigDecimal getRoiGoal() {
		if(Objects.isNull(roiGoal)){
			return new BigDecimal(0);
		}
		return roiGoal;
	}

	@ExcelProperty("行为场景")
	private String actionScene = Constants.EMPTTYSTR;
	@ExcelProperty("行为关键词")
	private String actionWords = Constants.EMPTTYSTR;
	@ExcelProperty("兴趣分类")
	private String interestCategories = Constants.EMPTTYSTR;
	@ExcelProperty("兴趣关键词")
	private String interestWords = Constants.EMPTTYSTR;
	@ExcelProperty("原始出价")
	private BigDecimal bid;

	public BigDecimal getBid() {
		if(Objects.isNull(bid)){
			return new BigDecimal(0);
		}
		return bid;
	}

	@ExcelProperty("转化出价")
	private BigDecimal cpaBid = new BigDecimal(0);

	public BigDecimal getCpaBid() {
		if(Objects.isNull(cpaBid)){
			return new BigDecimal(0);
		}
		return cpaBid;
	}

	@ExcelProperty("转化类型")
	private String converType = Constants.EMPTTYSTR;
	@ExcelProperty("出价类型")
	private String bidType = Constants.EMPTTYSTR;
	@ExcelProperty("智能放量")
	private String smartVolume = Constants.EMPTTYSTR;
	@ExcelProperty("投放位置")
	private String inventoryType = Constants.EMPTTYSTR;
	@ExcelProperty("投放量级")
	private Long throwCountLevel = 0l;

	public Long getThrowCountLevel() {
		if(Objects.isNull(throwCountLevel)){
			return 0l;
		}
		return throwCountLevel;
	}
}
