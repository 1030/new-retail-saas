package com.pig4cloud.pig.api.gdt.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * @author: nml
 * @time: 2020/12/18 12:26
 **/

@Data
public class GdtGroupInfoVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String adgroupName;

	private Long adgroupId;

	private List<String> siteSet;

	private String appId;

	private Boolean automaticSiteEnabled;

	private String dynamicCreativeId;

	private String promotedObjectType;

}
