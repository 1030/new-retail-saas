package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoGdtAudiencePackageFieldsEnum {

	//平台  1 ANDROID/ 2 IOS/ 3 PC/4 IPAD/ 5 WAP
	account_id                             ("account_id"                              , "广告主帐号"          ),
	targeting_name                         ("targeting_name"                          , "名称"                ),
	description                            ("description"                             , "描述"                ),
	age                                    ("age"                                     , "年龄"                ),
	gender                                 ("gender"                                  , "性别"                ),
	education                              ("education"                           , "学历"                ),
	marital_status                         ("marital_status"                      , "婚恋育儿状态"        ),
	working_status                         ("working_status"                      , "工作状态"            ),
	geo_location                           ("geo_location"                        , "地理位置"            ),
	user_os                                ("user_os"                             , "操作系统版本"        ),
	new_device                             ("new_device"                          , "新设备用户"          ),
	device_price                           ("device_price"                        , "设备价格"            ),
	device_brand_model                     ("device_brand_model"                  , "设备品牌型号"        ),
	network_type                           ("network_type"                        , "联网方式"            ),
	network_operator                       ("network_operator"                    , "移动运营商"          ),
	network_scene                          ("network_scene"                       , "上网场景"            ),
	dressing_index                         ("dressing_index"                      , "穿衣指数"            ),
	uv_index                               ("uv_index"                            , "紫外线指数"          ),
	makeup_index                           ("makeup_index"                        , "化妆指数"            ),
	climate                                ("climate"                             , "气象"                ),
	temperature                            ("temperature"                             , "温度"                ),
	air_quality_index                      ("air_quality_index"                       , "空气质量指数"        ),
	app_install_status                     ("app_install_status"                      , "应用安装"            ),
	consumption_status                     ("consumption_status"                  , "消费水平"            ),
	game_consumption_level                 ("game_consumption_level"                  , "游戏消费能力"        ),
	residential_community_price            ("residential_community_price"             , "居住社区价格"        ),
	financial_situation                    ("financial_situation"                     , "财产状态"            ),
	consumption_type                       ("consumption_type"                        , "消费类型"            ),
	custom_audience                        ("custom_audience"                     , "自定义人群-定向人群" ),
	excluded_custom_audience               ("excluded_custom_audience"            , "自定义人群-排除人群" ),
	behavior_or_interest                   ("behavior_or_interest"                , "行为兴趣意向"        ),
	mini_game_qq_status                    ("mini_game_qq_status"                 , "QQ小游戏使用"        ),
	wechat_ad_behavior                     ("wechat_ad_behavior"                      , "微信再营销"          ),
	wechat_official_account_category       ("wechat_official_account_category"        , "微信流量类型"        ),
	mobile_union_category                  ("mobile_union_category"                   , "移动媒体类型"        ),
	business_interest                      ("business_interest"                       , "商业兴趣"            ),
	keyword                                ("keyword"                                 , "关键词"              ),
	app_behavior                           ("app_behavior"                            , "app行为"             ),
	paid_user                              ("paid_user"                               , "付费用户"            ),
	deprecated_custom_audience             ("deprecated_custom_audience"          , "自定义人群"          ),
	deprecated_excluded_custom_audience    ("deprecated_excluded_custom_audience" , "排他人群"            ),
	deprecated_region                      ("deprecated_region"                   , "地域"                ),
	is_deleted                             ("is_deleted"                              , "是否已删除"          ),
	created_time                           ("created_time"                            , "创建时间"            ),
	last_modified_time                     ("last_modified_time"                      , "最后修改时间"        ),
	targeting_translation                  ("targeting_translation"               , "已选择条件的描述"    ),
	sync_statussmallint                    ("sync_statussmallint"                     , "同步状态"            ),
	min                    ("min"                     , "最小"            ),
	max                    ("max"                     , "最大"            ),
	interest                    ("interest"                     , "兴趣"            ),
	intention                    ("intention"                     , "意向"            ),
	behavior                    ("behavior"                     , "行为"            ),
	category_id_list                    ("category_id_list"                     , "类别id列表"            ),
	targeting_tags                    ("targeting_tags"                     , "定向标签"            ),
	deep_conversion_type                    ("deep_conversion_type"                     , "oCPC/oCPM 深度优化价值配置"            ),
	deep_conversion_behavior_spec                    ("deep_conversion_behavior_spec"                     , "oCPC/oCPM 优化转化行为配置"            ),
	goal                    ("goal"                     , "优化目标"            ),
	bid_amount                    ("bid_amount"                     , "出价"            ),
	id_adplatform                          ("targeting_id"                            , "定向id"              );




	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoGdtAudiencePackageFieldsEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoGdtAudiencePackageFieldsEnum item : ToutiaoGdtAudiencePackageFieldsEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoGdtAudiencePackageFieldsEnum item : ToutiaoGdtAudiencePackageFieldsEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoGdtAudiencePackageFieldsEnum item : ToutiaoGdtAudiencePackageFieldsEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
