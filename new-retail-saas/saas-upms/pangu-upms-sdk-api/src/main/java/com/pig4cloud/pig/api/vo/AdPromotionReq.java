package com.pig4cloud.pig.api.vo;
import io.swagger.annotations.ApiModelProperty;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

/**
 * 广告表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:21:02
 * table: ad_promotion
 */
@Data
public class AdPromotionReq extends Page {

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "广告ID")
	private String promotionId;

	@ApiModelProperty(value = "广告名称")
	private String promotionName;

	@ApiModelProperty(value = "项目ID")
	private String projectId;

	@ApiModelProperty(value = "广告账户ID")
	private String advertiserId;

	@ApiModelProperty(value = "广告创建时间，格式yyyy-mm-dd，表示过滤出当天创建的广告项目")
	private String promotionCreateTime;

	@ApiModelProperty(value = "广告更新时间，格式yyyy-mm-dd，表示过滤出当天更新的广告项目")
	private String promotionModifyTime;

	@ApiModelProperty(value = "广告状态")
	private String status;

	@ApiModelProperty(value = "操作状态")
	private String optStatus;

	@ApiModelProperty(value = "广告素材组合")
	private String promotionMaterials;

	@ApiModelProperty(value = "预算")
	private String budget;

	@ApiModelProperty(value = "目标转化出价/预期成本")
	private String cpaBid;

	@ApiModelProperty(value = "深度优化出价")
	private String deepCpabid;

	@ApiModelProperty(value = "深度转化ROI系数")
	private String roiGoal;

	@ApiModelProperty(value = "素材评级信息")
	private String materialScoreInfo;

	@ApiModelProperty(value = "来源(1:API,2:渠道后台)")
	private String soucreStatus;

	@ApiModelProperty(value = "是否删除  0否 1是")
	private String deleted;

	@ApiModelProperty(value = "创建时间")
	private String createTime;

	@ApiModelProperty(value = "修改时间")
	private String updateTime;

	@ApiModelProperty(value = "创建人")
	private String createId;

	@ApiModelProperty(value = "修改人")
	private String updateId;
}
