package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 广点通-广告组临时表 gdt_adgroup_temp
 *
 * @author hma
 * @date 2020-12-05
 */
@Setter
@Getter
public class GdtAdgroupTemp
{
	private static final long serialVersionUID = 1L;
	@TableId(value = "id",type = IdType.AUTO)
	/** 广告组临时主键id */
	private Long id;
	private Long userId;
	/** 广告组主键id */
	private Long adgroupId;
	/** 推广计划名称 */
	private String adgroupName;
	/** 推广计划主键id */
	private Long campaignId;
	/** 广告版位：数组字符串 */
	private String siteSet;

	/** 计费类型 */
	private String billingEvent;
	/** 广告出价，单位为分，出价限制：
CPC 出价限制：介于 10 分-10,000 分之间（0.1 元-100 元，单位为人民币）
CPM 出价限制：介于 150 分-99,900 之间（1.5 元-999 元，单位为人民币）
CPA 出价限制：介于 100 分-50,000 分之间（1 元-500 元，单位为人民币）
oCPC/oCPM 出价限制：介于 10 分-500,000 分之间（0.1 元-5000 元，单位为人民币） */
	private BigDecimal bidAmount;
	/** 广告组日预算，单位为分，设置为 0 表示不设预算（即不限） */
	private BigDecimal dailyBudget;
	/** 推广目标 */
	private String promotedObjectType;
	/** 定向id */
	private Long targetingId;
	/** 场景定向 */
	private String sceneSpec;
	/** 开始投放日期，日期格式：YYYY-MM-DD，且日期小于等于 end_date */
	private String beginDate;
	/** 结束投放日期，日期格式：YYYY-MM-DD，大于等于今天，且大于等于 begin_date ；如果希望长期投放，传空字符串 */
	private String endDate;
	/** 首日开始投放时间，首日开始投放时间：HH:ii:ss */
	private String firstDayBeginTime;
	/** 投放时间段，格式为 48 * 7 位字符串，且都为 0 和 1，以半个小时为最小粒度 */
	private String timeSeries;
	/** 客户设置的状态:AD_STATUS_NORMAL有效，AD_STATUS_SUSPEND暂停 */
	private String configuredStatus;
	/** 自定义分类，关键词用半角逗号','分隔，如：本地生活,餐饮 */
	private String customizedCategory;
	/** 创建时间 */
	private Long createdTime;
	/** 修改时间 */
	private Long lastModifiedTime;

	/** 删除状态：false 未删除 true 删除 */
	private String isDeleted;
	/** 扩量种子人群 */
	private String coldStartAudience;
	/** 是否使用自动扩量，当广告为 oCPC/oCPM、CPC 出价时，可将 expand_enabled 设置为 true */
	private String expandEnabled;
	/** 扩量不可突破定向 */
	private String expandTargeting;
	/** oCPC/oCPM 深度优化内容，oCPC/oCPM 深度优化内容，创建时若此字段不传，或传空则视为无限制条件； */
	//private String deepConversionSpec;
	/** 门店 id 列表 */
	private String poiList;
	/** 广告组在系统中的状态 */
	private String systemStatus;
	/** 出价系数设置内容，仅当投放 oCPC、oCPM 广告时可使用 */
	private String bidAdjustment;
	/** 是否开启自动版位功能：true, false  */
	private String automaticSiteEnabled;
	/** 推广目标主键id(--对应用户推广对象主键id,字段长度最小 0 字节，长度最大 128 字节) */
	private String promotedObjectId;
	/** 推广目标主键名称) */
	private String promotedObjectName;
	/** 安卓应用渠道包 id(字段长度最小 1 字节，长度最大 128 字节) */
	private String appAndroidChannelPackageId;
	/** 定向详细设置，存放所有定向条件。与 targeting_id 不能同时填写且不能同时为空，
（当campaign_type=CAMPAIGN_TYPE_WECHAT_MOMENTS 时必填） */
	@TableField(value = "targeting")
	private String targeting;
	/** 动态创意id(暂时不用） */
	private Long dynamicCreativeId;
	/** 动态商品广告属性，当广告形态为动态商品广告时，该字段必填 */
	private String dynamicAdSpec;
	/** 是否是激励视频广告：true, false */
	private String isRewardedVideoAd;
	/** 出价策略：出价策略，仅 oCPC/oCPM 广告可设置，且为必填 */
	private String bidStrategy;
	/** 出价类型，当出价类型为 SMART_BID_TYPE_SYSTEMATIC 时，不可传入 bid_amount，暂不支持微信流量，功能灰度开放中,{ SMART_BID_TYPE_CUSTOM, SMART_BID_TYPE_SYSTEMATIC } */
	/**
	 * SMART_BID_TYPE_CUSTOM
	 * 手动出价
	 * SMART_BID_TYPE_SYSTEMATIC
	 * 自动出价
	 */
	private String smartBidType;
	/** 是否使用系统优选：是否使用系统优选，限制：
系统优选不支持朋友圈广告使用；
当 auto_audience=true 时系统自动选择匹配行为兴趣定向；
系统优选(auto_audience)和行为兴趣定向(behavior_or_interest)不能同时填写，即行为兴趣和系统优选功能二选一；
系统优选(auto_audience)和自动扩量(expand_enabled)功能、定向包 id（targeting_id）、自定义人群(custom_audience)功能互斥；

可选值：{ true, false } */
	private String autoAudience;
	/** CPC/oCPM 深度优化方式配置 */
	private String deepOptimizationActionType;

	/** 转化 id，包含优化目标 当传入转化 id 时，可无需再传入 user_action_sets，optimization_goal */
	private Long conversionId;

	/** 广告优化目标类型 */
	//private String optimizationGoal;
	/** 转换归因 */
	//private String userActionSets;
	/**
	 * 转换归因类型 1 全网归因  2 精准匹配归因
	 */
	//private String userActionSetsType;

	/** 深度优化行为的出价，使用转化 id 确认优化目标和深度优化目标的广告主，深度优化行为的目标出价无需传 deep_conversion_spec 结构体中字段，可直接使用 deep_conversion_behavior_bid 字段传输深度出价，当深度转化行为目标为次留率时，单位为%，最小值 1，最大值 99。如输入 40，代表次留率目标 40%，单位为分。
最小值 1，最大值 500000 */
	private Integer deepConversionBehaviorBid;
	/** 深度优化价值的期望 ROI:最小值 0.01，最大值 1000，最多保留 2 位小数 */
	private BigDecimal deepConversionWorthRate;
	/** 出价方式:1. bid_mode 为 billing_event 升级字段，不可同时输入，升级后直接写入 bid_mode 字段即可；
2. 当投放智能出价广告，可写入 BID_MODE_OCPC/BID_MODE_OCPM。此时，optimization_goal 优化目标字段必填；
3. 当投放非智能出价广告，可写入 BID_MODE_CPC/BID_MODE_CPM/BID_MODE_CPA。此时，optimization_goal 优化目标字段不可填；
4. 针对非微信流量，BID_MODE_CPC 可编辑修改为 BID_MODE_OCPC，BID_MODE_CPM 可编辑修改为 BID_MODE_OCPM，其他修改不可操作。针对微信流量，bid_mode 字段不可修改；
5. 可通过 adcreative_templates/get 接口查询不同情况下支持的出价方式;， */
	private String bidMode;
	/** 是否开启一键起量，该功能灰度开放中:可选值：{ true, false } */
	private String autoAcquisitionEnabled;
	/** 键起量探索预算，单位为分 */
	private Integer autoAcquisitionBudget;
	/** 是否开启自动衍生视频创意，该功能灰度开放中可选值:true, false */
	private String autoDerivedCreativeEnabled;

	/** 广告形式：CAMPAIGN_TYPE_NORMAL 常规展示广告 */
	private String campaignType="CAMPAIGN_TYPE_NORMAL";

	/**
	 * 投放投放日期类型 ：1  长期投放  2 指定时间段投放
	 */
	private String launchDateType;

	/**
	 * 投放时间类型  1 全天投放  2 指定开始时间和结束时间  3  指定多个时间段
	 */
	private String timeSeriesType;

	//创意展示类型
	private String creativeDisplayType;

	public GdtAdgroupTemp(){}

	public GdtAdgroupTemp(Long adgroupId){
		this.adgroupId=adgroupId;
	}

	public GdtAdgroupTemp(Long adgroupId,Long lastModifiedTime){
		this.adgroupId=adgroupId;
		this.lastModifiedTime=lastModifiedTime;
	}

}
