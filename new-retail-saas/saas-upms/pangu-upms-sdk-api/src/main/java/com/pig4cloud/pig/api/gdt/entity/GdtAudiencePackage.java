package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 广点通-定向包
 * gdt_audience_package
 * @author kongyanfang
 * @date 2020-12-03 17:38:07
 */
@Data
@NoArgsConstructor
@TableName(value="gdt_audience_package")
public class GdtAudiencePackage extends Model<GdtAudiencePackage> {
    /**
     * 自增的 转化ID
     */
	@TableId(value="id",type= IdType.AUTO)
    private Long id;

    /**
     * 广告主帐号id，有操作权限的帐号id，不支持代理商id
     */
    private Long accountId;

    /**
     * 定向名称，
     */
    private String targetingName;

    /**
     * 定向描述
     */
    private String description;

    /**
     * 年龄定向
     */
    private String age;

    /**
     * 性别定向（仅单选）枚举列表：{MALE,FEMALE}
     */
    private String gender;

    /**
     * 用户学历,
     */
    private String education;

    /**
     * 婚恋育儿状态，
     */
    private String maritalStatus;

    /**
     * 工作状态，
     */
    private String workingStatus;

    /**
     * 地理位置定向，
     */
    private String geoLocation;

    /**
     * 操作系统定向,
     */
    private String userOs;

    /**
     * 新设备，最近三个月第一次使用该设备的用户，枚举列表：{IOS,ANDROID}
     */
    private String newDevice;

    /**
     * 设备价格定向，
     */
    private String devicePrice;

    /**
     * 设备品牌型号定向
     */
    private String deviceBrandModel;

    /**
     * 联网方式定向，
     */
    private String networkType;

    /**
     * 移动运营商定向，
     */
    private String networkOperator;

    /**
     * 上网场景，
     */
    private String networkScene;

    /**
     * 穿衣指数，
     */
    private String dressingIndex;

    /**
     * 紫外线指数，
     */
    private String uvIndex;

    /**
     * 化妆指数，
     */
    private String makeupIndex;

    /**
     * 气象，
     */
    private String climate;

    /**
     * 温度
     */
    private String temperature;

    /**
     * 空气质量指数，
     */
    private String airQualityIndex;

    /**
     * 应用安装（应用用户），
     */
    private String appInstallStatus;

    /**
     * 消费水平，枚举列表：{HIGH,LOW}
     */
    private String consumptionStatus;

    /**
     * 游戏消费能力，
     */
    private String gameConsumptionLevel;

    /**
     * 居住社区价格（仅支持单价格段，范围为：1~100000，单位是“元/m²”）
     */
    private String residentialCommunityPrice;

    /**
     * 财产状态，
     */
    private String financialSituation;

    /**
     * 消费类型定向，
     */
    private String consumptionType;

    /**
     * 自定义定向用户群， varchar(10)
     */
    private String customAudience;

    /**
     * 自定义排除用户群， varchar(10)
     */
    private String excludedCustomAudience;

    /**
     * 行为兴趣意向定向   varchar(10)
     */
    private String behaviorOrInterest;

    /**
     * QQ小游戏使用定向，
     */
    private String miniGameQqStatus;

    /**
     * 微信再营销，
     */
    private String wechatAdBehavior;

    /**
     * 微信流量类型定向，
     */
    private String wechatOfficialAccountCategory;

    /**
     * 移动媒体类型定向，
     */
    private String mobileUnionCategory;

    /**
     * 商业兴趣定向，通过 [targeting_tags/get] 接口获取
     */
    private String businessInterest;

    /**
     * 关键词定向
     */
    private String keyword;

    /**
     * app行为定向
     */
    private String appBehavior;

    /**
     * 付费用户
     */
    private String paidUser;

    /**
     * 自定义人群，
     */
    private String deprecatedCustomAudience;

    /**
     * 排他人群，
     */
    private String deprecatedExcludedCustomAudience;

    /**
     * 地域定向，
     */
    private String deprecatedRegion;

    /**
     * 是否已删除，true：是，false：否
     */
    private String isDeleted;

    /**
     * 创建时间（时间戳）
     */
    private Long createdTime;

    /**
     * 最后修改时间（时间戳）
     */
    private Long lastModifiedTime;

    /**
     * 已选择定向条件的描述
     */
    private String targetingTranslation;

    /**
     * 同步状态 0未同步/1同步成功/2同步失败
     */
    private Short syncStatus;

    /**
     * 转化跟踪在 第三方平台中的ID (头条/UC 的转化id)
     */
    private Long idAdplatform;
    @TableField(exist = false)
    private String excludedConvertedAudience;
}
