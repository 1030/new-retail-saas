/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.dto;

import com.pig4cloud.pig.api.entity.tag.AdTagList;
import com.pig4cloud.pig.api.enums.MakeTypeEnum;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 素材库
 * @date 2021-06-19 16:12:13
 */
@Data
public class AdMaterialResp {
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 素材名称
     */
    private String name;
    /**
     * 素材类型（1：视频，2：图片）
     */
    private Integer type;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件访问地址
     */
    private String fileUrl;
    /**
     * 素材封面展示地址
     */
    private String imageUrl;
    /**
     * 主游戏ID
     */
    private Integer mainGameId;
    /**
     * 主游戏名称
     */
    private String mainGameName;
	/**
	 * 子游戏ID
	 */
	private Integer gameId;
	/**
	 * 子游戏名称
	 */
	private String gameName;
    /**
     * 创意者
     */
    private Integer creatorId;
    /**
     * 创意者名称
     */
    private String creatorName;
    /**
     * 制作者
     */
    private Integer makerId;
    /**
     * 制作者名称
     */
    private String makerName;
    /**
     * 制作类型（1：原创，2：原创衍生，3：抄本，4：抄本衍生）
     */
    private Integer makeType;
    /**
     * 制作类型（1：原创，2：原创衍生，3：抄本，4：抄本衍生）
     */
    private String makeTypeName;
    /**
     * md5值
     */
    private String md5;
    /**
     * 素材宽
     */
    private String width;
    /**
     * 素材高
     */
    private String height;
	/**
	 * 尺寸（宽高）
	 */
	private String widthHeight;
    /**
     * 尺寸类型：（1:横屏视频，2:竖屏视频，3:小图，4:横版大图，5:竖版大图，6:gif图，7:卡片主图，0:其他）
     */
    private Integer screenType;
    /**
     * 视频大小
     */
    private String size;
    /**
     * 素材格式
     */
    private String format;
    /**
     * 素材时长
     */
    private String duration;
    /**
     * 标签
     */
    private String labels;
    /**
     * 状态：（0：启用，1：禁用）
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 禁用理由
     */
    private String disableReason;
    /**
     * 消耗
     */
    private BigDecimal cost;
    /**
     * 返点后消耗
     */
    private BigDecimal rudeCost;
    /**
     * 同步的广告账户集合
     */
    private String advertiserIds;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 修改人
     */
    private String updateUser;
	/**
	 * 同步状态：0：未同步，1：部分同步，2：全部同步
	 */
	private Integer ttSynStatus = 0;
	/**
	 * 同步状态：0：未同步，1：部分同步，2：全部同步
	 */
	private Integer gdtSynStatus = 0;
	/**
	 * 同步状态：0：未同步，1：部分同步，2：全部同步
	 */
	private Integer ksSynStatus = 0;
	/**
	 * 用户收藏ID
	 */
	private Integer collectId;
	/**
	 * 三方平台文件ID
	 */
	private String platformFileId;
	/**
	 * 三方平台素材ID
	 */
	private String platformMaterialId;
	/**
	 * 视频同步广告账户信息列表-------头条
	 */
	private List<AdMaterialAccount> pushTtList;
	/**
	 * 视频同步广告账户信息列表-------广点通
	 */
	private List<AdMaterialAccount> pushGdtList;

	/**
	 * 标签ID集合
	 */
	private String tagIds;

	private List<AdTagList> tagList;

	/**
	 * 视频同步广告账户信息列表-------快手
	 */
	private List<AdMaterialAccount> pushKsList;

    public String getMakeTypeName(){
    	if (Objects.nonNull(makeType)){
    		return MakeTypeEnum.getName(makeType);
		}
    	return "-";
	}
	public String getWidthHeight(){
    	if (StringUtils.isNotBlank(width) && StringUtils.isNotBlank(height)){
			return Double.valueOf(width).intValue() + "x" + Double.valueOf(height).intValue();
		}
    	return "-";
	}
}
