package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @ClassName AdVideo.java
 * @author cx
 * @version 1.0.0
 * @Desc TODO
 * @Time 2020/11/5 13:38
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_video")
public class AdVideo extends Model<AdVideo> {

	private static final long serialVersionUID = 1L;
    /**
     * 主键ID
     */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value = "主键ID")
	private Long id;

    /**
     * 视频名称
     */
	@ApiModelProperty(value = "视频名称")
	@TableField(value = "v_name")
    private String vName;

    /**
     * 文件名称
     */
	@ApiModelProperty(value = "文件名称")
	@TableField(value = "file_name")
    private String fileName;

    /**
     * 文件访问地址
     */
	@ApiModelProperty(value = "文件访问地址")
	@TableField(value = "file_url")
    private String fileUrl;

	/**
	 * 图片访问地址
	 */
	@ApiModelProperty(value = "图片访问地址")
	@TableField(value = "image_url")
	private String imageUrl;

    /**
     * 视频宽
     */
	@ApiModelProperty(value = "视频宽")
	@TableField(value = "width")
    private String width;

    /**
     * 视频高
     */
	@ApiModelProperty(value = "视频高")
	@TableField(value = "height")
    private String height;

	/**
	 * 1横屏，2竖屏
	 */
	@ApiModelProperty(value = "1横屏，2竖屏")
	@TableField(value = "screen_type")
	private Integer screenType;

    /**
     * 视频大小
     */
	@ApiModelProperty(value = "视频大小")
	@TableField(value = "v_size")
    private String vSize;

    /**
     * 视频格式
     */
	@ApiModelProperty(value = "视频格式")
	@TableField(value = "format")
    private String format;

    /**
     * 视频时长
     */
	@ApiModelProperty(value = "视频时长")
	@TableField(value = "duration")
    private String duration;

    /**
     * 项目ID
     */
	@ApiModelProperty(value = "项目ID")
	@TableField(value = "project_id")
    private Integer projectId;

    /**
     * 头条上传返回关联id
     */
	@ApiModelProperty(value = "真实路径")
	@TableField(value = "real_path")
    private String realPath;

    /**
     * 视频md5值
     */
	@ApiModelProperty(value = "视频md5值")
	@TableField(value = "md5")
    private String md5;

    /**
     * 是否删除：0正常，1删除
     */
	@ApiModelProperty(value = "是否删除：0正常，1删除")
	@TableField(value = "isdelete")
    private Integer isdelete;

    /**
     * 备注
     */
	@ApiModelProperty(value = "备注")
	@TableField(value = "remark")
    private String remark;

    /**
     * 创建时间
     */
	@ApiModelProperty(value = "创建时间")
	@TableField(value = "createtime")
    private Date createtime;

    /**
     * 创建人
     */
	@ApiModelProperty(value = "创建人")
	@TableField(value = "createuser")
    private String createuser;

    /**
     * 修改时间
     */
	@ApiModelProperty(value = "修改时间")
	@TableField(value = "updatetime")
    private Date updatetime;

    /**
     * 修改人
     */
	@ApiModelProperty(value = "修改人")
	@TableField(value = "updateuser")
    private String updateuser;
}