package com.pig4cloud.pig.api.vo;

import cn.hutool.core.collection.ListUtil;
import com.alibaba.excel.annotation.ExcelProperty;
import com.pig4cloud.pig.api.util.Constants;
import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Setter
@Getter
public class PlanGdtAttrAnalyseReportVo extends PlanBaseAttrVo {
	@ExcelProperty("状态")
	private String adStatus = Constants.EMPTTYSTR;
	/*@ExcelProperty("素材")
	private List<MaterialVo> materialVos = ListUtil.empty();*/
	@ExcelProperty("投放天数")
	private Integer throwDays;

	public Integer getThrowDays() {
		if(Objects.isNull(throwDays)){
			return 0;
		}
		return throwDays;
	}

	@ExcelProperty("性别")
	private String gender = Constants.EMPTTYSTR;
	@ExcelProperty("年龄")
	private String age = Constants.EMPTTYSTR;
	@ExcelProperty("游戏用户消费能力")
	private String gameUserConsumeAbility = Constants.EMPTTYSTR;
	@ExcelProperty("定义定向用户")
	private String defRetargetingUser = Constants.EMPTTYSTR;
	@ExcelProperty("人群包排除")
	private String retargetingTagsExclude = Constants.EMPTTYSTR;
	@ExcelProperty("行为")
	private String action = Constants.EMPTTYSTR;
	@ExcelProperty("兴趣")
	private String interest = Constants.EMPTTYSTR;
	@ExcelProperty("意向")
	private String intention = Constants.EMPTTYSTR;
	@ExcelProperty("自动扩量")
	private String autoExpand = Constants.EMPTTYSTR;
	@ExcelProperty("优化目标")
	private String optimizeTarget = Constants.EMPTTYSTR;
	@ExcelProperty("深度优化目标")
	private String deepOptimizeTarget = Constants.EMPTTYSTR;
	@ExcelProperty("出价策略")
	private String bidStrategy = Constants.EMPTTYSTR;
	@ExcelProperty("出价")
	private BigDecimal bid;

	public BigDecimal getBid() {
		if(Objects.isNull(bid)){
			return new BigDecimal(0);
		}
		return bid;
	}

	@ExcelProperty("深度目标出价")
	private String deepTargetBid = "0";

	public String getDeepTargetBid() {
		if(StringUtils.isBlank(deepTargetBid)){
			return "0";
		}
		return deepTargetBid;
	}

	@ExcelProperty("是否自动扩量")
	private String autoExpandEnable = Constants.NO;
	@ExcelProperty("广告投放位置")
	private String adThrowLoaction = Constants.EMPTTYSTR;
}

