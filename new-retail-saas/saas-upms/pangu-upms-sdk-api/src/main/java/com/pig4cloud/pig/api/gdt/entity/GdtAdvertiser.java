package com.pig4cloud.pig.api.gdt.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@TableName(value="gdt_advertiser")
public class GdtAdvertiser {
	
	@ApiModelProperty(value = "授权的推广帐号 id")
	@TableId(value = "account_id")
	private String accountId;
	
	@ApiModelProperty(value = "企业名称")
	@TableField(value = "corporation_name")
	private String corporationName;
	
	@ApiModelProperty(value = "账号类型")
	@TableField(value = "account_type")
	private String accountType;
	
	@ApiModelProperty(value = "管家账户")
	@TableField(value = "housekeeper")
	private String housekeeper;
	
	@ApiModelProperty(value = "修改时间")
	@TableField( fill = FieldFill.UPDATE)
	private Date updatetime;
	
	
	@ApiModelProperty(value = "创建时间")
	@TableField( fill = FieldFill.INSERT)
	private Date createtime;


}
