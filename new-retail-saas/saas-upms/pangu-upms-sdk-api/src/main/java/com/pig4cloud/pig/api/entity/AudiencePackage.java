package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.pig4cloud.pig.api.enums.ToutiaoDeliveryRangeEnum;
import com.pig4cloud.pig.api.enums.ToutiaoAPLandTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;


/**
 * 定向包
 * audience_package
 * @author kongyanfang
 * @date 2020-11-08 15:48:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="audience_package")
public class AudiencePackage extends Model<AudiencePackage> {
    /**
     * 自增的 转化ID
     */
	@TableId(value="id",type= IdType.AUTO)
    private Integer id;


	/**
	 * 广告主ID
	 */
	private String advertiserId;

	/**
	 * 定向包名称
	 */
	private String name;

	/**
	 * 定向包描述
	 */
	private String description;

	/**
	 * 定向包类型，取值范围：EXTERNAL 落地页/ARTICLE 文章推广/GOODS 商品推广/DPA 商品目录/STORE 门店推广/AWEME 抖音号推广/SHOP 店铺直投/APP_ANDROID 应用下载-安卓/APP_IOS 应用下载-IOS
	 */
	private String landingType;

	/**
	 * 广告投放范围  DEFAULT 默认/UNION 只投放到资讯联盟（穿山甲）/UNIVERSAL 通投智选
	 */
	private String deliveryRange;

	/**
	 * select 省市/county 区县/local 商圈
	 */
	private String district;

	/**
	 * 省
	 */
	private String province;

	/**
	 * 市or区县 （当district=county时，city存放区县,）
	 */
	private String city;

	/**
	 * 商圈
	 */
	private String businessIds;

	/**
	 * HOME 居住在该地区的用户/TRAVEL 到该地区旅行的用户/ALL 该地区内的所有用户/CURRENT 正在该地区的用户
	 */
	private String locationType;

	/**
	 * 受众性别, 允许值: GENDER_FEMALE 女,GENDER_MALE 男, NONE 不限
	 */
	private String gender;

	/**
	 * 受众年龄区间, 允许值: AGE_BETWEEN_18_23,AGE_BETWEEN_24_30,AGE_BETWEEN_31_40, AGE_BETWEEN_41_49,AGE_ABOVE_50
	 */
	private String age;

	/**
	 * 定向人群包列表，内容为人群包id,  逗号隔开的人群包列表
	 */
	private String retargetingTags;

	/**
	 * 排除人群包列表，内容为人群包id   逗号隔开的人群包列表
	 */
	private String retargetingTagsExclude;

	/**
	 * 行为兴趣选择，允许值：UNLIMITED不限/CUSTOM 自定义 / RECOMMEND 系统推荐
	 */
	private String interestActionMode;

	/**
	 * 关注/评论/点赞/分享/ 逗号隔开的列表
	 */
	private String awemeFanBehaviors;

	/**
	 * 抖音分类 时尚/个人管理/二次元  逗号隔开的列表
	 */
	private String awemeFanCategories;

	/**
	 * 抖音达人账户 逗号隔开的列表
	 */
	private String awemeFanAccounts;

	/**
	 * 时间范围  1 近15天 / 2 近30天 /  3 近60天
	 */
	private Short awemeFanTimeScope;

	/**
	 * 平台  1 ANDROID/ 2 IOS/ 3 PC/4 IPAD/ 5 WAP
	 */
	private String platform;

	/**
	 * 受众网络类型：10 unknown/1 WIFI/2 2G/3 3G/4 4G/5 5G
	 */
	private String ac;

	/**
	 * 已安装用户:0 表示不限，1表示过滤，2表示定向
	 */
	private Short hideIfExists;

	/**
	 * 过滤已转化用户           1广告计划       2广告组       3广告账户        5 公司账户
	 */
	private Short hideIfConverted;

	/**
	 * 过滤时间，hide_if_converted=5时有效      107 7天;   1 1个月;   3 3个月;    6 6个月;   12 12个月
	 */
	private Short convertedTimeDuration;

	/**
	 * 安卓版本：受众最低android版本  允许值: "0.0", "2.0", "2.1", "2.2", "2.3", "3.0", "3.1", "3.2", "4.0","4.1","4.2", "4.3", "4.4", "4.5", "5.0", "NONE"
	 */
	private String androidOsv;

	/**
	 * 受众运营商，允许值: MOBILE 移动/UNICOM 联通 / TELCOM 电信
	 */
	private String carrier;

	/**
	 * 新用户    1一个月以内/ 2一到三个月/ 3三个月以上,用逗号隔开
	 */
	private String activateType;

	/**
	 * 手机品牌，用逗号隔开的手机品牌列表
	 */
	private String deviceBrand;

	/**
	 * 手机价格定向,  区间起始
	 */
	private Integer launchPriceFrom;

	/**
	 * 手机价格定向,  区间上线
	 */
	private Integer launchPriceTo;

	/**
	 * 职业状态     926968303 大学生  /  926968304教师 /  927051338 IT  /  926968305 公务员  /  927138917 金融  /  927138916  医务人员
	 */
	private String career;

	/**
	 * 允许值：0表示关闭，1表示开启
	 */
	private Short autoExtendEnabled;

	/**
	 * 平台类型 1 头条
	 */
	private Short platformId;

	/**
	 * 同步状态 0未同步/1同步成功/2同步失败
	 */
	private Short syncStatus;

	/**
	 * 转化跟踪在 第三方平台中的ID (头条/UC 的转化id)
	 */
	private String idAdPlatform;

	/**
	 * 创建时间
	 */
	private Date createtime;

	/**
	 * 文章分类
	 */
	private String articleCategory;

	/**
	 * E-COMMERCE 电商互动行为 ,NEWS 资讯互动行为, APP APP推广互动行为
	 */
	private String actionScene;

	/**
	 * 7, 15, 30, 60, 90, 180, 365
	 */
	private Integer actionDays;

	/**
	 * 行为类目
	 */
	private String actionCategories;

	/**
	 * 行为关键词
	 */
	private String actionWords;

	/**
	 * 兴趣目录
	 */
	private String interestCategories;

	/**
	 * 兴趣关键词
	 */
	private String interestWords;








	public String getLandingTypeName() {
		return ToutiaoAPLandTypeEnum.valueByType(this.getLandingType());
	}

	public String getDeliveryRangeName() {
		if (StringUtils.isBlank(this.getDeliveryRange())) {
			return null;
		} else {
			return ToutiaoDeliveryRangeEnum.valueByType(this.getDeliveryRange());
		}
	}

	public String getInterestActionMode(){
		if (StringUtils.isNotBlank(this.interestActionMode) &&
				this.interestActionMode.equals("UNLIMITED")){
			return null;
		}
		return this.interestActionMode;
	}

/*	private String launchPriceRange;*/
}