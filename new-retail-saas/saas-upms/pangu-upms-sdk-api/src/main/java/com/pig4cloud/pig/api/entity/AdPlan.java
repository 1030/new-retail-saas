package com.pig4cloud.pig.api.entity;

import cn.hutool.core.collection.ListUtil;
import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.pig4cloud.pig.api.vo.MaterialVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

/**
 * 广告计划表 ad_plan
 *
 * @author hma
 * @date 2020-11-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_plan")
public class AdPlan extends Model<AdPlan>
{
	private static final long serialVersionUID = 1L;


	/** 广告计划主键id */
	@JSONField(name = "ad_id")
	@TableId("ad_id")
	private Long adId;
	/** 广告计划名称 */
	@JSONField(name = "name")
	private String adName;
	private String status;
	/** 状态：AD_STATUS_DELIVERY_OK	投放中	AD_STATUS_DISABLE	计划暂停	AD_STATUS_AUDIT	新建审核中	AD_STATUS_REAUDIT	修改审核中	AD_STATUS_DONE	已完成（投放达到结束时间）	AD_STATUS_CREATE	计划新建	AD_STATUS_AUDIT_DENY	审核不通过	AD_STATUS_BALANCE_EXCEED	账户余额不足	AD_STATUS_BUDGET_EXCEED	超出预算	AD_STATUS_NOT_START	未到达投放时间	AD_STATUS_NO_SCHEDULE	不在投放时段	AD_STATUS_CAMPAIGN_DISABLE	已被广告组暂停	AD_STATUS_CAMPAIGN_EXCEED	广告组超出预算	AD_STATUS_DELETE	已删除	AD_STATUS_ALL	所有包含已删除	AD_STATUS_NOT_DELETE */
	/** 推广组主键id */
	@JSONField(name = "campaign_id")
	private Long campaignId;


	/** 投放范围 ： 通投智选(UNIVERSAL)  默认(DEFAULT) 穿山甲(UNION) */
	@JSONField(name = "delivery_range")
	private String deliveryRange;
	/** 投放形式： 激励视频(REWARDED_VIDEO) 原生(ORIGINAL_VIDEO) 开屏(SPLASH_VIDEO) */
	@JSONField(name = "union_video_type")
	private String unionVideoType;
	/** 目标：1 转化量 */
	private String target;
	/** 下载方式：DOWNLOAD_URL下载链接，QUICK_APP_URL快应用+下载链接，EXTERNAL_URL落地页链接 */
	@JSONField(name = "download_type")
	private String downloadType;
	/** 下载地址 */
	@JSONField(name = "download_url")
	private String downloadUrl;
	/** 落地页链接 */
	@JSONField(name = "external_url")
	private String externalUrl;
	/** 应用下载详情链接 */
	@JSONField(name = "quick_app_url")
	private String quickAppUrl;
	/** 定向包主键id */
	@JSONField(name = "audience_package_id")
	private Long audienceId;
	/** 转化目标主键id */
	@JSONField(name = "convert_id")
	private Long convertId;
	/** 投放时间类型："SCHEDULE_FROM_NOW"从今天起长期投放, "SCHEDULE_START_END"设置开始和结束日期 */
	@JSONField(name = "schedule_type")
	private String scheduleType;
	/** 投放开始时间 */
	@JSONField(name = "start_time")
	private String startTime;
	/** 投放结束时间 */
	@JSONField(name = "end_time")
	private String endTime;
	/** 投放场景：1 常规投放  2  放量投放 */
	@JSONField(name = "launch_scenario")
	private Integer launchScenario;
	/** 投放方式 */
	@JSONField(name = "flow_control_mode")
	private String flowControlMode;
	/** 创建时间 */
	@JSONField(name = "ad_create_time")
	private String  adCreateTime;
	/** 修改时间 */
	@JSONField(name = "ad_modify_time")
	private String adModifyTime;
	/** 投放场景：FLOW_CONTROL_MODE_FAST	优先跑量（对应CPC的加速投放）	FLOW_CONTROL_MODE_SMOOTH	优先低成本（对应CPC的标准投放）	FLOW_CONTROL_MODE_BALANCE	均衡投放（新增字段） */
	@JSONField(name = "smart_bid_type")
	private String smartBidType;
	/** 预算类型：取值: "BUDGET_MODE_DAY"日预算, "BUDGET_MODE_TOTAL"总预算 */
	@JSONField(name = "budget_mode")
	private String budgetMode;
	/** 预算(出价方式为CPC、CPM、CPV时，不少于100元；出价方式为OCPM、OCPC时，不少于300元；取值范围: ≥ 0 */
	private BigDecimal budget;
	/** 创意投放位置 String 接收String[] 类型的，在JSONObject.parseArray 时回转化为JSON字符串*/
	@JSONField(name = "inventory_type")
	private String inventoryType;
	/**
	 * 广告位大类。允许值 MANUAL首选媒体 SCENE场景广告位，SMART优选广告位，UNIVERSAL通投智选
	 */
	@JSONField(name = "inventory_catalog")
	private String inventoryCatalog;
	/**
	 * 优选广告位 允许值NORMAL表示不使用优选，SMART表示使用优选，UNIVERSAL表示通投
	 */
	@JSONField(name = "smart_inventory")
	private String smartInventory;
	/**
	 * 场景广告位
	 */
	@JSONField(name = "scene_inventory")
	private String sceneInventory;

	/**
	 * 目前仅使用了 点击监测链接
	 */
	@JSONField(name = "action_track_url")
	private String actionTrackUrl;
	/**
	 * 操作状态
	 */
	@JSONField(name = "opt_status")
	private String optStatus;
	@JSONField(name = "package")
	@TableField(value="package")
	private String downPackage;

	private String pricing;
	/** 点击出价/展示出价，当pricing为"CPC"、"CPM"、"CPA"出价方式时必填 */
	private BigDecimal bid;
	/** 目标转化出价/预期成本， 当pricing为"OCPM"、"OCPC"出价方式时必填） */
	@JSONField(name = "cpa_bid")
	private BigDecimal cpaBid;

	@JSONField(name = "hide_if_exists")
	private Integer hideIfExists;

	@JSONField(name = "hide_if_converted")
	private String hideIfConverted;

	/*@JSONField(name = "device_brand_type")
	private Integer deviceBrandType;*/

	@JSONField(name = "device_brand")
	private String deviceBrand;

	/*@JSONField(name = "launch_price_type")
	private Integer launchPriceType;*/

	@JSONField(name = "launch_price")
	private String launchPrice;

	@JSONField(name = "auto_extend_enabled")
	private Integer autoExtendEnabled;

	@JSONField(name = "landing_type")
	private String  landingType="APP";

	@JSONField(name = "app_type")
	private String appType="APP_ANDROID";

	@TableField(exist = false)
	private Audience audience;


	/**
	 * 地域
	 */
	private String district;

	/**
	 * 地域定向省市或者区县列表(当传递省份ID时,旗下市县ID可省略不传),当district为"CITY"或"COUNTY"时有值
	 */
	private String city;

	/**
	 * 商圈ID数组，district为"BUSINESS_DISTRICT"时有值
	 */
	@TableField(value="business_ids")
	private String businessIds;

	/**
	 * 位置类型
	 */
	@TableField(value="location_type")
	private String locationType;

	/**
	 * 性别
	 */
	private String gender;

	/**
	 * 年龄, 详见【附录-受众年龄区间】
	 * 取值: "AGE_BETWEEN_18_23", "AGE_BETWEEN_24_30","AGE_BETWEEN_31_40", "AGE_BETWEEN_41_49", "AGE_ABOVE_50"
	 */
	private String age;

	/**
	 * 定向人群包列表（自定义人群），内容为人群包id。如果选择"同时定向与排除"，需传入retargeting_tags_include和retargeting_tags_exclude
	 */
	@JSONField(name = "retargeting_tags_include")
	@TableField(value="retargeting_tags_include")
	private String retargetingTagsInclude;

	/**
	 * 排除人群包列表（自定义人群），内容为人群包id。如果选择"同时定向与排除"，需传入retargeting_tags_include和retargeting_tags_exclude
	 */
	@JSONField(name = "retargeting_tags_exclude")
	@TableField(value="retargeting_tags_exclude")
	private String retargetingTagsExclude;

	/**
	 * 行为兴趣
	 * 取值："UNLIMITED"不限,"CUSTOM"自定义,"RECOMMEND"系统推荐。若与自定义人群同时使用，系统推荐("RECOMMEND")不生效
	 * 仅推广范围为默认时可填，且不可与老版行为兴趣定向同时填写，否则会报错
	 */
	@TableField(value="interest_action_mode")
	private String interestActionMode;

	@TableField(value="action_scene")
	private String actionScene;

	@TableField(value="action_days")
	private Integer actionDays;

	@TableField(value="action_categories")
	private String actionCategories;

	@TableField(value="action_words")
	private String actionWords;

	@TableField(value="interest_categories")
	private String interestCategories;

	@TableField(value="interest_words")
	private String interestWords;

	private String platform;

	private String ac;

	@TableField(value="device_type")
	private String deviceType;

	@TableField(value="superior_popularity_type")
	private String superiorPopularityType;

	@TableField(value="flow_package")
	private String flowPackage;

	@TableField(value="exclude_flow_package")
	private String excludeFlowPackage;

	//转化目标
	@JSONField(name = "external_action")
	@TableField(value="external_action")
	private String externalAction;

	//深度转化目标
	@JSONField(name = "deep_external_action")
	@TableField(value="deep_external_action")
	private String deepExternalAction;

	//资产ID集合
	@JSONField(name = "asset_ids")
	@TableField(value="asset_ids")
	private String assetIds;

	@JSONField(name = "advertiser_id")
	private String advertiserId;

	//深度优化方式
	@JSONField(name = "deep_bid_type")
	private String deepBidType;

	//深度优化出价，deep_bid_type为"DEEP_BID_MIN"时必填。当对应的转化convert_id，设定深度转化目标时才会有效。
	//DEEP_BID_MIN即自定义双出价（手动出价方式下），  即smartBidType传SMART_BID_CUSTOM常规投放
	//取值范围：0.1~10000元
	@JSONField(name = "deep_cpabid")
	private BigDecimal deepCpabid;

	//深度转化ROI系数, 范围(0,5]，精度：保留小数点后四位, deep_bid_type为"ROI_COEFFICIENT"时必填
	@JSONField(name = "roi_goal")
	private BigDecimal roiGoal;
	// 新增字段  投放量级
	private Long  estimateNum;

	public AdPlan(){

	}
	public AdPlan(Long adId,String optStatus){
		this.adId=adId;
		this.optStatus=optStatus;

	}


	public AdPlan(String adName){
		this.adName=adName;

	}

	public AdPlan(Long adId,BigDecimal budget,BigDecimal cpaBid){
		this.adId=adId;
		this.budget=budget;
		this.cpaBid=cpaBid;

	}
	@JSONField(serialize=false,deserialize=false)
	@TableField(exist = false)
	private List<AdCreative> adCreatives = ListUtil.empty();
	@TableField(exist = false)
	private List<MaterialVo> materialVos = ListUtil.list(false);
}
