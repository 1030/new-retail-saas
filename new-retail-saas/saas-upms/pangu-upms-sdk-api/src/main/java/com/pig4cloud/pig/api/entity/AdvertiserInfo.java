package com.pig4cloud.pig.api.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class AdvertiserInfo implements Serializable{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -4215152352346341225L;
	
	private String advertiser_id;
    private String name;
    
    private BigDecimal balance;
    
    private BigDecimal valid_balance;

    private BigDecimal cash;
    
    private BigDecimal valid_cash;
    
    private BigDecimal grant;
    
    
    private BigDecimal valid_grant;
    
    private Date createtime;
    
    
    private Date updatetime;
    
    /**
     * 对应的账户管家账户
     */
    private String housekeeper;
    
    private BigDecimal budget;
    
    private String budget_mode;


	public AdvertiserInfo(){}
	public AdvertiserInfo(String  advertiserId,String budgetMode,BigDecimal budget,Date updatetime){
		this.advertiser_id=advertiserId;
		this.budget_mode=budgetMode;
		this.budget=budget;
		this.updatetime=updatetime;
	}
}
