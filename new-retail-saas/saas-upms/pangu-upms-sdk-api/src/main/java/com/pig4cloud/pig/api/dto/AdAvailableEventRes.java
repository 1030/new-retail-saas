package com.pig4cloud.pig.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 广告资产事件表
 * @author  chenxiang
 * @version  2022-04-01 16:25:19
 * table: ad_assets_event
 */
@Data
public class AdAvailableEventRes implements Serializable {
	/**
	 * 资产ID
	 */
	private Long assetId;
	/**
	 * 事件ID
	 */
	private Long eventId;
	/**
	 * 广告账户
	 */
	private String advertiserId;
	/**
	 * 事件回传方式列表，允许值:落地页支持:JSSDK JS埋码 、EXTERNAL_API API回传、XPATH XPath圈选应用支持：APPLICATION_API 应用API、APPLICATION_SDK 应用SDK、快应用支持：QUICK_APP_API 快应用API
	 */
	private String trackTypes;
	/**
	 * 事件类型
	 */
	private String eventType;
	/**
	 * 事件中文名称
	 */
	private String eventCnName;
	/**
	 * 事件描述
	 */
	private String description;
	/**
	 * 事件属性
	 */
	private String properties;
}

	

	
	

