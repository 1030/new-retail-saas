package com.pig4cloud.pig.api.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class TtCustomAudiencesDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3051067853803160999L;
	
	@NotBlank(message = "广告主ID不能为空")
	private String advertiser_id;//广告主ID
	
	@NotBlank(message = "数据源名称不能为空")
	private String data_source_name;//数据源名称, 限30个字符内
	
//	@NotBlank(message = "数据源描述不能为空")
	private String description;//数据源描述, 限256个字符内
	
	
	/**
	 * IMEI               = 0;
     * IDFA               = 1;
     * UID                = 2;
     * IMEI_MD5           = 4;
     * IDFA_MD5           = 5;
     * MOBILE_HASH_SHA256 = 6;
     * OAID               = 7;
     * OAID_MD5           = 8;
	 */
	private Integer data_format = 0;//数据格式, 枚举值:"0"：ProtocolBuffer
	private String file_storage_type="0";//数据存储类型, 枚举值:"0"：API
	
	@Size(min=1, max=1000, message = "文件路径不在范围1~1000范围内")
	private String[] file_paths;//一次上传最多1000个
	
	private String data_source_type = "UID";//投放数据源类型，枚举值如下: "UID"：用户ID, "DID"：设备ID ,默认值： "UID"
	
	private String downUrl;
	
	private String md5;
	
	@NotNull(message = "匹配类型不能为空")
	private Integer dataType;
	
}
