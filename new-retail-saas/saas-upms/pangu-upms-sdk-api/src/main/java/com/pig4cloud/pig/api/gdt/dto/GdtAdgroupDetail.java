package com.pig4cloud.pig.api.gdt.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 广点通-广告组表详情 gdt_adgroup
 * 
 * @author nml
 * @date 2020-12-22
 */
@Setter
@Getter
public class GdtAdgroupDetail {
	private static final long serialVersionUID = 1L;

	/** 广告组主键id */
	private Long adgroupId;
	/** 推广计划名称 */
	private String adgroupName;
	/** 广告版位：数组字符串 */
	private String siteSet;

	/** 广告出价，单位为分 */
	private BigDecimal bidAmount;

	/** 广告组日预算，单位为分，设置为 0 表示不设预算（即不限） */
	private BigDecimal dailyBudget;

	/** 开始投放日期，日期格式：YYYY-MM-DD，且日期小于等于 end_date */
	private String beginDate;
	private String endDate;

	/** 投放时间段，格式为 48 * 7 位字符串，且都为 0 和 1，以半个小时为最小粒度 */
	private String timeSeries;

	/** 客户设置的状态:AD_STATUS_NORMAL有效，AD_STATUS_SUSPEND暂停 */
	private String configuredStatus;
	private String systemStatus;

	/** 是否使用自动扩量，当广告为 oCPC/oCPM、CPC 出价时，可将 expand_enabled 设置为 true */
	private String expandEnabled;

	/*定向*/
	private String targeting;


	/* 广告组临时表获取 */
	//广告形式
	private String campaignType;

	//应用名称
	private String promotedObjectName;

	//投放时间类型
	private String timeSeriesType;
}
