
package com.pig4cloud.pig.api.dto;


import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


/**
 * Created by hma on 2020/11/13.
 * 广告计划（创意，广告组）对象dto
 */
@Setter
@Getter
public class AdvertiserDto {
	private Long advertiserId;

	/**
	 * 广告id
	 */
	private Long  adId;
	/**
	 * 广告创意名
	 */
	private Long creativeId;
	/**
	 * 广告组id
	 */
	private Long campaignId;
	/**
	 * 操作状态  "enable"表示启用计划,  "disable"表示暂停计划。
	 */
	private String optStatus ;

	/**
	 * 判断新老广告计划,1:表示老广告计划 2:表示新广告计划
	 */
	private String versionType;

	/**
	 * 广告计划出价
	 */
	private BigDecimal cpaBid;
	
	private String ignoreWarning;


	public  AdvertiserDto(){}

	public  AdvertiserDto(Long advertiserId, Long  adId,BigDecimal cpaBid){
		this.advertiserId=advertiserId;
		this.adId=adId;
		this.cpaBid=cpaBid;
	}

	public  AdvertiserDto(Long advertiserId, Long  adId,BigDecimal cpaBid,String versionType){
		this.advertiserId=advertiserId;
		this.adId=adId;
		this.cpaBid=cpaBid;
		this.versionType=versionType;
	}

}
