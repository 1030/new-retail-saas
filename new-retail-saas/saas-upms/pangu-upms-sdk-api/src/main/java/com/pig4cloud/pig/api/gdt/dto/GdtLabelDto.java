package com.pig4cloud.pig.api.gdt.dto;

import com.pig4cloud.pig.api.entity.GdtActionInterest;
import com.pig4cloud.pig.common.core.util.HashUtil;
import lombok.Data;

@Data
public class GdtLabelDto {
    /**
     * id，当 tag_class 为 KEYWORD 时，不返回
     */
    private Long id;
    /**
     *  BEHAVIOR :   行为
     *  INTEREST   : 兴趣
     *  INTENTION   :  意向
     */
    private String type;
    /**
     * 名称
     */
    private String  name;
    /**
     * 父节点 id，当节点为根节点时，该值为 0
     */
    private Long parent_id;
    /**
     * 父节点标签名，如果没有父节点返回空字符串
     */
    private String parent_name;
    /**
     * 城市级别，仅当 type=REGION、BUSINESS_DISTRICT 时有效，[枚举详情]
     */
    private String  city_level;
    /**
     * 行为兴趣标签返回类型，仅当 type=BEHAVIOR、INTEREST 时有效，[枚举详情]
     */
    private String tag_class;

    public GdtActionInterest convertGdtActionInterest(){
        GdtActionInterest gdtActionInterest=new GdtActionInterest();
        gdtActionInterest.setThirdCityLevel(this.getCity_level());
        gdtActionInterest.setThirdId(this.getId());
        gdtActionInterest.setThirdName(this.getName());
        gdtActionInterest.setThirdParentId(this.getParent_id());
        gdtActionInterest.setThirdParentName(this.getParent_name());
        gdtActionInterest.setThirdTagClass(this.getTag_class());
        gdtActionInterest.setType(this.getType());
        return gdtActionInterest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GdtLabelDto that = (GdtLabelDto) o;
        return this.hashCode() == HashUtil.apHash(String.format("%s_%s_%s",that.id,that.parent_id,that.type));
    }

    @Override
    public int hashCode() {

        return HashUtil.apHash(String.format("%s_%s_%s",id,parent_id,type));
    }
}
