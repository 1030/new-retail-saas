package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

/**
 * 资质文件
 *
 * @TableName ad_qualified_file
 */
@Data
@EqualsAndHashCode
@TableName(value = "ad_qualified_file")
public class AdQualifiedFileDO implements Serializable {

	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long qualifiedId;

	/**
	 * 资质文件类目，0：软著；1:版号
	 */
	private Integer qualifiedType;

	/**
	 * 主游戏ID
	 */
	private Long pgid;

	/**
	 * 子游戏ID
	 */
	private Long gameId;

	/**
	 * 文件名称
	 */
	private String fileName;

	/**
	 * 文件大小
	 */
	private Long fileSize;

	/**
	 * 文件类型
	 */
	private String fileType;

	/**
	 * 文件在本地地址
	 */
	private String filePath;

	/**
	 * 文件散列值
	 */
	private String fileMd5;

	/**
	 * 是否删除，0：否；1：是；
	 */
	private Integer isdelete;

	/**
	 * 创建人
	 */
	private Integer createuser;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	private LocalDateTime createtime;

	/**
	 * 修改人
	 */
	private Integer updateuser;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private LocalDateTime updatetime;

	/**
	 * 主游戏ID集合
	 */
	@TableField(exist = false)
	private Collection<Long> pgids;

	/**
	 * 主游戏名称
	 */
	@TableField(exist = false)
	private String pgname;

	/**
	 * 子游戏名称
	 */
	@TableField(exist = false)
	private String gameName;

	/**
	 * 创建者姓名
	 */
	@TableField(exist = false)
	private String createuserName;

	/**
	 * 修改者姓名
	 */
	@TableField(exist = false)
	private String updateuserName;

	public AdQualifiedFileDO setQualifiedId(Long qualifiedId) {
		this.qualifiedId = qualifiedId;
		return this;
	}

	public AdQualifiedFileDO setQualifiedType(Integer qualifiedType) {
		this.qualifiedType = qualifiedType;
		return this;
	}

	public AdQualifiedFileDO setPgid(Long pgid) {
		this.pgid = pgid;
		return this;
	}

	public AdQualifiedFileDO setGameId(Long gameId) {
		this.gameId = gameId;
		return this;
	}

	public AdQualifiedFileDO setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}

	public AdQualifiedFileDO setFileSize(Long fileSize) {
		this.fileSize = fileSize;
		return this;
	}

	public AdQualifiedFileDO setFileType(String fileType) {
		this.fileType = fileType;
		return this;
	}

	public AdQualifiedFileDO setFilePath(String filePath) {
		this.filePath = filePath;
		return this;
	}

	public AdQualifiedFileDO setFileMd5(String fileMd5) {
		this.fileMd5 = fileMd5;
		return this;
	}

	public AdQualifiedFileDO setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
		return this;
	}

	public AdQualifiedFileDO setCreateuser(Integer createuser) {
		this.createuser = createuser;
		return this;
	}

	public AdQualifiedFileDO setCreatetime(LocalDateTime createtime) {
		this.createtime = createtime;
		return this;
	}

	public AdQualifiedFileDO setUpdateuser(Integer updateuser) {
		this.updateuser = updateuser;
		return this;
	}

	public AdQualifiedFileDO setUpdatetime(LocalDateTime updatetime) {
		this.updatetime = updatetime;
		return this;
	}

	public AdQualifiedFileDO setPgids(Collection<Long> pgids) {
		this.pgids = pgids;
		return this;
	}

	public AdQualifiedFileDO setPgname(String pgname) {
		this.pgname = pgname;
		return this;
	}

	public AdQualifiedFileDO setCreateuserName(String createuserName) {
		this.createuserName = createuserName;
		return this;
	}

	public AdQualifiedFileDO setUpdateuserName(String updateuserName) {
		this.updateuserName = updateuserName;
		return this;
	}

}