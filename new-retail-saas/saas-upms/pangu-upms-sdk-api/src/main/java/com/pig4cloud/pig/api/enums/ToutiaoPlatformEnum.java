package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoPlatformEnum {

	//平台  1 ANDROID/ 2 IOS/ 3 PC/4 IPAD/ 5 WAP
	ANDROID  (  "1"   ,      "ANDROID"),
	IOS  (  "2"   ,      "IOS"),
	PC  (  "3"   ,      "PC"),
	IPAD  (  "4"   ,      "IPAD"),
	WAP  (  "5"   ,      "WAP");



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoPlatformEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoPlatformEnum item : ToutiaoPlatformEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoPlatformEnum item : ToutiaoPlatformEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoPlatformEnum item : ToutiaoPlatformEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
