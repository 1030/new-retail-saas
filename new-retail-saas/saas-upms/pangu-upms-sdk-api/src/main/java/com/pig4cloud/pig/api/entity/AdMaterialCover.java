/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 素材视频封面图
 *
 * @author pigx code generator
 * @date 2021-06-23 14:32:25
 */
@Data
@TableName("ad_material_cover")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "素材视频封面图")
public class AdMaterialCover extends Model<AdMaterialCover> {
private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id",type= IdType.AUTO)
    @ApiModelProperty(value="主键")
    private Long id;
    /**
     * 关联的素材ID
     */
    @ApiModelProperty(value="关联的素材ID")
    private Integer materialId;
    /**
     * 图片名称
     */
    @ApiModelProperty(value="图片名称")
    private String fileName;
    /**
     * 访问地址
     */
    @ApiModelProperty(value="访问地址")
    private String fileUrl;
    /**
     * 真实路径
     */
    @ApiModelProperty(value="真实路径")
    private String realPath;
    /**
     * 是否封面：1：是，0：否
     */
    @ApiModelProperty(value="是否封面：1：是，0：否")
    private Integer isCover;
    /**
     * 图片md5值
     */
    @ApiModelProperty(value="图片md5值")
    private String md5;
    /**
     * 是否删除：0:正常；1：删除
     */
    @ApiModelProperty(value="是否删除：0:正常；1：删除")
    private Integer status;
    }
