package com.pig4cloud.pig.api.gdt.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;


/**
 * 广点通推广计划级别报表日流水
 * 2020/12/16
 *
 * @author yk
 */
@Getter
@Setter
public class GdtCampaignDayReport implements Serializable {

	/**
	 * 客户设置的状态:AD_STATUS_NORMAL有效，AD_STATUS_SUSPEND暂停
	 */
	private String configuredStatus;

	/**
	 * 推广计划主键id
	 */
	private Long campaignId;

	/**
	 * 推广计划名称
	 */
	private String campaignName;

	/**
	 * 推广目标
	 */
	private String promotedObjectType;

	/**
	 * 推广计划日预算，单位为分，设置为 0 表示不设预算（即不限）
	 */
	private BigDecimal dailyBudget;

	/*曝光量*/
	private String viewCount;

	/*点击量*/
	private String validClickCount;

	/*点击率*/
	private String ctr;

	/*点击均价*/
	private String cpc;

	/*消耗*/
	private String cost;

	/**
	 * 广告账号
	 */
	private String accountId;

	public String getCost() {
		int a = Integer.parseInt(this.cost);
		DecimalFormat df=new DecimalFormat("0.00");
		this.cost = df.format((float)a/(float)100);
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	//计算点击率
	public String getCtr() {
		//曝光量为0
		if ("0".equals(this.viewCount)){
			return ctr;
		}else {//曝光量不为0
			int a = Integer.parseInt(this.validClickCount);
			int b = Integer.parseInt(this.viewCount);
			NumberFormat nf = NumberFormat.getPercentInstance();
			nf.setMinimumFractionDigits( 2 );
			this.ctr = nf.format((float) a / (float) b);
			return ctr;
		}
	}

	public void setCtr(String ctr) {
		this.ctr = ctr;
	}

	//计算点击均价
	public String getCpc() {
		//点击量为0
		if ("0".equals(this.validClickCount)){
			return cpc;
		}else {//点击量不为0
			int a = Integer.parseInt(this.cost);
			int b = Integer.parseInt(this.validClickCount);
			DecimalFormat df=new DecimalFormat("0.00");
			this.cpc = df.format((float)a/((float)b*100));
			return cpc;
		}
	}

	public void setCpc(String cpc) {
		this.cpc = cpc;
	}

}