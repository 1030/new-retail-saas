/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.feign.factory;

import com.pig4cloud.pig.api.feign.RemoteAdMaterialService;
import com.pig4cloud.pig.api.feign.fallback.RemoteAdMaterialServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author cx
 * @version 1.0.0
 * @Time 2021/10/14 17:22
 */
@Component
public class RemoteAdMaterialServiceFallbackFactory implements FallbackFactory<RemoteAdMaterialService> {

	@Override
	public RemoteAdMaterialService create(Throwable throwable) {
		RemoteAdMaterialServiceFallbackImpl remoteAdMaterialServiceFallback = new RemoteAdMaterialServiceFallbackImpl();
		remoteAdMaterialServiceFallback.setCause(throwable);
		return remoteAdMaterialServiceFallback;
	}
}
