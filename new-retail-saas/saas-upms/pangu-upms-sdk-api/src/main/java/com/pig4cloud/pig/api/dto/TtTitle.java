package com.pig4cloud.pig.api.dto;

import lombok.Data;

@Data
public class TtTitle {
	private String title;

	private Long[] creativeWordIds;

	private Long[] dpaDictIds;
}
