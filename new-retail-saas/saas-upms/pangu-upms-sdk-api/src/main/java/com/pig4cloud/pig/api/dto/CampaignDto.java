
package com.pig4cloud.pig.api.dto;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by hma on 2020/11/6.
 * 推广组统计查询对象参数
 */
@Setter
@Getter
public class CampaignDto  extends Page {
	private Long advertiserId;

	/**
	 * 开始时间
	 */
	private String sdate;
	/**
	 * 结束时间
	 */
	private String edate;
	/**
	 * 广告创意名称(广告计划名称)
	 */
	private String campaignName;

	/*广告组id*/
	private Long campaignId;

	private List<Long> advertiserArr;

	/**
	 * 1查询推广组    2 查询广告计划
	 */
	private Integer type;
	/**
	 * 投放类型：1 头条  2 广点通
	 */
	private Integer adType;

	private String[] advertiserIds;

	/**
	 * 操作类型 1 报表统计合计
	 */
	private  Integer operate;

}
