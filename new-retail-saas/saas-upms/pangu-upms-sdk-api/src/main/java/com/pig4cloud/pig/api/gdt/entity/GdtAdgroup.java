package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 广点通-广告组表 gdt_adgroup
 *
 * @author hma
 * @date 2020-12-05
 */
@Setter
@Getter
public class GdtAdgroup
{
	private static final long serialVersionUID = 1L;

	/** 广告组主键id */
	@TableId(value = "adgroup_id")
	private Long adgroupId;
	/** 推广计划名称 */
	private String adgroupName;
	/** 推广计划主键id */
	private Long campaignId;
	/** 广告版位：数组字符串 */
	private String siteSet;
	/** 广告优化目标类型 */
	private String optimizationGoal;
	/** 计费类型 */
	private String billingEvent;
	/** 广告出价，单位为分，出价限制：
CPC 出价限制：介于 10 分-10,000 分之间（0.1 元-100 元，单位为人民币）
CPM 出价限制：介于 150 分-99,900 之间（1.5 元-999 元，单位为人民币）
CPA 出价限制：介于 100 分-50,000 分之间（1 元-500 元，单位为人民币）
oCPC/oCPM 出价限制：介于 10 分-500,000 分之间（0.1 元-5000 元，单位为人民币） */
	private BigDecimal bidAmount;
	/** 广告组日预算，单位为分，设置为 0 表示不设预算（即不限） */
	private BigDecimal dailyBudget;
	/** 推广目标 */
	private String promotedObjectType;

	private String promotedObjectId;
	/** 定向id */
	private Long targetingId;

	/** 定向详细设置*/
	private String targeting;
	/** 场景定向 */
	private String sceneSpec;
	/** 开始投放日期，日期格式：YYYY-MM-DD，且日期小于等于 end_date */
	private String beginDate;
	/** 结束投放日期，日期格式：YYYY-MM-DD，大于等于今天，且大于等于 begin_date ；如果希望长期投放，传空字符串 */
	private String endDate;

	/** 投放时间段，格式为 48 * 7 位字符串，且都为 0 和 1，以半个小时为最小粒度 */
	private String timeSeries;
	/** 客户设置的状态:AD_STATUS_NORMAL有效，AD_STATUS_SUSPEND暂停 */
	private String configuredStatus;
	/** 自定义分类，关键词用半角逗号','分隔，如：本地生活,餐饮 */
	private String customizedCategory;
	/** 创建时间 */
	private Long createdTime;
	/** 修改时间 */
	private Long lastModifiedTime;

	/** 转换归因 */
	private String userActionSets;
	/** 删除状态：false 未删除 true 删除 */
	private String isDeleted;
	/** 扩量种子人群 */
	private String coldStartAudience;
	/** 是否使用自动扩量，当广告为 oCPC/oCPM、CPC 出价时，可将 expand_enabled 设置为 true */
	private String expandEnabled;
	/** 扩量不可突破定向 */
	private String expandTargeting;
	/** oCPC/oCPM 深度优化内容，oCPC/oCPM 深度优化内容，创建时若此字段不传，或传空则视为无限制条件； */
	private String deepConversionSpec;

	private String deepOptimizationGoal;
	/** 门店 id 列表 */
	private String poiList;
	/** 广告组在系统中的状态 */
	private String systemStatus;
	/** 出价系数设置内容，仅当投放 oCPC、oCPM 广告时可使用 */
	private String bidAdjustment;
	/**
	 * 是否开启自动版位  ：true 开启，false 不开启
	 */
	private Boolean automaticSiteEnabled;

	/**
		* 出价方式
	 */
	private String bidMode;
	/** 出价类型，当出价类型为 SMART_BID_TYPE_SYSTEMATIC 时，不可传入 bid_amount，暂不支持微信流量，功能灰度开放中,{ SMART_BID_TYPE_CUSTOM, SMART_BID_TYPE_SYSTEMATIC } */
	/**
	 * SMART_BID_TYPE_CUSTOM
	 * 手动出价
	 * SMART_BID_TYPE_SYSTEMATIC
	 * 自动出价
	 */
	private String smartBidType;
	/** 广告账号 */
	private String accountId;

	private String status;

	private String firstDayBeginTime;

	private String appAndroidChannelPackageId;

	private Long dynamicCreativeId;

	private String dynamicCreativeIdSet;

	private String dynamicAdSpec;

	private String isRewardedVideoAd;

	private String  bidStrategy;
	private String autoAudience;
	private String deepOptimizationActionType;
	private String conversionId;
	private String deepConversionBehaviorBid;
	private String deepConversionWorthRate;
	//private String bidMode;
	private String autoAcquisitionEnabled;
	private Integer autoAcquisitionBudget;
	private String autoDerivedCreativeEnabled;
	private String targetingTranslation;

	private String creativeDisplayType;

	public GdtAdgroup(){}
	public GdtAdgroup(Long adgroupId){
		this.adgroupId=adgroupId;

	}


	public GdtAdgroup(String adgroupName){
		this.adgroupName=adgroupName;
	}
}
