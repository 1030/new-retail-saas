package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 广点通动态词包
 * gdt_dynamic_wordpack
 * @author nml
 * @date 2020-12-03 15:21:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="gdt_dynamic_wordpack")
public class GdtDynamicWordpack extends Model<GdtDynamicWordpack>{
    /**
     */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value = "id")
    private Long id;

    /**
     * 词包名称
     */
	@ApiModelProperty
	@TableField(value = "name")
    private String name;

    /**
     * 通配符
     */
	@ApiModelProperty
	@TableField(value = "wildcard")
    private String wildcard;

    /**
     * 词包长度
     */
	@ApiModelProperty
	@TableField(value = "length")
    private String length;

    /**
     * 词包类型：根据广告版位和创意形式ID可用性定义;1:系统动态词包
     */
	@ApiModelProperty
	@TableField(value = "type")
    private String type;

    /**
     * 广告版位
     */
	@ApiModelProperty
	@TableField(value = "siteset")
    private String siteset;

    /**
     * 创意形式ID
     */
	@ApiModelProperty
	@TableField(value = "creative_form_id")
    private String creativeFormId;

    /**
     * 备注
     */
	@ApiModelProperty
	@TableField(value = "remark")
    private String remark;

    /**
     * 是否删除：0正常，1删除
     */
	@ApiModelProperty
	@TableField(value = "isdelete")
    private Integer isdelete;

    /**
     * 创建人
     */
	@ApiModelProperty
	@TableField(value = "createuser")
    private String createuser;

    /**
     * 修改人
     */
	@ApiModelProperty
	@TableField(value = "updateuser")
    private String updateuser;

    /**
     * 创建时间
     */
	@ApiModelProperty
	@TableField(value = "createtime",fill= FieldFill.INSERT)
    private Date createtime;

    /**
     * 修改时间
     */
	@ApiModelProperty
	@TableField(value = "updatetime",fill= FieldFill.UPDATE)
    private Date updatetime;
}