package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_action_interest")
public class AdActionInterest extends Model<AdActionInterest> {

	@TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 三方主键父id
     */
    private Long thirdParentId;
    /**
     * 三方主键id
     */
    private Long thirdId;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 人数覆盖量
     */
    private String num;
    /**
     * 广告账号
     */
    private String advertiserId;
    /**
     * 名称
     */
    private String name;



    public AdActionInterest(){

    }

    public AdActionInterest(Integer type){
        this.type=type;

    }




}
