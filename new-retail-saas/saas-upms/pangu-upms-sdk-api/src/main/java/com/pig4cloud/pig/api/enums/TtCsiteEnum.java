package com.pig4cloud.pig.api.enums;

import com.pig4cloud.pig.common.core.util.NumberUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 头条版位
 */
public enum TtCsiteEnum {
	/**
	 * 投放版位
	 */

	jrtt("今日头条", new Long[]{0L, 10000L}, new Long[]{80000L, 110001L}),
	xgsp("西瓜视频", new Long[]{10001L,10099L}, null),
	hsxsp("火山小视频", new Long[]{30001L,30099L}, null),
	dx("抖音", new Long[]{40001L,40099L}, null),
	fqxs("番茄小说", new Long[]{26001L,26099L}, null),
	csjkpgg("穿山甲开屏广告", new Long[]{800000000L,800000000L}, null),
	csjfkpgg("穿山甲网盟非开屏广告", new Long[]{900000000L,900000000L}, null),
	ttggw("通投广告位", new Long[]{33013L,33013L}, null),
	ss("搜索", new Long[]{38016L,38016L}, null),
	;
	/**
	 * 名称
	 */
	private String name;

	private Long[] range1;

	private Long[] range2;


	TtCsiteEnum(String name, Long[] range1, Long[] range2){
		this.name = name;
		this.range1 = range1;
		this.range2 = range2;
	}

	public String getName() {
		return name;
	}

	public Long[] getRange1() {
		return range1;
	}

	public Long[] getRange2() {
		return range2;
	}


	//通过范围值得到名称

	public static String getNameByValue(String value){
		if (StringUtils.isBlank(value) || !NumberUtil.isNumeric(value)){
			return null;
		}

		for (TtCsiteEnum item : TtCsiteEnum.values()) {
			Long[] range1 = item.getRange1();
			Long[] range2 = item.getRange2();
			if ((Long.valueOf(value).compareTo(range1[0]) >= 0 && Long.valueOf(value).compareTo(range1[1]) <= 0) ||
					(range2 != null && Long.valueOf(value).compareTo(range2[0]) >= 0 && Long.valueOf(value).compareTo(range2[1]) <= 0)) {
				return item.name;
			}
		}
		return null;

	}
}
