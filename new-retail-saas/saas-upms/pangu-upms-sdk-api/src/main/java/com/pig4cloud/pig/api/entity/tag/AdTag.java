package com.pig4cloud.pig.api.entity.tag;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 标签表(ad_tag)实体类
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ad_tag")
public class AdTag extends Model<AdTag> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id",type = IdType.AUTO)
	private Long id;
    /**
     * 标签名称
     */
    private String tagName;
    /**
     * 标签颜色
     */
    private String tagColor;
    /**
     * 是否删除  0否 1是
     */
    private Integer deleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(update = "now()")
	private Date updateTime;
    /**
     * 创建人
     */
    private Long createId;
    /**
     * 修改人
     */
    private Long updateId;

}