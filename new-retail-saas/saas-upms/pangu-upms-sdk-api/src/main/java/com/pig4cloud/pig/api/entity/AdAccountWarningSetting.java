package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author chengang
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_account_warning_setting")
public class AdAccountWarningSetting extends Model<AdAccountWarningSetting> {

	private static final long serialVersionUID = -4846463548424420828L;

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键id")
	private Long id;

	@TableField(value = "days")
	@ApiModelProperty(value = "不活跃的天数")
	private Integer days;

	@TableField(value = "users")
	@ApiModelProperty(value = "通知人集合逗号分割")
	private String users;

	@TableField(value = "times")
	@ApiModelProperty(value = "通知时间点")
	private String times;

	@TableField(value = "send_date")
	@ApiModelProperty(value = "每日发送邮件日期")
	private Date sendDate;


	@TableField(value = "is_send")
	@ApiModelProperty(value = "是否发送邮件  1没有 2已发送")
	private Integer isSend;


	@TableField(value = "is_deleted")
	@ApiModelProperty(value = "是否删除  0否 1是")
	private Integer isDeleted;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(value = "update_time")
	private Date updateTime;

	@TableField(value = "create_id")
	@ApiModelProperty(value = "创建人")
	private Long createId;

	@TableField(value = "update_id")
	@ApiModelProperty(value = "修改人")
	private Long updateId;


}
