package com.pig4cloud.pig.api.gdt.dto;

import lombok.Data;

/**
 * 广点通-广告组表 gdt_adgroup
 * 
 * @author yk
 * @date 2020-12-08
 */
@Data
public class GdtAdGroupDeep
{
	private static final long serialVersionUID = 1L;

	/** oCPC/oCPM 深度优化内容，oCPC/oCPM 深度优化内容，创建时若此字段不传，或传空则视为无限制条件； */
	private String deep_conversion_type;

	private GdtAdGroupDeepGoal deep_conversion_behavior_spec;

	private GdtAdGroupDeepGoal deep_conversion_worth_spec;

	private GdtAdGroupDeepGoal deep_conversion_worth_advanced_spec;


}
