package com.pig4cloud.pig.api.gdt.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @projectName:pig
 * @description:广点通人群信息
 * @author:Zhihao
 * @createTime:2020/12/5 17:56
 * @version:1.0
 */
@Getter
@Setter
public class GdtCustomAudiences implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 客户群主键ID
	 */
	@TableId(value = "id")
	private Integer id;
	/**
	 * 人群 id
	 */
	private Long audienceId;
	/**
	 * 人群归属的推广帐号 id
	 */
	private Long accountId;
	/**
	 * 人群名称
	 */
	private String name;
	/**
	 * 广告主对人群在自己系统里的编码
	 */
	private String externalAudienceId;
	/**
	 * 人群描述
	 */
	private String description;
	/**
	 * 人群类型
	 * 号码文件人群=CUSTOMER_FILE，
	 * 拓展人群=LOOKALIKE，
	 * 用户行为人群=USER_ACTION，
	 * 地理位置人群=LBS，
	 * 关键词人群=KEYWORD，
	 * 广告人群=AD，
	 * 组合人群=COMBINE，
	 * 标签人群=LABEL
	 */
	private String type;
	/**
	 * 处理状态
	 * ：待处理=PENDING，
	 * 处理中=PROCESSING，
	 * 成功可用=SUCCESS，
	 * 错误=ERROR
	 */
	private String status;
	/**
	 * 人群错误码, 1 表示系统错误； 101 表示种子人群活跃用户低于 2K ； 102 表示种子人群无共同特征； 201 表示人群上传的号码包文件格式错误； 202 表示解析人群上传的号码包文件失败； 203 表示号码包文件人群匹配失败。
	 */
	private Integer errorCode;
	/**
	 * 用户覆盖数
	 */
	private Long userCount;
	/**
	 * 创建时间，格式为 yyyy-MM-dd HH:mm:ss，如 2016-11-01 10:42:56
	 */
	private String createdTime;
	/**
	 * 最后更新时间，格式为 yyyy-MM-dd HH:mm:ss，如 2016-11-01 10:42:56
	 */
	private String lastModifiedTime;
}