package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum GdtPageStatusEnum {

	//落地页审核状态
	LANDING_PAGE_STATUS_EDITING   ("LANDING_PAGE_STATUS_EDITING", "编辑中"),
	LANDING_PAGE_STATUS_PENDING("LANDING_PAGE_STATUS_PENDING", "待审核"),
	LANDING_PAGE_STATUS_APPROVED  ("LANDING_PAGE_STATUS_APPROVED","审核通过"),
	LANDING_PAGE_STATUS_REJECTED  ("LANDING_PAGE_STATUS_REJECTED","审核不过"),
	LANDING_PAGE_STATUS_DELETED  ("LANDING_PAGE_STATUS_DELETED","操作版本已删除");

	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	GdtPageStatusEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过type   取name
	 * @return
	 */
	public static String nameByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (GdtPageStatusEnum item : GdtPageStatusEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;
	}
}
