package com.pig4cloud.pig.api.feign.fallback;

import com.pig4cloud.pig.api.entity.SelfCustomAudience;
import com.pig4cloud.pig.api.feign.RemoteSelfAudienceService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * @Description
 * @Author chengang
 * @Date 2022/7/18
 */
@Slf4j
@Component
public class RemoteSelfAudienceServiceFallbackImpl implements RemoteSelfAudienceService {

	@Setter
	private Throwable cause;

	@Override
	public List<SelfCustomAudience> getListByDto(String from, SelfCustomAudience entity) {
		log.error("getListByDto is error:", cause);
		return Collections.emptyList();
	}

	@Override
	public boolean saveEntity(String from, SelfCustomAudience entity) {
		log.error("saveEntity is error:", cause);
		return false;
	}
}
