package com.pig4cloud.pig.api.dto;

import com.pig4cloud.pig.api.util.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author ：lile
 * @date ：2021/7/14 14:31
 * @description：
 * @modified By：
 */
@Data
public class AdAccountAgentDto extends Page {
	/**
	 * 代理商管理的主键
	 */
	private String id;
	/**
	 * 广告账户ID
	 */
	private String[] advertiserArr;


	private String advertiserId;
	/**
	 * 广告账户名称
	 */
	private String advertiserName;
	/**
	 * 代理商ID
	 */
	private Integer agentId;
	/**
	 * 代理商名称
	 */
	private String agentName;
	/**
	 * 生效时间
	 */
	private String effectiveTime;
	/**
	 * 失效时间
	 */
	private String invalidTime;

	/**
	 * 插入数据库的世间
	 */
	private Date effectTime;

	/**
	 * 插入数据库的世间
	 */
	private Date dealTime;

	/**
	 * 创建人
	 */
	private String createUser;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改人
	 */
	private String updateUser;
	/**
	 * 修改时间
	 */
	private Date updateTime;
}
