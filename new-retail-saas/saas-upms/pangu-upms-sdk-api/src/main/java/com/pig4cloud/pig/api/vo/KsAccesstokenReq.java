package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 快手授权token表
 * @author  chenxiang
 * @version  2022-03-22 20:29:09
 * table: ks_accesstoken
 */
@Data
@Accessors(chain = true)
public class KsAccesstokenReq extends Page {

	private String state;

	private String authCode;

	private Integer userId;

	private Integer agentId;
}
