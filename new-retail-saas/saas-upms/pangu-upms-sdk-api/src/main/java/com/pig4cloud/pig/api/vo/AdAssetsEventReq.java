package com.pig4cloud.pig.api.vo;

import com.alibaba.fastjson.JSONArray;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

/**
 * 广告资产事件表
 * @author  chenxiang
 * @version  2022-04-01 16:25:19
 * table: ad_assets_event
 */
@Data
public class AdAssetsEventReq extends Page {
	/**
	 * 广告账户ID
	 */
	private String advertiserId;
	/**
	 * 资产ID
	 */
	private String assetId;
	/**
	 * 事件ID
	 */
	private String eventId;
	/**
	 * 事件类型
	 */
	private String eventType;
	/**
	 * 事件名称
	 */
	private String eventCnName;
	/**
	 * 统计方式 ： 统计方式， 允许值：ONLY_ONE仅一次、EVERY_ONE每一次（仅付费ROI时可用）
	 */
	private String statisticalType;
	/**
	 * 事件回传方式列表，允许值:落地页支持:JSSDK JS埋码 、EXTERNAL_API API回传、XPATH XPath圈选应用支持：APPLICATION_API 应用API、APPLICATION_SDK 应用SDK、快应用支持：QUICK_APP_API 快应用API
	 */
	private String trackTypes;
	/**
	 * 事件信息列表
	 */
	private JSONArray event;
}
