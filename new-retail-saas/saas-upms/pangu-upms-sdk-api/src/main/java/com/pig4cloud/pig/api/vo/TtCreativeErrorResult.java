package com.pig4cloud.pig.api.vo;

import lombok.Data;

/**
 * @Description
 * @Author chengang
 * @Date 2021/9/14
 */
@Data
public class TtCreativeErrorResult {

	private String code;
	private String message;

}
