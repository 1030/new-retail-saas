package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoDeviceBrandEnum {

	HONOR      ("HONOR"      ,"荣耀"    ),
	APPLE      ("APPLE"      ,"苹果"    ),
	HUAWEI     ("HUAWEI"     ,"华为"    ),
	XIAOMI     ("XIAOMI"     ,"小米"    ),
	SAMSUNG    ("SAMSUNG"    ,"三星"    ),
	OPPO       ("OPPO"       ,"OPPO"    ),
	VIVO       ("VIVO"       ,"VIVO"    ),
	MEIZU      ("MEIZU"      ,"魅族"    ),
	GIONEE     ("GIONEE"     ,"金立"    ),
	COOLPAD    ("COOLPAD"    ,"酷派"    ),
	LENOVO     ("LENOVO"     ,"联想"    ),
	LETV       ("LETV"       ,"乐视"    ),
	ZTE        ("ZTE"        ,"中兴"    ),
	CHINAMOBILE("CHINAMOBILE","中国移动"),
	HTC        ("HTC"        ,"HTC"     ),
	PEPPER     ("PEPPER"     ,"小辣椒"  ),
	NUBIA      ("NUBIA"      ,"努比亚"  ),
	HISENSE    ("HISENSE"    ,"海信"    ),
	QIKU       ("QIKU"       ,"奇酷"    ),
	TCL        ("TCL"        ,"TCL"     ),
	SONY       ("SONY"       ,"索尼"    ),
	SMARTISAN  ("SMARTISAN"  ,"锤子手机"),
    qh360      ("360"        ,"360手机" ),
	ONEPLUS    ("ONEPLUS"    ,"一加手机"),
	LG         ("LG"         ,"LG"      ),
	MOTO       ("MOTO"       ,"摩托罗拉"),
	NOKIA      ("NOKIA"      ,"诺基亚"  ),
	GOOGLE     ("GOOGLE"     ,"谷歌"    );



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoDeviceBrandEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoDeviceBrandEnum item : ToutiaoDeviceBrandEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoDeviceBrandEnum item : ToutiaoDeviceBrandEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoDeviceBrandEnum item : ToutiaoDeviceBrandEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
