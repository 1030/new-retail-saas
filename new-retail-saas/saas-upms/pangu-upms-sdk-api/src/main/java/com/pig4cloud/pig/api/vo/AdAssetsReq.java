package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;

import java.util.List;

/**
 * 广告资产表
 * @author  chenxiang
 * @version  2022-04-01 15:02:28
 * table: ad_assets
 */
@Data
public class AdAssetsReq extends Page {
	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 资产ID
	 */
	private String assetId;
	/**
	 * 应用资产名
	 */
	private String assetName;
	/**
	 * 广告账户
	 */
	private String advertiserId;
	/**
	 * 资产类型 THIRD_EXTERNAL 三方落地页、APP 应用、QUICK_APP 快应用
	 */
	private String assetType;
	/**
	 * 应用包ID
	 */
	private String packageId;
	/**
	 * 包名
	 */
	private String packageName;
	/**
	 * appId
	 */
	private String appCloudId;
	/**
	 * 应用名
	 */
	private String appName;
	/**
	 * 应用类型，允许值：IOS、Android
	 */
	private String appType;
	/**
	 * 下载地址
	 */
	private String downloadUrl;
	/**
	 * 来源(1:API,2:渠道后台)
	 */
	private Integer soucreStatus;
	/**
	 * 事件名称
	 */
	private String eventCnName;
	/**
	 * 广告账户列表
	 */
	private List<String> adList;
}
