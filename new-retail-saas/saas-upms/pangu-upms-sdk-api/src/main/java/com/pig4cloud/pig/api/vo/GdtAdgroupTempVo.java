package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.gdt.entity.GdtAdgroupTemp;
import lombok.Getter;
import lombok.Setter;


/**
 * 广点通-广告组临时表 gdt_adgroup_temp
 * 
 * @author hma
 * @date 2020-12-05
 */
@Setter
@Getter
public class GdtAdgroupTempVo extends GdtAdgroupTemp
{
	private static final long serialVersionUID = 1L;

	/**
	 * 定向包名称
	 */
     private String targetingName;

	/**
	 * 判断新增还是更新：0 新增，1 更新
	 */
	//private String idStr;


}
