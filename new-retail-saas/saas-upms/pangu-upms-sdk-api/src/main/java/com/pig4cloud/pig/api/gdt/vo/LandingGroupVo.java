package com.pig4cloud.pig.api.gdt.vo;

import lombok.Getter;
import lombok.Setter;


/**
 * @程序化落地页
 * @author yk
 *
 */
@Getter
@Setter
public class LandingGroupVo{

	/**
	 * 落地页组ID
	 */
	private String groupId;

	/**
	 * 落地页组名称
	 */
	private String groupTitle;

	/**
	 * 落地页组 URL
	 */
	private String groupUrl;

	/**
	 * 落地页组状态
	 */
	private String groupStatus;

	/**
	 * 流量分配方式
	 */
	private String groupFlowType;

	/**
	 * 站点列表
	 */
	private String sites;

	/**
	 * 广告主id
	 */
	private String advertiserId;

	/**
	 * 创建时间
	 */
	private String createTime;

	/**
	 * 更新时间
	 */
	private String updateTime;

	/**
	 * 多个广告主id
	 */
	private String[] advertiserIds;
}
