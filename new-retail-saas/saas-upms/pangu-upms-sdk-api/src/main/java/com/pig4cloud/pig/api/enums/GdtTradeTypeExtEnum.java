package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum GdtTradeTypeExtEnum {
	//广点通
	CHARGE("CHARGE","充值"),
	TRANSFER_IN("TRANSFER_IN","转入"),
	PAY("PAY","消费"),
	TRANSFER_BACK("TRANSFER_BACK","回划"),
	EXPIRE("EXPIRE","过期");


	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	GdtTradeTypeExtEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (GdtTradeTypeExtEnum item : GdtTradeTypeExtEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (GdtTradeTypeExtEnum item : GdtTradeTypeExtEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (GdtTradeTypeExtEnum item : GdtTradeTypeExtEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
