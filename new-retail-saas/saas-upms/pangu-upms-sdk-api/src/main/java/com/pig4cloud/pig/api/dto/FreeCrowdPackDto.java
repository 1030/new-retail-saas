package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/6/28 21:16
 * @description：
 * @modified By：
 */
@Data
public class FreeCrowdPackDto implements Serializable {

	private static final long serialVersionUID = -4709678072731116081L;

	private String pgidArr;

	private Integer startDate;

	private Integer endDate;

	// 开始充值金额
	private BigDecimal startPay;

	// 结束充值金额
	private BigDecimal endPay;

	// 开始活跃天数
	private Integer activeStartNum;

	//结束活跃天数
	private Integer activeEndNum;

	//开始活跃最大天数
	private Integer activeStartMaxNum;

	//结束活跃最大天数
	private Integer activeEndMaxNum;


	//打包字段 0：IMEI 1:IDFA 2:UID 4:IMEI_MD5 5:IDFA_MD5 6:MOBILE_HASH_SHA256  7:OAID 8:OAID_MD5 9:MOBILE_MD5 10:MAC地址
	private Integer type;
	//人群包名称
	private String packageName;

}
