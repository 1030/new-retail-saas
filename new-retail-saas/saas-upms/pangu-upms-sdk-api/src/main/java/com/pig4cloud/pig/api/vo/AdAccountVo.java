/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.util.Page;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 广告账户表
 *
 * @author pigx code generator
 * @date 2021-05-31 10:53:49
 */
@Data
@Accessors(chain = true)
public class AdAccountVo extends Page {
	/**
	 * 主键
	 */
	private String id;
	/**
	 * 媒体编码
	 */
	private String mediaCode;
	/**
	 * 广告账户ID
	 */
	private String advertiserId;
	/**
	 * 广告账户ID列表
	 */
	private List<String> advertiserIds;
	/**
	 * 广告账户名称
	 */
	private String advertiserName;
	/**
	 * 投放人
	 */
	private String throwUser;
	/**
	 * 代理商ID
	 */
	private String agentId;

	/**
	 * 用户id数组
	 */
	private List<String> userArr;
	/**
	 * 是否超管角色 true 超管 false 非超管
	 */
	private Boolean isAdmin;
	/**
	 * 是否系统管理员
	 */
	private int isSys = 0;
	/**
	 * 当前角色管理的用户列表
	 */
	public String userIds;


	/**
	 * 开始时间
	 */
	private Long startTime;

	/**
	 * 开始时间
	 */
	private Long endTime;

	/**
	 * 管家账户
	 */
	private String housekeeper;

	/**
	 * 是否是母账号
	 */
	private Integer isPaccount;

	public AdAccountVo setId(String id) {
		this.id = id;
		return this;
	}

	public AdAccountVo setMediaCode(String mediaCode) {
		this.mediaCode = mediaCode;
		return this;
	}

	public AdAccountVo setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
		return this;
	}

	public AdAccountVo setAdvertiserName(String advertiserName) {
		this.advertiserName = advertiserName;
		return this;
	}

	public AdAccountVo setThrowUser(String throwUser) {
		this.throwUser = throwUser;
		return this;
	}

	public AdAccountVo setAgentId(String agentId) {
		this.agentId = agentId;
		return this;
	}

	public AdAccountVo setUserArr(List<String> userArr) {
		this.userArr = userArr;
		return this;
	}

	public AdAccountVo setAdmin(Boolean admin) {
		isAdmin = admin;
		return this;
	}

	public AdAccountVo setIsSys(int isSys) {
		this.isSys = isSys;
		return this;
	}

	public AdAccountVo setUserIds(String userIds) {
		this.userIds = userIds;
		return this;
	}

	public AdAccountVo setStartTime(Long startTime) {
		this.startTime = startTime;
		return this;
	}

	public AdAccountVo setEndTime(Long endTime) {
		this.endTime = endTime;
		return this;
	}

	public AdAccountVo setHousekeeper(String housekeeper) {
		this.housekeeper = housekeeper;
		return this;
	}

	public AdAccountVo setIsPaccount(Integer isPaccount) {
		this.isPaccount = isPaccount;
		return this;
	}
}
