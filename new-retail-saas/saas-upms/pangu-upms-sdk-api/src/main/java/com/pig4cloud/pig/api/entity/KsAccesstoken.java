package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 快手授权token表
 * @author  chenxiang
 * @version  2022-03-22 20:29:09
 * table: ks_accesstoken
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ks_accesstoken")
public class KsAccesstoken extends Model<KsAccesstoken>{
	/**
	 * 广告主 ID
	 */
	@TableId(value = "advertiser_id")
	private String advertiserId;
	/**
	 * 申请应用后快手返回的 app_id
	 */
	@TableField(value = "app_id")
	private String appId;
	/**
	 * 申请应用后快手返回的 secret
	 */
	@TableField(value = "secret")
	private String secret;
	/**
	 * 授权时返回的 auth_code
	 */
	@TableField(value = "auth_code")
	private String authCode;
	/**
	 * 用于验证权限的 token
	 */
	@TableField(value = "access_token")
	private String accessToken;
	/**
	 * access_token 剩余有效时间，单位：秒
	 */
	@TableField(value = "access_token_expires_in")
	private Long accessTokenExpiresIn;
	/**
	 * 用于获取新的 access_token 和 refresh_token，并且刷新过期时间
	 */
	@TableField(value = "refresh_token")
	private String refreshToken;
	/**
	 * refresh_token 剩余有效时间，单位：秒
	 */
	@TableField(value = "refresh_token_expires_in")
	private Long refreshTokenExpiresIn;
	/**
	 * 已授权账户所有的 account_id
	 */
	@TableField(value = "advertiser_ids")
	private String advertiserIds;
	/**
	 * 获取token的最新时间
	 */
	@TableField(value = "token_time")
	private Date tokenTime;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

