package com.pig4cloud.pig.api.gdt.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @projectName:pig
 * @description:广点通人群文件信息
 * @author:Zhihao
 * @createTime:2020/12/5 21:00
 * @version:1.0
 */
@Setter
@Getter
public class GdtCustomAudiencesFile {
	private static final long serialVersionUID = 1L;

	/**
	 * 客户群文件主键ID
	 */
	private Integer id;
	/**
	 * 数据文件id
	 */
	private Integer customAudienceFileId;
	/**
	 * 文件名称
	 */
	private String name;
	/**
	 * 人群归属的推广帐号 id
	 */
	private Long accountId;
	/**
	 * 人群 id
	 */
	private Long audienceId;
	/**
	 * 号码包用户 id 类型，[枚举详情]枚举列表：{ GDT_OPENID, HASH_IDFA, HASH_IMEI, HASH_MAC, HASH_MOBILE_PHONE, HASH_QQ, IDFA, IMEI, MAC, MOBILE_QQ_OPENID, QQ, WX_OPENID, WECHAT_OPENID, SALTED_HASH_IMEI, SALTED_HASH_IDFA, OAID, HASH_OAID }
	 */
	private String userIdType;
	/**
	 * 文件操作类型，如果不填则默认为 APPEND，[枚举详情]枚举列表：{ APPEND, REDUCE }
	 */
	private String operationType;
	/**
	 * 微信/QQ 手机版 appid，当且仅当数据文件中的用户 id 类型为 WECHAT_OPENID（WX_OPENID）或 MOBILE_QQ_OPENID 时有效字段长度最小 1 字节，长度最大 128 字节
	 */
	private String openAppId;
	/**
	 * 数据加密的盐 id，当且仅当数据文件中的用户 id 类型为 SALTED_HASH_IMEI 或 SALTED_HASH_IDFA 时有效字段长度最小 1 字节，长度最大 32 字节
	 */
	private Integer saltId;
	/**
	 * 处理状态
	 */
	private String processStatus;
	/**
	 * 处理完成后的状态码，0 表示成功，非 0 失败。
	 */
	private Integer processCode;
	/**
	 * 错误具体信息
	 */
	private String errorMessage;
	/**
	 * 文件总行数
	 */
	private Integer lineCount;
	/**
	 * 文件中没有格式错误的行数
	 */
	private Integer validLineCount;
	/**
	 * 文件包含的用户数
	 */
	private Integer userCount;
	/**
	 * 文件大小
	 */
	private Integer size;
	/**
	 * 创建时间，格式为 yyyy-MM-dd HH:mm:ss,如 2016-11-01 10:42:56
	 */
	private String createdTime;

}