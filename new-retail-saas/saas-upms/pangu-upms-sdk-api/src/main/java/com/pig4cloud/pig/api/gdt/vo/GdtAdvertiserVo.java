package com.pig4cloud.pig.api.gdt.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class GdtAdvertiserVo {
	private String accountId;

	private String corporationName;

	private BigDecimal dailyBudget;

	//总余额
	private BigDecimal totalBalance;

	//现金余额
	private BigDecimal cashBalance;

	//分成余额
	private BigDecimal sharedBalance;

	//赠送余额
	private BigDecimal giftBalance;

	private BigDecimal alertFund;

	private String orderDailyBudget;

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance.divide(new BigDecimal(100));
	}

	public void setCashBalance(BigDecimal cashBalance) {
		this.cashBalance = cashBalance.divide(new BigDecimal(100));
	}
	public void setSharedBalance(BigDecimal sharedBalance) {
		this.sharedBalance = sharedBalance.divide(new BigDecimal(100));
	}
	public void setGiftBalance(BigDecimal giftBalance) {
		this.giftBalance = giftBalance.divide(new BigDecimal(100));
	}
}
