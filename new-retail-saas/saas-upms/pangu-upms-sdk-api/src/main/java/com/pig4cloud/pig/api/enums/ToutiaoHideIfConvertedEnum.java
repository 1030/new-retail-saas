package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoHideIfConvertedEnum {

	//平台  1 ANDROID/ 2 IOS/ 3 PC/4 IPAD/ 5 WAP
	AD_PLAN  (  "1"  ,      "广告计划"),
	AD_GROUP  (     "2",      "广告组"),
	AD_ACCOUNT  (  "3"   ,      "广告账户"),
	CPN_ACCOUNT  (  "5"   ,      "公司账户");



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoHideIfConvertedEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoHideIfConvertedEnum item : ToutiaoHideIfConvertedEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (type == null){
			return null;
		}

		for (ToutiaoHideIfConvertedEnum item : ToutiaoHideIfConvertedEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (type == null){
			return false;
		}

		for (ToutiaoHideIfConvertedEnum item : ToutiaoHideIfConvertedEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
