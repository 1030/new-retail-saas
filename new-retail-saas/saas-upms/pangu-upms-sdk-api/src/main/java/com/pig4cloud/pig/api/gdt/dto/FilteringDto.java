package com.pig4cloud.pig.api.gdt.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Hma on 2020/12/3.
 * gdt请求对象
 */
@Setter
@Getter
public class FilteringDto {
	private String field;
	private String  operator;
	private Object[] values;
	public FilteringDto( ){

	}

	public FilteringDto(String field,String  operator, Object[] values ){
		this.field=field;
		this.operator=operator;
		this.values=values;

	}


}
