package com.pig4cloud.pig.api.gdt.dto;

import lombok.Data;

import java.util.List;

@Data
public class GdtResultPageInfo {
	/**
	 * 页号，从1开始
	 */
	private Integer page;
	/**
	 * 每页数据条数
	 */
	private Integer page_size;
	/**
	 * 总数据条数
	 */
	private Integer total_number;
	/**
	 * 总页数
	 */
	private Integer total_page;
}
