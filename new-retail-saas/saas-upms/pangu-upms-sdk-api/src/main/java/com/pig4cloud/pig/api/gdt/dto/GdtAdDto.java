package com.pig4cloud.pig.api.gdt.dto;

import com.pig4cloud.pig.api.gdt.entity.GdtAd;
import lombok.Getter;
import lombok.Setter;

/**
 * 广告 dto
 * 
 * @author hma
 * @date 2020-12-14
 */
@Setter
@Getter
public class GdtAdDto extends GdtAd
{
	private static final long serialVersionUID = 1L;
	/**
	 * 多个创意id用逗号分隔
	 */
     private String adcreativeIds;
	/**
	 * 多个广告id用逗号分隔
	 */
	 private String adIds;

	public GdtAdDto(){}

	public GdtAdDto(String accountId,Long  campaignId,Long adgroupId,Long adcreativeId,String adName,String configuredStatus,
					String impressionTrackingUrl,String clickTrackingUrl,String feedsInteractionEnabled){
		super(accountId,campaignId,adgroupId,adcreativeId,adName,configuredStatus,impressionTrackingUrl,clickTrackingUrl,feedsInteractionEnabled);
	}


}
