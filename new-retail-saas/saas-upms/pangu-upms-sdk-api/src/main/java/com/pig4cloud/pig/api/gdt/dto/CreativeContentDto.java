package com.pig4cloud.pig.api.gdt.dto;

import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Getter;
import lombok.Setter;


/**
 * @广告创意形式vo
 * @author hma
 *
 */
@Getter
@Setter
public class CreativeContentDto {
	/**
	 * 推广目标类型
	 */
	private String promotedObjectType;
	/**
	 * 广告版本
	 */
	private String siteSets;
	/**
	 * 创意形式id
	 */
	private Long adcreativeTemplateId;


	/**
	 * 	广告创意元素列表
	 */
	private String  adcreativeElements;
	/**
	 * 	创意内容
	 */
	private String  adcreativeName;

	private String pageType;
	private String pageSpec;

	public String getSiteSets(){
		return StringUtils.isNotBlank(siteSets)? siteSets = siteSets.replaceAll("&quot;","\""):siteSets;
	}

}
