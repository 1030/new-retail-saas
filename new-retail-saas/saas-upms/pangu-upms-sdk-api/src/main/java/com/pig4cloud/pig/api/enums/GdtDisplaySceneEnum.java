package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 广点通场景定向标签
 */
public enum GdtDisplaySceneEnum {
	PLACEMENT_TYPE_BANNER("PLACEMENT_TYPE_BANNER",
			"Banner"),
	PLACEMENT_TYPE_INLINE("PLACEMENT_TYPE_INLINE",
			"插屏"),
	PLACEMENT_TYPE_SPLASH("PLACEMENT_TYPE_SPLASH",
			"开屏"),
	PLACEMENT_TYPE_NATIVE("PLACEMENT_TYPE_NATIVE",
			"原生"),
	PLACEMENT_TYPE_REWARDED_VIDEO("PLACEMENT_TYPE_REWARDED_VIDEO",
			"激励视频");
	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	GdtDisplaySceneEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 *
	 * @return
	 */
	public static String typeByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (GdtDisplaySceneEnum item : GdtDisplaySceneEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 *
	 * @return
	 */
	public static String valueByType(String type) {
		if (StringUtils.isBlank(type)) {
			return null;
		}

		for (GdtDisplaySceneEnum item : GdtDisplaySceneEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type) {
		if (StringUtils.isBlank(type)) {
			return false;
		}

		for (GdtDisplaySceneEnum item : GdtDisplaySceneEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
