package com.pig4cloud.pig.api.feign;

import com.pig4cloud.pig.api.entity.AdLandingPage;
import com.pig4cloud.pig.api.feign.factory.RemoteLandingPageServiceFallbackFactory;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/**
 * @author cx
 * @version 1.0.0
 * @Time 2021/10/14 15:30
 */
@FeignClient(contextId = "remoteLandingPageService", value = ServiceNameConstants.UPMS_SDK_SERVICE, fallbackFactory = RemoteLandingPageServiceFallbackFactory.class)
public interface RemoteLandingPageService {

	/**
	 * 根据素材ID列表查询素材信息
	 *
	 * @param from
	 * @param landingPageIds
	 * @return
	 */
	@PostMapping(value = "/landingGroup/getLandingPageListByIds")
	R<List<AdLandingPage>> getLandingPageListByIds(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody List<String> landingPageIds);

}
