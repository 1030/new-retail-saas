package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoAPLandTypeEnum {

	//定向包类型，取值范围：EXTERNAL 落地页/ARTICLE 文章推广/GOODS 商品推广/DPA 商品目录/STORE 门店推广/AWEME 抖音号推广/SHOP 店铺直投/APP_ANDROID 应用下载-安卓/APP_IOS 应用下载-IOS

	EXTERNAL     (  "EXTERNAL"      ,                  "落地页"),
	ARTICLE      (  "ARTICLE"       ,                "文章推广"),
	GOODS        (  "GOODS"         ,              "商品推广"),
	DPA          (  "DPA"           ,            "商品目录"),
	STORE        (  "STORE"         ,              "门店推广"),
	AWEME        (  "AWEME"         ,              "抖音号推广"),
	SHOP         (  "SHOP"          ,             "店铺直投"),
	APP_ANDROID  (  "APP_ANDROID"   ,                    "应用下载-Android"),
	APP_IOS      (  "APP_IOS"       ,                "应用下载-IOS");



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoAPLandTypeEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoAPLandTypeEnum item : ToutiaoAPLandTypeEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoAPLandTypeEnum item : ToutiaoAPLandTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoAPLandTypeEnum item : ToutiaoAPLandTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
