package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/6/28 19:09
 * @description：
 * @modified By：
 */
@Data
public class FreeCrowdPackVo {
	// 父游戏id
	private Integer pgid;

	// 父游戏名称
	private String pgidName;

	//IMEI
	private String imei;

	//IDFA
	private String idfa;

	//OAID
	private String oaid;

	//手机号
	private String mobile;

	//手机号
	private String mac;

	//充值金额
	private BigDecimal payfee = BigDecimal.ZERO;

	//活跃天数
	private Integer activenum = 0;

	//最高连续活跃天数
	private Integer activemaxnum = 0;

}
