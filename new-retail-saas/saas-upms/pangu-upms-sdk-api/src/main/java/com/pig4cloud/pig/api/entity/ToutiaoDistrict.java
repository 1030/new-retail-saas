package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 头条 地域表
 * tt_district
 * @author kongyanfang
 * @date 2020-11-11 20:21:28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="tt_district")
public class ToutiaoDistrict extends Model<ToutiaoDistrict> {
	/**
	 * 主键
	 */
	private Integer id;

	/**
	 * 父id
	 */
	private Integer parent;

	/**
	 * 级别 1 省 2市 3区县
	 */
	private Integer level;

	/**
	 * 子项
	 */
	private String children;

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 是否省会
	 */
	private Integer proviceCapital;

	/**
	 * 发展规划，第几线城市
	 */
	private Integer cityLevel;


	public ToutiaoDistrict(Integer id, Integer parent, Integer level, String children, String name, Integer proviceCapital) {
		this.id = id;
		this.parent = parent;
		this.level = level;
		this.children = children;
		this.name = name;
		this.proviceCapital = proviceCapital;
	}
}