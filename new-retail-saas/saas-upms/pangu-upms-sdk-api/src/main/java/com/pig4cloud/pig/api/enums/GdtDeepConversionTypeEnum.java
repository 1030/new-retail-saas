package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 深度优化类型
 */
public enum GdtDeepConversionTypeEnum {
	DEEP_CONVERSION_WORTH("DEEP_CONVERSION_WORTH","优化 ROI"),
	DEEP_CONVERSION_BEHAVIOR("DEEP_CONVERSION_BEHAVIOR","优化转化行为");
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	GdtDeepConversionTypeEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 *
	 * @return
	 */
	public static String typeByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (GdtDeepConversionTypeEnum item : GdtDeepConversionTypeEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 *
	 * @return
	 */
	public static String valueByType(String type) {
		if (StringUtils.isBlank(type)) {
			return null;
		}

		for (GdtDeepConversionTypeEnum item : GdtDeepConversionTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type) {
		if (StringUtils.isBlank(type)) {
			return false;
		}

		for (GdtDeepConversionTypeEnum item : GdtDeepConversionTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
