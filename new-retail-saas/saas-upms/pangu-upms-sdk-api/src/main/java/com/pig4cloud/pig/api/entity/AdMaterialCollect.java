/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户收藏素材表
 *
 * @author cx
 * @date 2021-06-24 19:31:09
 */
@Data
@TableName("ad_material_collect")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "用户收藏素材表")
public class AdMaterialCollect extends Model<AdMaterialCollect> {
private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id",type= IdType.AUTO)
    @ApiModelProperty(value="主键")
    private Long id;
    /**
     * 用户ID
     */
    @ApiModelProperty(value="用户ID")
    private Integer userId;
    /**
     * 素材ID
     */
    @ApiModelProperty(value="素材ID")
    private Integer materialId;
    /**
     * 收藏时间
     */
    @ApiModelProperty(value="收藏时间")
    private Date createTime;
    }
