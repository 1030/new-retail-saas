package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 素材库
 * @date 2021-06-19 16:12:13
 */
@Data
public class UserByMaterialVo  implements Serializable {
	private static final long serialVersionUID = 52278585861899L;
    /**
     * 人物类型
     */
    private Integer characterType;
}
