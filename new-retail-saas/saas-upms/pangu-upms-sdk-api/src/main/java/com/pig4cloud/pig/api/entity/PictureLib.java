package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @description:
 * @author: nml
 * @time: 2020/10/29 17:52
 **/

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_picture_lib")
public class PictureLib extends Model<PictureLib> {

	private static final long serialVersionUID = 1L;

	/*
	*  唯一id
	* */
	@TableId(value="id",type= IdType.AUTO)
	@ApiModelProperty(value = "id")
	private Integer id;

	/**
	 * 图片名称：前端显示名称 可修改
	 */
	@NotBlank(message = "图片名称不能为空")
	@ApiModelProperty(value = "图片名称")
	@TableField(value = "picture_name")
	private String pictureName;

	/**
	 * 图片文件存储名称
	 */
	@ApiModelProperty(value = "图片文件存储名称")
	@TableField(value = "picture_filename")
	private String pictureFilename;

	@ApiModelProperty(value = "图片类型: 1:小图;2:横版大图;3:竖版大图;4:gif图;0:其他类型")
	@TableField(value = "picture_type")
	private Integer pictureType;

	@ApiModelProperty(value = "来源：1用户上传。2，视频封面")
	@TableField(value = "source_type")
	private Integer sourceType;

	@ApiModelProperty(value = "关联的视频ID")
	@TableField(value = "video_id")
	private Integer videoId;

	@ApiModelProperty(value = "是否视频封面：1：是，0：否")
	@TableField(value = "is_cover")
	private Integer isCover;

	@ApiModelProperty(value = "图片地址")
	@TableField(value = "picture_url")
	private String pictureUrl;

	@ApiModelProperty(value = "真实路径")
	@TableField(value = "real_path")
	private String realPath;

	@ApiModelProperty(value = "图片md5值")
	@TableField(value = "md5")
	private String md5;

	@ApiModelProperty(value = "图片宽")
	@TableField(value = "width")
	private String width;

	@ApiModelProperty(value = "图片高")
	@TableField(value = "height")
	private String height;

	@ApiModelProperty(value = "1横屏，2竖屏")
	@TableField(value = "screen_type")
	private Integer screenType;

	@ApiModelProperty(value = "图片大小")
	@TableField(value = "p_size")
	private String pSize;

	@ApiModelProperty(value = "图片格式")
	@TableField(value = "format")
	private String format;

	/**
	 * 图片尺寸
	 */
	@ApiModelProperty(value = "图片尺寸")
	@TableField(value = "picture_size")
	private String pictureSize;

	/**
	 * 图片是否删除：0:删除；1：未删除
	 */
	@ApiModelProperty(value = "图片状态")
	@TableField(value = "picture_status")
	private String pictureStatus;

	/**
	 * 标签id 可修改
	 */
	@ApiModelProperty(value = "标签id")
	@TableField(value = "label_id")
	private String labelId;

	@ApiModelProperty(value = "创建人")
	@TableField(value = "creator")
	private String creator;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "创建时间")
	//@TableField(value = "create_time",fill= FieldFill.INSERT)
	private Date createTime;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "修改时间")
	//该注解解决字段映射问题
	//@TableField(value = "update_time",fill= FieldFill.UPDATE)
	private Date updateTime;

	@ApiModelProperty(value = "类型: 1.icon 2.五图 3.loadiing 4.投放视频素材 5.投放平面素材")
	@TableField(value = "p_type")
	private Integer p_type;
}
