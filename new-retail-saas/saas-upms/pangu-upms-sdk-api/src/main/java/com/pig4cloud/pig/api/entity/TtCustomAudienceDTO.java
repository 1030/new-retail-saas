package com.pig4cloud.pig.api.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class TtCustomAudienceDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4709678072731116081L;

	private String customAudienceId;
	
	private String name;
	
	private String[] advertiserIds;
	
	private String source;
	
	private String sdate;
	
	private String edate;
	
	/**
	 * @状态 可用:CUSTOM_AUDIENCE_DELIVERY_STATUS_AVAILABLE  不可用：CUSTOM_AUDIENCE_DELIVERY_STATUS_UNAVAILABLE
	 */
	private String deliveryStatus;
	
	private Integer coverNumType;
}
