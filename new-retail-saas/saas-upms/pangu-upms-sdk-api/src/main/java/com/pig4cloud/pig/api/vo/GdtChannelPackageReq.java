package com.pig4cloud.pig.api.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class GdtChannelPackageReq {
	/**
	 * 广告账户
	 */
	private String adAccount;

	/**
	 * 广告包 APP id
	 */
	private String unionAppId;

	private List<Long> adIds;

	private List<String> appChlList;
}
