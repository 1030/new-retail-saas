package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author ：lile
 * @date ：2021/7/1 16:43
 * @description：
 * @modified By：
 */
@Data
public class FreeCrowdVo {
	// 父游戏id
	private Integer pgid;

	// 父游戏名称
	private String pgidName;

	//IMEI
	private String imei;

	//IDFA
	private String idfa;

	//OAID
	private String oaid;

	//手机号
	private String mobile;

	//手机号
	private String mac;

	//充值金额
	private BigDecimal payfee = BigDecimal.ZERO;

	//活跃天数
	private Integer activenum = 0;

	//最高连续活跃天数
	private Integer activemaxnum = 0;

	public Integer getPgid() {
		return pgid;
	}

	public String getPgidName() {
		if (StringUtils.isNotBlank(pgidName)) {
			return pgidName;
		} else {
			return "-";
		}
	}

	public String getImei() {
		if (StringUtils.isNotBlank(imei)) {
			return imei;
		} else {
			return "-";
		}
	}

	public String getIdfa() {
		if (StringUtils.isNotBlank(idfa)) {
			return idfa;
		} else {
			return "-";
		}
	}

	public String getOaid() {
		if (StringUtils.isNotBlank(oaid)) {
			return oaid;
		} else {
			return "-";
		}
	}

	public String getMobile() {
		if (StringUtils.isNotBlank(mobile)) {
			return mobile;
		} else {
			return "-";
		}
	}

	public String getMac() {
		if (StringUtils.isNotBlank(mac)) {
			return mac;
		} else {
			return "-";
		}
	}

	public BigDecimal getPayfee() {
		if (null == payfee) {
			return BigDecimal.ZERO;
		} else {
			return payfee;
		}
	}

	public Integer getActivenum() {
		return activenum;
	}

	public Integer getActivemaxnum() {
		return activemaxnum;
	}
}
