package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class AdAwemeBannedVo extends Page {

	/**
	 * 屏蔽用户ID
	 */
	@NotNull(message = "屏蔽用户ID不能为空", groups = {Delete.class})
	private Long id;

	/**
	 * 抖音号
	 */
	private String awemeId;

	/**
	 * 抖音号列表
	 */
	@Size(min = 1, max = 50, message = "抖音号数量不能小于1不能大于50", groups = {Add.class})
	private List<String> awemeIds;

	/**
	 * 抖音昵称
	 */
	private String awemeName;

	/**
	 * 屏蔽类型
	 */
	@NotBlank(message = "屏蔽类型不能为空", groups = {Add.class})
	private String bannedType;

	/**
	 * 屏蔽昵称关键词
	 */
	private String nicknameKeyword;

	/**
	 * 屏蔽昵称关键词列表
	 */
	@Size(min = 1, max = 50, message = "屏蔽昵称关键词数量不能小于1不能大于50", groups = {Add.class})
	private List<String> keywords;

	/**
	 * 广告账户ID
	 */
	@NotBlank(message = "广告账户ID不能为空", groups = {Add.class})
	private String advertiserId;

	/**
	 * 广告账户名称
	 */
	@NotBlank(message = "广告账户名称不能为空", groups = {Add.class})
	private String advertiserName;

	public interface Add {
	}

	public interface Edit {
	}

	public interface Delete {
	}

}
