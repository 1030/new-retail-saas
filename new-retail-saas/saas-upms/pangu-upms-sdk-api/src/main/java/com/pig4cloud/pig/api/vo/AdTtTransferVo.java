package com.pig4cloud.pig.api.vo;

import lombok.Data;

/**
 * @author ：lile
 * @date ：2021/7/15 15:18
 * @description：
 * @modified By：
 */
@Data
public class AdTtTransferVo {
	/**
	 * 代理商id
	 */
	private Integer agentId;

}
