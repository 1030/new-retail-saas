package com.pig4cloud.pig.api.entity.tag;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 标签关联表(ad_tag_relate)实体类
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ad_tag_relate")
public class AdTagRelate extends Model<AdTagRelate> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id",type = IdType.AUTO)
	private Long id;
    /**
     * 标签id
     */
    private Long tagId;
    /**
     * 标签关联id
     */
    private Long relateId;
    /**
     * 1.视频 2.图片 3.需求
     */
    private Integer type;
    /**
     * 是否删除  0否 1是
     */
    private Integer deleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(update = "now()")
	private Date updateTime;
    /**
     * 创建人
     */
    private Long createId;
    /**
     * 修改人
     */
    private Long updateId;

}