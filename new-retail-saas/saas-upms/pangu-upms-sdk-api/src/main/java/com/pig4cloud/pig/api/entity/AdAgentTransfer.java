package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author ：lile
 * @date ：2021/7/13 16:21
 * @description：
 * @modified By：
 */
@Data
@TableName("ad_agent_transfer")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "转账设置代理商id")
public class AdAgentTransfer extends Model<AdAgentTransfer> {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键")
	private Integer id;

	/**
	 * 代理商ID
	 */
	@ApiModelProperty(value = "代理商ID")
	private Integer agentId;

	/**
	 * 渠道类型
	 */
	@ApiModelProperty(value = "渠道类型")
	private Integer platform;

	/**
	 * 渠道类型名称
	 */
	@ApiModelProperty(value = "渠道类型名称")
	private String platformName;

	/**
	 * 广告账户代理的代理商ID
	 */
	@ApiModelProperty(value = "广告账户代理的代理商ID")
	private Long adagentId;

	/**
	 * 主渠道编码
	 */
	@ApiModelProperty(value = "主渠道编码")
	private String chncode;

	/**
	 * 主渠道编码名称
	 */
	@ApiModelProperty(value = "主渠道编码名称")
	private String chnname;

	/**
	 * 删除状态：0正常，1删除
	 */
	@ApiModelProperty(value = "删除状态：0正常，1删除")
	private Integer isDelete;
	/**
	 * 创建人
	 */
	@ApiModelProperty(value = "创建人")
	private String createUser;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**
	 * 修改人
	 */
	@ApiModelProperty(value = "修改人")
	private String updateUser;
	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "更新时间")
	private Date updateTime;
}
