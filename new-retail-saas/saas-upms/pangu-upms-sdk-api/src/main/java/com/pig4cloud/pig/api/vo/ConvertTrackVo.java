package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.entity.ConvertTrack;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ConvertTrackVo extends ConvertTrack {
	//status名称
	private String statusName;

	//convert_type 名称
	private String convertTypeName;

	// deep_external_action 名称
	private String deepExternalActionName;

	//ad_platform 名称
	private String adPlatformName;

	//sync_status 名称
	private String syncStatusName;

	private String adAccountName;





	public String getStatusName() {
		//1 AD_CONVERT_STATUS_ACTIVE（已激活） / 0 AD_CONVERT_STATUS_INACTIVE（未激活）
		if (this.getStatus() == null) {
			return null;
		}	
		switch (this.getStatus()){
			case 1:
				return "已激活";
			case 0:
				return "未激活";
			default:
				return "未知状态";
		}
	}

	public String getConvertTypeName() {
		//14 AD_CONVERT_TYPE_PAY / 8  AD_CONVERT_TYPE_ACTIVE / 13 AD_CONVERT_TYPE_ACTIVE_REGISTER / 30 关键行为
		if (this.getConvertType() == null) {
			return null;
		}
		switch (this.getConvertType()){
			case 14:
				return "支付";
			case 8 :
				return "激活";
			case 13:
				return "注册";
			case 25:
				return "关键行为";
			default:
				return "未知类型";
		}
	}

	public String getDeepExternalActionName() {
		//1 AD_CONVERT_TYPE_PAY / 2 AD_CONVERT_TYPE_NEXT_DAY_OPEN / 3 AD_CONVERT_TYPE_GAME_ADDICTION / 4 AD_CONVERT_TYPE_PURCHASE_ROI / 5 AD_CONVERT_TYPE_LT_ROI

		if (this.getDeepExternalAction() == null) {
			return "无";
		}
		switch (this.getDeepExternalAction()){
			case 1:
				return "付费";
			case 2:
				return "次留";
			case 3:
				return "关键行为";
			case 4:
				return "付费ROI";
			case 5:
				return "广告变现ROI";
			case 6:
				return "内广ROI";
			default:
				return "未知类型";
		}
	}

	public String getAdPlatformName() {
		//1头条 2UC

		if (this.getAdPlatform() == null) {
			return null;
		}
		switch (this.getAdPlatform()){
			case 1:
				return "头条";
			case 2:
				return "UC";
			default:
				return "未知平台类型";
		}
	}

	public String getSyncStatusName() {
		//0未同步/1同步成功/2同步失败

		if (this.getSyncStatus() == null) {
			return null;
		}
		switch (this.getSyncStatus()){
			case 0:
				return "未同步";
			case 1:
				return "同步成功";
			case 2:
				return "同步失败";
			default:
				return "未知同步状态";
		}
	}







}
