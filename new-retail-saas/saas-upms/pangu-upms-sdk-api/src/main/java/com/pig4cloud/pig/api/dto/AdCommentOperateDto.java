package com.pig4cloud.pig.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AdCommentOperateDto {
	private List<OperateComment> operateComments;
	private String text;
}
