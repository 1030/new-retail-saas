package com.pig4cloud.pig.api.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 统计对应数据
 */
@Data
public class TtStatistic {
	@ApiModelProperty(value = "消耗/总花费")
	private BigDecimal cost=new BigDecimal(0);

	@ApiModelProperty(value = "展示数")
	private Long show=0L;

	@ApiModelProperty(value = "展现数据-平均千次展现费用")
	private BigDecimal avgShowCost=new BigDecimal(0);

	@ApiModelProperty(value = "点击数")
	private Long click=0L;

	@ApiModelProperty(value = "点击率")
	private BigDecimal ctr=new BigDecimal(0);

	@ApiModelProperty(value = "平均点击单价")
	private BigDecimal avgClickCost=new BigDecimal(0);

	@ApiModelProperty(value = "转化数")
	private Long convert=0L;


	@ApiModelProperty(value = "转化成本")
	private BigDecimal convertCost=new BigDecimal(0);


	@ApiModelProperty(value = "转化率")
	private BigDecimal convertRate=new BigDecimal(0);

	@ApiModelProperty(value = "深度转化")
	private Integer deepConvert;

	@ApiModelProperty(value = "转化数据-深度转化成本")
	/** 转化数据-深度转化成本 */
	private BigDecimal deepConvertCost;
	@ApiModelProperty(value = " 转化数据-深度转化率")
	/** 转化数据-深度转化率 */
	private BigDecimal deepConvertRate;

}
