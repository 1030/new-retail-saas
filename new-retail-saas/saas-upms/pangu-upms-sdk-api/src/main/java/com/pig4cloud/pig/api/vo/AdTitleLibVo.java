package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

/**
 * ad_title_lib
 *
 * @author nml
 * @date 2020-11-09 10:37:32
 */
@Getter
@Setter
public class AdTitleLibVo extends Page<Object> {

	/*
	 *  批量查询动态词包ids
	 * */
	private String creativeWordIds;

	/*
	 *  批量删除图片pids
	 * */
	private String pids;

	/**
	 * 主键
	 */
	@NotEmpty(message = "ID不能为空", groups = {Delete.class})
	private String idArr;

	/**
	 *
	 */
	@NotNull(message = "ID不能为空", groups = {Update.class})
	private Long id;

	/**
	 * 标题名称
	 */
	private String titleName;

	/**
	 * 平台，0：通用；1：头条；7：UC；8：广点通；9：百度，...
	 */
	@NotNull(message = "平台ID不能为空", groups = {Create.class})
	private Integer platformId;

	/**
	 * 项目ID, 表示主游戏ID
	 */
	@NotNull(message = "主游戏ID不能为空", groups = {Create.class})
	private Integer projectId;

	/**
	 * 是否删除：0正常，1删除
	 */
	private Integer isdelete;

	/**
	 * 备注(创意词ID，多个ID使用','分隔)
	 */
	private String remark;

	/**
	 * 创建时间
	 */
	private Date createtime;

	/**
	 * 创建人
	 */
	private String createuser;

	/**
	 * 修改时间
	 */
	private Date updatetime;

	/**
	 * 修改人
	 */
	private String updateuser;

	/**
	 * 标题信息集合
	 */
	@NotEmpty(message = "标题信息不能为空", groups = {Create.class})
	private Collection<Title> titles;

	@Data
	public static class Title {
		private String titleName;
		private String remark;
	}

	public interface Create {
	}

	public interface Update {
	}

	public interface Delete {
	}

}