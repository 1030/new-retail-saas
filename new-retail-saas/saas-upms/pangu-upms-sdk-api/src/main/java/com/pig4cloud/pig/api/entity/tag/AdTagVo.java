package com.pig4cloud.pig.api.entity.tag;


import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AdTagVo implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * id
	 */
	private Long id;
	/**
	 * 标签名称
	 */
	private String tagName;
	/**
	 * 标签颜色
	 */
	private String tagColor;
	/**
	 * 是否删除  0否 1是
	 */
	private Integer deleted;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(update = "now()")
	private Date updateTime;
	/**
	 * 创建人
	 */
	private Long createId;
	/**
	 * 修改人
	 */
	private Long updateId;

	private Long videoNum;

	private Long imageNum;

}
