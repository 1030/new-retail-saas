package com.pig4cloud.pig.api.util;

import java.math.BigDecimal;

public class Constants {

	/**
	 * sys_config对应key
	 */
	public static final String SYS_CONFIG_CDK_CODE = "cdk";
	public static final String SYS_CONFIG_CDK_KEY = "cdkswitch";
	public static final String SYS_CONFIG_CDK_AMOUNT = "cdkamount";
	public static final String SYS_CONFIG_CDK_VOUCHER_KEY = "cdkvoucherswitch";//专属充值券开关
	public static final String SYS_CONFIG_CDK_3367_KEY = "cdk3367switch";//3367代金券卡包开关
	public static final String STRING_LEVEL = "级";
	/**
	 * QQ第三方登录state
	 */
	public static final String QQ_CONNECT_STATE = "qq_connect_state";

	/**
	 * 收入状态 1：上涨 2：下滑 3：持平
	 */
	public static final String NUMBER_FLUCATION_UP = "1";
	public static final String NUMBER_FLUCATION_OFF = "2";
	public static final String NUMBER_FLUCATION_EVEN = "3";
	public static final String STRING_FLUCATION_UP = "上涨";
	public static final String STRING_FLUCATION_OFF = "下滑";
	public static final String STRING_FLUCATION_EVEN = "持平";

	/**
	 * 支付状态 :1：未结算 2：已结算,等待支付 3: 已支付
	 */
	public static final String PAYSTATUS_UNBALANCED = "1";
	public static final String PAYSTATUS_NONPAYMENT = "2";
	public static final String PAYSTATUS_PAID = "3";
	public static final String STRING_PAYSTATUS_UNBALANCED = "未结算";
	public static final String STRING_PAYSTATUS_NONPAYMENT = "已结算,等待支付";
	public static final String STRING_PAYSTATUS_PAID = "已支付";

	/**
	 * 客户状态:1：未激活状态；2：正常状态；3：锁定状态
	 */
	public static final int CUSTOMER_STATUS_INACTIVE = 1;
	public static final int CUSTOMER_STATUS_NORMAL = 2;
	public static final int CUSTOMER_STATUS_LOCK = 3;

	public static final String STRING_ZERO_DECIMALS = "0.000";
	public static final String STRING_ZERO_NUMBER = "0";

	/**
	 * 最小结算金额
	 */
	public static final BigDecimal MIN_BALANCE_MONEY = new BigDecimal(100);

	/**
	 * 百分号
	 */
	public static final String STRING_PERCENT = "%";

	/**
	 * 金额格式化样式###,###.00(保留2位)
	 */
	public static final String STRING_FORMAT_MONEY_TWO_DECIMAL = "###,##0.00";

	/**
	 * 金额格式化样式###,###.000
	 */
	public static final String STRING_FORMAT_MONEY = "￥###,##0.000";

	/**
	 * 数字格式化样式###,###
	 */
	public static final String STRING_FORMAT_NUMBER = "###,###";
	/**
	 * 数字格式化样式###,###
	 */
	public static final String STRING_FORMAT_DECIMALS = "###,###.000";
	/**
	 * 英文逗号
	 */
	public static final String COMMA = ",";
	/**
	 * 英文分号
	 */
	public static final String SEMICOLON = ";";
	/**
	 * 英文冒号
	 */
	public static final String  CONLON = ":";

	/**
	 * 空字符串
	 */
	public static final String  EMPTTYSTR = "";
	/**
	 *
	 */
	public static final String POINT = ".";
	/**
	 * 中括号
	 */
	public static final String  MIDDLE_BRACKETS = "[]";
	/**
	 * 前中括号
	 */
	public static final String  PRE_MIDDLE_BRACKETS = "[";
	/**
	 * 后中括号
	 */
	public static final String  SUFFIX_MIDDLE_BRACKETS = "]";
	/**
	 * 大括号
	 */
	public static final String  BIG_BRACKETS = "{}";
	/**
	 * 前大括号
	 */
	public static final String  PRE_BIG_BRACKETS = "{";
	/**
	 * 后大括号
	 */
	public static final String  SUFFIX_BIG_BRACKETS = "}";
	/**
	 * 加号
	 */
	public static final String STRING_SIGN_PLUS = "+";

	/**
	 * 减号
	 */
	public static final String STRING_SIGN_MINUS = "-";
	/**
	 * 是
	 */
	public static final String YES = "是";
	/**
	 * 否
	 */
	public static final String NO = "否";
	/**
	 * 汉字  时
	 */
	public static final String ZH_HOUR = "时";

	/**
	 * 搜狗导航 key
	 */
	public static final String STRING_SOUGOUNAV = "sgnav";
	/**
	 * 搜狗导航 value
	 */
	public static final String CHAR_SOUGOUNAV = "1";
	/**
	 * 搜狗搜索 key
	 */
	public static final String STRING_SOUGOUSC = "sgsc";
	/**
	 * 搜狗搜索 value
	 */
	public static final String CHAR_SOUGOUSC = "2";
	/**
	 * hao123 key
	 */
	public static final String STRING_HAO123 = "hao123";
	/**
	 * hao123 value
	 */
	public static final String CHAR_HAO123 = "3";

	/**
	 * 全部 字符all
	 */
	public static final String STRING_ALL = "all";

	/**
	 * 状态 :0 可用 1 删除
	 */
	public static final int STATUS_USABLE = 0;

	/**
	 * 状态 :0 可用 1 删除
	 */
	public static final int STATUS_DELETED = 1;

	public static final String MESSAGE = "msg";

	public static final String RESULT_STATUS = "status";

	public static final String RESULT_STATUS_SUCCESS = "success";

	public static final String RESULT_STATUS_WARNING = "warning";

	public static final String RESULT_STATUS_ERROR = "error";

	public static final String RESULT_STATUS_LOGIN = "login";

	public static final String ERROR_MSG = "提交失败，请检查信息填写是否完整!";
	public static final String PARAM_ERROR_MSG = "参数传递错误!";
	public static final String SYS_ERROR_MSG = "系统繁忙!";
	public static final String SUCCESS_MSG_ADD = "新增成功!";
	public static final String SUCCESS_MSG_UPDATE = "更新成功!";
	public static final String SUCCESS_MSG_UPLOAD = "上传成功!";
	public static final String SUCCESS_MSG_DELETE = "删除成功!";
	public static final String ERROR_MSG_LOGIN = "请登录!";

	public final static String CONTENT_CHARSET_UTF8 = "utf-8";

	/**
	 * 操作员类型 1 后台管理员 2 用户 3 系统
	 */
	public static final String OPERATOR_TYPE_ADMIN = "1";
	public static final String OPERATOR_TYPE_CUSTOMER = "2";
	public static final String OPERATOR_TYPE_SYSTEM = "3";

	/**
	 * 操作类型 1新增 2 修改 3 删除  4登录
	 */
	public static final String OPERATOR_CREATE = "1";
	public static final String OPERATOR_UPDATE = "2";
	public static final String OPERATOR_DELETE = "3";
	public static final String OPERATOR_LOGIN = "4";

	/**
     * UTF-8编码
     */
    public static final String UTF8 = "UTF-8";

    /**
     * cookie中的JSESSIONID名称
     */
    public static final String JSESSION_COOKIE = "JSESSIONID";
    /**
     * url中的jsessionid名称
     */
    public static final String JSESSION_URL = "jsessionid";
    /**
     * HTTP POST请求
     */
    public static final String POST = "POST";
    /**
     * HTTP GET请求
     */
    public static final String GET = "GET";


    public static final String PAGE_NO = "pageNo";

    public static final String PAGE_SIZE = "pageSize";

	public final static String CONTEXT_CONFIG_LOCATION = "contextConfigLocation";
	public final static String SPRING_CONTEXT_NAME = "applicationContext";
	public final static String SERVICE_MAP_BEAN_NAME = "ServiceMap";
	public final static String SERVICE_RSP_MAP_BEAN_NAME = "ServiceRspMap";
	public final static String CONTENT_TYPE_XML = "text/html";
	/**
	 * 主要用来ajax返回状态
	 */
	public static final String STATUS = "success";
	/**
	 * ajax返回警告状态
	 */
	public static final String WARN = "warn";
	/**
	 * ajax返回成功状态
	 */
	public static final boolean SUCCESS = true;
	/**
	 * ajax返回错误状态
	 */
	public static final boolean ERROR = false;

	/**
	 * ajax返回数据参数名
	 */
	public static final String DATA = "data";

	/**
	 * 重定向参数名
	 */
	public static final String REDIRECT_URL = "rurl";


	public final static String FRONT_LOGIN_USER_BEAN_SESSION_KEY = "frontLoginUserBean";//前台登陆session

	/**
     * 前端缓存的一个bean 对象；
     */
    public static final String FRONT_CACHE_BEAN = "front_bean_";

	/**
	 * 用户最近玩过的游戏 前缀 memcache
	 */
	public static final String USER_PLAY_GAMES_ = "user_play_games_";

	/**
	 * 用户本月签到 前缀 memcache
	 */
	public static final String USER_SIGN_INFOS_ = "user_sign_infos_";

	/**
	 * 用户站内信未读条数 前缀 memcache
	 */
	public static final String USER_MSG_COUNT_ = "user_msg_count_";

	/**
	 * 用户信息 前缀 redis
	 */
	public static final String USER_KEY = "uk_s_";

	/**
	 * 用户活动充值10元 前缀 redis
	 * key的全文为 u_act_rcg_10_YYYYMMDD_uid 例如：u_act_rcg_10_20170424_192
	 */
	public static final String USER_ACT_RECHARGE_TEN_ = "u_act_rcg_10_";

	/**
	 * 用户活动每日充值次数, actid 是对应活动id
	 * key的全文为 u_act_recharge_actid_YYYYMMDD_uid 例如： u_act_recharge_25_20170424_192
	 */
	public static final String USER_ACT_RECHARGE_ACID_ = "u_act_recharge_";

	/**
	 * 第三方登录类型
	 */
	public final static String THIRD_PARTY_TYPE = "thirdPartyType";
	/**
	 * 第三方登录QQ
	 */
	public final static String THIRD_PARTY_QQ_OPENID = "thirdPartyQqOpenId";
	/**
	 * 第三方登录微信
	 */
	public final static String THIRD_PARTY_WEIXIN_OPENID = "thirdPartyWeixinOpenId";

	/**
	 * 首充redisKEY：f_c_cdk_+用户名
	 */
	public final static String FIRST_RECHARGE_CDK_ = "f_c_cdk_";

	/**
	 * 活动redisKEY：a_c_cdk_num_YYYYMMDD_用户名  例如a_d_cdk_num_20170809_donkey
	 * 存放的值为 该用户当日兑换次数
	 */
	public final static String ACT_DAILY_RECHARGE_CDK_NUM_ = "a_d_cdk_num_";

	/**
	 * 活动redisKEY：a_c_cdk_money_YYYYMMDD_用户名  例如a_d_cdk_money_20170809_donkey
	 * 存放的值为 该用户当日兑换总金额
	 */
	public final static String ACT_DAILY_RECHARGE_CDK_MONEY_ = "a_d_cdk_money_";

	/**
	 * 活动redisKEY：a_t_cdk_num_用户名  例如a_t_cdk_num_donkey
	 * 存放的值为 该用户总共兑换次数
	 */
	public final static String ACT_TOTAL_RECHARGE_CDK_NUM_ = "a_t_cdk_num_";

	/**
	 * 活动redisKEY：a_t_cdk_money_用户名  例如a_t_cdk_money_donkey
	 * 存放的值为 该用户总共兑换总金额
	 */
	public final static String ACT_TOTAL_RECHARGE_CDK_MONEY_ = "a_t_cdk_money_";

	public final static String REDIS_KEY_RECHARGE_TMP_API_ = "3367v2_api_recharge_tmp_";  // 临时订单
	public final static String REDIS_KEY_RECHARGE_ = "3367v2_recharge_";  // 正式订单
	public final static String REDIS_KEY_RECHARGE_ORDERNO_LIST = "3367v2_recharge_orderno_list";  // 订单号list

	public final static String SUPERLY_ORDER_KEY_ = "superly_order_key_";  // 补单-订单号


	public static final String GAME_HEALTHY_SYSID = "3367platform";

	public static final String OCEANENGINE_REDIS_TOKEN_KEY_PRIX_ = "oceanengine_redis_token_key_prix_";
	// 快手账户token  redis   key
	public static final String REDIS_KS_TOKEN_KEY_PRIX = "redis_ks_token_key_prix_";
	public static final String OCEANENGINE_REDIS_ADVER_TOKEN_KEY_PRIX_ = "oceanengine_redis_adver_token_key_prix_";

	public static final String AD_CAMPAIGN_REDIS_TOKEN_KEY_PRIX_ = "ad_campaign_key_prix_";

	//通用设置账号名称
	public static final String AD_REDIS_AD_NAME_KEY_PRIX_ = "ad_redis_ad_name_key_prix_";


	//头条人群包自身创建的数据源列表
	public static final String TT_REDIS_CUSTOMAUDIENCE_DATASOURCE_LIST_REDIS_KEY = "tt_redis_customaudience_datasource_list_redis_key";

	//头条广告计划超出预期标识
	public static final String TT_REDIS_ADID_COST_WARNING_KEY_PRIX_ = "tt_redis_adid_cost_warning_key_prix_";

	//头条转化跟踪redis加锁前缀
	public static final String TT_REDIS_LOCK_CONVERTID_KEY_PRIX_ = "tt_redis_lock_convertid_key_prix_";


	////////////////////////////////广点通////////////////////////////


	//广点通管家账户获取token
	public static final String GDT_REDIS_TOKEN_KEY_PRIX_ = "gdt_redis_token_key_prix_";

	//广点通定向标签
	public static final String GDT_TARGETING_TAGS_KEY_PRIX_ = "gdt_targeting_tags_key_prix_";

	//广点通广告组超出预期标识
	public static final String GDT_REDIS_ADGROUP_COST_WARNING_KEY_PRIX_ = "gdt_redis_adgroup_cost_warning_key_prix_";



	//字典表key
	public static final String DICT_KEY_PRIX_ = "dict_key_prix_";
	
	/**
	 * 素材推送中缓存
	 */
	public static final String AD_MATERIAL_PROGRESS_PUSH_PRIX = "ad_material_progress_push_";

	public enum ADMATERIAL_STATUS{
		//启用
		ENABLE(0),
		//禁用
		DISABLE(1);
		private int status;
		private ADMATERIAL_STATUS(int v){
			this.status=v;
		}
		public int V(){
			return this.status;
		}
	}
	public enum 	ADMATERIAL_DEL{
		//正常未删除
		NODEL(0),
		//删除
		DEL(1);
		private int status;
		private ADMATERIAL_DEL(int v){
			this.status=v;
		}
		public int V(){
			return this.status;
		}
	}

	// 动心上报kafka配置
	public static final String DONGXIN_DATA = "data";
	public static final String DATA_TYPE_DONGXIN_EVENTNAME = "event_name";
	public static final String DATA_TYPE_DONGXIN_EVENTTIME = "event_time";
	public static final String PANGU_GDT_AD_GROUP_DAY_REPORT = "pangu_oe_creative_day_report";
	
	/**
	 * 广告推送状态
	 * 推送状态 0.未推送 1.推送中 2.推送完成 3.推送失败
	 */
	public static final int PREPARE_PUSH = 0;
	public static final int PROGRESS_PUSH = 1;
	public static final int FINISH_PUSH = 2;
	public static final int FAIL_HPUSH = 3;
}
