package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.util.Collection;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.api.vo
 * @Author 马嘉祺
 * @Date 2021/9/6 17:22
 * @Description
 */
@Data
public class TtAdReportVO {

	private Collection<ReportAdvertiser> advertisers;

	private String start_date;

	private String end_date;

	private Integer days;

	private Integer inventory;

	@Data
	public static class ReportAdvertiser {

		private String advertiserId;

		private String accessToken;

		public ReportAdvertiser() {
		}

		public ReportAdvertiser(String advertiserId, String accessToken) {
			this.advertiserId = advertiserId;
			this.accessToken = accessToken;
		}

	}

}
