package com.pig4cloud.pig.api.gdt.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by hma on 2020/12/28.
 * 查询文案助手dto
 */
@Setter
@Getter
public class CreativetoolsDto {
	/**
	 * 账号id
	 */
	private Long accountId;
	/**
	 * 文案最长字数限制
	 */
	private Integer maxTextLength;
	/**
	 * 	一级广告行业，广点通的一级行业分类 id
	 */
	private Long categoryFirstLevel;
	/**
	 * 二级广告行业，广点通的二级行业分类 id
	 */
	private Long categorySecondLevel;
	/**
	 * 关键字，有多个关键词用半角逗号','分割，如：零花钱,金额:长度最小 1 字节，长度最大 255 字节
	 */
	private String keyword;

}
