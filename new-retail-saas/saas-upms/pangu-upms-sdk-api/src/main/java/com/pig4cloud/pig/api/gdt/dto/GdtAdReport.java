package com.pig4cloud.pig.api.gdt.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * @description: 广点通广告创意列表及报表数据
 * @author: nml
 * @time: 2020/12/14 17:46
 **/

@Setter
@Getter
public class GdtAdReport implements Serializable {

	/*推广计划id*/
	private Long campaignId;

	/*广告组 id*/
	private Long adgroupId;

	/** 广告组名称 */
	private String adgroupName;

	/*广告id*/
	private Long adId;

	/** 广告名称 */
	private String adName;

	/*adcreative_id*/
	private Long adcreativeId;

	/** 广告创意 */
	private String adcreative; //2 广告表 用来拿到imageUrl

	/*创意缩略图，支持预览*/
	private String imageUrl;

	/** 创意元素 */
	private String adcreativeElements; //1 广告创意表 中用来拿到imageUrl

	/** 创意形式 id */
	private Long adcreativeTemplateId;//拿到创意名称

	/** 创意形式 名称 */
	private String adcreativeTemplateName;


	/*曝光量*/
	private String viewCount ;

	/*点击量*/
	private String validClickCount  ;

	/*点击率*/
	private String ctr  ;

	/*点击均价*/
	private String cpc  ;

	/*消耗*/
	private String cost  ;

	/*开关*/
	private String onOff;

	public String getCost() {
		int a = Integer.parseInt(this.cost);
		DecimalFormat df=new DecimalFormat("0.00");
		this.cost = df.format((float)a/(float)100);
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	//计算点击率
	public String getCtr() {
		//曝光量为0
		if ("0".equals(this.viewCount)){
			return ctr;
		}else {//曝光量不为0
			int a = Integer.parseInt(this.validClickCount);
			int b = Integer.parseInt(this.viewCount);
			NumberFormat nf = NumberFormat.getPercentInstance();
			nf.setMinimumFractionDigits( 2 );
			this.ctr = nf.format((float) a / (float) b);
			return ctr;

		}
	}

	public void setCtr(String ctr) {
		this.ctr = ctr;
	}

	//计算点击均价
	public String getCpc() {
		//点击量为0
		if ("0".equals(this.validClickCount)){
			return cpc;
		}else {//点击量不为0
			int a = Integer.parseInt(this.cost);
			int b = Integer.parseInt(this.validClickCount);
			DecimalFormat df=new DecimalFormat("0.00");
			this.cpc = df.format((float)a/((float)b*100));
			return cpc;


		}
	}

	public void setCpc(String cpc) {
		this.cpc = cpc;
	}

}
