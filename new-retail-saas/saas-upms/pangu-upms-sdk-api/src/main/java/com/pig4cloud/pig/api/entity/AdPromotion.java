package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 广告表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:21:02
 * table: ad_promotion
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_promotion")
public class AdPromotion extends Model<AdPromotion>{
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 广告ID
	 */
	@TableField(value = "promotion_id")
	private Long promotionId;
	/**
	 * 广告名称
	 */
	@TableField(value = "promotion_name")
	private String promotionName;
	/**
	 * 项目ID
	 */
	@TableField(value = "project_id")
	private Long projectId;
	/**
	 * 广告账户ID
	 */
	@TableField(value = "advertiser_id")
	private Long advertiserId;
	/**
	 * 广告创建时间，格式yyyy-mm-dd，表示过滤出当天创建的广告项目
	 */
	@TableField(value = "promotion_create_time")
	private String promotionCreateTime;
	/**
	 * 广告更新时间，格式yyyy-mm-dd，表示过滤出当天更新的广告项目
	 */
	@TableField(value = "promotion_modify_time")
	private String promotionModifyTime;
	/**
	 * 广告状态
	 */
	@TableField(value = "status")
	private String status;
	/**
	 * 操作状态
	 */
	@TableField(value = "opt_status")
	private String optStatus;
	/**
	 * 广告素材组合
	 */
	@TableField(value = "promotion_materials")
	private String promotionMaterials;
	/**
	 * 预算
	 */
	@TableField(value = "budget")
	private BigDecimal budget;
	/**
	 * 目标转化出价/预期成本
	 */
	@TableField(value = "cpa_bid")
	private BigDecimal cpaBid;
	/**
	 * 深度优化出价
	 */
	@TableField(value = "deep_cpabid")
	private BigDecimal deepCpabid;
	/**
	 * 深度转化ROI系数
	 */
	@TableField(value = "roi_goal")
	private BigDecimal roiGoal;
	/**
	 * 素材评级信息
	 */
	@TableField(value = "material_score_info")
	private String materialScoreInfo;
	/**
	 * 来源(1:API,2:渠道后台)
	 */
	@TableField(value = "soucre_status")
	private Integer soucreStatus;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;


	public AdPromotion(){

	}

	public AdPromotion(Long promotionId,String optStatus){
		this.promotionId=promotionId;
		this.optStatus=optStatus;

	}

	public AdPromotion(Long promotionId,BigDecimal budget,BigDecimal cpaBid){
		this.promotionId=promotionId;
		this.budget=budget;
		this.cpaBid=cpaBid;

	}

}






