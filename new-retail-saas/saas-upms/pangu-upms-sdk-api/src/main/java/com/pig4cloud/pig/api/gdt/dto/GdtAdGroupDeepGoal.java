package com.pig4cloud.pig.api.gdt.dto;

import lombok.Data;

@Data
public class GdtAdGroupDeepGoal {

    private String goal;

}
