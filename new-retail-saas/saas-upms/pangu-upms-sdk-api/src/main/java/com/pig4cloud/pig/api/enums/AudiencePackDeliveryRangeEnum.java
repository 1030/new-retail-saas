package com.pig4cloud.pig.api.enums;


import org.apache.commons.lang3.StringUtils;

public enum AudiencePackDeliveryRangeEnum {

	//广告投放范围  DEFAULT 默认/UNION 只投放到资讯联盟（穿山甲）/UNIVERSAL 通投智选

	DEFAULT    (  "DEFAULT"     ,      "默认"),
	UNION      (  "UNION"       ,      "穿山甲"),
	UNIVERSAL  (  "UNIVERSAL"   ,      "通投智选");


	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	AudiencePackDeliveryRangeEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (AudiencePackDeliveryRangeEnum item : AudiencePackDeliveryRangeEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (AudiencePackDeliveryRangeEnum item : AudiencePackDeliveryRangeEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (AudiencePackDeliveryRangeEnum item : AudiencePackDeliveryRangeEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
