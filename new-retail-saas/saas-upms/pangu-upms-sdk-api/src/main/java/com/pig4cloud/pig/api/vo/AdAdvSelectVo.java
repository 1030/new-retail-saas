package com.pig4cloud.pig.api.vo;

import java.io.Serializable;
import java.util.TreeSet;

import lombok.Data;


@Data
public class AdAdvSelectVo  implements Serializable, Comparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3309147144008041159L;
	
	
	private String advertiserId;
	
	private String name;
	
	private TreeSet<AdAdvSelectVo> childrenList = new TreeSet<>();
	
	
	@Override
	public int compareTo(Object o) {
	    if (!(o instanceof AdAdvSelectVo)) {	        
	        throw new RuntimeException("不是AdAdvSelectVo对象");
	    }
	    AdAdvSelectVo p = (AdAdvSelectVo) o;
	    
	    return this.advertiserId.compareTo(p.advertiserId);
	}

}
