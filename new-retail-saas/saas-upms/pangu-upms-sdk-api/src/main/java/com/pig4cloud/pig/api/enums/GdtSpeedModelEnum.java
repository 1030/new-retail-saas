package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 广点通投放类型
 */
public enum GdtSpeedModelEnum {
	SPEED_MODE_STANDARD("SPEED_MODE_STANDARD", "标准投放"),
	SPEED_MODE_FAST("SPEED_MODE_FAST", "加速投放");
	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	GdtSpeedModelEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 *
	 * @return
	 */
	public static String typeByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (GdtSpeedModelEnum item : GdtSpeedModelEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 *
	 * @return
	 */
	public static String valueByType(String type) {
		if (StringUtils.isBlank(type)) {
			return null;
		}

		for (GdtSpeedModelEnum item : GdtSpeedModelEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type) {
		if (StringUtils.isBlank(type)) {
			return false;
		}

		for (GdtSpeedModelEnum item : GdtSpeedModelEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
