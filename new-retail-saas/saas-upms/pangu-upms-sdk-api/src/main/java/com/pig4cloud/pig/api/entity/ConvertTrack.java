/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


/**
 * @广告账户
 * @author john
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="convert_track")
public class ConvertTrack extends Model<ConvertTrack> {

	private static final long serialVersionUID = 1L;

	//@TableField(strategy = FieldStrategy.NOT_NULL)		//, insertStrategy = FieldStrategy.NOT_NULL
	//@ApiModelProperty(value = "自增的 转化ID")
	@TableId(value="id",type= IdType.AUTO)
	private Integer id;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "广告平台 1头条 2UC")
	private Short adPlatform;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "同步状态 0未同步/1同步成功/2同步失败/5已使用")
	private Short syncStatus;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "转化状态 （已激活）         1 AD_CONVERT_STATUS_ACTIVE（激活） / 0 AD_CONVERT_STATUS_INACTIVE（未激活）")
	private Short status;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "转化跟踪在 第三方平台中的ID (头条/UC 的转化id)")
	private String idAdPlatform;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "转化名称")
	private String name;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "新建广告时，gameid（子游戏id）")
	private String appId;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "新建广告时，游戏名")
	private String appName;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "子游戏ID")
	private Long gameId;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "新建广告时，游戏包名")
	private String packageName;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "新建广告时，下载链接")
	private String downloadUrl;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "广告账户（新建广告时 广告账户id）")
	private String adAccount;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "转化类型（转化目标）         14 AD_CONVERT_TYPE_PAY / 8  AD_CONVERT_TYPE_ACTIVE / 13 AD_CONVERT_TYPE_ACTIVE_REGISTER / 25 AD_CONVERT_TYPE_GAME_ADDICTION (关键行为)  ")
	private Short convertType;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "深度转化目标 1 AD_CONVERT_TYPE_PAY / 2 AD_CONVERT_TYPE_NEXT_DAY_OPEN / 3 AD_CONVERT_TYPE_GAME_ADDICTION / 4 AD_CONVERT_TYPE_PURCHASE_ROI / 5 AD_CONVERT_TYPE_LT_ROI")
	private Short deepExternalAction;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "转化来源类型    7 AD_CONVERT_SOURCE_TYPE_SDK（应用下载SDK）")
	private Short convertSourceType;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "转化统计方式：ONLY_ONE（仅一次），EVERY_ONE（每一次）")
	private String convertDataType;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "1 APP_ANDROID/ 2 APP_IOS;当传app_id")
	private Short appType;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "点击监测链接")
	private String actionTrackUrl;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "展示监测链接")
	private String displayTrackUrl;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "有效播放监测链接")
	private String videoPlayEffectiveTrackUrl;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "视频播放监测链接")
	private String videoPlayTrackUrl;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "视频播完监测链接")
	private String videoPlayDoneTrackUrl;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "转化次数")
	private Integer convertCount;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "广告监测主键ID")
	private Integer monitorId;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "分包渠道编码")
	private String appChl;


	/**
	 * 创建时间
	 */
	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "创建时间")
	private Date createtime;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "")
	private Short platformId;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "")
	private Integer planCount;



	public String getappTypeEName(){
		switch (this.appType){
			case 1:
				return "APP_ANDROID";
			case 2:
				return "APP_IOS";
			default:
				return "UNKNOWN";
		}
	}

	public String getConvertTypeEName(){
		//14 AD_CONVERT_TYPE_PAY / 8  AD_CONVERT_TYPE_ACTIVE / 13 AD_CONVERT_TYPE_ACTIVE_REGISTER / 25 AD_CONVERT_TYPE_GAME_ADDICTION (关键行为)
		switch (this.convertType){
			case 14:
				return "AD_CONVERT_TYPE_PAY";
			case 8:
				return "AD_CONVERT_TYPE_ACTIVE";
			case 13:
				return "AD_CONVERT_TYPE_ACTIVE_REGISTER";
			case 25:
				return "AD_CONVERT_TYPE_GAME_ADDICTION";
			default:
				return "UNKNOWN";
		}
	}
	public String getDeepExternalActionEName(){
		//1 AD_CONVERT_TYPE_PAY / 2 AD_CONVERT_TYPE_NEXT_DAY_OPEN / 3 AD_CONVERT_TYPE_GAME_ADDICTION / 4 AD_CONVERT_TYPE_PURCHASE_ROI / 5 AD_CONVERT_TYPE_LT_ROI',
		//6 AD_CONVERT_TYPE_UG_ROI
		if (null == this.deepExternalAction){
			return null;
		}
		switch (this.deepExternalAction){
			case 1:
				return "AD_CONVERT_TYPE_PAY";
			case 2:
				return "AD_CONVERT_TYPE_NEXT_DAY_OPEN";
			case 3:
				return "AD_CONVERT_TYPE_GAME_ADDICTION";
			case 4:
				return "AD_CONVERT_TYPE_PURCHASE_ROI";
			case 5:
				return "AD_CONVERT_TYPE_LT_ROI";
			case 6:
				return "AD_CONVERT_TYPE_UG_ROI";
			default:
				return "UNKNOWN";
		}
	}

	public String getConvertSourceTypeEName(){
		//7 AD_CONVERT_SOURCE_TYPE_SDK（应用下载SDK）
		switch (this.convertSourceType){
			case 7:
				return "AD_CONVERT_SOURCE_TYPE_SDK";
			default:
				return "UNKNOWN";
		}
	}

}
