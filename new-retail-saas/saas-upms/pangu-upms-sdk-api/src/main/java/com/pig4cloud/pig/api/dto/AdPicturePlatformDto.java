package com.pig4cloud.pig.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 *
 * @author nml
 * @date 2020-11-04 11:06:33
 */
@Getter
@Setter
public class AdPicturePlatformDto {


	/**
	 * 图片id
	 */
	private Long pictureId;

	/**
	 * tt平台id
	 */
	private Long ttId;

	/**
	 * uc平台id
	 */
	private Long ucId;

	/**
	 * gdt平台id
	 */
	private Long gdtId;

}