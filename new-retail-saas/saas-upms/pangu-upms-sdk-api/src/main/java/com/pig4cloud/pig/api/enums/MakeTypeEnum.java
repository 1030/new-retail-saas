package com.pig4cloud.pig.api.enums;

/**
 * 素材制作类型
 * 1：原创，2：原创衍生，3：抄本，4：抄本衍生
 * @author cx
 * @date 2020-5-7 20:00:47
 */
public enum MakeTypeEnum {

	MAKE_TYPE1(1, "原创"),
	MAKE_TYPE2(2, "原创衍生"),
	MAKE_TYPE3(3, "抄本"),
	MAKE_TYPE4(4, "抄本衍生");

	private MakeTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}
	public static String getName(Integer type) {
		for (MakeTypeEnum ele : values()) {
			if(ele.getType().equals(type)) {
				return ele.getName();
			}
		}
		return "未知";
	}

	private Integer type;

	private String name;

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
