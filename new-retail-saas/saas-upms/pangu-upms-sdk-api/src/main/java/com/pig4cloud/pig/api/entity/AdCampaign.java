package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 投放平台-广告组表 ad_campaign
 * 
 * @author hma
 * @date 2020-11-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_campaign")
public class AdCampaign extends Model<AdCampaign> {
	private static final long serialVersionUID = 1L;
	
	/**  */
	@JSONField(name = "campaign_id")
	@ApiModelProperty
	@TableField(value = "campaign_id")
	private Long campaignId;

	/** 广告组名称 */
	@JSONField(name = "name")
	@ApiModelProperty
	@TableField(value = "campaign_name")
	private String campaignName;

	/** 广告账号主键id */
	@JSONField(name = "advertiser_id")
	@ApiModelProperty
	@TableField(value = "advertiser_id")
	private Long advertiserId;

	/** 业务类型（企业类型）：001 动游科技 */
	@ApiModelProperty
	@TableField(value = "business_type")
	private String businessType;

	/**  日元算类型：BUDGET_MODE_DAY（日预算）
BUDGET_MODE_INFINITE（不限） */
	@JSONField(name = "budget_mode")
	@ApiModelProperty
	@TableField(value = "budget_mode")
	private String budgetMode;

	/** 组预算 */
	@JSONField(name = "budget")
	@ApiModelProperty
	@TableField(value = "budget")
	private BigDecimal campaignBudget;

	@JSONField(name = "status")
	@ApiModelProperty
	@TableField(value = "operation")
	private String  status;

	/** 推广目标：1 应用推广  2 销售线推广 */
	@JSONField(name = "landing_type")
	@ApiModelProperty
	@TableField(value = "landing_type")
	private String adTarget;

	/** 投放类型：1 头条  2 广点通 */
	@ApiModelProperty
	@TableField(value = "ad_type")
	private Integer adType;

	/**  */
	@JSONField(name = "campaign_create_time")
	@ApiModelProperty
	@TableField(value = "campaign_create_time")
	private String createTime;

	/**  */
	@JSONField(name = "campaign_modify_time")
	@ApiModelProperty
	@TableField(value = "campaign_modify_time")
	private String updateTime;

	/**  */
	@JSONField(name = "user_id")
	@ApiModelProperty
	@TableField(value = "user_id")
	private Long userId;

	/**
	 * 广告组类型:FEED信息流,SEARCH搜索广告
	 */
	@JSONField(name = "campaign_type")
	@ApiModelProperty
	@TableField(value = "campaign_type")
	private String campaignType;

	/**
	 * 营销目的: CONVERSION 行动转化,INTENTION用户意向,ACKNOWLEDGE品牌认知 UNLIMITED 非营销链路
	 */
	@JSONField(name = "marketing_purpose")
	@ApiModelProperty
	@TableField(value = "marketing_purpose")
	private String marketingPurpose;

	/**
	 * 操作状态
	 */
	@JSONField(name = "opt_status")
	private String optStatus;

	public AdCampaign(){}

	public AdCampaign(Long campaignId,String status){
		this.campaignId=campaignId;
		this.status=status;
	}

	public AdCampaign(Long campaignId,String budgetMode,BigDecimal campaignBudget,String  updateTime){
		this.campaignId=campaignId;
		this.budgetMode=budgetMode;
		this.campaignBudget=campaignBudget;
		this.updateTime=updateTime;
	}

}
