package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

/**
 * 广告创意表 ad_creative
 * 
 * @author hma
 * @date 2020-11-10
 */
@Setter
@Getter
public class AdCreative
{
	private static final long serialVersionUID = 1L;
	

	/** 创意ID */
	@JSONField(name = "creative_id")
	@TableId("creative_id")
	private Long creativeId;
	/** 广告计划ID */
	@JSONField(name = "ad_id")
	private Long adId;
	/** 广告主ID */
	@JSONField(name = "advertiser_id")
	private Long advertiserId;
	/** 创意状态： CREATIVE_STATUS_DELIVERY_OK	投放中	CREATIVE_STATUS_NOT_START	未到达投放时间	CREATIVE_STATUS_NO_SCHEDULE	不在投放时段	CREATIVE_STATUS_DISABLE	创意暂停	CREATIVE_STATUS_CAMPAIGN_DISABLE	已被广告组暂停	CREATIVE_STATUS_CAMPAIGN_EXCEED	广告组超出预算	CREATIVE_STATUS_AUDIT	新建审核中	CREATIVE_STATUS_REAUDIT	修改审核中	CREATIVE_STATUS_DELETE	已删除	CREATIVE_STATUS_DONE	已完成（投放达到结束时间）	CREATIVE_STATUS_AD_DISABLE	广告计划暂停	CREATIVE_STATUS_AUDIT_DENY	审核不通过	CREATIVE_STATUS_BALANCE_EXCEED	账户余额不足	CREATIVE_STATUS_BUDGET_EXCEED	超出预算	CREATIVE_STATUS_DATA_ERROR	数据错误（数据错误时返回，极少出现）	CREATIVE_STATUS_PRE_ONLINE	预上线	CREATIVE_STATUS_AD_AUDIT	广告计划新建审核中	CREATIVE_STATUS_AD_REAUDIT	广告计划修改审核中	CREATIVE_STATUS_AD_AUDIT_DENY	广告计划审核不通过	CREATIVE_STATUS_ALL	所有包含已删除	CREATIVE_STATUS_NOT_DELETE	所有不包含已删除（状态过滤默认值）	CREATIVE_STATUS_ADVERTISER_BUDGET_EXCEED	超出账户日预算 */
	private String status;
	/** 创意素材类型（素材样式） */
	@JSONField(name = "image_mode")
	private String imageMode;
	/** 图片素材，图片ID列表 */
	@JSONField(name = "image_ids")
	private String imageIds="";
	/** 视频素材，封面图片ID */
	@JSONField(name = "image_id")
	private String imageId;
	/** 视频素材，视频ID） */
	@JSONField(name = "video_id")
	private String videoId;
	/** 广告位置 */
	@JSONField(name = "inventory_type")
	private String inventoryType;
	/** 创意方式：1 程序化创意  2 自定义创意 */
	@JSONField(name = "creative_type")
	private Integer creativeType;
	/** 创建时间 */
	@JSONField(name = "creative_create_time")
	private String creativeCreateTime;
	/** 修改时间 */
	@JSONField(name = "creative_modify_time")
	private String creativeModifyTime;

	private String  title;

	@JSONField(name = "opt_status")
	private String optStatus;

	public AdCreative(){}

	public AdCreative(Long creativeId, String optStatus){
		this.creativeId=creativeId;
		this.optStatus=optStatus;
	}




}
