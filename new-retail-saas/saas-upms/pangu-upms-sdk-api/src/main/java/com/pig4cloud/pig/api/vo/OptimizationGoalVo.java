package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author chengang
 * @Date 2022/5/19
 */
@Data
public class OptimizationGoalVo implements Serializable {

	private static final long serialVersionUID = 8786322829864632267L;

	private String name;

	private String value;

	private List<OptimizationGoal> behaviorList;

	private List<OptimizationGoal> worthList;

}
