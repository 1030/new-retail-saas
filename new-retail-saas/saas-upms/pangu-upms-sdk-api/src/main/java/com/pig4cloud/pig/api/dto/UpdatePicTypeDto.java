package com.pig4cloud.pig.api.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UpdatePicTypeDto {
	@NotNull(message = "id为空")
	private Integer id;
	@NotNull(message = "type为空")
	private Integer type;
}
