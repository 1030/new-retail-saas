package com.pig4cloud.pig.api.gdt.dto;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pig4cloud.pig.api.gdt.entity.GdtAdCreativeTemp;
import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 广点通广告创意临时表 gdt_ad_creative_temp
 * 
 * @author hma
 * @date 2020-12-11
 */
@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class GdtAdCreativeCreateDto extends GdtAdCreativeTemp implements Serializable
{
	private static final long serialVersionUID = 1L;

	private Long adgroupId;
	/**
	 * 广告组名称（广告名称）
	 */
	private String adgroupName;

	private JSONObject adcreativeElementsObject;
	/**
	 * 广告版位  jonsarr字符串
	 */
	private String siteSets;
	public String getSiteSets(){
		return StringUtils.isNotBlank(siteSets)? siteSets = siteSets.replaceAll("&quot;","\""):siteSets;
	}





}
