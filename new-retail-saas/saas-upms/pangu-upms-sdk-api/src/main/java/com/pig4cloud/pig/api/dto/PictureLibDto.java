package com.pig4cloud.pig.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: nml
 * @time: 2020/11/18 17:59
 **/
@Getter
@Setter
public class PictureLibDto {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键id :图片id
	 */
	private Long id;

	/**
	 * 图片名称：前端显示名称 可修改
	 */
	private String pictureName;

	/**
	 * 图片文件存储名称
	 */
	private String pictureFilename;

	private Integer pictureType;

	private Integer sourceType;

	private Integer videoId;

	/**
	 * 图片访问地址
	 */
	private String pictureUrl;

	private String realPath;

	private String md5;

	private String width;

	private String height;

	private Integer screenType;

	private String pSize;

	private String format;

	/**
	 * 图片尺寸
	 */
	private String pictureSize;

	/**
	 * 图片是否删除：0:删除；1：未删除
	 */
	private String pictureStatus;

	/**
	 * 标签id 可修改
	 */
	private String labelId;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * tt平台id
	 */
	private Long ttId;

	/**
	 * gdt平台id
	 */
	private Long gdtId;

	private String ttAdvertiserIds;

	private String gdtAdvertiserIds;

	private List<Map<String, String>> pushTtAdStatusList;

	private List<Map<String, String>> pushGdtAdStatusList;

	private String labelname;

	private Integer p_type;

}
