package com.pig4cloud.pig.api.vo;


import lombok.Getter;
import lombok.Setter;

/**
 * @ 程序化落地页 前端控制器
 * @author yk
 */
@Setter
@Getter
public class Site {

	/**
	 * 落地页组ID
	 */
	private String groupId;

	private String siteName;

	private String member_id;

	private String site_id;

	private String site_url;

	private String site_opt_status;

	private String site_audit_status;

}
