package com.pig4cloud.pig.api.dto;


import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Created by hma on 2020/11/6.
 * 账号预算传输对象
 */
@Setter
@Getter
public class BudgetDto {

	private String advertiserId;

	/**
	 * 日预算金额：范围1000<=X <= 9999999.99、仅支持最多2位小数据
	 */
	private BigDecimal budget;

	/**
	 * 日元算类型：BUDGET_MODE_DAY（日预算）
	 BUDGET_MODE_INFINITE（不限）
	 */
	private String budgetMode;
	/**
	 * 1 修改日预算  2 定时修改日预算
	 */
	private Integer type;
	/**
	 * 广告组id
	 */
	private Long campaignId;
	/**
	 * 广告计划id
	 */
	private Long adId;

	/**
	 * 目标转换出价
	 */
	private BigDecimal cpaBid;

	/**
	 * 广点通广告组id
	 */
	private Long adgroupId;


	/**
	 * 广点通推广计划id
	 */
	private Long campId;

	/**
	 * 判断新老广告计划,1:表示老广告计划 2:表示新广告计划
 	 */
	private String versionType;


}
