package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "ad_terms_banned")
public class AdTermBannedEntity extends Model<AdTermBannedEntity> {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 屏蔽词
	 */
	private String terms;

	/**
	 * 广告账户ID
	 */
	@TableField(value = "advertiser_id")
	private String advertiserId;

	/**
	 * 广告账户名称
	 */
	@TableField(value = "advertiser_name")
	private String advertiserName;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_date")
	private Date createDate;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_date")
	private Date updateDate;

	/**
	 * 是否删除
	 */
	@TableField(value = "is_deleted")
	private Integer isDeleted;

	public AdTermBannedEntity setId(Long id) {
		this.id = id;
		return this;
	}

	public AdTermBannedEntity setTerms(String terms) {
		this.terms = terms;
		return this;
	}

	public AdTermBannedEntity setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
		return this;
	}

	public AdTermBannedEntity setAdvertiserName(String advertiserName) {
		this.advertiserName = advertiserName;
		return this;
	}

	public AdTermBannedEntity setCreateDate(Date createDate) {
		this.createDate = createDate;
		return this;
	}

	public AdTermBannedEntity setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
		return this;
	}

	public AdTermBannedEntity setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
		return this;
	}
}
