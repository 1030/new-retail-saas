package com.pig4cloud.pig.api.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

/**
 * @author ：lile
 * @date ：2021/7/13 16:36
 * @description：
 * @modified By：
 */
@Data
public class AdAgentTransferDto extends Page {

	/**
	 * id
	 */
	private Integer id;

	/**
	 * 代理商id
	 */
	private Integer agentId;

	/**
	 * 渠道类型
	 */
	private Integer platform;

	/**
	 * 广告账户代理的代理商ID
	 */
	private Long adagentId;

	/**
	 * 主渠道编码
	 */
	private String chncode;


	/**
	 * 主渠道名称
	 */
	private String chnname;

}
