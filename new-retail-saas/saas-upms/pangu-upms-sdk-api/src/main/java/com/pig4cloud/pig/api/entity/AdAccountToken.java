package com.pig4cloud.pig.api.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class AdAccountToken implements Serializable {

    private static final long serialVersionUID = 6618752925976815391L;

    private String advertiserId;

    private String advertiserName;

    private String accessToken;

    private String housekeeper;

	private BigDecimal balance;
}
