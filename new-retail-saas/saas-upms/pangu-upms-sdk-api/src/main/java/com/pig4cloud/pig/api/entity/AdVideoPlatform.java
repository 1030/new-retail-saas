package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @ClassName AdVideoPlatform.java
 * @author cx
 * @version 1.0.0
 * @Desc TODO
 * @Time 2020/11/12 16:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="ad_video_platform")
public class AdVideoPlatform extends Model<AdVideoPlatform> {

	private static final long serialVersionUID = 1L;

    /**
     * 三方平台：1头条，2广点通
     */
	@ApiModelProperty(value = "三方平台：1头条，2广点通")
	@TableField(value = "type")
    private String type;

    /**
     * 台视频id
     */
	@ApiModelProperty(value = "平台视频id")
	@TableField(value = "vid")
    private String vid;

	/**
	 * 广告账户ID
	 */
	@ApiModelProperty(value = "广告账户ID")
	@TableField(value = "advertiser_id")
	private String advertiserId;

    /**
     * 三方平台视频ID
     */
	@ApiModelProperty(value = "三方平台视频ID")
	@TableField(value = "platform_vid")
    private String platformVid;

    /**
     * 三方素材ID
     */
	@ApiModelProperty(value = "三方素材ID")
	@TableField(value = "material_id")
    private String materialId;

    /**
     * 修改时间
     */
	@ApiModelProperty(value = "修改时间")
	@TableField(value = "updatetime")
    private Date updatetime;

}