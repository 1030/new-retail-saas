package com.pig4cloud.pig.api.gdt.dto;

import com.alibaba.fastjson.JSONObject;
import com.pig4cloud.pig.api.gdt.entity.GdtAdgroupTemp;
import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Getter;
import lombok.Setter;

/**
 * 广点通-广告组临时参数dto
 * 
 * @author hma
 * @date 2020-12-05
 */
@Setter
@Getter
public class GdtAdgroupCreateTempDto extends GdtAdgroupTemp
{
	private static final long serialVersionUID = 1L;

	private String accountId;
	/**
	 * 是否开启分版位出价：0 未开启 1 开启
	 */
	private Integer autoEnable=0;

	private JSONObject targetingObject;
	private JSONObject bidAdjustmentObject;
	private String siteSets;
	/**
	 * 是否精准归因：true  false
	 */


	private Boolean copyFlag  = false;
	public String getSiteSets(){
		return StringUtils.isNotBlank(siteSets)? siteSets = siteSets.replaceAll("&quot;","\""):siteSets;
	}

}
