package com.pig4cloud.pig.api.vo;
import com.pig4cloud.pig.api.entity.AdCampaignStatistic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 投放广告计划报表 数据展示
 * 
 * @author hma
 * @date 2020-11-10
 */
@Setter
@Getter
public class AdPlanStatisticVo extends AdCampaignStatistic
{
	private static final long serialVersionUID = 1L;
	/** 广告计划名称 */
	private String adName;
	/** 状态：AD_STATUS_DELIVERY_OK	投放中	AD_STATUS_DISABLE	计划暂停	AD_STATUS_AUDIT	新建审核中	AD_STATUS_REAUDIT	修改审核中	AD_STATUS_DONE	已完成（投放达到结束时间）	AD_STATUS_CREATE	计划新建	AD_STATUS_AUDIT_DENY	审核不通过	AD_STATUS_BALANCE_EXCEED	账户余额不足	AD_STATUS_BUDGET_EXCEED	超出预算	AD_STATUS_NOT_START	未到达投放时间	AD_STATUS_NO_SCHEDULE	不在投放时段	AD_STATUS_CAMPAIGN_DISABLE	已被广告组暂停	AD_STATUS_CAMPAIGN_EXCEED	广告组超出预算	AD_STATUS_DELETE	已删除	AD_STATUS_ALL	所有包含已删除	AD_STATUS_NOT_DELETE */
	private String status;
	/** 投放范围 ： 通投智选(UNIVERSAL)  默认(DEFAULT) 穿山甲(UNION) */
	private String deliveryRange;
	/** 投放形式： 激励视频(REWARDED_VIDEO) 原生(ORIGINAL_VIDEO) 开屏(SPLASH_VIDEO) */
	private String unionVideoType;
	/** 目标：1 转化量 */
	private String target;
	/** 下载方式：DOWNLOAD_URL下载链接，QUICK_APP_URL快应用+下载链接，EXTERNAL_URL落地页链接 */
	private String downloadType;
	/** 下载地址 */
	private String downloadUrl;
	/** 落地页链接 */
	private String externalUrl;
	/** 应用下载详情链接 */
	private String quickAppUrl;
	/** 定向包主键id */
	@ApiModelProperty(value = " 定向包")
	private Long audienceId;
	/** 转化目标主键id */
	private Long convertId;
	/** 投放时间类型："SCHEDULE_FROM_NOW"从今天起长期投放, "SCHEDULE_START_END"设置开始和结束日期 */
	private String scheduleType;
	/** 投放开始时间 */
	private String startTime;
	/** 投放结束时间 */
	private String endTime;
	/** 投放场景：*/
	private Integer launchScenario;
	/** 投放方式 */
	private String flowControlMode;
	/** 创建时间 */
	private Date adCreateTime;
	/** 修改时间 */
	private Date adModifyTime;
	/** 投放场景：FLOW_CONTROL_MODE_FAST	优先跑量（对应CPC的加速投放）	FLOW_CONTROL_MODE_SMOOTH	优先低成本（对应CPC的标准投放）	FLOW_CONTROL_MODE_BALANCE	均衡投放（新增字段） */
	private String smartBidType;
	/** 预算类型：取值: "BUDGET_MODE_DAY"日预算, "BUDGET_MODE_TOTAL"总预算 */
	private String budgetMode;
	/** 预算(出价方式为CPC、CPM、CPV时，不少于100元；出价方式为OCPM、OCPC时，不少于300元；取值范围: ≥ 0 */
	private BigDecimal budget;
	/** 创意投放位置 */
	private String inventoryType;
	/**
	 * 广告账号名称
	 */
	private String name;
	/**
	 * 广告账号
	 */
	private String  servingAccount;


	/** 投放类型：1 头条  2 广点通 */
	private Integer adType;

	/**
	 * 类型描述
	 */
	private String acTypeDesc;

	/**
	 * 转换目标名称
	 */
	private String convertName;



	private String downPackage;

	private String pricing;
	/** 点击出价/展示出价，当pricing为"CPC"、"CPM"、"CPA"出价方式时必填 */
	private BigDecimal bid;
	/** 目标转化出价/预期成本， 当pricing为"OCPM"、"OCPC"出价方式时必填） */

	private BigDecimal cpaBid;

	/**
	 * 推广目的
	 */
	private String adTarget;

	
	/**
	 * 超出消耗预算预警阈值
	 */
	private String costWarning;

	/**
	 * 超出消耗预算预警阈值
	 */
	@ApiModelProperty(value = "性别")
	private String gender;
	/**
	 * 超出消耗预算预警阈值
	 */
	@ApiModelProperty(value = "年龄")
	private String  age;


	@ApiModelProperty(value = "人群定向包")
	private String retargetingTagsInclude;

	@ApiModelProperty(value = "排除定向包")
	private String retargetingTagsExclude;


	@ApiModelProperty(value = "行为兴趣")
	private String interestActionMode;
	@ApiModelProperty(value = "行为场景")
	private String actionScene;
	@ApiModelProperty(value = "用户行为发生的天数")
	private String actionDays;
	@ApiModelProperty(value = "行为类目词")
	private String actionCategories;
	@ApiModelProperty(value = "行为关键词")
	private String actionWords;
	@ApiModelProperty(value = "兴趣分类")
	private String	interestCategories;
	@ApiModelProperty(value = "兴趣关键词")
	private String interestWords;

	// 新增字段
	@ApiModelProperty(value = "深度出价类型/深度优化方式")
	private String deepBidType;

	// 新增字段
	@ApiModelProperty(value = "ROI系数")
	private BigDecimal roiGoal;

	// 新增字段
	@ApiModelProperty(value = "学习期状态")
	private String learningPhase;

	//todo  计算投放天数
	@ApiModelProperty(value = "投放天数")
	private Long scheduleDay;

	// 新增字段
	@ApiModelProperty(value = "投放量级(预估)")
	private Long  estimateNum;











}
