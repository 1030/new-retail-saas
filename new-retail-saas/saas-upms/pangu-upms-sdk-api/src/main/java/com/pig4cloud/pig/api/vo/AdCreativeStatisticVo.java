package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.entity.AdCampaignStatistic;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 广告创意表 ad_creative
 * 
 * @author hma
 * @date 2020-11-10
 */
@Setter
@Getter
public class AdCreativeStatisticVo extends AdCampaignStatistic
{
	private static final long serialVersionUID = 1L;

	/** 创意状态： CREATIVE_STATUS_DELIVERY_OK	投放中	CREATIVE_STATUS_NOT_START	未到达投放时间	CREATIVE_STATUS_NO_SCHEDULE	不在投放时段	CREATIVE_STATUS_DISABLE	创意暂停	CREATIVE_STATUS_CAMPAIGN_DISABLE	已被广告组暂停	CREATIVE_STATUS_CAMPAIGN_EXCEED	广告组超出预算	CREATIVE_STATUS_AUDIT	新建审核中	CREATIVE_STATUS_REAUDIT	修改审核中	CREATIVE_STATUS_DELETE	已删除	CREATIVE_STATUS_DONE	已完成（投放达到结束时间）	CREATIVE_STATUS_AD_DISABLE	广告计划暂停	CREATIVE_STATUS_AUDIT_DENY	审核不通过	CREATIVE_STATUS_BALANCE_EXCEED	账户余额不足	CREATIVE_STATUS_BUDGET_EXCEED	超出预算	CREATIVE_STATUS_DATA_ERROR	数据错误（数据错误时返回，极少出现）	CREATIVE_STATUS_PRE_ONLINE	预上线	CREATIVE_STATUS_AD_AUDIT	广告计划新建审核中	CREATIVE_STATUS_AD_REAUDIT	广告计划修改审核中	CREATIVE_STATUS_AD_AUDIT_DENY	广告计划审核不通过	CREATIVE_STATUS_ALL	所有包含已删除	CREATIVE_STATUS_NOT_DELETE	所有不包含已删除（状态过滤默认值）	CREATIVE_STATUS_ADVERTISER_BUDGET_EXCEED	超出账户日预算 */
	private String status;
	/** 创意素材类型（素材样式） */
	private String imageMode;
	private String imageIds;
	/** 视频素材，封面图片ID */
	private String imageId;
	/** 视频素材，视频ID） */
	private String videoId;
	/** 广告位置 */
	private String inventoryType;
	/** 创意方式：1 程序化创意  2 自定义创意 */
	private Integer creativeType;
	/** 创建时间 */
	private Date creativeCreateTime;
	/** 修改时间 */
	private Date creativeModifyTime;
	/**
	 * 图片类型 ：1 图片  2 视频
	 */
	private String imgType;
	/**
	 * 视频素材，封面图片url
	 */
	private String imgUrl;

	private String videoUrl;
	/**
	 * 媒体类型
	 */
	private String adType;

	/**
	 * 类型描述
	 */
	private String acTypeDesc;

	/** 创意方式描述*/
	private String creativeTypeDes;


	/**
	 * 投放账号
	 */
	private String servingAccount;
	/**
	 * 投放账号名称
	 */
	private String name;


}
