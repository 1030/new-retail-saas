package com.pig4cloud.pig.api.vo;

import lombok.Data;

/**
 * @Title null.java
 * @Package com.pig4cloud.pig.api.vo
 * @Author 马嘉祺
 * @Date 2021/7/20 17:24
 * @Description
 */
@Data
public class PushIconVO {

	private Boolean success;

	private String message;

	private Long materialId;

	private String imageId;

	private String advertiserId;

	private String imageUri;

	public PushIconVO setSuccess(Boolean success) {
		this.success = success;
		return this;
	}

	public PushIconVO setMessage(String message) {
		this.message = message;
		return this;
	}

	public PushIconVO setMaterialId(Long materialId) {
		this.materialId = materialId;
		return this;
	}

	public PushIconVO setImageId(String imageId) {
		this.imageId = imageId;
		return this;
	}

	public PushIconVO setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
		return this;
	}

	public PushIconVO setImageUri(String imageUri) {
		this.imageUri = imageUri;
		return this;
	}

}
