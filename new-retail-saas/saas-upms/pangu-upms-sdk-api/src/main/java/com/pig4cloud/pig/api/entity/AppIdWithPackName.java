package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="wan_game_channel_platform")
public class AppIdWithPackName extends Model<AppIdWithPackName> {

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "gameid")
	private Integer gameid;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "appid")
	private String appid;

	@TableField(updateStrategy = FieldStrategy.NOT_NULL)
	@ApiModelProperty(value = "pkName")
	private String pkName;
}
