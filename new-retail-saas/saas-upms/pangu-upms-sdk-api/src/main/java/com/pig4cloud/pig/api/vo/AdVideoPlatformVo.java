package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @ClassName AdVideoPlatform.java
 * @author cx
 * @version 1.0.0
 * @Desc TODO
 * @Time 2020/11/12 16:10
 */
@Data
public class AdVideoPlatformVo extends Page {

	private String id;
    /**
     * 三方平台：1头条，2广点通
     */
    private String type;

    /**
     * 台视频id
     */
    private String vid;

	/**
	 * 广告账户ID
	 */
	private String advertiserId;

    /**
     * 三方平台视频ID
     */
    private String platformVid;

    /**
     * 三方素材ID
     */
    private String materialId;

    /**
     * 修改时间
     */
    private Date updatetime;
	/**
	 * 广告账户名称
	 */
	private String advertiserName;
	/**
	 * 创建人ID
	 */
	private Integer userId;

}