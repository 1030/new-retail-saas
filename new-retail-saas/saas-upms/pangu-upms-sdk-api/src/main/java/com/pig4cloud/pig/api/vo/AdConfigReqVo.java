package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AdConfigReqVo implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 522727844502361899L;

	/*
	 * 广告主id
	 */
	private Long advertiserId;

	/*
	 * 广告组id
	 */
	private Long campaignId;

	/*
	 * 广告计划id(本地计划ID)
	 */
	private Long adId;

	/*
	 * 自定义广告计划名称
	 */
	private String adName;

	/*
	 * 广告计划数量
	 * 20220407 监测组的数量 = 广告计划数量
	 */
	private Integer adNum;

	/*
	 * 广告创意-数量
	 */
	private Integer adCreativeNum;

	/*
	 * 转化跟踪ID列表
	 * 20220407 改为事件机制，此字段暂不用
	 */
	private List<Long> convertTrackIdList;

	/*
	 * 本地广告创意ID
	 */
	private Long adCreativeId;

}
