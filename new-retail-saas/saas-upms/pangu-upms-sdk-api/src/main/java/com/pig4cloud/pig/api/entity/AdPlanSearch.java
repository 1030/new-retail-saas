package com.pig4cloud.pig.api.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author chengang
 * @Date 2022/1/10
 */
@Data
public class AdPlanSearch implements Serializable {

	private static final long serialVersionUID = 5585493037151429811L;

	private String adId;
	private String adName;

	public AdPlanSearch (String adId,String adName) {
		this.adId = adId;
		this.adName = adName;
	}



}
