package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 运营素材表
 *
 * @TableName ad_operate_material
 */
@Data
@EqualsAndHashCode
@TableName(value = "ad_operate_game")
public class AdOperateGameDO implements Serializable {

	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 运营素材ID
	 */
	private Long materialId;

	/**
	 * 子游戏主键
	 */
	private Long gameid;

	public AdOperateGameDO setId(Long id) {
		this.id = id;
		return this;
	}

	public AdOperateGameDO setMaterialId(Long materialId) {
		this.materialId = materialId;
		return this;
	}

	public AdOperateGameDO setGameid(Long gameid) {
		this.gameid = gameid;
		return this;
	}

}