package com.pig4cloud.pig.api.dto;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdVideoReport.java
 * @createTime 2020年11月12日 20:00:00
 */
public class AdVideoReport implements Serializable {
	private String name;
	private String advertiserId;
	private String cost;
	private String show;
	private String cpm;
	private String click;
	private String clickRate;
	private String avgClickCost;
	private String convert;
	private String convertRate;
	private String convertCost;

	public String getAdvertiserId() {
		return advertiserId;
	}

	public void setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getShow() {
		return show;
	}

	public void setShow(String show) {
		this.show = show;
	}

	public String getCpm() {
		if (StringUtils.isBlank(cpm) && StringUtils.isNotBlank(cost) && StringUtils.isNotBlank(click)){
			cpm = String.format("%.2f", Double.parseDouble(cost)/Double.parseDouble(click));
		}
		return cpm;
	}

	public void setCpm(String cpm) {
		this.cpm = cpm;
	}

	public String getClick() {
		return click;
	}

	public void setClick(String click) {
		this.click = click;
	}

	public String getAvgClickCost() {
		return avgClickCost;
	}

	public void setAvgClickCost(String avgClickCost) {
		this.avgClickCost = avgClickCost;
	}

	public String getConvert() {
		return convert;
	}

	public void setConvert(String convert) {
		this.convert = convert;
	}

	public String getConvertCost() {
		return convertCost;
	}

	public void setConvertCost(String convertCost) {
		this.convertCost = convertCost;
	}

	public String getConvertRate() {
		if (StringUtils.isBlank(convertRate) && StringUtils.isNotBlank(convert) && StringUtils.isNotBlank(click)){
			convertRate = String.format("%.2f", Double.parseDouble(convert)/Double.parseDouble(click)*100);
		}
		return convertRate;
	}

	public void setConvertRate(String convertRate) {
		this.convertRate = convertRate;
	}

	public String getClickRate() {
		if (StringUtils.isBlank(clickRate) && StringUtils.isNotBlank(click) && StringUtils.isNotBlank(show)){
			clickRate = String.format("%.2f", Double.parseDouble(click)/Double.parseDouble(show)*100);
		}
		return clickRate;
	}

	public void setClickRate(String clickRate) {
		this.clickRate = clickRate;
	}
}
