package com.pig4cloud.pig.api.gdt.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @蹊径落地页
 * @author yk
 */
@Data
public class GdtXlpDto {

	@ApiModelProperty(value = "自增ID")
    private Long id;

    /**
     * 落地页缩略图
     */
	@ApiModelProperty(value = "落地页缩略图地址：落地页缩略图")
    private String previewUrl;

    /**
     * 落地页名称
     */
	@ApiModelProperty(value = "落地页名称")
    private String pageName;

    /**
     * 落地页id
     */
	@ApiModelProperty(value = "落地页ID")
    private String pageId;

    /**
     * 状态（蹊径落地页发布状态）
     */
	@ApiModelProperty(value = "状态")
    private String pagePublishStatus;

    /**
     * 状态（蹊径落地页审核状态）
     */
	@ApiModelProperty(value = "状态")
    private String pageStatus;

	@ApiModelProperty(value = "状态名称")
	private String pageStatusName;


	private String pageType;

}
