package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdVideoReportData.java
 * @createTime 2021年01月18日 14:25:00
 */
@Data
public class AdVideoReportData implements Serializable {
	/**
	 * 视频ID
	 */
	private String id;
	/**
	 * 视频名称
	 */
	private String videoName;
	/**
	 * 视频播放地址
	 */
	private String videoUrl;
	/**
	 * 视频图片地址
	 */
	private String videoImageUrl;
	/**
	 * 素材ID
	 */
	private String materialId;
	/**
	 * 制作人名称
	 */
	private String makeName;
	/**
	 * 平台名称
	 */
	private String platformName;
	/**
	 * 消耗
	 */
	private BigDecimal cost;
	/**
	 * 曝光数/展示数
	 */
	private Integer showData;
	/**
	 * 点击数
	 */
	private Integer click;
	/**
	 * 点击率(点击数/曝光数)
	 */
	private BigDecimal ctr;
	/**
	 * 25%播放占比(播放时长超过25%进度的次数/播放数)
	 */
	private BigDecimal play25FeedBreakRate;
	/**
	 * 50%播放占比(播放时长超过50%进度的次数/播放数)
	 */
	private BigDecimal play50FeedBreakRate;
	/**
	 * 99%播放占比(播放时长超过99%进度的次数/播放数)
	 */
	private BigDecimal play99FeedBreakRate;
	/**
	 * 激活数
	 */
	private Integer active;
	/**
	 * 激活成本
	 */
	private BigDecimal activeCost;
	/**
	 * 点击激活率
	 */
	private BigDecimal clickActiveRate;
	/**
	 * 注册数
	 */
	private Integer register;
	/**
	 * 注册成本
	 */
	private BigDecimal registerCost;
	/**
	 * 注册率
	 */
	private BigDecimal registerRate;
	/**
	 * 新增付费人数
	 */
	private Integer newPayUserNumber;
	/**
	 * 付费次数
	 */
	private Integer payNumber;
	/**
	 * 新增付费成本
	 */
	private BigDecimal newPayCost;
	/**
	 * 新增付费率
	 */
	private BigDecimal newPayRate;
	/**
	 * 新增付费金额
	 */
	private BigDecimal newPayMoney;
	/**
	 * 首日ROI
	 */
	private BigDecimal firstDayROI;
	/**
	 * 首日LTV
	 */
	private BigDecimal firstDayLTV;
	/**
	 * 3日LTV
	 */
	private BigDecimal threeDayLTV;
	/**
	 * 7日LTV
	 */
	private BigDecimal sevenDayLTV;
	/**
	 * 15日LTV
	 */
	private BigDecimal fifteenDayLTV;
	/**
	 * 30日LTV
	 */
	private BigDecimal thirtyDayLTV;
	/**
	 * 次留率
	 */
	private BigDecimal twoStayRate;
	/**
	 * 3留率
	 */
	private BigDecimal threeStayRate;
	/**
	 * 7留率
	 */
	private BigDecimal sevenStayRate;

	/**
	 * 15留率
	 */
	private BigDecimal fifteenStayRate;
	/**
	 * 30留率
	 */
	private BigDecimal thirtyStayRate;

	/**
	 * 总充值金额
	 */
	private BigDecimal payMoney;
	/**
	 * 累计ROI
	 */
	private BigDecimal totalROI;
	/**
	 * 档位通过率
	 */
	private BigDecimal gearPassRate;
}
