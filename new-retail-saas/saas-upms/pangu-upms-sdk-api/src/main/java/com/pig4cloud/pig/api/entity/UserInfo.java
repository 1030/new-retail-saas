package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName(value="cs_user_info")
public class UserInfo {

	
    private Integer id ;
    
    @ApiModelProperty(value = "用户名称")
	@TableField(value = "user_name")
    private String userName ;
    
    @ApiModelProperty(value = "用户密码")
	@TableField(value = "pass_word")
    private String passWord ;
    private String phone ;
    private String email ;
    
    @ApiModelProperty(value = "用户内存")
	@TableField(value = "create_day")
    private String createDay ;
}
