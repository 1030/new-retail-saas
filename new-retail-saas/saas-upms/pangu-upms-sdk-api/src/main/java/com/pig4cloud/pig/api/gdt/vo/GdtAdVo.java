package com.pig4cloud.pig.api.gdt.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @description:
 * @author: nml
 * @time: 2020/12/18 12:26
 **/

@Setter
@Getter
public class GdtAdVo extends Page {

	/**
	 * 开始时间
	 */
	private String sdate;

	/**
	 * 结束时间
	 */
	private String edate;

	/*接收广告账户ids*/
	private String[] advertiserIds;

	/*广告账户ids*/
	private List<String> accountIds;

	/*开关*/
	private String onOff;

	/*广告名称*/
	private String adName;

	/** 广告 id */
	private Long adId;

}
