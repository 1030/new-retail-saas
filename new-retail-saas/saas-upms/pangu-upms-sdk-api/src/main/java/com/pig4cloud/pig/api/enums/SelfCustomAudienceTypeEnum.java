/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public enum SelfCustomAudienceTypeEnum {

	IMEI(0,"IMEI"),
	IDFA(1,"IDFA"),
	UID(2,"UID"),
	IMEI_MD5(4,"HASH_IMEI"),
	IDFA_MD5(5,"HASH_IDFA"),
	MOBILE_HASH_SHA256(6,"SHA256_MOBILE_PHONE"),
	OAID(7,"OAID"),
	OAID_MD5(8,"HASH_OAID"),
	MOBILE_MD5(9,"HASH_MOBILE_PHONE"),
	MAC(10,"MAC");


	private final Integer id;
	private final String value;

	public static String getNameByKey(Integer id){
		if (id == null){
			return null;
		}
		for (SelfCustomAudienceTypeEnum item : SelfCustomAudienceTypeEnum.values()) {
			if (id.equals(item.getId())) {
				return item.getValue();
			}
		}
		return null;

	}

}
