package com.pig4cloud.pig.api.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AdCommentDTO extends Page {
	private List<String> adIdList;
	private String startTime;
	private String endTime;
	private String text;
	private Integer rank;
	private String replyStatus;

	List<String> advertiserIds;
}
