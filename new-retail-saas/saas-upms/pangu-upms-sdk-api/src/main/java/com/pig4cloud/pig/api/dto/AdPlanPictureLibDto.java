package com.pig4cloud.pig.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @description:
 * @author: nml
 * @time: 2020/11/21 11:07
 **/
@Getter
@Setter
public class AdPlanPictureLibDto {

	/**
	 * 图片id
	 */
	private Long pictureId;

	/**
	 * 图片名称：前端显示名称 可修改
	 */
	private String pictureName;

	/**
	 * 图片文件存储名称
	 */
	private String pictureFilename;


	private String pictureUrl;

/*	*//**
	 * 图片类型: 1:小图;2:横版大图;3:竖版大图;4:gif图;0:其他类型;
	 *//*
	private Integer pictureType;

	*//**
	 * 平台id:1:tt;2:uc;3:gdt
	 *//*
	private Integer platformId;

	*//**
	 * 平台名称
	 *//*
	private String platformName;

	*//**
	 * 广告账户id
	 *//*
	private String advertiserId;*/

	/**
	 * 第三方平台图片id
	 */
	private String platformPictureId;

	/**
	 * 第三方平台图片size
	 */
	private Integer platformPictureSize;

	/**
	 * 第三方平台图片width
	 */
	private Integer platformPictureWidth;

	/**
	 * 第三方平台图片height
	 */
	private Integer platformPictureHeight;

	/**
	 * 第三方平台图片预览地址(1h有效)
	 */
	private String platformPictureUrl;

	/**
	 * 第三方平台图片格式
	 */
	private String platformPictureFormat;

	/**
	 * 第三方平台图片md5
	 */
	private String platformPictureSignature;

	/**
	 * 第三方平台图片素材id
	 */
	private String platformPictureMaterialId;

/*	*//**
	 * 创建时间
	 *//*
	private Date createTime;

	*//**
	 * 更新时间
	 *//*
	private Date updateTime;*/

}
