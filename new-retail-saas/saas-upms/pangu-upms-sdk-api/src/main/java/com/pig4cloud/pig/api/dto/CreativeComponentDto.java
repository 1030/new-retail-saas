package com.pig4cloud.pig.api.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Description 基础创意组件DTO
 * @Author chengang
 * @Date 2021/8/24
 */
@Data
public class CreativeComponentDto implements Serializable {


	@NotEmpty(message = "广告账户不能为空")
	private String advertiser_id;

	@NotEmpty(message = "组件名称不能为空")
	@Size(min = 1,max = 20,message = "组件名称长度范围[1,20]")
	private String component_name;

	private String component_type;

	@NotEmpty(message = "组件数据JSON不能为空")
	private String component_data;

	/**
	 * 组件ID，编辑必填
	 */
	private String component_id;




}
