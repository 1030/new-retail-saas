package com.pig4cloud.pig.api.entity;

import com.pig4cloud.pig.common.core.util.HashUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * 广告表 ad
 *
 * @author yk
 * @date 2020-12-05
 */
@Setter
@Getter
public class GdtActionInterest {
    private static final long serialVersionUID = 1L;
    /**
     * 唯一标识
     */
    private Long id;
    /**
     * 三方对应父主键id
     */
    private Long thirdParentId;
    /**
     * 广点通数据对应主键id
     */
    private Long thirdId;
    /**
     * 名称
     */
    private String thirdName;
    /**
     * BEHAVIOR（行为），INTEREST（兴趣），INTENTION（意向）
     */
    private String type;
    /**
     * 父名称
     */
    private String thirdParentName;
    /**
     * 城市级别，仅当 type=REGION、BUSINESS_DISTRICT 时有效
     */
    private String thirdCityLevel;
    /**
     * 行为兴趣标签返回类型，仅当 type=BEHAVIOR、INTEREST 时有效
     */
    private String thirdTagClass;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GdtActionInterest that = (GdtActionInterest) o;
        return this.hashCode() == HashUtil.apHash(String.format("%s_%s_%s", that.thirdId, that.thirdParentId, that.type));
    }

    @Override
    public int hashCode() {
        return HashUtil.apHash(String.format("%s_%s_%s", this.thirdId, this.thirdParentId, this.type));
    }
}
