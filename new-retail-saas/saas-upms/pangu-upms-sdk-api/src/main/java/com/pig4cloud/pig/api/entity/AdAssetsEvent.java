package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 广告资产事件表
 * @author  chenxiang
 * @version  2022-04-01 16:25:19
 * table: ad_assets_event
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_assets_event")
public class AdAssetsEvent extends Model<AdAssetsEvent>{
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 资产ID
	 */
	@TableField(value = "asset_id")
	private Long assetId;
	/**
	 * 事件ID
	 */
	@TableField(value = "event_id")
	private Long eventId;
	/**
	 * 广告账户
	 */
	@TableField(value = "advertiser_id")
	private String advertiserId;
	/**
	 * 统计方式， 允许值：ONLY_ONE仅一次、EVERY_ONE每一次（仅付费ROI时可用）
	 */
	@TableField(value = "statistical_type")
	private String statisticalType;
	/**
	 * 事件回传方式列表，允许值:落地页支持:JSSDK JS埋码 、EXTERNAL_API API回传、XPATH XPath圈选应用支持：APPLICATION_API 应用API、APPLICATION_SDK 应用SDK、快应用支持：QUICK_APP_API 快应用API
	 */
	@TableField(value = "track_types")
	private String trackTypes;
	/**
	 * 事件类型
	 */
	@TableField(value = "event_type")
	private String eventType;
	/**
	 * 事件中文名称
	 */
	@TableField(value = "event_cn_name")
	private String eventCnName;
	/**
	 * 事件描述
	 */
	@TableField(value = "description")
	private String description;
	/**
	 * 创建时间
	 */
	@TableField(value = "third_create_time")
	private Date thirdCreateTime;
	/**
	 * 激活免联调状态，枚举值：Active 已激活、Inactive 未激活
	 */
	@TableField(value = "debugging_status")
	private String debuggingStatus;
	/**
	 * 事件属性
	 */
	@TableField(value = "properties")
	private String properties;
	/**
	 * 来源(1:API,2:渠道后台)
	 */
	@TableField(value = "soucre_status")
	private Integer soucreStatus;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "create_id")
	private Long createId;
	/**
	 * 修改人
	 */
	@TableField(value = "update_id")
	private Long updateId;
}

	

	
	

