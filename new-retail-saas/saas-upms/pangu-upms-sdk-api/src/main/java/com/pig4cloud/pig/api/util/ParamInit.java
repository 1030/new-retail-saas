package com.pig4cloud.pig.api.util;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * <p>
 * Title: 参数配置类
 * </p>
 */

public class ParamInit {
	private static Logger logger = LoggerFactory.getLogger(ParamInit.class);
    private static Hashtable hsparam = null;
    private static ParamInit paramInit = new ParamInit();
    private static String PARAMFILENAME = "application.properties";
    private static long lastRead = 0;
    private static long time = 3;

    private URL url = null;

    static {
        try {
            hsparam = new Hashtable();
            paramInit.check();
        } catch (Exception ex) {
        }

    }

    public static Hashtable getHashParam() {
        return hsparam;
    }

    public static Object getObject(String key) {
        Object value = hsparam.get(key.trim());
        return value;
    }

    public static String getString(String key) {
        String value = String.valueOf(hsparam.get(key.trim())).trim();
        return value;
    }

    public static Long getLong(String key) {
        Long value = NumericConverter(String.valueOf(hsparam.get(key.trim())).trim());
        return value;
    }

    public static Boolean getBoolean(String key) {
        Boolean value = BooleanConverter(String.valueOf(hsparam.get(key.trim())).trim());
        return value;
    }

    public static Boolean BooleanConverter(String value) {
        if (value == null) {
            return null;
        }
        if ("false".equalsIgnoreCase(value)) {
            return false;
        } else if ("true".equalsIgnoreCase(value)) {
            return true;
        } else {
            return null;
        }
    }

    public static Long NumericConverter(String value) {
        if (value == null) {
            return null;
        }
        boolean isNum = StringUtils.isNumeric(value);
        if (isNum) {
            return Long.valueOf(value);
        } else {
            return null;
        }
    }

    private void check() {
        if (url == null) {
            URL u = getClass().getClassLoader().getResource(PARAMFILENAME);
            if (u == null) {
                throw new IllegalStateException("读取项目配置失败.文件不存在");
            }
            url = u;
        }

        synchronized (url) {
            if (System.currentTimeMillis() - lastRead > time * 60000) {
                InputStream inputstream = null;
                try {
                    inputstream = url.openStream();
                    if (inputstream != null) {
                        java.util.Properties p = new java.util.Properties();
                        p.load(inputstream);
                        Enumeration propertyNames = p.propertyNames();
                        hsparam.clear();
                        while (propertyNames.hasMoreElements()) {
                            String propertyname = (String) propertyNames.nextElement();
                            hsparam.put(propertyname, p.getProperty(propertyname));
                            if ("logRoot".equals(propertyname)){
                            	System.setProperty(propertyname, p.getProperty(propertyname));
                            }
                        }
                        lastRead = System.currentTimeMillis();
                        logger.debug("项目配置文件读取成功");
                    } else {
                        logger.error("读取项目配置失败.文件不存在");
                    }
                } catch (IOException e) {
                    logger.error("读取项目配置失败", e);
                } finally {
                    if (inputstream != null) {
                        try {
                            inputstream.close();
                        } catch (IOException e) {
                            // nothing to do
                        }
                    }
                }
            }
        }
    }
}