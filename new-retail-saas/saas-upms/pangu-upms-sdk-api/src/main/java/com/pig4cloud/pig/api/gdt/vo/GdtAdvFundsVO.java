package com.pig4cloud.pig.api.gdt.vo;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;

import com.pig4cloud.pig.api.util.StringUtils;
import lombok.Data;

@Data
public class GdtAdvFundsVO {


	private String accountId;

	@Digits(integer = 18, fraction = 2, message = "金额格式错误")
	private BigDecimal groupCostWarning;


	private String funds;

	private String effect_funds;

	// 余额
	private BigDecimal balance;

	private String fund_type;


	private String fund_status;

	private BigDecimal realtime_cost;

}
