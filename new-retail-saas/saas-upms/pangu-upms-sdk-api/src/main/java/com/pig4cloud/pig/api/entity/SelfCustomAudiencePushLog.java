package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "self_custom_audiences_push_log")
public class SelfCustomAudiencePushLog extends Model<SelfCustomAudiencePushLog> {

	private static final long serialVersionUID = -4846463548424420828L;

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键id")
	private Long id;

	@TableField(value = "self_id")
	@ApiModelProperty(value = "自有人群包主键ID")
	private Long selfId;

	@TableField(value = "media_type")
	@ApiModelProperty(value = "媒体类型 1头条 8广点通")
	private Integer mediaType;

	@TableField(value = "advertiser_id")
	@ApiModelProperty(value = "广告账户ID")
	private String advertiserId;

	@TableField(value = "is_deleted")
	@ApiModelProperty(value = "是否删除  0否 1是")
	private Integer isDeleted;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(value = "update_time")
	private Date updateTime;

	@TableField(value = "create_id")
	@ApiModelProperty(value = "创建人")
	private Long createId;

	@TableField(value = "update_id")
	@ApiModelProperty(value = "修改人")
	private Long updateId;


}
