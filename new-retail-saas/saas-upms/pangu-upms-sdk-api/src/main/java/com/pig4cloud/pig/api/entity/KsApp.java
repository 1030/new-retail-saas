package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.omg.CORBA.IDLType;

import java.util.Date;

/**
 * 快手应用表
 * @author  chenxiang
 * @version  2022-03-26 11:17:47
 * table: ks_app
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ks_app")
public class KsApp extends Model<KsApp>{
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 广告账户ID
	 */
	@TableField(value = "advertiser_id")
	private String advertiserId;
	/**
	 * 父游戏ID
	 */
	@TableField(value = "pgame_id")
	private Long pgameId;
	/**
	 * 子游戏ID
	 */
	@TableField(value = "game_id")
	private Long gameId;
	/**
	 * 渠道code
	 */
	@TableField(value = "chl")
	private String chl;
	/**
	 * 应用ID
	 */
	@TableField(value = "app_id")
	private Long appId;
	/**
	 * 应用标记
	 */
	@TableField(value = "app_version")
	private String appVersion;
	/**
	 * 应用名称
	 */
	@TableField(value = "app_name")
	private String appName;
	/**
	 * 应用安全扫描状态:1-扫描中，2-成功，3-失败，4-失败重试中，5—手动输入
	 */
	@TableField(value = "scan_status")
	private Integer scanStatus;
	/**
	 * 对应maindb中advertiser_monitor_info主键
	 */
	@TableField(value = "ad_id")
	private Long adId;
	/**
	 * app_icon(图片token)
	 */
	@TableField(value = "image_token")
	private String imageToken;
	/**
	 * 应用详情图
	 */
	@TableField(value = "app_detail_img")
	private String appDetailImg;
	/**
	 * 应用图标链接
	 */
	@TableField(value = "app_icon_url")
	private String appIconUrl;
	/**
	 * 应用包名
	 */
	@TableField(value = "package_name")
	private String packageName;
	/**
	 * 应用类型(1：Android 应用下载，2：Android 网页游戏，3：iOS 应用下载，4：iOS 网页游戏)
	 */
	@TableField(value = "platform")
	private Integer platform;
	/**
	 * 应用下载地址/网页游戏链接地址
	 */
	@TableField(value = "url")
	private String url;
	/**
	 * 是否接入快手广告监测 SDK(0：未接入，1：已接入)
	 */
	@TableField(value = "use_sdk")
	private Integer useSdk;
	/**
	 * app 隐私政策链接，需与 app 相关，该字段会经过审核
	 */
	@TableField(value = "app_privacy_url")
	private String appPrivacyUrl;
	/**
	 * 权限信息，请通过应用权限信息列表接口获取信息
	 */
	@TableField(value = "permission_information")
	private String permissionInformation;
	/**
	 * 真实版本号
	 */
	@TableField(value = "real_app_version")
	private String realAppVersion;
	/**
	 * 应用包大小
	 */
	@TableField(value = "package_size")
	private Long packageSize;
	/**
	 * app应用详情图片
	 */
	@TableField(value = "app_detail_image_token")
	private String appDetailImageToken;
	/**
	 * 来源类型(1:API,2:渠道后台)
	 */
	@TableField(value = "source_type")
	private Integer sourceType;
	/**
	 * 快手应用更新时间
	 */
	@TableField(value = "update_time")
	private Long updateTime;
	/**
	 * 是否删除  0否 1是
	 */
	@TableField(value = "deleted")
	private Integer deleted;
	/**
	 * 创建时间
	 */
	@TableField(value = "platform_create_time")
	private Date platformCreateTime;
	/**
	 * 修改时间
	 */
	@TableField(value = "platform_update_time")
	private Date platformUpdateTime;
	/**
	 * 创建人
	 */
	@TableField(value = "platform_create_id")
	private Long platformCreateId;
	/**
	 * 修改人
	 */
	@TableField(value = "platform_update_id")
	private Long platformUpdateId;
}

	

	
	

