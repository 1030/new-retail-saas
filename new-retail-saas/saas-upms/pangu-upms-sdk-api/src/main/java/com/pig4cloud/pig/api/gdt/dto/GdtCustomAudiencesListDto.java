package com.pig4cloud.pig.api.gdt.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

/**
 * @projectName:pig
 * @description:人群列表搜索功能
 * @author:Zhihao
 * @createTime:2020/12/10 19:54
 * @version:1.0
 */
@Setter
@Getter
public class GdtCustomAudiencesListDto {

	/**
	 * 分页信息
	 */
	private Page page;
	/**
	 * 广告账号
	 */
	private String[] accountIds;

	/**
	 * 人群Id或者名称
	 */
	private String searchName;

	/**
	 * 人群类型
	 * 号码文件人群=CUSTOMER_FILE，
	 * 拓展人群=LOOKALIKE，
	 * 用户行为人群=USER_ACTION，
	 * 地理位置人群=LBS，
	 * 关键词人群=KEYWORD，
	 * 广告人群=AD，
	 * 组合人群=COMBINE，
	 * 标签人群=LABEL
	 */
	private String type;


	/**
	 * 处理状态
	 * ：待处理=PENDING，
	 * 处理中=PROCESSING，
	 * 成功可用=SUCCESS，
	 * 错误=ERROR
	 */
	private String status;
	/**
	 * 创建时间，格式为 yyyy-MM-dd HH:mm:ss，如 2016-11-01 10:42:56
	 */
	private String sdate;
	
	
	private String edate;


	private Integer coverNumType;

}
