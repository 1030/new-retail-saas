package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum TtConvertTypeEnum {

	//转化类型
	AD_CONVERT_TYPE_ACTIVE   ("AD_CONVERT_TYPE_ACTIVE", "激活", 8),
	AD_CONVERT_SOURCE_TYPE_APP_DOWNLOAD("AD_CONVERT_TYPE_ACTIVE_REGISTER", "注册", 13),
	AD_CONVERT_TYPE_PAY  ("AD_CONVERT_TYPE_PAY","付费", 14),
	AD_CONVERT_TYPE_GAME_ADDICTION  ("AD_CONVERT_TYPE_GAME_ADDICTION","关键行为", 25);


	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;


	private Integer value;

	//构造方法
	TtConvertTypeEnum(String type, String name, Integer value){
		this.type = type;
		this.name = name;
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public Integer getValue() {
		return value;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (TtConvertTypeEnum item : TtConvertTypeEnum.values()) {
			if (String.valueOf(item.getValue()).equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static Integer valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (TtConvertTypeEnum item : TtConvertTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getValue();
			}
		}
		return null;

	}


	/**
	 * 通过type   取name
	 * @return
	 */
	public static String nameByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (TtConvertTypeEnum item : TtConvertTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}
	/**
	 * value 取name
	 * @return
	 */
	public static String valueByName(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}
		for (TtConvertTypeEnum item : TtConvertTypeEnum.values()) {
			if (String.valueOf(item.getValue()).equals(value)) {
				return item.getName();
			}
		}
		return null;

	}




	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (TtConvertTypeEnum item : TtConvertTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
