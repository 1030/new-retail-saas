package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.entity.AdCampaignStatistic;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 投放广告组数据报表 数据展示
 * 
 * @author hma
 * @date 2020-11-09
 */
@Setter
@Getter
public class AdCampaignStatisticVo extends AdCampaignStatistic
{
	private static final long serialVersionUID = 1L;
	/** 广告组名称 */
	private String campaignName;
	/** 业务类型（企业类型）：001 动游科技 */
	private String businessType;
	/**  日元算类型：BUDGET_MODE_DAY（日预算）
	 BUDGET_MODE_INFINITE（不限） */
	private String budgetMode;
	/** 组预算 */
	private BigDecimal campaignBudget;
	/** 推广目标：1 应用推广  2 销售线推广 */
	private String  adTarget;
	/** 投放类型：1 头条  2 广点通 */
	private Integer adType;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	private String status;
	/**
	 * 类型描述
	 */
	private String acTypeDesc;


	/**
	 * 投放账号
	 */
	private String servingAccount;
	/**
	 * 投放账号名称
	 */
	private String name;







}
