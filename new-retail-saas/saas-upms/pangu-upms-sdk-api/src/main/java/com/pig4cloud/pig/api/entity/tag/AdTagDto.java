package com.pig4cloud.pig.api.entity.tag;

import lombok.Data;

import java.io.Serializable;
import com.pig4cloud.pig.api.util.Page;

@Data
public class AdTagDto extends Page implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	/**
	 * 标签名称
	 */
	private String tagName;
	/**
	 * 标签颜色
	 */
	private String tagColor;


}
