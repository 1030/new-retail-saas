package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum TtConvertSourceTypeEnum {

	//转化来源类型
	AD_CONVERT_SOURCE_TYPE_NORMAL_APP_DOWNLOAD   ("AD_CONVERT_SOURCE_TYPE_NORMAL_APP_DOWNLOAD", "普通应用下载", 9),
	AD_CONVERT_SOURCE_TYPE_APP_DOWNLOAD("AD_CONVERT_SOURCE_TYPE_APP_DOWNLOAD", "应用下载API", 4),
	AD_CONVERT_SOURCE_TYPE_SDK  ("AD_CONVERT_SOURCE_TYPE_SDK","应用下载SDK", 7);

	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;


	private Integer value;

	//构造方法
	TtConvertSourceTypeEnum(String type, String name, Integer value){
		this.type = type;
		this.name = name;
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public Integer getValue() {
		return value;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (TtConvertSourceTypeEnum item : TtConvertSourceTypeEnum.values()) {
			if (String.valueOf(item.getValue()).equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static Integer valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (TtConvertSourceTypeEnum item : TtConvertSourceTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getValue();
			}
		}
		return null;

	}


	/**
	 * 通过type   取name
	 * @return
	 */
	public static String nameByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (TtConvertSourceTypeEnum item : TtConvertSourceTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}




	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (TtConvertSourceTypeEnum item : TtConvertSourceTypeEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
