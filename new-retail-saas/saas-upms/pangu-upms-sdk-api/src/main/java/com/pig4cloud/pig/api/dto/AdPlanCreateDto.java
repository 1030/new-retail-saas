package com.pig4cloud.pig.api.dto;

import com.pig4cloud.pig.api.entity.AdPlanTemp;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 广告计划创建接口请求参数
 *
 * @author hma
 * @date 2020-11-18
 */
@Setter
@Getter
public class AdPlanCreateDto extends AdPlanTemp {

	private static final long serialVersionUID = 1L;

	/**
	 * 计划状态
	 * 默认值： "enable"开启状态
	 * 允许值: "enable"开启,"disable"关闭
	 */
	private String operation;

	/**
	 * 广告
	 */
	private Long  advertiserId;

	/**
	 * 价格区间
	 */
	private String launchPrice;

	/**
	 * 为空不是临时，从正式表来的（此处用来区分是否是否增加[]）
	 */
	private Boolean copyFlag = false;

	/**
	 * 新营销链路监测链接移动到广告计划了,多个用逗号分割
	 * 20220406 联调转化接口下线改为事件管理，此字段非必填
	 */
	private String actionTrackUrl;

	/**
	 * 监测链接组ID
	 */
	private Long trackGroupId;
	/**
	 * 监测链接组类型
	 */
	private String trackGroupType;


}
