/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Objects;

/**
 * 素材库
 * @date 2021-06-19 16:12:13
 */
@Data
@TableName("ad_material")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "素材库")
public class AdMaterial extends Model<AdMaterial> {
private static final long serialVersionUID = 1L;
    /**
     * 主键ID
     */
	@TableId(value="id",type= IdType.AUTO)
    @ApiModelProperty(value="主键ID")
    private Long id;
    /**
     * 素材名称
     */
    @ApiModelProperty(value="素材名称")
    private String name;
    /**
     * 素材类型（1：视频，2：图片）
     */
    @ApiModelProperty(value="素材类型（1：视频，2：图片）")
    private Integer type;
	@TableField(exist = false)
	private Integer typeName;
	public String getTypeName() {
		if(Objects.isNull(typeName)){
			return "";
		}
		switch (typeName){
			case 1:
				return "视频";
			case 2:
				return "图片";
			default:
				return "";
		}
	}
	/**
     * 文件名称
     */
    @ApiModelProperty(value="文件名称")
    private String fileName;
    /**
     * 文件访问地址
     */
    @ApiModelProperty(value="文件访问地址")
    private String fileUrl;
    /**
     * 真实路径
     */
    @ApiModelProperty(value="真实路径")
    private String realPath;
    /**
     * 素材封面展示地址
     */
    @ApiModelProperty(value="素材封面展示地址")
    private String imageUrl;
    /**
     * 主游戏ID
     */
    @ApiModelProperty(value="主游戏ID")
    private Integer mainGameId;
	/**
	 * 子游戏ID
	 */
	@ApiModelProperty(value="子游戏ID")
	private Integer gameId;
	/**
	 * 主游戏名称
	 */
	@TableField(exist = false)
	private String mainGameName;
	/**
	 * 子游戏名称
	 */
	@TableField(exist = false)
    private String gameName;
    /**
     * 创意者
     */
    @ApiModelProperty(value="创意者")
    private Integer creatorId;
	/**
	 * 创意者名称
	 */
	@TableField(exist = false)
	private String creatorName;
    /**
     * 制作者
     */
    @ApiModelProperty(value="制作者")
    private Integer makerId;
	/**
	 * 制作者名称
	 */
	@TableField(exist = false)
	private String makerName;
    /**
     * 制作类型（1：原创，2：原创衍生，3：抄本，4：抄本衍生）
     */
    @ApiModelProperty(value="制作类型（1：原创，2：原创衍生，3：抄本，4：抄本衍生）")
    private Integer makeType;
	/**
	 * 制作类型名称
	 */
	@TableField(exist = false)
	private String makeTypeName;

	public String getMakeTypeName() {
		if(Objects.isNull(makeTypeName)){
			return "";
		}
		switch (makeType){
			case 1:
				return "原创";
			case 2:
				return "原创衍生";
			case 3:
				return "抄本";
			case 4:
				return "抄本衍生";
			default:
				return "";
		}
	}


	/**
     * md5值
     */
    @ApiModelProperty(value="md5值")
    private String md5;
    /**
     * 素材宽
     */
    @ApiModelProperty(value="素材宽")
    private String width;
    /**
     * 素材高
     */
    @ApiModelProperty(value="素材高")
    private String height;
    /**
     * 尺寸类型：（1:横屏视频，2:竖屏视频，3:小图，4:横版大图，5:竖版大图，6:gif图，7:卡片主图，0:其他）
     */
    @ApiModelProperty(value="尺寸类型：（1:横屏视频，2:竖屏视频，3:小图，4:横版大图，5:竖版大图，6:gif图，7:卡片主图，0:其他）")
    private Integer screenType;
	@TableField(exist = false)
	private String screenTypeName;

	public String getScreenTypeName() {
		if(Objects.isNull(screenType)){
			return "";
		}
		switch (screenType){
			case 1:
				return "横屏";
			case 2:
				return "竖屏";
			default:
				return "";
		}
	}

	/**
     * 视频大小
     */
    @ApiModelProperty(value="视频大小")
    private String size;
    /**
     * 素材格式
     */
    @ApiModelProperty(value="素材格式")
    private String format;
    /**
     * 素材时长
     */
    @ApiModelProperty(value="素材时长")
    private String duration;
    /**
     * 标签
     */
    @ApiModelProperty(value="标签")
    private String labels;
    /**
     * 状态：（0：启用，1：禁用）
     */
    @ApiModelProperty(value="状态：（0：启用，1：禁用）")
    private Integer status;
    /**
     * 访问权限(0公开、1本人、2小组)
     */
    @ApiModelProperty(value="访问权限(0公开、1本人、2小组)")
    private Integer readpop;

	@TableField(exist = false)
	private String statusName;

	public String getStatusName() {
		if(Objects.isNull(status)){
			return "";
		}
		switch (status){
			case 1:
				return "横屏";
			case 2:
				return "竖屏";
			default:
				return "";
		}
	}

	/**
     * 是否删除：0正常，1删除
     */
    @ApiModelProperty(value="是否删除：0正常，1删除")
    private Integer isDelete;
    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;
    /**
     * 卖点ID
     */
    @ApiModelProperty(value="卖点ID")
    private Integer sellingPointId;
    /**
     * 素材来源：0：盘古；1：炎帝
     */
    @ApiModelProperty(value="素材来源：0：盘古；1：炎帝")
    private Integer origin;
    /**
     * 业务ID
     */
    @ApiModelProperty(value="业务ID")
    private Integer originId;
    /**
     * 禁用理由
     */
    @ApiModelProperty(value="禁用理由")
    private String disableReason;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private String createUser;
	@TableField(exist = false)
    private String createUserName;

	/**
     * 修改时间
     */
    @ApiModelProperty(value="修改时间")
    private Date updateTime;
    /**
     * 修改人
     */
    @ApiModelProperty(value="修改人")
    private String updateUser;

	@TableField(exist = false)
	private String updateUserName;
    }
