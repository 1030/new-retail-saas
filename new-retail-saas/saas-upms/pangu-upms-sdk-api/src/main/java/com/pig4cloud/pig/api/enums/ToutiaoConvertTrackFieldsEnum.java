package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoConvertTrackFieldsEnum {

	//平台  1 ANDROID/ 2 IOS/ 3 PC/4 IPAD/ 5 WAP
	ADVERTISER_ID                  ("advertiser_id"                   ,      "广告账号"             ),
	NAME                           ("name"                            ,      "转化名称"             ),
	APP_ID                         ("app_id"                          ,      "APPID"                ),
	DOWNLOAD_URL                   ("download_url"                    ,      "下载链接"             ),
	DEEP_EXTERNAL_ACTION           ("deep_external_action"            ,      "转化目标"             ),

	ACTION_TRACK_URL               ("action_track_url"                ,     "点击-监测链接"         ),
	DISPLAY_TRACK_URL              ("display_track_url"               ,     "展示-监测链接"         ),
	VIDEO_PLAY_EFFECTIVE_TRACK_URL ("video_play_effective_track_url"  ,     "有效播放-监测链接"     ),
	VIDEO_PLAY_TRACK_URL           ("video_play_track_url"            ,     "视频播放-监测链接"     ),
	VIDEO_PLAY_DONE_TRACK_URL      ("video_play_done_track_url"       ,     "视频播完-监测链接"     );



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoConvertTrackFieldsEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoConvertTrackFieldsEnum item : ToutiaoConvertTrackFieldsEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoConvertTrackFieldsEnum item : ToutiaoConvertTrackFieldsEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoConvertTrackFieldsEnum item : ToutiaoConvertTrackFieldsEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
