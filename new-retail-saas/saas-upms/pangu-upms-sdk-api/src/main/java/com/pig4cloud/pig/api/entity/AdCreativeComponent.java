package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


/**
 * @Description
 * @Author chengang
 * @Date 2021/8/24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_creative_component")
public class AdCreativeComponent extends Model<AdCreativeComponent> {


	private static final long serialVersionUID = -8035599394273604235L;

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键id")
	private Long id;

	@TableField(value = "advertiser_id")
	@ApiModelProperty(value = "广告账户")
	private String advertiserId;

	@TableField(value = "component_id")
	@ApiModelProperty(value = "组件ID")
	@JSONField(name = "component_id")
	private String componentId;

	@TableField(value = "component_name")
	@ApiModelProperty(value = "组件名称")
	@JSONField(name = "component_name")
	private String componentName;

	@TableField(value = "component_type")
	@JSONField(name = "component_type")
	@ApiModelProperty(value = "组件类型，基础组件仅支持推广卡片(PROMOTION_CARD)")
	private String componentType;

	@TableField(value = "third_create_time")
	@JSONField(name = "create_time")
	@ApiModelProperty(value = "创建时间，格式 2020-12-25 15:12:08 ")
	private String thirdCreateTime;

	@TableField(value = "status")
	@JSONField(name = "status")
	@ApiModelProperty(value = "组件审核状态，PASS：通过 UNDER：审核中 REJECT ：未通过")
	private String status;

	@TableField(value = "component_data")
	@JSONField(name = "component_data")
	@ApiModelProperty(value = "组件详细信息JSON")
	private String componentData;

	@TableField(value = "is_deleted")
	@ApiModelProperty(value = "是否删除  0否 1是")
	private Integer isDeleted;

	@ApiModelProperty(value = "创建时间")
	@TableField(value = "create_time")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	@TableField(value = "update_time")
	private Date updateTime;

	@TableField(value = "create_id")
	@ApiModelProperty(value = "创建人")
	private Long createId;

	@TableField(value = "update_id")
	@ApiModelProperty(value = "修改人")
	private Long updateId;

	@TableField(exist = false)
	private String imageUrl ;



}
