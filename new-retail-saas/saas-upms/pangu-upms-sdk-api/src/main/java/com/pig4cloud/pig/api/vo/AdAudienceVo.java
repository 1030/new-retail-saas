package com.pig4cloud.pig.api.vo;

import lombok.Data;

/**
 * @Description
 * @Author chengang
 * @Date 2021/6/24
 */
@Data
public class AdAudienceVo {

	//广告计划ID
	private String adid;

	//返点后消耗
	private Double cost = 0.00;
	//曝光量
	private Long showNum = 0L;
	//点击人数
	private Long clickNum = 0L;
	//新增设备注册数
	private Long usrNameNum = 0L;
	// 新增设备付费数
	private Long newUuidNum = 0L;
	// 新增设备在次日有登录行为的设备数
	private Long retention2 = 0L;

}
