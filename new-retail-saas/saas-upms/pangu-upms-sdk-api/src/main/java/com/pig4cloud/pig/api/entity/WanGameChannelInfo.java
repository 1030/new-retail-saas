package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * WanGameChannelInfo
 * @author  chengang
 * @version  2021-12-15 14:54:38
 * table: wan_game_channel_info
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "wan_game_channel_info")
public class WanGameChannelInfo extends Model<WanGameChannelInfo>{

	//columns START
			//id
			@TableId(value = "id",type = IdType.AUTO)
			private Long id;
			

			//游戏id
			@TableField(value = "gameid")
			private Integer gameid;
			

			//游戏包渠道编码（第三级）
			@TableField(value = "chl")
			private String chl;
			

			//渠道应用id
			@TableField(value = "appId")
			private String appId;
			

			//渠道应用名称
			@TableField(value = "appName")
			private String appName;
			

			//推广渠道编码（第二级）
			@TableField(value = "channel")
			private String channel;
			

			//平台类型:0-3399平台,1-头条,2-百度,3-新数,4-公会,7-UC,8-广点通
			@TableField(value = "platform")
			private Integer platform;
			

			//是否上报0不上报1上报
			@TableField(value = "isup")
			private Integer isup;
			

			//缓存状态0未缓存，1已经缓存
			@TableField(value = "cacheStatus")
			private Integer cacheStatus;
			

			//创建时间
			@TableField(value = "createtime")
			private Date createtime;
			

			//对应渠道包内容描述
			@TableField(value = "content")
			private String content;
			

			//备注
			@TableField(value = "remark")
			private String remark;
			

			//0 sdk支付，1苹果内置支付
			@TableField(value = "paytype")
			private Integer paytype;
			

			//广告主账户
			@TableField(value = "adAccount")
			private String adAccount;
			

			//渠道计划数
			@TableField(value = "plannum")
			private Integer plannum;
			

			//渠道创意数
			@TableField(value = "ideanum")
			private Integer ideanum;
			

			//1.api   2.sdk
			@TableField(value = "rtype")
			private Integer rtype;

			//推广类型（1信息流 2短视频）
			@TableField(value = "spread_type")
			private Integer spreadType;

			//结算类型(KOL(一口价) KOL(CPM按次))
			@TableField(value = "settle_type")
			private String settleType;
			

	//columns END 数据库字段结束
	

	
}

	

	
	

