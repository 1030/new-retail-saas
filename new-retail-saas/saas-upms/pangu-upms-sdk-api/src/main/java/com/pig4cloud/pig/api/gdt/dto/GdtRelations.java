package com.pig4cloud.pig.api.gdt.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GdtRelations implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9161104808320523719L;

	private String account_id;
	
	private String corporation_name;
	
	private String account_type;

}
