package com.pig4cloud.pig.api.dto;

import com.pig4cloud.pig.api.entity.AdPlanTemp;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * 新建广告计划 dto
 *
 * @author hma
 * @date 2020-11-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AdPlanTempDto extends AdPlanTemp {

	private static final long serialVersionUID = 1L;

	/**
	 * 广告账号id
	 */
	@NotNull(message = "广告账号ID不能为空", groups = {ActionCategory.class, ActionKeyword.class, InterestCategory.class, ActionInterestKeyword.class})
	private Long advertiserId;

	/**
	 * 行为兴趣的类目或关键词ID
	 */
	@NotNull(message = "行为兴趣的类目或关键词ID不能为空", groups = {ActionInterestKeyword.class})
	private Long actionInterestId;

	private String ads;

	/**
	 * 价格区间
	 */
	private String launchPrice;

	/**
	 * 判断新增还是更新：0 新增，1 更新
	 */
	//private String idStr;

	/**
	 * 行为关键词
	 */
	@Length(min = 1, message = "行为关键词长度不能小于1", groups = {ActionKeyword.class})
	private String queryWords;

	/**
	 * 查询类型，类目还是关键词，允许值：CATEGORY（类目）、KEYWORD（关键词）
	 */
	@NotBlank(message = "查询类型不能为空", groups = {ActionInterestKeyword.class})
	private String searchType;

	/**
	 * 查询目标，兴趣还是行为，允许值：ACTION（行为）、INTEREST（兴趣）
	 */
	@NotBlank(message = "查询目标不能为空", groups = {ActionInterestKeyword.class})
	private String targetType;

	/**
	 * 行为场景，E-COMMERCE、NEWS、APP、SEARCH
	 */
	private Collection<String> actionScenes;

	/**
	 * 行为天数，7、15、30、60、90、180、365
	 */
	@Range(min = 7, max = 365, message = "行为天数不能为空", groups = {ActionCategory.class, ActionKeyword.class})
	private Integer actionDays1;

	private String advertiserIds;

	/**
	 * 媒体编码
	 */
	private String mediaCode;
	/**
	 * 广告计划名称
	 */
	private String adName;

	public interface ActionCategory {
	}

	public interface ActionKeyword {
	}

	public interface InterestCategory {
	}

	public interface InterestKeyword {
	}

	public interface ActionInterestKeyword {
	}

}
