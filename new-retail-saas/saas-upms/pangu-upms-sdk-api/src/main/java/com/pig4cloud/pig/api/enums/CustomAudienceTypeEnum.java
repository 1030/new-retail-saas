/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;


@Getter
@RequiredArgsConstructor
public enum CustomAudienceTypeEnum {
	CUSTOMER_FILE("CUSTOMER_FILE",  "号码文件人群"),
	LOOKALIKE("LOOKALIKE",  "拓展人群"),
	USER_ACTION("USER_ACTION",  "用户行为人群"),
	LBS("LBS",  "地理位置人群"),
	KEYWORD("KEYWORD",  "关键词人群"),
	AD("AD",  "广告人群"),
	COMBINE("COMBINE",  "组合人群"),
	LABEL("LABEL",  "标签人群");

	private final String key;
	private final String name;

	public static String getNameByKey(String key){
		if (StringUtils.isBlank(key)){
			return null;
		}
		for (CustomAudienceTypeEnum item : CustomAudienceTypeEnum.values()) {
			if (key.equals(item.getKey())) {
				return item.getName();
			}
		}
		return null;

	}

}
