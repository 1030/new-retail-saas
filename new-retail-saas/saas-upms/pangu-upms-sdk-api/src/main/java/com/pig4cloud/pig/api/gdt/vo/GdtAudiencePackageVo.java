package com.pig4cloud.pig.api.gdt.vo;

import com.pig4cloud.pig.api.gdt.entity.GdtAudiencePackage;
import lombok.Data;

@Data
public class GdtAudiencePackageVo extends GdtAudiencePackage {
	private int adPlanCount;

	private String advertiserName;
}
