package com.pig4cloud.pig.api.dto;

import com.pig4cloud.pig.api.entity.AdAssetsEvent;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 广告资产表
 * @author  chenxiang
 * @version  2022-04-01 15:02:28
 * table: ad_assets
 */
@Data
public class AdAssetsRes implements Serializable {
	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 资产ID
	 */
	private Long assetId;
	/**
	 * 广告账户ID
	 */
	private String advertiserId;
	/**
	 * 广告账户名称
	 */
	private String advertiserName;
	/**
	 * 资产类型 THIRD_EXTERNAL 三方落地页、APP 应用、QUICK_APP 快应用
	 */
	private String assetType;
	/**
	 * 应用包ID
	 */
	private String packageId;
	/**
	 * 包名
	 */
	private String packageName;
	/**
	 * appId
	 */
	private String appCloudId;
	/**
	 * 应用名
	 */
	private String appName;
	/**
	 * 应用类型，允许值：IOS、Android
	 */
	private String appType;
	/**
	 * 下载地址
	 */
	private String downloadUrl;
	/**
	 * 应用资产名
	 */
	private String assetName;
	/**
	 * 事件名
	 */
	private String eventCnName;
	/**
	 * 来源(1:API,2:渠道后台)
	 */
	private Integer soucreStatus;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 创建人
	 */
	private Long createId;
	/**
	 * 修改人
	 */
	private Long updateId;
	/**
	 * 事件列表
	 */
	private List<AdAssetsEvent> eventList;
}


