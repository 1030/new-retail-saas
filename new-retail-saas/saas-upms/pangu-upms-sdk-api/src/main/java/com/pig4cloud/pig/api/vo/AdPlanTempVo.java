package com.pig4cloud.pig.api.vo;

import com.pig4cloud.pig.api.entity.AdPlanTemp;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 广告计划临时数据展示
 * 
 * @author hma
 * @date 2020-11-18
 */
@Setter
@Getter
public class AdPlanTempVo extends AdPlanTemp
{
	private static final long serialVersionUID = 1L;
	/**
	 * 投放账号
	 */
	private String name;
	/**
	 * 推广组名称
	 */
	private String campaignName;
	/**
	 * 广告组预算
	 */
	private BigDecimal campaignBudget;
	/**
	 * 推广目标
	 */
	private String landingType;

	private Long  advertiserId;

	/**
	 * 判断新增还是更新：1 更新，0或其他 新增
	 */
	//private String idStr;
   private String packageName;
   private String description;
   private String landingTypeName;
   private String deliveryRangeName;

}
