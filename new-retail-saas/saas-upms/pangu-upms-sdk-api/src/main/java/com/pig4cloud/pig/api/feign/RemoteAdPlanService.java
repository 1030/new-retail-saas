/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.api.feign;

import com.pig4cloud.pig.api.entity.AdPlan;
import com.pig4cloud.pig.api.vo.PlanGdtAttrAnalyseReportVo;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/28
 */
@FeignClient(contextId = "remoteAdPlanService", value = ServiceNameConstants.UPMS_SDK_SERVICE)
public interface RemoteAdPlanService {
	/**
	 * 根据广告ID获取广告AdPlan
	 * @param adids
	 * @return
	 */
	@PostMapping("/plan/getByAdids")
	R<List<AdPlan>> getByAdids(@RequestBody String adids);
	@PostMapping("/plan/getGdtByAdids")
	R<List<PlanGdtAttrAnalyseReportVo>> getGdtByAdids(@RequestBody String adids);
 }
