package com.pig4cloud.pig.api.gdt.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 广点通-推广计划表 参数 dto
 * 
 * @author hma
 * @date 2020-12-03
 */
@Setter
@Getter
public class GdtCampaignDto extends Page
{
	private static final long serialVersionUID = 1L;
	
	private String accountId;
	/**
	 * 推广组名称
	 */
	private String campaignName;
	private String promotedObjectType;





}
