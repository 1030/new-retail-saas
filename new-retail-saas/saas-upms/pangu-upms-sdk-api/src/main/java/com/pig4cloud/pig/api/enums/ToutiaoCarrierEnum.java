package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

public enum ToutiaoCarrierEnum {

	//受众运营商，允许值: MOBILE 移动/UNICOM 联通 / TELCOM 电信
	MOBILE  (  "MOBILE"   ,      "移动"),
	UNICOM  (  "UNICOM"   ,      "联通"),
	TELCOM  (  "TELCOM"   ,      "电信");



	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	ToutiaoCarrierEnum(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 * @return
	 */
	public static String typeByValue(String value){
		if (StringUtils.isBlank(value)){
			return null;
		}

		for (ToutiaoCarrierEnum item : ToutiaoCarrierEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 * @return
	 */
	public static String valueByType(String type){
		if (StringUtils.isBlank(type)){
			return null;
		}

		for (ToutiaoCarrierEnum item : ToutiaoCarrierEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	public static boolean containsType(String type){
		if (StringUtils.isBlank(type)){
			return false;
		}

		for (ToutiaoCarrierEnum item : ToutiaoCarrierEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
