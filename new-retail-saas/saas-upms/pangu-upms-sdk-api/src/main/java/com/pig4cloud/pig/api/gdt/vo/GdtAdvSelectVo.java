package com.pig4cloud.pig.api.gdt.vo;

import java.io.Serializable;
import java.util.TreeSet;

import lombok.Data;


@Data
public class GdtAdvSelectVo  implements Serializable, Comparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3309147144008041159L;
	
	
	private String accountId;
	
	private String name;
	
	private TreeSet<GdtAdvSelectVo> childrenList = new TreeSet<>();
	
	
	@Override
	public int compareTo(Object o) {
	    if (!(o instanceof GdtAdvSelectVo)) {	        
	        throw new RuntimeException("不是GdtAdvSelectVo对象");
	    }
	    GdtAdvSelectVo p = (GdtAdvSelectVo) o;
	    
	    return this.accountId.compareTo(p.accountId);
	}

}
