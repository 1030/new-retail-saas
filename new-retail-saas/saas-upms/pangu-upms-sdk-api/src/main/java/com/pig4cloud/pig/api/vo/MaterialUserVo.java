package com.pig4cloud.pig.api.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zjz
 */
@Data
public class MaterialUserVo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 素材类型（1：制作者，2：创意者）
	 */
	private String ptype;

	/**
	 * 素材类型（1：视频，2：图片）
	 */
	private String type;

	/**
	 * 状态：（0：启用，1：禁用）
	 */
	private String status;

	/**
	 * 当前登录用户ID
	 */
	private String loginUserId;
	/**
	 * 是否为收藏：true：false
	 */
	private String isCollect;


	private String isDelete;


	private String readpop;


}
