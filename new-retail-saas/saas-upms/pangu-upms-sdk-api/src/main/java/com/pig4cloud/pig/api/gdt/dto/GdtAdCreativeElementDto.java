package com.pig4cloud.pig.api.gdt.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 广点通广告创意参数dto
 * 
 * @author hma
 * @date 2020-12-11
 */
@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class GdtAdCreativeElementDto
{
	private static final long serialVersionUID = 1L;


}
