/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 素材推送箱表
 * @author chengj
 */
@Data
@TableName("ad_material_push_box")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "素材推送箱")
public class AdMaterialPushBox extends Model<AdMaterialPushBox> {
	
	private static final long serialVersionUID = 1168885469216750902L;
	
	/**
     * 主键
     */
	@TableId(value="id",type= IdType.AUTO)
    @ApiModelProperty(value="主键")
    private Integer id;
    /**
     * 素材ID
     */
    @ApiModelProperty(value="素材ID")
    private Long materialId;
    /**
     * 平台ID（1：头条，2：广点通）
     */
    @ApiModelProperty(value="平台ID（1：头条，2：广点通）")
    private Integer platformId;
    /**
     * 平台广告账户
     */
    @ApiModelProperty(value="平台广告账户")
    private String advertiserId;
   
    /**
     * 推送状态
     */
    @ApiModelProperty(value="推送状态（ 0.未推送 1.推送中 2.推送完成 3.推送失败）")
    private Integer pushStatus;
    
    /**
     * 失败原因
     */
    @ApiModelProperty(value="失败原因")
    private String failMsg;
    
    /**
     * 是否删除
     */
    @ApiModelProperty(value="是否删除（0正常，1删除）")
    private Integer isDeleted;
    
    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private String createUser;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    
    /**
     * 修改
     */
    @ApiModelProperty(value="修改时间")
    private Date updateTime;
    /**
     * 修改人
     */
    @ApiModelProperty(value="修改人")
    private String updateUser;
    
    }
