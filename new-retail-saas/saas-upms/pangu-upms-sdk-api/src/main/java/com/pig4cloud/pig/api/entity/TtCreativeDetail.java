package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * 广告创意表 ad_creative
 *
 * @author hma
 * @date 2020-11-10
 */
@Data
@Accessors(chain = true)
@TableName("tt_creative_detail")
public class TtCreativeDetail extends Model<TtCreativeDetail> implements Serializable {

	private static final long serialVersionUID = 6218785883826106694L;

	@JSONField(name = "advertiser_id")
	private String advertiserId;

	@JSONField(name = "ad_id")
	@TableId(value="ad_id")
	private String adId;

	@JSONField(name = "modify_time")
	private String modifyTime;

	@JSONField(name = "inventory_type")
	private String inventoryType;

	@JSONField(name = "smart_inventory")
	private Integer smartInventory;

	@JSONField(name = "scene_inventory")
	private String sceneInventory;

	@JSONField(name = "creative_material_mode")
	private String creativeMaterialMode;

	@JSONField(name = "procedural_package_id")
	private BigInteger proceduralPackageId;

	@JSONField(name = "procedural_package_version")
	private BigInteger proceduralPackageVersion;

	@JSONField(name = "is_presented_video")
	private Integer isPresentedVideo;

	@JSONField(name = "generate_derived_ad")
	private String generateDerivedAd;

	@JSONField(name = "image_list")
	private String imageList;

	@JSONField(name = "title_list")
	private String titleList;

	@JSONField(name = "source")
	private String source;

	@JSONField(name = "ies_core_user_id")
	private String iesCoreUserId;

	@JSONField(name = "is_feed_and_fav_see")
	private Integer isFeedAndFavSee;

	@JSONField(name = "creative_auto_generate_switch")
	private Integer creativeAutoGenerateSwitch;

	@JSONField(name = "app_name")
	private String appName;

	@JSONField(name = "sub_title")
	private String subTitle;

	@JSONField(name = "web_url")
	private String webUrl;

	@JSONField(name = "action_text")
	private String actionText;

	@JSONField(name = "playable_url")
	private String playableUrl;

	@JSONField(name = "is_comment_disable")
	private Integer isCommentDisable;

	@JSONField(name = "close_video_detail")
	private Integer closeVideoDetail;

	@JSONField(name = "creative_display_mode")
	private String creativeDisplayMode;

	@JSONField(name = "advanced_creative_type")
	private String advancedCreativeType;

	@JSONField(name = "advanced_creative_title")
	private String advancedCreativeTitle;

	@JSONField(name = "phone_number")
	private String phoneNumber;

	@JSONField(name = "")
	private String buttonText;

	@JSONField(name = "form_url")
	private String formUrl;

	@JSONField(name = "commerce_cards")
	private String commerceCards;

	@JSONField(name = "third_industry_id")
	private String thirdIndustryId;

	@JSONField(name = "ad_keywords")
	private String adKeywords;

	@JSONField(name = "track_url")
	private String trackUrl;

	@JSONField(name = "action_track_url")
	private String actionTrackUrl;

	@JSONField(name = "video_play_effective_track_url")
	private String videoPlayEffectiveTrackUrl;

	@JSONField(name = "video_play_done_track_url")
	private String videoPlayDoneTrackUrl;

	@JSONField(name = "video_play_track_url")
	private String videoPlayTrackUrl;

	@JSONField(name = "track_url_send_type")
	private String trackUrlSendType;

	@JSONField(name = "promotion_card")
	private String promotionCard;

	@JSONField(name = "creatives")
	private String creatives;

	@JSONField(name = "abstract_list")
	private String abstractList;

	@JSONField(name = "is_smart_title")
	private String isSmartTitle;

	@JSONField(name = "ad_category")
	private String adCategory;

	@JSONField(name = "dynamic_creative_switch")
	private String dynamicCreativeSwitch;

	@JSONField(name = "sub_link_id_list")
	private String subLinkIdList;

	@JSONField(name = "external_url")
	private String externalUrl;

	@JSONField(name = "component_info")
	private String componentInfo;

	@JSONField(name = "creative_list")
	private String creativeList;

	@JSONField(name = "creative")
	private String creative;

}
