/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @广告账户
 * @author john
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="tt_advertiser")
public class Advertising extends Model<Advertising> {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "广告账户id")
	@TableId(value = "advertiser_id")
	private String advertiserId;

	/**
	 * 平台ID：1头条，8广点通，10快手
	 */
	@TableField(value = "platform_id")
	private Integer platformId;

	/**
	 * 广告账户名称
	 */
	@NotBlank(message = "广告账户名称不能为空")
	@ApiModelProperty(value = "广告账户名称")
	private String name;

	/**
	 * 账户总余额(单位元)
	 */
	@ApiModelProperty(value = "账户总余额(单位元)")
	private BigDecimal balance;


	private String servingAccount;

	/**
	 * 账户可用总余额(单位元)
	 */
	@ApiModelProperty(value = "账户可用总余额(单位元)")
	@TableField(value = "valid_balance")
	private BigDecimal validBalance;
	
	/**
	 * 现金余额(单位元)
	 */
	@ApiModelProperty(value = "现金余额(单位元)")
	private BigDecimal cash;
	
	/**
	 * 现金可用余额(单位元)
	 */
	@ApiModelProperty(value = "现金可用余额(单位元)")
	@TableField(value = "valid_cash")
	private BigDecimal validCash;
	
	
	/**
	 * 赠款余额(单位元)
	 */
	@ApiModelProperty(value = "赠款余额(单位元)")
	@TableField(value = "`grant`")
	private BigDecimal grant;
	
	/**
	 * 赠款可用余额(单位元)
	 */
	@ApiModelProperty(value = "赠款可用余额(单位元)")
	@TableField(value = "valid_grant")
	private BigDecimal validGrant;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField( fill = FieldFill.INSERT)
	private Date createtime;
	
	
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField( fill = FieldFill.UPDATE)
	private Date updatetime;


	/**
	 * 对应tt_accesstoken 的ad_account
	 */
	@ApiModelProperty(value = "对应tt_accesstoken 的ad_account")
	private String housekeeper;

	/**
	 * 预算，单位：元； 精度：小数点后两位；举例：100.01。\r\n当预算类型为不限，返回的预算为0.0
	 */
	@ApiModelProperty(value = "预算，单位：元； 精度：小数点后两位；举例：100.01。\\r\\n当预算类型为不限，返回的预算为0.0")
	private BigDecimal budget;

	/**
	 * 预算类型\r\nBUDGET_MODE_DAY（日预算）\r\nBUDGET_MODE_INFINITE（不限）
	 */
	@ApiModelProperty(value = "预算类型\\r\\nBUDGET_MODE_DAY（日预算）\\r\\nBUDGET_MODE_INFINITE（不限）")
	@TableField(value = "budget_mode")
	private String budgetMode;

	@TableField(value = "deleted")
	private Integer deleted;
}
