package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 投放版位
 * @Author chengang
 * @Date 2022/5/16
 */
public enum GdtSiteSetEnum {
	/**
	 * 投放版位
	 */
	SITE_SET_MOMENTS("SITE_SET_MOMENTS" ," 微信朋友圈"),
	SITE_SET_WECHAT("SITE_SET_WECHAT" ," 微信公众号与小程序"),
	SITE_SET_KANDIAN("SITE_SET_KANDIAN" ," 腾讯看点"),
	SITE_SET_QQ_MUSIC_GAME("SITE_SET_QQ_MUSIC_GAME" ," QQ、腾讯音乐及游戏"),
	SITE_SET_MOBILE_INNER("SITE_SET_MOBILE_INNER" ," QQ、腾讯看点、腾讯音乐（待废弃）"),
	SITE_SET_TENCENT_NEWS("SITE_SET_TENCENT_NEWS" ," 腾讯新闻"),
	SITE_SET_TENCENT_VIDEO("SITE_SET_TENCENT_VIDEO" ," 腾讯视频"),
	SITE_SET_MOBILE_UNION("SITE_SET_MOBILE_UNION" ," 优量汇"),
	SITE_SET_MOBILE_YYB("SITE_SET_MOBILE_YYB" ," 应用宝"),
	SITE_SET_PCQQ("SITE_SET_PCQQ" ," PC QQ、QQ 空间、腾讯音乐"),
	SITE_SET_MINI_GAME_WECHAT("SITE_SET_MINI_GAME_WECHAT" ," 微信小游戏"),
	SITE_SET_MINI_GAME_QQ("SITE_SET_MINI_GAME_QQ" ," QQ 小游戏"),
	SITE_SET_MOBILE_GAME("SITE_SET_MOBILE_GAME" ," App 游戏"),
	SITE_SET_QBSEARCH("SITE_SET_QBSEARCH" ," QQ浏览器、应用宝版位"),
	SITE_SET_CHANNELS("SITE_SET_CHANNELS" ," 微信视频号"),
	SITE_SET_WECHAT_SEARCH("SITE_SET_WECHAT_SEARCH" ," 微信搜一搜"),
	;
	/**
	 * 类型
	 */
	private String type;
	/**
	 * 名称
	 */
	private String name;

	public static List<String> getAllType(){
		List<String> result = new ArrayList<>();
		for (GdtSiteSetEnum item : GdtSiteSetEnum.values()) {
			result.add(item.getType());
		}
		return result;
	}

	public static String getNameByType(String type) {
		if (StringUtils.isBlank(type)) {
			return null;
		}

		for (GdtSiteSetEnum item : GdtSiteSetEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	GdtSiteSetEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
