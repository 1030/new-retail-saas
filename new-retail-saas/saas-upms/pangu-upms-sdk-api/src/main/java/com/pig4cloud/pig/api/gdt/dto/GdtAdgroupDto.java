package com.pig4cloud.pig.api.gdt.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description:广告组设置请求参数
 * @author:yk
 * @createTime:2020/12/14
 */
@Setter
@Getter
public class GdtAdgroupDto extends Page {
	/** 开始时间*/
	private String sdate;
	/** 结束时间*/
	private String edate;

	private String adgroupName;
	/**
	 * 多个广告主id
	 */
	private String[] accountIds;

	private List<String> accountList;

	/*客户设置的状态*/
	private String configuredStatus;

	/** 广告 id */
	private Long adgroupId;

	/** 广告出价，单位为分，出价限制：
	 CPC 出价限制：介于 10 分-10,000 分之间（0.1 元-100 元，单位为人民币）
	 CPM 出价限制：介于 150 分-99,900 之间（1.5 元-999 元，单位为人民币）
	 CPA 出价限制：介于 100 分-50,000 分之间（1 元-500 元，单位为人民币）
	 oCPC/oCPM 出价限制：介于 10 分-500,000 分之间（0.1 元-5000 元，单位为人民币）
	 */
	private BigDecimal bidAmount;

	/**
	 * 出价方式
	 */
	private String bidMode;

	/** 广告组日预算，单位为分，设置为 0 表示不设预算（即不限） */
	private BigDecimal dailyBudget;

	/** 日期*/
	private String date;
	
	
	/**
	 *   忽略关闭预警
	 */
	private String ignoreWarning;

}
