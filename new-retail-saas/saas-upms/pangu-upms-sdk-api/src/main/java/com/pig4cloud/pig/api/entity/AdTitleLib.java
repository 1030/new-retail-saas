package com.pig4cloud.pig.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * ad_title_lib
 *
 * @author nml
 * @date 2020-11-07 17:22:45
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "ad_title_lib")
public class AdTitleLib extends Model<AdTitleLib> implements Serializable {
	/*
	 *  唯一id:标题id
	 * */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "id")
	private Long id;

	/**
	 * 标题名称
	 */
	@ApiModelProperty(value = "标题名称")
	@TableField(value = "title_name")
	private String titleName;

	/**
	 * 平台id，0：通用；1：头条；7：UC；8：广点通；9：百度，...
	 */
	@ApiModelProperty
	@TableField(value = "platform_id")
	private Integer platformId;

	/**
	 * 项目ID, 表示主游戏ID
	 */
	@ApiModelProperty
	@TableField(value = "project_id")
	private Integer projectId;

	/**
	 * 是否删除：0正常，1删除
	 */
	@ApiModelProperty
	@TableField(value = "isdelete")
	private Integer isdelete;

	/**
	 * 备注
	 */
	@ApiModelProperty
	@TableField(value = "remark")
	private String remark;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty
	@TableField(value = "createtime", fill = FieldFill.INSERT)
	private Date createtime;

	/**
	 * 创建人
	 */
	@ApiModelProperty
	@TableField(value = "createuser")
	private String createuser;

	/**
	 * 更新时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty
	@TableField(value = "updatetime", fill = FieldFill.UPDATE)
	private Date updatetime;

	/**
	 * 修改人
	 */
	@ApiModelProperty
	@TableField(value = "updateuser")
	private String updateuser;

	/**
	 * 父游戏名称
	 */
	@TableField(exist = false)
	private String pgname;

	/**
	 * 创建者姓名
	 */
	@TableField(exist = false)
	private String createUserName;

	/**
	 * 主游戏ID
	 */
	@TableField(exist = false)
	private Collection<Long> pGameIds;

	public AdTitleLib setId(Long id) {
		this.id = id;
		return this;
	}

	public AdTitleLib setTitleName(String titleName) {
		this.titleName = titleName;
		return this;
	}

	public AdTitleLib setPlatformId(Integer platformId) {
		this.platformId = platformId;
		return this;
	}

	public AdTitleLib setProjectId(Integer projectId) {
		this.projectId = projectId;
		return this;
	}

	public AdTitleLib setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
		return this;
	}

	public AdTitleLib setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	public AdTitleLib setCreatetime(Date createtime) {
		this.createtime = createtime;
		return this;
	}

	public AdTitleLib setCreateuser(String createuser) {
		this.createuser = createuser;
		return this;
	}

	public AdTitleLib setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
		return this;
	}

	public AdTitleLib setUpdateuser(String updateuser) {
		this.updateuser = updateuser;
		return this;
	}

	public AdTitleLib setPgname(String pgname) {
		this.pgname = pgname;
		return this;
	}

	public AdTitleLib setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
		return this;
	}

	public AdTitleLib setPGameIds(Collection<Long> pGameIds) {
		this.pGameIds = pGameIds;
		return this;
	}

}