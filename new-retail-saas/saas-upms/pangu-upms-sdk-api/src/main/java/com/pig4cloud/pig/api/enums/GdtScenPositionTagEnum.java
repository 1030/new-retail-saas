package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 *  广点通场景定向标签
 */
public enum GdtScenPositionTagEnum {
	WECHAT_POSITION("WECHAT_POSITION",
			"微信公众号与小程序定投"),

	OFFICIAL_ACCOUNT_MEDIA_CATEGORY("OFFICIAL_ACCOUNT_MEDIA_CATEGORY",
			"公众号媒体类型"),

	MINI_PROGRAM_AND_MINI_GAME("MINI_PROGRAM_AND_MINI_GAME",
			"小游戏小程序场景"),

	PAY_SCENE("PAY_SCENE",
			"订单详情页"),

	MOBILE_UNION_CATEGORY("MOBILE_UNION_CATEGORY",
			"优量汇媒体类型");
	//枚举值所包含的属性
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 名称
	 */
	private String name;

	//构造方法
	GdtScenPositionTagEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 通过value    取type
	 *
	 * @return
	 */
	public static String typeByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}

		for (GdtScenPositionTagEnum item : GdtScenPositionTagEnum.values()) {
			if (item.getName().equals(value)) {
				return item.getType();
			}
		}
		return null;

	}

	/**
	 * 通过type   取value
	 *
	 * @return
	 */
	public static String valueByType(String type) {
		if (StringUtils.isBlank(type)) {
			return null;
		}

		for (GdtScenPositionTagEnum item : GdtScenPositionTagEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}

	public static boolean containsType(String type) {
		if (StringUtils.isBlank(type)) {
			return false;
		}

		for (GdtScenPositionTagEnum item : GdtScenPositionTagEnum.values()) {
			if (item.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
