package com.pig4cloud.pig.api.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * 广点通品牌形象表
 * gdt_brand_image
 * @author nml
 * @date 2020-12-04 15:06:12
 */
@Getter
@Setter
public class GdtBrandImageVo extends Page{
    /**
     */
    private Long id;

    /**
     * 广点通图片id
     */
    private String imageId;

    /**
     * 广点通广告账户id
     */
    private String accountId;

    /**
     * 广点通图片名称
     */
    private String name;

    /**
     * 广点通图片宽
     */
    private String width;

    /**
     * 广点通图片高
     */
    private String height;

    /**
     * 广点通图片访问地址
     */
    private String imageUrl;

    /**
     * 广点通创建时间
     */
    private String createdTime;

    /**
     * 图片文件存储名称
     */
    private String picFilename;

    /**
     * 真实路径
     */
    private String realPath;

    /**
     * 图片本地访问地址
     */
    private String picUrl;

    /**
     * 图片md5值
     */
    private String md5;

    /**
     * 图片大小
     */
	private String picSize;

    /**
     * 图片格式
     */
    private String format;

    /**
     * 项目ID
     */
    private Integer projectId;

    /**
     * 是否删除：0正常，1删除
     */
    private Integer isdelete;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 创建人
     */
    private String createuser;

    /**
     * 修改时间
     */
    private Date updatetime;

    /**
     * 修改人
     */
    private String updateuser;

	//图片文件名称数组
	private String[] names;

    //图片文件数组
	private MultipartFile[] imgFiles;

	/*接收广告账户ids*/
	private String advertiserIds;

}