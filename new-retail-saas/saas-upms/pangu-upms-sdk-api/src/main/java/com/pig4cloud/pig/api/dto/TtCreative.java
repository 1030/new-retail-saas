package com.pig4cloud.pig.api.dto;

import lombok.Data;

@Data
public class TtCreative {
	private Long	creativeId	;//	创意ID。当类型为程序化创意时，没有审核通过前，该字段为null
	private String	imageMode	;//	素材类型
	private String	title	;//	创意标题
	private Long[]	creativeWordIds	;//	动态词包ID
	private String[]	imageIds	;//	图片ID列表，图片素材返回
	private String	imageId	;//	图片ID，视频封面，视频素材时返回
	private String	videoId	;//	视频ID，视频素材时返回
	private Short	derivePosterCid	;//	是否将视频的封面和标题同步到图片创意，1为开启，0为不开启。视频素材时返回
	private String	thirdPartyId	;//	创意自定义参数，例如开发者可设定此参数为创意打标签用于区分使用的素材类型
	private Long[]	dpaDictIds	;//	DPA词包ID列表，针对DPA广告
	private Long	templateId	;//	DPA模板ID，针对DPA广告
	private String	templateImageId	;//	DPA创意实际显示的图片ID，针对DPA广告
	private Short	dpaTemplate	;//	是否使用商品库视频模板，针对DPA广告
	private String	dpaVideoTemplateType	;//	商品库视频模板生成类型，针对DPA广告
	private String[]	dpaVideoTaskIds	;//	自定义商品库视频模板ID，针对DPA广告

}
