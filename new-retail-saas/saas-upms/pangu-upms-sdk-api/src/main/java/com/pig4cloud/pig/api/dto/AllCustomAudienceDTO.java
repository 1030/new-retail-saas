package com.pig4cloud.pig.api.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * @Description 二合一人群包查询对象
 * @Author chengang
 * @Date 2021/6/18
 */
@Data
public class AllCustomAudienceDTO extends Page implements Serializable {

	private String startDate;

	private String endDate;

	/**
	 * 媒体类型  1头条 8广点通
	 */
	private Integer mediaType;

	/**
	 * 接收入参广告账户ID集合
	 */
	private List<String> advertiserIds;

	/**
	 * 操作DB用list
	 */
	private List<String> adList;

	/**
	 * 平台权限：当前登录人所关联角色对应的投放人集合
	 */
	List<String> throwUserList;

	private String name;

	/**
	 * 1:人群包属性数据  2:人群包分账号投放数据 3:人群包汇总投放数据
	 */
	private Integer target = 1;

}
