package com.pig4cloud.pig.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 广告转化表
 * @author  chenxiang
 * @version  2022-05-23 10:35:26
 * table: ad_convert
 */
@Data
public class AdConvertRes implements Serializable {

	public AdConvertRes(){}

	public AdConvertRes(String convertId,String convertName){
		this.convertId = convertId;
		this.convertName =convertName;
	}

	/**
	 * 转化ID
	 */
	private String convertId;

	/**
	 * 转化名称
	 */
	private String convertName;

	/**
	 * 转化目标
	 */
	private String convertTargetValue;
	private String convertTargetName;

	/**
	 * 深度转化目标
	 */
	private String deepConversionValue = "";
	private String deepConversionName = "";


}


