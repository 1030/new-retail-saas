package com.pig4cloud.pig.api.dto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 项目表(头条体验版)
 * @author  chenxiang
 * @version  2022-09-27 14:20:40
 * table: ad_project
 */
@Data
public class AdProjectRes implements Serializable {

	@ApiModelProperty(value = "主键id")
	private Long id;

	@ApiModelProperty(value = "项目ID")
	private Long projectId;

	@ApiModelProperty(value = "项目名称")
	private String name;

	@ApiModelProperty(value = "广告账户ID")
	private Long advertiserId;

	@ApiModelProperty(value = "推广目的，枚举值：APP 应用推广")
	private String landingType;

	@ApiModelProperty(value = "子目标，枚举值：DOWNLOAD 应用下载、LAUNCH 应用调用、RESERVE 预约下载")
	private String appPromotionType;

	@ApiModelProperty(value = "营销场景，枚举值：VIDEO_AND_IMAGE 短视频/图片")
	private String marketingGoal;

	@ApiModelProperty(value = "广告类型，枚举值：ALL")
	private String adType;

	@ApiModelProperty(value = "目标操作，枚举值：ENABLE 启用项目、DISABLE暂停项目。")
	private String optStatus;

	@ApiModelProperty(value = "项目创建时间，格式yyyy-mm-dd")
	private String projectCreateTime;

	@ApiModelProperty(value = "项目更新时间，格式yyyy-mm-dd")
	private String projectModifyTime;

	@ApiModelProperty(value = "项目状态")
	private String status;

	@ApiModelProperty(value = "出价方式")
	private String pricing;

	@ApiModelProperty(value = "关联产品投放相关(product_setting,product_platform_id,product_id)")
	private String relatedProduct;

	@ApiModelProperty(value = "下载链接")
	private String downloadUrl;

	@ApiModelProperty(value = "下载方式，枚举值：DOWNLOAD_URL 直接下载、EXTERNAL_URL 落地页下载")
	private String downloadType;

	@ApiModelProperty(value = "优先从系统应用商店下载（下载模式），枚举值：APP_STORE_DELIVERY 优先商店下载、 DEFAULT 默认下载")
	private String downloadMode;

	@ApiModelProperty(value = "调起方式，枚举值： DIRECT_OPEN 直接调起、EXTERNAL_OPEN 落地页调起")
	private String launchType;

	@ApiModelProperty(value = "Deeplink直达链接")
	private String openUrl;

	@ApiModelProperty(value = "ulink直达链接")
	private String ulinkUrl;

	@ApiModelProperty(value = "预约下载链接")
	private String subscribeUrl;

	@ApiModelProperty(value = "优化目标(asset_ids,convert_id,external_action,deep_external_action)")
	private String optimizeGoal;

	@ApiModelProperty(value = "广告版位(inventory_catalog,inventory_type,union_video_type)")
	private String deliveryRange;

	@ApiModelProperty(value = "定向设置")
	private String audience;

	@ApiModelProperty(value = "投放设置")
	private String deliverySetting;

	@ApiModelProperty(value = "监测链接设置")
	private String trackUrlSetting;

	@ApiModelProperty(value = "来源(1:API,2:渠道后台)")
	private Integer soucreStatus;

	@ApiModelProperty(value = "是否删除  0否 1是")
	private Integer deleted;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

	@ApiModelProperty(value = "创建人")
	private Long createId;

	@ApiModelProperty(value = "修改人")
	private Long updateId;
}


