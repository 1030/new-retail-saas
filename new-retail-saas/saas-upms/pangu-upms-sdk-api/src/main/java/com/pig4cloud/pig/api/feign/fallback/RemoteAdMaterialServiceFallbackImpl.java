/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.feign.fallback;

import com.pig4cloud.pig.api.entity.AdMaterial;
import com.pig4cloud.pig.api.feign.RemoteAdMaterialService;
import com.pig4cloud.pig.api.vo.AdMaterialVo;
import com.pig4cloud.pig.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author cx
 * @version 1.0.0
 * @Time 2021/10/14 15:30
 */
@Slf4j
@Component
public class RemoteAdMaterialServiceFallbackImpl implements RemoteAdMaterialService {

	@Setter
	private Throwable cause;

	@Override
	public R addYanDiMaterial(AdMaterialVo record, String from) {
		return null;
	}

	@Override
	public R<List<AdMaterial>> getMaterialListByIds(String from, List<Long> materialIds) {
		log.error("根据素材ID列表查询素材信息接口熔断", cause);
		return R.ok(Collections.emptyList());
	}
	
	@Override
	public R<Set<Long>> getPreparePushMaterial(String from, String loginUserId) {
		log.error("查询素材推送箱待推送素材接口熔断", cause);
		return R.ok(new HashSet<>());
	}

}
