package com.pig4cloud.pig.api.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
public class AdTermBannedDto extends Page {
	private Long id;//id
	//@NotEmpty(message = "terms为空")
	@Size(max=255,min=1,message = "长度不允许超过255个长度")
	private String terms;//屏蔽词
	@NotEmpty(message = "advertiser_id为空")
	private String advertiser_id;//广告账户id
//	@NotEmpty(message = "advertiser_name为空")
	private String advertiser_name;//广告账户名称
	private List<String> terms_list; //屏蔽词列表
	List<String> advertiserIds;
}
