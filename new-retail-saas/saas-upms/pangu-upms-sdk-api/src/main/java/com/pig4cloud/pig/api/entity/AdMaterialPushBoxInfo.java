/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.api.entity;

import java.io.Serializable;
import java.util.List;

import com.pig4cloud.pig.api.vo.AdMaterialPushBoxVO;

import lombok.Data;

/**
 * 素材推送箱 返回结果
 * @author Administrator
 *
 */
@Data
public class AdMaterialPushBoxInfo implements Serializable {
	
	private static final long serialVersionUID = 7442633332987681506L;

	/**
	 * 显示未推送数量
	 */
	private Integer num = 0;
	
	/**
	 * 预推送集合
	 */
	List<AdMaterialPushBoxVO> adMaterialPreparePush;
	
	/**
	 * 推送中集合
	 */
	List<AdMaterialPushBoxVO> adMaterialProgressPush;
	
	/**
	 * 推送完成集合
	 */
	List<AdMaterialPushBoxVO> adMaterialFinishPush;
	
}
