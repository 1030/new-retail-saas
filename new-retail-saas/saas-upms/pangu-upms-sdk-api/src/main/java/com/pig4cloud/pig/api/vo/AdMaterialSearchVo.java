package com.pig4cloud.pig.api.vo;

import cn.hutool.core.collection.ListUtil;
import com.pig4cloud.pig.api.util.Constants;
import com.pig4cloud.pig.api.util.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class AdMaterialSearchVo {
	/**
	 * 素材名称
	 */
	@ApiModelProperty(value="素材名称")
	private String name;
	/**
	 * 主游戏ID
	 */
	@ApiModelProperty(value="主游戏ID集合,多个用逗号分割")
	private String mainGameIds;

	public List<Integer> getMainGameIds() {
		if(StringUtils.isBlank(mainGameIds)){
			return ListUtil.empty();
		}else {
			return Arrays.stream(mainGameIds.split(Constants.COMMA)).map(v->Integer.parseInt(v)).collect(Collectors.toList());
		}
	}

	/**
	 * 主游戏ID
	 */
	@ApiModelProperty(value="子游戏ID集合,多个用逗号分割")
	private String gameIds;

	public List<Integer> getGameIds() {
		if(StringUtils.isBlank(gameIds)){
			return ListUtil.empty();
		}else {
			return Arrays.stream(gameIds.split(Constants.COMMA)).map(v->Integer.parseInt(v)).collect(Collectors.toList());
		}
	}

	/**
	 * 创意者
	 */
	@ApiModelProperty(value="创意者Id集合,多个用逗号分割")
	private String creatorIds;

	public List<Integer> getCreatorIds() {
		if(StringUtils.isBlank(creatorIds)){
			return ListUtil.empty();
		}else {
			return Arrays.stream(creatorIds.split(Constants.COMMA)).map(v->Integer.parseInt(v)).collect(Collectors.toList());
		}
	}

	/**
	 * 制作者
	 */
	@ApiModelProperty(value="制作者Id集合,多个用逗号分割")
	private String makerIds;

	public List<Integer> getMakerIds() {
		if(StringUtils.isBlank(makerIds)){
			return ListUtil.empty();
		}else {
			return Arrays.stream(makerIds.split(Constants.COMMA)).map(v->Integer.parseInt(v)).collect(Collectors.toList());
		}
	}

	/**
	 * 制作类型（1：原创，2：原创衍生，3：抄本，4：抄本衍生）
	 */
	@ApiModelProperty(value="制作类型（1：原创，2：原创衍生，3：抄本，4：抄本衍生）集合，多个用逗号分割")
	private String makeTypes;

	public List<Integer> getMakeTypes() {
		if(StringUtils.isBlank(makeTypes)){
			return ListUtil.empty();
		}else {
			return Arrays.stream(makeTypes.split(Constants.COMMA)).map(v->Integer.parseInt(v)).collect(Collectors.toList());
		}
	}

	/**
	 * 素材宽
	 */
	@ApiModelProperty(value="素材宽")
	private String width;
	/**
	 * 素材高
	 */
	@ApiModelProperty(value="素材高")
	private String height;
	/**
	 * 标签
	 */
	@ApiModelProperty(value="标签")
	private String labels;
	/**
	 * 每页显示条数，默认 10
	 */
	@Min(1)
	@ApiModelProperty(value="每页显示条数，默认 10")
	private long size = 10;
	/**
	 * 当前页
	 */
	@Min(1)
	@ApiModelProperty(value="当前页，默认 1")
	private long current = 1;
}
