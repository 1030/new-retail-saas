package com.pig4cloud.pig.api.gdt.entity;


import java.math.BigInteger;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @广点通广告主授权码
 * @author zhuxm
 *
 */
@Data
@NoArgsConstructor
@TableName(value="gdt_accesstoken")
public class GdtAccesstoken extends Model<GdtAccesstoken> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6340348224308465761L;

	@ApiModelProperty(value = "授权的管家帐号 id")
	@TableId(value = "account_id")
	private String accountId;
	
	
	@ApiModelProperty(value = "授权的推广帐号对应的 QQ 号")
	@TableField(value = "account_uin")
	private String accountUin;
	
	
	
	@ApiModelProperty(value = "权限列表，若为空，则表示拥有所属应用的所有权限")
	@TableField(value = "scope_list")
	private String scopeList;	
	
	@ApiModelProperty(value = "授权的推广帐号对应的微信帐号 id")
	@TableField(value = "wechat_account_id")
	private String wechatAccountId;
	
	@ApiModelProperty(value = "授权账号身份类型，授权账号类型广告主,代理商,T1 账户,商务管家账户")
	@TableField(value = "account_role_type")
	private String accountRoleType;
	
	@ApiModelProperty(value = "账号类型")
	@TableField(value = "account_type")
	private String accountType;
	
	@ApiModelProperty(value = "角色")
	@TableField(value = "role_type")
	private String roleType;
	
	
	@ApiModelProperty(value = "应用 id")
	@TableField(value = "client_id")
	private String clientId;
	
	@ApiModelProperty(value = "应用 secret")
	@TableField(value = "client_secret")
	private String clientSecret;
	
	@ApiModelProperty(value = "OAuth 认证 code")
	@TableField(value = "authorization_code")
	private String authorizationCode;
	
	
	@ApiModelProperty(value = "回调地址")
	@TableField(value = "redirect_uri")
	private String redirectUri;
	
	
	@ApiModelProperty(value = "应用 access token")
	@TableField(value = "access_token")
	private String accessToken;
	
	
	@ApiModelProperty(value = "应用 refresh token")
	@TableField(value = "refresh_token")
	private String refreshToken;
	
	@ApiModelProperty(value = "时间戳，单位（秒）")
	@TableField(value = "time")
	private BigInteger time;
	
	
	@ApiModelProperty(value = "access_token 过期时间，单位（秒）")
	@TableField(value = "access_token_expires_in")
	private Integer accessTokenExpiresIn;
	
	@ApiModelProperty(value = "refresh_token 过期时间，单位（秒）")
	@TableField(value = "refresh_token_expires_in")
	private Integer refreshTokenExpiresIn;
	
	@ApiModelProperty(value = "创建时间")
	@TableField( fill = FieldFill.INSERT)
	private Date createtime;
	
	@ApiModelProperty(value = "修改时间")
	@TableField( fill = FieldFill.UPDATE)
	private Date updatetime;
	
	
	@ApiModelProperty(value = "同步其他平台标识,  N：还未同步，Y:已同步")
	@TableField(value = "issyn")
	private String issyn;
}