package com.pig4cloud.pig.api.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 创意素材关联关系
 *
 * @author xzl
 */
@Data
@Accessors(chain = true)
@TableName("ad_creative_material")
public class AdCreativeMaterialEntity implements Serializable {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;

	/**
	 * 广告主ID
	 */
	private Long advertiserId;

	/**
	 * 广告计划ID
	 */
	private Long adId;

	/**
	 * 创意ID
	 */
	private Long creativeId;

	/**
	 * 平台素材ID
	 */
	private Long materialId;

	/**
	 * 平台标识: 1-为头条; 8-广点通;
	 */
	private Integer platformId;

	/**
	 * 素材文件ID
	 */
	private String platformFileId;

	/**
	 * 落地页ID
	 */
	private Long landingPageId;

	/*public static AdCreativeMaterialEntity build(Long creativeId, Integer platformId, String platformFileId) {
		AdCreativeMaterialEntity entity = new AdCreativeMaterialEntity();
		entity.setCreativeId(creativeId);
		entity.setPlatformId(platformId);
		entity.setPlatformFileId(platformFileId);
		return entity;
	}*/

}
