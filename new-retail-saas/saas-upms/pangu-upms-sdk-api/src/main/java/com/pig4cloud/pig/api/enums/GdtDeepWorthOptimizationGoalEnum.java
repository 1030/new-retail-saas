package com.pig4cloud.pig.api.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @Description 深度优化价值目标
 * @Author chengang
 * @Date 2022/5/16
 */
public enum GdtDeepWorthOptimizationGoalEnum {
	/**
	 * 深度优化价值目标枚举类
	 */
	GOAL_30DAY_ORDER_ROAS("GOAL_30DAY_ORDER_ROAS", "下单ROI"),
	GOAL_1DAY_PURCHASE_ROAS("GOAL_1DAY_PURCHASE_ROAS", "首日付费ROI"),
	GOAL_1DAY_MONETIZATION_ROAS("GOAL_1DAY_MONETIZATION_ROAS", "首日变现ROI");

	/**
	 * 类型
	 */
	private String type;
	/**
	 * 名称
	 */
	private String name;

	public static String getNameByType(String type) {
		if (StringUtils.isBlank(type)) {
			return null;
		}

		for (GdtDeepWorthOptimizationGoalEnum item : GdtDeepWorthOptimizationGoalEnum.values()) {
			if (item.getType().equals(type)) {
				return item.getName();
			}
		}
		return null;

	}


	GdtDeepWorthOptimizationGoalEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
