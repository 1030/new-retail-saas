/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.api.feign.fallback;

import com.pig4cloud.pig.api.dto.AdAccountTreeResp;
import com.pig4cloud.pig.api.entity.AdAccount;
import com.pig4cloud.pig.api.feign.RemoteAccountService;
import com.pig4cloud.pig.api.vo.AdAccountVo;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/**
 * @author lengleng
 * @date 2019/2/1
 */
@Slf4j
@Component
public class RemoteAccountServiceFallbackImpl implements RemoteAccountService {

	@Setter
	private Throwable cause;

	@Override
	public R<List<AdAccount>> getAccountListByUserList(List<Integer> userIdArr, String from) {
		return null;
	}

	@Override
	public List<String> getAccountList2(List<Integer> userIdArr, String from) {
		return null;
	}

	@Override
	public R<List<AdAccountTreeResp>> getAccountTreeByUserList(List<String> userIdArr, Boolean isAdmin,String mediaCode, String from) {
		return null;
	}

	@Override
	public List<AdAccount> getAllList(@RequestBody AdAccountVo req, @RequestHeader(SecurityConstants.FROM) String from){
		return null;
	}

	@Override
	public R<List<AdAccount>> getAccountsByAuthorize(@RequestBody AdAccountVo req) {
		return null;
	}
}
