/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.api.feign;

import com.pig4cloud.pig.api.entity.AdAssetsTrack;
import com.pig4cloud.pig.api.entity.AdConvert;
import com.pig4cloud.pig.api.entity.ConvertTrack;
import com.pig4cloud.pig.api.entity.GdtChannelPackage;
import com.pig4cloud.pig.api.entity.KsApp;
import com.pig4cloud.pig.api.feign.factory.RemoteConvertServiceFallbackFactory;
import com.pig4cloud.pig.api.vo.AdConvertReq;
import com.pig4cloud.pig.api.vo.GdtChannelPackageReq;
import com.pig4cloud.pig.api.vo.SelectConvertTrackReq;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/28
 */
@FeignClient(contextId = "remoteConvertService", value = ServiceNameConstants.UPMS_SDK_SERVICE, fallbackFactory = RemoteConvertServiceFallbackFactory.class)
public interface RemoteConvertService {
	/**
	 * 创建转化
	 *
	 * @param record 转化实体
	 * @param from   是否内部调用
	 * @return succes、false
	 */
	@PostMapping("/convert_track/create")
	R create(@RequestBody ConvertTrack record, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 广点通渠道包
	 *
	 * @param record 转化实体
	 * @param from   是否内部调用
	 * @return succes、false
	 */
	@PostMapping("/gdt/channelPackage/create")
	R channelPackage(@RequestBody GdtChannelPackage record, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 广点通创建应用包
	 *
	 * @param record 转化实体
	 * @param from   是否内部调用
	 * @return succes、false
	 */
	@PostMapping("/gdt/channelPackage/extendPackageAdd")
	R extendPackageAdd(@RequestBody GdtChannelPackageReq record, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * @description: 根据广告监测ID或名字获取数据
	 * @author yuwenfeng
	 * @date 2021/12/14 14:53
	 */
	@PostMapping("/convert_track/selectInfoByKey")
	R<ConvertTrack> selectInfoByKey(@RequestBody SelectConvertTrackReq req, @RequestHeader(SecurityConstants.FROM) String from);
	/**
	 * @description: 获取转化三方状态列表
	 * @author yuwenfeng
	 * @date 2021/12/14 14:53
	 */
	@PostMapping("/convert_track/selectListByKey")
	R<List<ConvertTrack>> selectListByKey(@RequestBody SelectConvertTrackReq req, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 头条创建监测链接组
	 * @param record 转化实体
	 * @param from   是否内部调用
	 * @return succes、false
	 */
	@PostMapping("/adAssetsTrack/create")
	R<AdAssetsTrack> createEventTrackUrl(@RequestBody AdAssetsTrack record, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 广点通 - 创建转化归因
	 * @param record
	 * @param from
	 * @return
	 */
	@PostMapping("/adConvert/createGdtConversions")
	R createGdtConversions(@RequestBody AdConvert record, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 广点通 - 渠道包列表
	 * @param record
	 * @param from
	 * @return
	 */
	@PostMapping("/gdt/channelPackage/getPackageList")
	R<List<GdtChannelPackage>> getPackageList(@RequestBody GdtChannelPackageReq record, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 广点通 - 转化归因列表
	 * @param record
	 * @param from
	 * @return
	 */
	@PostMapping("/adConvert/getAdConvertListInner")
	R<List<AdConvert>> getAdConvertListInner(@RequestBody AdConvertReq record, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 创建快手应用
	 * @param app 转化实体
	 * @param from   是否内部调用
	 * @return succes、false
	 */
	@PostMapping("/ks/app/create")
	R<KsApp> createKsApp(@RequestBody KsApp app, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 创建快手应用
	 * @param record 转化实体
	 * @param from   是否内部调用
	 * @return succes、false
	 */
	@PostMapping("/adConvert/addOcpcTransFeed")
	R<AdConvert> addOcpcTransFeed(@RequestBody AdConvertReq record, @RequestHeader(SecurityConstants.FROM) String from);
}
