package com.pig4cloud.pig.api.gdt.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GdtAudiencePackagePageReq {
	private String[] advertiserIds;

	private String[] accountIds;

	private Short syncStatus;
}
