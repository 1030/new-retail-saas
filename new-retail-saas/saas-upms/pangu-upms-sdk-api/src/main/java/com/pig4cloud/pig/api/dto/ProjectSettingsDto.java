package com.pig4cloud.pig.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * project_settings
 * @author nml
 * @date 2020-11-04 11:06:33
 */
@Getter
@Setter
public class ProjectSettingsDto {
    /**
     */
    private Integer id;

    /**
     * 项目名称
     */
    private String projectName;

    /*
    * 资源数
    * */
    private Integer resourcesNum;

    /**
     * 更新时间
     */
    private Date updateTime;

	@Override
	public String toString() {
		return "ProjectSettingsDto{" +
				"id=" + id +
				", projectName='" + projectName + '\'' +
				", resourcesNum=" + resourcesNum +
				", updateTime=" + updateTime +
				'}';
	}
}