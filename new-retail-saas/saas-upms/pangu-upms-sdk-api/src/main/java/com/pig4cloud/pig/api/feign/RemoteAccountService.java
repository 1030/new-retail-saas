/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.api.feign;

import com.pig4cloud.pig.api.dto.AdAccountTreeResp;
import com.pig4cloud.pig.api.entity.AdAccount;
import com.pig4cloud.pig.api.feign.factory.RemoteAccountServiceFallbackFactory;
import com.pig4cloud.pig.api.vo.AdAccountVo;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/28
 */
@FeignClient(contextId = "remoteAccountService", value = ServiceNameConstants.UPMS_SDK_SERVICE, fallbackFactory = RemoteAccountServiceFallbackFactory.class)
public interface RemoteAccountService {
	/**
	 * 根据用户列表查询绑定的的广告账号列表
	 *
	 * @param userIdArr
	 * @return
	 */
	@PostMapping("/account/getAccountListByUserList")
	R<List<AdAccount>> getAccountListByUserList(@RequestParam("userIdArr") List<Integer> userIdArr, @RequestHeader(SecurityConstants.FROM) String from);


	/**
	 * 获取授权的广告账号列表
	 *
	 * @param userIdArr
	 * @return
	 */
	@PostMapping("/account/getAccountList2")
	List<String> getAccountList2(@RequestParam("userIdArr") List<Integer> userIdArr, @RequestHeader(SecurityConstants.FROM) String from);


	@PostMapping("/account/getAccountTreeByUserList")
	R<List<AdAccountTreeResp>> getAccountTreeByUserList(@RequestParam("userIdArr") List<String> userIdArr, @RequestParam("isAdmin") Boolean isAdmin, @RequestParam("mediaCode") String mediaCode, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 获取授权的广告账号列表
	 *
	 * @param req
	 * @return
	 */
	@PostMapping("/account/getAllList")
	List<AdAccount> getAllList(@RequestBody AdAccountVo req, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 根据授权查看广告账户列表
	 *
	 * @param req
	 * @return
	 */
	@PostMapping("/account/getAccountsByAuthorize")
	R<List<AdAccount>> getAccountsByAuthorize(@RequestBody AdAccountVo req);

}
