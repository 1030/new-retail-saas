package com.pig4cloud.pig.api.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdLabelRelateEntity {
	private Integer id;
	private Integer labelid;
	private Integer relateid;
	private Integer type;
}
