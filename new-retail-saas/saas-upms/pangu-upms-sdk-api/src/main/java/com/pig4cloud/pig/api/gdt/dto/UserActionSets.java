package com.pig4cloud.pig.api.gdt.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Hma on 2020/12/10.
 * gdt请求对象
 */
@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserActionSets {

	private Long id;
	private String  type;

}
