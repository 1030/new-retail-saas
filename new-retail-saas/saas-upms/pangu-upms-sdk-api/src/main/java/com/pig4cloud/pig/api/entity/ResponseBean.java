package com.pig4cloud.pig.api.entity;

import com.alibaba.fastjson.JSONObject;

/**
 * 巨量引擎api接口返回消息类
 *
 * @author gaozhi0
 * 2019-04-25 14:55
 **/
public class ResponseBean {

	/**
	 * 0  成功
	 */
	private String code;

	/**
	 *
	 */
	private String message;


	private String message_cn;

	/**
	 * 返回数据,多个接口返回值的此字段结构不同，取值时用JSONObject
	 **/
	protected JSONObject data;

	private String request_id;

	public ResponseBean() {
	}

	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage_cn() {
		return message_cn;
	}

	public void setMessage_cn(String message_cn) {
		this.message_cn = message_cn;
	}


	public JSONObject getData() {
		return data;
	}


	public void setData(JSONObject data) {
		this.data = data;
	}


	public String getRequest_id() {
		return request_id;
	}


	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}


}
