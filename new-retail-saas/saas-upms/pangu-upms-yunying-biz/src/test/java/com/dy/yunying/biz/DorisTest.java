package com.dy.yunying.biz;

import com.dy.yunying.api.entity.DorisTable;
import com.dy.yunying.biz.dao.doris.DorisTestMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName DorisTest
 * @Description todo
 * @Author nieml
 * @Time 2022/5/26 15:10
 * @Version 1.0
 **/
@Slf4j
//@RunWith(SpringRunner.class)
@SpringBootTest
public class DorisTest {

	@Autowired
	private DorisTestMapper dorisTestMapper;

	@Test
	public void list(){
		List<DorisTable> dorisTables = dorisTestMapper.cdkList();
		for (DorisTable dorisTable : dorisTables) {
			System.out.println(dorisTable.toString());
		}
	}

	@Test
	public void test(){
		System.out.println("test");
	}


}
