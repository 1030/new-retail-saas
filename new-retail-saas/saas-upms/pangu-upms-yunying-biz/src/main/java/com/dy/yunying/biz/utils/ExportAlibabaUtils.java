package com.dy.yunying.biz.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.google.common.collect.Lists;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author lsw
 * @version 1.0.0
 * @ClassName Export.java
 * @createTime 2023年3月6日 17:47:00
 */
public class ExportAlibabaUtils {


	public static void exportExcelData(HttpServletResponse response,String sheetName, String fileName, String columns, List<?> list,Class clazz){
		try {
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setCharacterEncoding("utf-8");
			response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
			response.setHeader("file-name", fileName);

			ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).head(clazz).build();
			WriteSheet writeSheet0 = EasyExcel.writerSheet(0, sheetName)
					// 只导出前端传入的自定义列
					.includeColumnFiledNames(Lists.newArrayList(columns.split(","))).head(clazz).build();

			excelWriter.write(list, writeSheet0);
			excelWriter.finish();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
