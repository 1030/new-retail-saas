package com.dy.yunying.biz.dao.manage.ext;

import com.dy.yunying.biz.dao.manage.WanGameAreaDOMapper;

import java.util.Map;

public interface WanGameAreaDOMapperExt extends WanGameAreaDOMapper {
	/**
	 * 根据游戏id获取游戏绑定区服数量
	 *
	 * @param map
	 * @return
	 */
	int selectAreaCountByMap(Map<String, Object> map);

}