package com.dy.yunying.biz.task;

import com.dy.yunying.biz.service.hongbao.PopupNoticeService;
import com.dy.yunying.biz.service.sign.SignActivityService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description: 公告到期
 * @author yuwenfeng
 * @date 2021/12/8 9:38
 */
@Component
public class NoticeFinishTask {

	@Autowired
	private PopupNoticeService popupNoticeService;

	@XxlJob("noticeFinishJob")
	public ReturnT<String> noticeFinishJob(String param) {
		try {
			XxlJobLogger.log("--公告到期下线--开始--");
			popupNoticeService.noticeFinish();
			XxlJobLogger.log("--公告到期下线--完成--");
		} catch (Exception e) {
			e.printStackTrace();
			XxlJobLogger.log("--公告到期下线--异常{}", e.getMessage());
		}
		return ReturnT.SUCCESS;
	}

}
