package com.dy.yunying.biz.dao.ads.sign;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.sign.SignPrizeGoods;
import org.apache.ibatis.annotations.Mapper;

/**
 * 奖品物品关联表
 * @author  chengang
 * @version  2021-12-01 10:14:26
 * table: sign_prize_goods
 */
@Mapper
public interface SignPrizeGoodsMapper extends BaseMapper<SignPrizeGoods> {
	

}


