package com.dy.yunying.biz.controller.hongbao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.hongbao.HbActivityKvpic;
import com.dy.yunying.api.vo.HbActivityKvpicVO;
import com.dy.yunying.biz.service.hongbao.HbActivityKvpicService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * kv图库表服务控制器
 *
 * @author zjz
 * @since 2022-06-20 16:23:13
 * @description 由 zjz 创建
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/activityKvpic")
public class HbActivityKvpicController{

	private final HbActivityKvpicService hbActivityKvpicService;
	/**
	 * 列表
	 *
	 * @return
	 */
	@GetMapping("/page")
	public R<Page<HbActivityKvpic>> page(HbActivityKvpicVO hbActivityKvpicVO) {
		Page<HbActivityKvpic> page = hbActivityKvpicService.getKvPage(hbActivityKvpicVO);
		return R.ok(page);
	}

	/**
	 * 保存
	 *
	 * @param hbActivityKvpic
	 * @return
	 */
	@PostMapping("/save")
	public R<Void> save(@RequestBody HbActivityKvpic hbActivityKvpic) {
		try {
			hbActivityKvpicService.doSave(hbActivityKvpic);
			return R.ok();
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 修改
	 *
	 * @param hbActivityKvpic
	 * @return
	 */
	@PostMapping("/edit")
	public R<Void> edit(@RequestBody HbActivityKvpic hbActivityKvpic) {
		try {
			if(hbActivityKvpic.getId()==null){
				return R.failed("ID不能为空");
			}
			hbActivityKvpicService.doEdit(hbActivityKvpic);
			return R.ok();
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 删除
	 *
	 * @param id
	 * @return
	 */
	@PostMapping("/delete")
	public R<Void> delete(@RequestParam @NotNull(message = "ID不能为空") Long id) {
		hbActivityKvpicService.doDelete(id);
		return R.ok();
	}
}