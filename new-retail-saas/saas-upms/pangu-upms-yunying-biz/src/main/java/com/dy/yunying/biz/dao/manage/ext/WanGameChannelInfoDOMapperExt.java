package com.dy.yunying.biz.dao.manage.ext;

import com.dy.yunying.api.entity.WanGameChannelInfoDO;
import com.dy.yunying.api.resp.hongbao.NodeData;
import com.dy.yunying.api.resp.hongbao.NodeParentData;
import com.dy.yunying.biz.dao.manage.WanGameChannelInfoDOMapper;

import java.util.List;

public interface WanGameChannelInfoDOMapperExt extends WanGameChannelInfoDOMapper {

	WanGameChannelInfoDO getByChl(String chl);

	int updateByChlSelective(WanGameChannelInfoDO gameChannelInfoDO);

	List<NodeData> selectSubNodeListByChannel(String channel);

	List<NodeParentData> selectAllSubNodeList();
}