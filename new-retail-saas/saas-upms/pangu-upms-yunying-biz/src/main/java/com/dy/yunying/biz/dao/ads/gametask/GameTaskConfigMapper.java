package com.dy.yunying.biz.dao.ads.gametask;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.gametask.GameTaskConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
* @Entity com.dy.yunying.biz.domain.GameTaskConfig
*/
public interface GameTaskConfigMapper extends BaseMapper<GameTaskConfig> {


    List<GameTaskConfig> getGameTaskListByParentGameIds(@Param("parentGameIds") List<Long> parentGameIds);

	String getAppIdByParentGameId(Long parentGameId);

	void batchSaveWithReplace(@Param("gameTaskConfigs") List<GameTaskConfig> gameTaskConfigs);

	void saveWithReplace(GameTaskConfig gameTaskConfig);
}
