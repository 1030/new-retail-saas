package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.TimeGameRegDto;
import com.dy.yunying.api.datacenter.vo.TimeGameDeviceRegVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * dogame 分时游戏注册
 * @author hma
 * @date 2022/8/19 15:36
 */
public interface TimeGameRegService {
	/**
	 * 列表数据
	 * @param req
	 * @return
	 */
	R<List<TimeGameDeviceRegVO>> page(TimeGameRegDto req);

	/**
	 * 总数量
	 * @param req
	 * @return
	 */
	R count(TimeGameRegDto req);

	/**
	 * 汇总数据
	 * @param req
	 * @return
	 */
	R<List<TimeGameDeviceRegVO>> collect(TimeGameRegDto req);
}
