package com.dy.yunying.biz.service.hbdata;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbActivityArea;
import com.dy.yunying.api.entity.raffle.RaffleActivityPrize;

import java.util.List;

/**
 * 抽奖奖品配置表服务接口
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
 */
public interface HbActivityAreaService extends IService<HbActivityArea> {


	void saveBatchList(List<HbActivityArea> list);
}
