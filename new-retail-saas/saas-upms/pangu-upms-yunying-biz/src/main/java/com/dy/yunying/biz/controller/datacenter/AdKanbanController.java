package com.dy.yunying.biz.controller.datacenter;


import com.dy.yunying.api.datacenter.dto.AdKanbanDto;
import com.dy.yunying.api.datacenter.export.AdExportKanbanOverviewVo;
import com.dy.yunying.api.datacenter.vo.AdKanbanOverviewVo;
import com.dy.yunying.biz.service.datacenter.AdKanbanService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportAlibabaUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * 大盘看板相关接口
 */
@RestController("kanban")
@RequestMapping("/dataCenter/kanban")
@Slf4j
public class AdKanbanController {

	@Autowired
	private AdKanbanService kanbanService;

	/**
	 * 大盘概览总数
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@PostMapping(value = "/count")
	public R countDataTotal(@Valid @RequestBody AdKanbanDto req) {
		return kanbanService.countDataTotal(req);
	}

	/**
	 * 看板概览数据
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getKanbanStatistic")
	public R selectAdOverviewSource(@Valid @RequestBody AdKanbanDto req) {
		if (req.getCurrent() <= 0 || req.getSize() <= 0) {
			return R.failed("分页信息不允许为空");
		}
		return kanbanService.selectKanbanStatitic(req);
	}

	/**
	 * 看板概览导出
	 *
	 * @param req
	 * @return
	 */
	@ResponseExcel(name = "看板导出", sheet = "看板导出")
	@RequestMapping("/excelPlanStatistic")
	public R excelPlanStatistic(@Valid @RequestBody AdKanbanDto req, HttpServletResponse response, HttpServletRequest request) {
		String sheetName = "看板导出";
		try {
			List<AdKanbanOverviewVo> list = kanbanService.excelKanbanStatitic(req);
			// 导出
			String fileName = URLEncoder.encode("看板导出--"+ DateUtils.getCurrentTimeNoUnderline(), "UTF-8").replaceAll("\\+", "%20");

			ExportAlibabaUtils.exportExcelData(response,sheetName,fileName,req.getColumns(),list, AdKanbanOverviewVo.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
