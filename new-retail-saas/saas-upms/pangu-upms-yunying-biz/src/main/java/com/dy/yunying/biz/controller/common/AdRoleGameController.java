/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.controller.common;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dy.yunying.api.entity.AdRoleGame;
import com.dy.yunying.api.req.AdRoleGameReq;
import com.dy.yunying.biz.service.manage.AdRoleGameService;
import com.pig4cloud.pig.admin.api.feign.RemoteUserService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 角色游戏
 *
 * @author zhuxm
 * @date 2021-06-28
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/rolegame")
@Api(value = "rolegame", tags = "角色可查看的游戏")
public class AdRoleGameController {

	private final AdRoleGameService adRoleGameService;

	private final RemoteUserService remoteUserService;


	/**
	 * 当前权限id下设置的游戏列表(设置的时候返显用)
	 * @param
	 * @return
	 */
	@RequestMapping("/detail/{roleId}")
	public R getList(@PathVariable String roleId) {
		List<AdRoleGame> list = adRoleGameService.list(Wrappers.<AdRoleGame>query().lambda().eq(AdRoleGame::getRoleId, roleId));
		return R.ok(list);
	}


	/**
	 * 保存角色平台账号
	 *
	 * @param map
	 * @return success/false
	 */
	@SysLog("保存角色平台账号")
	@PreAuthorize("@pms.hasPermission('sys_role_game*')")
	@RequestMapping("/save")
	public R save(@RequestBody Map<String, String> map) {
		try {
			String games = map.get("games");
			if (StringUtils.isBlank(games)) {
				log.error("保存失败");
				return R.failed("参数错误");
			}
			List<AdRoleGameReq> gamesList = JSON.parseArray(games, AdRoleGameReq.class);
			adRoleGameService.saveRoleUser(gamesList);
		} catch (Throwable a) {
			log.error("保存失败", a);
			return R.failed("保存失败");
		}
		return R.ok();
	}

	/**
	 * 获取自身权限下的主游戏ID列表
	 *
	 * @return
	 */
	@GetMapping("/getOwnerRolePGameIds")
	public R<List<Long>> getOwnerRolePGameIds() {
		List<Long> list = adRoleGameService.getOwnerRolePgIds();
		return R.ok(list);
	}

	/**
	 * 获取自身权限下的子游戏ID列表
	 *
	 * @return
	 */
	@GetMapping("/getOwnerRoleGameIds")
	public R<List<Long>> getOwnerRoleGameIds() {
		List<Long> list = adRoleGameService.getOwnerRoleGameIds();
		return R.ok(list);
	}

	/**
	 * 获取自身权限下的子游戏列表
	 *
	 * @param
	 * @return
	 */
	@RequestMapping("/getOwnerRoleGames")
	public R getOwnerRoleGames() {
		List<Long> list = adRoleGameService.getOwnerRolePgIds();
		return R.ok(list);
	}
}
