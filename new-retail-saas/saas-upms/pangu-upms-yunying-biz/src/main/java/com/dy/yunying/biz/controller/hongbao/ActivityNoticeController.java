/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.controller.hongbao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.hongbao.HbActivityNotice;
import com.dy.yunying.api.req.hongbao.ActivityIdReq;
import com.dy.yunying.api.req.hongbao.ActivityNoticeReq;
import com.dy.yunying.api.req.hongbao.OnlineActivityNoticeReq;
import com.dy.yunying.biz.config.CheckRepeatSubmit;
import com.dy.yunying.biz.service.hongbao.ActivityNoticeService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 活动公告
 *
 * @author yuwenfeng
 * @date 2021-11-03 15:26:05
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/activitynotice")
@Api(value = "activitynotice", tags = "活动公告管理")
@Slf4j
public class ActivityNoticeController {

	private final ActivityNoticeService activityNoticeService;

	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/selectActivityNoticePage")
	@PreAuthorize("@pms.hasPermission('hongbao_hbactivitynotice_get')")
	public R<Page<HbActivityNotice>> selectActivityNoticePage(Page<HbActivityNotice> page, @Valid ActivityIdReq req) {
		try {
			QueryWrapper<HbActivityNotice> query = new QueryWrapper<HbActivityNotice>();
			query.eq("activity_id", req.getId());
			query.orderByDesc("start_time");
			return R.ok(activityNoticeService.page(page, query));
		} catch (Exception e) {
			log.error("活动公告-分页查询-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@CheckRepeatSubmit
	@ApiOperation(value = "新增活动公告", notes = "新增活动公告")
	@SysLog("新增活动公告")
	@PostMapping("/addNotice")
	@PreAuthorize("@pms.hasPermission('hongbao_hbactivitynotice_add')")
	public R addNotice(@Valid @RequestBody ActivityNoticeReq activityNotice) {
		try {
			return activityNoticeService.addNotice(activityNotice);
		} catch (Exception e) {
			log.error("活动公告-新增活动公告-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@CheckRepeatSubmit
	@ApiOperation(value = "修改活动公告", notes = "修改活动公告")
	@SysLog("修改活动公告")
	@PostMapping("/editNotice")
	@PreAuthorize("@pms.hasPermission('hongbao_hbactivitynotice_edit')")
	public R editNotice(@Valid @RequestBody ActivityNoticeReq activityNotice) {
		try {
			return activityNoticeService.editNotice(activityNotice);
		} catch (Exception e) {
			log.error("活动公告-修改活动公告-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@CheckRepeatSubmit
	@ApiOperation(value = "上/下线活动公告", notes = "上/下线活动公告")
	@SysLog("上/下线活动公告")
	@PostMapping("/onlineNotice")
	@PreAuthorize("@pms.hasPermission('hongbao_hbactivitynotice_online')")
	public R onlineNotice(@Valid @RequestBody OnlineActivityNoticeReq onlineActivityNoticeReq) {
		try {
			return activityNoticeService.onlineNotice(onlineActivityNoticeReq);
		} catch (Exception e) {
			log.error("活动公告-上/下线活动公告-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}
}
