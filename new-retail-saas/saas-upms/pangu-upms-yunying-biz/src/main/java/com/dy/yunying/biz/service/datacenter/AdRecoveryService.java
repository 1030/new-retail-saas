package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.AdRecoveryDto;
import com.pig4cloud.pig.common.core.util.R;

public interface AdRecoveryService {
	R selectAdRecoverySource(AdRecoveryDto req);

	R countDataTotal(AdRecoveryDto req);
}
