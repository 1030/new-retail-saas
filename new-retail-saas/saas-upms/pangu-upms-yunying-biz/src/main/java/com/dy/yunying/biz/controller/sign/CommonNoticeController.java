package com.dy.yunying.biz.controller.sign;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.entity.hongbao.PopupNotice;
import com.dy.yunying.api.enums.ActivityStatusEnum;
import com.dy.yunying.api.req.hongbao.NoticeSortReq;
import com.dy.yunying.api.req.hongbao.OnlineStatusReq;
import com.dy.yunying.api.req.sign.CommonNoticeReq;
import com.dy.yunying.api.req.sign.SelectCommonNoticeReq;
import com.dy.yunying.api.resp.hongbao.CommonNoticeVo;
import com.dy.yunying.biz.service.hongbao.PopupNoticeService;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author yuwenfeng
 * @description: 通用公告
 * @date 2021/12/1 17:48
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/commonnotice")
@Api(value = "commonnotice", tags = "通用公告")
public class CommonNoticeController {

	private final PopupNoticeService popupNoticeService;

	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/selectCommonNoticePage")
	@PreAuthorize("@pms.hasPermission('common_notice_get')")
	public R<Page<CommonNoticeVo>> selectCommonNoticePage(Page<PopupNotice> page, SelectCommonNoticeReq selectReq) {
		try {
			QueryWrapper<PopupNotice> query = new QueryWrapper<>();
			query.ge(StringUtils.isNotBlank(selectReq.getStartTime()), "DATE_FORMAT(popup_start_time,'%Y%m%d')", selectReq.getStartTime());
			query.le(StringUtils.isNotBlank(selectReq.getFinishTime()), "DATE_FORMAT(popup_start_time,'%Y%m%d')", selectReq.getFinishTime());
			query.eq(null != selectReq.getPopupMold(), "popup_mold", selectReq.getPopupMold());
			query.eq(null != selectReq.getPushFrequency(), "push_frequency", selectReq.getPushFrequency());
			query.eq(null != selectReq.getPushNode(), "push_node", selectReq.getPushNode());
			if (null != selectReq.getPopupStatus()) {
				query.eq("popup_status", selectReq.getPopupStatus());
				if (ActivityStatusEnum.ACTIVITING.getStatus().equals(selectReq.getPopupStatus())) {
					query.ge("DATE_FORMAT(popup_end_time,'%Y-%m-%d %H:%i:%S')", DateUtils.getcurrentTime());
				}
			}
			query.ne("source_type", 1);
			query.eq("deleted", Constant.DEL_NO);
			if (StringUtils.isNotBlank(selectReq.getSortType())) {
				if (Constant.ORDER_DESC.equalsIgnoreCase(selectReq.getSortType())) {
					query.orderByDesc("popup_sort");
				} else if (Constant.ORDER_ASC.equalsIgnoreCase(selectReq.getSortType())) {
					query.orderByAsc("popup_sort");
				} else {
					return R.failed("sortType参数不合法");
				}
			} else {
				query.orderByDesc("id");
			}
			return R.ok(popupNoticeService.selectCommonNoticePage(page, query, selectReq));
		} catch (Exception e) {
			log.error("通用公告管理-分页查询-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "新增通用公告", notes = "新增通用公告")
	@SysLog("新增通用公告")
	@PostMapping("/addCommonNotice")
	@PreAuthorize("@pms.hasPermission('common_notice_add')")
	public R addCommonNotice(@Valid @RequestBody CommonNoticeReq commonNoticeReq) {
		try {
			return popupNoticeService.addCommonNotice(commonNoticeReq);
		} catch (Exception e) {
			log.error("通用公告管理-新增通用公告-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "修改通用公告", notes = "修改通用公告")
	@SysLog("修改通用公告")
	@PostMapping("/editCommonNotice")
	@PreAuthorize("@pms.hasPermission('common_notice_edit')")
	public R editCommonNotice(@Valid @RequestBody CommonNoticeReq commonNoticeReq) {
		try {
			return popupNoticeService.editCommonNotice(commonNoticeReq);
		} catch (Exception e) {
			log.error("通用公告管理-修改通用公告-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "通过id删除通用公告", notes = "通过id删除通用公告")
	@SysLog("通过id删除通用公告")
	@GetMapping("/deleteCommonNotice/{id}")
	@PreAuthorize("@pms.hasPermission('common_notice_del')")
	public R deleteCommonNotice(@PathVariable Long id) {
		try {
			return R.ok(popupNoticeService.removeById(id));
		} catch (Exception e) {
			log.error("通用公告管理-通过id删除通用公告-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "上线/下线通用公告", notes = "上线/下线通用公告")
	@SysLog("上线/下线通用公告")
	@GetMapping("/onlineCommonNotice")
	@PreAuthorize("@pms.hasPermission('common_notice_online')")
	public R onlineCommonNotice(@Valid OnlineStatusReq req) {
		try {
			return popupNoticeService.onlineCommonNotice(req);
		} catch (Exception e) {
			log.error("通用公告管理-上线/下线通用公告-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "公告排序", notes = "公告排序")
	@SysLog("公告排序")
	@GetMapping("/sortCommonNotice")
	public R sortCommonNotice(@Valid NoticeSortReq req) {
		try {
			return popupNoticeService.sortCommonNotice(req);
		} catch (Exception e) {
			log.error("通用公告管理-公告排序-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

}
