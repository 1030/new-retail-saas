package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.WanUser;

/**
 * WanUser
 * @author  zhuxm
 * @version  2022-01-13 16:01:46
 * table: wan_user
 */
public interface WanUserService extends IService<WanUser> {
}


