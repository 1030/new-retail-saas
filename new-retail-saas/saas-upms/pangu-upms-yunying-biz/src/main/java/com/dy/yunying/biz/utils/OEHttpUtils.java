package com.dy.yunying.biz.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.util.Map;
import java.util.Set;


/**
 * @author zxm
 * @投放平台http 请求工具类
 */
public class OEHttpUtils {

	// private static final String[] TLS_VERSION = {"TLSv1", "TLSv1.1", "TLSv1.2", "TLSv1.3"};
	private static final String[] TLS_VERSION = {"TLSv1.2"};

	@SneakyThrows
	public static String doPost(String url, Map<String, Object> data) {

		// 构造请求
		HttpPost httpEntity = new HttpPost(url);

		CloseableHttpResponse response = null;
		CloseableHttpClient client = null;

		try {
			SSLConnectionSocketFactory ssl = new SSLConnectionSocketFactory(SSLContexts.createDefault(), TLS_VERSION, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			client = HttpClientBuilder.create().setSSLSocketFactory(ssl).build();
			httpEntity.setEntity(new StringEntity(JSONObject.toJSONString(data), ContentType.APPLICATION_JSON));

			response = client.execute(httpEntity);
			if (response != null && response.getStatusLine().getStatusCode() == 200) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuilder result = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					result.append(line);
				}
				bufferedReader.close();
				return UicodeBackslashUtils.unicodeToCn(result.toString());
			}

		} finally {
			if (response != null) {
				response.close();
			}
			if (null != client) {
				client.close();
			}
		}
		return null;
	}

	@SneakyThrows
	public static String doPost(String url, Map<String, Object> data, String access_token) {

		// 构造请求
		HttpPost httpEntity = new HttpPost(url);
		httpEntity.setHeader("Access-Token", access_token);

		CloseableHttpResponse response = null;
		CloseableHttpClient client = null;

		try {
			SSLConnectionSocketFactory ssl = new SSLConnectionSocketFactory(SSLContexts.createDefault(), TLS_VERSION, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			client = HttpClientBuilder.create().setSSLSocketFactory(ssl).build();
			httpEntity.setEntity(new StringEntity(JSONObject.toJSONString(data), ContentType.APPLICATION_JSON));

			response = client.execute(httpEntity);
			if (response != null && response.getStatusLine().getStatusCode() == 200) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuilder result = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					result.append(line);
				}
				bufferedReader.close();
				return UicodeBackslashUtils.unicodeToCn(result.toString());
			}

		} finally {
			if (response != null) {
				response.close();
			}
			if (null != client) {
				client.close();
			}
		}
		return null;
	}

	@SneakyThrows
	public static String doGet(String url, Map<String, Object> data, String access_token) {

		// 构造请求
		HttpEntityEnclosingRequestBase httpEntity = new HttpEntityEnclosingRequestBase() {
			@Override
			public String getMethod() {
				return "GET";
			}
		};
		httpEntity.setHeader("Access-Token", access_token);

		CloseableHttpResponse response = null;
		CloseableHttpClient client = null;

		try {
			SSLConnectionSocketFactory ssl = new SSLConnectionSocketFactory(SSLContexts.createDefault(), TLS_VERSION, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			client = HttpClientBuilder.create().setSSLSocketFactory(ssl).build();
			httpEntity.setURI(URI.create(url));
			httpEntity.setEntity(new StringEntity(JSONObject.toJSONString(data), ContentType.APPLICATION_JSON));

			response = client.execute(httpEntity);
			if (response != null && response.getStatusLine().getStatusCode() == 200) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuilder result = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					result.append(line);
				}
				bufferedReader.close();
				return UicodeBackslashUtils.unicodeToCn(result.toString());
			}

		} finally {
			if (response != null) {
				response.close();
			}
			if (null != client) {
				client.close();
			}
		}
		return null;
	}

	/**
	 * 广点通接口
	 *
	 * @author zxm
	 */
	@SneakyThrows
	public static String doGdtGet(String url, Map<String, String> headers) {
		HttpClient client = null;
		try {
			URL urlR = new URL(url);
			URI uri = new URI(urlR.getProtocol(), urlR.getHost(), urlR.getPath(), urlR.getQuery(), null);
			client = new DefaultHttpClient();
			HttpGet get = new HttpGet(uri);
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 10000);
			HttpConnectionParams.setSoTimeout(params, 10000);
			if (headers != null && headers.size() > 0) {
				Set<String> set = headers.keySet();
				for (String key : set) {
					get.addHeader(key, headers.get(key));
				}
			}
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			if (entity == null) {
				return null;
			}
			String stringResult = EntityUtils.toString(entity, "utf-8");
			return UicodeBackslashUtils.unicodeToCn(stringResult);
		} finally {
			if (client != null) {
				client.getConnectionManager().shutdown();
			}
		}
	}


	/**
	 * @文件上传,头条和广点通通用。2个参数是广点通调用，3个参数头条调用
	 * @zhuxm
	 */
	@SneakyThrows
	public static String doFilePost(String url, MultipartEntityBuilder entityBuilder) {
		return doFilePost(url, entityBuilder, null);
	}

	/**
	 * @文件上传,头条和广点通通用。2个参数是广点通调用，3个参数头条调用
	 * @zhuxm
	 */
	@SneakyThrows
	public static String doFilePost(String url, MultipartEntityBuilder entityBuilder, String access_token) {
		// 构造请求
		HttpPost httpPost = new HttpPost(url);
		//文件参数和其他参数：entityBuilder
		HttpEntity entity = entityBuilder.build();
		CloseableHttpResponse response = null;
		CloseableHttpClient client = null;
		try {
			SSLConnectionSocketFactory ssl = new SSLConnectionSocketFactory(SSLContexts.createDefault(), TLS_VERSION, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			client = HttpClientBuilder.create().setSSLSocketFactory(ssl).build();
			httpPost.setURI(URI.create(url));
			httpPost.setEntity(entity);
			if (StringUtils.isNotBlank(access_token)) {
				httpPost.setHeader("Access-Token", access_token);
			}
			response = client.execute(httpPost);
			if (response != null && response.getStatusLine().getStatusCode() == 200) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuilder result = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					result.append(line);
				}
				bufferedReader.close();
				return result.toString();
			}
		} finally {
			if (response != null) {
				response.close();
			}
			if (null != client) {
				client.close();
			}
		}
		return null;
	}

}
