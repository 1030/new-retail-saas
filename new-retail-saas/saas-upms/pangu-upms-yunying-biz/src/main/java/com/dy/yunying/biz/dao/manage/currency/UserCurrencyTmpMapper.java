package com.dy.yunying.biz.dao.manage.currency;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.currency.UserCurrencyTmp;
import org.apache.ibatis.annotations.Mapper;

/**
* @author hejiale
* @description 针对表【user_currency_tmp(账号临时平台币信息)】的数据库操作Mapper
* @createDate 2022-03-25 09:56:35
* @Entity generate.entity.UserCurrencyTmp
*/
@Mapper
public interface UserCurrencyTmpMapper extends BaseMapper<UserCurrencyTmp> {

}




