package com.dy.yunying.biz.service.yyz.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.GameAppointmentScheduleGears;
import com.dy.yunying.api.entity.GameAppointmentScheduleGearsPage;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReward;
import com.dy.yunying.biz.dao.manage.GameAppointmentScheduleGearsRewardMapper;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleGearsRewardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 游戏预约进度表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_Schedule
 */
@Log4j2
@Service("GameAppointmentScheduleGearsRewardService")
@RequiredArgsConstructor
public class GameAppointmentScheduleGearsRewardServiceImpl extends ServiceImpl<GameAppointmentScheduleGearsRewardMapper, GameAppointmentScheduleGearsReward> implements GameAppointmentScheduleGearsRewardService {

	private final GameAppointmentScheduleGearsRewardMapper gameAppointmentScheduleGearsRewardMapper;

	@Override
	public List<GameAppointmentScheduleGearsReward> selectScheduleGearsReward(GameAppointmentScheduleGearsPage req) {
		List<GameAppointmentScheduleGearsReward> scheduleGearsRewards = gameAppointmentScheduleGearsRewardMapper.selectScheduleGearsReward(req);
		return scheduleGearsRewards;
	}

}


