/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.controller.common;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dy.yunying.api.entity.AdRoleUser;
import com.dy.yunying.api.req.AdRoleUserReq;
import com.dy.yunying.biz.service.manage.AdRoleUserService;
import com.pig4cloud.pig.admin.api.feign.RemoteUserService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 角色用户
 *
 * @author zhuxm
 * @date 2021-06-04
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/roleuser" )
@Api(value = "roleuser", tags = "角色可查看的平台用户")
public class AdRoleUserController {

	private final AdRoleUserService adRoleUserService;

	private final RemoteUserService remoteUserService;




	/**
	 * 当前权限id下的平台账号列表
	 * @param
	 * @return
	 */
	@RequestMapping("/detail/{roleId}")
	public R getList(@PathVariable String roleId){
		List<AdRoleUser> list = adRoleUserService.list(Wrappers.<AdRoleUser>query().lambda().eq(AdRoleUser::getRoleId, roleId));
		Set<Integer> userList = list.stream().map(AdRoleUser::getUserId).collect(Collectors.toSet());
		//全账号
		if(userList.contains(0)){
			return R.ok(0);
		}
		String users = StringUtils.join(userList, ",");
		return R.ok(users);
	}


	/**
	 * 保存角色平台账号
	 *
	 * @param adRoleUserReq
	 * @return success/false
	 */
	@SysLog("保存角色平台账号")
	@PreAuthorize("@pms.hasPermission('sys_role_user*')")
	@RequestMapping("/save")
	public R save(@RequestBody @Valid AdRoleUserReq adRoleUserReq) {
		try {
			adRoleUserService.saveRoleUser(adRoleUserReq);
		} catch (Throwable a) {
			log.error("保存失败", a);
			return R.failed("保存失败");
		}
		return R.ok();
	}
}
