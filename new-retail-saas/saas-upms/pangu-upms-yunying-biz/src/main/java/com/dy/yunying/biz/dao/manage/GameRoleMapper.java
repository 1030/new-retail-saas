package com.dy.yunying.biz.dao.manage;

import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.GameRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 角色信息表
 * @author  chengang
 * @version  2021-10-28 13:35:09
 * table: game_role
 */
@Mapper
public interface GameRoleMapper extends BaseMapper<GameRole> {

	/**
	 * 查询roleIds
	 * @param roleName
	 * @return
	 */
	Set<String> getGameRoleIds(String roleName);

	/**
	 * 查询GameRoleByPgids
	 * @param ids
	 * @return
	 */
	List<GameRole> getGameRoleByPgids(@Param("ids") Set<Long> ids);

}


