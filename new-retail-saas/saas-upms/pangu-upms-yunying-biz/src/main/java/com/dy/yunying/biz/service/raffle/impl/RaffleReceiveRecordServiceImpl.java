package com.dy.yunying.biz.service.raffle.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.entity.WanGameDO;
import com.dy.yunying.api.entity.raffle.RaffleReceiveRecord;
import com.dy.yunying.api.req.ParentGameReq;
import com.dy.yunying.api.req.raffle.RaffleReceiveRecordReq;
import com.dy.yunying.api.resp.raffle.RaffleReceiveRecordExportRes;
import com.dy.yunying.api.resp.raffle.RaffleReceiveRecordRes;
import com.dy.yunying.biz.dao.ads.raffle.RaffleReceiveRecordMapper;
import com.dy.yunying.biz.service.manage.GameRoleService;
import com.dy.yunying.biz.service.manage.GameService;
import com.dy.yunying.biz.service.manage.ParentGameService;
import com.dy.yunying.biz.service.raffle.RaffleReceiveRecordService;
import com.dy.yunying.biz.utils.DateUtils;
import com.google.api.client.util.Lists;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 领取记录信息表
 *
 * @author chenxiang
 * @version 2022-11-08 16:01:24
 * table: raffle_receive_record
 */
@Log4j2
@Service("raffleReceiveRecordService")
@RequiredArgsConstructor
public class RaffleReceiveRecordServiceImpl extends ServiceImpl<RaffleReceiveRecordMapper, RaffleReceiveRecord> implements RaffleReceiveRecordService {

	private final RaffleReceiveRecordMapper raffleReceiveRecordMapper;

	private final ParentGameService parentGameService;

	private final GameService gameService;

	private final GameRoleService gameRoleService;

	/**
	 * 分页
	 * @param record
	 * @return
	 */
	@Override
	 public R getPage(RaffleReceiveRecordReq record){
		IPage<RaffleReceiveRecordRes> iPage = raffleReceiveRecordMapper.selectReceiveRecordByPage(record);
		// 处理数据
		this.dealData(iPage.getRecords());
	 	return R.ok(iPage);
	 }

	/**
	 * 编辑
	 * @param record
	 * @return
	 */
	@Override
	 public R edit(RaffleReceiveRecord record){
		return this.updateById(record) ? R.ok() : R.failed();
	 }

	/**
	 * 处理列表数据
	 *
	 * @param list
	 */
	public void dealData(List<RaffleReceiveRecordRes> list) {
		try {
			if (CollectionUtils.isNotEmpty(list)) {
				// 父游戏集合
				Collection<Long> parentGameIds = list.stream().map(item -> {
					return Long.valueOf(String.valueOf(item.getParentGameId()));
				}).collect(Collectors.toSet());
				List<ParentGameDO> parentGameList = parentGameService.getCacheablePGameList(new ParentGameReq().setIds(parentGameIds));
				// 子游戏集合
				Collection<Long> gameIds = list.stream().map(item -> {
					return Long.valueOf(String.valueOf(item.getSubGameId()));
				}).collect(Collectors.toSet());
				gameIds.add(0L);
				List<WanGameDO> gameList = gameService.list(Wrappers.<WanGameDO>query().lambda().in(WanGameDO::getId, gameIds));

				for (RaffleReceiveRecordRes receivingRecordDto : list) {
					// 获取父游戏名称
					if (Objects.nonNull(parentGameList) && parentGameList.size() > 0) {
						parentGameList.forEach(game -> {
							if (receivingRecordDto.getParentGameId().intValue() == game.getId().intValue()) {
								receivingRecordDto.setParentGameName(game.getGname());
							}
						});
					}
					// 获取子游戏名称
					if (Objects.nonNull(gameList) && gameList.size() > 0) {
						gameList.forEach(game -> {
							if (receivingRecordDto.getSubGameId().intValue() == game.getId().intValue()) {
								receivingRecordDto.setGameName(game.getGname());
							}
						});
					}
					if (2 == receivingRecordDto.getGiftType()){
						receivingRecordDto.setGiftCode(null);
					}
					// 收货地址
					receivingRecordDto.setUserAddress(
							  (StringUtils.isNotBlank(receivingRecordDto.getReceiptProvinceName()) ? receivingRecordDto.getReceiptProvinceName() : "")
							+ (StringUtils.isNotBlank(receivingRecordDto.getReceiptCityName()) ? receivingRecordDto.getReceiptCityName() : "")
							+ (StringUtils.isNotBlank(receivingRecordDto.getReceiptAreaName()) ? receivingRecordDto.getReceiptAreaName() : "")
							+ (StringUtils.isNotBlank(receivingRecordDto.getReceiptAddress()) ? receivingRecordDto.getReceiptAddress() : "")
					);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 导出列表
	 * @param record
	 * @return
	 */
	@Override
	public List<RaffleReceiveRecordExportRes> selectExportList(RaffleReceiveRecordReq record){
		List<RaffleReceiveRecordExportRes> list = Lists.newArrayList();
		record.setSize(9999999L);
		List<RaffleReceiveRecordRes> raffleReceiveRecordResList = raffleReceiveRecordMapper.selectReceiveRecordByList(record);
		// 处理数据
		this.dealData(raffleReceiveRecordResList);
		if (CollectionUtils.isNotEmpty(raffleReceiveRecordResList)){
			for (RaffleReceiveRecordRes receiveRecordRes : raffleReceiveRecordResList){
				RaffleReceiveRecordExportRes recordExportRes = new RaffleReceiveRecordExportRes();
				recordExportRes.setUsername(receiveRecordRes.getUsername());
				recordExportRes.setActivityName(receiveRecordRes.getActivityName());
				recordExportRes.setParentGameName(receiveRecordRes.getParentGameName());
				recordExportRes.setAreaId(receiveRecordRes.getAreaId());
				recordExportRes.setRoleName(receiveRecordRes.getRoleName());
				recordExportRes.setRoleId(receiveRecordRes.getRoleId());
				recordExportRes.setObjectName(receiveRecordRes.getObjectName());
				recordExportRes.setGiftAmount(receiveRecordRes.getGiftAmount());
				recordExportRes.setReceiveTime(DateUtils.dateToString(receiveRecordRes.getReceiveTime(), DateUtils.YYYY_MM_DD_HH_MM_SS));
				recordExportRes.setRecordStatus(this.recordStatusName(receiveRecordRes.getRecordStatus()));
				recordExportRes.setForbidReason(receiveRecordRes.getForbidReason());
				recordExportRes.setUpdateName(receiveRecordRes.getUpdateName());
				recordExportRes.setUpdateTime(DateUtils.dateToString(receiveRecordRes.getUpdateTime(), DateUtils.YYYY_MM_DD_HH_MM_SS));
				recordExportRes.setReceiptUsername(receiveRecordRes.getReceiptUsername());
				recordExportRes.setReceiptPhone(receiveRecordRes.getReceiptPhone());
				recordExportRes.setUserAddress(receiveRecordRes.getUserAddress());
				recordExportRes.setExpressName(receiveRecordRes.getExpressName());
				recordExportRes.setExpressCode(receiveRecordRes.getExpressCode());
				list.add(recordExportRes);
			}
		}
		return list;
	}
	private String recordStatusName(Integer recordStatus){
		if (Objects.isNull(recordStatus)){
			return "";
		}
		if (1 == recordStatus){
			return "未填写地址";
		}else if(2 == recordStatus){
			return "待审核";
		}else if(3 == recordStatus){
			return "驳回";
		}else if(4 == recordStatus){
			return "已发货";
		}
		return "";
	}
}


