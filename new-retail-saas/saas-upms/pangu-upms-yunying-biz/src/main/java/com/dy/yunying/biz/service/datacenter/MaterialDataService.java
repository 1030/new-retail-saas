package com.dy.yunying.biz.service.datacenter;

import com.baomidou.mybatisplus.extension.api.R;
import com.dy.yunying.api.dto.MaterialDataDTO;
import com.dy.yunying.api.vo.MaterialDataVO;

import java.util.List;

/**
 * 素材数据报表Service
 */
public interface MaterialDataService {

	/**
	 * 查询数据报表总数
	 *
	 * @param material
	 * @return
	 */
	Integer count(MaterialDataVO material);

	/**
	 * 查询数据报表列表
	 *
	 * @param material
	 * @param paged
	 * @return
	 */
	List<MaterialDataDTO> list(MaterialDataVO material, boolean paged);

	/**
	 * 素材管理-数据
	 * @param material
	 * @return
	 */
	R materialData(MaterialDataVO material);
}
