package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbActivityCenterGame;
import org.apache.ibatis.annotations.Mapper;

/**
 * 活动中心与游戏关系表
 * @author  chenxiang
 * @version  2022-06-18 11:46:38
 * table: hb_activity_center_game
 */
@Mapper
public interface HbActivityCenterGameMapper extends BaseMapper<HbActivityCenterGame> {
}


