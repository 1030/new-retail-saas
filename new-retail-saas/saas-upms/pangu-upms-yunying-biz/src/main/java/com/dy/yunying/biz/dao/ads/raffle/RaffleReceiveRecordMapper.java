package com.dy.yunying.biz.dao.ads.raffle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.raffle.RaffleReceiveRecord;
import com.dy.yunying.api.req.raffle.RaffleReceiveRecordReq;
import com.dy.yunying.api.resp.raffle.RaffleReceiveRecordRes;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 领取记录信息表
 * @author  chenxiang
 * @version  2022-11-08 16:01:24
 * table: raffle_receive_record
 */
@Mapper
public interface RaffleReceiveRecordMapper extends BaseMapper<RaffleReceiveRecord> {

	IPage<RaffleReceiveRecordRes> selectReceiveRecordByPage(RaffleReceiveRecordReq record);

	List<RaffleReceiveRecordRes> selectReceiveRecordByList(RaffleReceiveRecordReq record);
}


