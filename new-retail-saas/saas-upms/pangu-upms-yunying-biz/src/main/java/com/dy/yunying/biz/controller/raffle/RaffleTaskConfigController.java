package com.dy.yunying.biz.controller.raffle;

import com.dy.yunying.api.entity.raffle.RaffleTaskConfig;
import com.dy.yunying.api.enums.HbConditionTypeEnum;
import com.dy.yunying.api.enums.RewardTypeEnum;
import com.dy.yunying.api.req.raffle.RaffleTaskConfigReq;
import com.dy.yunying.api.utils.CheckUtils;
import com.dy.yunying.biz.service.raffle.RaffleTaskConfigService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 抽奖任务配置表
 * @author  chenxiang
 * @version  2022-11-08 15:59:48
 * table: raffle_task_config
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/raffleTaskConfig")
public class RaffleTaskConfigController {
	
    private final RaffleTaskConfigService raffleTaskConfigService;

	/**
	 * 列表
	 * @param record
	 * @return
	 */
	@RequestMapping("/list")
	@PreAuthorize("@pms.hasPermission('RAFFLE_TASK_LIST')")
	public R getList(@RequestBody RaffleTaskConfigReq record){
		if (StringUtils.isBlank(record.getActivityId())){
			return R.failed("活动ID不能为空");
		}
		return raffleTaskConfigService.getList(record);
	}
	/**
	 * 新增
	 * @param record
	 * @return
	 */
	@RequestMapping("/add")
	@SysLog("抽奖任务新增")
	@PreAuthorize("@pms.hasPermission('RAFFLE_TASK_ADD')")
	public R add(@RequestBody RaffleTaskConfigReq record){
		RaffleTaskConfig raffleTaskConfig = new RaffleTaskConfig();
		raffleTaskConfig.setCreateId(SecurityUtils.getUser().getId().longValue());
		raffleTaskConfig.setCreateTime(new Date());
		R result = this.checkParam(record, raffleTaskConfig);
		if (0 != result.getCode()){
			return result;
		}
		return raffleTaskConfigService.addOrEdit(raffleTaskConfig);
	}
	/**
	 * 编辑
	 * @param record
	 * @return
	 */
	@RequestMapping("/edit")
	@SysLog("抽奖任务编辑")
	@PreAuthorize("@pms.hasPermission('RAFFLE_TASK_EDIT')")
	public R edit(@RequestBody RaffleTaskConfigReq record){
		if (StringUtils.isBlank(record.getId())){
			return R.failed("任务ID不能为空");
		}
		RaffleTaskConfig raffleTaskConfig = new RaffleTaskConfig();
		raffleTaskConfig.setId(Long.valueOf(record.getId()));
		raffleTaskConfig.setUpdateId(SecurityUtils.getUser().getId().longValue());
		raffleTaskConfig.setUpdateTime(new Date());
		R result = this.checkParam(record, raffleTaskConfig);
		if (0 != result.getCode()){
			return result;
		}
		return raffleTaskConfigService.addOrEdit(raffleTaskConfig);
	}
	/**
	 * 删除
	 * @param record
	 * @return
	 */
	@RequestMapping("/del")
	@SysLog("抽奖任务删除")
	@PreAuthorize("@pms.hasPermission('RAFFLE_TASK_DEL')")
	public R del(@RequestBody RaffleTaskConfigReq record){
		if (StringUtils.isBlank(record.getId())){
			return R.failed("任务ID不能为空");
		}
		return raffleTaskConfigService.del(record);
	}
	/**
	 * 参数校验
	 * @param record
	 * @return
	 */
	public R checkParam(RaffleTaskConfigReq record, RaffleTaskConfig raffleTaskConfig){
		raffleTaskConfig.setConditionRoleLevel(0);
		raffleTaskConfig.setConditionRoleDays(0);
		raffleTaskConfig.setConditionRoleRecharge(new BigDecimal(0));
		raffleTaskConfig.setConditionReachNum(1);
		raffleTaskConfig.setConditionLimitNum(1);
		raffleTaskConfig.setRewardNum(0);
		if (StringUtils.isBlank(record.getActivityId())){
			return R.failed("活动ID不能为空");
		}
		if (StringUtils.isBlank(HbConditionTypeEnum.getName(record.getConditionType()))){
			return R.failed("任务达成条件类型无效");
		}
		// 验证等级
		if (HbConditionTypeEnum.Condition1.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition2.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition9.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition15.getType().equals(record.getConditionType())){
			String conditionRoleLevel = record.getConditionRoleLevel();
			if (!CheckUtils.checkInt(conditionRoleLevel)){
				return R.failed("请输入有效的角色等级");
			}
			if (Integer.valueOf(conditionRoleLevel) < 1 || Integer.valueOf(conditionRoleLevel) > 999){
				return R.failed("角色等级应为1~999");
			}
			raffleTaskConfig.setConditionRoleLevel(Integer.valueOf(conditionRoleLevel));
		}
		// 验证天数
		if (HbConditionTypeEnum.Condition2.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition9.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition12.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition13.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition14.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition15.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition16.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition17.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition19.getType().equals(record.getConditionType())){
			String conditionRoleDays = record.getConditionRoleDays();
			if (!CheckUtils.checkInt(conditionRoleDays)){
				return R.failed("请输入有效的天数");
			}
			if (Integer.valueOf(conditionRoleDays) < 1 || Integer.valueOf(conditionRoleDays) > 999){
				return R.failed("天数值应为1~999");
			}
			raffleTaskConfig.setConditionRoleDays(Integer.valueOf(conditionRoleDays));
		}
		// 验证金额
		if (HbConditionTypeEnum.Condition3.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition5.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition8.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition11.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition13.getType().equals(record.getConditionType())
				|| HbConditionTypeEnum.Condition16.getType().equals(record.getConditionType())){
			String conditionRoleRecharge = record.getConditionRoleRecharge();
			if (!CheckUtils.checkMoney(conditionRoleRecharge)){
				return R.failed("请输入有效的金额");
			}
			if (Double.valueOf(conditionRoleRecharge) < 1 || Double.valueOf(conditionRoleRecharge) > 99999){
				return R.failed("金额值应为1~99999");
			}
			raffleTaskConfig.setConditionRoleRecharge(new BigDecimal(conditionRoleRecharge));
		}
		// 游戏任务ID
		if (HbConditionTypeEnum.Condition18.getType().equals(record.getConditionType())){
			String conditionTaskId = record.getConditionTaskId();
			if (StringUtils.isBlank(conditionTaskId)){
				return R.failed("请选择游戏任务");
			}
			raffleTaskConfig.setConditionTaskId(conditionTaskId);
			// 任务名称
			raffleTaskConfig.setTaskName(record.getTaskName());
		}else {
			// 任务名称
			raffleTaskConfig.setTaskName(HbConditionTypeEnum.getDesc(record.getConditionType(), record.getConditionRoleLevel(), record.getConditionRoleDays(), record.getConditionRoleRecharge(), record.getConditionReachNum()));
		}
		// 任务可完成次数
		if (HbConditionTypeEnum.Condition11.getType().equals(record.getConditionType())){
			String conditionLimitNum = record.getConditionLimitNum();
			if (!CheckUtils.checkInt(conditionLimitNum)){
				return R.failed("请输入有效的次数");
			}
			if (Integer.valueOf(conditionLimitNum) < 1 || Integer.valueOf(conditionLimitNum) > 99999){
				return R.failed("次数值应为1~99999");
			}
			raffleTaskConfig.setConditionLimitNum(Integer.valueOf(conditionLimitNum));
		}
		// 条件达成次数
		if (HbConditionTypeEnum.Condition20.getType().equals(record.getConditionType())){
			String conditionReachNum = record.getConditionReachNum();
			if (!CheckUtils.checkInt(conditionReachNum)){
				return R.failed("请输入有效的次数");
			}
			if (Integer.valueOf(conditionReachNum) < 1 || Integer.valueOf(conditionReachNum) > 99999){
				return R.failed("次数值应为1~99999");
			}
			raffleTaskConfig.setConditionReachNum(Integer.valueOf(conditionReachNum));
		}
		// 奖励类型
		if (StringUtils.isBlank(record.getRewardType())){
			return R.failed("请选择奖励类型");
		}

		if (RewardTypeEnum.TYPE1.getType().equals(record.getRewardType())){
			String rewardNum = record.getRewardNum();
			if (!CheckUtils.checkInt(rewardNum)){
				return R.failed("请输入有效的天数");
			}
			if (Integer.valueOf(rewardNum) < 1 || Integer.valueOf(rewardNum) > 999){
				return R.failed("天数值应为1~999");
			}
			raffleTaskConfig.setRewardNum(Integer.valueOf(rewardNum));
		}
		raffleTaskConfig.setActivityId(Long.valueOf(record.getActivityId()));
		raffleTaskConfig.setConditionType(Integer.valueOf(record.getConditionType()));
		raffleTaskConfig.setRewardType(Integer.valueOf(record.getRewardType()));
		return R.ok();
	}
}


