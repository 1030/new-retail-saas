package com.dy.yunying.biz.service.manage.impl;

import com.dy.yunying.api.entity.WanGameChannelPlatformDO;
import com.dy.yunying.api.vo.ChannelManageVo;
import com.dy.yunying.biz.dao.manage.ext.WanGameChannelPlatformDOMapperExt;
import com.dy.yunying.biz.service.manage.ChannelManageService;
import com.dy.yunying.biz.service.manage.GameChannelPlatformService;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: hjl
 * @Date: 2020/7/21 13:52
 */
@Slf4j
@Transactional
@Service
public class GameChannelPlatformServiceImpl implements GameChannelPlatformService {

	@Autowired
	private WanGameChannelPlatformDOMapperExt wanGameChannelPlatformDOMapperExt;

	@Autowired
	private ChannelManageService channelManageService;

	@Override
	public WanGameChannelPlatformDO getByPlatformAndGameid(Integer platformid, Integer gameid, Integer appid) {
		return wanGameChannelPlatformDOMapperExt.getByPlatformAndGameid(platformid, gameid, appid);
	}

	@Override
	public WanGameChannelPlatformDO getByGameidAndParentChlId(Integer gameId, Integer parentChlId) {
		ChannelManageVo parentChannel = channelManageService.getVOByPK(parentChlId);
		if (parentChannel == null) {
			log.error("主渠道不存在， parentChlId：{}", parentChlId);
			throw new BusinessException(13, "主渠道不存在");
		}
		return wanGameChannelPlatformDOMapperExt.getByPlatformAndGameid(parentChannel.getPlatform(), gameId, null);
	}

	@Override
	public WanGameChannelPlatformDO saveByUK(WanGameChannelPlatformDO wanGameChannelPlatformDO) {
		WanGameChannelPlatformDO wanGameChannelPlatformDB = getByPlatformAndGameid(wanGameChannelPlatformDO.getPlatformid(), wanGameChannelPlatformDO.getGameid(), null);
		if (wanGameChannelPlatformDB != null) {
			wanGameChannelPlatformDO.setId(wanGameChannelPlatformDB.getId());
			wanGameChannelPlatformDOMapperExt.updateByPrimaryKeySelective(wanGameChannelPlatformDO);
		} else {
			wanGameChannelPlatformDOMapperExt.insertSelective(wanGameChannelPlatformDO);
		}
		return wanGameChannelPlatformDO;
	}

}