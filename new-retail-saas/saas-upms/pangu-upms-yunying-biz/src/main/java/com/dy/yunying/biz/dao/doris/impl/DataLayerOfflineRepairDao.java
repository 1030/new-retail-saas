package com.dy.yunying.biz.dao.doris.impl;

import com.dy.yunying.api.datacenter.vo.AdDataAnalysisVO;
import com.dy.yunying.api.datacenter.vo.DataLayerOfflineRepairVO;
import com.dy.yunying.api.enums.DataReportEnum;
import com.dy.yunying.biz.config.YunYingDorisTableProperties;
import com.dy.yunying.biz.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName DataLayerOfflineRepairDao
 * @Description todo
 * @Author nieml
 * @Time 2023/1/10 17:29
 * @Version 1.0
 **/
@Slf4j
@Component(value = "dataLayerOfflineRepairDao")
public class DataLayerOfflineRepairDao {

	@Resource(name = "dataLayerDorisTemplate")
	private JdbcTemplate dorisTemplate;
	@Resource
	private YunYingDorisTableProperties yunYingDorisTableProperties;
	final private String indentStr = "		";
	final private String indentStr1 = "		";
	final private String indentStr2 = "				";
	final private String indentStr3 = "						";

	public String getAdsOperation3399AdReportSql(Long rsTime,Long reTime,String filterDayStr,String dataReportType){
		String adsOperation3399AdReportTable = yunYingDorisTableProperties.getAdsOperation3399AdReport();
		String adsOperation3399AdReportOfflineTable = yunYingDorisTableProperties.getAdsOperation3399AdReportOffline();
		String sql = "SELECT table_name as tableName,day,hour FROM %s where table_name = '%s' limit 1";
		String querySql = String.format(sql,yunYingDorisTableProperties.getDim3399OfflineRepair(),adsOperation3399AdReportTable);
		log.info("getAdsOperation3399AdReportSql-querySql:{}",querySql);
		List<DataLayerOfflineRepairVO> resList = dorisTemplate.query(querySql, new Object[]{}, new BeanPropertyRowMapper<>(DataLayerOfflineRepairVO.class));

		String fieldsSql = getFieldsSql(filterDayStr,dataReportType);

		StringBuilder tableSql = new StringBuilder();
		if (CollectionUtils.isEmpty(resList) || resList.size() < 1 ){
			tableSql.append(" ( ");
			tableSql.append(" select  ").append(fieldsSql).append(" from ").append(adsOperation3399AdReportTable)
					.append(" where ").append(filterDayStr).append(" between to_date(").append(rsTime).append(")").append(" and ").append(" to_date(").append(reTime).append(")");
			tableSql.append(" ) ar ");
			return tableSql.toString();
		}

		DataLayerOfflineRepairVO dataLayerOfflineRepairVO = resList.get(0);
		Date day = dataLayerOfflineRepairVO.getDay();
		int hour = dataLayerOfflineRepairVO.getHour();
		String dayStr = DateUtils.dateToString(day);

		tableSql.append(" ( ");
		tableSql.append(" select ").append(fieldsSql).append(" from ").append(adsOperation3399AdReportTable)
				.append(" where  active_day > hours_add(str_to_date('").append(dayStr).append("','%Y-%m-%d %H:%i:%s'),").append("(").append(hour).append(" - hour)").append(")")
				.append(" and ").append(filterDayStr).append(" between to_date(").append(rsTime).append(")").append(" and ").append(" to_date(").append(reTime).append(")");
		tableSql.append(" union all ");
		tableSql.append(" select ").append(fieldsSql).append(" from ").append(adsOperation3399AdReportOfflineTable)
				.append(" where  active_day <= hours_add(str_to_date('").append(dayStr).append("','%Y-%m-%d %H:%i:%s'),").append("(").append(hour).append(" - hour)").append(")")
				.append(" and ").append(filterDayStr).append(" between to_date(").append(rsTime).append(")").append(" and ").append(" to_date(").append(reTime).append(")");
		tableSql.append(" ) ar ");
		log.info("getAdsOperation3399AdReportSql-sql:{}",tableSql.toString());
		return tableSql.toString();
	}

	String getFieldsSql(String filterDayStr,String dataReportName){
		String fieldsSql = "";
		if (dataReportName.equals(DataReportEnum.ADANALYSIS.getType())
				|| dataReportName.equals(DataReportEnum.ADDATA.getType())
		){
			if (filterDayStr.equals(yunYingDorisTableProperties.getREG_DAY())){
				fieldsSql = yunYingDorisTableProperties.getAdsOperation3399AdReportRegFields();
			}
			if (filterDayStr.equals(yunYingDorisTableProperties.getACTIVE_DAY())){
				fieldsSql = yunYingDorisTableProperties.getAdsOperation3399AdReportActiveFields();
			}
		}
		if (dataReportName.equals(DataReportEnum.HOURDATA.getType()) ){
			fieldsSql = yunYingDorisTableProperties.getAdsOperation3399AdReportHourDataFields();
		}

		return fieldsSql;
	}


}
