package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.dto.currency.CurrencyOrderDTO;
import com.dy.yunying.api.entity.WanRechargeOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户充值记录表
 *
 * @author zhuxm
 * @version 2022-01-13 16:02:25
 * table: wan_recharge_order
 */
@Mapper
public interface WanRechargeOrderMapper extends BaseMapper<WanRechargeOrder> {

	Page<CurrencyOrderDTO> currencyOrderPage(Page<Object> page, @Param("record") CurrencyOrderDTO record);

	List<CurrencyOrderDTO> currencyOrderList(CurrencyOrderDTO record);

	Map<String, Object> currencyOrderTotal(CurrencyOrderDTO record);

}


