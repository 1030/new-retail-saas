package com.dy.yunying.biz.service.sign.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.sign.SignTaskGoodsDO;
import com.dy.yunying.biz.dao.ads.sign.SignTaskGoodsMapper;
import com.dy.yunying.biz.service.sign.SignTaskGoodsService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class SignTaskGoodsServiceImpl extends ServiceImpl<SignTaskGoodsMapper, SignTaskGoodsDO> implements SignTaskGoodsService {

}




