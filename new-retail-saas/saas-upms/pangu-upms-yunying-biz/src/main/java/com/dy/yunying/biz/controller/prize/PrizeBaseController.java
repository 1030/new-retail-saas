package com.dy.yunying.biz.controller.prize;

import com.dy.yunying.api.enums.*;
import com.dy.yunying.api.req.prize.PrizeConfigReq;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.StringUtil;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.util.CommonUtils;
import com.pig4cloud.pig.common.core.util.R;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * 奖励BaseController
 *
 * @author sunyouquan
 * @date 2022/5/6 11:06
 */

public class PrizeBaseController {

	private final String FILE_SUFFIX = "TXT";

	private final Long FILE_MIN_SIZE = 3L;

	private final Long FILE_MAX_SIZE = 1048576L;

	private final Integer LIMIT_MONEY_MAX_LENGTH = 6;

	private static final Pattern GIFT_CODE_PATTERN = Pattern.compile("^[A-Za-z0-9]{3,50}$");

	protected R checkPrizeConfigParam(PrizeConfigReq configReq) {

		R result;
		if (StringUtil.isBlank(configReq.getPrizeType())) {
			return R.failed("奖励类型为空");
		}
		if (StringUtil.isBlank(configReq.getIcon())) {
			return R.failed("奖励Icon为空！");
		}
		if (StringUtil.isBlank(configReq.getTitle())) {
			return R.failed("奖励名称不能为空");
		}
		if (StringUtil.isBlank(configReq.getDescription())) {
			return R.failed("奖励说明不能为空");
		}

		PrizeTypeEnum prizeTypeEnum = PrizeTypeEnum.getPrizeTypeEnum(Integer.valueOf(configReq.getPrizeType()));
		switch (prizeTypeEnum) {
			case VOUCHER:
				result = checkVoucherParams(configReq);
				break;
			case SWIMBEAN:
				result = checkSwimBeanParams(configReq);
				break;
			case CAMILO:
			case BAGCODE:
				result = checkBagCodeOrCamiloParams(configReq);
				break;
			default:
				result = R.failed("奖励类型不在合理范围内");
				break;
		}
		return result;
	}

	/**
	 * 礼包码、卡密参数校验
	 *
	 * @param configReq
	 * @return
	 */
	private R checkBagCodeOrCamiloParams(PrizeConfigReq configReq) {
		R baseCheck = baseCheck(configReq);
		if (CommonConstants.FAIL.equals(baseCheck.getCode())) {
			return baseCheck;
		}
		return R.ok();
	}

	/**
	 * 游豆参数校验
	 *
	 * @param configReq
	 * @return
	 */
	private R checkSwimBeanParams(PrizeConfigReq configReq) {
		R baseCheck = baseCheck(configReq);
		if (CommonConstants.FAIL.equals(baseCheck.getCode())) {
			return baseCheck;
		}
		if (StringUtils.isBlank(configReq.getMoney())) {
			return R.failed("游豆数量不能为空！");
		}
		if (!CommonUtils.isPositiveNumber(configReq.getMoney())) {
			return R.failed("游豆数量格式不正确");
		}
		return R.ok();
	}

	/**
	 * 代金卷参数校验
	 *
	 * @param configReq
	 * @return
	 */
	private R checkVoucherParams(PrizeConfigReq configReq) {
		// 游豆、代金券奖励按钮只能去选择去使用
		R baseCheck = baseCheck(configReq);
		if (CommonConstants.FAIL.equals(baseCheck.getCode())) {
			return baseCheck;
		}

		if (StringUtils.isBlank(configReq.getMoney())) {
			return R.failed("代金券金额不能为空！");
		}
		if (!CommonUtils.isMoney(configReq.getMoney(), 0)) {
			return R.failed("代金券金额格式不正确");
		}
		if (StringUtils.isBlank(configReq.getLimitMoney())) {
			return R.failed("满可用金额不能为空");
		}

		if (!CommonUtils.isMoney(configReq.getLimitMoney(), 0)) {
			return R.failed("满可用金额格式不正确");
		}

		if (configReq.getLimitMoney().trim().length() > LIMIT_MONEY_MAX_LENGTH) {
			return R.failed("满可用金额长度不能超过6位");
		}
		BigDecimal cdkAmount = new BigDecimal(configReq.getMoney());
		BigDecimal cdkLimitAmount = new BigDecimal(configReq.getLimitMoney());
		if (cdkLimitAmount.compareTo(cdkAmount) < 0) {
			return R.failed("满可用金额必须大于代金券金额");
		}
		return R.ok();
	}

	private R baseCheck(PrizeConfigReq configReq) {
		if (StringUtil.isBlank(configReq.getPrizeButton())) {
			return R.failed("奖励按钮不能为空");
		}
		PrizeTypeEnum prizeTypeEnum = PrizeTypeEnum.getPrizeTypeEnum(Integer.valueOf(configReq.getPrizeType()));
		if (prizeTypeEnum == PrizeTypeEnum.SWIMBEAN || prizeTypeEnum == PrizeTypeEnum.VOUCHER) {
			if (PrizeButtonEnum
					.getPrizeButtonEnum(Integer.valueOf(configReq.getPrizeButton())) != PrizeButtonEnum.USE) {
				return R.failed("游豆、代金券时，奖励按钮只能选择去使用！");
			}
			if (StringUtil.isBlank(configReq.getOpenType())) {
				return R.failed("奖励按钮为去使用时，跳转类型不能为空");
			}

			PrizeOpenTypeEnum prizeOpenTypeEnum = PrizeOpenTypeEnum
					.getPrizeOpenTypeEnum(Integer.valueOf(configReq.getOpenType()));
			if (prizeOpenTypeEnum == PrizeOpenTypeEnum.SDK) {
				// 需要有菜单
				if (StringUtil.isBlank(configReq.getMenuTitle())) {
					return R.failed("请选择功能定位跳转菜单");
				}
				//TODO
				String[] split = configReq.getMenuTitle().split(",");
				if (split.length<2) {
					return R.failed("请选择功能定位跳转菜单");
				}
				if (configReq.getMenuTitle().startsWith("红包")) {
					if (split.length<=1){
						return R.failed("请选择跳转红包类型");
					}
				}
				if (configReq.getMenuTitle().startsWith("活动")) {
					//1红包活动 2签到活动
					if (StringUtil.isBlank(configReq.getSkipMold())) {
						return R.failed("请选择跳转活动类型");
					}
					if ("1".equals(configReq.getSkipMold()) || "2".equals(configReq.getSkipMold())) {
						//具体红包活动的id
						if (StringUtils.isBlank(configReq.getSkipId())) {
							return R.failed("请选择跳转活动");
						}
					}
				}
			} else if (PrizeOpenTypeEnum.BROWER == prizeOpenTypeEnum || PrizeOpenTypeEnum.URL == prizeOpenTypeEnum) {
				//
				if (StringUtil.isBlank(configReq.getSkipUrl())) {
					return R.failed("请输入跳转链接!");
				}
			}
			if (prizeTypeEnum == PrizeTypeEnum.SWIMBEAN){
				if (StringUtil.isBlank(configReq.getCurrencyType())){
					return R.failed("游豆类型不能为空");
				}
				if (PrizeBeanTypeEnum.TEMP.getType().equals(Integer.valueOf(configReq.getCurrencyType()))){
					// 时间类型校验
					if (StringUtil.isBlank(configReq.getTimeType())) {
						return R.failed("有效规制不能为空!");
					}
					if (StringUtil.isNotBlank(configReq.getTimeType())){
						TimeTypeEnum timeTypeEnum = TimeTypeEnum.getTimeTypeEnum(Integer.valueOf(configReq.getTimeType()));
						if (timeTypeEnum == TimeTypeEnum.FIXTIME) {
							// 指定时间校验，校验开始时间和结束时间
							if (StringUtil.isBlank(configReq.getStartTime())){
								return R.failed("开始时间不能为空");
							}
							if (StringUtil.isBlank(configReq.getEndTime())){
								return R.failed("结束时间不能为空");
							}
							Date startTime = DateUtils.stringToDate(configReq.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
							Date endTime = DateUtils.stringToDate(configReq.getEndTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
							if (startTime !=null && endTime !=null){
								if (startTime.getTime() > endTime.getTime()) {
									return R.failed("开始时间不能大于结束时间");
								}
								if (System.currentTimeMillis() > endTime.getTime()) {
									return R.failed("结束时间不能小于当前时间");
								}
							}
						} else if (timeTypeEnum == TimeTypeEnum.ASSIGNTIME) {
							if (StringUtil.isBlank(configReq.getEffectiveTime())) {
								return R.failed("有效时间不能为空");
							}
							if (!CommonUtils.isPositiveNumber(configReq.getEffectiveTime())){
								return R.failed("有效时间不能负数！");
							}
						}
					}
				}
			}else {
				// 时间类型校验
				if (StringUtil.isBlank(configReq.getTimeType())) {
					return R.failed("有效规制不能为空!");
				}
				if (StringUtil.isNotBlank(configReq.getTimeType())){
					TimeTypeEnum timeTypeEnum = TimeTypeEnum.getTimeTypeEnum(Integer.valueOf(configReq.getTimeType()));
					if (timeTypeEnum == TimeTypeEnum.FIXTIME) {
						// 指定时间校验，校验开始时间和结束时间
						if (StringUtil.isBlank(configReq.getStartTime())){
							return R.failed("开始时间不能为空");
						}
						if (StringUtil.isBlank(configReq.getEndTime())){
							return R.failed("结束时间不能为空");
						}
						Date startTime = DateUtils.stringToDate(configReq.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
						Date endTime = DateUtils.stringToDate(configReq.getEndTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
						if (startTime !=null && endTime !=null){
							if (startTime.getTime() > endTime.getTime()) {
								return R.failed("开始时间不能大于结束时间");
							}
							if (System.currentTimeMillis() > endTime.getTime()) {
								return R.failed("结束时间不能小于当前时间");
							}
						}
					} else if (timeTypeEnum == TimeTypeEnum.ASSIGNTIME) {
						if (StringUtil.isBlank(configReq.getEffectiveTime())) {
							return R.failed("有效时间不能为空");
						}
						if (!CommonUtils.isPositiveNumber(configReq.getEffectiveTime())){
							return R.failed("有效时间不能负数！");
						}
					}
				}
			}

		} else {
			if (PrizeButtonEnum
					.getPrizeButtonEnum(Integer.valueOf(configReq.getPrizeButton())) != PrizeButtonEnum.COPY) {
				return R.failed("礼码包，卡密时，奖励按钮只能选择复制！");
			}

			// 礼包码卡密类型校验
			if (StringUtil.isBlank(configReq.getGiftType())) {
				return R.failed("礼码包，卡密类型不能为空");
			}
			GiftTypeEnum giftTypeEnum = GiftTypeEnum.getGiftTypeEnum(Integer.valueOf(configReq.getGiftType()));
			if (giftTypeEnum == GiftTypeEnum.COMMON) {
				String giftCode = configReq.getGiftCode();
				String giftAmount = configReq.getGiftAmount();
				if (StringUtil.isBlank(giftAmount)) {
					return R.failed("通用码数量不能为空");
				}
				if (!CommonUtils.isPositiveNumber(giftAmount)) {
					return R.failed("通用码数量只能为正整数");
				}
				if (giftAmount.trim().length() > LIMIT_MONEY_MAX_LENGTH) {
					return R.failed("通用码数量应为1~999999");
				}
				if (StringUtils.isBlank(configReq.getGiftCode())) {
					return R.failed("通用码不能为空！");
				}

				if (!GIFT_CODE_PATTERN.matcher(giftCode).find()) {
					return R.failed("通用礼包码应为3~50个英文大小写或数字组成");
				}

			} else if (giftTypeEnum == GiftTypeEnum.UNIQUE) {
				if (StringUtil.isBlank(configReq.getId())){
					if (configReq.getFile() == null || configReq.getFile().isEmpty()) {
						return R.failed("礼码包、卡密文件不能为空");
					}
					// 获取名称
					String filename = configReq.getFile().getOriginalFilename();
					// 获取后缀名
					String suffix = filename.substring(filename.lastIndexOf(".") + 1);
					if (!FILE_SUFFIX.equals(suffix.toUpperCase())) {
						return R.failed("礼码包、卡密文件仅支持txt格式");
					}
					Long size = configReq.getFile().getSize();
					if (size < FILE_MIN_SIZE || size > FILE_MAX_SIZE) {
						return R.failed("礼码包、卡密文件大小应在3字节~1M之间");
					}
				}

			}

		}
		return R.ok();
	}

}
