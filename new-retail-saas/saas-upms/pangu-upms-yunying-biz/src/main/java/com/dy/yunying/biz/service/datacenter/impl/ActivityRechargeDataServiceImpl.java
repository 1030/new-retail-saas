package com.dy.yunying.biz.service.datacenter.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.dy.yunying.api.datacenter.dto.ActivityRechargeDataDto;
import com.dy.yunying.api.datacenter.vo.ActivityRechargeDataVO;
import com.dy.yunying.api.entity.WanGameDO;
import com.dy.yunying.biz.config.YunYingProperties;
import com.dy.yunying.biz.dao.datacenter.impl.ActivityRechargeDataDao;
import com.dy.yunying.biz.service.datacenter.ActivityRechargeDataService;
import com.dy.yunying.biz.service.manage.AdRoleGameService;
import com.dy.yunying.biz.service.manage.AdRoleUserService;
import com.dy.yunying.biz.service.manage.GameService;
import com.dy.yunying.biz.utils.DateUtils;
import com.google.api.client.util.Lists;
import com.pig4cloud.pig.api.feign.RemoteAccountService;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hma
 * @date 2022/9/05 15:37
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ActivityRechargeDataServiceImpl implements ActivityRechargeDataService {

	private final ActivityRechargeDataDao activityRechargeDataDao;


	private final AdRoleUserService adRoleUserService;
	private final RemoteAccountService remoteAccountService;

	private  final GameService gameService;

	private final AdRoleGameService adRoleGameService;

	@Resource
	private YunYingProperties yunYingProperties;

	@Override
	public R<List<ActivityRechargeDataVO>> page(ActivityRechargeDataDto req) {
		List<ActivityRechargeDataVO> list;
		try {
			Long current=req.getCurrent();
			Long size=req.getSize();
			String startTime=req.getStartTime();
			String endTime=req.getEndTime();
			if(StringUtils.isBlank(startTime) ||StringUtils.isBlank(endTime) ){
				return R.failed("开始或者结束时间不允许为空!");
			}
			if(null == current ||null  == size ){
				return R.failed("分页参数不允许为空!");
			}
			this.managerParam(req);
			list = activityRechargeDataDao.list(req);
			deal(list,1);
		} catch (Exception e) {
			log.error("活跃付费明细列表查询异常：{}",e);
			return R.failed("活跃付费明细列表查询失败...");
		}
		return R.ok(list);
	}

	@Override
	public R count(ActivityRechargeDataDto req) {
		try{
			this.managerParam(req);
			Long total=activityRechargeDataDao.countDataTotal(req);
			return R.ok(total);
		}catch (Exception e){
			log.error("活跃付费明细列表合计总条数查询异常：{}",e);
			return R.failed("活跃付费明细合计总条数查询失败...");
		}

	}

	@Override
	public R<List<ActivityRechargeDataVO>> collect(ActivityRechargeDataDto req) {
		try{
			this.managerParam(req);
			List<ActivityRechargeDataVO> list=activityRechargeDataDao.collect(req);
			deal(list,2);
			return R.ok(list);
		}catch (Exception e){
			log.error("活跃付费明细汇总查询异常：{}",e);
			return R.failed("活跃付费明细汇总数据查询失败...");
		}
	}


	/**
	 * 根据用户查询用户对应的 渠道，用户权限
	 */
	private  void managerParam(ActivityRechargeDataDto req){
		// 判断是否管理员
		if (SecurityUtils.getRoles().contains(1)) {
			req.setIsSys(1);
		}
		List<String> accountList =Lists.newArrayList();
		List<Integer> gameList =Lists.newArrayList();
		if(req.getIsSys() != 1){
			//查询角色用户对应的广告账号
			accountList = adRoleUserService.getThrowAccountList();
			gameList = adRoleUserService.getThrowRoleGames();
		}
		// 授权的广告账户
		List<String> roleAdAccountList = new ArrayList<>();
		if(StringUtils.isNotBlank(req.getAdvertiserIdArr())){
			roleAdAccountList.addAll(Arrays.asList(req.getAdvertiserIdArr().split(",")));
			req.setRoleAdAccountList(roleAdAccountList);
		}else{
			// 查看条件没有广告账号的时候，根据授权查询当前用户可查询的广告账号
			if (ObjectUtils.isNotEmpty(accountList)) {
				roleAdAccountList.addAll(accountList);
				req.setRoleAdAccountList(roleAdAccountList);
			}
		}
        if(StringUtils.isBlank(req.getGameIdArr())){
         	//当查询条件为空的时候，对应的将游戏权限设置进去
			if(CollectionUtils.isNotEmpty(gameList)){
				 // 查看当没有授予部门账号的时候，是否是对应的全部账号
				 String gameArr = StringUtils.join(gameList, ",");
				 req.setGameIdArr(gameArr);
			}
        }
	}

	/**
	 * 数据处理
	 * @param data
	 */
	private void deal(List<ActivityRechargeDataVO> data, Integer type) {
		if (!CollectionUtils.isEmpty(data)){
			for (ActivityRechargeDataVO activityRechargeDataVO : data) {
				if(StringUtils.isBlank(activityRechargeDataVO.getParentGameName())){
					activityRechargeDataVO.setParentGameName("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getGameName())){
					activityRechargeDataVO.setGameName("-");
				}
                 if(StringUtils.isBlank(activityRechargeDataVO.getParentChlName())){
					 activityRechargeDataVO.setParentChlName("-");
				 }
				if(StringUtils.isBlank(activityRechargeDataVO.getChlName())){
					activityRechargeDataVO.setChlName("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getAreaId())){
					activityRechargeDataVO.setAreaId("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getAdId())){
					activityRechargeDataVO.setAdId("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getAdName())){
					activityRechargeDataVO.setAdName("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getCcName())){
					activityRechargeDataVO.setAdName("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getIp())){
					activityRechargeDataVO.setIp("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getOrderNo())){
					activityRechargeDataVO.setOrderNo("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getTradeNo())){
					activityRechargeDataVO.setTradeNo("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getProductId())){
					activityRechargeDataVO.setProductId("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getProductName())){
					activityRechargeDataVO.setProductName("-");
				}
				if(null == activityRechargeDataVO.getStatus()){
					activityRechargeDataVO.setStatusName("-");
				}else{

					String rTypeName=null;
					switch (activityRechargeDataVO.getRType()){
						case 1 :rTypeName="支付宝(扫码)";break;
						case 2 :rTypeName="微信(扫码)";break;
						case 14 :rTypeName="支付宝(h5)";break;
						case 15 :rTypeName="微信(h5)";break;
						case 16 :rTypeName="微信(JSAPI)";break;
						case 17 :rTypeName="ios";break;
						case 20 :rTypeName="代金券";break;
						case 21 :rTypeName="游豆";break;
						default: rTypeName="其他";break;
					}
					activityRechargeDataVO.setRgTypeName(rTypeName);
				}

				if(null == activityRechargeDataVO.getRType()){
					activityRechargeDataVO.setRgTypeName("-");
				}else{
					String statusName=null;
					//经过确认 目前充值成功的订单才上报到clickhouse充值表，所以里面都是充值成功的订单，暂时不用充值成功的状态进行判断
					switch (activityRechargeDataVO.getStatus()){
						case 0 :statusName=yunYingProperties.getRechargeStatus0();break;
						case 1 :statusName=yunYingProperties.getRechargeStatus1();break;
						case 2 :statusName=yunYingProperties.getRechargeStatus2();break;
						case 3 :statusName=yunYingProperties.getRechargeStatus3();break;
						case 4 :statusName=yunYingProperties.getRechargeStatus4();break;
						case 5 :statusName=yunYingProperties.getRechargeStatus5();break;
						case 6 :statusName=yunYingProperties.getRechargeStatus6();break;
						case 7 :statusName=yunYingProperties.getRechargeStatus7();break;
						case 8 :statusName=yunYingProperties.getRechargeStatus8();break;
						default: statusName="其他";break;
					}
					activityRechargeDataVO.setStatusName(statusName);
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getRoleId())){
					activityRechargeDataVO.setRoleId("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getRoleName())){
					activityRechargeDataVO.setRoleName("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getUserName())){
					activityRechargeDataVO.setUserName("-");
				}
				if(StringUtils.isBlank(activityRechargeDataVO.getCcName())){
					activityRechargeDataVO.setCcName("-");
				}
				String rechargeTime=activityRechargeDataVO.getPayTime();
				if(StringUtils.isBlank(activityRechargeDataVO.getPayTime())){
					activityRechargeDataVO.setPayTime("-");
				}else{
					String  rechargeTimeStr= DateUtils.transferLongToDate(Long.parseLong(rechargeTime),DateUtils.YYYY_MM_DD_HH_MM_SS);
					activityRechargeDataVO.setPayTime(rechargeTimeStr);
				}
				String regTime=activityRechargeDataVO.getRegTime();
				if(StringUtils.isBlank(regTime)){
					if(1 == type){
						activityRechargeDataVO.setRegTime("-");
					}else if(2 == type){
						//汇总
						activityRechargeDataVO.setRegTime("汇总");
					}
				}else{
					String regTimeStr= DateUtils.transferLongToDate(Long.parseLong(regTime),DateUtils.YYYY_MM_DD_HH_MM_SS);
					activityRechargeDataVO.setRegTime(regTimeStr);
				}
				if (StringUtils.isBlank(activityRechargeDataVO.getUcId())){
					activityRechargeDataVO.setUcId("-");
				}
//				activityRechargeDataVO.setStatus(null);
//				activityRechargeDataVO.setRType(null);
			}
		}

	}





}
