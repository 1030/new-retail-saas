package com.dy.yunying.biz.controller.data;

import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.dto.AdDataDto;
import com.dy.yunying.api.vo.AdDataVo;
import com.dy.yunying.biz.service.data.AdDataService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author ：lile
 * @date ：2021/6/17 13:58
 * @description：
 * @modified By：
 */
@RestController
@RequestMapping("/adData")
@Slf4j
public class AdDataController {

	@Autowired
	private AdDataService adDataService;


	/**
	 * 广告数据报表
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping("/list")
	public R selectAdDataSource(@Valid AdDataDto req) {
		return adDataService.selectAdDataSource(req);
	}

	/**
	 * 看板概览导出
	 *
	 * @param req
	 * @return
	 */
	@ResponseExcel(name = "广告数据报表导出", sheet = "广告数据报表导出")
	@RequestMapping("/excel")
	public R excelAdDataSource(@Valid AdDataDto req, HttpServletResponse response, HttpServletRequest request) {
		String sheetName = "广告数据报表导出";
		try {
			List<AdDataVo> list = adDataService.excelAdDataSource(req);
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(list);
			// 为百分比的值拼接百分号
			final String defaultValue = "0.00%";
			ExportUtils.spliceSuffix(resultListMap, Constant.PERCENT, defaultValue, "regRatio", "roi1", "weekRoi", "weekRoi", "monthRoi", "allRoi", "regPayRatio", "retention2Ratio");
			// 导出
			ExportUtils.exportExcelData(request, response, "广告数据报表-" + DateUtils.getCurrentTimeNoUnderline() + ".xlsx", sheetName, req.getTitles(), req.getColumns(), resultListMap);




		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
