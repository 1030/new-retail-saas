package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.MonitorRuleManager;
import com.dy.yunying.api.req.MonitorRuleManagerReq;

/**
 * @Description: monitor_rule_manager
 * @Author: hma
 * @Date:   2023-03-17
 * @Version: V1.0
 */
public interface MonitorRuleManagerMapper extends BaseMapper<MonitorRuleManager> {


	IPage<MonitorRuleManager> selectMonitorRuleList(MonitorRuleManagerReq monitorRuleManagerReq);

}
