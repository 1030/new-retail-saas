package com.dy.yunying.biz.controller.cps;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dy.yunying.api.cps.CpsBuckleRate;
import com.dy.yunying.api.cps.CpsBuckleRateReq;
import com.dy.yunying.api.cps.vo.CpsBuckleRateVO;
import com.dy.yunying.biz.service.cps.CpsBuckleRateService;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.mybatis.Page;
import com.pig4cloud.pig.common.core.util.CommonUtils;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author sunyq
 * @date 2022/10/11 17:46
 */
@Slf4j
@RestController()
@RequestMapping("/cps/cpsBuckleRate")
@RequiredArgsConstructor
public class CpsBuckleRateController {

	private final CpsBuckleRateService cpsBuckleRateService;


	@ApiOperation(value = "新增扣量比例", notes = "新增扣量比例")
	@PostMapping("/addBuckleRate")
	public R addBuckleRate(@RequestBody CpsBuckleRateReq req) {
		try {
			R result = checkParam(req);
			if (CommonConstants.FAIL.equals(result.getCode())) {
				return result;
			}
			return cpsBuckleRateService.add(req);
		} catch (Exception e) {
			log.error("新增扣量比例-接口异常：{}", e);
			return R.failed("接口异常，请联系管理员");
		}
	}



	@ApiOperation(value = "编辑扣量比例", notes = "编辑扣量比例")
	@PostMapping("/editBuckleRate")
	public R editBuckleRate(@RequestBody CpsBuckleRateReq req) {
		try {
			if (Objects.isNull(req.getId())){
				return R.failed("主键id不能为空");
			}
			R result = checkParam(req);
			if (CommonConstants.FAIL.equals(result.getCode())) {
				return result;
			}
			return cpsBuckleRateService.edit(req);
		} catch (Exception e) {
			log.error("新增扣量比例-接口异常：{}", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "扣量比例列表", notes = "扣量比例列表")
	@PostMapping("/page")
	public R<IPage<CpsBuckleRateVO>> page(@RequestBody(required = false) CpsBuckleRateReq req) {
		try {
			return cpsBuckleRateService.getPage(req);
		} catch (Exception e) {
			log.error("查询扣量比例-接口异常：{}", e);
			return R.failed("接口异常，请联系管理员");
		}
	}


	private R checkParam(CpsBuckleRateReq req) {
		Integer userId = req.getUserId();
		String startTimeStr = req.getStartTime();
		String endTimeStr = req.getEndTime();
		String buckleRate = req.getBuckleRate();
		if (StringUtils.isBlank(startTimeStr) || StringUtils.isBlank(endTimeStr)) {
			return R.failed("开始时间或结束时间不能为空");
		}
		Date startTime = DateUtils.stringToDate(startTimeStr, DateUtils.YYYY_MM_DD_HH_MM_SS);
		Date endTime = DateUtils.stringToDate(endTimeStr, DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (startTime.getTime() > endTime.getTime()) {
			return R.failed("开始时间不能大于结束时间");
		}
		if (StringUtils.isBlank(buckleRate) || !CommonUtils.isNumber(buckleRate) || new BigDecimal(buckleRate).compareTo(BigDecimal.ZERO)<0
				||  new BigDecimal(buckleRate).compareTo(new BigDecimal("99"))>0){
			return R.failed("扣量比例必须为0~99整数");
		}
		if (Objects.isNull(userId)){
			return R.failed("账号信息不能为空");
		}

		LambdaQueryWrapper<CpsBuckleRate> Wrapper = Wrappers.<CpsBuckleRate>lambdaQuery()
				.eq(CpsBuckleRate::getUserId, userId)
				.le(CpsBuckleRate::getStartTime, endTime)
				.ge(CpsBuckleRate::getEndTime, startTime);
		//如果是编辑需要去掉自己本身
		if (!Objects.isNull(req.getId())){
			Wrapper.ne(CpsBuckleRate::getId,req.getId());
		}
		//查看时间是否交替
		List<CpsBuckleRate> list = cpsBuckleRateService.list(Wrapper);
		if (CollectionUtils.isNotEmpty(list)){
			return R.failed("扣量比例时间存在重叠");
		}
		return R.ok();
	}


}
