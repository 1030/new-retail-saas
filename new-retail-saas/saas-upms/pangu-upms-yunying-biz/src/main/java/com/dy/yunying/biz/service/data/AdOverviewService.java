package com.dy.yunying.biz.service.data;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.dto.AdOverviewDto;
import com.dy.yunying.api.dto.AdOverviewDto2;
import com.dy.yunying.api.vo.AdDataAnalysisVO;
import com.dy.yunying.api.vo.AdPlanOverviewVo;
import com.dy.yunying.api.vo.PlanAttrAnalyseSearchVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface AdOverviewService {

	R<AdPlanOverviewVo> selectAdOverviewSource(AdOverviewDto req);

	List<AdPlanOverviewVo> excelPlanStatistic(AdOverviewDto req);

	/**
	 * 处理请求参数
	 *
	 * @param req
	 */
	void managerParam(AdOverviewDto req);


	R<AdDataAnalysisVO> list(AdOverviewDto2 req);
	R selectPlanAttrAnalyseReoport(Page page, PlanAttrAnalyseSearchVo searchVo);
}
