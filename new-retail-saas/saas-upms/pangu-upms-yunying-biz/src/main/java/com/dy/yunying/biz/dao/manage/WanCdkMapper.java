/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.WanCdk;
import com.dy.yunying.api.resp.hongbao.CdkVo;
import com.dy.yunying.api.resp.hongbao.WanUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * CDK信息表
 *
 * @author yuwenfeng
 * @date 2021-11-16 09:57:41
 */
@Mapper
public interface WanCdkMapper extends BaseMapper<WanCdk> {

	Page<CdkVo> selectCdkPage(Page<WanCdk> page,@Param(Constants.WRAPPER) QueryWrapper<WanCdk> query);

	WanUserVo selectUserByName(@Param("userName") String userName);
}
