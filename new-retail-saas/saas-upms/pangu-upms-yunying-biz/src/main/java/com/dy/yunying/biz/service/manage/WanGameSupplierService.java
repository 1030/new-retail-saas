package com.dy.yunying.biz.service.manage;

import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.entity.WanGameSupplierDO;
import com.dy.yunying.api.req.GameSupplierReq;
import com.dy.yunying.api.req.WanGameReq;
import com.dy.yunying.api.vo.WanGameReqVo;

public interface WanGameSupplierService {


	int deleteByPrimaryKey(Integer gid);

	int insert(WanGameSupplierDO record);

	int insertSelective(WanGameSupplierDO record);

	WanGameSupplierDO selectByPrimaryKey(Integer gid);

	int updateByPrimaryKeySelective(WanGameSupplierDO record);

	int updateByPrimaryKeyWithBLOBs(WanGameSupplierDO record);

	int updateByPrimaryKey(WanGameSupplierDO record);

	/***
	 * 根据父游戏Id批量更新子游戏充值回调地址
	 * @param req
	 * @auth kyf
	 * @return
	 */
	int updateExchangeUrl(GameSupplierReq req);

	/***
	 * 添加子游戏充值及秘钥信息
	 * @param entity
	 * @param parentGame
	 * @auth kyf
	 * @return
	 */
	int addGameSupplier(WanGameReqVo entity, ParentGameDO parentGame);
}
