

package com.dy.yunying.biz.service.manage;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.ParentGameArea;

/**
 * 主游戏区服关联表
 * @author  chengang
 * @version  2021-10-27 10:42:03
 * table: parent_game_area
 */
public interface ParentGameAreaService extends IService<ParentGameArea> {

	
}


