package com.dy.yunying.biz.service.manage.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.constant.VersionStatusEnum;
import com.dy.yunying.api.entity.GameVersionDO;
import com.dy.yunying.api.req.GameVersionReq;
import com.dy.yunying.api.vo.GameVersionVO;
import com.dy.yunying.biz.dao.manage.ext.GameVersionDOMapperExt;
import com.dy.yunying.biz.service.manage.GameVersionService;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 游戏版本
 *
 * @Author: hjl
 * @Date: 2020/7/21 13:52
 */
@Transactional
@Service
public class GameVersionServiceImpl implements GameVersionService {

	@Autowired
	private GameVersionDOMapperExt gameVersionDOMapper;

	@Value("${pack.app_file_url_root}")
	private String app_file_url_root;

	@Override
	public IPage<GameVersionVO> page(GameVersionReq req) {
		IPage<GameVersionVO> page = gameVersionDOMapper.page(req);
		if (page.getRecords() != null || page.getRecords().size() > 0) {
			page.getRecords().forEach(data -> {
				if (StringUtils.isNotBlank(data.getPath())) {
					data.setPath(app_file_url_root + data.getPath());
				}
			});
		}
		return page;
	}

	@Override
	public GameVersionDO getByPK(Long versionId) {
		return gameVersionDOMapper.selectByPrimaryKey(versionId);
	}

	@Override
	public GameVersionDO save(GameVersionDO gameVersionDO) {
		Integer userId = SecurityUtils.getUser().getId();
		gameVersionDO.setCreator(String.valueOf(userId));
		gameVersionDO.setCreateTime(DateUtils.getCurrentDate());
		gameVersionDO.setStatus(VersionStatusEnum.VALID.getStatus());
		gameVersionDOMapper.insertSelective(gameVersionDO);


		// 将该子游戏历史基础包置为失效
		gameVersionDOMapper.disableOther(gameVersionDO.getGameId(), gameVersionDO.getVersionId(), gameVersionDO.getPackType());
		return gameVersionDO;
	}

	@Override
	public void deleteByPK(Long versionId) {
		GameVersionDO gameVersionDO = new GameVersionDO();
		gameVersionDO.setVersionId(versionId);
		gameVersionDO.setStatus(VersionStatusEnum.DELETE.getStatus());
		gameVersionDOMapper.updateByPrimaryKeySelective(gameVersionDO);
	}
}