package com.dy.yunying.biz.service.hongbao.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.entity.hongbao.ActivityEventType;
import com.dy.yunying.biz.dao.ads.hongbao.ActivityEventTypeMapper;
import com.dy.yunying.biz.service.hongbao.ActivityEventTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：lile
 * @date ：2021/11/16 16:37
 * @description：
 * @modified By：
 */
@Log4j2
@Service(" ActivityEventTypeService")
@RequiredArgsConstructor
public class ActivityEventTypeServiceImpl extends ServiceImpl<ActivityEventTypeMapper, ActivityEventType> implements ActivityEventTypeService {

	@Autowired
	private ActivityEventTypeMapper activityEventTypeMapper;

	/**
	 * 查询 事件
	 *
	 * @return
	 */
	@Override
	public List<ActivityEventType> selectEventList() {
		QueryWrapper<ActivityEventType> query = new QueryWrapper<ActivityEventType>();
		query.eq("deleted", Constant.DEL_NO);
		return activityEventTypeMapper.selectList(query);
	}
}
