package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.AdOverviewDto2;
import com.dy.yunying.api.datacenter.vo.AdDataAnalysisVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 广告数据分析表相关方法
 */
public interface AdAnalysisService {

	/**
	 * 广告数据分析表总数查询
	 *
	 * @param req
	 * @return
	 */
	R count(AdOverviewDto2 req);

	/**
	 * 广告数据分析表分页列表查询
	 *
	 * @param req
	 * @return
	 */
	R<List<AdDataAnalysisVO>> page(AdOverviewDto2 req);

}
