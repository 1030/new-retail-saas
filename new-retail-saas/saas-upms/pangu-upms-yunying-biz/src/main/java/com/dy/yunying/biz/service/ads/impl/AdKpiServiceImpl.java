package com.dy.yunying.biz.service.ads.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.AdKpi;
import com.dy.yunying.api.entity.AdKpiDO;
import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.req.AdKpiReq;
import com.dy.yunying.api.vo.AdKpiVo;
import com.dy.yunying.biz.dao.ads.AdKpiMapper;
import com.dy.yunying.biz.dao.clickhouse3399.impl.AdKpiDataDao;
import com.dy.yunying.biz.dao.manage.ext.ParentGameDOMapperExt;
import com.dy.yunying.biz.service.ads.AdKpiService;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.api.util.StringUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @ClassName AdKpiServiceImpl
 * @Description todo
 * @Author nieml
 * @Time 2021/7/8 16:48
 * @Version 1.0
 **/
@Slf4j
@Service
public class AdKpiServiceImpl extends ServiceImpl<AdKpiMapper, AdKpi> implements AdKpiService {

	@Autowired
	private AdKpiMapper adKpiMapper;
	@Autowired
	private AdKpiDataDao adKpiDataDao;
	@Autowired
	private ParentGameDOMapperExt parentGameDOMapperExt;

	@Override
	public IPage<AdKpiVo> queryAdKpiDataPage(AdKpiReq req) {
		IPage<AdKpiDO> kpiPage = adKpiMapper.queryAdKpiPage(req);
		List<AdKpiDO> kpiList = kpiPage.getRecords();
		Page<AdKpiVo> kpiDataPage = new Page<AdKpiVo>();
		if (Objects.isNull(kpiList) || kpiList.size()<1){
			return kpiDataPage;
		}
		List<AdKpiVo> records = Lists.newArrayList();
		AdKpi adKpi = new AdKpi();
		kpiList.forEach( kpi -> {
			//调用CH的sql查询指标
			BeanUtils.copyProperties(kpi,adKpi);
			List<AdKpiVo> adKpiVos = adKpiDataDao.queryKpiData(adKpi);
			AdKpiVo adKpiVo = null;
			if (Objects.nonNull(adKpiVos) && adKpiVos.size() > 0){
				adKpiVo = adKpiVos.get(0);
			}else {
				adKpiVo = new AdKpiVo();
				adKpiVo.setRealLevel(0L);
			}
			getAdKpiVoRes(kpi, adKpiVo);
			records.add(adKpiVo);
		});
		kpiDataPage.setRecords(records);
		kpiDataPage.setTotal(kpiPage.getTotal());
		kpiDataPage.setCurrent(kpiPage.getCurrent());
		kpiDataPage.setSize(kpiPage.getSize());
		return kpiDataPage;
	}


	private void getAdKpiVoRes(AdKpiDO kpi,AdKpiVo adKpiVoRes){
		adKpiVoRes.setYearMonth(kpi.getYearMonth());
		adKpiVoRes.setHierarchy(kpi.getHierarchy());
		adKpiVoRes.setHierarchyId(kpi.getHierarchyId());
		adKpiVoRes.setHierarchyName(kpi.getHierarchyName());
		adKpiVoRes.setOs(kpi.getOs());
		Integer pgid = kpi.getPgid();
		adKpiVoRes.setPgid(pgid);
		if (pgid == 0){
			adKpiVoRes.setParentGameName("全部游戏");
		}else {
			adKpiVoRes.setParentGameName(kpi.getParentGameName());
		}
		adKpiVoRes.setTargetLevel(kpi.getTargetLevel());
		adKpiVoRes.setTargetRoi(kpi.getTargetRoi());
		adKpiVoRes.setTargetRoi30(kpi.getTargetRoi30());
		adKpiVoRes.setTargetOlduserMonthWater(kpi.getTargetOlduserMonthWater());
		adKpiVoRes.setTargetNewuserMonthWater(kpi.getTargetNewuserMonthWater());
	}


	@Override
	public IPage<AdKpiDO> queryAdKpiPage(AdKpiReq req) {
		IPage<AdKpiDO> page = adKpiMapper.queryAdKpiPage(req);
		List<ParentGameDO> parentGameDOS = parentGameDOMapperExt.selectParentGameListByCond(new ParentGameDO());
		List<AdKpiDO> records = page.getRecords();
		if (!CollectionUtils.isEmpty(records)){
			//根据父游戏id查询父游戏名称
			records.forEach(adKpiDO -> {
				Integer pgid = adKpiDO.getPgid();
				if (Objects.nonNull(pgid)){
					if (pgid == 0){
						adKpiDO.setParentGameName("全部游戏");
					}else {
						parentGameDOS.forEach(parentGameDO -> {
							if (parentGameDO.getId()==adKpiDO.getPgid().longValue()){
								adKpiDO.setParentGameName(parentGameDO.getGname());
							}
						});
						if (StringUtils.isBlank(adKpiDO.getParentGameName())){
							adKpiDO.setParentGameName("未知");
						}
					}
				}else {
					adKpiDO.setParentGameName("未知");
				}
			});
		}
		return page;
	}




	@Override
	public R saveAdKpi(AdKpi adKpi) {
		//查询指标实际值
		List<AdKpiVo> adKpiVos = adKpiDataDao.queryKpiData(adKpi);
		AdKpiVo adKpiVo = null;
		if (Objects.nonNull(adKpiVos) && adKpiVos.size() > 0){
			adKpiVo = adKpiVos.get(0);
		}else {
			adKpiVo = new AdKpiVo();
			adKpiVo.setRealLevel(0L);
		}
		adKpi.setRealLevel(adKpiVo.getRealLevel());
		adKpi.setRealRoi(adKpiVo.getRealRoi());
		adKpi.setRealRoi30(adKpiVo.getRealRoi30());
		adKpi.setRealNewuserMonthWater(adKpiVo.getRealNewuserMonthWater());
		adKpi.setRealOlduserMonthWater(adKpiVo.getRealOlduserMonthWater());
		// 当前用户
		final PigUser user = SecurityUtils.getUser();
		Date date = new Date();
		adKpi.setCreateTime(date);
		adKpi.setUpdateTime(date);
		adKpi.setCreateUser(user.getId().toString());
		adKpi.setIsDelete(0);
		boolean save = false;
		try {
			save = this.save(adKpi);
		} catch (Exception e) {
			log.error("saveAdKpi:{}",e);
		}
		if (save){
			return R.ok("保存成功");
		}
		return R.failed("保存失败");
	}


	@Override
	public R editAdKpi(AdKpi adKpi) {
		// 当前用户
		final PigUser user = SecurityUtils.getUser();
		Date date = new Date();
		adKpi.setUpdateTime(date);
		adKpi.setUpdateUser(user.getId().toString());
		boolean edit = false;
		try {
			edit = this.updateById(adKpi);
		} catch (Exception e) {
			log.error("editAdKpi:{}",e);
		}
		if (edit){
			return R.ok("编辑成功");
		}
		return R.failed("编辑失败");
	}


	@Override
	public R delAdKpi(Integer id) {
		AdKpi adKpi = new AdKpi();
		adKpi.setId(id);
		adKpi.setIsDelete(1);
		boolean edit = false;
		try {
			edit = this.updateById(adKpi);
		} catch (Exception e) {
			log.error("delAdKpi:{}",e);
		}
		if (edit){
			return R.ok("删除成功");
		}
		return R.failed("删除失败");
	}


}
