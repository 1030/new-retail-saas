package com.dy.yunying.biz.dao.ads.hongbao;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbInvitationTop;
import org.apache.ibatis.annotations.Mapper;

/**
 * 手动榜单表
 * @author  chengang
 * @version  2021-10-27 19:21:20
 * table: hb_invitation_top
 */
@Mapper
public interface HbInvitationTopMapper extends BaseMapper<HbInvitationTop> {

	int insertBatch(List<HbInvitationTop> list);
}


