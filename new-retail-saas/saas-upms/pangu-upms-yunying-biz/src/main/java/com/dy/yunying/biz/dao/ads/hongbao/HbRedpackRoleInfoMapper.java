package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbRedpackRoleInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 红包角色关联信息表
 * @author  chenxiang
 * @version  2021-11-18 15:03:20
 * table: hb_redpack_role_info
 */
@Mapper
public interface HbRedpackRoleInfoMapper extends BaseMapper<HbRedpackRoleInfo> {
	
}


