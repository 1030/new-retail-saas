package com.dy.yunying.biz.dao.manage.ext;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.SubChannelInfoDO;
import com.dy.yunying.api.entity.WanChannelPackDO;
import com.dy.yunying.api.entity.WanGameChannelInfoDO;
import com.dy.yunying.api.req.WanAppChlReq;
import com.dy.yunying.api.req.WanChannelCpsPackReq;
import com.dy.yunying.api.req.WanChannelPackReq;
import com.dy.yunying.api.vo.WanChannelPackPageVO;
import com.dy.yunying.api.vo.WanChannelPackVO;
import com.dy.yunying.biz.dao.manage.WanChannelPackDOMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface WanChannelPackDOMapperExt extends WanChannelPackDOMapper {

	IPage<WanChannelPackPageVO> page(WanChannelPackReq req);

	void updateByUKSelective(WanChannelPackDO wanChannelPackDO);

	WanChannelPackVO getVOByPK(Long packId);

	int countByCode(@Param("code") String code);

	List<String> getChlCodeByGameId(@Param("gameId") Long gameId);

	List<WanChannelPackVO> latestList(WanChannelPackReq req);

	List<WanGameChannelInfoDO> queryPkByCodeName(@Param("codeName") String codeName, @Param("chl") String chl);

	List<Map<String, String>> queryAppChlName(@Param("appChlArr") List<String> appChlArr);

	List<Map<String, String>> queryAppchlList(WanAppChlReq req);

	List<Map<String, String>> queryCpsAppchlList(WanAppChlReq req);

	/**
	 * cps 渠道分包列表
	 * @param req
	 * @return
	 */
	IPage<WanChannelPackPageVO> cpsPage(WanChannelCpsPackReq req);

    List<SubChannelInfoDO> queryChlInfoListByAuth(WanAppChlReq req);

	/**
	 * cps子渠道列表，带权限
	 * @param req
	 * @return
	 */
	List<Map<String, String>> queryCpsSubchlList(WanAppChlReq req);

	int updateByCode(@Param("list") List<String> list, @Param("backhaulType") Integer backhaulType);
}