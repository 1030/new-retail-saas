package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.req.ParentGameReq;
import com.dy.yunying.api.vo.ParentGameVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface ParentGameService extends IService<ParentGameDO> {

	/**
	 * 获取可缓存的主游戏列表
	 *
	 * @param req
	 * @return
	 */
	List<ParentGameDO> getCacheablePGameList(ParentGameReq req) throws Exception;

	R saveParentGame(ParentGameDO parentGame);

	R updateParentGame(ParentGameDO parentGame);

	ParentGameDO getByPK(Long id);

	IPage<ParentGameVO> getPage(ParentGameReq req);

	List<ParentGameVO> versionVOList(ParentGameDO req);
}
