package com.dy.yunying.biz.service.raffle;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.raffle.RaffleExchangeStore;
import com.dy.yunying.api.req.raffle.RaffleExchangeStoreReq;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 积分商店配置表服务接口
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
 */
public interface RaffleExchangeStoreService extends IService<RaffleExchangeStore> {

	R createEdit(RaffleExchangeStoreReq req);

	R editOnPrize(RaffleExchangeStoreReq req);

	void saveBatchList(List<RaffleExchangeStore> list);

}
