

package com.dy.yunying.biz.service.sign;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.sign.SignReceiveRecord;

/**
 * 奖品领取记录表
 * @author  chengang
 * @version  2021-12-01 10:14:36
 * table: sign_receive_record
 */
public interface SignReceiveRecordService extends IService<SignReceiveRecord> {
	

	
}


