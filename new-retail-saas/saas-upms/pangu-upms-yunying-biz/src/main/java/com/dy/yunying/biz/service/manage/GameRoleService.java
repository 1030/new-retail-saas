

package com.dy.yunying.biz.service.manage;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.GameRole;

/**
 * 角色信息表
 * @author  chengang
 * @version  2021-10-28 13:35:09
 * table: game_role
 */
public interface GameRoleService extends IService<GameRole> {
	

}


