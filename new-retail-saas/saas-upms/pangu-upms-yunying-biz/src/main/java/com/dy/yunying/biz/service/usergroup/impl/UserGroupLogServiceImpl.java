package com.dy.yunying.biz.service.usergroup.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.usergroup.UserGroupLog;
import com.dy.yunying.biz.dao.ads.usergroup.UserGroupLogMapper;
import com.dy.yunying.biz.service.usergroup.UserGroupLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service("userGroupLogService")
@RequiredArgsConstructor
public class UserGroupLogServiceImpl extends ServiceImpl<UserGroupLogMapper, UserGroupLog> implements UserGroupLogService {

	private final UserGroupLogMapper userGroupLogMapper;

}
