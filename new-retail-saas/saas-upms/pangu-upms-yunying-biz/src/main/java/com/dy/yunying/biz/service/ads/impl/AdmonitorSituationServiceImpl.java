package com.dy.yunying.biz.service.ads.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.AdmonitorSituation;
import com.dy.yunying.api.enums.Comparator;
import com.dy.yunying.api.enums.MonitorAdItem;
import com.dy.yunying.api.enums.MonitorTargetEnum;
import com.dy.yunying.biz.dao.ads.AdmonitorSituationMapper;
import com.dy.yunying.biz.exception.YunyingException;
import com.dy.yunying.biz.service.ads.AdmonitorSituationService;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import com.pig4cloud.pig.api.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName AdmonitorRuleServiceImpl
 * @Description todo
 * @Author yangyh
 * @Time 2021/6/18 15:05
 * @Version 1.0
 **/
@Service
@Slf4j
public class AdmonitorSituationServiceImpl extends ServiceImpl<AdmonitorSituationMapper, AdmonitorSituation> implements AdmonitorSituationService {

	@Override
	public List<AdmonitorSituation> getByAdMonitorRuleId(Integer adMonitorRuleId) {
		return list(new QueryWrapper<AdmonitorSituation>().eq("ad_monitor_rule_id", adMonitorRuleId));
	}

	@Override
	public boolean deleteByAdMonitorRuleId(Integer adMonitorRuleId) {
		AdmonitorSituation admonitorSituation = new AdmonitorSituation();
		admonitorSituation.setUpdateBy(SecurityUtils.getUser().getId());
		admonitorSituation.setUpdateTime(new Date());
		admonitorSituation.setDelFlag(1);
		baseMapper.update(admonitorSituation, new QueryWrapper<AdmonitorSituation>().eq("ad_monitor_rule_id", adMonitorRuleId));
		return true;
	}




	@Override
	public boolean saveAdmonitorSituation(AdmonitorSituation admonitorSituation) {
		admonitorSituation.setCreateBy(SecurityUtils.getUser().getId());
		admonitorSituation.setCreateTime(new Date());
		admonitorSituation.setUpdateBy(SecurityUtils.getUser().getId());
		admonitorSituation.setUpdateTime(new Date());
		admonitorSituation.setDelFlag(0);
		return save(admonitorSituation);
	}

	@Override
	public boolean updateAdmonitorSituation(AdmonitorSituation admonitorSituation) {
		admonitorSituation.setUpdateBy(SecurityUtils.getUser().getId());
		admonitorSituation.setUpdateTime(new Date());
		admonitorSituation.setDelFlag(0);
		return updateById(admonitorSituation);
	}

	@Override
	public void validateAdmonitorSituation(AdmonitorSituation admonitorSituation) {
		if (admonitorSituation == null) throw new YunyingException("监控情形为空");
		if (StringUtils.isBlank(admonitorSituation.getMonitorAditem())) throw new YunyingException("监控项为空");
		MonitorAdItem monitorAdItem =null;
		try {
			 monitorAdItem = MonitorAdItem.valueOf(admonitorSituation.getMonitorAditem());
		} catch (Exception e) {
			throw new YunyingException("监控项不正确【"+admonitorSituation.getMonitorAditem()+"】");
		}
		if (StringUtils.isBlank(admonitorSituation.getMonitorTarget())) throw new YunyingException("监控指标为空");
		try {
			MonitorTargetEnum monitorTargetEnum = MonitorTargetEnum.valueOf(admonitorSituation.getMonitorTarget());
		} catch (Exception e) {
			throw new YunyingException("监控指标不正确【"+admonitorSituation.getMonitorTarget()+"】");
		}
		if (StringUtils.isBlank(admonitorSituation.getCompare())) throw new YunyingException("比较运算符为空");
		try {
			 Comparator.valueOf(admonitorSituation.getCompare());
		} catch (Exception e) {
			throw new YunyingException("比较运算符不正确【"+admonitorSituation.getCompare()+"】");
		}
		String compareValue = admonitorSituation.getCompareValue();
		if (StringUtils.isBlank(compareValue)) throw new YunyingException("比较值为空");

		if(!isNumeric(compareValue)) {
			throw new YunyingException("比较值只能为数字");
		}
		String monitorTarget = admonitorSituation.getMonitorTarget();
		//校验新增付费率值：0-100
		if(MonitorTargetEnum.NEW_PAY_RATE.name().equals(monitorTarget)) {
			BigDecimal compareValueBD = new BigDecimal(compareValue);
			int i0 = compareValueBD.compareTo(new BigDecimal("0"));
			int i100 = compareValueBD.compareTo(new BigDecimal("100"));
			if (i0 == -1){
				throw new YunyingException("【新增付费率】比较值应大于等于0，小于等于100");
			}
			if(i100 == 1){
				throw new YunyingException("【新增付费率】比较值应大于等于0，小于等于100");
			}
		}
		//校验校验新增付费率值和ROI值，保留两位有效数字

	}


	public static boolean isNumeric(String str) {
		// 该正则表达式可以匹配所有的数字 包括负数
		Pattern pattern = Pattern.compile("-?[0-9]+(\\.[0-9]+)?");
		String bigStr;
		try {
			bigStr = new BigDecimal(str).toString();
		} catch (Exception e) {
			return false;//异常 说明包含非数字。
		}

		Matcher isNum = pattern.matcher(bigStr); // matcher是全匹配
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}

/*	public static void main(String[] args) {
		double aa = -19162431.1254;
		String a = "-19162431.1254";
		String b = "-19162431a1254";
		String c = "中文";
		System.out.println(isNumeric(Double.toString(aa)));
		System.out.println(isNumeric(a));
		System.out.println(isNumeric(b));
		System.out.println(isNumeric(c));
	}*/


}
