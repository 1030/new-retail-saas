package com.dy.yunying.biz.dao.manage.cps;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.cps.CpsUserPack;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.dy.yunying.biz.domain.CpsUserAuthPack
 */
public interface CpsUserAuthPackMapper extends BaseMapper<CpsUserPack> {
	/**
	 * 查询主渠道子渠道分包渠道信息
	 * @param channelList
	 * @return
	 */
	List<CpsUserPack> selectChannalInfoByAppChl(@Param("channelList") List<String> channelList);
}




