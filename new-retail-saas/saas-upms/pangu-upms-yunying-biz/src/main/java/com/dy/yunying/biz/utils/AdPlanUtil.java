package com.dy.yunying.biz.utils;

import cn.hutool.core.lang.Pair;
import com.alibaba.fastjson.JSON;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AdPlanUtil {

	private static final Map<String, String> OE_STATUS_MAPPING = new HashMap<String, String>() {{
		put("AD_STATUS_ENABLE", "启用");
		put("AD_STATUS_DISABLE", "计划暂停");
		put("AD_STATUS_CAMPAIGN_DISABLE", "已被广告组暂停");
		put("AD_STATUS_DELIVERY_OK", "投放中");
		put("AD_STATUS_REAUDIT", "修改审核中");
		put("AD_STATUS_AUDIT", "新建审核中");
		put("AD_STATUS_DONE", "已完成（投放达到结束时间）");
		put("AD_STATUS_CREATE", "计划新建");
		put("AD_STATUS_AUDIT_DENY", "审核不通过");
		put("AD_STATUS_BALANCE_EXCEED", "账户余额不足）");
		put("AD_STATUS_BUDGET_EXCEED", "超出预算");
		put("AD_STATUS_NOT_START", "未到达投放时间");
		put("AD_STATUS_NO_SCHEDULE", "不在投放时段");
		put("AD_STATUS_DELETE", "已删除");
		put("AD_STATUS_CAMPAIGN_EXCEED", "广告组超出预算");
		put("AD_STATUS_ALL", "所有包含已删除");
		put("AD_STATUS_NOT_DELETE", "所有不包含已删除");
		put("AD_STATUS_ADVERTISER_BUDGET_EXCEED", "超出广告主日预算");
		put("NOT_DELETED", "不限");
		put("ALL", "不限（包含已删除）");
		put("OK", "投放中(体验版)");
		put("DELETED", "已删除(体验版)");
		put("PROJECT_OFFLINE_BUDGET", "项目超出预算");
		put("PROJECT_PREOFFLINE_BUDGET", "项目接近预算");
		put("TIME_NO_REACH", "未到达投放时间(体验版)");
		put("TIME_DONE", "已完成(体验版)");
		put("NO_SCHEDULE", "不在投放时段(体验版)");
		put("AUDIT", "新建审核中(体验版)");
		put("REAUDIT", "修改审核中(体验版)");
		put("FROZEN", "已终止");
		put("AUDIT_DENY", "审核不通过(体验版)");
		put("OFFLINE_BUDGET", "广告超出预算");
		put("OFFLINE_BALANCE", "账户余额不足(体验版)");
		put("PREOFFLINE_BUDGET", "广告接近预算");
		put("DISABLED", "已暂停");
		put("PROJECT_DISABLED", "已被项目暂停");
		put("LIVE_ROOM_OFF", "关联直播间不可投");
		put("PRODUCT_OFFLINE", "关联商品不可投");
		put("AWEME_ACCOUNT_DISABLED", "关联抖音账号不可投");
		put("AWEME_ANCHOR_DISABLED", "锚点不可投");
		put("DISABLE_BY_QUOTA", "已暂停（配额达限）");
	}};
	private static final Map<String, String> TX_STATUS_MAPPING = new HashMap<String, String>() {{
		put("STATUS_UNKNOWN", "未知状态");
		put("STATUS_PENDING", "审核中");
		put("STATUS_DENIED", "审核不通过");
		put("STATUS_FROZEN", "冻结");
		put("STATUS_SUSPEND", "暂停中");
		put("STATUS_READY", "未到投放时间");
		put("STATUS_ACTIVE", "投放中");
		put("STATUS_STOP", "投放结束");
		put("STATUS_PREPARE", "准备中");
		put("STATUS_DELETED", "已删除");
		put("STATUS_ACTIVE_ACCOUNT_FROZEN", "广告被暂停（账户资金被冻结）");
		put("STATUS_ACTIVE_ACCOUNT_EMPTY", "广告被暂停（账户余额不足）");
		put("STATUS_ACTIVE_ACCOUNT_LIMIT", "广告被暂停（账户达日限额）");
		put("STATUS_ACTIVE_CAMPAIGN_LIMIT", "广告被暂停（推广计划达日限额））");
		put("STATUS_ACTIVE_CAMPAIGN_SUSPEND", "广告被暂停（推广计划暂停）");
		put("STATUS_PART_READY", "部分待投放");
		put("STATUS_PART_ACTIVE", "部分投放中");
	}};
	private static final Map<String, String> KS_STATUS_MAPPING = new HashMap<String, String>() {{
		put("-1", "不限");
		put("1", "计划已暂停");
		put("3", "计划超预算");
		put("6", "余额不足");
		put("11", "审核中");
		put("12", "审核未通过");
		put("14", "已结束");
		put("15", "已暂停");
		put("17", "组超预算");
		put("19", "未达投放时间");
		put("20", "有效");
	}};

	private static final Map<String, String> BD_STATUS_MAPPING = new HashMap<String, String>() {{
		put("0", "有效");
		put("1", "暂停推广");
		put("2", "推广计划暂停推广");
		put("3", "直播结束后暂停");
	}};

	private static final Map<String, String> OE_UNION_VIDEO_TYPE_MAPPING = Stream.of(Pair.of("REWARDED_VIDEO", "激励视频"), Pair.of("ORIGINAL_VIDEO", "原生视频"), Pair.of("SPLASH_VIDEO", "开屏视频")).collect(Collectors.toMap(Pair::getKey, Pair::getValue, (k1, k2) -> k2));
	private static final Map<String, String> KS_UNION_VIDEO_TYPE_MAPPING = Stream.of(Pair.of("0", "未知"), Pair.of("1", "正常投放"), Pair.of("2", "平滑投放"), Pair.of("3", "优先低成本")).collect(Collectors.toMap(Pair::getKey, Pair::getValue, (k1, k2) -> k2));

	private static final Map<String, String> OE_DELIVERY_RANGE_MAPPING = Stream.of(Pair.of("UNIVERSAL", "通投智选"), Pair.of("DEFAULT", "默认"), Pair.of("UNION", "穿山甲"), Pair.of("MANUAL", "首选媒体"), Pair.of("UNIVERSAL_SMART", "通投智选")).collect(Collectors.toMap(Pair::getKey, Pair::getValue, (k1, k2) -> k2));

	private static final Map<String, String> BD_DELIVERY_RANGE_MAPPING = Stream.of(Pair.of("1", "自定义类-百度信息流"), Pair.of("2", "自定义类-贴吧"), Pair.of("4", "百青藤"), Pair.of("8", "自定义类-好看视频"), Pair.of("64", "自定义类-百度小说")).collect(Collectors.toMap(Pair::getKey, Pair::getValue, (k1, k2) -> k2));

	private static final Map<String, String> OE_INVENTORY_TYPE_MAPPING = new HashMap<String, String>() {{
		put("INVENTORY_AWEME_FEED", "抖音信息流");
		put("INVENTORY_VIDEO_FEED", "西瓜信息流");
		put("INVENTORY_FEED", "头条信息流");
		put("INVENTORY_TEXT_LINK", "头条文章详情页");
		put("INVENTORY_HOTSOON_FEED", "火山信息流");
		put("INVENTORY_UNION_SLOT", "穿山甲");
		put("UNION_BOUTIQUE_GAME", "ohayoo精品游戏");
		put("INVENTORY_UNION_SPLASH_SLOT", "穿山甲开屏广告");
		put("INVENTORY_AWEME_SEARCH", "搜索广告——抖音位");
		put("INVENTORY_SEARCH", "搜索广告——头条位");
		put("INVENTORY_UNIVERSAL", "通投智选");
		put("INVENTORY_BEAUTY", "轻颜相机");
		put("INVENTORY_PIPIXIA", "皮皮虾");
		put("INVENTORY_AUTOMOBILE", "懂车帝");
		put("INVENTORY_STUDY", "好好学习");
		put("INVENTORY_FACE_U", "faceu");
		put("INVENTORY_TOMATO_NOVEL", "番茄小说");
		put("OTHER", "其他");
	}};
	private static final Map<String, String> TX_INVENTORY_TYPE_MAPPING = new HashMap<String, String>() {{
		put("SITE_SET_QZONE", "QQ空间，PC版位");
		put("SITE_SET_MOBILE_MYAPP", "应用宝");
		put("SITE_SET_MOBILE_INNER", "QQ、腾讯看点、腾讯音乐");
		put("SITE_SET_MOBILE_UNION", "优量汇");
		put("SITE_SET_WECHAT", "微信公众号与小程序");
		put("SITE_SET_TENCENT_NEWS", "腾讯新闻");
		put("SITE_SET_TENCENT_VIDEO", "腾讯视频");
		put("SITE_SET_MOBILE_YYB", "应用宝");
		put("SITE_SET_PCQQ", "PC-QQ、QQ空间、腾讯音乐");
		put("SITE_SET_KANDIAN", "腾讯看点");
		put("SITE_SET_QQ_MUSIC_GAME", "QQ、腾讯音乐及游戏");
		put("SITE_SET_MOMENTS", "微信朋友圈");
		put("SITE_SET_MINI_GAME_WECHAT", "微信小游戏");
		put("SITE_SET_MINI_GAME_QQ", "QQ小游戏");
		put("SITE_SET_MOBILE_GAME", "App游戏");
		put("SITE_SET_QQSHOPPING", "QQ购物");
	}};

	private static final Map<String, String> OE_CONVERT_TARGET_MAPPING = new HashMap<String, String>() {{
		put("AD_CONVERT_TYPE_PAY", "付费");
		put("AD_CONVERT_TYPE_ACTIVE", "激活");
		put("AD_CONVERT_TYPE_ACTIVE_REGISTER", "注册");
		put("AD_CONVERT_TYPE_GAME_ADDICTION", "关键行为");
	}};
	private static final Map<String, String> TX_CONVERT_TARGET_MAPPING = new HashMap<String, String>() {{
		put("OPTIMIZATIONGOAL_CLICK", "点击");
		put("OPTIMIZATIONGOAL_FIRST_PURCHASE", "首次付费");
		put("OPTIMIZATIONGOAL_ECOMMERCE_ORDER", "下单");
		put("OPTIMIZATIONGOAL_APP_ACTIVATE", "App激活");
		put("OPTIMIZATIONGOAL_ONE_DAY_RETENTION", "次日留存");
		put("OPTIMIZATIONGOAL_VIEW_COMMODITY_PAGE", "商品详情页浏览");
		put("OPTIMIZATIONGOAL_APP_REGISTER", "App注册");
		put("OPTIMIZATIONGOAL_PROMOTION_VIEW_KEY_PAGE", "关键页面访问");
		put("OPTIMIZATIONGOAL_ECOMMERCE_CART", "加入购物车");
		put("OPTIMIZATIONGOAL_APP_DOWNLOAD", "App下载");
		put("OPTIMIZATIONGOAL_MOBILE_APP_AD_INCOME", "广告变现");
		put("OPTIMIZATIONGOAL_WITHDRAW_DEPOSITS", "提现");
		put("OPTIMIZATIONGOAL_PRE_CREDIT", "预授信");
		put("OPTIMIZATIONGOAL_APPLY", "完件");
		put("OPTIMIZATIONGOAL_APP_PURCHASE", "付费次数");
	}};
	private static final Map<String, String> KS_CONVERT_TARGET_MAPPING = new HashMap<String, String>() {{
		put("0", "未知");
		put("2", "点击转化链接");
		put("10", "曝光");
		put("11", "点击");
		put("31", "下载完成");
		put("53", "提交线索");
		put("109", "电话卡激活");
		put("137", "量房");
		put("180", "激活");
		put("190", "付费");
		put("191", "首日ROI");
		put("348", "有效线索");
		put("383", "授信");
		put("384", "完件");
		put("715", "微信复制");
		put("739", "7日付费次数");
	}};

	private static final Map<String, String> BD_CONVERT_TARGET_MAPPING = new HashMap<String, String>() {{
		put("1", "咨询按钮点击");
		put("2", "电话按钮点击");
		put("3", "表单提交成功");
		put("4", "激活");
		put("5", "表单按钮点击");
		put("6", "下载（预约）按钮点击（小流量）");
		put("10", "购买成功");
		put("14", "订单提交成功");
		put("17", "三句话咨询");
		put("18", "留线索");
		put("19", "一句话咨询");
		put("20", "关键页面浏览");
		put("25", "注册（小流量）");
		put("26", "付费（小流量）");
		put("27", "客户自定义（小流量）");
		put("28", "次日留存（小流量）");
		put("30", "电话拨通");
		put("35", "微信复制按钮点击（小流量）");
		put("41", "申请（小流量）");
		put("42", "授信（小流量）");
		put("45", "商品下单成功");
		put("46", "加入购物车");
		put("47", "商品收藏");
		put("48", "商品详情页到达");
		put("53", "订单核对成功");
		put("54", "收货成功");
		put("56", "到店（小流量）");
		put("57", "店铺调起");
		put("67", "微信调起按钮点击");
		put("68", "粉丝关注成功");
		put("71", "应用调起");
		put("72", "聊到相关业务（小流量）");
		put("73", "回访-电话接通（小流量）");
		put("74", "回访-信息确认（小流量）");
		put("75", "回访-发现意向（小流量）");
		put("76", "回访-高潜成交（小流量）");
		put("77", "回访-成单客户（小流量）");
		put("93", "付费阅读(小流量)");
	}};

	private static final Map<String, String> OE_DEEP_CONVERT_MAPPING = new HashMap<String, String>() {{
		put("AD_CONVERT_TYPE_NEXT_DAY_OPEN", "次留");
		put("AD_CONVERT_TYPE_GAME_ADDICTION", "关键行为");
		put("AD_CONVERT_TYPE_PURCHASE_ROI", "付费ROI");
		put("AD_CONVERT_TYPE_LT_ROI", "广告变现ROI");
		put("AD_CONVERT_TYPE_PAY", "付费");
		put("AD_CONVERT_TYPE_UG_ROI", "内广ROI");
	}};
	private static final Map<String, String> TX_DEEP_CONVERT_MAPPING = new HashMap<String, String>() {{
		put("OPTIMIZATIONGOAL_APP_REGISTER", "注册");
		put("OPTIMIZATIONGOAL_ONE_DAY_RETENTION", "次日留存");
		put("OPTIMIZATIONGOAL_APP_PURCHASE", "付费次数");
		put("OPTIMIZATIONGOAL_FIRST_PURCHASE", "首次付费");
		put("GOAL_1DAY_PURCHASE_ROAS", "首日付费ROI");
		put("GOAL_3DAY_PURCHASE_ROAS", "3天付费ROI");
		put("GOAL_7DAY_PURCHASE_ROAS", "7天付费ROI");
		put("GOAL_15DAY_PURCHASE_ROAS", "15天付费ROI");
		put("GOAL_30DAY_PURCHASE_ROAS", "30天付费ROI");
		put("GOAL_60DAY_PURCHASE_ROAS", "60天付费ROI");
	}};
	private static final Map<String, String> KS_DEEP_CONVERT_MAPPING = new HashMap<String, String>() {{
		put("3", "付费");
		put("7", "次日留存");
		put("10", "完件");
		put("11", "授信");
		put("13", "添加购物车");
		put("14", "提交订单");
		put("15", "购买");
		put("44", "有效线索");
		put("92", "付费roi");
		put("181", "激活后24H次日留存");
		put("0", "无");
	}};

	private static final Map<String, String> BD_DEEP_CONVERT_MAPPING = new HashMap<String, String>() {{
		put("10", "购买成功");
		put("25", "注册（小流量）");
		put("26", "付费（小流量）");
		put("27", "客户自定义（小流量）");
		put("28", "次日留存（小流量）");
		put("42", "授信（小流量）");
		put("45", "商品下单成功");
		put("53", "订单核对成功");
		put("54", "收货成功");
		put("56", "到店（小流量）");
		put("72", "聊到相关业务（小流量）");
		put("73", "回访-电话接通（小流量）");
		put("74", "回访-信息确认（小流量）");
		put("75", "回访-发现意向（小流量）");
		put("76", "回访-高潜成交（小流量）");
		put("77", "回访-成单客户（小流量）");
	}};

	private static final Map<String, String> OE_DEEP_BID_TYPE_MAPPING = new HashMap<String, String>() {{
		put("DEEP_BID_DEFAULT", "不启用，无深度优化");
		put("DEEP_BID_PACING", "自动优化（手动出价方式下）");
		put("DEEP_BID_MIN", "自定义双出价（手动出价方式下）");
		put("SMARTBID", "自动优化（自动出价方式下）");
		put("AUTO_MIN_SECOND_STAGE", "自定义双出价（自动出价方式下）");
		put("ROI_COEFFICIENT", "ROI系数");
		put("ROI_PACING", "ROI系数——自动优化");
		put("MIN_SECOND_STAGE", "两阶段优化");
		put("PACING_SECOND_STAGE", "动态两阶段");
		put("BID_PER_ACTION", "每次付费出价");
		put("SOCIAL_ROI", "ROI三出价");
		put("DEEP_BID_TYPE_RETENTION_DAYS", "留存天数");
	}};
	private static final Map<String, String> TX_DEEP_BID_TYPE_MAPPING = Stream.of(Pair.of("DEEP_OPTIMIZATION_ACTION_TYPE_DOUBLE_GOAL_BID", "双目标出价"), Pair.of("DEEP_OPTIMIZATION_ACTION_TYPE_TWO_STAGE_BID", "两阶段出价")).collect(Collectors.toMap(Pair::getKey, Pair::getValue, (k1, k2) -> k2));

	public static String getStatusEnumValue(String enumKey, int ctype, String defaultValue) {
		if (PlatformTypeEnum.TT.getValue().equals(String.valueOf(ctype))) {
			return OE_STATUS_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.GDT.getValue().equals(String.valueOf(ctype))) {
			return TX_STATUS_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.KS.getValue().equals(String.valueOf(ctype))){
			return KS_STATUS_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.BD.getValue().equals(String.valueOf(ctype))){
			return BD_STATUS_MAPPING.getOrDefault(enumKey, defaultValue);
		}else {
			return defaultValue;
		}
	}

	public static String getUnionVideoEnumValue(String enumKey, int ctype, String defaultValue) {
		if (PlatformTypeEnum.TT.getValue().equals(String.valueOf(ctype))) {
			return OE_UNION_VIDEO_TYPE_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.KS.getValue().equals(String.valueOf(ctype))){
			return KS_UNION_VIDEO_TYPE_MAPPING.getOrDefault(enumKey, defaultValue);
		} else {
			return defaultValue;
		}
	}

	public static String getDeliveryRangeEnumValue(String enumKey, int ctype, String defaultValue) {
		if (PlatformTypeEnum.TT.getValue().equals(String.valueOf(ctype))) {
			return OE_DELIVERY_RANGE_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.BD.getValue().equals(String.valueOf(ctype))) {
			final String deliveryRange = StringUtils.defaultIfBlank(enumKey, "[]");
			return JSON.parseArray(deliveryRange, String.class).stream().map(e -> BD_DELIVERY_RANGE_MAPPING.getOrDefault(e, defaultValue)).collect(Collectors.toList()).toString();
		} else {
			return defaultValue;
		}
	}

	public static String getInventoryTypeEnumValue(String enumKey, int ctype, String defaultValue) {
		if (1 == ctype) {
			return OE_INVENTORY_TYPE_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (8 == ctype) {
			return TX_INVENTORY_TYPE_MAPPING.getOrDefault(enumKey, defaultValue);
		} else {
			return defaultValue;
		}
	}

	public static String getConvertTargetEnumValue(String enumKey, int ctype, String defaultValue) {
		if (PlatformTypeEnum.TT.getValue().equals(String.valueOf(ctype))) {
			return OE_CONVERT_TARGET_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.GDT.getValue().equals(String.valueOf(ctype))) {
			return TX_CONVERT_TARGET_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.KS.getValue().equals(String.valueOf(ctype))){
			return KS_CONVERT_TARGET_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.BD.getValue().equals(String.valueOf(ctype))){
			return BD_CONVERT_TARGET_MAPPING.getOrDefault(enumKey, defaultValue);
		} else {
			return defaultValue;
		}
	}

	public static String getDeepConvertEnumValue(String enumKey, int ctype, String defaultValue) {
		if (PlatformTypeEnum.TT.getValue().equals(String.valueOf(ctype))) {
			return OE_DEEP_CONVERT_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.GDT.getValue().equals(String.valueOf(ctype))) {
			return TX_DEEP_CONVERT_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.KS.getValue().equals(String.valueOf(ctype))){
			return KS_DEEP_CONVERT_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (PlatformTypeEnum.BD.getValue().equals(String.valueOf(ctype))){
			return BD_DEEP_CONVERT_MAPPING.getOrDefault(enumKey, defaultValue);
		} else {
			return defaultValue;
		}
	}

	public static String getDeepBidTypeEnumValue(String enumKey, int ctype, String defaultValue) {
		if (1 == ctype) {
			return OE_DEEP_BID_TYPE_MAPPING.getOrDefault(enumKey, defaultValue);
		} else if (8 == ctype) {
			return TX_DEEP_BID_TYPE_MAPPING.getOrDefault(enumKey, defaultValue);
		} else {
			return defaultValue;
		}
	}

}
