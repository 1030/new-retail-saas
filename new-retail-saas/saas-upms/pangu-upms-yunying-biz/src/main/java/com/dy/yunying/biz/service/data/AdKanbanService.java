package com.dy.yunying.biz.service.data;


import com.dy.yunying.api.dto.AdKanbanDto;
import com.dy.yunying.api.entity.ExcelData;
import com.dy.yunying.api.vo.AdKanbanOverviewVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;


public interface AdKanbanService {

	/**
	 * 查询大盘看板统计数据
	 * @param req
	 * @return
	 */
    R<AdKanbanOverviewVo> selectKanbanStatitic(AdKanbanDto req);


	List<AdKanbanOverviewVo> excelKanbanStatitic(AdKanbanDto req);
}
