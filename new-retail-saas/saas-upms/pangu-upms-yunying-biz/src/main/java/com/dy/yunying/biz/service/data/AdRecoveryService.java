package com.dy.yunying.biz.service.data;

import com.dy.yunying.api.dto.AdRecoveryDto;
import com.pig4cloud.pig.common.core.util.R;

public interface AdRecoveryService {
	R selectAdRecoverySource(AdRecoveryDto req);
}
