package com.dy.yunying.biz.controller.datacenter;

import com.dy.yunying.api.datacenter.dto.DailyDataDto;
import com.dy.yunying.api.datacenter.dto.TimeSharingGameDTO;
import com.dy.yunying.api.datacenter.vo.DailyDataVO;
import com.dy.yunying.api.datacenter.vo.TimeSharingGameDataVO;
import com.dy.yunying.biz.service.datacenter.TimeSharingGameDataService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.DateUtil;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 分时游戏数据
 * @author sunyq
 * @date 2022/8/17 14:41
 */
@Slf4j
@RestController("timeSharingGameData")
@RequestMapping("/dataCenter/timeSharingGameData")
@RequiredArgsConstructor
public class TimeSharingGameDataController {

	private final TimeSharingGameDataService timeSharingGameDataService;

	/**
	 * 列表
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/list")
	public R<List<TimeSharingGameDataVO>> list(@Valid @RequestBody TimeSharingGameDTO req) {
		//
		String startTime = req.getStartTime();
		String endTime = req.getEndTime();
		if (StringUtils.isEmpty(startTime) || StringUtils.isEmpty(endTime)){
			return R.failed("请选择时间！");
		}

		long daySub = DateUtils.getDaySub(startTime, endTime);
		if (daySub > 0L){
			return R.failed("时间选择不能跨天！");
		}

		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(startTime, formatter);
		if (date.isAfter(localDate)){
			return R.failed("时间选择不能大于当前时间");
		}

		try {
			List<TimeSharingGameDataVO> list = timeSharingGameDataService.list(req);
			return R.ok(list);
		} catch (Exception e) {
			log.error("查询分时游戏数据列表数据失败.....{}",e);
			return R.failed("查询分时游戏数据列表数据失败");
		}
	}


	/**
	 * 汇总
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/collect")
	public R<TimeSharingGameDataVO> collect(@Valid @RequestBody TimeSharingGameDTO req) {
		//
		String startTime = req.getStartTime();
		String endTime = req.getEndTime();
		if (StringUtils.isEmpty(startTime) || StringUtils.isEmpty(endTime)){
			return R.failed("请选择时间！");
		}
		long daySub = DateUtils.getDaySub(startTime, endTime);
		if (daySub > 0L){
			return R.failed("时间选择不能跨天！");
		}
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(startTime, formatter);
		if (date.isAfter(localDate)){
			return R.failed("时间选择不能大于当前时间");
		}
		try {
			TimeSharingGameDataVO timeSharingGameDataVO = timeSharingGameDataService.collect(req);
			return R.ok(timeSharingGameDataVO);
		} catch (Exception e) {
			log.error("查询分时游戏数据汇总数据失败.....{}",e);
			return R.failed("查询分时游戏数据汇总数据失败");
		}
	}


	/**
	 * 汇总
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/export")
	public void export(@Valid @RequestBody TimeSharingGameDTO req,HttpServletResponse response, HttpServletRequest request) {
		//
		try {
			List<TimeSharingGameDataVO> result = new ArrayList<>();
			List<TimeSharingGameDataVO> list = timeSharingGameDataService.list(req);

			TimeSharingGameDataVO timeSharingGameDataVO = timeSharingGameDataService.collect(req);
			if (CollectionUtils.isNotEmpty(list)){
				result.addAll(list);
			}
			if (ObjectUtils.isNotEmpty(timeSharingGameDataVO)){
				result.add(timeSharingGameDataVO);
			}
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(result);
			String sheetName = "分时游戏数据";
			ExportUtils.exportExcelData(request, response, String.format("%s-%s.xlsx", sheetName, DateUtils.getCurrentTimeNoUnderline()), sheetName, req.getTitles(), req.getColumns(), resultListMap);
		} catch (Exception e) {
			log.error("分时游戏数据导出异常, 异常原因：{}",e.toString());
			throw new BusinessException("导出异常");
		}
	}



}
