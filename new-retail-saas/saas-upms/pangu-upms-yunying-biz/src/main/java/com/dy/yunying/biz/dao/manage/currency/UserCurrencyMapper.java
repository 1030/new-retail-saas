package com.dy.yunying.biz.dao.manage.currency;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.currency.UserCurrency;
import org.apache.ibatis.annotations.Mapper;

/**
* @author hejiale
* @description 针对表【user_currency(账号平台币信息)】的数据库操作Mapper
* @createDate 2022-03-23 15:40:47
*/
@Mapper
public interface UserCurrencyMapper extends BaseMapper<UserCurrency> {

}




