package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbActivityRoleInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 活动角色信息表
 *
 * @author chengang
 * @version 2021-10-28 12:02:13
 * table: hb_activity_role_info
 */
@Mapper
public interface HbActivityRoleInfoMapper extends BaseMapper<HbActivityRoleInfo> {

	/**
	 * 一次通过提现
	 *
	 * @param record
	 * @return
	 */
	int updatePassedOnceByRoleKey(HbActivityRoleInfo record);

	/**
	 * 一次拒绝提现
	 *
	 * @param record
	 * @return
	 */
	int updateRejectOnceByRoleKey(HbActivityRoleInfo record);

}


