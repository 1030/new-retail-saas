package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.WanProhibitLog;

/**
 * @Entity com.dy.yunying.biz.domain.WanProhibitLog
 */
public interface WanProhibitLogMapper extends BaseMapper<WanProhibitLog> {

}




