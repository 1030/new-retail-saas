package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.AdKpi;
import com.dy.yunying.api.entity.AdKpiDO;
import com.dy.yunying.api.req.AdKpiReq;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: nml
 * @time: 2021/7/8 14:28
 **/
@Component
public interface AdKpiMapper extends BaseMapper<AdKpi> {

	IPage<AdKpiDO> queryAdKpiPage(AdKpiReq req);

}
