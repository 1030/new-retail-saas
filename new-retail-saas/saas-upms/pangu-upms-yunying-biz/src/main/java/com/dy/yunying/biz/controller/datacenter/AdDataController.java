package com.dy.yunying.biz.controller.datacenter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.datacenter.dto.AdDataDto;
import com.dy.yunying.api.datacenter.export.AdExportDataVo;
import com.dy.yunying.api.datacenter.export.XingTuExportDataDTO;
import com.dy.yunying.api.datacenter.vo.AdDataVo;
import com.dy.yunying.biz.service.datacenter.AdDataService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportAlibabaUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 广告数据报表相关接口
 *
 * @author ：lile
 * @date ：2021/6/17 13:58
 * @description：
 * @modified By：
 */
@Slf4j
@RestController("adData")
@RequestMapping("/dataCenter/adData")
@RequiredArgsConstructor
public class AdDataController {

	private final AdDataService adDataService;

	/**
	 * 广告数据报表总数
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping(value = "/count")
	public R countDataTotal(@Valid AdDataDto req) {
		return adDataService.countDataTotal(req);
	}

	/**
	 * 广告数据报表分页数据
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping("/list")
	public R selectAdDataSource(@Valid AdDataDto req) {
		return adDataService.selectAdDataSource(req);
	}

	/**
	 * 广告数据报表导出
	 *
	 * @param req
	 * @return
	 */
	@ResponseExcel(name = "广告数据报表导出", sheet = "广告数据报表导出")
	@RequestMapping("/excel")
	public R excelAdDataSource(@Valid AdDataDto req, HttpServletResponse response, HttpServletRequest request) {
		String sheetName = "广告数据报表导出";
		try {
			List<AdDataVo> resultList = adDataService.excelAdDataSource(req);

			String fileName = URLEncoder.encode("广告数据报表-"+ DateUtils.getCurrentTimeNoUnderline(), "UTF-8").replaceAll("\\+", "%20");

			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(resultList);

			ExportUtils.spliceSuffix(resultListMap, Constant.PERCENT, Constant.DEFAULT_VALUE,
					"regRatio", "roi1", "weekRoi", "monthRoi", "allRoi","duplicateDeviceRatio",
					"regPayRatio", "retention2Ratio", "createRoleRate", "certifiedRate", "activePayRate", "periodPayRate");

			List<AdExportDataVo> list= new ArrayList<>();

			JSONArray array = JSONArray.parseArray(JSON.toJSONString(resultListMap));

			for (int i = 0; i < array.size(); i++) {
				JSONObject jsonObject = array.getJSONObject(i);
				AdExportDataVo detail = JSON.parseObject(String.valueOf(jsonObject), AdExportDataVo.class);
				list.add(detail);
			}

			ExportAlibabaUtils.exportExcelData(response,sheetName,fileName,req.getColumns(),list,AdExportDataVo.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
