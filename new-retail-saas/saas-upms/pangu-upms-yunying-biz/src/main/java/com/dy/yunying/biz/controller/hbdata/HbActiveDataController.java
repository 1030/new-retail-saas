package com.dy.yunying.biz.controller.hbdata;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.dy.yunying.api.req.hongbao.HbActiveDataReq;
import com.dy.yunying.api.vo.hbdata.*;
import com.dy.yunying.biz.service.hbdata.HbActiveDataService;
import com.dy.yunying.biz.utils.DateUtils;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.util.List;

/**
 * @author ：lile
 * @date ：2021/11/17 17:48
 * @description：
 * @modified By：
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/hbData")
@Api(value = "hbData", tags = "活动数据")
@Slf4j
public class HbActiveDataController {

	@Autowired
	private HbActiveDataService hbActiveDataService;

	/**
	 * 弹窗公告展示，消息推送
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@PostMapping("/popupList")
	public R popupList(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.popupList(req);
	}

	/**
	 * 任务模块页PV/UV
	 */
	@ResponseBody
	@PostMapping("/activeTypePU")
	public R activeTypePU(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.activeTypePU(req);
	}

	/**
	 * 点击一键提现按钮数，点击提现记录按钮数
	 */
	@ResponseBody
	@PostMapping("/activeCash")
	public R activeCash(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.activeCash(req);
	}

	/**
	 * 活动规则 统计PV、UV
	 */
	@ResponseBody
	@PostMapping("/activeRule")
	public R activeRule(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.activeRule(req);
	}

	/**
	 * 活动任务完成数
	 */
	@ResponseBody
	@PostMapping("/activeFinish")
	public R activeFinish(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.activeFinish(req);
	}


	/**
	 * 点击领取奖励数，代金券领取数
	 */
	@ResponseBody
	@PostMapping("/activeHbDraw")
	public R activeHbDraw(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.activeHbDraw(req);
	}

	/**
	 * SDK侧边栏红包UI点击数，收益明细按钮点击数
	 */
	@ResponseBody
	@PostMapping("/activeHbProfit")
	public R activeHbProfit(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.activeHbProfit(req);
	}


//	/**
//	 * 受邀页面
//	 */
//	@ResponseBody
//	@PostMapping("/invitedList")
//	public R invitedList(@Valid @RequestBody HbActiveDataReq req) {
//		return hbActiveDataService.invitedList(req);
//	}

	/**
	 * 活动任务分析
	 */
	@ResponseBody
	@PostMapping("/activityTaskDetail")
	public R activityTaskDetail(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.activityTaskDetail(req);
	}

	/**
	 * 活动访问和参与概况
	 */
	@ResponseBody
	@PostMapping("/activityVisitDetail")
	public R activityVisitDetail(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.activityVisitDetail(req);
	}

	/**
	 * 消息及浮标推送
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@PostMapping("/messageList")
	public R messageList(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.messageList(req);
	}

	/**
	 * SDK浮标特效点击数
	 */
	@ResponseBody
	@PostMapping("/floatClick")
	public R floatClick(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.floatClick(req);
	}

	/**
	 * 邀请弹框展示数
	 */
	@ResponseBody
	@PostMapping("/invitePv")
	public R invitePvList(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.invitePvList(req);
	}

	/**
	 * 各个档位游戏货币兑换数，各个档位现金提现数
	 */
	@ResponseBody
	@PostMapping("/activeCashConfig")
	public R activeCashConfig(@Valid @RequestBody HbActiveDataReq req) {
		return hbActiveDataService.activeCashConfig(req);
	}

	@PostMapping("/export")
	@PreAuthorize("@pms.hasPermission('EXPORT_ACTIVITY_DATA_BTN')")
	public void export4RedPack(@RequestBody HbActiveDataReq req, HttpServletResponse response) {
		try {
			//活动任务分析
			R task = hbActiveDataService.activityTaskDetail(req);
			//活动访问和参与概况
			R visit = hbActiveDataService.activityVisitDetail(req);
			//消息及浮标推送
			R message = hbActiveDataService.messageList(req);
			//SDK浮标特效点击数
			R ft = hbActiveDataService.floatClick(req);
			//邀请弹框展示数
			R invite = hbActiveDataService.invitePvList(req);
			//各个档位游戏货币兑换数，各个档位现金提现数
			R cash = hbActiveDataService.activeCashConfig(req);

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setCharacterEncoding("utf-8");
			String fileName = URLEncoder.encode("活动数据-"+ DateUtils.getCurrentTimeNoUnderline(), "UTF-8").replaceAll("\\+", "%20");
			response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
			response.setHeader("file-name", fileName);
			ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
			WriteSheet writeSheet0 = EasyExcel.writerSheet(0, "活动任务分析")
					.excludeColumnFiledNames(Lists.newArrayList("redpackId")).head(HbActiveHbDrawVo.class).build();
			WriteSheet writeSheet1 = EasyExcel.writerSheet(1, "活动访问和参与概况")
					.excludeColumnFiledNames(Lists.newArrayList("activityType")).head(HbActiveVisitDetailVo.class).build();
			WriteSheet writeSheet2 = EasyExcel.writerSheet(2, "消息及浮标推送").head(HbMessageVo.class).build();
			WriteSheet writeSheet3 = EasyExcel.writerSheet(3, "SDK浮标特效点击数").head(HbFloatClickVo.class).build();
			WriteSheet writeSheet4 = EasyExcel.writerSheet(4, "邀请弹框展示数").head(HbInvitePvVo.class).build();
			WriteSheet writeSheet5 = EasyExcel.writerSheet(5, "各个档位游戏货币兑换数，各个档位现金提现数")
					.excludeColumnFiledNames(Lists.newArrayList("cashType","cashConfig","moneySum")).head(HbActiveCashConfigVo.class).build();
			excelWriter.write((List<HbActiveHbDrawVo>) task.getData(),writeSheet0);
			excelWriter.write((List<HbActiveVisitDetailVo>) visit.getData(),writeSheet1);
			excelWriter.write((List<HbMessageVo>) message.getData(),writeSheet2);
			excelWriter.write((List<HbFloatClickVo>) ft.getData(),writeSheet3);
			excelWriter.write((List<HbInvitePvVo>) invite.getData(),writeSheet4);
			excelWriter.write((List<HbActiveCashConfigVo>) cash.getData(),writeSheet5);
			excelWriter.finish();
		} catch (Exception e) {
			log.error("导出活动数据失败：{}", e.getMessage());
		}
	}
}
