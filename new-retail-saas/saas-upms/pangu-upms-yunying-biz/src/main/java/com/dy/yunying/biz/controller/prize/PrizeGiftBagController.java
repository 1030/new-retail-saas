package com.dy.yunying.biz.controller.prize;

import com.dy.yunying.api.req.prize.PrizeGiftBagReq;
import com.dy.yunying.biz.service.prize.PrizeGiftBagService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 奖品礼包码
 *
 * @author chenxiang
 * @version 2022-04-25 16:25:54 table: prize_gift_bag
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/prizeGiftBag")
public class PrizeGiftBagController {

	private final PrizeGiftBagService prizeGiftBagService;

	@RequestMapping("/list")
	public R list(@RequestBody PrizeGiftBagReq record) {
		return R.ok();
	}



}
