package com.dy.yunying.biz.service.hongbao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbActivityDynamicType;
import com.dy.yunying.biz.dao.ads.hongbao.HbActivityDynamicTypeMapper;
import com.dy.yunying.biz.service.hongbao.HbActivityDynamicTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 活动动态类型
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class HbActivityDynamicTypeServiceImpl extends ServiceImpl<HbActivityDynamicTypeMapper, HbActivityDynamicType> implements HbActivityDynamicTypeService {
}
