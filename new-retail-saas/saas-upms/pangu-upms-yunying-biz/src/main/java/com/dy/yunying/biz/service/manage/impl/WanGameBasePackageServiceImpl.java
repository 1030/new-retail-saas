package com.dy.yunying.biz.service.manage.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.req.WanGameBasePackageReq;
import com.dy.yunying.api.vo.GamePackageManageVO;
import com.dy.yunying.biz.dao.manage.WanGameBasePackageDOMapper;
import com.dy.yunying.biz.service.manage.WanGameBasePackageService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class WanGameBasePackageServiceImpl implements WanGameBasePackageService {

    @Autowired
    private WanGameBasePackageDOMapper wanGameBasePackageDOMapper;

	@Value("${pack.app_file_url_root}")
	private String app_file_url_root;

    /*查询子游戏基础包信息列表*/
    @Override
    public IPage<GamePackageManageVO> selectWanGameBasePackageList(WanGameBasePackageReq req) {
        /*查询基础包信息，分页查询*/
		IPage<GamePackageManageVO> ipage = wanGameBasePackageDOMapper.queryWanGameBasePackageList(req);
		if(ipage != null && CollectionUtils.isNotEmpty(ipage.getRecords())) {
			for (GamePackageManageVO gamePackageManageVO : ipage.getRecords()) {
				String path = gamePackageManageVO.getPath();
				if (StringUtils.isNotBlank(path)) {
					gamePackageManageVO.setUrl(app_file_url_root + path);
				} else {
					gamePackageManageVO.setUrl("");
				}
			}
		}
    	return ipage;
    }

}
