package com.dy.yunying.biz.service.manage;

import com.dy.yunying.api.dto.AccountManageDTO;
import com.dy.yunying.api.dto.AccountProhibitDTO;
import com.dy.yunying.api.vo.AccountManageVO;
import com.pig4cloud.pig.common.core.mybatis.Page;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * @author sunyq
 * @date 2022/8/19 11:40
 */
public interface AccountManageService {
	/**
	 * 根据条件查询账户信息
	 * @param accountManageDTO
	 * @return
	 */
	Page<AccountManageVO> selectWanUserPageByCondition(AccountManageDTO accountManageDTO);

	/**
	 * 根据条件查询账户信息
	 * @param accountManageDTO
	 * @return
	 */
	long selectTotalByCondition(AccountManageDTO accountManageDTO);

	/**
	 * 新增账号封禁
	 * @param dto
	 * @return
	 */
	R addAccountProhibit(AccountProhibitDTO dto);

	/**
	 * 账号解封
	 * @param req
	 * @return
	 */
	R accountLeft(AccountProhibitDTO req);
}
