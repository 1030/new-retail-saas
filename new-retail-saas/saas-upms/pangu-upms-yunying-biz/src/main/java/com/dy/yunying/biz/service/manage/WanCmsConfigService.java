package com.dy.yunying.biz.service.manage;

import com.dy.yunying.api.entity.WanCmsConfigDO;

public interface WanCmsConfigService {

	int deleteByPrimaryKey(Integer id);

	int insert(WanCmsConfigDO record);

	int insertSelective(WanCmsConfigDO record);

	WanCmsConfigDO selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(WanCmsConfigDO record);

	int updateByPrimaryKey(WanCmsConfigDO record);

	/**
	 * 插入cms配置
	 *
	 * @param name
	 * @param cfgstatus
	 * @param cfgtype
	 * @param typeid
	 * @return
	 */
	int insertWanCmsConfig(String name, String cfgstatus, String cfgtype, Integer typeid);

}
