package com.dy.yunying.biz.datasource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * @author leisw
 * @date 2022/9/15 11:40
 */
@Configuration
public class DorisDataSourceConfig {
	@Bean(name = "dorisTemplate")
	public JdbcTemplate dorisDailyDataTemplate(
			@Qualifier("dorisDataSource") DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean(name = "dorisDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.dorisdata")
	public DataSource getDateSource() {
		return DataSourceBuilder.create().build();
	}
}
