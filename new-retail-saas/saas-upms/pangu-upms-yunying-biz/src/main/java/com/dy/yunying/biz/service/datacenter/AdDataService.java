package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.AdDataDto;
import com.dy.yunying.api.datacenter.vo.AdDataVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 广告数据报表相关方法
 */
public interface AdDataService {

	/**
	 * 广告数据报表总数
	 *
	 * @param req
	 * @return
	 */
	R countDataTotal(AdDataDto req);

	/**
	 * 广告数据报表分页数据
	 *
	 * @param req
	 * @return
	 */
	R selectAdDataSource(AdDataDto req);

	/**
	 * 广告数据报表导出
	 *
	 * @param req
	 * @return
	 */
	List<AdDataVo> excelAdDataSource(AdDataDto req);

}
