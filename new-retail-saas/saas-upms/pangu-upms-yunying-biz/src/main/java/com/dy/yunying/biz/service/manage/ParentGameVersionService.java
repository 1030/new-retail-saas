package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.dto.ParentGamePackDto;
import com.dy.yunying.api.dto.SonGameDto;
import com.dy.yunying.api.entity.GameVersionDO;
import com.dy.yunying.api.entity.ParentGameVersionDO;
import com.dy.yunying.api.req.ParentGameVersionReq;
import com.dy.yunying.api.resp.ParentGameDataRes;
import com.dy.yunying.api.vo.ParentGameVersionVO;

import java.util.List;

/**
 * @Author: hjl
 * @Date: 2020/7/21 13:52
 */
public interface ParentGameVersionService {

	ParentGameVersionDO save(ParentGameVersionDO gameVersionDO);

	/**
	 * 逻辑删除
	 *
	 * @param versionId
	 */
	void deleteByPK(Long versionId);

	IPage<ParentGameVersionVO> page(ParentGameVersionReq req);

	ParentGameVersionDO getByPK(Long versionId);

	/**
	 * 查询游戏可用的最新版本
	 *
	 * @param gameId
	 * @return
	 */
	ParentGameVersionDO getLatestByGameId(Long gameId, Integer packType);

	ParentGamePackDto getParentGamePackByPK(Long versionId);

	/**
	 * 更新子游戏基础包
	 *
	 * @param parentGameVersionDO
	 * @param sonGameList
	 */
	List<GameVersionDO> updateBasePack(ParentGameVersionDO parentGameVersionDO, List<SonGameDto> sonGameList);

	List<GameVersionDO> updateBasePack(ParentGameVersionDO parentGameVersionDO, Long sonGameId);

	/**
	 * 更新子游戏基础包，及已打包主渠道的测试包
	 *
	 * @param parentGameVersionDO
	 * @param sonGameList
	 */
	List<GameVersionDO> updateBasePack2(ParentGameVersionDO parentGameVersionDO, List<SonGameDto> sonGameList);

	List<GameVersionDO> updateBasePack2(ParentGameVersionDO parentGameVersionDO, Long sonGameId);

	List<ParentGameVersionDO> getPackTypeByGameId(Long gameId);

	List<ParentGameVersionDO> selectListByPgId(List<ParentGameDataRes> pgIdList);

}
