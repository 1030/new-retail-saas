package com.dy.yunying.biz.dao.clickhouse3399;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.ToutiaoClick;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @description: toutiaoclick
 * @author yuwenfeng
 * @date 2022/3/2 15:53
 */
@Mapper
public interface ToutiaoClickMapper extends BaseMapper<ToutiaoClick> {

	@Select("SELECT callback_url FROM toutiao_click WHERE convert_id = #{adPlatformId} ORDER BY ts DESC LIMIT 1")
	String getCallBackUrlByConvertId(@Param("adPlatformId") String adPlatformId);
}
