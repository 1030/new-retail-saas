package com.dy.yunying.biz.service.currency;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.currency.UserCurrency;

/**
* @author hejiale
* @description 针对表【user_currency(账号平台币信息)】的数据库操作Service
* @createDate 2022-03-23 15:40:47
*/
public interface UserCurrencyService extends IService<UserCurrency> {

}
