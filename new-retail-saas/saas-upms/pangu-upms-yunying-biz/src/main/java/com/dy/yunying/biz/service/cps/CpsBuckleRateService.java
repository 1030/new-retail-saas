package com.dy.yunying.biz.service.cps;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.cps.CpsBuckleRate;
import com.dy.yunying.api.cps.CpsBuckleRateReq;
import com.dy.yunying.api.cps.vo.CpsBuckleRateVO;
import com.pig4cloud.pig.common.core.mybatis.Page;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 *
 * @author Administrator
 */
public interface CpsBuckleRateService extends IService<CpsBuckleRate> {
	/**
	 * 新增
	 * @param req
	 * @return
	 */
    R add(CpsBuckleRateReq req);

	/**
	 * 编辑
	 * @param req
	 * @return
	 */
	R edit(CpsBuckleRateReq req);

	/**
	 * 查询列表
	 * @param req
	 * @return
	 */
	R<IPage<CpsBuckleRateVO>> getPage(CpsBuckleRateReq req);
}
