package com.dy.yunying.biz.controller.hongbao;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.hongbao.HbActivityCenterDO;
import com.dy.yunying.api.entity.hongbao.HbActivityDynamicType;
import com.dy.yunying.api.vo.HbActivityCenterVO;
import com.dy.yunying.api.vo.HbActivityCenterVO.Edit;
import com.dy.yunying.api.vo.HbActivityCenterVO.Save;
import com.dy.yunying.biz.service.hongbao.HbActivityCenterService;
import com.dy.yunying.biz.service.hongbao.HbActivityDynamicTypeService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 活动中心管理Controller
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/activityCenter")
@RequiredArgsConstructor
public class HbActivityCenterController {

	private static final Map<String, String> ORDER_BY_NAME_MAPPING = new HashMap<String, String>(){{
		put("activityOrder", "activity_order");
	}};

	private final HbActivityCenterService hbActivityCenterService;

	private final HbActivityDynamicTypeService hbActivityDynamicTypeService;

	/**
	 * 活动中心分页列表
	 *
	 * @return
	 */
	@PostMapping("/page")
	public R<Page<HbActivityCenterDO>> page(@RequestBody HbActivityCenterVO activityCenter) {
		if (StringUtils.isNotBlank(activityCenter.getOrderByName())) {
			activityCenter.setOrderByName(ORDER_BY_NAME_MAPPING.get(activityCenter.getOrderByName()));
		}
		final Page<HbActivityCenterDO> page = hbActivityCenterService.getPage(activityCenter);
		dealData(page);
		return R.ok(page);
	}


	private void dealData(Page<HbActivityCenterDO> page){
		if (Objects.nonNull(page) && !CollectionUtils.isEmpty(page.getRecords())) {
			List<HbActivityCenterDO> list = page.getRecords();
			//增加手机号的搜索
			Set<Long> typeids = list.stream().map(HbActivityCenterDO::getDynamicTypeId).filter(Objects::nonNull).collect(Collectors.toSet());
			List<HbActivityDynamicType> typeList = CollectionUtils.isEmpty(typeids) ? new ArrayList<>() :  hbActivityDynamicTypeService.list(Wrappers.<HbActivityDynamicType>lambdaQuery().select(HbActivityDynamicType::getId, HbActivityDynamicType::getShowName).in(HbActivityDynamicType::getId, typeids));
			//处理特殊情况为空
			final Map<Long, String> typeMap = CollectionUtils.isEmpty(typeList) ? new HashMap<>() :  typeList.stream().collect(Collectors.toMap(HbActivityDynamicType::getId, type -> StringUtils.isBlank(type.getShowName()) ? "-" : type.getShowName(), (k1, k2) -> k1));
			list.forEach((item) -> {
				if (CollectionUtil.isNotEmpty(typeMap) && item.getDynamicTypeId() != null) {
					item.setShowName(typeMap.get(item.getDynamicTypeId()));
				}
			});
		}

	}


	/**
	 * 活动中心保存活动
	 *
	 * @param activityCenter
	 * @return
	 */
	@PostMapping("/save")
	public R<Void> save(@RequestBody @Validated(Save.class) HbActivityCenterVO activityCenter) {
		try {
			R result = this.checkParam(activityCenter);
			if (0 != result.getCode()){
				return result;
			}
			hbActivityCenterService.doSave(activityCenter);
			return R.ok();
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 活动中心修改活动
	 *
	 * @param activityCenter
	 * @return
	 */
	@PostMapping("/edit")
	public R<Void> edit(@RequestBody @Validated(Edit.class) HbActivityCenterVO activityCenter) {
		try {
			R result = this.checkParam(activityCenter);
			if (0 != result.getCode()){
				return result;
			}
			hbActivityCenterService.doEdit(activityCenter);
			return R.ok();
		} catch (Exception e) {
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 活动中心删除活动
	 *
	 * @param id
	 * @return
	 */
	@DeleteMapping("/delete")
	public R<Void> delete(@RequestParam @NotNull(message = "ID不能为空") Long id) {
		hbActivityCenterService.doDelete(id);
		return R.ok();
	}

	public R checkParam(HbActivityCenterVO activityCenter){
		if (3 == activityCenter.getSourceType()) {
			String openUrl = activityCenter.getOpenUrl();
			String activityName = activityCenter.getActivityName();
			if (StringUtils.isBlank(activityName)){
				return R.failed("活动名称不能为空");
			}
			if (activityName.length() > 180){
				return R.failed("活动名称输入过长");
			}
			if (StringUtils.isBlank(openUrl)) {
				return R.failed("外部链接地址不能为空");
			}
			if (openUrl.length() > 800){
				return R.failed("外部链接地址输入过长");
			}
			if (openUrl.indexOf("http://") != 0 && openUrl.indexOf("https://") != 0){
				return R.failed("请输入正确的外部链接地址");
			}
			if (StringUtils.isBlank(activityCenter.getStartTime())) {
				return R.failed("活动开始时间不能为空");
			}
			if (StringUtils.isBlank(activityCenter.getFinishTime())) {
				return R.failed("活动开始时间不能为空");
			}
			if (Objects.isNull(activityCenter.getOpenType())){
				return R.failed("请选择打开方式");
			}
			if (CollectionUtils.isEmpty(activityCenter.getGameDataList())) {
				return R.failed("游戏范围集合不能为空");
			}
			if (CollectionUtils.isEmpty(activityCenter.getAreaDataList())) {
				return R.failed("区服范围集合不能为空");
			}
			if (CollectionUtils.isEmpty(activityCenter.getChannelDataList())) {
				return R.failed("渠道范围集合不能为空");
			}
		}else{
			if (Objects.isNull(activityCenter.getActivityId())){
				return R.failed("活动ID不能为空");
			}
			if (Objects.isNull(activityCenter.getSourceType())){
				return R.failed("活动类型不能为空");
			}
			if (Objects.isNull(activityCenter.getOpenType())){
				return R.failed("请选择打开方式");
			}
		}
		return R.ok();
	}

}
