package com.dy.yunying.biz.service.prize;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.prize.PrizeConfig;
import com.dy.yunying.api.entity.prize.PrizeGiftBag;
import com.pig4cloud.pig.common.core.util.R;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 奖品礼包码
 *
 * @author chenxiang
 * @version 2022-04-25 16:25:54 table: prize_gift_bag
 */
public interface PrizeGiftBagService extends IService<PrizeGiftBag> {

	/**
	 * 保存唯一礼包码信息
	 * @param file 礼包码文件
	 * @param prizeType prizeType
	 * @param sourceId sourceId
	 * @return
	 */
	R<List<String>> saveUniqueGiftBag(MultipartFile file, Integer prizeType, Long sourceId);

	/**
	 * 保存通用礼包码信息
	 * @param prizeConfig prizeConfig
	 * @return
	 */
	R saveCommonGiftBag(PrizeConfig prizeConfig);

}
