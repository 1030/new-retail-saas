package com.dy.yunying.biz.service.hongbao;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbActivityKvpic;
import com.dy.yunying.api.vo.HbActivityKvpicVO;

/**
 * kv图库表服务接口
 *
 * @author zjz
 * @description 由 zjz 创建
 */
public interface HbActivityKvpicService extends IService<HbActivityKvpic> {

	/**
	 *
	 * @param hbActivityKvpic
	 * @return
	 */
	Page<HbActivityKvpic> getKvPage(HbActivityKvpicVO hbActivityKvpic);

	/**
	 *
	 * @param hbActivityKvpic
	 */
	void doSave(HbActivityKvpic hbActivityKvpic);

	/**
	 *
	 * @param hbActivityKvpic
	 */
	void doEdit(HbActivityKvpic hbActivityKvpic);

	/**
	 *
	 * @param id
	 */
	void doDelete(Long id);
}
