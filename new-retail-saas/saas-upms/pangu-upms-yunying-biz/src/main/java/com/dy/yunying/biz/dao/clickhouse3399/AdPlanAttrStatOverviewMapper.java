package com.dy.yunying.biz.dao.clickhouse3399;


import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.dto.AdOverviewDto;
import com.dy.yunying.api.dto.AdOverviewDto2;
import com.dy.yunying.api.vo.AdDataAnalysisVO;
import com.dy.yunying.api.vo.AdPlanOverviewVo;
import com.dy.yunying.api.vo.PlanAttrAnalyseSearchVo;
import com.pig4cloud.pig.api.vo.PlanBaseAttrVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdPlanAttrStatOverviewMapper extends BaseMapper<AdPlanOverviewVo> {
	/**
	 * 分页查询广告报表
	 * @param page
	 * @param req
	 * @return
	 */
	@SqlParser(filter=true)
	IPage<AdPlanOverviewVo> selectAdOverviewSourceTable(Page page,@Param("param") AdOverviewDto req);

	/**
	 * 查询广告列表
	 * @param req
	 * @return
	 */
	@SqlParser(filter=true)
	List<AdPlanOverviewVo> selectAdOverviewSourceTable(@Param("param") AdOverviewDto req,Boolean notPage);

	/**
	 * 查询广告列表
	 * @param req
	 * @return
	 */
	@SqlParser(filter=true)
	IPage<AdPlanOverviewVo> list(Page page,@Param("param") AdOverviewDto req);

	@SqlParser(filter=true)
	List<AdDataAnalysisVO> list(@Param("param") AdOverviewDto2 req);

	@SqlParser(filter=true)
	List<PlanBaseAttrVo> selectPlanAttrAnalyseReoport(@Param("param") PlanAttrAnalyseSearchVo searchVo);
	@SqlParser(filter=true)
	List<PlanBaseAttrVo> selectPlanAttrAnalyseReoport(Page<PlanBaseAttrVo> mapPage, @Param("param") PlanAttrAnalyseSearchVo searchVo);
	Integer getPlatformByParentchlCode(@Param("parentchlCode")  String parentchlCode);
}
