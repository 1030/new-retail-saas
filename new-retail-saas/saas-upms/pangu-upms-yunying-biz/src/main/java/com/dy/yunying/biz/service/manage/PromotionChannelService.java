package com.dy.yunying.biz.service.manage;

import com.dy.yunying.api.entity.PromotionChannelDO;

/**
 * @Author: hjl
 * @Date: 2020-9-1 14:30:18
 */
public interface PromotionChannelService {

	PromotionChannelDO updateByPKSelective(PromotionChannelDO promotionChannelDO);
}
