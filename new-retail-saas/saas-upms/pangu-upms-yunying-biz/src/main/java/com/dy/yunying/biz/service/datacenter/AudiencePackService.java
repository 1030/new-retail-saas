package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.dto.audience.AudiencePackDto;
import com.dy.yunying.api.vo.audience.AudiencePack;
import com.pig4cloud.pig.common.core.util.R;

public interface AudiencePackService {

	/**
	 * 获取指定条件的设备信息
	 * @param dto
	 * @param isPage
	 * @return
	 */
	AudiencePack page(AudiencePackDto dto, boolean isPage);

	AudiencePack count(AudiencePackDto dto, boolean isPage);

	/**
	 * 打包
	 * @param dto
	 * @return
	 */
	R pack(AudiencePackDto dto);
}
