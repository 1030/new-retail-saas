package com.dy.yunying.biz.service.manage.impl;

import com.dy.yunying.api.entity.WanGameImgsDO;
import com.dy.yunying.api.req.WanGameImgsReq;
import com.dy.yunying.biz.dao.manage.ext.WanGameImgsDOMapperExt;
import com.dy.yunying.biz.service.manage.WanGameImgsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class WanGameImgsServiceImpl implements WanGameImgsService {

	@Autowired
	private WanGameImgsDOMapperExt wanGameImgsDOMapperExt;

	@Override
	public int deleteByPrimaryKey(Long id) {
		return wanGameImgsDOMapperExt.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(WanGameImgsDO record) {
		return wanGameImgsDOMapperExt.insert(record);
	}

	@Override
	public int insertSelective(WanGameImgsDO record) {
		return wanGameImgsDOMapperExt.insertSelective(record);
	}

	@Override
	public WanGameImgsDO selectByPrimaryKey(Long id) {
		return wanGameImgsDOMapperExt.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(WanGameImgsDO record) {
		return wanGameImgsDOMapperExt.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(WanGameImgsDO record) {
		return wanGameImgsDOMapperExt.updateByPrimaryKey(record);
	}

	/**
	 * 批量新增图片
	 *
	 * @author cx
	 */
	@Override
	public boolean insertWanGameImgsList(List<WanGameImgsReq> list) {
		return wanGameImgsDOMapperExt.insertWanGameImgsList(list);
	}

	/**
	 * 按游戏id删除游戏图片
	 *
	 * @author cx
	 * @date 2020年7月22日09:58:01
	 */
	@Override
	public boolean deleteWanGameImgs(WanGameImgsReq entity) {
		return wanGameImgsDOMapperExt.deleteWanGameImgs(entity);
	}

}
