package com.dy.yunying.biz.service.raffle.impl;

import com.dy.yunying.api.req.raffle.RaffleDataReq;
import com.dy.yunying.api.resp.raffle.RaffleDataDetail;
import com.dy.yunying.api.resp.raffle.RaffleDataSummary;
import com.dy.yunying.biz.dao.hbdataclickhouse.impl.RaffleDataDao;
import com.dy.yunying.biz.service.raffle.RaffleDataService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author chengang
 * @Date 2022/11/9
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class RaffleDataServiceImpl implements RaffleDataService {

	private final RaffleDataDao raffleDataDao;

	@Override
	public List<RaffleDataDetail> page(RaffleDataReq req,Boolean page) {
		return raffleDataDao.list(req,page);
	}

	@Override
	public RaffleDataSummary summary(RaffleDataReq req) {
		return raffleDataDao.summary(req);
	}

	@Override
	public Long count(RaffleDataReq req) {
		return raffleDataDao.count(req);
	}
}
