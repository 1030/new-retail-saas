package com.dy.yunying.biz.service.currency;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.dto.currency.UserCurrencyRecordDto;
import com.dy.yunying.api.entity.currency.UserCurrencyRecord;
import com.dy.yunying.api.req.currency.UserCurrencyRecordReq;
import com.dy.yunying.api.vo.UserCurrencyRecordVO;
import com.pig4cloud.pig.common.core.util.R;
import java.util.List;


/**
* @author hejiale
* @description 针对表【user_currency_record(账号平台币信息)】的数据库操作Service
* @createDate 2022-03-25 09:56:27
*/
public interface UserCurrencyRecordService extends IService<UserCurrencyRecord> {

	R<IPage<UserCurrencyRecordVO>> pageDto(UserCurrencyRecordReq req);

	List<UserCurrencyRecordDto> exportRecord(UserCurrencyRecordReq req);

}
