package com.dy.yunying.biz.service.hongbao.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbActivity;
import com.dy.yunying.api.entity.hongbao.HbActivityNotice;
import com.dy.yunying.api.enums.ActivityStatusEnum;
import com.dy.yunying.api.req.hongbao.ActivityNoticeReq;
import com.dy.yunying.api.req.hongbao.OnlineActivityNoticeReq;
import com.dy.yunying.biz.dao.ads.hongbao.HbActivityMapper;
import com.dy.yunying.biz.dao.ads.hongbao.HbActivityNoticeMapper;
import com.dy.yunying.biz.service.hongbao.ActivityNoticeService;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author yuwenfeng
 * @description: 活动公告
 * @date 2021/11/3 15:32
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ActivityNoticeServiceImpl extends ServiceImpl<HbActivityNoticeMapper, HbActivityNotice> implements ActivityNoticeService {

	private final HbActivityMapper activityMapper;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R addNotice(ActivityNoticeReq activityNotice) {
		if (null == activityNotice.getActivityId() || activityNotice.getActivityId() <= 0) {
			return R.failed("活动ID不能为空");
		}
		HbActivity activity = activityMapper.selectOne(new QueryWrapper<HbActivity>().eq("id", activityNotice.getActivityId()).le("activity_status", ActivityStatusEnum.ACTIVITING.getStatus()));
		if (null == activity) {
			return R.failed("当前活动不存在或已下线，无法添加公告");
		}
		Date startTime = DateUtils.stringToDate(activityNotice.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == startTime) {
			return R.failed("开始时间不合法");
		}
		Date finishTime = DateUtils.stringToDate(activityNotice.getFinishTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == finishTime) {
			return R.failed("结束时间不合法");
		}
		if (finishTime.getTime() <= startTime.getTime()) {
			return R.failed("结束时间不能比开始时间小");
		}
		if (1 == activityNotice.getJumpOff()) {
			if (StringUtils.isBlank(activityNotice.getNoticeInfo())) {
				return R.failed("跳转打开时，详情不能为空");
			}
		}
		HbActivityNotice info = JSON.parseObject(JSON.toJSONString(activityNotice), HbActivityNotice.class);
		info.setNoticeTitle(activityNotice.getNoticeTitle().trim());
		info.setNoticeStatus(ActivityStatusEnum.READY.getStatus());
		info.setCreateTime(new Date());
		info.setCreateId(SecurityUtils.getUser().getId().longValue());
		info.setUpdateTime(new Date());
		info.setUpdateId(SecurityUtils.getUser().getId().longValue());
		baseMapper.insert(info);
		return R.ok();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R editNotice(ActivityNoticeReq activityNotice) {
		if (null == activityNotice.getId() || activityNotice.getId() <= 0) {
			return R.failed("ID不能为空");
		}
		HbActivityNotice info = baseMapper.selectOne(new QueryWrapper<HbActivityNotice>().eq("id", activityNotice.getId()).eq("notice_status", ActivityStatusEnum.READY.getStatus()));
		if (null == info) {
			return R.failed("只能对待上线的进行编辑");
		}
		HbActivity activity = activityMapper.selectOne(new QueryWrapper<HbActivity>().eq("id", info.getActivityId()).le("activity_status", ActivityStatusEnum.ACTIVITING.getStatus()));
		if (null == activity) {
			return R.failed("当前活动不存在或已下线，无法编辑公告");
		}
		Date startTime = DateUtils.stringToDate(activityNotice.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == startTime) {
			return R.failed("开始时间不合法");
		}
		Date finishTime = DateUtils.stringToDate(activityNotice.getFinishTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == finishTime) {
			return R.failed("结束时间不合法");
		}
		if (finishTime.getTime() <= startTime.getTime()) {
			return R.failed("结束时间不能比开始时间小");
		}
		if (1 == activityNotice.getJumpOff()) {
			if (StringUtils.isBlank(activityNotice.getNoticeInfo())) {
				return R.failed("跳转打开时，详情不能为空");
			}
		}
		info = JSON.parseObject(JSON.toJSONString(activityNotice), HbActivityNotice.class);
		info.setNoticeTitle(activityNotice.getNoticeTitle().trim());
		info.setActivityId(activity.getId());
		info.setNoticeStatus(ActivityStatusEnum.READY.getStatus());
		info.setUpdateTime(new Date());
		info.setUpdateId(SecurityUtils.getUser().getId().longValue());
		baseMapper.updateById(info);
		return R.ok();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R onlineNotice(OnlineActivityNoticeReq req) {
		if (null == req.getId() || req.getId() <= 0) {
			return R.failed("ID不能为空");
		}
		HbActivityNotice info = baseMapper.selectById(req.getId());
		if (null == info) {
			return R.failed("数据不存在");
		}
		if (2 != req.getNoticeStatus() && 3 != req.getNoticeStatus()) {
			return R.failed("公告状态数据不合法");
		}
		info.setNoticeStatus(req.getNoticeStatus());
		info.setUpdateTime(new Date());
		info.setUpdateId(SecurityUtils.getUser().getId().longValue());
		baseMapper.updateById(info);
		return R.ok();
	}
}
