package com.dy.yunying.biz.service.znfx.impl;

import com.alibaba.fastjson.JSON;
import com.dy.yunying.api.dto.RoiTopTenDto;
import com.dy.yunying.api.req.FutureMaterialReq;
import com.dy.yunying.api.vo.*;
import com.dy.yunying.biz.dao.ads.AdCreativeMaterialMapper;
import com.dy.yunying.biz.dao.znfx.CreateTopDataDao;
import com.dy.yunying.biz.dao.znfx.FutureMaterialDataDao;
import com.dy.yunying.biz.service.znfx.TopTenService;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: zhuxm
 * @time: 2023/3/21 14:30
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TopTenServiceImpl implements TopTenService {

	private final CreateTopDataDao createTopDataDao;
	private final AdCreativeMaterialMapper adCreativeMaterialMapper;
	private final FutureMaterialDataDao futureMaterialDataDao;
	@Override
	public List<RoiTopTenVo> topTen(RoiTopTenDto dto){
		List<RoiTopTenDaoVo> list = createTopDataDao.roiTopTen(dto);
		List<RoiTopTenVo> rlist =  new ArrayList<>();
		if(CollectionUtils.isEmpty(list)){
			return rlist;
		}
		List<String> cidList = list.stream().map(RoiTopTenDaoVo::getCid).collect(Collectors.toList());
		if(CollectionUtils.isEmpty(cidList)){
			return rlist;
		}
		//非头条体验版 头条体验版
		List<MaterialTopVo> materialTopVoList ="2".equals(dto.getExperience())?adCreativeMaterialMapper.getTopExperience(list): adCreativeMaterialMapper.getTopMaterial(cidList);
		Map<String, MaterialTopVo> map = materialTopVoList.stream().collect(Collectors.toMap(MaterialTopVo::getCid, a -> a));
		list.forEach(a -> {
			RoiTopTenVo vo = new RoiTopTenVo();
			vo.setSort(a.getSort());
			vo.setRoi(a.getRoi());
			vo.setCost(a.getCost());
			vo.setCid("2".equals(dto.getExperience())? a.getCid()+"-"+a.getAdid() : a.getCid());
			MaterialTopVo mt = map.get(a.getCid());
			if(mt==null){
				rlist.add(vo);
				return;
			}
			vo.setTmId(mt.getTmId());
			vo.setAdvertiserId(mt.getAdvertiserId());
			vo.setMaterialId(mt.getMaterialId());
			vo.setMaterialName(mt.getMaterialName());
			vo.setMaterialUrl(mt.getMaterialUrl());
			vo.setPageUrl(mt.getPageUrl());
			vo.setImageUrl(mt.getImageUrl());
			vo.setPageImage(mt.getPageImage());
			vo.setPageName(mt.getPageName());
			vo.setFormat(mt.getFormat());
			vo.setTitle(Lists.newArrayList(mt.getTitle()));
			if("2".equals(dto.getExperience())){
				List<String> titles = new ArrayList<>();
				if(StringUtils.isNotBlank(mt.getExTitle())){
					List<TopTitleVo> titleList = JSON.parseArray(mt.getExTitle(),TopTitleVo.class);
					titles = titleList.stream().map(TopTitleVo::getTitle).collect(Collectors.toList());
				}
				vo.setTitle(titles);
			}
			rlist.add(vo);
		});
		return rlist;
	}


	@Override
	public R futureMaterial(FutureMaterialReq req){
		List<FutureDaoVo> daolist = futureMaterialDataDao.futureMaterial(req);
		List<FutureMaterialVo> list = new ArrayList<>();
		if(CollectionUtils.isEmpty(daolist)){
			return R.ok(list);
		}
		//素材ID
		List<String> midList = daolist.stream().map(FutureDaoVo::getCid).collect(Collectors.toList());
		if(CollectionUtils.isEmpty(midList)){
			return R.ok(list);
		}
		List<MaterialDaoVo> daoList = adCreativeMaterialMapper.getFutureMaterial(midList);
		Map<String,MaterialDaoVo> map2 = daoList.stream().collect(Collectors.toMap(MaterialDaoVo::getMaterialId, a -> a));
		daolist.forEach(a->{
			FutureMaterialVo vo = new FutureMaterialVo();
			vo.setValue(a.getValue());
			MaterialDaoVo mvo = map2.get(a.getCid());
			if(mvo==null){
				list.add(vo);
				return;
			}
			vo.setMaterialName(mvo.getMaterialName());
			vo.setMaterialUrl(mvo.getMaterialUrl());
			vo.setMaterialId(mvo.getMaterialId());
			vo.setImageUrl(mvo.getImageUrl());
			vo.setFormat(mvo.getFormat());
			list.add(vo);
		});
		return R.ok(list);
	}


	@Override
	public int futureCount(FutureMaterialReq req){
		return futureMaterialDataDao.futureCount(req);
	}
}
