package com.dy.yunying.biz.service.manage.impl;

import com.dy.yunying.api.entity.WanGameChannelInfo;
import com.dy.yunying.api.entity.WanGameChannelInfoDO;
import com.dy.yunying.api.entity.WanGameChannelPlatformDO;
import com.dy.yunying.api.vo.ChannelManageVo;
import com.dy.yunying.biz.dao.manage.WanGameChannelInfoDao;
import com.dy.yunying.biz.service.manage.WanGameChannelInfoService;
import com.dy.yunying.biz.dao.manage.ext.WanGameChannelInfoDOMapperExt;
import com.dy.yunying.biz.service.manage.ChannelManageService;
import com.dy.yunying.biz.service.manage.GameChannelPlatformService;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.core.util.R;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class WanGameChannelInfoServiceImpl implements WanGameChannelInfoService {

	private static Logger logger = LoggerFactory.getLogger(WanGameChannelInfoServiceImpl.class);

	@Autowired
	private WanGameChannelInfoDao wanGameChannelInfoDao;

	@Autowired
	private WanGameChannelInfoDOMapperExt wanGameChannelInfoDOMapper;

	@Autowired
	private ChannelManageService channelManageService;

	@Autowired
	private GameChannelPlatformService gameChannelPlatformService;


	@Override
	public List<WanGameChannelInfo> selectWanGameChannelInfoListByCond(Map<String, Object> map) {
		return wanGameChannelInfoDao.selectWanGameChannelInfoListByCond(map);
	}

	@Override
	public WanGameChannelInfoDO getByChl(String chl) {
		return wanGameChannelInfoDOMapper.getByChl(chl);
	}

	@Override
	public WanGameChannelInfoDO updateByChl(WanGameChannelInfoDO gameChannelInfoDO) {
		wanGameChannelInfoDOMapper.updateByChlSelective(gameChannelInfoDO);
		return gameChannelInfoDO;
	}

	/**
	 * 修改包名
	 *
	 * @param wanGameChannelInfoDO
	 * @return
	 */
	@Override
	public R updateCodeName(WanGameChannelInfoDO wanGameChannelInfoDO) {
		int i = wanGameChannelInfoDOMapper.updateByChlSelective(wanGameChannelInfoDO);
		if (i > 0) {
			return R.ok("修改包名成功");
		} else {
			return R.failed("修改包名失败");
		}
	}

	@Override
	public void save(WanGameChannelInfoDO wanGameChannelInfoDO) {
		// 查询是否存在
		WanGameChannelInfoDO gameChannelInfoDB = wanGameChannelInfoDOMapper.getByChl(wanGameChannelInfoDO.getChl());
		if (gameChannelInfoDB == null) {
			Integer gameid = wanGameChannelInfoDO.getGameid();
			// 获取子渠道信息
			ChannelManageVo sonChl = channelManageService.selectChildChlByCode(wanGameChannelInfoDO.getChannel());
			// 获取主渠道信息
			ChannelManageVo parentChl = channelManageService.getVOByPK(sonChl.getPid());
			Integer platform = parentChl.getPlatform();
			Integer rtype = sonChl.getRtype();
			Integer isup = sonChl.getIsup();
			Integer paytype = sonChl.getPaytype();
			WanGameChannelPlatformDO wanGameChannelPlatformDO = gameChannelPlatformService.getByPlatformAndGameid(platform, gameid, null);
			if (wanGameChannelPlatformDO != null) {
				Long appid = wanGameChannelPlatformDO.getAppid();
				String appname = wanGameChannelPlatformDO.getAppname();
				wanGameChannelInfoDO.setAppid(appid == null ? "" : String.valueOf(appid));
				wanGameChannelInfoDO.setAppname(appname);
			}
			wanGameChannelInfoDO.setPlatform(platform);
			wanGameChannelInfoDO.setRtype(rtype);
			wanGameChannelInfoDO.setIsup(isup);
			wanGameChannelInfoDO.setPaytype(paytype);
			wanGameChannelInfoDO.setCreatetime(DateUtils.getCurrentDate());
			wanGameChannelInfoDOMapper.insertSelective(wanGameChannelInfoDO);
		} else {
			wanGameChannelInfoDO.setSpreadType(gameChannelInfoDB.getSpreadType());
			wanGameChannelInfoDO.setSettleType(gameChannelInfoDB.getSettleType());
			wanGameChannelInfoDO.setId(gameChannelInfoDB.getId());
			wanGameChannelInfoDOMapper.updateByPrimaryKeySelective(wanGameChannelInfoDO);
		}
	}
}
