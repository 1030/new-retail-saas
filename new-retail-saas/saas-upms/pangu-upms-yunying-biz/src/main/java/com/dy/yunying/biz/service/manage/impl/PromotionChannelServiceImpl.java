package com.dy.yunying.biz.service.manage.impl;

import com.dy.yunying.api.entity.PromotionChannelDO;
import com.dy.yunying.biz.dao.manage.ext.PromotionChannelDOMapperExt;
import com.dy.yunying.biz.service.manage.PromotionChannelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: hjl
 * @Date: 2020-9-1 14:30:18
 */
@Transactional
@Slf4j
@Service
public class PromotionChannelServiceImpl implements PromotionChannelService {

	@Autowired
	private PromotionChannelDOMapperExt mapper;

	@Override
	public PromotionChannelDO updateByPKSelective(PromotionChannelDO promotionChannelDO) {
		mapper.updateByPrimaryKeySelective(promotionChannelDO);
		return promotionChannelDO;
	}
}
