package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.dto.ChannelManageDto;
import com.dy.yunying.api.entity.ChannelManageDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ChannelManageDOMapper extends BaseMapper<ChannelManageDO> {
	int deleteByPrimaryKey(Integer id);

	int insert(ChannelManageDto record);

	int insertSelective(ChannelManageDO record);

	ChannelManageDO selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ChannelManageDO record);

	int updateByPrimaryKey(ChannelManageDO record);

	int insertchl(ChannelManageDto channel);
}