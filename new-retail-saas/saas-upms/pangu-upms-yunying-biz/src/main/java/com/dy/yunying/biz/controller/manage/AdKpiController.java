package com.dy.yunying.biz.controller.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.AdKpi;
import com.dy.yunying.api.entity.AdKpiDO;
import com.dy.yunying.api.req.AdKpiReq;
import com.dy.yunying.api.vo.AdKpiVo;
import com.dy.yunying.biz.service.ads.AdKpiService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * @ClassName AdKpiController
 * @Description todo
 * @Author nieml
 * @Time 2021/7/8 16:53
 * @Version 1.0
 **/
@RestController
@Slf4j
@RequestMapping("/kpi")
@Api(value = "kpi", tags = "kpi管理模块")
public class AdKpiController {

	@Autowired
	private AdKpiService adKpiService;

	@GetMapping(value = "/data")
	public R queryAdKpiDataPage(AdKpiReq req){
		IPage<AdKpiVo> res = adKpiService.queryAdKpiDataPage(req);
		return R.ok(res);
	}

	@SysLog("KPI完成率")
	@GetMapping(value = "/page")
	public R queryAdKpiPage(AdKpiReq req){
		try {
			IPage<AdKpiDO> page = adKpiService.queryAdKpiPage(req);
			return R.ok(page);
		} catch (Exception e) {
			log.error("queryAdKpiPage:{}",e);
			return R.failed("kpi列表查询失败");
		}
	}

	@SysLog("添加KPI")
	@PostMapping(value = "/save")
	public R saveAdKpi(@RequestBody AdKpi adKpi){
		//参数校验
		R checkRes = checkParams(adKpi);
		int code = checkRes.getCode();
		if (code == 0){
			return adKpiService.saveAdKpi(adKpi);
		}
		return R.failed(checkRes.getMsg());
	}

	@SysLog("修改KPI")
	@PostMapping(value = "/edit")
	public R editAdKpi(@RequestBody AdKpi adKpi){
		//参数校验
		R checkRes = checkParams(adKpi);
		int code = checkRes.getCode();
		if (code == 0){
			return adKpiService.editAdKpi(adKpi);
		}
		return R.failed(checkRes.getMsg());
	}

	@SysLog("删除KPI")
	@PostMapping(value = "/del")
	public R delAdKpi(Integer id){
		//参数校验
		if (id == null || id <1){
			return R.failed("请选择Kpi");
		}
		return adKpiService.delAdKpi(id);
	}

	public R checkParams(AdKpi adKpi){
		try {
			Integer hierarchyId = adKpi.getHierarchyId();
			Integer hierarchy = adKpi.getHierarchy();
			Integer os = adKpi.getOs();
			Integer pgid = adKpi.getPgid();
			Integer yearMonth = adKpi.getYearMonth();
			Long targetLevel = adKpi.getTargetLevel();
			Long targetNewuserMonthWater = adKpi.getTargetNewuserMonthWater();
			Long targetOlduserMonthWater = adKpi.getTargetOlduserMonthWater();
			BigDecimal targetRoi = adKpi.getTargetRoi();
			BigDecimal targetRoi30 = adKpi.getTargetRoi30();
			BigDecimal bigDecimal0 = new BigDecimal("0");
			if (hierarchy == null || hierarchy >3 || hierarchy < 1){
				throw new Exception("层级不能为空");
			}
			if (hierarchyId == null  ){
				throw new Exception("层级id不能为空");
			}
			if (os == null ){
				throw new Exception("系统不能为空");
			}
			if (pgid == null ){
				throw new Exception("父游戏id不能为空");
			}
			if (yearMonth == null ){
				throw new Exception("年月不能为空");
			}
			if (targetLevel == null || targetLevel <=0){
				throw new Exception("目标量级不能为空且大于0");
			}
			if (targetNewuserMonthWater == null || targetNewuserMonthWater <=0){
				throw new Exception("目标新用户月流水不能为空且大于0");
			}
			if (targetOlduserMonthWater == null || targetOlduserMonthWater <=0){
				throw new Exception("目标老用户月流水不能为空且大于0");
			}
			if (targetRoi == null || targetRoi.compareTo(bigDecimal0) < 1){
				throw new Exception("目标ROI不能为空且大于0");
			}
			if (targetRoi30 == null || targetRoi30.compareTo(bigDecimal0) < 1){
				throw new Exception("目标ROI30不能为空且大于0");
			}
		}catch (Exception e){
			log.error("checkParams:[{}]",e);
			return R.failed(e.getMessage());
		}
		return R.ok();
	}

}


