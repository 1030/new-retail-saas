package com.dy.yunying.biz.controller.currency;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.AdKpiDO;
import com.dy.yunying.api.req.currency.UserCurrencyReq;
import com.dy.yunying.biz.service.currency.UserCurrencyService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 用户平台币（游豆）
 *
 * @author hejiale
 * @date 2022-3-23 14:25:29
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/currency")
@Api(value = "currency", tags = "用户游豆")
@Slf4j
public class UserCurrencyController {

	private final UserCurrencyService userCurrencyService;

	@SysLog("用户游豆列表")
	@RequestMapping("/page")
//	@PostMapping("/page")
//	@PreAuthorize("@pms.hasPermission('currency_page')")
//	@Inner(value = false)
	public R page(@Valid @RequestBody UserCurrencyReq req){
		try {
			IPage<AdKpiDO> page = userCurrencyService.page(req);
			return R.ok(page);
		} catch (Exception e) {
			log.error("用户游豆-列表:{}",e);
			return R.failed("用户游豆列表查询失败");
		}
	}

}
