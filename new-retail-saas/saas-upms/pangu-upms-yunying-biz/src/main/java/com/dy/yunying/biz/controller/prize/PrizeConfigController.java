package com.dy.yunying.biz.controller.prize;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dy.yunying.api.entity.prize.PrizeConfig;
import com.dy.yunying.api.entity.prize.PrizeGiftBag;
import com.dy.yunying.api.enums.GiftTypeEnum;
import com.dy.yunying.api.req.prize.PrizeConfigReq;
import com.dy.yunying.api.resp.prize.PrizeConfigRes;
import com.dy.yunying.biz.service.prize.PrizeConfigService;
import com.dy.yunying.biz.service.prize.PrizeGiftBagService;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 奖励配置表
 *
 * @author chenxiang
 * @version 2022-04-25 16:25:44 table: prize_config
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/prizeConfig")
public class PrizeConfigController extends PrizeBaseController {

	private final PrizeConfigService prizeConfigService;

	private final PrizeGiftBagService prizeGiftBagService;
	/**
	 * 奖励配置列表
	 * @param record
	 * @return
	 */
	@RequestMapping("/list")
	public R list(@RequestBody PrizeConfigReq record) {
		return prizeConfigService.getAllPrizeConfig(record);
	}

	/**
	 * 创建奖励配置接口
	 * @param configReq
	 * @return
	 */
	@RequestMapping("/create")
	@SysLog("创建奖励配置")
	public R create(PrizeConfigReq configReq) {
		//
		R result = checkPrizeConfigParam(configReq);
		if (CommonConstants.FAIL.equals(result.getCode())) {
			return result;
		}
		configReq.setId(null);
		R<PrizeConfigRes> prizeConfigResR = prizeConfigService.savePrizeConfig(configReq);
		if (CommonConstants.FAIL.equals(prizeConfigResR.getCode())) {
			return prizeConfigResR;
		}
		return R.ok(prizeConfigResR.getData(),prizeConfigResR.getMsg());
	}



	/**
	 * TODO 测试用的
	 * 创建奖励配置接口
	 * @param configReq
	 * @return
	 */
	@RequestMapping("/createFile")
	@SysLog("创建奖励配置")
	public R createFile(PrizeConfigReq configReq) {
		//
		R result = checkPrizeConfigParam(configReq);
		if (CommonConstants.FAIL.equals(result.getCode())) {
			return result;
		}
		configReq.setId(null);
		R<PrizeConfigRes> prizeConfigResR = prizeConfigService.savePrizeConfig(configReq);
		if (CommonConstants.FAIL.equals(prizeConfigResR.getCode())) {
			return prizeConfigResR;
		}
		return R.ok(prizeConfigResR.getData());
	}

	/**
	 * 编辑奖励配置接口
	 * @param configReq
	 * @return
	 */
	@RequestMapping("/edit")
	@SysLog("编辑奖励配置")
	public R edit(PrizeConfigReq configReq) {
		if (StringUtils.isBlank(configReq.getId())) {
			return R.failed("未获取到唯一主键ID");
		}
		R result = checkPrizeConfigParam(configReq);
		if (CommonConstants.FAIL.equals(result.getCode())) {
			return result;
		}
		R<PrizeConfigRes> prizeConfigResR = prizeConfigService.savePrizeConfig(configReq);
		if (CommonConstants.FAIL.equals(prizeConfigResR.getCode())) {
			return prizeConfigResR;
		}
		return R.ok(prizeConfigResR.getData(),prizeConfigResR.getMsg());
	}

	/**
	 * 查看奖励配置接口
	 * @return
	 */
	@RequestMapping("/view")
	public R view(@RequestBody PrizeConfigReq configReq) {
		if (StringUtils.isBlank(configReq.getId())) {
			return R.failed("未获取到唯一主键ID");
		}
		PrizeConfig prizeConfig = prizeConfigService.getById(Long.valueOf(configReq.getId()));
		if (prizeConfig == null){
			R.failed("奖励配置不存在");
		}
		PrizeConfigRes prizeConfigRes = new PrizeConfigRes();
		BeanUtils.copyProperties(prizeConfig, prizeConfigRes);
		GiftTypeEnum giftTypeEnum = GiftTypeEnum.getGiftTypeEnum(prizeConfig.getGiftType());
		if (giftTypeEnum == GiftTypeEnum.UNIQUE){
			List<PrizeGiftBag> list = prizeGiftBagService.list(Wrappers.<PrizeGiftBag>lambdaQuery().eq(PrizeGiftBag::getSourceId, prizeConfig.getId()));
			if (CollectionUtils.isNotEmpty(list)){
				List<String> collect = list.stream().map(new Function<PrizeGiftBag, String>() {
					@Override
					public String apply(PrizeGiftBag prizeGiftBag) {
						return prizeGiftBag.getGiftCode();
					}
				}).collect(Collectors.toList());
				prizeConfigRes.setGiftContent(String.join(",",collect));
			}
		}
		return R.ok(prizeConfigRes);
	}

	/**
	 * 删除奖励配置
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteById")
	@SysLog("删除奖励配置")
	public R delete(@RequestParam("id") Long id) {
		return prizeConfigService.doRemoveById(id);
	}

}
