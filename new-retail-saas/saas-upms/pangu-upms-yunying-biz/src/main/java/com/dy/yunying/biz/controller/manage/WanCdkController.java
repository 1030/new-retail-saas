/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.controller.manage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.WanCdk;
import com.dy.yunying.api.req.hongbao.GenerateCdkReq;
import com.dy.yunying.api.req.hongbao.GrantCdkReq;
import com.dy.yunying.api.req.hongbao.ImportCdkReq;
import com.dy.yunying.api.req.hongbao.SelectCdkPageReq;
import com.dy.yunying.api.resp.hongbao.AdminUserVo;
import com.dy.yunying.api.resp.hongbao.CdkVo;
import com.dy.yunying.biz.service.manage.WanCdkService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * CDK信息表
 *
 * @author yuwenfeng
 * @date 2021-11-16 09:57:41
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/cdk")
@Api(value = "cdk", tags = "CDK信息表管理")
@Slf4j
public class WanCdkController {

	private final WanCdkService wanCdkService;

	@ApiOperation(value = "生成者下拉列表", notes = "生成者下拉列表")
	@GetMapping("/selectUserList")
	@PreAuthorize("@pms.hasPermission('yuying_wancdk_get')")
	public R<List<AdminUserVo>> selectUserList() {
		try {
			return R.ok(wanCdkService.selectUserList());
		} catch (Exception e) {
			log.error("代金券管理-生成者下拉列表-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/selectCdkPage")
	@PreAuthorize("@pms.hasPermission('yuying_wancdk_get')")
	public R<Page<CdkVo>> selectCdkPage(Page<WanCdk> page, @Valid SelectCdkPageReq req) {
		try {
			QueryWrapper<WanCdk> query = new QueryWrapper<WanCdk>();
			if (StringUtils.isNotBlank(req.getCdkCode())) {
				query.eq("wcdk.cdk_code", req.getCdkCode());
			}
			if (null != req.getCreateUserId() && req.getCreateUserId() > 0) {
				query.eq("wcdk.create_user_id", req.getCreateUserId());
			}
			if (null != req.getExpiryType() && req.getExpiryType() > 0) {
				query.eq("wcdk.expiry_type", req.getExpiryType());
			}
			if (null != req.getPgid() && req.getPgid() > 0) {
				query.eq("wcdk.pgid", req.getPgid());
			}
			if (null != req.getGameid() && req.getGameid() > 0) {
				query.eq("wcdk.gameid", req.getGameid());
			}
			if (null != req.getMoney() && req.getMoney().doubleValue() > 0) {
				query.eq("wcdk.money", req.getMoney());
			}
			if (null != req.getSourceType() && req.getSourceType() > 0) {
				query.eq("wcdk.source_type", req.getSourceType());
			}
			if (null != req.getSourceId() && req.getSourceId() > 0) {
				query.eq("wcdk.source_id", req.getSourceId());
			}
			if (null != req.getUseStatus() && req.getUseStatus() > 0) {
				query.eq("wcdk.use_status", req.getUseStatus());
			}
			if (StringUtils.isNotBlank(req.getBelongsName())) {
				query.eq("wuser.username", req.getBelongsName());
			}
			if (StringUtils.isNotBlank(req.getStartTime())) {
				query.ge("DATE_FORMAT(wcdk.create_time,'%Y-%m-%d')", req.getStartTime());
			}
			if (StringUtils.isNotBlank(req.getFinishTime())) {
				query.le("DATE_FORMAT(wcdk.create_time,'%Y-%m-%d')", req.getFinishTime());
			}
			query.eq("1", 1);
			query.orderByDesc("wcdk.create_time");
			return R.ok(wanCdkService.selectCdkPage(page, query));
		} catch (Exception e) {
			log.error("代金券管理-分页查询-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "生成代金券", notes = "生成代金券")
	@SysLog("生成代金券")
	@PostMapping("/generateCdk")
	@PreAuthorize("@pms.hasPermission('yuying_wancdk_generate')")
	public R generateCdk(@Valid @RequestBody GenerateCdkReq req) {
		try {
			return wanCdkService.generateCdk(req);
		} catch (Exception e) {
			log.error("代金券管理-生成代金券-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "发放代金券", notes = "发放代金券")
	@SysLog("发放代金券")
	@PostMapping("/grantCdk")
	@PreAuthorize("@pms.hasPermission('yuying_wancdk_grant')")
	public R grantCdk(@Valid @RequestBody GrantCdkReq req) {
		try {
			return wanCdkService.grantCdk(req);
		} catch (Exception e) {
			log.error("代金券管理-发放代金券-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	/**
	 * 导入代金券
	 *
	 * @param req
	 * @return
	 */
	@SysLog("导入代金券")
	@PostMapping("/importCdk")
	@PreAuthorize("@pms.hasPermission('yunying_wancdk_import')")
	public R<Void> importCdk(ImportCdkReq req) {
		try {
			wanCdkService.importCdk(req);
			return R.ok();
		} catch (Exception e) {
			log.error("代金券管理-导入代金券-接口", e);
			return R.failed(e.getMessage());
		}
	}

}
