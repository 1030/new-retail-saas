package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbActivityCenterArea;
import org.apache.ibatis.annotations.Mapper;

/**
 * 活动中心与区服关系表
 * @author  chenxiang
 * @version  2022-06-18 11:46:48
 * table: hb_activity_center_area
 */
@Mapper
public interface HbActivityCenterAreaMapper extends BaseMapper<HbActivityCenterArea> {
}


