package com.dy.yunying.biz.service.currency.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.currency.UserCurrencyTmp;
import com.dy.yunying.biz.dao.manage.currency.UserCurrencyTmpMapper;
import com.dy.yunying.biz.service.currency.UserCurrencyTmpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
* @author hejiale
* @description 针对表【user_currency_tmp(账号临时平台币信息)】的数据库操作Service实现
* @createDate 2022-03-25 09:56:35
*/
@Service
@RequiredArgsConstructor
@Slf4j
public class UserCurrencyTmpServiceImpl extends ServiceImpl<UserCurrencyTmpMapper, UserCurrencyTmp>
    implements UserCurrencyTmpService {

}




