package com.dy.yunying.biz.service.znfx;

import com.dy.yunying.api.req.znfx.AdPlanAnalyseReq;
import com.dy.yunying.api.req.znfx.AdPlanOverviewDto;
import com.dy.yunying.api.resp.znfx.AdPlanDataExportRes;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface AdPlanAnalyseService {
	/**
	 * 计划属性分析图表
	 * @param record
	 * @return
	 */
	R chartData(AdPlanAnalyseReq record);
	/**
	 * 计划分析计划汇总
	 * @param record
	 * @return
	 */
	R adPlanCollect(AdPlanAnalyseReq record);
	/**
	 * 导出列表
	 * @param req
	 * @return
	 */
	List<AdPlanDataExportRes> exportDataList(AdPlanOverviewDto req);

}
