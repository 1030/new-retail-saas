package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.GameVersionDO;

public interface GameVersionDOMapper {
	int deleteByPrimaryKey(Long versionId);

	int insert(GameVersionDO record);

	int insertSelective(GameVersionDO record);

	GameVersionDO selectByPrimaryKey(Long versionId);

	int updateByPrimaryKeySelective(GameVersionDO record);

	int updateByPrimaryKey(GameVersionDO record);
}