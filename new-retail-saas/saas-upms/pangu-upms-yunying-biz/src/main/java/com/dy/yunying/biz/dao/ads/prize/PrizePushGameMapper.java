package com.dy.yunying.biz.dao.ads.prize;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.prize.PrizePushGame;
import org.apache.ibatis.annotations.Mapper;

/**
 * 奖励与游戏关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:59
 * table: prize_push_game
 */
@Mapper
public interface PrizePushGameMapper extends BaseMapper<PrizePushGame> {
}


