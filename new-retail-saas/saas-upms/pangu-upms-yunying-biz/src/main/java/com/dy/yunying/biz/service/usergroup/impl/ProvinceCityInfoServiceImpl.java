package com.dy.yunying.biz.service.usergroup.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.usergroup.Device;
import com.dy.yunying.api.entity.usergroup.ProvinceCityInfo;
import com.dy.yunying.biz.dao.ads.usergroup.DeviceMapper;
import com.dy.yunying.biz.dao.ads.usergroup.ProvinceCityInfoMapper;
import com.dy.yunying.biz.service.usergroup.DeviceService;
import com.dy.yunying.biz.service.usergroup.ProvinceCityInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service("provinceCityInfoService")
@RequiredArgsConstructor
public class ProvinceCityInfoServiceImpl extends ServiceImpl<ProvinceCityInfoMapper, ProvinceCityInfo> implements ProvinceCityInfoService {
}
