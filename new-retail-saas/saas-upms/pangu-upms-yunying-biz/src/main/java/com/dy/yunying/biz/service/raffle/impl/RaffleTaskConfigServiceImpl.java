package com.dy.yunying.biz.service.raffle.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.raffle.RaffleTaskConfig;
import com.dy.yunying.api.req.raffle.RaffleTaskConfigReq;
import com.dy.yunying.biz.dao.ads.raffle.RaffleTaskConfigMapper;
import com.dy.yunying.biz.service.raffle.RaffleTaskConfigService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 抽奖任务配置表
 *
 * @author chenxiang
 * @version 2022-11-08 15:59:48
 * table: raffle_task_config
 */
@Log4j2
@Service("raffleTaskConfigService")
@RequiredArgsConstructor
public class RaffleTaskConfigServiceImpl extends ServiceImpl<RaffleTaskConfigMapper, RaffleTaskConfig> implements RaffleTaskConfigService {

	/**
	 * 列表
	 * @param record
	 * @return
	 */
	@Override
	public R getList(RaffleTaskConfigReq record){
		return R.ok(this.list(Wrappers.<RaffleTaskConfig>lambdaQuery()
				.eq(RaffleTaskConfig::getActivityId, record.getActivityId())
				.eq(RaffleTaskConfig::getDeleted, 0)
				.orderByAsc(RaffleTaskConfig::getId)));
	}
	/**
	 * 新增编辑
	 * @param record
	 * @return
	 */
	@Override
	public R addOrEdit(RaffleTaskConfig record){
		return this.saveOrUpdate(record) ? R.ok() : R.failed();
	}
	/**
	 * 删除
	 * @param record
	 * @return
	 */
	@Override
	public R del(RaffleTaskConfigReq record){
		RaffleTaskConfig raffleTaskConfig = new RaffleTaskConfig();
		raffleTaskConfig.setId(Long.valueOf(record.getId()));
		raffleTaskConfig.setDeleted(1);
		return this.updateById(raffleTaskConfig) ? R.ok() : R.failed();
	}


	@Override
	public void saveBatchList(List<RaffleTaskConfig> list){
		this.saveBatch(list,1000);
	}
}


