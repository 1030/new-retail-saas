package com.dy.yunying.biz.dao.manage.ext;

import com.dy.yunying.api.entity.WanGameImgsDO;
import com.dy.yunying.api.req.WanGameImgsReq;
import com.dy.yunying.biz.dao.manage.WanGameImgsDOMapper;

import java.util.List;

public interface WanGameImgsDOMapperExt extends WanGameImgsDOMapper {
	/**
	 * 获取游戏图片列表
	 *
	 * @param entity
	 * @return
	 */
	public List<WanGameImgsDO> selectWanGameImgList(WanGameImgsReq entity);

	/**
	 * 批量新增图片
	 *
	 * @author cx
	 * @date 2020年7月22日09:58:01
	 */
	public boolean insertWanGameImgsList(List<WanGameImgsReq> list);

	/**
	 * 按游戏id删除游戏图片
	 *
	 * @author cx
	 * @date 2020年7月22日09:58:01
	 */
	public boolean deleteWanGameImgs(WanGameImgsReq entity);

}