package com.dy.yunying.biz.service.prize.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.prize.PrizePushChannel;
import com.dy.yunying.biz.dao.ads.prize.PrizePushChannelMapper;
import com.dy.yunying.biz.service.prize.PrizePushChannelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 奖励与渠道关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:48
 * table: prize_push_channel
 */
@Log4j2
@Service("prizePushChannelService")
@RequiredArgsConstructor
public class PrizePushChannelServiceImpl extends ServiceImpl<PrizePushChannelMapper, PrizePushChannel> implements PrizePushChannelService {
}


