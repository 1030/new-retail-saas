package com.dy.yunying.biz.dao.clickhouse3399;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.dto.AdKanbanDto;
import com.dy.yunying.api.vo.AdKanbanOverviewVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdKanbanMapper.java
 * @createTime 2021年06月16日 10:59:00
 */
@Mapper
public interface AdKanbanMapper extends BaseMapper<AdKanbanOverviewVo> {

	/**
	 * 分页查询广告报表
	 * @param page
	 * @param req
	 * @return
	 */
	@SqlParser(filter=true)
	IPage<AdKanbanOverviewVo> selectAdKanbanSourceTable(Page page, @Param("param") AdKanbanDto req);
	/**
	 * 广告报表
	 * @param req
	 * @return
	 */
	@SqlParser(filter=true)
	List<AdKanbanOverviewVo> selectAdKanbanSourceTable(@Param("param") AdKanbanDto req);

}
