package com.dy.yunying.biz.service.ads;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.dto.MonitorRuleManagerDto;
import com.dy.yunying.api.req.MonitorRuleManagerReq;
import com.dy.yunying.api.entity.MonitorRuleManager;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @Description: monitor_rule_manager
 * @Author: hma
 * @Date:   2023-03-17
 * @Version: V1.0
 */
public interface MonitorRuleManagerService extends IService<MonitorRuleManager> {

	/**
	 *查询监控规则列表
	 * @param managerDto
	 * @return
	 */
	R<IPage<MonitorRuleManager>> queryMonitorRules(MonitorRuleManagerReq managerDto);

	/**
	 * 查询监控规则详情
	 * @param id
	 * @return
	 */

	 R<MonitorRuleManager> getMonitorRuleManager(Integer id);


	/**
	 * 逻辑删除监控规则
	 * @param id
	 * @return
	 */
	 R deleteMonitorRule(Integer id);

	/**
	 * 修改监控规则
	 * @param managerDto
	 * @return
	 */
	 R updateMonitorRule(MonitorRuleManagerDto managerDto);
	/**
	 * 开启/关闭规则管理
	 * @param id
	 * @param monitorStatus
	 * @return
	 */
	  R updateMonitorRuleStatus(Integer id, Integer monitorStatus);


	/**
	 * 保存监控规则
	 * @param managerDto
	 * @return
	 */
	 R saveMonitorRule(MonitorRuleManagerDto managerDto);
}
