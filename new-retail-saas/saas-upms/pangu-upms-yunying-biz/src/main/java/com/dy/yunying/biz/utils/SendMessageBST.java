package com.dy.yunying.biz.utils;

import com.sjda.framework.common.utils.HttpUtil;
import com.sjda.framework.common.utils.MD5;
import lombok.extern.log4j.Log4j2;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


@Log4j2
public class SendMessageBST {

	public static final String BST_UID = "175359";
	public static final String BST_PASSWORD = "7ujm,ki8@Wytx2022";

	public static final String BST_API = "http://mksms.8315.cn:9891";
	/**
	 * 短信发送
	 */
	public static final String BST_SENDSMS = "/cmppweb/sendsms";
	/**
	 * 余量查询
	 */
	public static final String BST_BALANCE = "/cmppweb/balance";


	public static Boolean sendMesBst(String mobile,String message) {
		try {
			Map<String, String> param = new HashMap<>();
			param.put("uid", BST_UID);
			param.put("pwd", MD5.sign(BST_PASSWORD, "", "utf-8"));
			param.put("mobile", mobile);
			param.put("srcphone", "10690300");
			param.put("msg", URLEncoder.encode(String.format(message), "UTF-8"));
			String url = BST_API + BST_SENDSMS;
			String result = HttpUtil.httpPost(url, param);
			log.info("-----------------------------bstSendSms, result:{}", result);
			return org.apache.commons.lang3.StringUtils.startsWith(result, "0,");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return false;
		}
	}
}
