package com.dy.yunying.biz.service.datacenter.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.dy.yunying.api.constant.OsEnum;
import com.dy.yunying.api.datacenter.dto.AdKanbanDto;
import com.dy.yunying.api.datacenter.vo.AdKanbanOverviewVo;
import com.dy.yunying.api.datacenter.vo.AdRecoveryVo;
import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.entity.WanGameDO;
import com.dy.yunying.api.req.ChannelManageReq;
import com.dy.yunying.api.vo.ChannelManageVo;
import com.dy.yunying.biz.dao.datacenter.impl.AdKanbanDao;
import com.dy.yunying.biz.service.datacenter.AdKanbanService;
import com.dy.yunying.biz.service.manage.AdRoleUserService;
import com.dy.yunying.biz.service.manage.GameService;
import com.dy.yunying.biz.service.manage.ParentGameService;
import com.pig4cloud.pig.api.feign.RemoteAccountService;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ：lile
 * @date ：2021/12/1 10:28
 * @description：
 * @modified By：
 */
@Log4j2
@Service(value = "dcAdKanbanServiceImpl")
public class AdKanbanServiceImpl implements AdKanbanService {

	@Autowired
	private AdKanbanDao adKanbanDao;

	@Autowired
	private AdRoleUserService adRoleUserService;

	@Autowired
	private RemoteAccountService remoteAccountService;

	@Autowired
	private ParentGameService parentGameService;

	@Autowired
	private GameService gameService;


	@Override
	public R selectKanbanStatitic(AdKanbanDto req) {

		// 当前用户
		final PigUser user = SecurityUtils.getUser();
		List<Integer> ownerRoleUserIds = adRoleUserService.getOwnerRoleUserIds();
		// 当前账号及管理的账号
		List<Integer> roleUserIdList = new ArrayList<>();
		req.setRoleUserIdList(roleUserIdList);
		roleUserIdList.add(user.getId());
		roleUserIdList.addAll(ownerRoleUserIds);

		// 授权的广告账户
		List<String> roleAdAccountList = new ArrayList<>();
		req.setRoleAdAccountList(roleAdAccountList);
		final List<String> accountList2 = remoteAccountService.getAccountList2(roleUserIdList, SecurityConstants.FROM_IN);

		if (ObjectUtils.isNotEmpty(accountList2)) {
			roleAdAccountList.addAll(accountList2);
		}
		// 判断是否管理员
		if (SecurityUtils.getRoles().contains(1)) {
			req.setIsSys(1);
		}


		List<AdKanbanOverviewVo> resultData = adKanbanDao.selectAdKanbanSourceTable(req);

		deal(resultData);
		return R.ok(resultData);
	}

	@Override
	public List<AdKanbanOverviewVo> excelKanbanStatitic(AdKanbanDto req) {
		// 当前用户
		final PigUser user = SecurityUtils.getUser();
		List<Integer> ownerRoleUserIds = adRoleUserService.getOwnerRoleUserIds();
		// 当前账号及管理的账号
		List<Integer> roleUserIdList = new ArrayList<>();
		req.setRoleUserIdList(roleUserIdList);
		roleUserIdList.add(user.getId());
		roleUserIdList.addAll(ownerRoleUserIds);

		// 授权的广告账户
		List<String> roleAdAccountList = new ArrayList<>();
		req.setRoleAdAccountList(roleAdAccountList);
		final List<String> accountList2 = remoteAccountService.getAccountList2(roleUserIdList, SecurityConstants.FROM_IN);
		if (ObjectUtils.isNotEmpty(accountList2)) {
			roleAdAccountList.addAll(accountList2);
		}
		// 判断是否管理员
		if (SecurityUtils.getRoles().contains(1)) {
			req.setIsSys(1);
		}

		List<AdKanbanOverviewVo> adKanbanOverviewList = adKanbanDao.selectAdKanbanSourceTable(req);

		deal(adKanbanOverviewList);
		return adKanbanOverviewList;
	}

	@Override
	public R countDataTotal(AdKanbanDto req) {

		// 当前用户
		final PigUser user = SecurityUtils.getUser();
		List<Integer> ownerRoleUserIds = adRoleUserService.getOwnerRoleUserIds();
		// 当前账号及管理的账号
		List<Integer> roleUserIdList = new ArrayList<>();
		req.setRoleUserIdList(roleUserIdList);
		roleUserIdList.add(user.getId());
		roleUserIdList.addAll(ownerRoleUserIds);

		// 授权的广告账户
		List<String> roleAdAccountList = new ArrayList<>();
		req.setRoleAdAccountList(roleAdAccountList);
		final List<String> accountList2 = remoteAccountService.getAccountList2(roleUserIdList, SecurityConstants.FROM_IN);
		if (ObjectUtils.isNotEmpty(accountList2)) {
			roleAdAccountList.addAll(accountList2);
		}
		// 判断是否管理员
		if (SecurityUtils.getRoles().contains(1)) {
			req.setIsSys(1);
		}
		Long count = adKanbanDao.countDataTotal(req);
		return R.ok(count);
	}


	private void deal(List<AdKanbanOverviewVo> adRecoveryVos) {
		if (CollectionUtils.isNotEmpty(adRecoveryVos)) {
			//封装数据
			for (AdKanbanOverviewVo adKanbanOverviewVo : adRecoveryVos) {
				try {

					final Long pgid = adKanbanOverviewVo.getPgid();

					final Long gameid = adKanbanOverviewVo.getGid();

					final ParentGameDO parentGameDO = parentGameService.getByPK(pgid);
					String parentGameName = "-";
					if (parentGameDO != null) {
						parentGameName = parentGameDO.getGname();
					}
					adKanbanOverviewVo.setPgame(parentGameName);

					if (Objects.nonNull(adKanbanOverviewVo.getOs())) {
						adKanbanOverviewVo.setOsName(OsEnum.getName(adKanbanOverviewVo.getOs().intValue()));
					}


					final WanGameDO wanGameDO = gameService.selectVOByPK(gameid);
					String gameName = "-";
					if (wanGameDO != null) {
						gameName = wanGameDO.getGname();
					}
					adKanbanOverviewVo.setSgame(gameName);

				} catch (Exception e) {
					log.error("数据处理异常", e);
				}
			}
		}
	}

}
