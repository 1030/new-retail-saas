package com.dy.yunying.biz.controller.znfx;

import com.dy.yunying.api.entity.znfx.ChartVO;
import com.dy.yunying.api.req.znfx.AdPlanAnalyseReq;
import com.dy.yunying.api.req.znfx.ZnfxReq;
import com.dy.yunying.api.utils.CheckUtils;
import com.dy.yunying.biz.service.znfx.ZnfxService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @description:
 * @author: zhuxm
 * @time: 2023/3/21 14:03
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/znfx")
@Api(value = "znfx", tags = "智能分析")
@Slf4j
public class ZnfxController {

	private final ZnfxService paidUserService;

	@ApiOperation(value = "图形数据查询", notes = "图形数据查询")
	@PostMapping("/chart_data_search")
	public R<List<ChartVO>> chartDataSearch(@Valid @RequestBody ZnfxReq znfxReq) {
		try {
			R result = this.checkChartDataSearchParam(znfxReq);
			if (0 != result.getCode()){
				return result;
			}
			return R.ok(paidUserService.selectChartData(znfxReq));
		} catch (Exception e) {
			log.error("智能分析-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}


	@ApiOperation(value = "计划用户分析图形数据查询", notes = "计划用户分析图形数据查询")
	@PostMapping("/plan_user_search")
	public R<List<ChartVO>> planUserSearch(@Valid @RequestBody ZnfxReq znfxReq) {
		try {
			R result = checkPlanUserSearchParam(znfxReq);
			if (0 != result.getCode()){
				return result;
			}
			return R.ok(paidUserService.planUserSearch(znfxReq));
		} catch (Exception e) {
			log.error("智能分析-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}


	/**
	 * 校验付费用户分析入参
	 * @param record
	 * @return
	 */
	private R checkChartDataSearchParam(ZnfxReq record){
		final String totalPayCostMin = record.getTotalPayCostMin();
		final String totalPayCostMax = record.getTotalPayCostMax();
		if ((StringUtils.isNotBlank(totalPayCostMin) && !CheckUtils.checkMoney(totalPayCostMin))
				|| (StringUtils.isNotBlank(totalPayCostMax) && !CheckUtils.checkMoney(totalPayCostMax))){
			return R.failed("请输入正确的累计付费金额");
		}
		return R.ok();
	}

	/**
	 * 校验计划用户分析入参
	 * @param record
	 * @return
	 */
	private R checkPlanUserSearchParam(ZnfxReq record){
		final String firstRoiMin = record.getFirstRoiMin();
		final String firstRoiMax = record.getFirstRoiMax();
		final String threeRoiMin = record.getThreeRoiMin();
		final String threeRoiMax = record.getThreeRoiMax();
		final String sevenRoiMin = record.getSevenRoiMin();
		final String sevenRoiMax = record.getSevenRoiMax();
		final String fifteenRoiMin = record.getFifteenRoiMin();
		final String fifteenRoiMax = record.getFifteenRoiMax();
		final String thirtyRoiMin = record.getThirtyRoiMin();
		final String thirtyRoiMax = record.getThirtyRoiMax();
		final String regCostMin = record.getRegCostMin();
		final String regCostMax = record.getRegCostMax();
		final String firstPayCostMin = record.getFirstPayCostMin();
		final String firstPayCostMax = record.getFirstPayCostMax();
		final String totalPayCostMin = record.getTotalPayCostMin();
		final String totalPayCostMax = record.getTotalPayCostMax();

		if ((StringUtils.isNotBlank(firstRoiMin) && !CheckUtils.checkMoney(firstRoiMin))
				|| (StringUtils.isNotBlank(firstRoiMax) && !CheckUtils.checkMoney(firstRoiMax))){
			return R.failed("请输入正确的首日ROI");
		}
		if ((StringUtils.isNotBlank(threeRoiMin) && !CheckUtils.checkMoney(threeRoiMin))
				|| (StringUtils.isNotBlank(threeRoiMax) && !CheckUtils.checkMoney(threeRoiMax))){
			return R.failed("请输入正确的3日ROI");
		}
		if ((StringUtils.isNotBlank(sevenRoiMin) && !CheckUtils.checkMoney(sevenRoiMin))
				|| (StringUtils.isNotBlank(sevenRoiMax) && !CheckUtils.checkMoney(sevenRoiMax))){
			return R.failed("请输入正确的7日ROI");
		}
		if ((StringUtils.isNotBlank(fifteenRoiMin) && !CheckUtils.checkMoney(fifteenRoiMin))
				|| (StringUtils.isNotBlank(fifteenRoiMax) && !CheckUtils.checkMoney(fifteenRoiMax))){
			return R.failed("请输入正确的15日ROI");
		}
		if ((StringUtils.isNotBlank(thirtyRoiMin) && !CheckUtils.checkMoney(thirtyRoiMin))
				|| (StringUtils.isNotBlank(thirtyRoiMax) && !CheckUtils.checkMoney(thirtyRoiMax))){
			return R.failed("请输入正确的30日ROI");
		}
		if ((StringUtils.isNotBlank(regCostMin) && !CheckUtils.checkMoney(regCostMin))
				|| (StringUtils.isNotBlank(regCostMax) && !CheckUtils.checkMoney(regCostMax))){
			return R.failed("请输入正确的注册成本");
		}
		if ((StringUtils.isNotBlank(firstPayCostMin) && !CheckUtils.checkMoney(firstPayCostMin))
				|| (StringUtils.isNotBlank(firstPayCostMax) && !CheckUtils.checkMoney(firstPayCostMax))){
			return R.failed("请输入正确的首日付费成本");
		}
		if ((StringUtils.isNotBlank(totalPayCostMin) && !CheckUtils.checkMoney(totalPayCostMin))
				|| (StringUtils.isNotBlank(totalPayCostMax) && !CheckUtils.checkMoney(totalPayCostMax))){
			return R.failed("请输入正确的累计付费成本");
		}
		return R.ok();
	}

}
