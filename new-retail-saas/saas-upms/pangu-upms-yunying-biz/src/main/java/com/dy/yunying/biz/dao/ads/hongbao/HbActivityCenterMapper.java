package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.hongbao.HbActivityCenterDO;
import org.apache.ibatis.annotations.Param;

/**
 * @author MyPC
 * @description 针对表【hb_activity_center(活动中心配置表)】的数据库操作Mapper
 * @createDate 2022-01-12 19:26:58
 * @Entity generator.domain.HbActivityCenter
 */
public interface HbActivityCenterMapper extends BaseMapper<HbActivityCenterDO> {

	/**
	 * 查询活动列表
	 *
	 * @param page
	 * @param record
	 * @return
	 */
	Page<HbActivityCenterDO> selectActivityListBySelective(Page<Object> page, @Param("record") HbActivityCenterDO record);

}
