package com.dy.yunying.biz.service.cps.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.cps.CpsBuckleRate;
import com.dy.yunying.api.cps.CpsBuckleRateReq;
import com.dy.yunying.api.cps.vo.CpsBuckleRateVO;
import com.dy.yunying.biz.service.cps.CpsBuckleRateService;
import com.dy.yunying.biz.dao.ads.CpsBuckleRateMapper;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.core.mybatis.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Administrator
 */
@Service
public class CpsBuckleRateServiceImpl extends ServiceImpl<CpsBuckleRateMapper, CpsBuckleRate>
    implements CpsBuckleRateService{

	@Autowired
	private CpsBuckleRateMapper cpsBuckleRateMapper;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R add(CpsBuckleRateReq req) {
		CpsBuckleRate cpsBuckleRate = new CpsBuckleRate();
		cpsBuckleRate.setUserId(req.getUserId());
		cpsBuckleRate.setBuckleRate(new BigDecimal(req.getBuckleRate()));
		cpsBuckleRate.setStartTime(DateUtils.stringToDate(req.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS));
		cpsBuckleRate.setEndTime(DateUtils.stringToDate(req.getEndTime(), DateUtils.YYYY_MM_DD_HH_MM_SS));
		cpsBuckleRate.setCreateId(SecurityUtils.getUser().getId().longValue());
		cpsBuckleRate.setCreateTime(new Date());
		baseMapper.insert(cpsBuckleRate);
		return R.ok();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R edit(CpsBuckleRateReq req) {
		CpsBuckleRate cpsBuckleRate = new CpsBuckleRate();
		cpsBuckleRate.setId(req.getId());
		cpsBuckleRate.setUserId(req.getUserId());
		cpsBuckleRate.setBuckleRate(new BigDecimal(req.getBuckleRate()));
		cpsBuckleRate.setStartTime(DateUtils.stringToDate(req.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS));
		cpsBuckleRate.setEndTime(DateUtils.stringToDate(req.getEndTime(), DateUtils.YYYY_MM_DD_HH_MM_SS));
		cpsBuckleRate.setUpdateId(SecurityUtils.getUser().getId().longValue());
		cpsBuckleRate.setUpdateTime(new Date());
		baseMapper.updateById(cpsBuckleRate);
		return R.ok();
	}

	@Override
	public R<IPage<CpsBuckleRateVO>> getPage(CpsBuckleRateReq req) {
		if (Objects.isNull(req)){
			req = new CpsBuckleRateReq();
		}
		IPage<CpsBuckleRateVO> page = cpsBuckleRateMapper.getPage(req);
		return R.ok(page);
	}
}




