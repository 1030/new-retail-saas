package com.dy.yunying.biz.dao.ads.usergroup;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.usergroup.ProvinceCityInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProvinceCityInfoMapper extends BaseMapper<ProvinceCityInfo> {
}
