package com.dy.yunying.biz.datasource;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

//数据源1
@Configuration
//配置mybatis的接口类放的地方
@MapperScan(basePackages = "com.dy.yunying.biz.dao.ads", sqlSessionFactoryRef = "adsSqlSessionFactory")
public class DmAdsDataSourceConfig {

	@Autowired
	private MybatisPlusInterceptor mybatisPlusInterceptor;


	// 将这个对象放入Spring容器中
	@Bean(name = "adsDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.ads")
	public DataSource getDateSource1() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "adsSqlSessionFactory")
	// 表示这个数据源是默认数据源
	// @Qualifier表示查找Spring容器中名字为adsDataSource的对象
	public MybatisSqlSessionFactoryBean iProcessSqlSessionFactory(
			@Qualifier("adsDataSource") DataSource datasource) throws Exception {
		MybatisSqlSessionFactoryBean sqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(getDateSource1());
		MybatisConfiguration configuration = new MybatisConfiguration();
		//configuration.setMapUnderscoreToCamelCase(false);
		sqlSessionFactoryBean.setConfiguration(configuration);
		sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/mapper/ads/**/*.xml"));

		//设置 MyBatis-Plus 分页插件
		Interceptor[] plugins = {mybatisPlusInterceptor};
		sqlSessionFactoryBean.setPlugins(plugins);

		//全局配置
		GlobalConfig globalConfig  = new GlobalConfig();
		//配置填充器
		globalConfig.setMetaObjectHandler(new MetaObjectHandlerConfig());
		sqlSessionFactoryBean.setGlobalConfig(globalConfig);

		return sqlSessionFactoryBean;

	}

	@Bean("adsSqlSessionTemplate")
	// 表示这个数据源是默认数据源
	public SqlSessionTemplate test1sqlsessiontemplate(
			@Qualifier("adsSqlSessionFactory") SqlSessionFactory sessionfactory) {
		return new SqlSessionTemplate(sessionfactory);
	}

	@Primary
	@Bean(name = "adsTransactionManager")
	public PlatformTransactionManager txManager(@Qualifier("adsDataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}
}
