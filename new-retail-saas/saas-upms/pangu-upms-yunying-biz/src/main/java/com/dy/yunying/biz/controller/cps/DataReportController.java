package com.dy.yunying.biz.controller.cps;

import com.dy.yunying.api.cps.dto.DataReportDto;
import com.dy.yunying.api.cps.vo.DataReportVO;
import com.dy.yunying.biz.service.cps.DataReportService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

/**
 * cps数据报表(3399开发平台)
 * @author leisw
 * @date 2022/9/15 10:29
 */
@Slf4j
@RestController("dataReport")
@RequestMapping("/cps/dataReport")
@RequiredArgsConstructor
public class DataReportController {


	private final DataReportService dataReportService;


	/**
	 * cps数据报表数据总数
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/count")
	public R countDataTotal(@Valid @RequestBody DataReportDto req) {

		return dataReportService.count(req);
	}


	/**
	 * cps数据报表列表
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/list")
	public R<List<DataReportVO>> list(@Valid @RequestBody DataReportDto req) {
		Integer cycleType=req.getCycleType();
		if(null == cycleType){
			cycleType=5;
		}
		req.setCycleType(cycleType);
		R<List<DataReportVO>> page = dataReportService.page(req);
		if (CommonConstants.FAIL.equals(page.getCode())){
			return page;
		}

		return page;
	}

	/**
	 * cps数据报表汇总
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/collect")
	public R collect(@Valid @RequestBody DataReportDto req) {
		req.setCycleType(5);
		req.setQueryColumn("");
		if(5 == req.getCycleType()){
			req.setSize(null);
			req.setCurrent(null);
		}
		R<List<DataReportVO>> collect = dataReportService.collect(req);
		if (CommonConstants.FAIL.equals(collect.getCode())){
			return collect;
		}
		return collect;
	}


	/**
	 * cps数据报表数据导出
	 *
	 * @param req
	 * @return
	 */
	@ResponseExcel(name = "cps数据报表导出", sheet = "cps数据报表导出")
	@RequestMapping("/excelExport")
	public R export(@Valid  DataReportDto req, HttpServletResponse response, HttpServletRequest request) {
		List<DataReportVO> result = new ArrayList<>();
		String sheetName = "cps数据报表";
		req.setSize(9999999L);
		//默认导出数据按照日期排序，
		req.setKpiValue(null);
		CompletableFuture<List<DataReportVO>> pageFuture = CompletableFuture.supplyAsync(new Supplier<List<DataReportVO>>() {
			@Override
			public List<DataReportVO> get() {
				R page = dataReportService.page(req);
				if (CommonConstants.SUCCESS.equals(page.getCode())) {
					return (List<DataReportVO>) page.getData();
				}
				return null;
			}
		});
		DataReportDto collect  =  new DataReportDto();
		BeanUtils.copyProperties(req,collect);
		collect.setQueryColumn("");
		collect.setCycleType(5);
		CompletableFuture<List<DataReportVO>> totalFuture = CompletableFuture.supplyAsync(new Supplier<List<DataReportVO>>() {
			@Override
			public List<DataReportVO> get() {
				R page = dataReportService.page(collect);
				if (CommonConstants.SUCCESS.equals(page.getCode())) {
					return (List<DataReportVO>) page.getData();
				}
				return null;
			}
		});
		try {
			List<DataReportVO> list = pageFuture.get();
			List<DataReportVO> totalList = totalFuture.get();
			if (!CollectionUtils.isEmpty(list)){
				result.addAll(list);
			}
			if (!CollectionUtils.isEmpty(totalList)){
				result.addAll(totalList);
			}
			//
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(result);
			// 为百分比的值拼接百分号
			ExportUtils.exportExcelData(request, response, String.format("%s-%s.xlsx", sheetName, DateUtils.getCurrentTimeNoUnderline()), sheetName, req.getTitles(), req.getColumns(), resultListMap);
		} catch (Exception e) {
			log.error("cps数据报表数据导出异常, 异常原因：{}",e.toString());
			throw new BusinessException("导出异常");
		}

		return null;

	}

}
