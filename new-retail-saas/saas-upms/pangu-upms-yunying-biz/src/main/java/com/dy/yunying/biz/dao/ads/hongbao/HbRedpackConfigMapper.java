package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.dy.yunying.api.dto.hongbao.HbRedpackConfigDto;
import com.dy.yunying.api.entity.hongbao.HbRedpackConfig;
import com.dy.yunying.api.resp.hongbao.CashExportVo;
import com.dy.yunying.api.resp.hongbao.GiftVoData;
import com.dy.yunying.api.resp.hongbao.RedPackCountData;
import com.dy.yunying.api.resp.hongbao.RedPackExportVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 红包配置表
 *
 * @author chenxiang
 * @version 2021-10-25 14:19:00
 * table: hb_redpack_config
 */
@Mapper
public interface HbRedpackConfigMapper extends BaseMapper<HbRedpackConfig> {

	//单表基本CURD操作BaseMapper已经存在,请尽量不用重载父类已定义的方法

	Double sumTotalCost(Long activityId);

	List<RedPackCountData> selectMoneyCountList(@Param("activityId") Long activityId);

	HbRedpackConfigDto selectRedpackConfigById(@Param("id") String id);

	List<GiftVoData> selectGiftList(@Param("activityId") Long activityId, @Param("type") int type);

	/**
	 * 红包-导出未使用的礼包码
	 * @param activityId
	 * @return
	 */
	List<RedPackExportVo> exportNoUseGift4RedPack(@Param("activityId") Long activityId);

	/**
	 * 提现档次-导出未使用的礼包码
	 * @param activityId
	 * @return
	 */
	List<CashExportVo> exportNoUseGift4Cash(@Param("activityId") Long activityId);

	int insertBatch(List<HbRedpackConfig> list);

	/**
	 * 活动下线导出礼包码时将未领取完的礼包码总数量更新未已领取数量
	 * @param activityId
	 * @return
	 */
	int updateHasCounts(@Param("activityId") Long activityId);
}


