package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.WanGameAreaDO;

public interface WanGameAreaDOMapper {
	int deleteByPrimaryKey(Long id);

	int insert(WanGameAreaDO record);

	int insertSelective(WanGameAreaDO record);

	WanGameAreaDO selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(WanGameAreaDO record);

	int updateByPrimaryKey(WanGameAreaDO record);
}