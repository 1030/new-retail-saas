package com.dy.yunying.biz.service.raffle;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.raffle.RaffleActivity;
import com.dy.yunying.api.req.raffle.RaffleActivityEditReq;
import com.dy.yunying.api.req.raffle.RaffleActivityReq;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 抽奖活动配置表服务接口
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
 */
public interface RaffleActivityService extends IService<RaffleActivity> {


	/**
	 * 抽奖活动分页列表
	 * @param record
	 * @return
	 */
	R getPage(RaffleActivityReq record);
	/**
	 * 列表
	 * @param record
	 * @return
	 */
	R getList(RaffleActivityReq record);

	/**
	 * 抽奖活动新增-编辑
	 * @param record
	 * @return
	 */
	R createEdit(RaffleActivityEditReq record);

	/**
	 * 复制抽奖活动
	 * @param id
	 * @return
	 */
	R copy(Long id);
	/**
	 * 批量更新kv图
	 * @return
	 */
	R batchKvpic(RaffleActivityEditReq record);
	/**
	 * 更新状态
	 * @param record
	 * @return
	 */
	R activityStatus(RaffleActivityEditReq record);
}
