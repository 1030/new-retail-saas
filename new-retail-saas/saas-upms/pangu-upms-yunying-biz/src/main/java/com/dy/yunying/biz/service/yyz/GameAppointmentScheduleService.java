package com.dy.yunying.biz.service.yyz;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.GameAppointmentSchedule;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleReq;
import com.pig4cloud.pig.common.core.util.R;


/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_schedule
 */
public interface GameAppointmentScheduleService extends IService<GameAppointmentSchedule> {

	/**
	 * 预约列表 - 分页
	 *
	 * @param req
	 * @return
	 */
	R getPage(GameAppointmentScheduleReq req);


	R gameAddSchedule(GameAppointmentScheduleReq req);


	R gameEditSchedule(GameAppointmentScheduleReq req);


	R getLineList(GameAppointmentScheduleReq req);
}


