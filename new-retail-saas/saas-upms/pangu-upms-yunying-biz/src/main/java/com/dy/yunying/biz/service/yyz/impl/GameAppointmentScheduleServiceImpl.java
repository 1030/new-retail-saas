package com.dy.yunying.biz.service.yyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.*;
import com.dy.yunying.api.req.yyz.*;
import com.dy.yunying.api.resp.GameAppointmentScheduleUserColumn;
import com.dy.yunying.api.resp.yyz.GameAppointmentAllTotalRes;
import com.dy.yunying.api.resp.yyz.GameAppointmentScheduleOnlineRes;
import com.dy.yunying.api.resp.yyz.GameAppointmentScheduleRes;
import com.dy.yunying.biz.dao.manage.GameAppointmentMapper;
import com.dy.yunying.biz.dao.manage.GameAppointmentScheduleMapper;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleGearsRewardService;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleService;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleUserService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 游戏预约进度表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_Schedule
 */
@Log4j2
@Service("GameAppointmentScheduleService")
@RequiredArgsConstructor
public class GameAppointmentScheduleServiceImpl extends ServiceImpl<GameAppointmentScheduleMapper, GameAppointmentSchedule> implements GameAppointmentScheduleService {

	private final GameAppointmentMapper gameAppointmentMapper;

	private final GameAppointmentScheduleMapper gameAppointmentScheduleMapper;

	private final GameAppointmentScheduleUserService gameAppointmentScheduleUserService;

	private final GameAppointmentScheduleGearsRewardService scheduleGearsRewardService;

	private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	private static SimpleDateFormat formatss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public R getPage(GameAppointmentScheduleReq req) {
		List<GameAppointmentScheduleRes> list= new ArrayList<>();
		QueryWrapper<GameAppointmentSchedule> wrapper = new QueryWrapper<>();
		if (StringUtils.isNotBlank(req.getType())){
			wrapper.eq("type",req.getType());
		}
		List<GameAppointmentSchedule> gameAppointmentSchedulesList = this.list(wrapper);
		gameAppointmentSchedulesList.forEach(gameAppointmentSchedule -> {
			GameAppointmentScheduleRes gameAppointmentScheduleRes = new GameAppointmentScheduleRes();
			gameAppointmentScheduleRes.setId(gameAppointmentSchedule.getId());
			gameAppointmentScheduleRes.setType(gameAppointmentSchedule.getType());
			gameAppointmentScheduleRes.setGameName(gameAppointmentSchedule.getGameName());

			List<GameAppointmentScheduleGearsPage> gameAppointmentScheduleGears = gameAppointmentScheduleMapper.selectGameAppointmentScheduleList(gameAppointmentSchedule);
			gameAppointmentScheduleGears.forEach(scheduleGears ->{
				List<GameAppointmentScheduleGearsReward> scheduleGearsRewards = scheduleGearsRewardService.selectScheduleGearsReward(scheduleGears);
				scheduleGears.setScheduleGearsRewards(scheduleGearsRewards);
			});
			gameAppointmentScheduleRes.setGameAppointmentSchedule(gameAppointmentScheduleGears);
			List<GameAppointmentScheduleOnlineRes> gameAppointmentScheduleOnlineRes = getGameAppointmentNum(gameAppointmentSchedule);
			gameAppointmentScheduleRes.setOnLine(gameAppointmentScheduleOnlineRes);
			Integer allTotal = getAllTotal(gameAppointmentScheduleOnlineRes);
			gameAppointmentScheduleRes.setAllTotal(allTotal);
			list.add(gameAppointmentScheduleRes);
		});
		return R.ok(list);
	}


	@Override
	public R gameAddSchedule(GameAppointmentScheduleReq req) {
		GameAppointmentSchedule appointmentSchedule = this.getOne(Wrappers.<GameAppointmentSchedule>lambdaQuery().eq(GameAppointmentSchedule::getType, req.getType()));
		if (Objects.nonNull(appointmentSchedule)) {
			log.info("此游戏已存在,无法继续添加");
			return R.failed(6, "此游戏已存在,无法继续添加");
		}
		GameAppointmentSchedule gameAppointmentSchedule=new GameAppointmentSchedule();
		gameAppointmentSchedule.setType(req.getType());
		gameAppointmentSchedule.setGameName(gameAppointmentMapper.getGameName(req.getType()));
		gameAppointmentSchedule.setCreateTime(new Date());
		gameAppointmentSchedule.setUpdateTime(new Date());
		boolean flag = this.save(gameAppointmentSchedule);
		if(flag){
			return R.ok(1, "新增预约游戏成功");
		}
		return R.failed(6, "新增预约游戏失败");
	}

	@Override
	public R gameEditSchedule(GameAppointmentScheduleReq req) {
		GameAppointmentSchedule gameAppointmentSchedule=new GameAppointmentSchedule();
		gameAppointmentSchedule.setId(Long.valueOf(req.getId()));
		gameAppointmentSchedule.setUpdateTime(new Date());
		boolean flag = this.updateById(gameAppointmentSchedule);
		if(flag){
			return R.ok(1, "编辑预约游戏成功");
		}
		return R.failed(6, "编辑预约游戏失败");
	}

	@Override
	public R getLineList(GameAppointmentScheduleReq req) {
		List<GameAppointmentScheduleUserNum> scheduleUserNums = gameAppointmentScheduleMapper.selectGameAppointmentScheduleUserNumList(req);
		return R.ok(scheduleUserNums);
	}

	//获得在线人数
	private List<GameAppointmentScheduleOnlineRes> getGameAppointmentNum(GameAppointmentSchedule gameAppointmentSchedule) {
		List<GameAppointmentScheduleOnlineRes> gameAppointmentScheduleOnlineResList = new ArrayList<>();

		List<GameAppointmentScheduleUser> gameAppointmentScheduleUsers = gameAppointmentScheduleUserService.getOnlineList(gameAppointmentSchedule);
		gameAppointmentScheduleUsers.forEach(gameAppointmentScheduleUser -> {
			GameAppointmentScheduleOnlineRes gameAppointmentScheduleOnlineRes =new GameAppointmentScheduleOnlineRes();
			gameAppointmentScheduleOnlineRes.setOnlineName(gameAppointmentScheduleUser.getConventionName()+"预约人数");
			gameAppointmentScheduleOnlineRes.setOnlineNum(gameAppointmentScheduleUser.getNumber().intValue());
			gameAppointmentScheduleOnlineResList.add(gameAppointmentScheduleOnlineRes);
		});
		return gameAppointmentScheduleOnlineResList;
	}


	private Integer getAllTotal(List<GameAppointmentScheduleOnlineRes> gameAppointmentScheduleOnlineRes) {
		Integer allTotal = gameAppointmentScheduleOnlineRes.stream().mapToInt(gameAppointmentScheduleOnlineRes1 -> gameAppointmentScheduleOnlineRes1.getOnlineNum().intValue()).sum();
		return allTotal;
	}

}


