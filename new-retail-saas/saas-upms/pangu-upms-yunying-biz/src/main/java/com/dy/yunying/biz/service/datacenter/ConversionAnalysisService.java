package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.dto.ConversionAnalysisDto;
import com.dy.yunying.api.vo.ConversionAnalysisVo;

import java.util.List;

/**
 * @author chengang
 */
public interface ConversionAnalysisService {

	/**
	 * 转化分析报表总数
	 *
	 * @param analysisDto
	 * @return
	 */
	int count(ConversionAnalysisDto analysisDto);

	/**
	 * 转化分析报表分页
	 *
	 * @param analysisDto 查询条件
	 * @param isPage 是否分页
	 * @return
	 */
	List<ConversionAnalysisVo> page(ConversionAnalysisDto analysisDto, boolean isPage);


}
