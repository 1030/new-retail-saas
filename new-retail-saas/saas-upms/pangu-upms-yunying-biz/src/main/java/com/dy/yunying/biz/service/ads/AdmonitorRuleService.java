package com.dy.yunying.biz.service.ads;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.dto.AdmonitorRuleDto;
import com.dy.yunying.api.entity.AdmonitorRule;
import com.pig4cloud.pig.common.core.util.R;

public interface AdmonitorRuleService extends IService<AdmonitorRule> {
    R<IPage<AdmonitorRule>> queryAdmonitorRules(AdmonitorRuleDto admonitorRuleDto);

    R<AdmonitorRule> getAdmonitorRuleById(Integer id);

	R<Boolean> deleteAdmonitorRule(Integer id);

	R<Boolean> updateAdmonitorRuleStatus(Integer id, Integer monitorStatus);

	R<Boolean> saveAdmonitorRule(AdmonitorRule admonitorRule);

	R<Boolean> updateAdmonitorRule(AdmonitorRule admonitorRule);
}
