package com.dy.yunying.biz.controller.hbdata;

import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.datacenter.vo.NoticeDataReportVO;
import com.dy.yunying.api.req.NoticeDataReq;
import com.dy.yunying.biz.service.hongbao.PopupNoticeService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.api.util.StringUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author yuwenfeng
 * @description: 公告数据报表
 * @date 2022/3/7 17:58
 */
@RestController
@RequestMapping("/noticeData")
@Slf4j
public class NoticeDataController {

	@Autowired
	private PopupNoticeService popupNoticeService;

	@ResponseBody
	@GetMapping(value = "/selectNoticeCount")
	public R selectNoticeCount(@Valid NoticeDataReq req) {
		req.setCurrent(null);
		req.setSize(null);
		return popupNoticeService.countNoticeDataTotal(req);
	}

	@SysLog("公告数据报表")
	@ResponseBody
	@GetMapping("/selectNoticeData")
	public R<List<NoticeDataReportVO>> selectNoticeDataSource(@Valid NoticeDataReq req) {
		return R.ok(popupNoticeService.selectNoticeDataSource(req));
	}

	@SysLog("公告数据报表导出")
	@ResponseExcel(name = "公告数据报表导出", sheet = "公告数据报表导出")
	@RequestMapping("/excelNoticeData")
	public R excelNoticeDataSource(@Valid NoticeDataReq req, HttpServletResponse response, HttpServletRequest request) {
		String sheetName = "公告数据报表导出";
		try {
			req.setCurrent(null);
			req.setSize(null);
			List<NoticeDataReportVO> list = popupNoticeService.selectNoticeDataSource(req);
			// 查询汇总行-汇总只有一条
			if (StringUtils.isNotBlank(req.getQueryColumn()) || 4 != req.getCycleType()) {
				req.setCycleType(4);
				req.setQueryColumn(Constant.EMPTTYSTR);
				List<NoticeDataReportVO> allDataList = popupNoticeService.selectNoticeDataSource(req);
				list.addAll(allDataList);
			}
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(list);
			// 为百分比的值拼接百分号
			final String defaultValue = "0.00%";
			ExportUtils.spliceSuffix(resultListMap, Constant.PERCENT, defaultValue, "touchRatio", "clickRatio");
			// 导出
			ExportUtils.exportExcelData(request, response, "公告数据报表-" + DateUtils.getCurrentTimeNoUnderline() + ".xlsx", sheetName, req.getTitles(), req.getColumns(), resultListMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
