package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.req.WanGameBasePackageReq;
import com.dy.yunying.api.vo.GamePackageManageVO;

public interface WanGameBasePackageService {

    //查询所有父游戏+gname查询父游戏
	IPage<GamePackageManageVO> selectWanGameBasePackageList(WanGameBasePackageReq req);

}
