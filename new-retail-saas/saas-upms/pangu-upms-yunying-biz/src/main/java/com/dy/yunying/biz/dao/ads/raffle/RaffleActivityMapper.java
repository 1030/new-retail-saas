package com.dy.yunying.biz.dao.ads.raffle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.raffle.RaffleActivity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 抽奖活动配置表(raffle_activity)数据Mapper
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
*/
@Mapper
public interface RaffleActivityMapper extends BaseMapper<RaffleActivity> {

}
