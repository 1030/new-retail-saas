package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.GdtApp;
import org.apache.ibatis.annotations.Mapper;

/**
 * 广点通推广目标表
 * @author  chenxiang
 * @version  2022-10-13 13:33:44
 * table: gdt_app
 */
@Mapper
public interface GdtAppMapper extends BaseMapper<GdtApp> {
}


