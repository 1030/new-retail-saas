package com.dy.yunying.biz.dao.ads.sign;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.sign.SignReceiveRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 奖品领取记录表
 * @author  chengang
 * @version  2021-12-01 10:14:36
 * table: sign_receive_record
 */
@Mapper
public interface SignReceiveRecordMapper extends BaseMapper<SignReceiveRecord> {
	

}


