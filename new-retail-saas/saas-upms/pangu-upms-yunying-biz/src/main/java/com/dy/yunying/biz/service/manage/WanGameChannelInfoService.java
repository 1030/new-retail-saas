package com.dy.yunying.biz.service.manage;

import com.dy.yunying.api.entity.WanGameChannelInfo;
import com.dy.yunying.api.entity.WanGameChannelInfoDO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;
import java.util.Map;


public interface WanGameChannelInfoService {

	List<WanGameChannelInfo> selectWanGameChannelInfoListByCond(Map<String, Object> map);

	void save(WanGameChannelInfoDO wanGameChannelInfoDO);

	WanGameChannelInfoDO getByChl(String chl);

	WanGameChannelInfoDO updateByChl(WanGameChannelInfoDO gameChannelInfoDO);

	R updateCodeName(WanGameChannelInfoDO wanGameChannelInfoDO);
}
