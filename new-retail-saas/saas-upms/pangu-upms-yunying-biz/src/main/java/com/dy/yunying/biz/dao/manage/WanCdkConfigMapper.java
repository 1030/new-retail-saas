package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.WanCdkConfig;
import com.dy.yunying.api.resp.WanCdkConfigRes;
import com.dy.yunying.api.resp.WanCdkExportData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 代金券导出数据配置表
 * @author  chenxiang
 * @version  2022-04-18 16:23:05
 * table: cdk_grant_config
 */
@Mapper
public interface WanCdkConfigMapper extends BaseMapper<WanCdkConfig> {

	List<WanCdkExportData> selectUserData(WanCdkConfigRes record);

}


