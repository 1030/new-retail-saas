package com.dy.yunying.biz.service.prize;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.prize.PrizePushGame;

/**
 * 奖励与游戏关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:59
 * table: prize_push_game
 */
public interface PrizePushGameService extends IService<PrizePushGame> {
}


