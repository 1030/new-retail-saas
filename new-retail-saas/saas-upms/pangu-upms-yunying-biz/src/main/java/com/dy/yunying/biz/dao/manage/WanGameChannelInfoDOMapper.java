package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.WanGameChannelInfoDO;

/**
 * 请使用{@link WanGameChannelInfoMapper}中的方法，如果没有相应的方法也请在该类中扩展！！！！
 */
@Deprecated
public interface WanGameChannelInfoDOMapper {

	int deleteByPrimaryKey(Long id);

	int insert(WanGameChannelInfoDO record);

	int insertSelective(WanGameChannelInfoDO record);

	WanGameChannelInfoDO selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(WanGameChannelInfoDO record);

	int updateByPrimaryKey(WanGameChannelInfoDO record);

}