package com.dy.yunying.biz.controller.znfx;
import java.math.BigDecimal;

import com.dy.yunying.biz.utils.DateUtils;
import com.google.common.collect.Lists;

import com.dy.yunying.api.dto.RoiTopTenDto;
import com.dy.yunying.api.entity.znfx.ChartVO;
import com.dy.yunying.api.req.znfx.ZnfxReq;
import com.dy.yunying.api.vo.RoiTopTenVo;
import com.dy.yunying.biz.service.znfx.TopTenService;
import com.dy.yunying.biz.service.znfx.ZnfxService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.statement.select.Top;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: zhuxm
 * @time: 2023/3/21 14:03
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/create")
@Api(value = "create", tags = "创意top10")
@Slf4j
public class CreateTopTenController {

	private final TopTenService topTenService;

	@ApiOperation(value = "图形数据查询", notes = "图形数据查询")
	@PostMapping("/topTen")
	public R<List<RoiTopTenVo>> chartDataSearch(@RequestBody RoiTopTenDto dto) {
		try {
			if (StringUtils.isBlank(dto.getSdate()) || StringUtils.isBlank(dto.getEdate())){
				return R.failed("请选择开始时间和结束时间");
			}
			if (null == dto.getTopType()){
				return R.failed("TOP类型不能为空");
			}
			return R.ok(topTenService.topTen(dto));
		} catch (Exception e) {
			log.error("TOP10-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}



}
