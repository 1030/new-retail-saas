package com.dy.yunying.biz.service.manage.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.WanUserThird;
import com.dy.yunying.biz.dao.manage.WanUserThirdMapper;

import com.dy.yunying.biz.service.manage.WanUserThirdService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class WanUserThirdServiceImpl extends ServiceImpl<WanUserThirdMapper, WanUserThird>
    implements WanUserThirdService{

}




