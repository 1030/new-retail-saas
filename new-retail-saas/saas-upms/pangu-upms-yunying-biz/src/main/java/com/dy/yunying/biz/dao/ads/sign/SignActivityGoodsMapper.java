package com.dy.yunying.biz.dao.ads.sign;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.sign.SignActivityGoods;
import org.apache.ibatis.annotations.Mapper;

/**
 * 签到活动物品表
 * @author  chengang
 * @version  2021-12-01 10:14:07
 * table: sign_activity_goods
 */
@Mapper
public interface SignActivityGoodsMapper extends BaseMapper<SignActivityGoods> {
	

}


