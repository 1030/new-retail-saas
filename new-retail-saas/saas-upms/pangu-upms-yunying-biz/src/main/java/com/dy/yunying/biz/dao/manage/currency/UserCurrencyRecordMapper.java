package com.dy.yunying.biz.dao.manage.currency;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.dto.currency.UserCurrencyRecordDto;
import com.dy.yunying.api.entity.currency.UserCurrencyRecord;
import com.dy.yunying.api.req.currency.UserCurrencyRecordReq;
import org.apache.ibatis.annotations.Mapper;

/**
* @author hejiale
* @description 针对表【user_currency_record(账号平台币信息)】的数据库操作Mapper
* @createDate 2022-03-25 09:56:27
* @Entity generate.entity.UserCurrencyRecord
*/
@Mapper
public interface UserCurrencyRecordMapper extends BaseMapper<UserCurrencyRecord> {

	IPage<UserCurrencyRecordDto> pageDto(UserCurrencyRecordReq req);
}




