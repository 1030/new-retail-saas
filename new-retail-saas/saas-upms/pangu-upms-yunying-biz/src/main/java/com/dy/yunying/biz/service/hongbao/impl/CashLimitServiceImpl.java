package com.dy.yunying.biz.service.hongbao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbCashLimit;
import com.dy.yunying.biz.dao.ads.hongbao.HbCashLimitMapper;
import com.dy.yunying.biz.service.hongbao.CashLimitService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author yuwenfeng
 * @description: 提现规则限制
 * @date 2022/1/11 20:42
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class CashLimitServiceImpl extends ServiceImpl<HbCashLimitMapper, HbCashLimit> implements CashLimitService {

}
