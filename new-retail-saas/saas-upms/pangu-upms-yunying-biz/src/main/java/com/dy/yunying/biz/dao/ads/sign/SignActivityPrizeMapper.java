package com.dy.yunying.biz.dao.ads.sign;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.sign.SignActivityPrize;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * 签到活动奖品表
 * @author  chengang
 * @version  2021-12-01 10:14:16
 * table: sign_activity_prize
 */
@Mapper
public interface SignActivityPrizeMapper extends BaseMapper<SignActivityPrize> {

	List<SignActivityPrize> selectReceiveCountList(@Param("prizeIds") Collection<Long> prizeIds);

    int selectPrizeFullCount(@Param("activityId") Long activityId);

	int updatePrizeByActivityId(@Param("activityId") Long activityId);
}


