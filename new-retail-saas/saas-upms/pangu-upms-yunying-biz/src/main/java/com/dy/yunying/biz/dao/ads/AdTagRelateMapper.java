package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.tag.AdTagRelate;
import org.apache.ibatis.annotations.Mapper;

/**
 * 标签关联表(ad_tag_relate)数据Mapper
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
*/
@Mapper
public interface AdTagRelateMapper extends BaseMapper<AdTagRelate> {

}
