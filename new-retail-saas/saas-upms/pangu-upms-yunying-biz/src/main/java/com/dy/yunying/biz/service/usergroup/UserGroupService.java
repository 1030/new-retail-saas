

package com.dy.yunying.biz.service.usergroup;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.usergroup.UserGroup;
import com.dy.yunying.api.req.usergroup.UserGroupStatReq;
import com.dy.yunying.api.vo.usergroup.UserGroupNoticePriceVO;
import com.dy.yunying.api.vo.usergroup.UserGroupStatVO;
import com.dy.yunying.api.vo.usergroup.UserGroupVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;


public interface UserGroupService extends IService<UserGroup> {

	R saveOrUpdateUserGroup(UserGroupVo vo) throws Throwable;

	boolean updateByGroupId(UserGroup userGroup);

	List<UserGroupStatVO> getStatNums(UserGroupStatReq req);

	UserGroupNoticePriceVO getBindNoticeList(Long groupId);
}


