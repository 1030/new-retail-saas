package com.dy.yunying.biz.service.znfx;

import com.dy.yunying.api.dto.RoiTopTenDto;
import com.dy.yunying.api.req.FutureMaterialReq;
import com.dy.yunying.api.vo.RoiTopTenVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface TopTenService {

	List<RoiTopTenVo> topTen(RoiTopTenDto dto);


	R futureMaterial(FutureMaterialReq req);

	int futureCount(FutureMaterialReq req);
}
