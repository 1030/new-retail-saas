package com.dy.yunying.biz.service.yyz;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.GameAppointmentScheduleGears;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReq;
import com.pig4cloud.pig.common.core.util.R;


/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_schedule
 */
public interface GameAppointmentScheduleGearsService extends IService<GameAppointmentScheduleGears> {

	/**
	 * 预约列表 - 分页
	 *
	 * @param req
	 * @return
	 */
	R getPage(GameAppointmentScheduleGearsReq req);

	/**
	 * 新增
	 *
	 * @param req
	 * @return
	 */
	R gameAddSchedule(GameAppointmentScheduleGearsReq req);

	/**
	 * 修改
	 *
	 * @param req
	 * @return
	 */
	R gameEditSchedule(GameAppointmentScheduleGearsReq req);
	/**
	 * 删除
	 *
	 * @param req
	 * @return
	 */
	R gameDelSchedule(GameAppointmentScheduleGearsReq req);

}


