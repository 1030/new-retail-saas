package com.dy.yunying.biz.service.yyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.GameAppointmentScheduleGears;
import com.dy.yunying.api.entity.GameAppointmentScheduleGearsPage;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsList;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReq;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReward;
import com.dy.yunying.biz.dao.manage.GameAppointmentMapper;
import com.dy.yunying.biz.dao.manage.GameAppointmentScheduleGearsMapper;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleGearsRewardService;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleGearsService;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.common.core.mybatis.Page;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 游戏预约进度表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_Schedule
 */
@Log4j2
@Service("GameAppointmentScheduleGearsService")
@RequiredArgsConstructor
public class GameAppointmentScheduleGearsServiceImpl extends ServiceImpl<GameAppointmentScheduleGearsMapper, GameAppointmentScheduleGears> implements GameAppointmentScheduleGearsService {

	private final GameAppointmentMapper gameAppointmentMapper;

	private final GameAppointmentScheduleGearsMapper gameAppointmentScheduleGearsMapper;

	@Autowired
	private final GameAppointmentScheduleGearsRewardService scheduleGearsRewardService;


	@Override
	public R getPage(GameAppointmentScheduleGearsReq req) {
		IPage<GameAppointmentScheduleGearsPage> scheduleGearsPage = gameAppointmentScheduleGearsMapper.selectScheduleGearsPage(req);
		scheduleGearsPage.getRecords().forEach(scheduleGears ->{

			List<GameAppointmentScheduleGearsReward> scheduleGearsRewards=new ArrayList<>();
			if(Objects.nonNull(scheduleGears)) {
				scheduleGearsRewards = scheduleGearsRewardService.selectScheduleGearsReward(scheduleGears);
			}
			scheduleGears.setScheduleGearsRewards(scheduleGearsRewards);
		});
		return R.ok(scheduleGearsPage);
	}

	@Override
	public R gameAddSchedule(GameAppointmentScheduleGearsReq req) {
		GameAppointmentScheduleGears appointmentScheduleGears = this.getOne(Wrappers.<GameAppointmentScheduleGears>lambdaQuery().eq(GameAppointmentScheduleGears::getType, req.getType()).eq(GameAppointmentScheduleGears::getRuleNumber, req.getRuleNumber()));
		if (Objects.nonNull(appointmentScheduleGears)) {
			log.info("当前档位已存在,无法继续添加");
			return R.failed(6, "当前档位已存在,无法继续添加");
		}

		GameAppointmentScheduleGears gameAppointmentScheduleGears = new GameAppointmentScheduleGears();
		try {
				gameAppointmentScheduleGears.setCreateTime(new Date());
				gameAppointmentScheduleGears.setUpdateTime(new Date());
				gameAppointmentScheduleGears.setType(req.getType());
				gameAppointmentScheduleGears.setRuleNumber(req.getRuleNumber());
				gameAppointmentScheduleGears.setGameName(gameAppointmentMapper.getGameName(req.getType()));

			// 修改素材信息
			boolean flag = this.save(gameAppointmentScheduleGears);
			if (!flag) {
				log.info(gameAppointmentScheduleGears.getName() + "上传失败");
				return R.failed(6, "新建预约进度失败");
			}
			setScheduleGearsReward(gameAppointmentScheduleGears,req);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("文件上传失败：{}", e.getMessage());
			return R.failed(6, "新建预约进度失败");
		}
		return R.ok(1, "新建预约进度成功");
	}

	@Override
	public R gameEditSchedule(GameAppointmentScheduleGearsReq req) {
		GameAppointmentScheduleGears appointmentScheduleGears = this.getOne(Wrappers.<GameAppointmentScheduleGears>lambdaQuery().notIn(GameAppointmentScheduleGears::getId,req.getId()).eq(GameAppointmentScheduleGears::getType, req.getType()).eq(GameAppointmentScheduleGears::getRuleNumber, req.getRuleNumber()));
		if (Objects.nonNull(appointmentScheduleGears)) {
			log.info("当前档位已存在,无法继续添加");
			return R.failed(6, "当前档位已存在,无法继续添加");
		}
		GameAppointmentScheduleGears gameAppointmentScheduleGears = new GameAppointmentScheduleGears();
		try {
			gameAppointmentScheduleGears.setId(Long.valueOf(req.getId()));
			gameAppointmentScheduleGears.setRuleNumber(req.getRuleNumber());
			gameAppointmentScheduleGears.setUpdateTime(new Date());
			// 修改素材信息
			boolean flag = this.updateById(gameAppointmentScheduleGears);
			if (!flag) {
				log.info(gameAppointmentScheduleGears.getName() + "上传失败");
				return R.failed(6, "编辑预约进度失败");
			}
			setScheduleGearsReward(gameAppointmentScheduleGears,req);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("文件上传失败：{}", e.getMessage());
			return R.failed(6, "编辑预约进度失败");
		}
		return R.ok(1, "编辑预约进度成功");
	}

	@Override
	public R gameDelSchedule(GameAppointmentScheduleGearsReq req) {
		GameAppointmentScheduleGears gameAppointmentScheduleGears = new GameAppointmentScheduleGears();
		gameAppointmentScheduleGears.setId(Long.valueOf(req.getId()));
		gameAppointmentScheduleGears.setIsDelete(2);
		boolean flag = this.updateById(gameAppointmentScheduleGears);
		if (!flag) {
			return R.failed(6,"删除失败");
		}
		//删除规则与规则奖励的关系
		this.scheduleGearsRewardService.remove(Wrappers.<GameAppointmentScheduleGearsReward>lambdaQuery().eq(GameAppointmentScheduleGearsReward::getRuleId, gameAppointmentScheduleGears.getId()));
		return R.ok(1,"删除成功");
	}

	/**
	 * 更新规则与规则奖励的关系
	 * @param gameAppointmentScheduleGears
	 * @param req
	 */
	private void setScheduleGearsReward(GameAppointmentScheduleGears gameAppointmentScheduleGears,GameAppointmentScheduleGearsReq  req) {
		//删除规则与规则奖励的关系
		this.scheduleGearsRewardService.remove(Wrappers.<GameAppointmentScheduleGearsReward>lambdaQuery().eq(GameAppointmentScheduleGearsReward::getRuleId, gameAppointmentScheduleGears.getId()));
		//添加规则与规则奖励的关系入库
		List<GameAppointmentScheduleGearsReward> scheduleGearsRewards = Lists.newArrayList();
		List<GameAppointmentScheduleGearsList> rewardList = req.getRewardList();
		rewardList.forEach(reward -> {
			GameAppointmentScheduleGearsReward scheduleGearsReward = new GameAppointmentScheduleGearsReward();

			scheduleGearsReward.setType(req.getType());
			scheduleGearsReward.setRuleId(gameAppointmentScheduleGears.getId().toString());
			scheduleGearsReward.setRewardName(reward.getRewardName());
			scheduleGearsReward.setRewardNum(reward.getRewardNum());
			scheduleGearsReward.setBucketName(reward.getBucketName());
			scheduleGearsReward.setFileUrl(reward.getFileUrl());
			scheduleGearsReward.setSort(reward.getSort());
			scheduleGearsReward.setRuleNumber(req.getRuleNumber());
			scheduleGearsReward.setCreateTime(new Date());
			scheduleGearsReward.setUpdateTime(new Date());
			scheduleGearsRewards.add(scheduleGearsReward);
		});
		scheduleGearsRewardService.saveBatch(scheduleGearsRewards);

	}



}


