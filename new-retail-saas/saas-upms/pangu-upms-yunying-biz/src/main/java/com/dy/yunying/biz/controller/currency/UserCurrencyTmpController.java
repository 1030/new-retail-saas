package com.dy.yunying.biz.controller.currency;

import com.dy.yunying.biz.service.currency.UserCurrencyTmpService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户游豆明细
 *
 * @author hejiale
 * @date 2022-3-23 14:25:29
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/currencyTmp")
@Api(value = "currencyRecord", tags = "用户游豆明细")
@Slf4j
public class UserCurrencyTmpController {

	private final UserCurrencyTmpService userCurrencyTmpService;

//	@SysLog("用户游豆明细列表")
//	@PostMapping("/page")
////	@PreAuthorize("@pms.hasPermission('currency_record_page')")
//	public R page(@Valid @RequestBody UserCurrencyReq req){
//		try {
//			IPage<AdKpiDO> page = userCurrencyTmpService.page(req);
//			return R.ok(page);
//		} catch (Exception e) {
//			log.error("用户游豆-列表:{}",e);
//			return R.failed("用户游豆列表查询失败");
//		}
//	}

}
