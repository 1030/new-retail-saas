package com.dy.yunying.biz.service.manage;


import com.dy.yunying.api.dto.AccountProhibitLogDTO;

/**
 *
 * @Author: leisw
 * @Date: 2022/8/22 13:52
 */
public interface ProhibitLogService {

	AccountProhibitLogDTO save(AccountProhibitLogDTO record);

}