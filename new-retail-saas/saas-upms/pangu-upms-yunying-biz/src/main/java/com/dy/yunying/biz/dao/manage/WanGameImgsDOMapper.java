package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.WanGameImgsDO;

public interface WanGameImgsDOMapper {
	int deleteByPrimaryKey(Long imgid);

	int insert(WanGameImgsDO record);

	int insertSelective(WanGameImgsDO record);

	WanGameImgsDO selectByPrimaryKey(Long imgid);

	int updateByPrimaryKeySelective(WanGameImgsDO record);

	int updateByPrimaryKey(WanGameImgsDO record);
}