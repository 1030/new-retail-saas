package com.dy.yunying.biz.exception;

import lombok.Data;

/**
 * @Author helei
 */
@Data
public class CommentException extends RuntimeException{
	private Integer code;

	public CommentException(Integer code,String message){
		super(message);
		this.code = code;
	}
}
