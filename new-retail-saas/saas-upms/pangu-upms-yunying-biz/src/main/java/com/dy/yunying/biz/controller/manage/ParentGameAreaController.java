package com.dy.yunying.biz.controller.manage;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dy.yunying.api.dto.ParentGameAreaDto;
import com.dy.yunying.api.entity.ParentGameArea;
import com.dy.yunying.biz.service.manage.ParentGameAreaService;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 主游戏区服关联表
 *
 * @author chengang
 * @version 2021-10-27 10:42:03
 * table: parent_game_area
 */
@RestController
@RequestMapping("/parentGameArea")
@RequiredArgsConstructor
public class ParentGameAreaController {

	private final ParentGameAreaService parentGameAreaService;

	@SysLog("新增或编辑主游戏区服")
	@PostMapping("/saveOrUpdate")
	public R saveOrUpdate(@RequestBody ParentGameArea dto) {
		R check = this.baseCheck(dto);
		if (check.getCode() == CommonConstants.FAIL) {
			return check;
		}
		//校验区服ID和区服名称不能重复,不同的主游戏之间可以重复
		List<ParentGameArea> list = parentGameAreaService.list(Wrappers.<ParentGameArea>lambdaQuery()
				.eq(ParentGameArea::getParentGameId, dto.getParentGameId())
				.eq(ParentGameArea::getDeleted, 0)
				.and(wrapper -> wrapper.eq(ParentGameArea::getAreaId, dto.getAreaId())
						.or()
						.eq(ParentGameArea::getAreaName, dto.getAreaName()))
				.and(dto.getId() != null, wrapper -> wrapper.ne(ParentGameArea::getId, dto.getId())));
		if (CollectionUtils.isNotEmpty(list)) {
			return R.failed("区服ID和区服名称不能重复");
		}
		PigUser user = SecurityUtils.getUser();
		dto.setCreateId(user.getId().longValue());
		dto.setUpdateId(user.getId().longValue());
		return R.ok(parentGameAreaService.saveOrUpdate(dto));
	}

	@SysLog("删除主游戏区服")
	@GetMapping("/delete")
	public R delete(@RequestParam Long id) {
		if (id == null) {
			return R.failed("主键ID不能为空");
		}
		return R.ok(parentGameAreaService.removeById(id));
	}

	@PreAuthorize("@pms.hasPermission('PARENT_GAME_AREA')")
	@GetMapping("/list")
	public R getList(ParentGameAreaDto dto) {
		if (dto.getParentGameId() == null) {
			return R.failed("父游戏ID不能为空");
		}
		LambdaQueryWrapper<ParentGameArea> query = new LambdaQueryWrapper<>();
		query.and(wrapper -> wrapper.eq(ParentGameArea::getDeleted, 0));
		query.and(wrapper -> wrapper.eq(ParentGameArea::getParentGameId, dto.getParentGameId()));
		//根据创建时间升序
		query.orderByAsc(ParentGameArea::getCreateTime);
		return R.ok(parentGameAreaService.list(query));
	}

	private R baseCheck(ParentGameArea dto) {
		if (dto.getParentGameId() == null) {
			return R.failed("父游戏ID不能为空");
		}

		if (dto.getAreaId() == null) {
			return R.failed("区服ID不能为空");
		}

		if (StringUtils.isBlank(dto.getAreaName())) {
			return R.failed("区服名称不能为空");
		}

		if (dto.getAreaName().length() > 30) {
			return R.failed("区服名称长度不能超过30");
		}

		return R.ok();

	}


}


