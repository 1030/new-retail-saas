package com.dy.yunying.biz.service.manage;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.WanUserThird;

/**
 *
 */
public interface WanUserThirdService extends IService<WanUserThird> {
}
