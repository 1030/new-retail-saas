package com.dy.yunying.biz.service.hongbao;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.dto.hongbao.HbReceivingRecordDto;
import com.dy.yunying.api.entity.hongbao.HbReceivingRecord;
import com.dy.yunying.api.vo.hongbao.HbReceivingRecordVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 奖品领取记录
 * @author  chenxiang
 * @version  2021-10-25 14:21:04
 * table: hb_receiving_record
 */
public interface HbReceivingRecordService extends IService<HbReceivingRecord> {
	/**
	 * 领取记录列表
	 * @param vo
	 * @return
	 */
	R getPage(HbReceivingRecordVo vo);
	/**
	 * 审核礼品
	 * @param vo
	 * @return
	 */
	R verifyRecord(HbReceivingRecordVo vo);
	/**
	 * 发货 - 填写快递信息
	 * @param vo
	 * @return
	 */
	R deliverGoods(HbReceivingRecordVo vo);
	/**
	 * 导出
	 * @param vo
	 * @return
	 */
	List<HbReceivingRecordDto> exportRecord(HbReceivingRecordVo vo);
}


