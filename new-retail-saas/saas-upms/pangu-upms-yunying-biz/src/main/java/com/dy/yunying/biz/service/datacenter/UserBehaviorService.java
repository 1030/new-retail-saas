package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.UserBehaviorDto;
import com.pig4cloud.pig.common.core.util.R;


/**
 * @description:
 * @author: leisw
 * @time: 2022/8/19 14:54
 **/

public interface UserBehaviorService {

	R count(UserBehaviorDto req);

	/*
	 * 查询用户行为数据
	 * */
	R selectUserBehavior(UserBehaviorDto req);

	R getUserBehavior(UserBehaviorDto req);

	R getOsName(UserBehaviorDto req);
}
