package com.dy.yunying.biz.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName YunYingDorisTableProperties
 * @Description todo
 * @Author nieml
 * @Time 2022/12/29 16:28
 * @Version 1.0
 **/
@RefreshScope
@Configuration
@Data
public class YunYingDorisTableProperties {

	/*******************************doris*******************************/
	private final String ACTIVE_DAY = "active_day";
	private final String REG_DAY = "reg_day";

	/**
	 *  查询字段
	 * */
	@Value("${adsOperation3399AdReportActiveFields:active_day,week,month,year,reg_day,reg_week,reg_month,media_code,chl_app,ad_id,creative_id, active_device_bitmap,active_device_pay_bitmap,active_fee,active_givemoney,active_pay_num}")
	private String adsOperation3399AdReportActiveFields;

	@Value("${adsOperation3399AdReportRegFields:reg_day,active_day,reg_week,reg_month,reg_year,media_code,chl_app,ad_id,creative_id,reg_device_bitmap,new_device_bitmap, active_device_bitmap_1, new_device_pay_bitmap,new_fee_1,new_givemoney_1, new_fee_week, new_givemoney_week, new_fee_month,new_givemoney_month,active_device_pay_bitmap, total_fee, total_givemoney,deduplicate_device_bitmap,return_device_bitmap,duplicate_device_bitmap,new_pay_num_1,create_role_bitmap,create_role_num ,certi_device_bitmap,reg_account_bitmap, certi_account_bitmap,active_fee,active_givemoney,active_device_bitmap,active_pay_num}")
	private String adsOperation3399AdReportRegFields;

	@Value("${adsOperation3399AdReportHourDataFields:reg_day,reg_hour,active_day,hour,media_code,chl_app,ad_id,new_device_bitmap,new_fee_1,active_device_pay_bitmap}")
	private String adsOperation3399AdReportHourDataFields;


	/**
	 * doris 中间表
	 * */
	@Value("${adsOperation3399AdReport:operation_3399_dw.ads_operation_ad_report}")
	private String adsOperation3399AdReport;
	@Value("${adsOperation3399AdReport:operation_3399_dw.ads_operation_ad_report_offline}")
	private String adsOperation3399AdReportOffline;
	@Value("${dwsOperation3399AdCostD:operation_3399_dw.dws_operation_ad_cost_d}")
	private String dwsOperation3399AdCostD;

	@Value("${dim3399OfflineRepair:operation_3399_dw.dim_offline_repair}")
	private String dim3399OfflineRepair;


	/**
	 * doris中的维度表/视图
	 * */
	@Value("${vDimOperation3399ChannelGameInvestor:dc_dim.v_dim_operation_3399_channel_game_investor}")
	private String vDimOperation3399ChannelGameInvestor;
	@Value("${vDimOperation3399AdChannel:dc_dim.v_dim_operation_3399_ad_channel}")
	private String vDimOperation3399AdChannel;
	@Value("${dim3399MysqlAdAccountAgent:dc_dim.dim_3399_mysql_ad_account_agent}")
	private String dim3399MysqlAdAccountAgent;
	@Value("${vDimPanguAdidInfo3399:dc_dim.v_dim_pangu_adid_info_3399}")
	private String vDimPanguAdidInfo3399;


	@Value("${dim200PanguMysqlAdCreativeMaterial:dc_dim.dim_200_pangu_mysql_ad_creative_material}")
	private String dim200PanguMysqlAdCreativeMaterial;
	@Value("${dim200PanguMysqlAdMaterial:dc_dim.dim_200_pangu_mysql_ad_material}")
	private String dim200PanguMysqlAdMaterial;
	@Value("${vOdsmysqlDesignSellingPoint:dc_dim.v_odsmysql_design_selling_point}")
	private String vOdsmysqlDesignSellingPoint;
	/*******************************doris*******************************/

}
