package com.dy.yunying.biz.dao.ads.prize;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.prize.PrizePush;
import com.dy.yunying.api.req.prize.PrizePushReq;
import com.dy.yunying.api.resp.prize.PrizePushRes;
import com.dy.yunying.api.vo.usergroup.UserGroupStatVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 奖励推送表
 *
 * @author chenxiang
 * @version 2022-04-25 16:25:27 table: prize_push
 */
@Mapper
public interface PrizePushMapper extends BaseMapper<PrizePush> {

	/**
	 * 奖品推送列表 - 分页
	 * @param record
	 * @return
	 */
	IPage<PrizePushRes> selectPrizePushPage(PrizePushReq record);

	List<Long> selectRangeNoticeId(@Param("parentGameArr") String parentGameArr, @Param("gameArr") String gameArr,
			@Param("areaArr") String areaArr);

	List<UserGroupStatVO> countWithUserGroupIds(@Param("userGroupIds")List<Long> userGroupIds);

	List<Long> getPrizeIdsWithUserGroupId(@Param("userGroupId")Long groupId);
}
