package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbActivityCenterChannel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 活动中心与渠道关系表
 * @author  chenxiang
 * @version  2022-06-18 11:46:59
 * table: hb_activity_center_channel
 */
@Mapper
public interface HbActivityCenterChannelMapper extends BaseMapper<HbActivityCenterChannel> {
}


