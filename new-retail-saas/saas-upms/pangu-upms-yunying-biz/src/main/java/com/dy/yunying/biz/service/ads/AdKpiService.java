package com.dy.yunying.biz.service.ads;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.AdKpi;
import com.dy.yunying.api.entity.AdKpiDO;
import com.dy.yunying.api.req.AdKpiReq;
import com.dy.yunying.api.vo.AdKpiVo;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @description:
 * @author: nml
 * @time: 2021/7/8 16:03
 **/
public interface AdKpiService extends IService<AdKpi> {

	IPage<AdKpiVo> queryAdKpiDataPage(AdKpiReq req);

	IPage<AdKpiDO> queryAdKpiPage(AdKpiReq req);

	R saveAdKpi(AdKpi adKpi);

	R editAdKpi(AdKpi adKpi);

	R delAdKpi(Integer id);

}
