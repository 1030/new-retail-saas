package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.dto.ChannelManageDto;
import com.dy.yunying.api.entity.ChannelManageDO;
import com.dy.yunying.api.req.ChannelManageReq;
import com.dy.yunying.api.vo.ChannelManageVo;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;

import java.util.List;
import java.util.Map;

//@FeignClient(contextId = "channelManageService", value = ServiceNameConstants.UPMS_YUNYING_SERVICE)
public interface ChannelManageService extends IService<ChannelManageDO> {
	R<Integer> saveParentchl(ChannelManageDto channel);

	IPage<ChannelManageVo> queryParentchlList(ChannelManageReq req);

	ChannelManageVo getVOByPK(Integer id);

	ChannelManageVo getVOByCode(String code);

	R updateParentchl(ChannelManageDto channel);

	R savechl(ChannelManageDto channel);

	IPage<ChannelManageVo> queryChlList(ChannelManageReq req);

	ChannelManageVo selectParentChlByCode(String chncode);

	ChannelManageVo selectParentChlByCodeIgnoreDel(String chncode);

	ChannelManageVo selectChildChlByCode(String chncode);

	ChannelManageVo selectChildChlByCodeIgnoreDel(String chncode);

	List<Map<String, String>> queryPlatForm(Map<String, Object> map);

	List<Map<String, Object>> queryChannelFromManager(Map<String, Object> map);

	R updateChl(ChannelManageDto req);

	ChannelManageVo getDefaultSonVO(int pid);

	void savedefaultchl(ChannelManageDto channel);

	R saveCpsChl(ChannelManageDto channel);
}
