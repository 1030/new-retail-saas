package com.dy.yunying.biz.dao.ads.usergroup;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.usergroup.UserGroup;
import com.dy.yunying.api.entity.usergroup.UserGroupLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserGroupLogMapper extends BaseMapper<UserGroupLog> {
}
