package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbCashLimit;

/**
 * @author yuwenfeng
 * @description: 提现规则限制
 * @date 2022/1/11 20:40
 */
public interface CashLimitService extends IService<HbCashLimit> {

}
