package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.WanChannelPackDO;

public interface WanChannelPackDOMapper {
	int deleteByPrimaryKey(Long packId);

	int insert(WanChannelPackDO record);

	int insertSelective(WanChannelPackDO record);

	WanChannelPackDO selectByPrimaryKey(Long packId);

	int updateByPrimaryKeySelective(WanChannelPackDO record);

	int updateByPrimaryKey(WanChannelPackDO record);
}