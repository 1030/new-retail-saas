

package com.dy.yunying.biz.service.hongbao;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbInvitationTop;

/**
 * 手动榜单表
 * @author  chengang
 * @version  2021-10-27 19:21:20
 * table: hb_invitation_top
 */
public interface HbInvitationTopService extends IService<HbInvitationTop> {

	
}


