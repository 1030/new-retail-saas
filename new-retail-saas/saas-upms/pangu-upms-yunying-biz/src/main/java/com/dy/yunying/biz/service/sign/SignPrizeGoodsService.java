

package com.dy.yunying.biz.service.sign;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.sign.SignPrizeGoods;

/**
 * 奖品物品关联表
 * @author  chengang
 * @version  2021-12-01 10:14:26
 * table: sign_prize_goods
 */
public interface SignPrizeGoodsService extends IService<SignPrizeGoods> {

	
}


