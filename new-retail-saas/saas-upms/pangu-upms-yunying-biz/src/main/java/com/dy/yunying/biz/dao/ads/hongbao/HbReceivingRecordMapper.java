package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.dto.hongbao.HbReceivingRecordDto;
import com.dy.yunying.api.entity.hongbao.HbReceivingRecord;
import com.dy.yunying.api.vo.hongbao.HbReceivingRecordVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 奖品领取记录
 *
 * @author chenxiang
 * @version 2021-10-25 14:21:04
 * table: hb_receiving_record
 */
@Mapper
public interface HbReceivingRecordMapper extends BaseMapper<HbReceivingRecord> {
	/**
	 * 领取记录
	 *
	 * @param vo
	 * @return
	 */
	IPage<HbReceivingRecordDto> selectRecordByPage(HbReceivingRecordVo vo);

	Double sumExpendBalanceByType(@Param("activityId") Long activityId, @Param("type") int type);

	Double sumExpendBalanceExcludeGift(@Param("activityId") Long activityId, @Param("type") int type);
}


