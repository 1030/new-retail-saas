package com.dy.yunying.biz.dao.manage.ext;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.entity.ParentGameDOSums;
import com.dy.yunying.api.entity.ParentSonGameDO;
import com.dy.yunying.api.req.ParentGameReq;
import com.dy.yunying.api.resp.hongbao.NodeData;
import com.dy.yunying.api.vo.ParentGameVO;
import com.dy.yunying.biz.dao.manage.ParentGameDOMapper;

import java.util.List;

public interface ParentGameDOMapperExt extends ParentGameDOMapper {

	IPage<ParentGameVO> page(ParentGameReq req);

	//父游戏名查询,核实新增用户名是否存在于源表中
	ParentGameDO selectByParentGName(ParentGameDO parentGame);

	List<ParentGameDOSums> selectNums();

	/**
	 * 根据查询条件获取游戏list
	 *
	 * @param
	 * @return
	 */
	List<ParentGameDO> selectParentGameListByCond(ParentGameDO req);

	List<ParentGameVO> selectParentGameListCodeAndName(ParentGameDO req);

	List<ParentSonGameDO> selectParentSonGameList();

	List<ParentGameVO> versionVOList(ParentGameDO req);

	List<NodeData> selectMainGameList();

    List<NodeData> selectMainGameListByPid(Long[] pGids);

}