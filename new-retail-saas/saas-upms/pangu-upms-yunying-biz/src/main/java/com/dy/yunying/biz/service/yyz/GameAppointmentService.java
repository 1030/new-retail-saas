package com.dy.yunying.biz.service.yyz;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.GameAppointment;
import com.dy.yunying.api.req.yyz.GameAppointmentImportSendReq;
import com.dy.yunying.api.req.yyz.GameAppointmentQueryReq;
import com.dy.yunying.api.req.yyz.GameAppointmentQueryReqList;
import com.dy.yunying.api.req.yyz.GameAppointmentReq;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment
 */
public interface GameAppointmentService extends IService<GameAppointment> {


	/**
	 * 预约列表 - 分页
	 *
	 * @param req
	 * @return
	 */
	R getPage(GameAppointmentQueryReq req);

	/**
	 * 新增
	 *
	 * @param req
	 * @return
	 */
	R addAppointment(GameAppointmentReq req);


	/**
	 * 预约总数(幻兽苍角)
	 *
	 * @return
	 */
	R total();

	/**
	 * 预约总数
	 *
	 * @return
	 */
	R allTotal(GameAppointmentReq req);

	R batchMsg(GameAppointmentReq req);

	R getGameList();

	R sendCodeList();

	/***
	 * 角色查询-分页
	 */
	void export(HttpServletResponse response, GameAppointmentQueryReqList req) ;

}


