/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.service.manage;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.AdRoleUser;
import com.dy.yunying.api.req.AdRoleUserReq;
import com.pig4cloud.pig.admin.api.vo.UserSelectVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 角色可查看的平台用户
 *
 * @author zhuxm
 * @date 2021-06-04
 */
public interface AdRoleUserService extends IService<AdRoleUser> {

	List<UserSelectVO> getOwnerRoleUsers();

	List<Integer> getOwnerRoleUserIds();


	void saveRoleUser(AdRoleUserReq adRoleUserReq);
	/**
	 * 投放人绑定广告账户并且激活的的用户
	 * @return
	 */
	R bindingUserList();

	/**
	 * 投放人已激活的用户
	 * @return
	 */
	R activationUserList();

	/**
	 * 查询cps用户
	 * @return
	 */
	R selectCpsUserList();

	/**
	 * 当前登录人所关联角色对应的投放人集合
	 * @return
	 */
	List<Integer> getThrowUserList();

	/**
	 * 当前登录人所关联角色对应的投放人的广告账户集合
	 * @return
	 */
	List<String> getThrowAccountList();

	/**
	 * 	获取自身权限下的子游戏列表
	 * @param
	 * @return
	 */
	List<Integer> getThrowRoleGames();

	/**
	 * 查询飞书用户信息
	 * @return
	 */
	 R selectFsUserList();
}
