package com.dy.yunying.biz.controller.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.vo.ParentGameVO;
import com.dy.yunying.api.req.ParentGameReq;
import com.dy.yunying.biz.service.manage.AdRoleGameService;
import com.dy.yunying.biz.service.manage.ParentGameService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ：lile
 * @date ：2021/5/29 15:00
 * @description：
 * @modified By：
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/parentgame")
@Api(value = "parentgame", tags = "父游戏管理模块")
public class ParentGameController {

	@Autowired
	private ParentGameService parentGameService;

	@Autowired
	private AdRoleGameService adRoleGameService;

	/**
	 * 获取父游戏名称列表
	 *
	 * @param req
	 * @return
	 */
	@PostMapping("/getCacheablePGameList")
	public R<List<ParentGameDO>> getCacheablePGameList(@RequestBody ParentGameReq req) {
		try {
			List<ParentGameDO> parentGameList = parentGameService.getCacheablePGameList(req);
			return R.ok(parentGameList, "查询成功");
		} catch (Exception e) {
			log.error("查询父游戏名称列表失败", e);
			return R.failed("查询父游戏名称列表失败");
		}
	}

	/**
	 * 分页列表
	 *
	 * @param req
	 * @return
	 */
	@SysLog("游戏管理")
	@RequestMapping("/page")
	public R getPage(ParentGameReq req) {
		try {
			Page pge = new Page();
			pge.setSize(req.getSize());
			pge.setCurrent(req.getCurrent());
			//获取自身角色下的主游戏id列表
			req.setIds(adRoleGameService.getOwnerRolePgIds());
			req.setStatus(1);
			return R.ok(parentGameService.getPage(req), "查询成功");
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed("查询失败");
		}
	}


	//添加父游戏
	@SysLog("添加父游戏")
	@PostMapping(value = "/addParenGame.json")
	public R edit(@RequestBody ParentGameDO parentGame) {
		if (StringUtils.isBlank(parentGame.getGname()) || parentGame.getGname().length() >= 50) {
			return R.failed("游戏名称长度不能超过限制，且非空");
		}
		String pk_name = parentGame.getPkName();
		if (StringUtils.isNotBlank(pk_name)) {
			if (!pk_name.matches("([a-zA-Z_][a-zA-Z0-9_]*[.])*([a-zA-Z_][a-zA-Z0-9_]*)$")) {
				return R.failed("APP包名格式验证失败，例：com.sjda.GameApi");
			}
			if (pk_name.length() >= 100) {
				return R.failed("包名长度不能超过限制");
			}
		}

		if (StringUtils.isNotBlank(parentGame.getExchangeUrl()) && parentGame.getExchangeUrl().length() >= 100) {
			return R.failed("充值回调地址长度超过限制");
		}
		if (StringUtils.isNotBlank(parentGame.getLoginaddress()) && parentGame.getLoginaddress().length() >= 100) {
			return R.failed("登录地址长度超过限制");
		}
		if (StringUtils.isNotBlank(parentGame.getExchangeUrl()) && !isUrl(parentGame.getExchangeUrl())) {
			return R.failed("充值回调地址格式错误");
		}
		if (StringUtils.isNotBlank(parentGame.getRoleQueryUrl()) && !isUrl(parentGame.getRoleQueryUrl())) {
			return R.failed("游戏角色等级校验地址格式错误");
		}
		if (StringUtils.isNotBlank(parentGame.getLoginaddress()) && !isUrl(parentGame.getLoginaddress())) {
			return R.failed("登录地址格式错误");
		}
		if (StringUtils.isNotBlank(parentGame.getLoginMapUrl()) && parentGame.getLoginMapUrl().trim().length() > 200) {
			return R.failed("登录图地址长度超过限制");
		}
		if (StringUtils.isNotBlank(parentGame.getGameLogo()) && parentGame.getGameLogo().trim().length() > 200) {
			return R.failed("游戏LOGO地址长度超过限制");
		}
		if (StringUtils.isNotBlank(parentGame.getLogingMapUrl()) && parentGame.getLogingMapUrl().trim().length() > 200) {
			return R.failed("游戏LOGING图地址长度超过限制");
		}
		/*设置修改日期*/
		parentGame.setLaunchtime(new Date());
		/*设置请求接口秘钥*/
		parentGame.setQueryKey(getRandomString());
		/*设置兑换接口秘钥*/
		parentGame.setExchangeKey(getRandomString());

		R result = parentGameService.saveParentGame(parentGame);
		return result;
	}

	//编辑----更新父游戏
	@SysLog("编辑父游戏")
	@PostMapping("/editParentGame.json")
	public R updateParentGame(@RequestBody ParentGameDO parentGame) {
		if (StringUtils.isBlank(parentGame.getGname()) || parentGame.getGname().length() >= 50) {
			return R.failed("游戏名称长度不能超过限制，且非空");
		}
		String pk_name = parentGame.getPkName();
		if (StringUtils.isNotBlank(pk_name)) {
			if (!pk_name.matches("([a-zA-Z_][a-zA-Z0-9_]*[.])*([a-zA-Z_][a-zA-Z0-9_]*)$")) {
				return R.failed("APP包名格式验证失败，例：com.sjda.GameApi");
			}
			if (pk_name.length() >= 100) {
				return R.failed("包名长度不能超过限制");
			}
		}
		if (StringUtils.isNotBlank(parentGame.getExchangeUrl()) && parentGame.getExchangeUrl().length() >= 100) {
			return R.failed("充值回调地址长度超过限制");
		}
		if (StringUtils.isNotBlank(parentGame.getLoginaddress()) && parentGame.getLoginaddress().length() >= 100) {
			return R.failed("登录地址长度超过限制");
		}
		if (StringUtils.isNotBlank(parentGame.getExchangeUrl()) && !isUrl(parentGame.getExchangeUrl())) {
			return R.failed("充值回调地址格式错误");
		}
		if (StringUtils.isNotBlank(parentGame.getLoginaddress()) && !isUrl(parentGame.getLoginaddress())) {
			return R.failed("登录地址格式错误");
		}
		if (StringUtils.isNotBlank(parentGame.getLoginMapUrl()) && parentGame.getLoginMapUrl().trim().length() > 200) {
			return R.failed("登录图地址长度超过限制");
		}
		if (StringUtils.isNotBlank(parentGame.getGameLogo()) && parentGame.getGameLogo().trim().length() > 200) {
			return R.failed("游戏LOGO地址长度超过限制");
		}
		if (StringUtils.isNotBlank(parentGame.getLogingMapUrl()) && parentGame.getLogingMapUrl().trim().length() > 200) {
			return R.failed("游戏LOGING图地址长度超过限制");
		}
		if (StringUtils.isNotBlank(parentGame.getRoleQueryUrl()) && !isUrl(parentGame.getRoleQueryUrl())) {
			return R.failed("游戏角色等级校验地址格式错误");
		}
		R result = parentGameService.updateParentGame(parentGame);
		return result;
	}


	//验证登录地址和充值回调地址是否合法
	public boolean isUrl(String s) {
		String regEx = "^(http|https|ftp)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&%\\$\\-]+)*@)?((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.[a-zA-Z]{2,4})(\\:[0-9]+)?(/[^/][a-zA-Z0-9\\.\\,\\?\\'\\\\/\\+&%\\$#\\=~_\\-@]*)*$";
		Pattern p = Pattern.compile(regEx);
		Matcher matcher = p.matcher(s);
		return matcher.matches();
	}

	/*生成32位随机字符串*/
	public String getRandomString() {
		String s = UUID.randomUUID().toString();
		//replaceAll() 方法使用给定的参数 replacement 替换字符串所有匹配给定的正则表达式的子字符串
		String str = s.replaceAll("-", "");
		return str;
	}
}
