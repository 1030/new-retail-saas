package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.SendMassage;
import com.dy.yunying.api.req.yyz.SendMassageReq;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: send_massage_log
 */
@Mapper
public interface SendMassageMapper extends BaseMapper<SendMassage> {

	List<String>  selectGameAppointmentList(SendMassageReq req);

	String selectMessage(SendMassageReq req);

}


