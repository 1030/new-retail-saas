package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.WanGameChannelInfo;

import java.util.List;
import java.util.Map;

/**
 * 请使用{@link WanGameChannelInfoMapper}中的方法，如果没有相应的方法也请在该类中扩展！！！！
 */
@Deprecated
public interface WanGameChannelInfoDao {

	List<WanGameChannelInfo> selectWanGameChannelInfoListByCond(Map<String, Object> map);

	int selectChlNumByChannels(Map<String, Object> map);

}
