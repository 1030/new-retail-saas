package com.dy.yunying.biz.service.datacenter.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.dy.yunying.api.datacenter.dto.AdRecoveryDto;
import com.dy.yunying.api.datacenter.dto.RetentionDto;
import com.dy.yunying.api.entity.ChannelManageDO;
import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.datacenter.vo.RetentionVo;
import com.dy.yunying.api.entity.WanGameDO;
import com.dy.yunying.biz.dao.datacenter.impl.RetentionDao;
import com.dy.yunying.biz.service.datacenter.RetentionService;
import com.dy.yunying.biz.service.manage.*;
import com.google.api.client.util.Lists;
import com.pig4cloud.pig.api.feign.RemoteAccountService;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName RetentionServiceImpl
 * @Description done
 * @Author nieml
 * @Time 2021/6/21 16:58
 * @Version 1.0
 **/
@Service(value = "dcRetentionServiceImpl")
@Slf4j
public class RetentionServiceImpl implements RetentionService {

	@Autowired
	private RetentionDao retentionDaoNew;
	@Autowired
	private AdRoleUserService adRoleUserService;

	@Autowired
	private RemoteAccountService remoteAccountService;

	@Autowired
	private ParentGameService parentGameService;

	@Autowired
	private GameChannelPackService gameChannelPackService;

	@Autowired
	private ChannelManageService channelManageService;

	@Autowired
	private GameService gameService;

	@Override
	public R<Long> countDataTotal(RetentionDto req) {
		try {
			this.managerParam(req);

			Long count = retentionDaoNew.countDataTotal(req);
			return R.ok(count);
		} catch (Exception e) {
			log.error("countDataTotal:[{}]", e);
			return R.failed("留存总记录数查询失败");
		}

	}

	@Override
	public R selectRetentionData(RetentionDto req) {
		try {
			this.managerParam(req);

			List<RetentionVo> retentionVos = retentionDaoNew.selectRetentionData(req);
			//获取游戏名称
			deal(retentionVos);
			return R.ok(retentionVos);
		} catch (Exception e) {
			log.error("selectRetentionData:[{}]", e);
			return R.failed("留存报表查询失败");
		}
	}

	private void managerParam(RetentionDto req){
		// 判断是否管理员
		if (SecurityUtils.getRoles().contains(1)) {
			req.setIsSys(1);
		} else {
			// 当前用户
			final PigUser user = SecurityUtils.getUser();
			List<Integer> ownerRoleUserIds = adRoleUserService.getOwnerRoleUserIds();
			// 当前账号及管理的账号
			List<Integer> roleUserIdList = new ArrayList<>();
			req.setRoleUserIdList(roleUserIdList);
			roleUserIdList.add(user.getId());
			roleUserIdList.addAll(ownerRoleUserIds);

			// 授权的广告账户
			List<String> roleAdAccountList = new ArrayList<>();
			req.setRoleAdAccountList(roleAdAccountList);
			final List<String> accountList2 = adRoleUserService.getThrowAccountList();
			if (ObjectUtils.isNotEmpty(accountList2)) {
				roleAdAccountList.addAll(accountList2);
			}
		}
	}

	private void deal(List<RetentionVo> retentionVos) {
		if (CollectionUtils.isNotEmpty(retentionVos)) {

			//分包编码名称
			List<String> appChlArr = new ArrayList<>();
			// 主渠道cdoe
			List<String> channelCodeArr = Lists.newArrayList();

			if (ObjectUtils.isNotEmpty(retentionVos)) {
				retentionVos.forEach(dataAccount -> {
					String appchl = dataAccount.getAppchl();
					if (StringUtils.isNotBlank(appchl)) {
						appChlArr.add(appchl);
					}
					//主渠道
					String channelCode = dataAccount.getParentchl();
					if (StringUtils.isNotBlank(channelCode)) {
						channelCodeArr.add(channelCode);
					}
				});
			}
			List<String> distinctValue = appChlArr.stream().distinct().collect(Collectors.toList());
			List<Map<String, String>> appChlArrList = gameChannelPackService.queryAppChlName(distinctValue);
			Map<String, String> mapappChl = new HashMap<>();
			if (ObjectUtils.isNotEmpty(appChlArrList)) {
				appChlArrList.forEach(appChlData -> mapappChl.put(String.valueOf(appChlData.get("code")), appChlData.get("codeName")));
			}
			// 渠道list
			Map<String, String> channelMap = new HashMap<>();
			if (channelCodeArr.size() > 0) {
				QueryWrapper<ChannelManageDO> wrapper = new QueryWrapper();
				wrapper.in("chncode", channelCodeArr.stream().distinct().collect(Collectors.toList()));
				wrapper.eq("pid", 0);
				List<ChannelManageDO> channelManageDOList = channelManageService.list(wrapper);
				if (ObjectUtils.isNotEmpty(channelManageDOList)) {
					channelMap = channelManageDOList.stream().collect(Collectors.toMap(ChannelManageDO::getChncode, ChannelManageDO::getChnname));
				}
			}

			for (RetentionVo retentionVo : retentionVos) {
				try {
					final Long pgid = retentionVo.getPgid();

					final Long gameid = retentionVo.getGameid();

					final ParentGameDO parentGameDO = parentGameService.getByPK(pgid);
					String parentGameName = "-";
					if (parentGameDO != null) {
						parentGameName = parentGameDO.getGname();
					}
					retentionVo.setParentGameName(parentGameName);

					final WanGameDO wanGameDO = gameService.selectVOByPK(gameid);
					String gameName = "-";
					if (wanGameDO != null) {
						gameName = wanGameDO.getGname();
					}
					retentionVo.setGname(gameName);


					// 分包编码名称
					String appchl = retentionVo.getAppchl();
					retentionVo.setAppchl(mapappChl.get(appchl) == null ? appchl : mapappChl.get(appchl));

					String parentchl = retentionVo.getParentchl();
					retentionVo.setParentchlName(channelMap.get(parentchl) == null ? parentchl : channelMap.get(parentchl));

				} catch (Exception e) {
					log.error("数据处理异常", e);
				}
			}
		}
	}

}
