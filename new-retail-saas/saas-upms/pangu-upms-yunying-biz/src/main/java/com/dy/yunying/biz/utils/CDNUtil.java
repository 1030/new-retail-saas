package com.dy.yunying.biz.utils;

import com.alibaba.fastjson.JSON;
import com.dy.yunying.biz.config.CDNConfig;
import com.huaweicloud.sdk.cdn.v1.CdnClient;
import com.huaweicloud.sdk.cdn.v1.model.CreateRefreshTasksRequest;
import com.huaweicloud.sdk.cdn.v1.model.CreateRefreshTasksResponse;
import com.huaweicloud.sdk.cdn.v1.model.RefreshTaskRequest;
import com.huaweicloud.sdk.cdn.v1.model.RefreshTaskRequestBody;
import com.huaweicloud.sdk.cdn.v1.region.CdnRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

/**
 * CDN工具类
 *
 * @Author: hjl
 * @Date: 2020-9-19 10:51:18
 */
@Slf4j
public class CDNUtil {

	/**
	 * 使用ThreadLocal实现线程内的单例
	 *
	 * @return
	 */
	private static final ThreadLocal<CdnClient> threadLocalInstanceThreadLocal = ThreadLocal.withInitial(() -> {
		return clientInit();
//		return clientInitTest();
	});

	public static CdnClient getClient() {
		/**
		 * 直接从threadLocalInstanceThreadLocal对象中get
		 * 那这个"单例模式"就写完了
		 * 注意它是带引号的单例
		 */
		return threadLocalInstanceThreadLocal.get();
	}

	private static CdnClient clientInit() {
		ICredential auth = new GlobalCredentials()
				.withAk(CDNConfig.ak)
				.withSk(CDNConfig.sk);

		CdnClient client = CdnClient.newBuilder()
				.withCredential(auth)
				.withRegion(CdnRegion.valueOf("cn-north-1"))
				.build();
		return client;
	}

	private static CdnClient clientInitTest() {
		String ak = "Y9TOKUG1KUQNWHPSO7LA";
		String sk = "oqjIxNr8xqJFvP5rBODfwUIWG2wk6WngkHg7lGFB";
		ICredential auth = new GlobalCredentials()
				.withAk(ak)
				.withSk(sk);

		CdnClient client = CdnClient.newBuilder()
				.withCredential(auth)
				.withRegion(CdnRegion.valueOf("cn-north-1"))
				.build();
		return client;
	}

	/**
	 * 创建CDN刷新缓存任务
	 *
	 * @param refreshType
	 * @param refreshTaskUrls
	 */
	public static void createRefreshTask(RefreshTaskRequestBody.TypeEnum refreshType, List<String> refreshTaskUrls) {
		CreateRefreshTasksRequest request = new CreateRefreshTasksRequest();
		RefreshTaskRequest requestBody = new RefreshTaskRequest();
		request.setBody(requestBody);
		RefreshTaskRequestBody refreshTask = new RefreshTaskRequestBody();
		requestBody.setRefreshTask(refreshTask);
		refreshTask.withType(refreshType).withUrls(refreshTaskUrls);
		request.withEnterpriseProjectId(CDNConfig.enterprise_project_id);
		try {
			CreateRefreshTasksResponse response = getClient().createRefreshTasks(request);
			log.info("refreshTask : {}", JSON.toJSONString(response));
			if (response != null && 200 == response.getHttpStatusCode()) {
				log.info("CreateRefreshTask success...");
			} else {
				log.error("CreateRefreshTask fail...");
			}
		} catch (ConnectionException e) {
			log.error("CreateRefreshTask error", e);
		}
	}

	public static void main(String[] args) {
		List<String> urls = Arrays.asList("http://apk.3367.com/file/test20210617.txt");
		createRefreshTask(RefreshTaskRequestBody.TypeEnum.FILE, urls);
	}
}
