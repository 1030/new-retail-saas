package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.datacenter.vo.NoticeDataReportVO;
import com.dy.yunying.api.dto.hongbao.PopupNoticeDto;
import com.dy.yunying.api.entity.hongbao.PopupNotice;
import com.dy.yunying.api.req.NoticeDataReq;
import com.dy.yunying.api.req.hongbao.NoticeSortReq;
import com.dy.yunying.api.req.hongbao.OnlineStatusReq;
import com.dy.yunying.api.req.hongbao.PopupNoticeReq;
import com.dy.yunying.api.req.sign.CommonNoticeReq;
import com.dy.yunying.api.req.sign.SelectCommonNoticeReq;
import com.dy.yunying.api.resp.hongbao.CommonNoticeVo;
import com.dy.yunying.api.resp.hongbao.MenusVO;
import com.dy.yunying.api.resp.hongbao.NodeParentData;
import com.dy.yunying.api.resp.hongbao.OptionData;
import com.dy.yunying.api.vo.usergroup.UserGroupStatVO;
import com.dy.yunying.api.vo.hongbao.PopupNoticeVo;
import com.dy.yunying.api.vo.usergroup.UserGroupStatVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * @author yuwenfeng
 * @description: 通过服务接口
 * @date 2021/12/1 17:51
 */
public interface PopupNoticeService extends IService<PopupNotice> {

	R addPopupNotice(PopupNoticeDto popupNoticeDto);

	R updatePopupNotice(PopupNoticeDto popupNoticeDto);

	R deletePopupNotice(PopupNoticeDto popupNoticeDto);

	R<Page<PopupNoticeVo>> queryNotice(PopupNoticeReq popupNoticeReq);

	PopupNotice isExistsOne(PopupNoticeDto popupNoticeDto);


	Page<CommonNoticeVo> selectCommonNoticePage(Page<PopupNotice> page, QueryWrapper<PopupNotice> query, SelectCommonNoticeReq selectReq);

	R addCommonNotice(CommonNoticeReq commonNoticeReq);

	R editCommonNotice(CommonNoticeReq commonNoticeReq);

	R onlineCommonNotice(OnlineStatusReq req);


	void noticeFinish();

	List<String> selectSdkDynamicMenusTitleList();

	List<NoticeDataReportVO> selectNoticeDataSource(NoticeDataReq req);

	R sortCommonNotice(NoticeSortReq req);

	List<NodeParentData> selectChannel(String pId);

	R countNoticeDataTotal(NoticeDataReq req);

	List<OptionData> selectActityListByType(Integer type);

	List<MenusVO> selectSdkDynamicMenusTitleListByParentId(Long id);

	List<UserGroupStatVO> countWithUserGroupIds(List<Long> groupIds);

	List<Long> getNoticeIdsWithUserGroupId(Long groupId);
}
