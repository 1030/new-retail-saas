package com.dy.yunying.biz.service.sign;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.sign.SignTaskConfigDO;
import com.dy.yunying.api.req.sign.SignTaskConfigVO;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 *
 */
public interface SignTaskConfigService extends IService<SignTaskConfigDO> {

	/**
	 * 获取任务配置列表
	 *
	 * @param activityId
	 * @return
	 */
	List<SignTaskConfigDO> taskConfigList(Long activityId);

	/**
	 * 保存任务配置信息
	 *
	 * @param task
	 */
	Pair<Integer, Integer> saveTaskConfig(SignTaskConfigVO task);

	/**
	 * 修改任务配置信息
	 *
	 * @param task
	 */
	Pair<Integer, Integer> editTaskConfig(SignTaskConfigVO task);

	/**
	 * 根据任务ID删除任务配置
	 *
	 * @param taskId
	 */
	void deleteTaskConfigByTaskId(Long taskId);

	/**
	 * 根据活动ID删除任务配置
	 *
	 * @param activityId
	 */
	void deleteTaskConfigByActivityId(Long activityId);

	/**
	 * 保存礼包码信息
	 *
	 * @param giftBagFile 礼包码文件
	 * @param giftType    礼包码类型：1-红包，2-提现，3-签到奖品，4-签到任务
	 * @param objectId    来源ID
	 * @return pair.left=总上传数，pair.right=成功保存数
	 */
	Pair<Integer, Integer> saveSignGiftBag(MultipartFile giftBagFile, Integer giftType, Long objectId);

}
