package com.dy.yunying.biz.controller.datacenter;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.dy.yunying.api.dto.audience.AudiencePackDto;
import com.dy.yunying.api.vo.audience.AudiencePack;
import com.dy.yunying.api.vo.audience.AudiencePackVo;
import com.dy.yunying.biz.service.datacenter.AudiencePackService;
import com.pig4cloud.pig.api.entity.SelfCustomAudience;
import com.pig4cloud.pig.api.feign.RemoteSelfAudienceService;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.util.CommonUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description 人群打包
 * @Author chengang
 * @Date 2022/7/18
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/audience")
public class AudiencePackController {

	private final AudiencePackService audiencePackService;
	private final RemoteSelfAudienceService remoteSelfAudienceService;

	/**
	 * 人群打包列表
	 *
	 * @param dto
	 * @return
	 */
	@PostMapping(value = "page")
	public R<AudiencePack> page(@RequestBody AudiencePackDto dto) {
		R baseCheck = this.baseCheck(dto);
		if (CommonConstants.FAIL.equals(baseCheck.getCode())) {
			return baseCheck;
		}
		final AudiencePack page = audiencePackService.page(dto, true);
		return R.ok(page);
	}

	/**
	 * 总条数单独接口，减少列表渲染时间
	 * @param dto
	 * @return
	 */
	@PostMapping(value = "count")
	public R<AudiencePack> count(@RequestBody AudiencePackDto dto) {
		R baseCheck = this.baseCheck(dto);
		if (CommonConstants.FAIL.equals(baseCheck.getCode())) {
			return baseCheck;
		}
		final AudiencePack page = audiencePackService.count(dto, false);
		return R.ok(page);
	}



	/**
	 * 自有人群打包
	 */
	@PostMapping("/pack")
	public R createPackage(@RequestBody AudiencePackDto dto) {
		if (dto.getType() == null) {
			return R.failed("打包字段不能为空");
		}
		if (StringUtils.isBlank(dto.getPackageName())) {
			return R.failed("人群包名称不能为空");
		}
		R baseCheck = this.baseCheck(dto);
		if (CommonConstants.FAIL.equals(baseCheck.getCode())) {
			return baseCheck;
		}
		// 人群包名称不能重复
		SelfCustomAudience audience = new SelfCustomAudience();
		audience.setName(dto.getPackageName());
		List<SelfCustomAudience> list = remoteSelfAudienceService.getListByDto(SecurityConstants.FROM_IN,audience);
		if (ObjectUtils.isNotEmpty(list)) {
			return R.failed("存在相同人群包名称");
		}
		R result = audiencePackService.pack(dto);
		return result;
	}

	private R baseCheck(AudiencePackDto dto){
		if (StringUtils.isBlank(dto.getStartDate())
				|| StringUtils.isBlank(dto.getEndDate())) {
			return R.failed("请输入查询日期");
		}
		//兼容前端页面
		if (StringUtils.isNotBlank(dto.getStartDate())
			&& dto.getStartDate().indexOf("-") != -1) {
			return R.failed("请输入yyyyMMdd格式的开始日期");
		}
		if (StringUtils.isNotBlank(dto.getEndDate())
				&& dto.getEndDate().indexOf("-") != -1) {
			return R.failed("请输入yyyyMMdd格式的结束日期");
		}
		if (StringUtils.isNotBlank(dto.getStartPay())) {
			if (!CommonUtils.isMoney(dto.getStartPay(),0)) {
				return R.failed("开始充值金额格式不正确");
			}
		}
		if (StringUtils.isNotBlank(dto.getEndPay())) {
			if (!CommonUtils.isMoney(dto.getEndPay(),0)) {
				return R.failed("结束充值金额格式不正确");
			}
		}

		if (StringUtils.isNotBlank(dto.getActiveStartNum())) {
			if (!CommonUtils.isPositiveNumber(dto.getActiveStartNum())) {
				return R.failed("开始活跃天数格式不正确");
			}
		}

		if (StringUtils.isNotBlank(dto.getActiveEndNum())) {
			if (!CommonUtils.isPositiveNumber(dto.getActiveEndNum())) {
				return R.failed("结束活跃天数格式不正确");
			}
		}

		if (StringUtils.isNotBlank(dto.getActiveStartMaxNum())) {
			if (!CommonUtils.isPositiveNumber(dto.getActiveStartMaxNum())) {
				return R.failed("开始最高连续活跃天数格式不正确");
			}
		}

		if (StringUtils.isNotBlank(dto.getActiveEndMaxNum())) {
			if (!CommonUtils.isPositiveNumber(dto.getActiveEndMaxNum())) {
				return R.failed("结束最高连续活跃天数格式不正确");
			}
		}
		return R.ok();
	}


}
