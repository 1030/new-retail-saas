package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbActivityCenterDO;
import com.dy.yunying.api.vo.HbActivityCenterVO;

/**
 * @author MyPC
 * @description 针对表【hb_activity_center(活动中心配置表)】的数据库操作Service
 * @createDate 2022-01-12 19:26:58
 */
public interface HbActivityCenterService extends IService<HbActivityCenterDO> {

	/**
	 * 活动中心分页列表
	 *
	 * @param activityCenter
	 * @return
	 */
	Page<HbActivityCenterDO> getPage(HbActivityCenterVO activityCenter);

	/**
	 * 活动中心保存活动
	 *
	 * @param activityCenter
	 */
	void doSave(HbActivityCenterVO activityCenter);

	/**
	 * 活动中心修改活动
	 *
	 * @param activityCenter
	 */
	void doEdit(HbActivityCenterVO activityCenter);

	/**
	 * 活动中心删除活动
	 *
	 * @param id
	 */
	void doDelete(Long id);

}
