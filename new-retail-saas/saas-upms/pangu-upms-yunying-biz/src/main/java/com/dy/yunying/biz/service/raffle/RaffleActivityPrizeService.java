package com.dy.yunying.biz.service.raffle;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.raffle.GradeSortReq;
import com.dy.yunying.api.entity.raffle.RaffleActivityPrize;
import com.dy.yunying.api.req.raffle.RaffleActivityPrizeReq;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 抽奖奖品配置表服务接口
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
 */
public interface RaffleActivityPrizeService extends IService<RaffleActivityPrize> {

	void saveBatchList(List<RaffleActivityPrize> list);

	R editPrize(RaffleActivityPrizeReq raffleActivityPrizeReq);

	/**
	 * @Description: 活动已上线编辑
	 * @Author: zjz
	 * @Date: 2022/11/10
	 */
	R editOnPrize(RaffleActivityPrizeReq raffleActivityPrizeReq);


	R getGradeSort(GradeSortReq req);

}
