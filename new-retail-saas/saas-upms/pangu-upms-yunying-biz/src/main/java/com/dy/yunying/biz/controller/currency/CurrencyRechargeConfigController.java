package com.dy.yunying.biz.controller.currency;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.CurrencyRechargeConfigDO;
import com.dy.yunying.api.vo.CurrencyRechargeConfigVO;
import com.dy.yunying.api.vo.CurrencyRechargeConfigVO.Edit;
import com.dy.yunying.api.vo.CurrencyRechargeConfigVO.Save;
import com.dy.yunying.biz.service.currency.CurrencyRechargeConfigService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 游豆充值配置
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/currencyRechargeConfig")
@Api(value = "currencyRechargeConfig", tags = "游豆充值配置")
public class CurrencyRechargeConfigController {

	private final CurrencyRechargeConfigService currencyRechargeConfigService;

	/**
	 * 分页列表
	 *
	 * @param config
	 * @return
	 */
	@PostMapping("/page")
	public R<Page<CurrencyRechargeConfigDO>> page(@RequestBody CurrencyRechargeConfigVO config) {
		final Page<CurrencyRechargeConfigDO> page = currencyRechargeConfigService.list(config);
		return R.ok(page);
	}

	/**
	 * 保存
	 *
	 * @param config
	 * @return
	 */
	@PostMapping("/save")
	public R<Void> save(@RequestBody @Validated(Save.class) CurrencyRechargeConfigVO config) throws Exception {
		try {
			currencyRechargeConfigService.save(config);
			return R.ok();
		} catch (IllegalArgumentException e) {
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 修改
	 *
	 * @param config
	 * @return
	 */
	@PostMapping("/edit")
	public R<Void> edit(@RequestBody @Validated(Edit.class) CurrencyRechargeConfigVO config) throws Exception {
		try {
			currencyRechargeConfigService.edit(config);
			return R.ok();
		} catch (IllegalArgumentException e) {
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 删除
	 *
	 * @param id
	 * @return
	 */
	@DeleteMapping("/remove")
	public R<Void> remove(@RequestParam Long id) throws Exception {
		currencyRechargeConfigService.remove(id);
		return R.ok();
	}

}
