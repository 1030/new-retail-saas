package com.dy.yunying.biz.controller.datacenter;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.dy.yunying.api.datacenter.dto.TimeGameRegDto;
import com.dy.yunying.api.datacenter.vo.RegRetentionVO;
import com.dy.yunying.api.datacenter.vo.TimeGameAccountRegVO;
import com.dy.yunying.api.datacenter.vo.TimeGameDeviceRegVO;
import com.dy.yunying.api.vo.hbdata.HbActiveCashConfigVo;
import com.dy.yunying.api.vo.hbdata.HbActiveHbDrawVo;
import com.dy.yunying.api.vo.hbdata.HbActiveVisitDetailVo;
import com.dy.yunying.api.vo.hbdata.HbFloatClickVo;
import com.dy.yunying.api.vo.hbdata.HbInvitePvVo;
import com.dy.yunying.api.vo.hbdata.HbMessageVo;
import com.dy.yunying.biz.service.datacenter.TimeGameRegService;
import com.dy.yunying.biz.utils.DateUtils;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

/**
 * dogame分时游戏注册表
 *
 * @author hma
 * @date 2022/8/19 10:29
 */
@Slf4j
@RestController("timeGameReg")
@RequestMapping("/dataCenter/timeGameReg")
@RequiredArgsConstructor
public class TimeGameRegController {

	private final TimeGameRegService timeGameRegService;


	/**
	 * 分时游戏注册表总条数
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/count")
	public R countDataTotal(@Valid @RequestBody TimeGameRegDto req) {
		req.setCurrent(null);
		req.setSize(null);
		return timeGameRegService.count(req);
	}


	/**
	 * 分时游戏注册表数据列表
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/list")
	public R<List<TimeGameDeviceRegVO>> list(@Valid @RequestBody TimeGameRegDto req) {
		R<List<TimeGameDeviceRegVO>> page = timeGameRegService.page(req);
		if (CommonConstants.FAIL.equals(page.getCode())) {
			return page;
		}

		return page;
	}


	/**
	 * 分时游戏注册表数据汇总
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/collect")
	public R collect(@Valid @RequestBody TimeGameRegDto req) {
		req.setCurrent(null);
		req.setSize(null);
		R<List<TimeGameDeviceRegVO>> collect = timeGameRegService.collect(req);
		if (CommonConstants.FAIL.equals(collect.getCode())) {
			return collect;
		}
		return collect;
	}


	/**
	 * 分时游戏注册表数据导出
	 *
	 * @param req
	 * @return
	 */
	@SysLog("分时游戏注册表数据导出")
	@RequestMapping("/excelExport")
	public void export(@Valid @RequestBody TimeGameRegDto req, HttpServletResponse response, HttpServletRequest request) {
		req.setCurrent(null);
		req.setSize(null);
		//导出默认按照倒叙排列
		req.setKpiValue(null);
		List<TimeGameDeviceRegVO> result = new ArrayList<>();
		String sheetName = "分时游戏注册表";
		try {
			CompletableFuture<List<TimeGameDeviceRegVO>> pageFuture = CompletableFuture.supplyAsync(new Supplier<List<TimeGameDeviceRegVO>>() {
				@Override
				public List<TimeGameDeviceRegVO> get() {
					req.setCycle(1);
					R page = timeGameRegService.page(req);
					if (CommonConstants.SUCCESS.equals(page.getCode())) {
						return (List<TimeGameDeviceRegVO>) page.getData();
					}
					return null;
				}
			});

			CompletableFuture<List<TimeGameDeviceRegVO>> totalFuture = CompletableFuture.supplyAsync(new Supplier<List<TimeGameDeviceRegVO>>() {
				@Override
				public List<TimeGameDeviceRegVO> get() {
					//汇总查询
					req.setCycle(2);
					R page = timeGameRegService.collect(req);
					if (CommonConstants.SUCCESS.equals(page.getCode())) {
						return (List<TimeGameDeviceRegVO>) page.getData();
					}
					return null;
				}
			});
			List<TimeGameDeviceRegVO> list = pageFuture.get();
			List<TimeGameDeviceRegVO> totalList = totalFuture.get();
			if (!CollectionUtils.isEmpty(list)) {
				result.addAll(list);
			}
			if (!CollectionUtils.isEmpty(totalList)) {
				result.addAll(totalList);
			}
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setCharacterEncoding("utf-8");
			String fileName = URLEncoder.encode("分时游戏注册表-" + DateUtils.getCurrentTimeNoUnderline(), "UTF-8").replaceAll("\\+", "%20");
			response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
			response.setHeader("file-name", fileName);
			ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
			Integer dim=req.getDim();
			if(null == dim){
				dim=1 ;
			}
			WriteSheet writeSheet0 =null;
			if(1 == dim){
				//账号维度表头展示内容
				 writeSheet0 = EasyExcel.writerSheet(0, sheetName).head(TimeGameAccountRegVO.class).build();
			}else{
				//设备维度 表头展示内容
				 writeSheet0 = EasyExcel.writerSheet(0, sheetName).head(TimeGameDeviceRegVO.class).build();
			}

			excelWriter.write(result, writeSheet0);
			excelWriter.finish();
		} catch (Exception e) {
			log.error("分时游戏注册表数据导出异常, 异常原因：{}", e.toString());
			throw new BusinessException("导出异常");
		}


	}


}
