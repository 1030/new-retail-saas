package com.dy.yunying.biz.service.hongbao;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbGiftBag;
import com.dy.yunying.api.req.hongbao.GiftCodeReq;
import com.pig4cloud.pig.common.core.util.R;


/**
 * 红包礼包码
 * @author  chenxiang
 * @version  2021-10-25 14:20:24
 * table: hb_gift_bag
 */
public interface HbGiftBagService extends IService<HbGiftBag> {

	R selectGroupCodeLists(GiftCodeReq giftCodeReq);
	
}


