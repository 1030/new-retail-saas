package com.dy.yunying.biz.service.manage.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.dy.yunying.api.entity.WanProhibitLog;
import com.dy.yunying.biz.service.manage.WanProhibitLogService;
import com.dy.yunying.biz.dao.manage.WanProhibitLogMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class WanProhibitLogServiceImpl extends ServiceImpl<WanProhibitLogMapper, WanProhibitLog>
    implements WanProhibitLogService{

}




