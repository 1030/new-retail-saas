package com.dy.yunying.biz.service.datacenter.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dy.yunying.api.datacenter.dto.UserBehaviorDto;
import com.dy.yunying.api.datacenter.vo.*;
import com.dy.yunying.api.entity.ChannelManageDO;
import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.entity.WanGameDO;
import com.dy.yunying.biz.dao.datacenter.impl.UserBehaviorDataDao;
import com.dy.yunying.biz.dao.manage.ChannelManageDOMapper;
import com.dy.yunying.biz.dao.manage.ParentGameDOMapper;
import com.dy.yunying.biz.dao.manage.WanGameDOMapper;
import com.dy.yunying.biz.service.datacenter.UserBehaviorService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @ClassName UserBehaviorServiceImpl
 * @Description done
 * @Author leisw
 * @Time 2022/8/19 14:55
 * @Version 1.0
 **/

@Service(value = "dcUserBehaviorServiceImpl")
@Slf4j
@RequiredArgsConstructor
public class UserBehaviorServiceImpl implements UserBehaviorService {

	@Autowired
	private UserBehaviorDataDao userBehaviorDataDao;

	private final ChannelManageDOMapper channelManageDOMapper;

	private final ParentGameDOMapper parentGameDOMapper;

	private final WanGameDOMapper wanGameDOMapper;



	@Override
	public R count(UserBehaviorDto req) {
		Long count;
		try {
			count = userBehaviorDataDao.count(req);
		} catch (Exception e) {
			log.error("countDataTotal:[{}]", e);
			return R.failed("用户行为数据查询失败");
		}
		return R.ok(count);
	}

	@Override
	public R selectUserBehavior(UserBehaviorDto req) {
		try {

			List<UserBehaviorVo> resultData = userBehaviorDataDao.list(req); // 查询数据
			Set<String> set = resultData.stream().map(UserBehaviorVo::getIp).collect(Collectors.toSet());
			List<UserBehaviorDimIpVo> dimIP = userBehaviorDataDao.getDimIP(StringUtils.join(set, ","));
			Map<String, String>  ipArea = dimIP.stream().distinct().collect(Collectors.toMap(UserBehaviorDimIpVo::getIp, UserBehaviorDimIpVo::getArea, (key1, key2) -> key1));
			resultData.forEach(result -> {
				if(StringUtils.isNotBlank(ipArea.get(result.getIp()))){
					result.setArea(ipArea.get(result.getIp()));
				}else {
					result.setArea(ipArea.get("-"));
				}
			});
			deal(resultData, req);
			return R.ok(resultData);
		}catch (Exception e){
			log.error("查询用户明细数据失败:{}",e);
			return R.failed("查询用户明细数据失败");
		}
	}

	@Override
	public R getUserBehavior(UserBehaviorDto req) {
		List<UserBehaviorScreenVo> userBehavior = userBehaviorDataDao.getUserBehavior(req);
		return R.ok(userBehavior);
	}

	@Override
	public R getOsName(UserBehaviorDto req) {
		List<UserBehaviorOsNameVo> osName = userBehaviorDataDao.getOsName(req);

		return R.ok(osName);
	}


	private void deal(List<UserBehaviorVo> resultData,UserBehaviorDto req) {
		if (CollectionUtils.isEmpty(resultData)) {
			return;
		}
		// 填充主渠道名称
		final List<String> parentCodeList = resultData.stream().map(UserBehaviorVo::getParentchl).distinct().filter(StringUtils::isNotEmpty).collect(Collectors.toList());
		final Map<String, String> parentMapping = parentCodeList.isEmpty() ? Collections.emptyMap() : channelManageDOMapper.selectList(Wrappers.<ChannelManageDO>lambdaQuery().
				select(ChannelManageDO::getChncode, ChannelManageDO::getChnname).
				in(ChannelManageDO::getChncode, parentCodeList).
				eq(ChannelManageDO::getPid, 0)).
				stream().collect(Collectors.toMap(ChannelManageDO::getChncode, ChannelManageDO::getChnname, (k1, k2) -> k1));
		resultData.forEach(e -> e.setParentchlName(StringUtils.defaultIfBlank(parentMapping.get(e.getParentchl()), "-")));

		// 填充子包渠道名称
		final List<String> appChlList = resultData.stream().map(UserBehaviorVo::getChl).distinct().filter(StringUtils::isNotEmpty).collect(Collectors.toList());
		final Map<String, String> appMapping = appChlList.isEmpty() ? Collections.emptyMap() : channelManageDOMapper.selectList(Wrappers.<ChannelManageDO>lambdaQuery()
				.select(ChannelManageDO::getChncode, ChannelManageDO::getChnname)
				.in(ChannelManageDO::getChncode, appChlList))
				//codeName 存在 null 值会导致NPE
//					.stream().collect(Collectors.toMap(WanGameChannelInfoDO::getChl, WanGameChannelInfoDO::getCodeName, (k1, k2) -> k1));
				.stream().collect(Collectors.toMap(ChannelManageDO::getChncode, ChannelManageDO::getChnname, (k1, k2) -> k1));

		resultData.forEach(e -> e.setAppchlName(StringUtils.defaultIfBlank(appMapping.get(e.getChl()), "-")));

		// 填充主游戏名称
		final List<Long> PgcodeList = resultData.stream().map(UserBehaviorVo::getPgid).distinct().filter(Objects::nonNull).collect(Collectors.toList());
		final Map<Long, String> pgmapping = PgcodeList.isEmpty() ? Collections.emptyMap() : parentGameDOMapper.selectList(Wrappers.<ParentGameDO>lambdaQuery().select(ParentGameDO::getId, ParentGameDO::getGname).in(ParentGameDO::getId, PgcodeList)).stream().collect(Collectors.toMap(ParentGameDO::getId, ParentGameDO::getGname, (k1, k2) -> k1));
		resultData.forEach(e -> e.setParentGameName(StringUtils.defaultIfBlank(pgmapping.get(e.getPgid()), "-")));

		// 填充子游戏名称
		final List<Long> gamecCodeList = resultData.stream().map(UserBehaviorVo::getGameid).distinct().filter(Objects::nonNull).collect(Collectors.toList());
		final Map<Long, String> mapping = gamecCodeList.isEmpty() ? Collections.emptyMap() : wanGameDOMapper.selectList(Wrappers.<WanGameDO>lambdaQuery().select(WanGameDO::getId, WanGameDO::getGname).in(WanGameDO::getId, gamecCodeList)).stream().collect(Collectors.toMap(WanGameDO::getId, WanGameDO::getGname, (k1, k2) -> k1));
		resultData.forEach(e -> e.setGname(StringUtils.defaultIfBlank(mapping.get(e.getGameid()), "-")));

	}

}
