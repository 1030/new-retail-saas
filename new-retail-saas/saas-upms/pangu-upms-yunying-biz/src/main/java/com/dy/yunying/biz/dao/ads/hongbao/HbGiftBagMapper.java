package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.dy.yunying.api.entity.hongbao.HbGiftBag;
import com.dy.yunying.api.req.hongbao.GiftCodeReq;
import com.dy.yunying.api.resp.hongbao.GiftCodeData;
import com.dy.yunying.api.resp.hongbao.GiftData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 红包礼包码
 *
 * @author chenxiang
 * @version 2021-10-25 14:20:24
 * table: hb_gift_bag
 */
@Mapper
public interface HbGiftBagMapper extends BaseMapper<HbGiftBag> {

	//单表基本CURD操作BaseMapper已经存在,请尽量不用重载父类已定义的方法

	List<GiftData> selectGroupCodeList(@Param("type") int type, @Param("usable") int usable, @Param("ids") List<Long> ids);

	List<GiftCodeData> selectGroupCodeLists(GiftCodeReq giftCodeReq);

}


