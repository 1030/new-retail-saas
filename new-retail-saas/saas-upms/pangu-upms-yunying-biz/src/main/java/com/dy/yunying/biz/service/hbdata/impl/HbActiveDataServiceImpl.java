package com.dy.yunying.biz.service.hbdata.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dy.yunying.api.entity.hongbao.HbActivity;
import com.dy.yunying.api.entity.hongbao.HbRedpackConfig;
import com.dy.yunying.api.req.hongbao.HbActiveDataReq;
import com.dy.yunying.api.vo.hbdata.*;
import com.dy.yunying.biz.dao.ads.hongbao.HbActivityMapper;
import com.dy.yunying.biz.dao.ads.hongbao.HbRedpackConfigMapper;
import com.dy.yunying.biz.dao.hbdataclickhouse.impl.HbActiveDataDao;
import com.dy.yunying.biz.service.hbdata.HbActiveDataService;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.common.core.util.CommonUtils;
import com.pig4cloud.pig.common.core.util.R;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 埋点文档：https://x5lyzq06dc.feishu.cn/sheets/shtcnTdmBOPYjvtovPG1H5tGZhg?sheet=haftrx
 * @author ：lile
 * @date ：2021/11/17 17:50
 * @description：
 * @modified By：
 */
@Log4j2
@Service
public class HbActiveDataServiceImpl implements HbActiveDataService {

	@Autowired
	private HbActiveDataDao hbActiveDataDao;

	@Autowired
	private HbRedpackConfigMapper hbRedpackConfigMapper;

	@Autowired
	private HbActivityMapper hbActivityMapper;

	/**
	 * 红包三期前台不展示
	 * 弹窗公告展示
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R popupList(HbActiveDataReq req) {
		List<HbDataNoticeVo> popupList = hbActiveDataDao.popupList(req);
		return R.ok(popupList);
	}

	/**
	 * SDK浮标特效点击数
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R floatClick(HbActiveDataReq req) {
		if (!checkActivityId(req.getActiveId())) {
			return R.failed("活动ID必须为正整数");
		}
		List<HbFloatClickVo> floatList = hbActiveDataDao.floatClick(req);
		return R.ok(floatList);
	}

	/**
	 * 任务模块页PV/UV
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R activeTypePU(HbActiveDataReq req) {
		List<HbActiveTypePuVo> activeTypePUList = hbActiveDataDao.activeTypePU(req);
		return R.ok(activeTypePUList);
	}

	/**
	 * 点击一键提现按钮数，点击提现记录按钮数
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R activeCash(HbActiveDataReq req) {
		List<HbActiveCashVo> activeCashList = hbActiveDataDao.activeCash(req);
		return R.ok(activeCashList);
	}

	/**
	 * 活动规则页
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R activeRule(HbActiveDataReq req) {
		List<HbActiveRuleVo> activeRuleList = hbActiveDataDao.activeRule(req);
		return R.ok(activeRuleList);
	}

	/**
	 * 活动任务完成数
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R activeFinish(HbActiveDataReq req) {
		List<HbActiveFinishVo> activeFinishList = hbActiveDataDao.activeFinish(req);
		return R.ok(activeFinishList);
	}

	/**
	 * 各个档位游戏货币兑换数，各个档位现金提现数
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R activeCashConfig(HbActiveDataReq req) {
		if (!checkActivityId(req.getActiveId())) {
			return R.failed("活动ID必须为正整数");
		}
		List<HbActiveCashConfigVo> activeCashConfigList = hbActiveDataDao.activeCashConfig(req);
		return R.ok(activeCashConfigList);
	}

	/**
	 * 红包三期前台不展示
	 * 点击领取奖励数，代金券领取数
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R activeHbDraw(HbActiveDataReq req) {
		List<HbActiveHbDrawVo> activeHbDrawList = hbActiveDataDao.activeHbDraw(req);
		return R.ok(activeHbDrawList);
	}

	/**
	 * SDK侧边栏红包UI点击数，收益明细按钮点击数
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R activeHbProfit(HbActiveDataReq req) {
		List<HbActiveHbProfitVo> activeHbProfitList = hbActiveDataDao.activeHbProfit(req);
		return R.ok(activeHbProfitList);
	}

	/**
	 * 消息及浮标推送
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R messageList(HbActiveDataReq req) {
		if (!checkActivityId(req.getActiveId())) {
			return R.failed("活动ID必须为正整数");
		}
		List<HbMessageVo> messageHbList = hbActiveDataDao.messageList(req);
		return R.ok(messageHbList);
	}

	/**
	 * 邀请弹框展示数
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R invitePvList(HbActiveDataReq req) {
		if (!checkActivityId(req.getActiveId())) {
			return R.failed("活动ID必须为正整数");
		}
		List<HbInvitePvVo> invitePvHbList = hbActiveDataDao.invitePvList(req);
		return R.ok(invitePvHbList);
	}

	/**
	 * 受邀页面
	 *
	 * @param req
	 * @return
	 */
	@Override
	public R invitedList(HbActiveDataReq req) {
		List<HbInvitedVo> invitedHbList = hbActiveDataDao.invitedList(req);
		return R.ok(invitedHbList);
	}

	@Override
	public R activityTaskDetail(HbActiveDataReq req) {
		if (!checkActivityId(req.getActiveId())) {
			return R.failed("活动ID必须为正整数");
		}
		List<HbActiveHbDrawVo> activeHbDrawList = hbActiveDataDao.activityTaskDetail(req);
		//因是一次性查出的故分页获取领取条件
		if (CollectionUtils.isNotEmpty(activeHbDrawList)) {
			Map<Long,String> redPackMapAll = new HashMap<>();
			List<Long> redpackIds = activeHbDrawList.stream().map(HbActiveHbDrawVo::getRedpackId).collect(Collectors.toList());
			int selectSize = 500;
			int roleRows = redpackIds.size() % selectSize == 0 ? redpackIds.size() / selectSize : redpackIds.size() / selectSize + 1;
			Stream.iterate(0, m -> m + 1).limit(roleRows).forEach(r -> {
				List<Long> tempRedpackIds = redpackIds.stream().skip(r * selectSize).limit(selectSize).collect(Collectors.toList());
				List<HbRedpackConfig> hbRedpackConfigs = hbRedpackConfigMapper.selectList(Wrappers.<HbRedpackConfig>lambdaQuery().in(HbRedpackConfig::getId,tempRedpackIds));
				if (CollectionUtils.isNotEmpty(hbRedpackConfigs)) {
					Map<Long,String> redPackMap = hbRedpackConfigs.stream().collect(Collectors.toMap(HbRedpackConfig::getId,HbRedpackConfig::getTitle));
					redPackMapAll.putAll(redPackMap);
				}
			});
			activeHbDrawList.forEach(item -> {
				item.setRedpackTitle(redPackMapAll.get(item.getRedpackId()) == null ? "" : redPackMapAll.get(item.getRedpackId()));
			});
		}
		return R.ok(activeHbDrawList);
	}

	@Override
	public R activityVisitDetail(HbActiveDataReq req) {
		if (!checkActivityId(req.getActiveId())) {
			return R.failed("活动ID必须为正整数");
		}
		// 过滤脏数控 如：activityType == 0 SQL 已过滤
		// 不是所有的事件都上报了活动类型，故从mysql中获取
		List<HbActiveVisitDetailVo> visitDetailVoList = hbActiveDataDao.activityVisitDetail(req);
		if (CollectionUtils.isNotEmpty(visitDetailVoList)) {
			Map<Long,Integer> activityMapAll = new HashMap<>();
			List<Long> activityIds = visitDetailVoList.stream().map(HbActiveVisitDetailVo::getActivityId).collect(Collectors.toList());
			int selectSize = 500;
			int roleRows = activityIds.size() % selectSize == 0 ? activityIds.size() / selectSize : activityIds.size() / selectSize + 1;
			Stream.iterate(0, m -> m + 1).limit(roleRows).forEach(r -> {
				List<Long> tempActivityIds = activityIds.stream().skip(r * selectSize).limit(selectSize).collect(Collectors.toList());
				List<HbActivity> hbRedpackConfigs = hbActivityMapper.selectListByIds(tempActivityIds);
				if (CollectionUtils.isNotEmpty(hbRedpackConfigs)) {
					Map<Long,Integer> activityMap = hbRedpackConfigs.stream().collect(Collectors.toMap(HbActivity::getId,HbActivity::getActivityType));
					activityMapAll.putAll(activityMap);
				}
			});
			visitDetailVoList.forEach(item -> {
				Integer type = activityMapAll.get(item.getActivityId());
				item.setActivityType(type == null ? 0 : type);
			});
		}
		return R.ok(visitDetailVoList);
	}

	private boolean checkActivityId(String activityId){
		if (StringUtils.isNotBlank(activityId)) {
			return CommonUtils.isPositiveNumber(activityId);
		}
		return true;
	}
}
