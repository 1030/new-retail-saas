package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.PopupNoticeChannel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PopupNoticeChannelMapper extends BaseMapper<PopupNoticeChannel> {

	int insertBatchPopupNoticeChannel(List<PopupNoticeChannel> popupNoticeChannelList);
}
