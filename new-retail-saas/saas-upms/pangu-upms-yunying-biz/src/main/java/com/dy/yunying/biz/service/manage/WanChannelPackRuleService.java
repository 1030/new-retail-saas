package com.dy.yunying.biz.service.manage;


import com.dy.yunying.api.dto.BatchSettingRuleDto;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 分包回传规则表服务接口
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
 */
public interface WanChannelPackRuleService {

	R batchSetting(BatchSettingRuleDto req);
}
