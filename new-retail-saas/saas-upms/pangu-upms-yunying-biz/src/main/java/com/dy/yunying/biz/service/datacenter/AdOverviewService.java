package com.dy.yunying.biz.service.datacenter;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.datacenter.dto.AdOverviewDto2;
import com.dy.yunying.api.datacenter.vo.AdDataAnalysisVO;
import com.dy.yunying.api.datacenter.vo.PlanAttrAnalyseSearchVo;
import com.dy.yunying.api.dto.AdOverviewDto;
import com.dy.yunying.api.vo.AdPlanOverviewVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

@Deprecated
public interface AdOverviewService {

	/**
	 * 广告数据分析表总数查询
	 *
	 * @param req
	 * @return
	 */
	R countDataTotal(AdOverviewDto2 req);

	/**
	 * 广告数据分析表分页列表查询
	 *
	 * @param req
	 * @return
	 */
	R<AdDataAnalysisVO> list(AdOverviewDto2 req);


	/**
	 * 计划属性分析表总数查询
	 */
	R<Long> countDataTotal(PlanAttrAnalyseSearchVo req);

	/**
	 * 计划属性分析表分页列表查询
	 *
	 * @param page
	 * @param searchVo
	 * @return
	 */
	R selectPlanAttrAnalyseReoport(Page page, PlanAttrAnalyseSearchVo searchVo);


	/*
	 *******************************************************************************************************************
	 * 以下为废弃代码
	 *******************************************************************************************************************
	 */

	@Deprecated
	R<AdPlanOverviewVo> selectAdOverviewSource(AdOverviewDto req);

	@Deprecated
	List<AdPlanOverviewVo> excelPlanStatistic(AdOverviewDto req);

}