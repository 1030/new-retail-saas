package com.dy.yunying.biz.controller.hongbao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.dto.hongbao.PopupNoticeDto;
import com.dy.yunying.api.entity.hongbao.PopupNotice;
import com.dy.yunying.api.req.hongbao.PopupNoticeReq;
import com.dy.yunying.api.vo.hongbao.PopupNoticeVo;
import com.dy.yunying.biz.service.hongbao.ActivityEventTypeService;
import com.dy.yunying.biz.service.hongbao.PopupNoticeService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author ：lile
 * @date ：2021/10/26 17:45
 * @description：
 * @modified By：
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/notice")
@Api(value = "notice", tags = "公告弹窗")
@Slf4j
public class PopupNoticeController {

	@Autowired
	private PopupNoticeService popupNoticeService;

	@Autowired
	private ActivityEventTypeService activityEventTypeService;


	@ApiOperation(value = "新增公告弹窗", notes = "新增公告弹窗")
	@SysLog("新增公告弹窗")
	@PostMapping("/add")
	public R addPopupNotice(@RequestBody PopupNoticeDto popupNoticeDto) {
		try {
			// 校验此活动下，相同类型的事件是否唯一的
			PopupNotice popupNoticeVo = popupNoticeService.isExistsOne(popupNoticeDto);
			if (popupNoticeVo != null) {
				return R.failed("存在相同类型的事件,请选择另一个");
			}
			return popupNoticeService.addPopupNotice(popupNoticeDto);
		} catch (Exception e) {
			log.error("公告弹窗-新增通知-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "编辑公告弹窗", notes = "编辑公告弹窗")
	@SysLog("编辑公告弹窗")
	@PostMapping("/update")
	public R updatePopupNotice(@RequestBody PopupNoticeDto popupNoticeDto) {
		try {
			return popupNoticeService.updatePopupNotice(popupNoticeDto);
		} catch (Exception e) {
			log.error("公告弹窗-新增通知-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "删除公告弹窗", notes = "删除公告弹窗")
	@SysLog("删除公告弹窗")
	@PostMapping("/delete")
	public R deletePopupNotice(@RequestBody PopupNoticeDto popupNoticeDto) {
		try {
			if (Objects.isNull(popupNoticeDto.getId())) {
				return R.failed("未获取到要删除的公告id");
			}
			return popupNoticeService.deletePopupNotice(popupNoticeDto);
		} catch (Exception e) {
			log.error("公告弹窗-新增通知-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	// 查询通知记录
	@ApiOperation(value = "公告弹窗分页查询", notes = "公告弹窗分页查询")
	@PostMapping("/queryNotice")
	public R<Page<PopupNoticeVo>> queryNotice(@RequestBody PopupNoticeReq popupNoticeReq) {
		try {
			return popupNoticeService.queryNotice(popupNoticeReq);
		} catch (Exception e) {
			log.error("活动通知分页查询-分页查询-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}


}
