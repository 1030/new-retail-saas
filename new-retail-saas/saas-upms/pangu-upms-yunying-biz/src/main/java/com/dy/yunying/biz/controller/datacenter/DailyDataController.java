package com.dy.yunying.biz.controller.datacenter;

import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.datacenter.dto.DailyDataDto;
import com.dy.yunying.api.datacenter.vo.DailyDataVO;
import com.dy.yunying.biz.service.datacenter.DailyDataService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

/**
 * 每日运营数据
 * @author sunyq
 * @date 2022/6/21 10:29
 */
@Slf4j
@RestController("dailyData")
@RequestMapping("/dataCenter/dailyData")
@RequiredArgsConstructor
public class DailyDataController {

	private final DailyDataService dailyDataService;


	/**
	 * 广告数据分析报表
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/count")
	public R countDataTotal(@Valid @RequestBody DailyDataDto req) {
		return dailyDataService.count(req);
	}


	/**
	 * 每日运营数据列表
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/list")
	public R<List<DailyDataVO>> list(@Valid @RequestBody DailyDataDto req) {
		R<List<DailyDataVO>> page = dailyDataService.page(req);
		if (CommonConstants.FAIL.equals(page.getCode())){
			return page;
		}
		deal(page);
		return page;
	}

	private void deal(R<List<DailyDataVO>> page) {
		List<DailyDataVO> data = page.getData();
		if (!CollectionUtils.isEmpty(data)){
			for (DailyDataVO dailyDataVO : data) {
				dailyDataVO.setRegRate(dailyDataVO.getRegRate() + "%");
				dailyDataVO.setNewPayFeeRate(dailyDataVO.getNewPayFeeRate()+"%");
				dailyDataVO.setRetentionRate(dailyDataVO.getRetentionRate()+"%");
				dailyDataVO.setPayFeeRate(dailyDataVO.getPayFeeRate()+"%");
				dailyDataVO.setOldPayFeeRate(dailyDataVO.getOldPayFeeRate()+"%");
			}
		}

	}


	/**
	 * 每日运营数据汇总
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/collect")
	public R collect(@Valid @RequestBody DailyDataDto req) {
		R<List<DailyDataVO>> collect = dailyDataService.collect(req);
		if (CommonConstants.FAIL.equals(collect.getCode())){
			return collect;
		}
		deal(collect);
		return collect;
	}


	/**
	 * 每日运营数据导出
	 *
	 * @param req
	 * @return
	 */
	@ResponseExcel(name = "每日运营报表导出", sheet = "每日运营报表导出")
	@RequestMapping("/excelExport")
	public R export(@Valid @RequestBody DailyDataDto req, HttpServletResponse response, HttpServletRequest request) {
		//
		List<DailyDataVO> result = new ArrayList<>();
		String sheetName = "每日运营数据";
		CompletableFuture<List<DailyDataVO>> pageFuture = CompletableFuture.supplyAsync(new Supplier<List<DailyDataVO>>() {
			@Override
			public List<DailyDataVO> get() {
				R page = dailyDataService.page(req);
				if (CommonConstants.SUCCESS.equals(page.getCode())) {
					return (List<DailyDataVO>) page.getData();
				}
				return null;
			}
		});

		CompletableFuture<List<DailyDataVO>> totalFuture = CompletableFuture.supplyAsync(new Supplier<List<DailyDataVO>>() {
			@Override
			public List<DailyDataVO> get() {
				R page = dailyDataService.collect(req);
				if (CommonConstants.SUCCESS.equals(page.getCode())) {
					return (List<DailyDataVO>) page.getData();
				}
				return null;
			}
		});
		try {
			List<DailyDataVO> list = pageFuture.get();
			List<DailyDataVO> totalList = totalFuture.get();
			if (!CollectionUtils.isEmpty(list)){
				result.addAll(list);
			}
			if (!CollectionUtils.isEmpty(totalList)){
				result.addAll(totalList);
			}
			//
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(result);
			// 为百分比的值拼接百分号
			final String defaultValue = "0.00%";
			ExportUtils.spliceSuffix(resultListMap, Constant.PERCENT, defaultValue, "regRate", "retentionRate", "payFeeRate", "newPayFeeRate",  "oldPayFeeRate");
			ExportUtils.exportExcelData(request, response, String.format("%s-%s.xlsx", sheetName, DateUtils.getCurrentTimeNoUnderline()), sheetName, req.getTitles(), req.getColumns(), resultListMap);
		} catch (Exception e) {
			log.error("每日运营数据导出异常, 异常原因：{}",e.toString());
			throw new BusinessException("导出异常");
		}

		return null;
	}


}
