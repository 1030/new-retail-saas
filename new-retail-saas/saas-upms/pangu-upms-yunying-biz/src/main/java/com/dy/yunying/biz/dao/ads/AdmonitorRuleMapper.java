package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.dto.AdmonitorRuleDto;
import com.dy.yunying.api.entity.AdmonitorRule;

public interface AdmonitorRuleMapper extends BaseMapper<AdmonitorRule> {

	IPage<AdmonitorRule> queryAdmonitorRules(AdmonitorRuleDto admonitorRuleDto);
}
