package com.dy.yunying.biz.service.yyz;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.GameAppointmentScheduleGears;
import com.dy.yunying.api.entity.GameAppointmentScheduleGearsPage;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReward;

import java.util.List;


/**
 * 游戏预约表
 *
 * @author leisw
 * @version 2022-01-17 17:14:29
 * table: game_appointment_schedule_gears_reward
 */
public interface GameAppointmentScheduleGearsRewardService extends IService<GameAppointmentScheduleGearsReward> {

	List<GameAppointmentScheduleGearsReward> selectScheduleGearsReward(GameAppointmentScheduleGearsPage req);

}


