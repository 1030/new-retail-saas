package com.dy.yunying.biz.controller.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.req.WanGameBasePackageReq;
import com.dy.yunying.api.vo.GamePackageManageVO;
import com.dy.yunying.biz.service.manage.WanGameBasePackageService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ 渠道参数配置
 * @author zhuxm
 *
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/package")
@Api(value = "package", tags = "联调管理-渠道参数配置模块")
public class WanGameBasePackageController {

    @Autowired
    private WanGameBasePackageService wanGameBasePackageService;

    /*查询子游戏基础包信息列表*/
	@SysLog("渠道参数配置")
    @RequestMapping(value = "/getBasePackageList")
    public R queryWanGameBasePackageList(@RequestBody WanGameBasePackageReq req){
		IPage<GamePackageManageVO> data = wanGameBasePackageService.selectWanGameBasePackageList(req);
        return R.ok(data);
    }
}
