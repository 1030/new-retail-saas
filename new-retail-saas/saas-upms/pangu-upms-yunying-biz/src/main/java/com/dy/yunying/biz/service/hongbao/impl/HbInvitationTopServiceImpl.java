package com.dy.yunying.biz.service.hongbao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbInvitationTop;
import com.dy.yunying.biz.dao.ads.hongbao.HbInvitationTopMapper;
import com.dy.yunying.biz.service.hongbao.HbInvitationTopService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 手动榜单表
 * @author  chengang
 * @version  2021-10-27 19:21:20
 * table: hb_invitation_top
 */
@Log4j2
@Service("hbInvitationTopService")
@RequiredArgsConstructor
public class HbInvitationTopServiceImpl extends ServiceImpl<HbInvitationTopMapper, HbInvitationTop> implements HbInvitationTopService {
	

	
	
}


