package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.cps.CpsBuckleRate;
import com.dy.yunying.api.cps.CpsBuckleRateReq;
import com.dy.yunying.api.cps.vo.CpsBuckleRateVO;
import com.pig4cloud.pig.common.core.mybatis.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.dy.yunying.biz.domain.CpsBuckleRate
 */
public interface CpsBuckleRateMapper extends BaseMapper<CpsBuckleRate> {
	/**
	 * 列表
	 * @param req
	 * @return
	 */
	IPage<CpsBuckleRateVO> getPage(CpsBuckleRateReq req);
}




