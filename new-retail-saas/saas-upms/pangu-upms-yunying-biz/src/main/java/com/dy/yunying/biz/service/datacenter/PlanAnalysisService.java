package com.dy.yunying.biz.service.datacenter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.datacenter.vo.PlanAttrAnalyseSearchVo;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 计划属性分析表相关方法
 */
public interface PlanAnalysisService {

	/**
	 * 计划属性分析表总数查询
	 *
	 * @param req
	 * @return
	 */
	R<Long> count(PlanAttrAnalyseSearchVo req);

	/**
	 * 计划属性分析表分页列表查询
	 *
	 * @param page
	 * @param searchVo
	 * @return
	 */
	R page(Page page, PlanAttrAnalyseSearchVo searchVo);

}
