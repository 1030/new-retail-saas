package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.GameAppointmentScheduleGears;
import com.dy.yunying.api.entity.GameAppointmentScheduleGearsPage;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReq;
import com.pig4cloud.pig.common.core.mybatis.Page;
import org.apache.ibatis.annotations.Mapper;



/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_gears
 */
@Mapper
public interface GameAppointmentScheduleGearsMapper extends BaseMapper<GameAppointmentScheduleGears> {


	IPage<GameAppointmentScheduleGearsPage> selectScheduleGearsPage(GameAppointmentScheduleGearsReq req);

}


