package com.dy.yunying.biz.controller.datacenter;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.datacenter.dto.AdOverviewDto2;
import com.dy.yunying.api.datacenter.export.AdExportDataAnalysisVO;
import com.dy.yunying.api.datacenter.export.AdExportDataVo;
import com.dy.yunying.biz.service.datacenter.AdAnalysisService;
import com.dy.yunying.biz.service.doris.AdAnalysisDorisService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportAlibabaUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.api.util.StringUtils;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 广告数据分析表相关接口
 */
@Slf4j
@RestController("dcAdOverview")
@RequestMapping("/dataCenter/adOverview")
@RequiredArgsConstructor
public class AdOverviewController {

	private final AdAnalysisService adAnalysisService;

	/**
	 * 广告数据分析报表
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/count")
	public R countDataTotal(@Valid @RequestBody AdOverviewDto2 req) {
//		return adOverviewService.countDataTotal(req);
		return adAnalysisService.count(req);
	}

	/**
	 * 广告数据分析表
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/list")
	public R list(@Valid @RequestBody AdOverviewDto2 req) {
//		return adOverviewService.list(req);
		return adAnalysisService.page(req);
	}

	/**
	 * 广告数据分析报表导出
	 *
	 * @param req
	 * @return
	 */
	@ResponseExcel(name = "广告数据分析报表导出", sheet = "广告数据分析报表导出")
	@RequestMapping("/excelPlanStatistic")
	public R excelPlanStatistic(@Valid @RequestBody AdOverviewDto2 req, HttpServletResponse response, HttpServletRequest request) {
		try {
			List resultList = new ArrayList();
			String sheetName = "广告数据分析报表";
			//临时采用循环查询解决数据过大的问题
			Long current = 1L;
			while (true) {
				req.setSize(1500L);
				req.setCurrent(current);
				List tmpList = (List) adAnalysisService.page(req).getData();
				if (CollectionUtils.isEmpty(tmpList)) {
					break;
				}
				resultList.addAll(tmpList);
				current++;
			}
			// 查询汇总行-汇总只有一条
			if (StringUtils.isNotBlank(req.getQueryColumn()) || 4 != req.getCycleType()) {
				req.setSize(1L);
				req.setCurrent(1L);
				req.setCycleType(4);
				req.setQueryColumn(Constant.EMPTTYSTR);
				R collect = adAnalysisService.page(req);
				resultList.addAll((List) collect.getData());
			}

			String fileName = URLEncoder.encode("广告数据分析报表-"+ DateUtils.getCurrentTimeNoUnderline(), "UTF-8").replaceAll("\\+", "%20");

			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(resultList);

			ExportUtils.spliceSuffix(resultListMap, Constant.PERCENT, Constant.DEFAULT_VALUE,
					"activationRatio", "clickActiveRatio", "clickRatio", "regPayRatio", "regRatio", "retention2Ratio", "payedRetention2", "allRoi",
					"monthRoi", "weekRoi", "roi1", "createRoleRate", "certifiedRate", "activePayRate", "periodPayRate", "periodROI", "totalPayRate","duplicateDeviceRatio");

			List<AdExportDataAnalysisVO> list= new ArrayList<>();

			JSONArray array = JSONArray.parseArray(JSON.toJSONString(resultListMap));

			for (int i = 0; i < array.size(); i++) {
				JSONObject jsonObject = array.getJSONObject(i);
				AdExportDataAnalysisVO detail = JSON.parseObject(String.valueOf(jsonObject), AdExportDataAnalysisVO.class);
				list.add(detail);
			}

			ExportAlibabaUtils.exportExcelData(response,sheetName,fileName,req.getColumns(),list,AdExportDataAnalysisVO.class);

		} catch (BusinessException e) {
			log.error("excelPlanStatistic is error", e);
			throw e;
		} catch (Exception e) {
			log.error("excelPlanStatistic is error", e);
			throw new BusinessException("导出异常");
		}
		return null;
	}

//	/**
//	 * 广告概览数据
//	 *
//	 * @param req
//	 * @return
//	 */
//	@ResponseBody
//	@GetMapping(value = "/getPlanStatistic")
//	public R selectAdOverviewSource(@Valid AdOverviewDto req) {
//		if (req.getCurrent() <= 0 || req.getSize() <= 0) {
//			return R.failed("分页信息不允许为空");
//		}
//		return adOverviewService.selectAdOverviewSource(req);

//	}
//	/**
//	 * 广告概览导出
//	 *
//	 * @param req
//	 * @return
//	 */
//	@SysLog("广告概览导出")
//	@ResponseExcel(name = "广告概览导出", sheet = "广告概览导出")
//	@GetMapping("/excelPlanStatistic")
//	public R excelPlanStatistic(AdOverviewDto req, HttpServletResponse response, HttpServletRequest request) {
//		String fileName = "";
//		try {
//			String sheetName = "广告概览导出";
//			List<AdPlanOverviewVo> adPlanOverviewVoList = adOverviewService.excelPlanStatistic(req);
//			// list对象转listMap
//			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(adPlanOverviewVoList);
//			// 导出
//			fileName = "广告概览导出-" + DateUtils.getCurrentTimeNoUnderline() + ".xlsx";
//			ExportUtils.exportExcelData(request, response, fileName, sheetName, req.getTitles(), req.getColumns(), resultListMap);
//		} catch (Exception e) {
//			log.error("excelPlanStatistic is error", e);
//		}
//
//		return null;

//	}

}
