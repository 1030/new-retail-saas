/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.controller.hongbao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.entity.hongbao.HbCashConfig;
import com.dy.yunying.api.req.hongbao.CashConfigReq;
import com.dy.yunying.api.req.hongbao.SelectCashConfigPageReq;
import com.dy.yunying.api.resp.hongbao.CashConfigVo;
import com.dy.yunying.biz.config.CheckRepeatSubmit;
import com.dy.yunying.biz.service.hongbao.CashConfigService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 红包提现档次表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:18
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/cashConfig")
@Api(value = "cashConfig", tags = "提现档次管理")
@Slf4j
public class CashConfigController {

	private final CashConfigService cashConfigService;

	@ApiOperation(value = "提现档次分页查询", notes = "提现档次分页查询")
	@GetMapping("/selectCashConfigPage")
	@PreAuthorize("@pms.hasPermission('hongbao_hbcashConfig_get')")
	public R<Page<CashConfigVo>> selectCashConfigPage(Page<HbCashConfig> page, @Valid SelectCashConfigPageReq selectReq) {
		try {
			QueryWrapper<HbCashConfig> query = new QueryWrapper<HbCashConfig>();
			if (null != selectReq.getId() && selectReq.getId() > 0) {
				query.eq("activity_id", selectReq.getId());
			}
			if (null != selectReq.getCashType() && selectReq.getCashType() > 0) {
				query.eq("cash_type", selectReq.getCashType());
			}
			query.eq("deleted", Constant.DEL_NO);
			query.orderByAsc("cash_money");
			return R.ok(cashConfigService.selectCashConfigPage(page, query));
		} catch (Exception e) {
			log.error("提现档次管理-分页查询-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@CheckRepeatSubmit
	@ApiOperation(value = "新增红包提现档次", notes = "新增红包提现档次")
	@PostMapping("/addCashConfig")
	@PreAuthorize("@pms.hasPermission('hongbao_hbcashConfig_add')")
	public R addCashConfig(@Valid CashConfigReq cashConfig) {
		try {
			return cashConfigService.addCashConfig(cashConfig);
		} catch (Exception e) {
			log.error("提现档次管理-新增提现档次-接口：{}", e.getMessage());
			return R.failed(e.getMessage());
		}
	}

	@CheckRepeatSubmit
	@ApiOperation(value = "修改红包提现档次", notes = "修改红包提现档次")
	@PostMapping("/editCashConfig")
	@PreAuthorize("@pms.hasPermission('hongbao_hbcashConfig_edit')")
	public R editCashConfig(@Valid CashConfigReq cashConfig) {
		try {
			return cashConfigService.editCashConfig(cashConfig);
		} catch (Exception e) {
			log.error("提现档次管理-修改提现档次-接口：{}", e.getMessage());
			return R.failed(e.getMessage());
		}
	}

	@CheckRepeatSubmit
	@ApiOperation(value = "删除红包提现档次", notes = "删除红包提现档次")
	@SysLog("删除红包提现档次")
	@GetMapping("/delCashConfig")
	@PreAuthorize("@pms.hasPermission('hongbao_hbcashConfig_del')")
	public R delCashConfig(Long cashConfigId) {
		try {
			return cashConfigService.delCashConfig(cashConfigId);
		} catch (Exception e) {
			log.error("提现档次管理-删除红包提现档次-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

}
