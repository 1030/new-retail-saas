package com.dy.yunying.biz.controller.cps;

import com.dy.yunying.api.cps.CpsNodeData;
import com.dy.yunying.api.cps.CpsUserAuthPackReq;
import com.dy.yunying.api.resp.hongbao.NodeData;
import com.dy.yunying.biz.service.cps.CpsUserAuthPackService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * cps分包授权
 * @author sunyq
 * @date 2022/10/11 13:59
 */
@Slf4j
@RestController()
@RequestMapping("/cps/cpsAuthPack")
@RequiredArgsConstructor
public class CpsUserAuthPackController {

	private final CpsUserAuthPackService cpsUserAuthPackService;


	@ApiOperation(value = "当前用户权限下渠道树形列表", notes = "渠道树形列表")
	@GetMapping("/selectChannelTree")
	public R<List<CpsNodeData>> selectChannelTree(@RequestParam("userId") Integer userId) {
		try {
			return R.ok(cpsUserAuthPackService.selectChannelTree(userId));
		} catch (Exception e) {
			log.error("cps分包授权-渠道树形列表-接口：{}", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "保存分包授权", notes = "保存分包授权")
	@PostMapping("/saveAuthPack")
	public R saveAuthPack(@RequestBody CpsUserAuthPackReq cpsUserAuthPackReq) {
		try {
			//保存分包渠道授权
			if (Objects.isNull(cpsUserAuthPackReq.getUserId())){
				R.failed("分包授权用户不能为空");
			}
			return cpsUserAuthPackService.saveAuthPack(cpsUserAuthPackReq);
		} catch (Exception e) {
			log.error("cps分包授权-渠道树形列表-接口：{}", e);
			return R.failed("接口异常，请联系管理员");
		}
	}
}
