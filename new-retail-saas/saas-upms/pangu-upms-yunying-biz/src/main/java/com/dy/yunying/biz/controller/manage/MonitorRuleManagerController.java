package com.dy.yunying.biz.controller.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.dto.MonitorRuleManagerDto;
import com.dy.yunying.api.entity.MonitorRuleManager;
import com.dy.yunying.api.req.MonitorRuleManagerReq;
import com.dy.yunying.biz.service.ads.MonitorRuleManagerService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Objects;

/**
 * @ClassName MonitorRuleManagerController
 * @Description todo
 * @Author hma
 * @Time 2023/3/20 17:17
 * @Version 1.0
 **/
@Slf4j
@RestController
@RequestMapping("/monitorRule")
@Api(value = "monitorRule", tags = "监控规则管理模块")
public class MonitorRuleManagerController {

	@Autowired
	private MonitorRuleManagerService monitorRuleManagerService;


	@GetMapping(value = "/queryMonitorRuleManager")
	public R<IPage<MonitorRuleManager>> queryMonitorRuleManager(MonitorRuleManagerReq monitorRuleManagerReq) {
		return monitorRuleManagerService.queryMonitorRules(monitorRuleManagerReq);
	}


	@GetMapping(value = "/getMonitorRuleManagerById")
	public R<MonitorRuleManager> getMonitorRuleManagerById(Integer id) {
		if(null == id){
			return  R.failed("规则id不允许为空");
		}
		return monitorRuleManagerService.getMonitorRuleManager(id);
	}





	@PostMapping(value = "/saveMonitorRuleManager")
	public R<Boolean> saveMonitorRuleManager(@RequestBody @Valid MonitorRuleManagerDto monitorRuleManagerDto) {
		return monitorRuleManagerService.saveMonitorRule(monitorRuleManagerDto);
	}

	@PostMapping(value = "/updateMonitorRuleManager")
	public R<Boolean> updateMonitorRuleManager(@RequestBody @Valid MonitorRuleManagerDto monitorRuleManagerDto) {
		if(Objects.isNull(monitorRuleManagerDto)){
			return  R.failed("参数不允许为空");
		}

		if(Objects.isNull(monitorRuleManagerDto.getId())){
			return  R.failed("规则id不允许为空");
		}
		return monitorRuleManagerService.updateMonitorRule(monitorRuleManagerDto);
	}

	@PostMapping(value = "/updateRuleStatus")
	public R<Boolean> updateRuleStatus( Integer id, Integer monitorStatus) {
		if(null == id){
			return  R.failed("规则id不允许为空");
		}
		if(null == monitorStatus){
			return  R.failed("监控状态不允许为空");
		}
		return monitorRuleManagerService.updateMonitorRuleStatus(id,monitorStatus);
	}

	@PostMapping(value = "/deleteMonitorRuleManager")
	public R<Boolean> deleteMonitorRuleManager(Integer id) {
		if(null == id){
			return  R.failed("规则id不允许为空");
		}
		return monitorRuleManagerService.deleteMonitorRule(id);
	}



}
