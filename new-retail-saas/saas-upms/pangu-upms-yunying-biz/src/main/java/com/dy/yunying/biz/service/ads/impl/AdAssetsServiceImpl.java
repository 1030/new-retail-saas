package com.dy.yunying.biz.service.ads.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.biz.dao.ads.AdAssetsMapper;
import com.dy.yunying.biz.service.ads.AdAssetsService;
import com.pig4cloud.pig.api.entity.AdAssets;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 广告资产表
 * @author  chenxiang
 * @version  2022-04-06 10:39:18
 * table: ad_assets
 */
@Log4j2
@Service("adAssetsService")
@RequiredArgsConstructor
public class AdAssetsServiceImpl extends ServiceImpl<AdAssetsMapper, AdAssets> implements AdAssetsService {
}


