

package com.dy.yunying.biz.service.usergroup;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.usergroup.Device;


public interface DeviceService extends IService<Device> {
}


