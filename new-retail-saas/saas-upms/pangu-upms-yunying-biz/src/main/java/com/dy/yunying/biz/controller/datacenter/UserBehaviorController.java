package com.dy.yunying.biz.controller.datacenter;

import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.datacenter.dto.HourDataDto;
import com.dy.yunying.api.datacenter.dto.UserBehaviorDto;
import com.dy.yunying.api.datacenter.vo.DailyDataVO;
import com.dy.yunying.api.datacenter.vo.UserBehaviorVo;
import com.dy.yunying.api.enums.HourDataKpiEnum;
import com.dy.yunying.api.req.yyz.GameAppointmentQueryReqList;
import com.dy.yunying.biz.service.datacenter.HourDataService;
import com.dy.yunying.biz.service.datacenter.UserBehaviorService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

/**
 * 用户行为数据
 *
 * @ClassName UserBehaviorController
 * @Description done
 * @Author leisw
 * @Time 2022/8/19 14:48
 * @Version 1.0
 **/

@RestController("user_behavior")
@RequestMapping("/dataCenter/UserBehavior")
@Slf4j
public class UserBehaviorController {


	@Autowired
	private UserBehaviorService userBehaviorService;

	/**
	 * 用户行为数据条数
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/count")
	public R count(@Valid @RequestBody UserBehaviorDto req) {
		try {
			//校验参数
			if (StringUtils.isBlank(req.getStartTime())) {
				R.failed("查询开始日期不允许为空");
			}
			if (StringUtils.isBlank(req.getEndTime())) {
				R.failed("查询结束日期不允许为空");
			}
			return userBehaviorService.count(req);
		} catch (Exception e) {
			log.error("count:{}", e);
			return R.failed(e.getMessage());
		}
	}



	/**
	 * 用户行为数据
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list")
	public R selectUserBehavior(@Valid @RequestBody UserBehaviorDto req) {
		try {
			//校验参数
			if (StringUtils.isBlank(req.getStartTime())) {
				R.failed("查询开始日期不允许为空");
			}
			if (StringUtils.isBlank(req.getEndTime())) {
				R.failed("查询结束日期不允许为空");
			}
			return userBehaviorService.selectUserBehavior(req);
		} catch (Exception e) {
			log.error("selectUserBehavior:{}", e);
			return R.failed(e.getMessage());
		}
	}


	/**
	 * 用户行为数据下拉框
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserBehavior")
	public R getUserBehavior(@Valid @RequestBody UserBehaviorDto req) {
		try {
			return userBehaviorService.getUserBehavior(req);
		} catch (Exception e) {
			log.error("getUserBehavior:{}", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 用户来源下拉框
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getOsName")
	public R getOsName(@Valid @RequestBody UserBehaviorDto req) {
		try {

			return userBehaviorService.getOsName(req);
		} catch (Exception e) {
			log.error("getOsName:{}", e);
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 用户信息导出
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/export")
	public R export(HttpServletResponse response, HttpServletRequest request, @Valid @RequestBody UserBehaviorDto req) {
		//
		List<UserBehaviorVo> result = new ArrayList<>();
		String sheetName = "用户行为记录数据";
		CompletableFuture<List<UserBehaviorVo>> pageFuture = CompletableFuture.supplyAsync(new Supplier<List<UserBehaviorVo>>() {
			@Override
			public List<UserBehaviorVo> get() {
				R page = userBehaviorService.selectUserBehavior(req);
				if (CommonConstants.SUCCESS.equals(page.getCode())) {
					return (List<UserBehaviorVo>) page.getData();
				}
				return null;
			}
		});

		try {
			List<UserBehaviorVo> list = pageFuture.get();
			if (!CollectionUtils.isEmpty(list)){
				result.addAll(list);
			}
			//
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(result);
			//获得列表名
			String titles = "行为时间,用户行为,手机号,账号ID,设备ID,登录端口,平台来源,游戏,区服,角色名,角色ID,等级,登录IP,地区,机型,系统";
			//获得columns
			String columns = "behaviorTime,userBehaviorName,mobile,username,uuid,osname,appchlName,gname,areaid,rolename,roleid,rolelevel,ip,area,deviceBrand,deviceModel";
			// 为百分比的值拼接百分号
			ExportUtils.exportExcelData(request, response, String.format("%s-%s.xlsx", sheetName, DateUtils.getCurrentTimeNoUnderline()), sheetName,titles,columns, resultListMap);
		} catch (Exception e) {
			log.error("用户行为记录数据导出异常, 异常原因：{}",e.toString());
			return R.failed("数据导出失败");
		}

		return R.ok("数据导出成功");
	}

}
