package com.dy.yunying.biz.service.prize;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.prize.PrizePush;
import com.dy.yunying.api.req.prize.PrizeOnlineOrOfflineReq;
import com.dy.yunying.api.req.prize.PrizePushReq;
import com.dy.yunying.api.vo.usergroup.UserGroupStatVO;
import com.pig4cloud.pig.common.core.util.R;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 奖励推送表
 *
 * @author chenxiang
 * @version 2022-04-25 16:25:27 table: prize_push
 */
public interface PrizePushService extends IService<PrizePush> {

	/**
	 * 分页列表
	 * @param record
	 * @return
	 */
	R getPage(PrizePushReq record);

	/**
	 * 新增-编辑
	 * @param record
	 * @return
	 */
	R createEdit(PrizePushReq record);

	/**
	 * 奖励推送启停
	 * @param req
	 * @return
	 */
	R prizePushStartOrStop(PrizeOnlineOrOfflineReq req);

	/**
	 * 删除奖励推送
	 * @param id
	 * @return
	 */
	R doRemoveById(Long id);

	/**
	 * 复制奖励推送
	 * @param id
	 * @return
	 */
	R copy(Long id);

	List<UserGroupStatVO> countWithUserGroupIds(List<Long> userGroupIds);

	List<Long> getPrizeIdsWithUserGroupId(Long userGroupId);
}
