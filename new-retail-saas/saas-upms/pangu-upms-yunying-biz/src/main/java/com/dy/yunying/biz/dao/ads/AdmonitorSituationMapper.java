package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.AdmonitorSituation;

public interface AdmonitorSituationMapper extends BaseMapper<AdmonitorSituation> {

}
