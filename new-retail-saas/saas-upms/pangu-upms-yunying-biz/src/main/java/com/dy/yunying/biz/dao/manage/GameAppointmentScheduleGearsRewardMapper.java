package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.GameAppointmentScheduleGearsPage;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReward;

import java.util.List;

public interface GameAppointmentScheduleGearsRewardMapper extends BaseMapper<GameAppointmentScheduleGearsReward> {

	List<GameAppointmentScheduleGearsReward> selectScheduleGearsReward(GameAppointmentScheduleGearsPage req);

}