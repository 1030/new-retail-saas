package com.dy.yunying.biz.service.data.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.constant.OsEnum;
import com.dy.yunying.api.constant.TerminalTypeEnum;
import com.dy.yunying.api.dto.AdKanbanDto;
import com.dy.yunying.api.vo.AdKanbanOverviewVo;
import com.dy.yunying.biz.dao.clickhouse3399.AdKanbanMapper;
import com.dy.yunying.biz.service.data.AdKanbanService;
import com.dy.yunying.biz.service.manage.AdRoleUserService;
import com.google.api.client.util.Lists;
import com.pig4cloud.pig.api.feign.RemoteAccountService;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class AdKanbanServiceImpl implements AdKanbanService {
	@Autowired
	private AdKanbanMapper adKanbanMapper;
	@Autowired
	private AdRoleUserService adRoleUserService;
	@Autowired
	private RemoteAccountService remoteAccountService;

	@Override
	public R selectKanbanStatitic(AdKanbanDto req) {
		Page page = new Page();
		page.setSize(req.getSize());
		page.setCurrent(req.getCurrent());

		// 当前用户
		final PigUser user = SecurityUtils.getUser();
		List<Integer> ownerRoleUserIds = adRoleUserService.getOwnerRoleUserIds();
		// 当前账号及管理的账号
		List<Integer> roleUserIdList = new ArrayList<>();
		req.setRoleUserIdList(roleUserIdList);
		roleUserIdList.add(user.getId());
		roleUserIdList.addAll(ownerRoleUserIds);

		// 授权的广告账户
		List<String> roleAdAccountList = new ArrayList<>();
		req.setRoleAdAccountList(roleAdAccountList);
		final List<String> accountList2 = remoteAccountService.getAccountList2(roleUserIdList, SecurityConstants.FROM_IN);
		roleAdAccountList.addAll(accountList2);
		// 判断是否管理员
		if (SecurityUtils.getRoles().contains(1)) {
			req.setIsSys(1);
		}

		// 处理查询列
		dealParam(req);

		IPage<AdKanbanOverviewVo> iPage = adKanbanMapper.selectAdKanbanSourceTable(page, req);

		// 处理没有返回数据 框架会返回All elements are null问题
		String records = String.valueOf(iPage.getRecords());
		if ("[null]".equals(records)) {
			iPage.setRecords(Lists.newArrayList());
			iPage.setTotal(0);
		}

		return R.ok(iPage);

	}


	@Override
	public List<AdKanbanOverviewVo> excelKanbanStatitic(AdKanbanDto req) {
		// 当前用户
		final PigUser user = SecurityUtils.getUser();
		List<Integer> ownerRoleUserIds = adRoleUserService.getOwnerRoleUserIds();
		// 当前账号及管理的账号
		List<Integer> roleUserIdList = new ArrayList<>();
		req.setRoleUserIdList(roleUserIdList);
		roleUserIdList.add(user.getId());
		roleUserIdList.addAll(ownerRoleUserIds);

		// 授权的广告账户
		List<String> roleAdAccountList = new ArrayList<>();
		req.setRoleAdAccountList(roleAdAccountList);
		final List<String> accountList2 = remoteAccountService.getAccountList2(roleUserIdList, SecurityConstants.FROM_IN);
		roleAdAccountList.addAll(accountList2);
		// 判断是否管理员
		if (SecurityUtils.getRoles().contains(1)) {
			req.setIsSys(1);
		}

		// 处理查询列
		dealParam(req);

		List<AdKanbanOverviewVo> adKanbanOverviewList = adKanbanMapper.selectAdKanbanSourceTable(req);

		if (CollectionUtils.isNotEmpty(adKanbanOverviewList) && adKanbanOverviewList.size() > 0) {
			adKanbanOverviewList.forEach(item -> {
				if (StringUtils.isNotBlank(item.getOs())) {
					item.setOsName(OsEnum.getName(Integer.parseInt(item.getOs())));
				}
			});
		}

		return adKanbanOverviewList;
	}

	public void dealParam(AdKanbanDto req) {
		if (StringUtils.isNotBlank(req.getQueryColumn())) {
			String[] queryColumns = req.getQueryColumn().split(",");
			String querySql = "";
			String groupSql = "";
			for (int i = 0; i < queryColumns.length; i++) {
				if ("os".equals(queryColumns[i])) {
					querySql += "os os,";
					groupSql += "os,";
				} else if ("pgid".equals(queryColumns[i])) {
					querySql += "pgid pgid,pgname pgame,sharing sharing,";
					groupSql += "pgid,pgname,sharing,";
				} else if ("gid".equals(queryColumns[i])) {
					querySql += "gameid gid,gname sgame,";
					groupSql += "gameid,gname,";
				} else if ("deptId".equals(queryColumns[i])) {
					querySql += "dept_id deptId,dept_name deptName,";
					groupSql += "dept_id,dept_name,";
				}
			}
			req.setQuerySql(querySql);
			req.setGroupSql("group by " + groupSql.substring(0, groupSql.length() - 1));
		}
	}
}