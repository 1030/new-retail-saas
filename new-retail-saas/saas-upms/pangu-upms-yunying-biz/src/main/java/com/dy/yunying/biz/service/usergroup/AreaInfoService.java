

package com.dy.yunying.biz.service.usergroup;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.usergroup.AreaInfo;


public interface AreaInfoService extends IService<AreaInfo> {
}


