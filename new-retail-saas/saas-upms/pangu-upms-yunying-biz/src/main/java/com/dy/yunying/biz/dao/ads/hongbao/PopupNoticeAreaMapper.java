package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.PopupNoticeArea;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PopupNoticeAreaMapper extends BaseMapper<PopupNoticeArea> {

	int insertBatchPopupNoticeArea(List<PopupNoticeArea> popupNoticeAreaList);

}
