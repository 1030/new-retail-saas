package com.dy.yunying.biz.service.prize.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.prize.PrizePushArea;
import com.dy.yunying.biz.dao.ads.prize.PrizePushAreaMapper;
import com.dy.yunying.biz.service.prize.PrizePushAreaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 奖励与区服关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:36
 * table: prize_push_area
 */
@Log4j2
@Service("prizePushAreaService")
@RequiredArgsConstructor
public class PrizePushAreaServiceImpl extends ServiceImpl<PrizePushAreaMapper, PrizePushArea> implements PrizePushAreaService {
}


