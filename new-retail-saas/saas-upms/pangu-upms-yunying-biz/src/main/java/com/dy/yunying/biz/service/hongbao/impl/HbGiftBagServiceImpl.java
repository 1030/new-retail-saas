package com.dy.yunying.biz.service.hongbao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbGiftBag;
import com.dy.yunying.api.req.hongbao.GiftCodeReq;
import com.dy.yunying.api.resp.hongbao.GiftCodeData;
import com.dy.yunying.biz.dao.ads.hongbao.HbGiftBagMapper;
import com.dy.yunying.biz.service.hongbao.HbGiftBagService;
import com.mysql.cj.xdevapi.Collection;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 红包礼包码
 * @author  chenxiang
 * @version  2021-10-25 14:20:24
 * table: hb_gift_bag
 */
@Log4j2
@Service("hbGiftBagService")
@RequiredArgsConstructor
public class HbGiftBagServiceImpl extends ServiceImpl<HbGiftBagMapper, HbGiftBag> implements HbGiftBagService {

	@Override
	public R selectGroupCodeLists(GiftCodeReq giftCodeReq){
		List<GiftCodeData> list = this.baseMapper.selectGroupCodeLists(giftCodeReq);
		StringBuilder sb = new StringBuilder();
		list.stream().map(GiftCodeData::getGiftCode).forEach(a-> sb.append(a).append(","));
		String info = sb.toString();
		if(StringUtils.isNotBlank(info)){
			info = info.substring(0,info.length()-1);
		}
		return R.ok(info);
	}
}


