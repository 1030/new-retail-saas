package com.dy.yunying.biz.service.hongbao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbActivityRoleInfo;
import com.dy.yunying.biz.dao.ads.hongbao.HbActivityRoleInfoMapper;
import com.dy.yunying.biz.service.hongbao.HbActivityRoleInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * 活动角色信息表
 *
 * @author chengang
 * @version 2021-10-28 12:02:13
 * table: hb_activity_role_info
 */
@Log4j2
@Service("hbActivityRoleInfoService")
@RequiredArgsConstructor
public class HbActivityRoleInfoServiceImpl extends ServiceImpl<HbActivityRoleInfoMapper, HbActivityRoleInfo> implements HbActivityRoleInfoService {


	private final KafkaTemplate<String, Object> kafkaTemplate;

	@Value("${hb.kafka.total.withdrawal.topic}")
	private String hbTopic;

	/**
	 * 角色活动数据统一发送服务
	 *
	 * @param roleid
	 * @param content
	 */
	@Override
	public void send(String roleid, byte[] content) {

		String key = hbTopic + '_' + roleid;
		if (StringUtils.isNotBlank(roleid)) {
			kafkaTemplate.send(hbTopic, key, content);
		} else {
			key = hbTopic;
			kafkaTemplate.send(hbTopic, key, content);
		}
	}

}


