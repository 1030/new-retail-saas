package com.dy.yunying.biz.service.usergroup.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.usergroup.AreaInfo;
import com.dy.yunying.api.entity.usergroup.DeviceInfo;
import com.dy.yunying.biz.dao.ads.usergroup.AreaInfoMapper;
import com.dy.yunying.biz.dao.ads.usergroup.DeviceInfoMapper;
import com.dy.yunying.biz.service.usergroup.AreaInfoService;
import com.dy.yunying.biz.service.usergroup.DeviceInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service("areaInfoService")
@RequiredArgsConstructor
public class AreaInfoServiceImpl extends ServiceImpl<AreaInfoMapper, AreaInfo> implements AreaInfoService {
}
