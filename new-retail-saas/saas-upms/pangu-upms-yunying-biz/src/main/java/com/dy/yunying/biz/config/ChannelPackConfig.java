package com.dy.yunying.biz.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * 渠道分包配置
 * 动态刷新
 *
 * @Author: hjl
 * @Date: 2022-4-26 13:43:02
 */
@RefreshScope
@Configuration
@Data
public class ChannelPackConfig {

	/**
	 * app下载根url
	 */
	@Value("${pack.app_file_url_root}")
	private String app_file_url_root;

	/**
	 * 批量分包api
	 */
	@Value("${pack.pack_api_channel_batch_pack}")
	private String pack_api_channel_batch_pack;

	/**
	 * 监测链接域名
	 */
	@Value("${pack.api_v3_detection_domain}")
	private String api_v3_detection_domain;
}