package com.dy.yunying.biz.task;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Author chengang
 * @Date 2021/7/15
 */
@Slf4j
@Component
public class DemoTask {

	@XxlJob("demoTask")
	public ReturnT<String> demoTask(String param){
		XxlJobLogger.log("-----------------DEMO START--------------------------");
		log.info("请求入参：String param 必须要加喔！");
		XxlJobLogger.log("-----------------DEMO END--------------------------");

		if ("0".equals(param)) {
			return ReturnT.SUCCESS;
		} else if ("1".equals(param)) {
			return ReturnT.FAIL;
		} else {
			return new ReturnT<String>(ReturnT.FAIL.getCode(), "command exit value("+param+") is failed");
		}
	}

}
