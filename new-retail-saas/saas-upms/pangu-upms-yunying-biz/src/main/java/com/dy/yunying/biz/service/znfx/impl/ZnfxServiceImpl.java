package com.dy.yunying.biz.service.znfx.impl;

import com.dy.yunying.api.entity.znfx.ChartData;
import com.dy.yunying.api.entity.znfx.ChartDataDetail;
import com.dy.yunying.api.entity.znfx.ChartVO;
import com.dy.yunying.api.enums.ChartTypeEnum;
import com.dy.yunying.api.enums.ZnfxAgeEnum;
import com.dy.yunying.api.enums.ZnfxCodeEnum;
import com.dy.yunying.api.req.znfx.ZnfxReq;
import com.dy.yunying.biz.dao.znfx.PlanUserDao;
import com.dy.yunying.biz.dao.znfx.PaidUserDao;
import com.dy.yunying.biz.service.znfx.ZnfxService;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.api.enums.GdtSiteSetEnum;
import com.pig4cloud.pig.api.enums.KsCsiteEnum;
import com.pig4cloud.pig.api.enums.TtCsiteEnum;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;

/**
 * @description:
 * @author: zhuxm
 * @time: 2023/3/21 14:30
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ZnfxServiceImpl implements ZnfxService {

	private final PaidUserDao paidUserDao;

	private final PlanUserDao planUserDao;

	@Override
	public List<ChartVO> selectChartData(ZnfxReq znfxReq) {

		List<ChartVO> list = new ArrayList<>();
		//查询范围
		String searchRange = znfxReq.getSearchRange();
		String[] ranges = searchRange.split(",");
		for(String range : ranges){
			ZnfxCodeEnum znfxCodeEnum = ZnfxCodeEnum.getOneByCode(range);
			if(znfxCodeEnum == null){
				continue;
			}
			//组装基础信息
			ChartVO pieVO = new ChartVO();
			pieVO.setTarget(znfxCodeEnum.getCode());
			pieVO.setTitle(znfxCodeEnum.getName());
			pieVO.setIsAxis(1);//1:横坐标横向放置   2:横坐标纵向放置   默认为1
			ChartData chartData;
			try {
				switch (znfxCodeEnum){
					case CLICKTIME:
						//组装点击时间分布
						chartData = new ChartData();
						//柱状图
						chartData.setType(ChartTypeEnum.BAR.getCode());
						List<ChartDataDetail> clickData = paidUserDao.selectClicktimeData(znfxReq);
						//求总
						Long total = clickData.stream().mapToLong(ChartDataDetail::getValue).sum();
						if(total.compareTo(0L) > 0){
							chartData.setData(clickData);
						}else{
							chartData.setData(new ArrayList<>());
						}
						pieVO.setSeries(Lists.newArrayList(chartData));
						list.add(pieVO);
						break;
					case AREA:
						//组装地域分布
						pieVO.setIsAxis(2);
						chartData = new ChartData();
						chartData.setType(ChartTypeEnum.BAR.getCode());
						chartData.setData(paidUserDao.selectAreaData(znfxReq));
						pieVO.setSeries(Lists.newArrayList(chartData));
						list.add(pieVO);
						break;
					case OS:
						//组装系统分布
						pieVO.setIsAxis(1);
						chartData = new ChartData();
						chartData.setType(ChartTypeEnum.PIE.getCode());
						chartData.setData(paidUserDao.selectOsData(znfxReq));
						pieVO.setSeries(Lists.newArrayList(chartData));
						list.add(pieVO);
						break;
					case MODE:
						//组装机型分布
						pieVO.setIsAxis(2);
						chartData = new ChartData();
						chartData.setType(ChartTypeEnum.BAR.getCode());
						chartData.setData(paidUserDao.selectModeData(znfxReq));
						pieVO.setSeries(Lists.newArrayList(chartData));
						list.add(pieVO);
						break;
					case MEDIO:
						//组装媒体来源
						pieVO.setIsAxis(2);
						chartData = new ChartData();
						chartData.setType(ChartTypeEnum.BAR.getCode());
						List<ChartDataDetail> data = paidUserDao.selectMedioData(znfxReq);
						//过滤、设置name值
						data.stream().filter(Objects::nonNull).filter(v -> Objects.nonNull(v.getName())).filter(v -> v.getName().split("-").length == 2).forEach(v -> {
							String name = v.getName();
							String ctype  = name.split("-")[0];
							String csite  = name.split("-")[1];

							PlatformTypeEnum ctypeEnum = PlatformTypeEnum.getByValue(ctype);
							if(ctypeEnum != null){
								switch (ctypeEnum){
									case TT:
										v.setName(Objects.isNull(TtCsiteEnum.getNameByValue(csite)) ? "其他": TtCsiteEnum.getNameByValue(csite));
										break;
									case GDT:
										v.setName(Objects.isNull(GdtSiteSetEnum.getNameByType(csite)) ? "其他": GdtSiteSetEnum.getNameByType(csite));
										break;
									case KS:
										v.setName(Objects.isNull(KsCsiteEnum.getNameByType(csite)) ? "其他": KsCsiteEnum.getNameByType(csite));
										break;
								}
							}
						});
						//按名称汇总（确保名称不重复，其他除外）
						Map<String, Long> dataMap = CollectionUtils.isEmpty(data) ? new HashMap<>() : data.stream().collect(groupingBy(ChartDataDetail::getName, summingLong(ChartDataDetail::getValue)));
						List<ChartDataDetail> newData = dataMap.entrySet().stream().map(c -> new ChartDataDetail(c.getKey(), c.getValue())).collect(Collectors.toList());

						chartData.setData(newData);
						pieVO.setSeries(Lists.newArrayList(chartData));
						list.add(pieVO);
						break;
					case AGE:
						//组装年龄来源
						pieVO.setIsAxis(1);
						chartData = new ChartData();
						chartData.setType(ChartTypeEnum.PIE.getCode());
						//解析年龄分布 AGE_BETWEEN_18_23, AGE_BETWEEN_24_30,AGE_BETWEEN_31_40,AGE_BETWEEN_41_49, AGE_ABOVE_50
						List<ChartDataDetail> ageData = paidUserDao.selectAgeData(znfxReq);
						ageData.forEach(v -> {
							ZnfxAgeEnum znfxAgeEnum = ZnfxAgeEnum.getOneByage(v.getName());
							v.setName(znfxAgeEnum == null ? v.getName() : znfxAgeEnum.getName());
						});
						chartData.setData(ageData);
						pieVO.setSeries(Lists.newArrayList(chartData));
						list.add(pieVO);
						break;
					case SEX:
						//组装性别来源
						pieVO.setIsAxis(1);
						chartData = new ChartData();
						chartData.setType(ChartTypeEnum.PIE.getCode());
						chartData.setData(paidUserDao.selectSexData(znfxReq));
						pieVO.setSeries(Lists.newArrayList(chartData));
						list.add(pieVO);
						break;
				}
			} catch (Throwable t) {
				log.error("{}执行错误，跳过", znfxCodeEnum.getName());
			}
		}
		return list;

	}


	/**
	 * 计划用户分析图形数据查询
	 * @param znfxReq
	 * @return
	 */
	@Override
	public List<ChartVO> planUserSearch(ZnfxReq znfxReq) {

		List<ChartVO> list = new ArrayList<>();
		//查询范围
		String searchRange = znfxReq.getSearchRange();
		String[] ranges = searchRange.split(",");
		for(String range : ranges){
			ZnfxCodeEnum znfxCodeEnum = ZnfxCodeEnum.getOneByCode(range);
			if(znfxCodeEnum == null){
				continue;
			}
			//组装基础信息
			ChartVO pieVO = new ChartVO();
			pieVO.setTarget(znfxCodeEnum.getCode());
			pieVO.setTitle(znfxCodeEnum.getName());
			pieVO.setIsAxis(1);//1:横坐标横向放置   2:横坐标纵向放置   默认为1
			ChartData chartData;
			switch (znfxCodeEnum){
				case CLICKTIME:
					//组装点击时间分布
					chartData = new ChartData();
					chartData.setType(ChartTypeEnum.LINE.getCode());
					chartData.setData(planUserDao.selectClicktimeData(znfxReq));
					pieVO.setSeries(Lists.newArrayList(chartData));
					list.add(pieVO);
					break;
				case FRISTTIME:
					//组装在线时长分布
					chartData = new ChartData();
					chartData.setType(ChartTypeEnum.PIE.getCode());
					chartData.setData(planUserDao.selectOnlineTimeData(znfxReq));
					pieVO.setSeries(Lists.newArrayList(chartData));
					list.add(pieVO);
					break;
				case OS:
					//组装系统分布
					chartData = new ChartData();
					chartData.setType(ChartTypeEnum.PIE.getCode());
					chartData.setData(planUserDao.selectOsData(znfxReq));
					pieVO.setSeries(Lists.newArrayList(chartData));
					list.add(pieVO);
					break;
				case AREA:
					//组装地域分布
					pieVO.setIsAxis(2);
					chartData = new ChartData();
					chartData.setType(ChartTypeEnum.BAR.getCode());
					chartData.setData(planUserDao.selectAreaData(znfxReq));
					pieVO.setSeries(Lists.newArrayList(chartData));
					list.add(pieVO);
					break;
				case MODE:
					//组装机型分布
					pieVO.setIsAxis(2);
					chartData = new ChartData();
					chartData.setType(ChartTypeEnum.BAR.getCode());
					chartData.setData(planUserDao.selectModeData(znfxReq));
					pieVO.setSeries(Lists.newArrayList(chartData));
					list.add(pieVO);
					break;
				case AGE:
					//组装年龄来源
					pieVO.setIsAxis(1);
					chartData = new ChartData();
					chartData.setType(ChartTypeEnum.PIE.getCode());
					//解析年龄分布 AGE_BETWEEN_18_23, AGE_BETWEEN_24_30,AGE_BETWEEN_31_40,AGE_BETWEEN_41_49, AGE_ABOVE_50
					List<ChartDataDetail> ageData = planUserDao.selectAgeData(znfxReq);
					ageData.forEach(v -> {
						ZnfxAgeEnum znfxAgeEnum = ZnfxAgeEnum.getOneByage(v.getName());
						v.setName(znfxAgeEnum == null ? v.getName() : znfxAgeEnum.getName());
					});
					chartData.setData(ageData);
					pieVO.setSeries(Lists.newArrayList(chartData));
					list.add(pieVO);
					break;
				case SEX:
					//组装性别来源
					pieVO.setIsAxis(1);
					chartData = new ChartData();
					chartData.setType(ChartTypeEnum.PIE.getCode());
					chartData.setData(planUserDao.selectSexData(znfxReq));
					pieVO.setSeries(Lists.newArrayList(chartData));
					list.add(pieVO);
					break;
			}
		}
		return list;
	}
}
