package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.WanGameSupplierDO;

public interface WanGameSupplierDOMapper {
	int deleteByPrimaryKey(Integer gid);

	int insert(WanGameSupplierDO record);

	int insertSelective(WanGameSupplierDO record);

	WanGameSupplierDO selectByPrimaryKey(Integer gid);

	int updateByPrimaryKeySelective(WanGameSupplierDO record);

	int updateByPrimaryKeyWithBLOBs(WanGameSupplierDO record);

	int updateByPrimaryKey(WanGameSupplierDO record);
}
