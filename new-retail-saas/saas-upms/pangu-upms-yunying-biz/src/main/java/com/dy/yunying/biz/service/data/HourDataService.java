package com.dy.yunying.biz.service.data;

import com.dy.yunying.api.dto.HourDataDto;
import com.pig4cloud.pig.common.core.util.R;


/**
 * @description:
 * @author: nml
 * @time: 2021/6/24 14:54
 **/

public interface HourDataService {

	/*
	 * 查询分时数据
	 * */
	R selectHourData(HourDataDto req);

}
