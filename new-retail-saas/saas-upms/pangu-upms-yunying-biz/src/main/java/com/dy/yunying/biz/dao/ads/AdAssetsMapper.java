package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.api.entity.AdAssets;
import org.apache.ibatis.annotations.Mapper;

/**
 * 广告资产表
 * @author  chenxiang
 * @version  2022-04-06 10:39:18
 * table: ad_assets
 */
@Mapper
public interface AdAssetsMapper extends BaseMapper<AdAssets> {
}


