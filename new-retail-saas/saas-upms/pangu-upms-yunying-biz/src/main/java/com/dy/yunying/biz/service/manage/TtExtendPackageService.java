package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.TtExtendPackageDO;

public interface TtExtendPackageService extends IService<TtExtendPackageDO> {

	void queryExtendPackage(String param);
}
