package com.dy.yunying.biz.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * 广点通配置
 * 动态刷新
 *
 * @Author: hjl
 * @Date: 2022-4-26 14:07:20
 */
@RefreshScope
@ConfigurationProperties("baidu.track")
@Component
public class BdConfig {

	/**
	 * 点击监测链接模版
	 */
	public static String click_track_url_template;

	/**
	 * 展示监测链接模版
	 */
	public static String display_track_url_template;

	public void setClick_track_url_template(String click_track_url_template) {
		BdConfig.click_track_url_template = click_track_url_template;
	}

	public void setDisplay_track_url_template(String display_track_url_template) {
		BdConfig.display_track_url_template = display_track_url_template;
	}
}