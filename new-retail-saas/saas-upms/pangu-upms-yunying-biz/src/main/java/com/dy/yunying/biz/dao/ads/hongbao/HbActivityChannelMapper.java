package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbActivityChannel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 活动渠道范围关系表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:19
 */
@Mapper
public interface HbActivityChannelMapper extends BaseMapper<HbActivityChannel> {

}
