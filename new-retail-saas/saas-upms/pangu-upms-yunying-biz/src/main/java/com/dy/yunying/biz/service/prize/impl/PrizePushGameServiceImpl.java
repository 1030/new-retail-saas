package com.dy.yunying.biz.service.prize.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.prize.PrizePushGame;
import com.dy.yunying.biz.dao.ads.prize.PrizePushGameMapper;
import com.dy.yunying.biz.service.prize.PrizePushGameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 奖励与游戏关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:59
 * table: prize_push_game
 */
@Log4j2
@Service("prizePushGameService")
@RequiredArgsConstructor
public class PrizePushGameServiceImpl extends ServiceImpl<PrizePushGameMapper, PrizePushGame> implements PrizePushGameService {
}


