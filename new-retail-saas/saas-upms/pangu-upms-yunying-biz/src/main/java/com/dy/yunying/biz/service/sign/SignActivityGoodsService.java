

package com.dy.yunying.biz.service.sign;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.sign.SignActivityGoods;

/**
 * 签到活动物品表
 * @author  chengang
 * @version  2021-12-01 10:14:07
 * table: sign_activity_goods
 */
public interface SignActivityGoodsService extends IService<SignActivityGoods> {

	
}


