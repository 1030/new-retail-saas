package com.dy.yunying.biz.service.manage.impl;

import com.dy.yunying.api.entity.WanCmsConfigDO;
import com.dy.yunying.api.req.WanCmsConfigReq;
import com.dy.yunying.biz.dao.manage.ext.WanCmsConfigDOMapperExt;
import com.dy.yunying.biz.service.manage.WanCmsConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class WanCmsConfigServiceImpl implements WanCmsConfigService {

	@Autowired
	private WanCmsConfigDOMapperExt wanCmsConfigDOMapperExt;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return wanCmsConfigDOMapperExt.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(WanCmsConfigDO record) {
		return wanCmsConfigDOMapperExt.insert(record);
	}

	@Override
	public int insertSelective(WanCmsConfigDO record) {
		return wanCmsConfigDOMapperExt.insertSelective(record);
	}

	@Override
	public WanCmsConfigDO selectByPrimaryKey(Integer id) {
		return wanCmsConfigDOMapperExt.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(WanCmsConfigDO record) {
		return wanCmsConfigDOMapperExt.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(WanCmsConfigDO record) {
		return wanCmsConfigDOMapperExt.updateByPrimaryKey(record);
	}

	/**
	 * 插入cms配置
	 *
	 * @param name
	 * @param cfgstatus
	 * @param cfgtype
	 * @param typeid
	 * @return
	 */
	@Override
	public int insertWanCmsConfig(String name, String cfgstatus, String cfgtype, Integer typeid) {
		WanCmsConfigReq cmsConfig = new WanCmsConfigReq();
		cmsConfig.setName("游戏中心-" + name);
		cmsConfig.setCfgstatus("1");
		cmsConfig.setStatus(1);
		cmsConfig.setCfgtype("games");
		cmsConfig.setTypeid(typeid);
		return wanCmsConfigDOMapperExt.insertWanCmsConfig(cmsConfig);
	}

}
