package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.GameAppointmentSchedule;
import com.dy.yunying.api.entity.GameAppointmentScheduleGearsPage;
import com.dy.yunying.api.entity.GameAppointmentScheduleUserNum;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleReq;
import com.dy.yunying.api.resp.GameAppointmentScheduleUserColumn;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 游戏预约进度表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment
 */
@Mapper
public interface GameAppointmentScheduleMapper extends BaseMapper<GameAppointmentSchedule> {


	/***
	 * 查询档位表
	 * @param req
	 * @return
	 */
	List<GameAppointmentScheduleGearsPage> selectGameAppointmentScheduleList(GameAppointmentSchedule req);


	/***
	 * 查询各来源预约人数
	 * @param req
	 * @return
	 */
	List<GameAppointmentScheduleUserNum> selectGameAppointmentScheduleUserNumList(GameAppointmentScheduleReq req);
}


