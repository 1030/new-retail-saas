package com.dy.yunying.biz.dao.manage.ext;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.req.GameVersionReq;
import com.dy.yunying.api.vo.GameVersionVO;
import com.dy.yunying.biz.dao.manage.GameVersionDOMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;
import java.util.List;

public interface GameVersionDOMapperExt extends GameVersionDOMapper {

	IPage<GameVersionVO> page(GameVersionReq req);

	void disableOther(@Param("gameId") Long gameId, @Param("versionId") Long versionId, @Param("packType") Integer packType);

	List<GameVersionVO> getSonGamePackType(@Param("gameIds") List<BigInteger> gameIds);
}