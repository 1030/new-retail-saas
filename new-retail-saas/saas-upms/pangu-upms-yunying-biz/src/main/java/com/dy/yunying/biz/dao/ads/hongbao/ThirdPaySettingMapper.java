package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.ThirdPaySetting;
import org.apache.ibatis.annotations.Mapper;

/**
 * 三方支付信息设置表
 * @author  chengang
 * @version  2023-02-28 14:11:55
 * table: third_pay_setting
 */
@Mapper
public interface ThirdPaySettingMapper extends BaseMapper<ThirdPaySetting> {

}


