package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.RegRetentionDto;
import com.dy.yunying.api.datacenter.vo.RegRetentionVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * dogame 注册留存率
 * @author hma
 * @date 2022/8/19 15:36
 */
public interface RegRetentionService {
	/**
	 * 列表数据
	 * @param req
	 * @return
	 */
	R<List<RegRetentionVO>> page(RegRetentionDto req);

	/**
	 * 总数量
	 * @param req
	 * @return
	 */
	R count(RegRetentionDto req);

	/**
	 * 汇总数据
	 * @param req
	 * @return
	 */
	R<List<RegRetentionVO>> collect(RegRetentionDto req);
}
