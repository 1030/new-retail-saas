package com.dy.yunying.biz.dao.manage.ext;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.dto.ParentGamePackDto;
import com.dy.yunying.api.entity.ParentGameVersionDO;
import com.dy.yunying.api.req.ParentGameVersionReq;
import com.dy.yunying.api.resp.ParentGameDataRes;
import com.dy.yunying.api.vo.ParentGameVersionVO;
import com.dy.yunying.biz.dao.manage.ParentGameVersionDOMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ParentGameVersionDOMapperExt extends ParentGameVersionDOMapper {

	IPage<ParentGameVersionVO> page(ParentGameVersionReq req);

	ParentGamePackDto getParentGamePackByPK(Long versionId);

	void disableOther(@Param("gameId") Long gameId, @Param("versionId") Long versionId, @Param("packType") Integer packType);

	ParentGameVersionDO getLatestByGameId(@Param("gameId") Long gameId, @Param("packType") Integer packType);

	List<ParentGameVersionDO> getPackTypeByGameId(@Param("gameId") Long gameId);

	List<ParentGameVersionDO> selectListByPgId(@Param("pgIdList") List<ParentGameDataRes> pgIdList);
}
