package com.dy.yunying.biz.dao.manage.ext;


import com.dy.yunying.api.req.GameSupplierReq;
import com.dy.yunying.biz.dao.manage.WanGameSupplierDOMapper;

public interface WanGameSupplierDOMapperExt extends WanGameSupplierDOMapper {

	int updateExchangeUrl(GameSupplierReq req);
}