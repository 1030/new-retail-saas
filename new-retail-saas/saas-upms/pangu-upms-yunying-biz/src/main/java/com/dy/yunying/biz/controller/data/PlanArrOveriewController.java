package com.dy.yunying.biz.controller.data;


import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.dto.AdOverviewDto;
import com.dy.yunying.api.vo.AdPlanOverviewVo;
import com.dy.yunying.api.vo.PlanAttrAnalyseSearchVo;
import com.dy.yunying.biz.service.data.AdOverviewService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.api.vo.MaterialVo;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 计划属性统计
 */
@RestController
@RequestMapping("/planarr")
@Slf4j
public class PlanArrOveriewController {


	@Autowired
	private AdOverviewService adOverviewService;

	/**
	 * 广告概览数据
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping(value = "/getStatistic")
	public R getStatistic(@Valid PlanAttrAnalyseSearchVo req) {
		if (!PlatformTypeEnum.containsType(String.valueOf(req.getCtype()))) {
			throw new BusinessException(String.format("渠道类型标识不合法【%s】", req.getCtype()));
		}
		return adOverviewService.selectPlanAttrAnalyseReoport(null, req);
	}

	/**
	 * 广告概览数据
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping(value = "/getStatisticPage")
	public R getStatisticPage(@Valid PlanAttrAnalyseSearchVo req) {
		if (!PlatformTypeEnum.containsType(String.valueOf(req.getCtype()))) {
			throw new BusinessException(String.format("渠道类型标识不合法【%s】", req.getCtype()));
		}
		return adOverviewService.selectPlanAttrAnalyseReoport(new Page(req.getCurrent(), req.getSize()), req);
	}


	/**
	 * 广告概览导出
	 *
	 * @param req
	 * @return
	 */
//	@SysLog("计划属性统计导出")
	@ResponseExcel(name = "计划属性统计导出", sheet = "计划属性统计导出")
	@GetMapping("/excelStatistic")
	public R excelPlanStatistic(PlanAttrAnalyseSearchVo req, HttpServletResponse response, HttpServletRequest request) {
		try {
			if (!PlatformTypeEnum.containsType(String.valueOf(req.getCtype()))) {
				throw new BusinessException(String.format("渠道类型标识不合法【%s】", req.getCtype()));
			}
			String sheetName = "计划属性统计导出";
			R result = adOverviewService.selectPlanAttrAnalyseReoport(null, req);
			//list对象转listMap PlanTtAttrAnalyseReportVo
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps((List) result.getData());
			if (CollectionUtils.isEmpty(resultListMap)) {
				throw new BusinessException("无数据");
			}
			resultListMap.stream().forEach(v -> {
				List<MaterialVo> vos = (List<MaterialVo>) v.get("materialVos");
				if (CollectionUtils.isNotEmpty(vos)) {
					v.put("materialVos", ArrayUtil.join(vos.stream().map(v1 -> v1.getMName()).toArray(), Constant.COMMA));
				} else {
					v.put("materialVos", Constant.EMPTTYSTR);
				}
				BigDecimal regPayRatio = (BigDecimal) v.get("regPayRatio");
				if (!Objects.isNull(regPayRatio)) {
					v.put("regPayRatio", String.format("%s%s", regPayRatio.doubleValue() ,Constant.PERCENT));
				} else {
					v.put("regPayRatio", String.format("0%s", Constant.PERCENT));
				}
				BigDecimal roi1 = (BigDecimal) v.get("roi1");
				if (!Objects.isNull(regPayRatio)) {
					v.put("roi1", String.format("%s%s", roi1.doubleValue() , Constant.PERCENT));
				} else {
					v.put("roi1", String.format("0%s", Constant.PERCENT));
				}
				BigDecimal retention2Ratio = (BigDecimal) v.get("retention2Ratio");
				if (!Objects.isNull(regPayRatio)) {
					v.put("retention2Ratio", String.format("%s%s", retention2Ratio.doubleValue() , Constant.PERCENT));
				} else {
					v.put("retention2Ratio", String.format("0%s", Constant.PERCENT));
				}
				BigDecimal allRoi = (BigDecimal) v.get("allRoi");
				if (!Objects.isNull(regPayRatio)) {
					v.put("allRoi", String.format("%s%s", allRoi.doubleValue() , Constant.PERCENT));
				} else {
					v.put("allRoi", String.format("0%s", Constant.PERCENT));
				}
			});
			// 导出
			R re = ExportUtils.exportExcelData(request,response, String.format("计划属性统计导出-%s.xlsx", DateUtils.getCurrentTimeNoUnderline()), sheetName, req.getTitles(), req.getColumns(), resultListMap);
			if (re.getCode() == CommonConstants.FAIL) {
				throw new BusinessException(re.getMsg());
			}
		} catch (BusinessException e) {
			log.error("excelPlanStatistic is error", e);
			throw e;
		} catch (Exception e) {
			log.error("excelPlanStatistic is error", e);
			throw new BusinessException("导出异常");
		}
		return null;
	}

}
