package com.dy.yunying.biz.dao.manage;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.WanGameDO;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WanGameDOMapper extends BaseMapper<WanGameDO> {
	int deleteByPrimaryKey(Long id);

	int insert(WanGameDO record);

	int insertSelective(WanGameDO record);

	WanGameDO selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(WanGameDO record);

	int updateByPrimaryKey(WanGameDO record);

	List<WanGameDO> getWanGameByGameIds(@Param("ids") Set<Long> ids);
}