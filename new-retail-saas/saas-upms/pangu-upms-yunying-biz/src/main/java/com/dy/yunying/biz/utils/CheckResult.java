package com.dy.yunying.biz.utils;

public class CheckResult {

    //状态
    private boolean status;
    //提示
    private String message;
    
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
    
}
