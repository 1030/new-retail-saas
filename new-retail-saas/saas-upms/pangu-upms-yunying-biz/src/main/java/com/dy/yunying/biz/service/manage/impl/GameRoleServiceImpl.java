package com.dy.yunying.biz.service.manage.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.GameRole;
import com.dy.yunying.biz.dao.manage.GameRoleMapper;
import com.dy.yunying.biz.service.manage.GameRoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 角色信息表
 * @author  chengang
 * @version  2021-10-28 13:35:09
 * table: game_role
 */
@Log4j2
@Service("gameRoleService")
@RequiredArgsConstructor
public class GameRoleServiceImpl extends ServiceImpl<GameRoleMapper, GameRole> implements GameRoleService {

	
	
}


