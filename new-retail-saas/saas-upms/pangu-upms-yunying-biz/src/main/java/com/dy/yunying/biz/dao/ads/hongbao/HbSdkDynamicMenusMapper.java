package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbSdkDynamicMenus;
import com.dy.yunying.api.resp.hongbao.MenusVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author yuwenfeng
 * @description: SDK动态菜单表
 * @date 2022/3/7 10:16
 */
@Mapper
public interface HbSdkDynamicMenusMapper extends BaseMapper<HbSdkDynamicMenus> {

	@Select("SELECT menu_title FROM hb_sdk_dynamic_menus WHERE STATUS = 1 AND deleted = 0 AND parent_id = 0 GROUP BY menu_title")
	List<String> selectMenuTitleGroupTitle();
	@Select("SELECT id as menusId,menu_title as menusTitle,menu_code as menusCode FROM hb_sdk_dynamic_menus WHERE STATUS = 1 AND deleted = 0 AND parent_id = #{id} AND os_type = 2")
	List<MenusVO> selectMenuTitleByParentId(Long id);
}


