package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.AdExternalDict;
import org.apache.ibatis.annotations.Mapper;

/**
 * 转化目标字典表
 * @author  chenxiang
 * @version  2022-07-04 13:58:21
 * table: ad_external_dict
 */
@Mapper
public interface AdExternalDictMapper extends BaseMapper<AdExternalDict> {
}


