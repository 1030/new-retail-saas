package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbActivityDynamicType;
import org.apache.ibatis.annotations.Mapper;

/**
 * 活动动态类型
 */
@Mapper
public interface HbActivityDynamicTypeMapper extends BaseMapper<HbActivityDynamicType> {
}
