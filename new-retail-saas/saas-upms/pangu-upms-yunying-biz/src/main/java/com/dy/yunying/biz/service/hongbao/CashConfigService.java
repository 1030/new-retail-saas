package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbActivity;
import com.dy.yunying.api.entity.hongbao.HbCashConfig;
import com.dy.yunying.api.req.hongbao.CashConfigReq;
import com.dy.yunying.api.resp.hongbao.CashConfigVo;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author yuwenfeng
 * @description: 提现档次服务
 * @date 2021/10/23 10:47
 */
public interface CashConfigService extends IService<HbCashConfig> {

	Page<CashConfigVo> selectCashConfigPage(Page<HbCashConfig> page, QueryWrapper<HbCashConfig> query);

	R addCashConfig(CashConfigReq cashConfig);

	R editCashConfig(CashConfigReq cashConfig);

	R delCashConfig(Long cashConfigId);
}
