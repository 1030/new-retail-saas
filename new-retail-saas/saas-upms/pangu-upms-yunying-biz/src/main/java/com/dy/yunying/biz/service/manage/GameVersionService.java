package com.dy.yunying.biz.service.manage;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.GameVersionDO;
import com.dy.yunying.api.req.GameVersionReq;
import com.dy.yunying.api.vo.GameVersionVO;

/**
 * @Author: hjl
 * @Date: 2020/7/21 13:52
 */
public interface GameVersionService {

	GameVersionDO save(GameVersionDO gameVersionDO);

	/**
	 * 逻辑删除
	 *
	 * @param versionId
	 */
	void deleteByPK(Long versionId);

	IPage<GameVersionVO> page(GameVersionReq req);

	GameVersionDO getByPK(Long versionId);
}