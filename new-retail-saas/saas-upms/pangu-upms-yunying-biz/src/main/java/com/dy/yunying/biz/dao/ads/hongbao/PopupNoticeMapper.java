package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.PopupNotice;
import com.dy.yunying.api.vo.usergroup.UserGroupStatVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PopupNoticeMapper extends BaseMapper<PopupNotice> {

	int onlineByActivityId(@Param("activityId") Long activityId, @Param("popupStatus") Integer popupStatus);

	List<Long> selectRangeNoticeId(@Param("parentGameArr") String parentGameArr, @Param("gameArr") String gameArr, @Param("areaArr") String areaArr, @Param("parentChannelArr") String parentChannelArr, @Param("childChannelArr") String childChannelArr);

	List<PopupNotice> selectListByIds(@Param("noticeIds") List<Long> noticeIds);

	List<UserGroupStatVO> countWithUserGroupIds(@Param("userGroupIds")List<Long> userGroupIds);

	List<Long> getNoticeIdsWithUserGroupId(@Param("userGroupId")Long userGroupId);
}
