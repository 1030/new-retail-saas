package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbReceivingAddress;
import org.apache.ibatis.annotations.Mapper;

/**
 * 实物奖品领取地址记录
 * @author  chenxiang
 * @version  2021-10-25 14:20:46
 * table: hb_receiving_address
 */
@Mapper
public interface HbReceivingAddressMapper extends BaseMapper<HbReceivingAddress> {
	
	//单表基本CURD操作BaseMapper已经存在,请尽量不用重载父类已定义的方法
}


