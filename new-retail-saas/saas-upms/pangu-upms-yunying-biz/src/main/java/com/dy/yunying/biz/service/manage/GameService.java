package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.dto.SonGameDto;
import com.dy.yunying.api.entity.WanGameDO;
import com.dy.yunying.api.req.WanGameReq;
import com.dy.yunying.api.vo.WanGameReqVo;
import com.dy.yunying.api.vo.WanGameVersionVO;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public interface GameService extends IService<WanGameDO> {

	int deleteByPrimaryKey(Long id);

	int insert(WanGameDO record);

	int insertSelective(WanGameDO record);

	WanGameDO selectByPrimaryKey(Long id);

	WanGameDO selectVOByPK(Long id);

	List<WanGameDO> selectVOByPKs(List<Long> gameIdList);

	WanGameVersionVO selectVersionVOByPK(Long id, Integer packType);

	List<WanGameVersionVO> versionVOList(WanGameDO req);

	List<Map<String, Object>> versionVOTree(WanGameDO req);

	int updateByPrimaryKeySelective(WanGameDO record);

	int updateByPrimaryKey(WanGameDO record);

	/**
	 * 获取可缓存的子游戏列表
	 *
	 * @param req
	 * @return
	 * @throws Exception
	 */
	List<WanGameDO> getCacheableGameList(WanGameReq req) throws Exception;

	/**
	 * 游戏列表
	 *
	 * @param record
	 * @return
	 */
	IPage<WanGameReqVo> selectGameList(WanGameReq record);

	/**
	 * 根据查询条件获取游戏信息
	 *
	 * @param map
	 * @return
	 */
	WanGameReqVo selectWanGameByCond(Map<String, Object> map);

	/**
	 * 根据查询条件获取游戏list
	 *
	 * @param
	 * @return
	 */
	List<WanGameDO> selectWanGameListByCond(WanGameDO req);

	/**
	 * 删除，批量删除
	 *
	 * @param list
	 * @return
	 */
	public Map<String, Object> batchDelete(String[] list);

	/**
	 * 新增游戏
	 *
	 * @param record
	 * @return
	 */
	BigInteger insertWanGame(WanGameReqVo record);

	/**
	 * 修改游戏
	 *
	 * @author cx
	 * @date 2020年7月24日11:07:02
	 */
	boolean updateWanGameByPrimaryKey(WanGameReqVo entity);

	List<SonGameDto> selectDtoByPKs(List<Long> gameIdList);

	List<SonGameDto> getAutoPackListByPgid(Long pgid);
}
