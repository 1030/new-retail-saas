package com.dy.yunying.biz.controller.datacenter;

import com.dy.yunying.api.datacenter.vo.RealTimeKanbanSummaryVO;
import com.dy.yunying.api.datacenter.vo.RealTimeKanbanTimeSharingVO;
import com.dy.yunying.biz.service.datacenter.RealTimeKanbanService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sunyq
 * @date 2022/8/16 16:35
 */
@Slf4j
@RestController("realTimeKanban")
@RequestMapping("/dataCenter/realTimeKanban")
@RequiredArgsConstructor
public class RealTimeKanbanController {

	private final RealTimeKanbanService realTimeKanbanService;

	/**
	 * 实时看板汇总数据
	 *
	 * @param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/summary")
	public R summary() {
		Map<String,Object> result = new HashMap<>(4);
		List<RealTimeKanbanSummaryVO> list = realTimeKanbanService.summary();
		result.put("time", LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		result.put("list", list);
		return R.ok(result);
	}

	/**
	 * 实时看板汇总数据
	 *
	 * @param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/timeSharing")
	public R timeSharing() {
		List<RealTimeKanbanTimeSharingVO> list = realTimeKanbanService.timeSharing();
		return R.ok(list);
	}

}
