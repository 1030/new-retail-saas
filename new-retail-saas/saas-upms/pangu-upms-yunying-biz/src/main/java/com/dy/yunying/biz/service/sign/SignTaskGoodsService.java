package com.dy.yunying.biz.service.sign;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.sign.SignTaskGoodsDO;

/**
 *
 */
public interface SignTaskGoodsService extends IService<SignTaskGoodsDO> {

}
