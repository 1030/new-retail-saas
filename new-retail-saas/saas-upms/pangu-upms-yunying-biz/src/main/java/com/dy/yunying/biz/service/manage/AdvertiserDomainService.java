/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.AdvertiserDomain;

/**
 * 广告域名管理
 *
 * @author pigx code generator
 * @date 2021-06-04 15:53:15
 */
public interface AdvertiserDomainService extends IService<AdvertiserDomain> {

}
