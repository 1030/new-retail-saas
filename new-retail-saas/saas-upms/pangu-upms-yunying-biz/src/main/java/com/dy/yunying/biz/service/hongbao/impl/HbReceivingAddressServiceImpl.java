package com.dy.yunying.biz.service.hongbao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbReceivingAddress;
import com.dy.yunying.biz.dao.ads.hongbao.HbReceivingAddressMapper;
import com.dy.yunying.biz.service.hongbao.HbReceivingAddressService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 实物奖品领取地址记录
 * @author  chenxiang
 * @version  2021-10-25 14:20:46
 * table: hb_receiving_address
 */
@Log4j2
@Service("hbReceivingAddressService")
@RequiredArgsConstructor
public class HbReceivingAddressServiceImpl extends ServiceImpl<HbReceivingAddressMapper, HbReceivingAddress> implements HbReceivingAddressService {
	
	
}


