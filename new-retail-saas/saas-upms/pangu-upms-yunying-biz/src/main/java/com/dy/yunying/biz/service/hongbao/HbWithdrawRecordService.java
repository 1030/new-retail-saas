

package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbWithdrawRecord;
import com.dy.yunying.api.req.hongbao.HbWithdrawRecordRejectReq;
import com.dy.yunying.api.req.hongbao.HbWithdrawRecordReq;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 提现记录表
 * @author  zhuxm
 * @version  2021-10-28 20:22:20
 */
public interface HbWithdrawRecordService extends IService<HbWithdrawRecord> {


	public R pass(HbWithdrawRecordRejectReq req) throws Exception;

	public R reject(HbWithdrawRecordRejectReq req) throws Exception;

	public R batchReviewer(HbWithdrawRecordRejectReq req) throws Exception;

	/**
	 * 查询提现记录列表
	 * @param req
	 * @return
	 */
	 IPage<HbWithdrawRecord> selectRecordPage(HbWithdrawRecordReq req);
}


