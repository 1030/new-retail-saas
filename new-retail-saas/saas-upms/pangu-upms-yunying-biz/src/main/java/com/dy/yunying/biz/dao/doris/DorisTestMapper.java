package com.dy.yunying.biz.dao.doris;

import com.dy.yunying.api.entity.DorisTable;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description:
 * @author: nml
 * @time: 2022/5/26 14:59
 **/
@Mapper
public interface DorisTestMapper {

	List<DorisTable> cdkList();

}
