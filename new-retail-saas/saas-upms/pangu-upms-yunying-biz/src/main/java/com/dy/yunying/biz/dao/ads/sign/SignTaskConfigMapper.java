package com.dy.yunying.biz.dao.ads.sign;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.sign.SignTaskConfigDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Entity com.dy.yunying.biz.dao.ads.sign..SignTaskConfig
 */
@Mapper
public interface SignTaskConfigMapper extends BaseMapper<SignTaskConfigDO> {

	/**
	 * 根据活动ID查询任务列表
	 *
	 * @param activityId
	 * @return
	 */
	List<SignTaskConfigDO> selectTaskConfigListByActivityId(Long activityId);

}
