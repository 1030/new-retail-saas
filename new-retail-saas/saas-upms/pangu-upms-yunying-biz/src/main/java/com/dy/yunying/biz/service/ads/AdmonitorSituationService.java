package com.dy.yunying.biz.service.ads;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.AdmonitorSituation;

import java.util.List;

public interface AdmonitorSituationService extends IService<AdmonitorSituation> {
	List<AdmonitorSituation> getByAdMonitorRuleId(Integer adMonitorRuleId);

	boolean deleteByAdMonitorRuleId(Integer adMonitorRuleId);

    boolean saveAdmonitorSituation(AdmonitorSituation admonitorSituation);

	boolean updateAdmonitorSituation(AdmonitorSituation admonitorSituation);

	void validateAdmonitorSituation(AdmonitorSituation admonitorSituation);
}
