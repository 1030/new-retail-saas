package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.CurrencyRechargeConfigDO;

/**
 * @author MyPC
 * @description 针对表【currency_recharge_config(游豆充值配置表)】的数据库操作Mapper
 * @createDate 2022-03-28 11:22:30
 * @Entity com.dy.yunying.api.entity.CurrencyRechargeConfigDO
 */
public interface CurrencyRechargeConfigMapper extends BaseMapper<CurrencyRechargeConfigDO> {

}




