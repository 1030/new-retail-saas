package com.dy.yunying.biz.dao.manage.ext;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.dto.ChannelManageDto;
import com.dy.yunying.api.req.ChannelManageReq;
import com.dy.yunying.api.vo.ChannelManageVo;
import com.dy.yunying.biz.dao.manage.ChannelManageDOMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ChannelManageDOMapperExt extends ChannelManageDOMapper {

	IPage<ChannelManageVo> queryParentchlList(ChannelManageReq req);

	IPage<ChannelManageVo> queryChlList(ChannelManageReq req);

	List<ChannelManageVo> queryParentChlByCodeOrName(ChannelManageDto channel);

	List<ChannelManageVo> queryChlByCodeOrName(ChannelManageDto channel);

	List<ChannelManageVo> queryChlByCodeOrNameIgnoreDel(ChannelManageDto channel);

	ChannelManageVo selectParentChlByCode(String chlCode);

	ChannelManageVo selectParentChlByCodeIgnoreDel(String chlCode);

	ChannelManageVo getVOByPK(Integer id);

	ChannelManageVo getVOByCode(String code);

	List<Map<String, String>> queryPlatForm(Map<String, Object> map);

	List<ChannelManageVo> queryParentChlByName(ChannelManageDto channel);

	List<Map<String, Object>> queryChannelFromManager(Map<String, Object> map);

	ChannelManageVo queryChlByName(ChannelManageDto req);

	List<Map> queryChlForAuth();

	List<Map> queryChlByManager(@Param("manager") Integer manager);

	List<ChannelManageVo> queryParentNameChlList(Map<String, Object> map);

	List<ChannelManageVo> queryChlListFromCode(Map<String, Object> map);

	List<ChannelManageVo> queryagentlistFromCode(Map<String, Object> map);

	List<ChannelManageVo> queryChlnameFromAppchl(Map<String, Object> map);

	ChannelManageVo getDefaultSonVO(int pid);

	List<Map<String, Object>> findPlatFormList(@Param("platform") Integer platform);

	List<ChannelManageVo> queryChlByAccount(ChannelManageDto channel);

	IPage<ChannelManageVo> queryAdminChlList(ChannelManageReq req);

	Integer queryChlNum(@Param("pid") Integer pid);

	ChannelManageVo querySonChlBylist(ChannelManageDto channel);
}