package com.dy.yunying.biz.service.hongbao;

import com.dy.yunying.api.entity.hongbao.ActivityEventType;

import java.util.List;

public interface ActivityEventTypeService {
	List<ActivityEventType> selectEventList();

}
