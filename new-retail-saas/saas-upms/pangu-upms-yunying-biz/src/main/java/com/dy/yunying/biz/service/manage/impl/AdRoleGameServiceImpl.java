package com.dy.yunying.biz.service.manage.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.AdRoleGame;
import com.dy.yunying.api.entity.WanGameDO;
import com.dy.yunying.api.req.AdRoleGameReq;
import com.dy.yunying.api.resp.WanGameRes;
import com.dy.yunying.biz.dao.manage.AdRoleGameMapper;
import com.dy.yunying.biz.dao.manage.ext.GameDOMapperExt;
import com.dy.yunying.biz.service.manage.AdRoleGameService;
import com.pig4cloud.pig.admin.api.feign.RemoteUserService;
import com.pig4cloud.pig.common.core.constant.enums.AdRoleGameTypeEnum;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 角色可查看的平台用户
 *
 * @author zhuxm
 * @date 2021-06-04
 */
@Service
@RequiredArgsConstructor
public class AdRoleGameServiceImpl extends ServiceImpl<AdRoleGameMapper, AdRoleGame> implements AdRoleGameService {

	private final AdRoleGameMapper adRoleGameMapper;

	private final RemoteUserService remoteUserService;

	private final GameDOMapperExt gameDOMapperExt;


	/**
	 * 	获取自身权限下的子游戏列表
	 * @param
	 * @return
	 */
	@Override
	public List<WanGameRes> getOwnerRoleGames(){
		List<WanGameRes> resultList = new ArrayList<>();
		//自身角色
		List<Integer> roles = SecurityUtils.getRoles();
		if(roles != null && roles.size() > 0) {
			//含有的角色对应的游戏
			List<AdRoleGame> list = adRoleGameMapper.selectList(Wrappers.<AdRoleGame>query().lambda().in(AdRoleGame::getRoleId, roles));
			if(CollectionUtils.isNotEmpty(list)){
				Set<Integer> typeList = list.stream().map(AdRoleGame::getType).collect(Collectors.toSet());
				//含有全部游戏，子查询所有全部游戏
				if(typeList.contains(AdRoleGameTypeEnum.All.getValue())){
					List<WanGameDO> gameDoList =  gameDOMapperExt.selectList(Wrappers.<WanGameDO>query()
							.lambda().eq(WanGameDO::getStatus, 30).eq(WanGameDO::getIsdelete, 0).orderByAsc(WanGameDO::getId));
					List<WanGameRes> allGameList = gameDoList.stream().map(wanGameDO -> {
						WanGameRes res = new WanGameRes();
						BeanUtils.copyProperties(wanGameDO, res);
						res.setType(AdRoleGameTypeEnum.GAMEID.getValue());
						return res;
					}).collect(Collectors.toList());
					return allGameList;
				}
				Set<WanGameRes> gameList = new HashSet<>();
				list.forEach(item -> {
					//0  全部游戏  1 主游戏   2  子游戏
					Integer type = item.getType();

					if(type.equals(AdRoleGameTypeEnum.PGID.getValue())){
						//查询所有子游戏
						//查询主游戏id的子游戏列表
						List<WanGameDO> pDOlist =  gameDOMapperExt.selectList(Wrappers.<WanGameDO>query()
								.lambda().eq(WanGameDO::getPgid, item.getGameid()).eq(WanGameDO::getStatus, 30).eq(WanGameDO::getIsdelete, 0).orderByAsc(WanGameDO::getId));
						List<WanGameRes> pResList = pDOlist.stream().map(wanGameDO -> {
							WanGameRes res = new WanGameRes();
							BeanUtils.copyProperties(wanGameDO, res);
							res.setType(AdRoleGameTypeEnum.GAMEID.getValue());
							res.setPgid(item.getGameid());
							return res;
						}).collect(Collectors.toList());
						gameList.addAll(pResList);
					}else if(type.equals(AdRoleGameTypeEnum.GAMEID.getValue())){
						WanGameDO wanGameDO = gameDOMapperExt.selectById(item.getGameid());
						WanGameRes res = new WanGameRes();
						BeanUtils.copyProperties(wanGameDO, res);
						res.setType(AdRoleGameTypeEnum.GAMEID.getValue());
						gameList.add(res);
					}
				});
				return new ArrayList<>(gameList).stream().sorted(Comparator.comparingLong(WanGameRes::getId)).collect(Collectors.toList());
			}
		}
		return resultList;
	}

	/**
	 * 	获取自身权限下的子游戏ID列表
	 * @param
	 * @return
	 */
	@Override
	public List<Long> getOwnerRoleGameIds(){
		List<WanGameRes> list = this.getOwnerRoleGames();
		List<Long> gameIdList = list.stream().map(WanGameRes::getId).collect(Collectors.toList());
		return gameIdList;
	}



	/**
	 * 	获取自身权限下的主游戏ID列表
	 * @param
	 * @return
	 */
	@Override
	public List<Long> getOwnerRolePgIds(){
		List<WanGameRes> list = this.getOwnerRoleGames();
		Set<Long> pgIdSet = list.stream().map(WanGameRes::getPgid).collect(Collectors.toSet());
		List<Long> pgIdList = new ArrayList<>(pgIdSet);
		return pgIdList;
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveRoleUser(List<AdRoleGameReq> list){
		adRoleGameMapper.delete(Wrappers.<AdRoleGame>query().lambda().eq(AdRoleGame::getRoleId, list.get(0).getRoleId()));
		for(AdRoleGameReq adRoleGameReq : list ){
			AdRoleGame adRoleGame = new AdRoleGame();
			BeanUtils.copyProperties(adRoleGameReq, adRoleGame);
			adRoleGameMapper.insert(adRoleGame);
		}
	}
}
