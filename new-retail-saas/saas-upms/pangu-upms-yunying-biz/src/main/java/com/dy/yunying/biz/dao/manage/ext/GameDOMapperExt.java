package com.dy.yunying.biz.dao.manage.ext;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.dto.SonGameDto;
import com.dy.yunying.api.entity.WanGameDO;
import com.dy.yunying.api.req.WanGameReq;
import com.dy.yunying.api.resp.hongbao.NodeData;
import com.dy.yunying.api.resp.hongbao.NodeParentData;
import com.dy.yunying.api.vo.WanGameReqVo;
import com.dy.yunying.api.vo.WanGameVersionVO;
import com.dy.yunying.biz.dao.manage.WanGameDOMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;


public interface GameDOMapperExt extends WanGameDOMapper {
	/**
	 * 游戏列表
	 *
	 * @param
	 * @return
	 */
	IPage<WanGameReqVo> selectGameList(WanGameReq record);

	/**
	 * 根据查询条件获取游戏信息
	 *
	 * @param map
	 * @return
	 */
	WanGameReqVo selectWanGameByCond(Map<String, Object> map);

	/**
	 * 根据查询条件获取游戏list
	 *
	 * @param
	 * @return
	 */
	List<WanGameDO> selectWanGameListByCond(WanGameDO req);

	/**
	 * 删除，批量删除
	 *
	 * @param
	 * @return
	 */
	boolean updateGameIsDelAndStatusById(BigInteger gid);

	/**
	 * 新增游戏
	 *
	 * @param record
	 * @return
	 */
	boolean insertWanGame(WanGameReqVo record);

	/**
	 * 修改游戏
	 *
	 * @author cx
	 * @date 2020年7月24日11:07:02
	 */
	int updateWanGameByPrimaryKey(WanGameReqVo entity);

	/**
	 * 根据游戏统计充值金额
	 *
	 * @param
	 * @return
	 */
	List<WanGameReqVo> statRechargeSumByGame();

	/**
	 * 查询游戏及版本信息
	 */
	WanGameDO selectVOByPK(Long id);

	WanGameVersionVO selectVersionVOByPK(@Param("id") Long id, @Param("packType") Integer packType);

	List<WanGameVersionVO> versionVOList(WanGameDO req);

	List<WanGameDO> selectVOByPKs(List<Long> gameIdList);

	List<SonGameDto> selectDtoByPKs(List<Long> gameIdList);

	List<SonGameDto> getAutoPackListByPgid(Long pgid);

	List<NodeData> selectChildGameList(Long pgid);

	List<NodeParentData> selectAllGameList();
}