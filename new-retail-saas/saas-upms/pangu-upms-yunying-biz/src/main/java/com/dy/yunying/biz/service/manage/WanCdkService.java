/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.WanCdk;
import com.dy.yunying.api.req.hongbao.GenerateCdkReq;
import com.dy.yunying.api.req.hongbao.GrantCdkReq;
import com.dy.yunying.api.req.hongbao.ImportCdkReq;
import com.dy.yunying.api.resp.hongbao.AdminUserVo;
import com.dy.yunying.api.resp.hongbao.CdkVo;
import com.dy.yunying.api.resp.hongbao.OptionData;
import com.pig4cloud.pig.common.core.util.R;

import java.io.IOException;
import java.util.List;

/**
 * CDK信息表
 *
 * @author yuwenfeng
 * @date 2021-11-16 09:57:41
 */
public interface WanCdkService extends IService<WanCdk> {

	List<AdminUserVo> selectUserList();

	Page<CdkVo> selectCdkPage(Page<WanCdk> page, QueryWrapper<WanCdk> query);

	R generateCdk(GenerateCdkReq req);

	R grantCdk(GrantCdkReq req);

	List<OptionData> selectActityListByCdkSourceType(Integer cdkSourceType);
	/**
	 * 导入代金券数据
	 *
	 * @param req
	 */
	void importCdk(ImportCdkReq req) throws IOException;

}
