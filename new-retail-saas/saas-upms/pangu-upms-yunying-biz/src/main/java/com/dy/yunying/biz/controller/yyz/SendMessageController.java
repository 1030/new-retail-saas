package com.dy.yunying.biz.controller.yyz;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.dy.yunying.api.base.BaseDto;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.req.yyz.GameAppointmentImportSendReq;
import com.dy.yunying.api.req.yyz.GameAppointmentReq;
import com.dy.yunying.api.req.yyz.MsgReq;
import com.dy.yunying.api.req.yyz.SendMassageReq;
import com.dy.yunying.biz.service.yyz.SendMassageService;
import com.dy.yunying.biz.utils.ValidatorUtil;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @Author: kongyanfang
 * @Date: 2019/11/5 19:23
 */
@Inner(value = false)
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/sendMessage")
public class SendMessageController {

	private final SendMassageService sendMassageService;

	@RequestMapping("/sendSms")
	public R sendSms(@RequestBody MsgReq req) {
		log.info("sendSms begin, req ={}", BaseDto.toString(req));
		R x = checkRequest(req);
		if (x != null) return x;
		if(Objects.isNull(req.getType())){
			//默认预约类型：幻兽仓角预约
			req.setType(Constant.TYPE_1);
		}
		//验证图形验证码
		return sendMassageService.sendSms(req);
	}

	//	@SysLog("批量短信")
	@RequestMapping("/batchMsg")
	public R batchMsg(@RequestBody SendMassageReq req) {
		if (Objects.isNull(req)) {
			return R.failed("请求参数错误");
		}
		if (Objects.isNull(req.getCode())) {
			return R.failed("模板参数不能为空");
		}
		return this.sendMassageService.batchSendMsg(req);
	}

	/**
	 * 用户电话导入
	 *
	 * @param req
	 * @return
	 */
	@SysLog("物品导入")
	@RequestMapping("/importSendSms")
	public R bathImportSendSms(GameAppointmentImportSendReq req) {
		return this.sendMassageService.bathImportSend(req);
	}


	/***
	 * 校验参数
	 * @param req
	 * @return
	 */
	public R checkRequest(MsgReq req) {
		if (StringUtils.isBlank(req.getMobile()) || Objects.isNull(req.getCodeType())
				|| StringUtils.isBlank(req.getCaptcha())) {
			return R.failed("请求参数不合法");
		}
		if (!ValidatorUtil.isMobile(req.getMobile())) {
			return R.failed("手机号格式不正确");
		}
		return null;
	}


}
