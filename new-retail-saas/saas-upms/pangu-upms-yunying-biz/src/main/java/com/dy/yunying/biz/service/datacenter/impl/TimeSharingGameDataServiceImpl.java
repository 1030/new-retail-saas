package com.dy.yunying.biz.service.datacenter.impl;

import com.dy.yunying.api.datacenter.dto.TimeSharingGameDTO;
import com.dy.yunying.api.datacenter.vo.TimeSharingGameDataVO;
import com.dy.yunying.biz.dao.datacenter.impl.TimeSharingGameDataDao;
import com.dy.yunying.biz.service.datacenter.TimeSharingGameDataService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author sunyq
 * @date 2022/8/17 14:59
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TimeSharingGameDataServiceImpl implements TimeSharingGameDataService {

	private final TimeSharingGameDataDao timeSharingGameDataDao;

	private Map<String,String> timeMap = new HashMap<>();

	@PostConstruct
	public void initTimeMap(){
		timeMap.put("0","01:00");
		timeMap.put("1","02:00");
		timeMap.put("2","03:00");
		timeMap.put("3","04:00");
		timeMap.put("4","05:00");
		timeMap.put("5","06:00");
		timeMap.put("6","07:00");
		timeMap.put("7","08:00");
		timeMap.put("8","09:00");
		timeMap.put("9","10:00");
		timeMap.put("10","11:00");
		timeMap.put("11","12:00");
		timeMap.put("12","13:00");
		timeMap.put("13","14:00");
		timeMap.put("14","15:00");
		timeMap.put("15","16:00");
		timeMap.put("16","17:00");
		timeMap.put("17","18:00");
		timeMap.put("18","19:00");
		timeMap.put("19","20:00");
		timeMap.put("20","21:00");
		timeMap.put("21","22:00");
		timeMap.put("22","23:00");
		timeMap.put("23","24:00");
	}

	@Override
	public List<TimeSharingGameDataVO> list(TimeSharingGameDTO req) {

		List<TimeSharingGameDataVO> list = timeSharingGameDataDao.list(req);

		Map<String, Object> map = timeSharingGameDataDao.totalData(req);

		List<TimeSharingGameDataVO> resultList = new ArrayList<>();
		//处理数据
		String startTime = req.getStartTime();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.now();
		String today = localDate.format(formatter);
		int hour = 0;
		if (today.equals(startTime)){
			LocalDateTime now = LocalDateTime.now();
			hour = now.getHour();
		} else {
			hour = 23;
		}
		for (int i = hour; i >= 0; i--) {
			TimeSharingGameDataVO timeSharingGameDataVO = new TimeSharingGameDataVO();
			timeSharingGameDataVO.setDay(startTime);
			timeSharingGameDataVO.setHour(i+"");
			resultList.add(timeSharingGameDataVO);
		}
		for (TimeSharingGameDataVO timeSharingGameDataVO : resultList) {
			//计算累计金额,填充累计人数
			String hour1 = timeSharingGameDataVO.getHour();
			timeSharingGameDataVO.setHour(timeMap.get(hour1));
			BigDecimal totalMoney = BigDecimal.ZERO;
			for (TimeSharingGameDataVO sharingGameDataVO : list) {
				if (StringUtils.isNotEmpty(sharingGameDataVO.getHour())){
					if (Integer.valueOf(sharingGameDataVO.getHour())<=Integer.valueOf(hour1)){
						totalMoney = totalMoney.add(new BigDecimal(sharingGameDataVO.getActivePayAmounts()));
					}
				}
				if (hour1.equals(sharingGameDataVO.getHour())) {
					BeanUtils.copyProperties(sharingGameDataVO,timeSharingGameDataVO);
					timeSharingGameDataVO.setHour(timeMap.get(hour1));
				}
			}
			Object total = map.get(timeMap.get(hour1));
			if (ObjectUtils.isNotEmpty(total)){
				timeSharingGameDataVO.setTotalFeeAccounts(total.toString());
			} else {
				timeSharingGameDataVO.setTotalFeeAccounts("0");
			}
			timeSharingGameDataVO.setTotalPayAmounts(totalMoney.stripTrailingZeros().toPlainString());
			timeSharingGameDataVO.setNewPayFeeRate(timeSharingGameDataVO.getNewPayFeeRate()+"%");
			timeSharingGameDataVO.setRegRate(timeSharingGameDataVO.getRegRate()+"%");
			timeSharingGameDataVO.setPayFeeRate(timeSharingGameDataVO.getPayFeeRate()+"%");
			timeSharingGameDataVO.setOldPayFeeRate(timeSharingGameDataVO.getOldPayFeeRate()+"%");
		}
		// 排序
		List<TimeSharingGameDataVO> timeSharingGameDataVOList = dealWithSort(resultList, req);
		return timeSharingGameDataVOList;
	}

	private List<TimeSharingGameDataVO> dealWithSort(List<TimeSharingGameDataVO> initList, TimeSharingGameDTO req) {
		List<TimeSharingGameDataVO> timeSharingGameDataVOList = new ArrayList<>();
		String kpiValue = req.getKpiValue();
		String sort = req.getSort();
		if (StringUtils.isNotEmpty(kpiValue) && StringUtils.isNotEmpty(sort)){
			switch (kpiValue){
				case "hour":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getHour,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getHour)).collect(Collectors.toList()));
					}
					break;
				case "deviceNums":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getDeviceNums).reversed()).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getDeviceNums)).collect(Collectors.toList()));
					}
					break;
				case "accountNums":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getAccountNums,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getAccountNums)).collect(Collectors.toList()));
					}
					break;
				case "regRate":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getRegRate,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getRegRate)).collect(Collectors.toList()));
					}
					break;
				case "activePayAmounts":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getActivePayAmounts,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getActivePayAmounts)).collect(Collectors.toList()));
					}
					break;
				case "activeFeeAccounts":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getActiveFeeAccounts,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getActiveFeeAccounts)).collect(Collectors.toList()));
					}
					break;
				case "payFeeRate":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getPayFeeRate,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getPayFeeRate)).collect(Collectors.toList()));
					}
					break;
				case "arpu":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getArpu,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getArpu)).collect(Collectors.toList()));
					}
					break;
				case "arppu":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getArppu,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getArppu)).collect(Collectors.toList()));
					}
					break;
				case "totalFeeAccounts":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getTotalFeeAccounts,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getTotalFeeAccounts)).collect(Collectors.toList()));
					}
					break;
				case "totalPayAmounts":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getTotalPayAmounts,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getTotalPayAmounts)).collect(Collectors.toList()));
					}
					break;
				case "newPayAmounts":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewPayAmounts,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewPayAmounts)).collect(Collectors.toList()));
					}
					break;
				case "newPayNums":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewPayNums,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewPayNums)).collect(Collectors.toList()));
					}
					break;
				case "newPayFeeRate":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewPayFeeRate,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewPayFeeRate)).collect(Collectors.toList()));
					}
					break;
				case "newArpu":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewArpu,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewArpu)).collect(Collectors.toList()));
					}
					break;
				case "newArppu":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewArppu,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getNewArppu)).collect(Collectors.toList()));
					}
					break;
				case "oldActivePayAmounts":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldActivePayAmounts,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldActivePayAmounts)).collect(Collectors.toList()));
					}
					break;
				case "oldActiveFeeAccounts":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldActiveFeeAccounts,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldActiveFeeAccounts)).collect(Collectors.toList()));
					}
					break;
				case "oldPayFeeRate":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldPayFeeRate,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldPayFeeRate)).collect(Collectors.toList()));
					}
					break;
				case "oldArpu":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldArpu,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldArpu)).collect(Collectors.toList()));
					}
					break;
				case "oldArppu":
					if ("desc".equalsIgnoreCase(sort)){
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldArppu,Comparator.reverseOrder())).collect(Collectors.toList()));
					} else {
						timeSharingGameDataVOList.addAll(initList.stream().sorted(Comparator.comparing(TimeSharingGameDataVO::getOldArppu)).collect(Collectors.toList()));
					}
					break;
				default:
					timeSharingGameDataVOList.addAll(initList);
					break;

			}
		} else {
			timeSharingGameDataVOList.addAll(initList);
		}
		return timeSharingGameDataVOList;
	}

	@Override
	public TimeSharingGameDataVO collect(TimeSharingGameDTO req) {
		TimeSharingGameDataVO collect = timeSharingGameDataDao.collect(req);
		if (ObjectUtils.isNotEmpty(collect)){
			collect.setDay("汇总");
			collect.setHour("-");
			collect.setNewPayFeeRate(collect.getNewPayFeeRate()+"%");
			collect.setRegRate(collect.getRegRate()+"%");
			collect.setPayFeeRate(collect.getPayFeeRate()+"%");
			collect.setOldPayFeeRate(collect.getOldPayFeeRate()+"%");
		}
		return collect;
	}
}
