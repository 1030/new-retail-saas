package com.dy.yunying.biz.service.hbdata.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.dy.yunying.api.req.sign.SignActivityDataReq;
import com.dy.yunying.api.resp.SignActivityDataRes;
import com.dy.yunying.biz.dao.ads.sign.SignActivityMapper;
import com.dy.yunying.biz.dao.hbdataclickhouse.impl.SignActiveDataDao;
import com.dy.yunying.biz.service.hbdata.SignActiveDataService;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.api.util.StringUtils;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author chenxiang
 * @className SignActiveDataServiceImpl
 * @date 2021/12/2 20:58
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class SignActiveDataServiceImpl implements SignActiveDataService {
	@Autowired
	private SignActiveDataDao signActiveDataDao;

	private final SignActivityMapper signActivityMapper;

	/**
	 * 签到活动数据
	 * @param req
	 * @return
	 */
	public R dataList(SignActivityDataReq req){
		Map<String,Object> result = new HashMap<>();
		SignActivityDataRes summary = new SignActivityDataRes();
		// 查mysql统计数据
		List<SignActivityDataRes> activityDataList = signActivityMapper.selectSignActivityDataList(req);
		if (CollectionUtil.isNotEmpty(activityDataList)) {
			Set<String> activityIds = activityDataList.stream().map(SignActivityDataRes::getActivityId).collect(Collectors.toSet());
			// 查clickhouse统计数据
			List<SignActivityDataRes> signClickDataList = signActiveDataDao.signActivityDataList(req, activityIds);
			if (CollectionUtil.isNotEmpty(signClickDataList)) {
				// 领取奖励角色数
				Integer receiveRewardNum = 0;
				// 充值总金额
				BigDecimal totalRechargeAmount = new BigDecimal("0");
				// 查clickhouse汇总统计数据
				List<SignActivityDataRes> summaryClickDataList = signActiveDataDao.summarySignActivityDataList(req, activityIds);
				if (CollectionUtil.isNotEmpty(summaryClickDataList)) {
					summary = summaryClickDataList.get(0);
				}
				for (SignActivityDataRes clickData : signClickDataList) {
					for (SignActivityDataRes activityData : activityDataList) {
						if (StringUtils.isNotBlank(activityData.getActivityId()) && StringUtils.isNotBlank(clickData.getActivityId())
								&& activityData.getActivityId().equals(clickData.getActivityId())) {
							clickData.setActivityName(activityData.getActivityName());
							if (StringUtils.isNotBlank(activityData.getDate()) && StringUtils.isNotBlank(clickData.getDate())
									&& activityData.getDate().equals(clickData.getDate())) {
								clickData.setReceiveRewardNum(activityData.getReceiveRewardNum());
								clickData.setTotalRechargeAmount(activityData.getTotalRechargeAmount());
								receiveRewardNum += activityData.getReceiveRewardNum();
								totalRechargeAmount = totalRechargeAmount.add(activityData.getTotalRechargeAmount());
							}
						}
					}
				}
				summary.setReceiveRewardNum(receiveRewardNum);
				summary.setTotalRechargeAmount(totalRechargeAmount);
			}
			result.put("list",signClickDataList);
			result.put("summary",summary);
			return R.ok(result);
		}
		result.put("list", Lists.newArrayList());
		result.put("summary",summary);
		return R.ok(result);
	}
}
