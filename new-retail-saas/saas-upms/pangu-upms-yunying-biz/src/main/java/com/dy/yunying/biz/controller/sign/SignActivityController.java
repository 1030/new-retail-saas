/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.controller.sign;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.entity.sign.SignActivity;
import com.dy.yunying.api.enums.ActivityStatusEnum;
import com.dy.yunying.api.req.hongbao.OnlineStatusReq;
import com.dy.yunying.api.req.hongbao.SelectSignActivityPageReq;
import com.dy.yunying.api.req.sign.SignActivityReq;
import com.dy.yunying.api.resp.hongbao.NodeData;
import com.dy.yunying.api.resp.hongbao.SignActivityVo;
import com.dy.yunying.biz.service.sign.SignActivityService;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 签到活动表
 *
 * @author yuwenfeng
 * @date 2021-11-30 19:46:19
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/signactivity")
@Api(value = "signactivity", tags = "签到活动表管理")
@Slf4j
public class SignActivityController {

	private final SignActivityService signActivityService;

	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/selectSignActivityPage")
	@PreAuthorize("@pms.hasPermission('signactivity_get')")
	public R<Page<SignActivityVo>> selectSignActivityPage(Page<SignActivity> page, SelectSignActivityPageReq selectReq) {
		try {
			QueryWrapper<SignActivity> query = new QueryWrapper<SignActivity>();
			if (StringUtils.isNotBlank(selectReq.getActivityName())) {
				query.and(wrapper -> wrapper.like("id", selectReq.getActivityName()).or().like("activity_name", selectReq.getActivityName()));
			}
			if (StringUtils.isNotBlank(selectReq.getStartTime())) {
				query.ge("DATE_FORMAT(start_time,'%Y-%m-%d')", selectReq.getStartTime());
			}
			if (StringUtils.isNotBlank(selectReq.getFinishTime())) {
				query.le("DATE_FORMAT(start_time,'%Y-%m-%d')", selectReq.getFinishTime());
			}
			if (null != selectReq.getParentGameId() && selectReq.getParentGameId() > 0) {
				query.eq("parent_game_id", selectReq.getParentGameId());
			}
			if (null != selectReq.getActivityStatus()) {
				query.eq("activity_status", selectReq.getActivityStatus());
				if (ActivityStatusEnum.ACTIVITING.getStatus().equals(selectReq.getActivityStatus())) {
					query.ge("DATE_FORMAT(finish_time,'%Y-%m-%d %H:%i:%S')", DateUtils.getcurrentTime());
				}
			}
			query.eq("deleted", Constant.DEL_NO);
			query.orderByDesc("start_time");
			return R.ok(signActivityService.selectSignActivityPage(page, query));
		} catch (Exception e) {
			log.error("签到活动管理-分页查询-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "根据父游戏ID获取区服列表", notes = "根据父游戏ID获取区服列表")
	@GetMapping("/selectAreaListByPgameIds")
	public R<List<NodeData>> selectAreaListByPgameIds(@Valid String pGgmeIds) {
		try {
			return R.ok(signActivityService.selectAreaListByPgameIds(pGgmeIds));
		} catch (Exception e) {
			log.error("签到活动管理-根据父游戏ID获取区服列表-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "新增签到活动", notes = "新增签到活动")
	@SysLog("新增签到活动")
	@PostMapping("/addSignActivity")
	@PreAuthorize("@pms.hasPermission('signactivity_add')")
	public R addSignActivity(@Valid @RequestBody SignActivityReq signActivityReq) {
		try {
			return signActivityService.addSignActivity(signActivityReq);
		} catch (Exception e) {
			log.error("签到活动管理-新增签到活动-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "修改签到活动", notes = "修改签到活动")
	@SysLog("修改签到活动")
	@PostMapping("/editSignActivity")
	@PreAuthorize("@pms.hasPermission('signactivity_edit')")
	public R editSignActivity(@Valid @RequestBody SignActivityReq signActivityReq) {
		try {
			return signActivityService.editSignActivity(signActivityReq);
		} catch (Exception e) {
			log.error("签到活动管理-新增签到活动-接口：{}", e.getMessage());
			return R.failed("接口异常:" + e.getMessage());
		}
	}

	@ApiOperation(value = "上线/下线签到活动", notes = "上线/下线签到活动")
	@SysLog("上线/下线签到活动")
	@GetMapping("/onlineSignActivity")
	@PreAuthorize("@pms.hasPermission('signactivity_online')")
	public R onlineSignActivity(@Valid OnlineStatusReq req) {
		try {
			return signActivityService.onlineSignActivity(req);
		} catch (Exception e) {
			log.error("签到活动管理-上线/下线签到活动-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	/**
	 * 签到活动下拉列表查询
	 *
	 * @return
	 */
	@PostMapping("/selectActivityList")
	public R<List<SignActivity>> selectActivityList(@RequestBody(required = false) SelectSignActivityPageReq activityReq) {
		final List<SignActivity> activityList = signActivityService.selectActivityList(activityReq);
		return R.ok(activityList);
	}


	@ApiOperation(value = "复制签到活动", notes = "复制签到活动")
	@SysLog("复制签到活动")
	@PostMapping("/copySignActivity")
	@PreAuthorize("@pms.hasPermission('signactivity_copy')")
	public R copySignActivity(@Valid @RequestBody SignActivityReq signActivityReq) {
		try {
			return signActivityService.copySignActivity(signActivityReq);
		} catch (Exception e) {
			log.error("签到活动管理-复制签到活动-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

}
