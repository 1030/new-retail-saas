package com.dy.yunying.biz.service.raffle;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.raffle.RaffleReceiveRecord;
import com.dy.yunying.api.req.raffle.RaffleReceiveRecordReq;
import com.dy.yunying.api.resp.raffle.RaffleReceiveRecordExportRes;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 领取记录信息表
 * @author  chenxiang
 * @version  2022-11-08 16:01:24
 * table: raffle_receive_record
 */
public interface RaffleReceiveRecordService extends IService<RaffleReceiveRecord> {
	/**
	 * 分页
	 * @param record
	 * @return
	 */
	R getPage(RaffleReceiveRecordReq record);
	/**
	 * 编辑
	 * @param record
	 * @return
	 */
	R edit(RaffleReceiveRecord record);
	/**
	 * 导出列表
	 * @param record
	 * @return
	 */
	List<RaffleReceiveRecordExportRes> selectExportList(RaffleReceiveRecordReq record);
}


