package com.dy.yunying.biz.controller.datacenter;

import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.dto.ConversionAnalysisDto;
import com.dy.yunying.api.vo.ConversionAnalysisVo;
import com.dy.yunying.biz.service.datacenter.ConversionAnalysisService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.util.CommonUtils;
import com.pig4cloud.pig.common.core.util.ECollectionUtil;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description 转化分析报表
 * @Author chengang
 * @Date 2022/2/28
 */
@RestController
@RequestMapping("/dataCenter/conversion")
@RequiredArgsConstructor
public class ConversionAnalysisController {

	private final ConversionAnalysisService conversionAnalysisService;

	/**
	 * 转化分析报表总数
	 *
	 * @param analysisDto
	 * @return
	 */
	@PostMapping("/count")
	public R<Map<String, Object>> count(@RequestBody ConversionAnalysisDto analysisDto) {
		if (!CommonUtils.isPositiveNumber(analysisDto.getLevel())) {
			return R.failed("等级只能是正整数");
		}
		this.requireConversionQueryBean(analysisDto);
		final int page = conversionAnalysisService.count(analysisDto);
		return R.ok(Collections.singletonMap("total", page));
	}

	/**
	 * 转化分析报表分页
	 *
	 * @param analysisDto
	 * @return
	 */
	@PostMapping("/page")
	public R<List<ConversionAnalysisVo>> page(@RequestBody ConversionAnalysisDto analysisDto) {
		if (!CommonUtils.isPositiveNumber(analysisDto.getLevel())) {
			return R.failed("等级只能是正整数");
		}
		this.requireConversionQueryBean(analysisDto);
		final List<ConversionAnalysisVo> page = conversionAnalysisService.page(analysisDto, true);
		return R.ok(page);
	}

	/**
	 * 转化分析报表导出
	 *
	 * @param analysisDto
	 * @return
	 */
	@GetMapping("/export")
	public void export(ConversionAnalysisDto analysisDto, HttpServletRequest request, HttpServletResponse response) {
		this.requireConversionQueryBean(analysisDto);
		// 获取列表
		final List<ConversionAnalysisVo> list = conversionAnalysisService.page(analysisDto, false);
		analysisDto.setCycleType(4).setGroupBys(Collections.emptyList()).setOrderByName(null).setOrderByAsc(null);
		// 获取汇总行
		final List<ConversionAnalysisVo> total = conversionAnalysisService.page(analysisDto, false);
		list.addAll(total);

		final String sheetName = "转化分析报表导出";
		// list对象转listMap
		List<Map<String, Object>> resultMapList = MapUtils.objectsToMaps(list);
		// 为百分比的值拼接百分号
		final String defaultValue = "0.00%";
		ExportUtils.spliceSuffix(resultMapList, Constant.PERCENT, defaultValue,
				"clickRatio", "regRatio", "completeRegRatio", "certifiedRegRatio", "noCertifiedRegRatio", "createRoleUpgradeRatio", "payRatio");
		// 导出
		ExportUtils.exportExcelData(request, response, "转化分析报表导出" + DateUtils.getCurrentTimeNoUnderline() + ".xlsx",
				sheetName, analysisDto.getExportTitles(), analysisDto.getExportColumns(), resultMapList);
	}

	private void requireConversionQueryBean(ConversionAnalysisDto analysisDto) {
		analysisDto.setPgids(ECollectionUtil.stringToLongSet(analysisDto.getPgidArr()));
		analysisDto.setGameids(ECollectionUtil.stringToLongSet(analysisDto.getGameidArr()));
		analysisDto.setDeptIds(ECollectionUtil.stringToLongSet(analysisDto.getDeptIdArr()));
		analysisDto.setUserGroupIds(ECollectionUtil.stringToLongSet(analysisDto.getUserGroupIdArr()));
		analysisDto.setInvestorIds(ECollectionUtil.stringToLongSet(analysisDto.getInvestorIdArr()));
		analysisDto.setGroupBys(Optional.ofNullable(analysisDto.getGroupByArr())
				.filter(StringUtils::isNotEmpty)
				.map(e -> Arrays.asList(e.split(",")))
				.orElse(Collections.emptyList())
				.stream()
				.filter(StringUtils::isNotEmpty)
				.collect(Collectors.toList()));
	}
}
