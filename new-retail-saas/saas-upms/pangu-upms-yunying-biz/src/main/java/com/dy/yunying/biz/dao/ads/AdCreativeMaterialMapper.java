/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.dao.ads;


import com.dy.yunying.api.vo.MaterialTopVo;
import com.dy.yunying.api.vo.MaterialDaoVo;
import com.dy.yunying.api.vo.RoiTopTenDaoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 素材同步平台信息表
 *
 * @author xzl
 * @date 2021-06-23 14:32:25
 */
public interface AdCreativeMaterialMapper{

	List<MaterialTopVo> getTopMaterial(@Param("list") List<String> list);

	List<MaterialTopVo> getTopExperience(@Param("list") List<RoiTopTenDaoVo> list);

	List<MaterialDaoVo> getFutureMaterial(@Param("list") List<String> list);

}
