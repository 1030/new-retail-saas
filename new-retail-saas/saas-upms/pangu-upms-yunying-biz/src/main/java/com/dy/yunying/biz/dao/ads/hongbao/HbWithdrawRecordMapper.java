package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbWithdrawRecord;
import org.apache.ibatis.annotations.Mapper;

import java.math.BigDecimal;

/**
 * 提现记录
 *
 * @author zhuxm
 * @version 2021-10-28 20:25:44
 */
@Mapper
public interface HbWithdrawRecordMapper extends BaseMapper<HbWithdrawRecord> {

	/**
	 * 根据ID将状态修改为提现中
	 *
	 * @param record
	 * @return
	 */
	int updateStatusToWithdrawingById(HbWithdrawRecord record);

	/**
	 * 根据ID将状态修改为拒绝
	 *
	 * @param record
	 * @return
	 */
	int updateStatusToRejectById(HbWithdrawRecord record);

	/**
	 * 查询已通过审核的金额
	 *
	 * @param activityId
	 * @return
	 */
	BigDecimal selectPassedExpendBalance(Long activityId);

}


