package com.dy.yunying.biz.service.datacenter.impl;

import com.dy.yunying.api.datacenter.dto.TimeGameRegDto;
import com.dy.yunying.api.datacenter.vo.TimeGameDeviceRegVO;
import com.dy.yunying.biz.dao.datacenter.impl.TimeGameRegDao;
import com.dy.yunying.biz.service.datacenter.TimeGameRegService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * dogame 分时游戏注册
 * @author hma
 * @date 2022/8/19 15:37
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TimeGameRegServiceImpl implements TimeGameRegService {

	private final TimeGameRegDao timeGameRegDao;

	@Override
	public R<List<TimeGameDeviceRegVO>> page(TimeGameRegDto req) {
		List<TimeGameDeviceRegVO> list;
		try {
			req.setCycle(1);
			list = timeGameRegDao.list(req);
		} catch (Exception e) {
			log.error("分时游戏注册数据查询失败:{}",e);
			return R.failed("分时游戏注册数据查询失败...");
		}
		return R.ok(list) ;
	}

	@Override
	public R count(TimeGameRegDto req) {
		return R.ok(timeGameRegDao.count(req));
	}

	@Override
	public R<List<TimeGameDeviceRegVO>> collect(TimeGameRegDto req) {
		try {
		Long count = timeGameRegDao.count(req);
		if (count==0){
			return R.ok(new ArrayList<>());
		}
		req.setCycle(2);
		return R.ok(timeGameRegDao.collect(req));
		} catch (Exception e) {
			log.error("分时游戏注册汇总查询失败:{}",e);
			return R.failed("分时游戏注册数据查询失败...");
		}

	}

}
