package com.dy.yunying.biz.service.yyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.GameAppointmentSchedule;
import com.dy.yunying.api.entity.GameAppointmentScheduleUser;
import com.dy.yunying.api.req.yyz.GameAppointmentReq;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleUserReq;
import com.dy.yunying.api.resp.yyz.GameAppointmentScheduleUserRes;
import com.dy.yunying.biz.dao.manage.GameAppointmentMapper;
import com.dy.yunying.biz.dao.manage.GameAppointmentScheduleMapper;
import com.dy.yunying.biz.dao.manage.GameAppointmentScheduleUserMapper;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleUserService;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 游戏预约进度表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_Schedule
 */
@Log4j2
@Service("GameAppointmentScheduleUserService")
@RequiredArgsConstructor
public class GameAppointmentScheduleUserServiceImpl extends ServiceImpl<GameAppointmentScheduleUserMapper, GameAppointmentScheduleUser> implements GameAppointmentScheduleUserService {

	private final GameAppointmentMapper gameAppointmentMapper;

	private final GameAppointmentScheduleUserMapper gameAppointmentScheduleUserMapper;

	private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	private static SimpleDateFormat formatss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public R getPage(GameAppointmentScheduleUserReq req) {
		IPage<GameAppointmentScheduleUser> iPage = gameAppointmentScheduleUserMapper.selectGameAppointmentUserList(req);
		return R.ok(iPage);
	}
 
	@Override
	public List<GameAppointmentScheduleUser> getOnlineList(GameAppointmentSchedule req) {
		QueryWrapper<GameAppointmentScheduleUser> wrapper = new QueryWrapper<>();
		//设置取出当前值的没有删除的list
		wrapper.eq("type",req.getType());
		wrapper.eq("is_delete",1);
		//获得当前存入mysql的数据
		List<GameAppointmentScheduleUser> list = this.list(wrapper);
		if(CollectionUtils.isEmpty(list)){
			list=Lists.newArrayList();
		}
		list.add(addOnlineNum(req.getType()));
		return list;
	}

	@Override
	public R gameAddSchedule(GameAppointmentScheduleUserReq req) {
		GameAppointmentScheduleUser appointmentScheduleUser = this.getOne(Wrappers.<GameAppointmentScheduleUser>lambdaQuery().eq(GameAppointmentScheduleUser::getIsDelete,1).eq(GameAppointmentScheduleUser::getType, req.getType()).eq(GameAppointmentScheduleUser::getConventionName, req.getConventionName()));
		if (Objects.nonNull(appointmentScheduleUser)) {
			log.info("标签规则名称已存在");
			return R.failed(1, "标签规则名称已存在");
		}

		Date date =new Date();
		GameAppointmentScheduleUser gameAppointmentScheduleUser =new GameAppointmentScheduleUser();
		gameAppointmentScheduleUser.setType(req.getType());
		gameAppointmentScheduleUser.setConventionTypeId(Integer.valueOf(req.getConventionTypeId()));
		gameAppointmentScheduleUser.setConventionName(req.getConventionName());
		gameAppointmentScheduleUser.setCreateTime(date);
		gameAppointmentScheduleUser.setUpdateTime(date);

		if("2".equals(req.getConventionTypeId())){
			gameAppointmentScheduleUser.setNumber(req.getNumber());
		} else if("3".equals(req.getConventionTypeId())){
			if(StringUtils.isNotBlank(req.getScheduleHour()) && req.getScheduleHour().split("-").length >=2) {
				gameAppointmentScheduleUser.setScheduleType(req.getScheduleType());
				gameAppointmentScheduleUser.setScheduleHour(req.getScheduleHour());
				String startHour = req.getScheduleHour().split("-")[0];
				String endHour = req.getScheduleHour().split("-")[1];
				gameAppointmentScheduleUser.setStartHour(startHour);
				gameAppointmentScheduleUser.setEndHour(endHour);
			}
			gameAppointmentScheduleUser.setIsStart(Integer.valueOf(req.getIsStart()));
			gameAppointmentScheduleUser.setStartTime(req.getStartTime());
			gameAppointmentScheduleUser.setEndTime(req.getEndTime());
			gameAppointmentScheduleUser.setFrequency(Long.valueOf(req.getFrequency()));
			gameAppointmentScheduleUser.setTimeUnit(Integer.valueOf(req.getTimeUnit()));
			try {
				gameAppointmentScheduleUser.setAddTime(formatss.parse(format.format(date)+":59"));
			} catch (ParseException e) {
				log.error("添加开始规则时间失败");
				return R.failed(6, "添加开始规则时间失败");
			}
			gameAppointmentScheduleUser.setMinNum(Long.valueOf(req.getMinNum()));
			gameAppointmentScheduleUser.setMaxNum(Long.valueOf(req.getMaxNum()));
			gameAppointmentScheduleUser.setExpectNum(req.getExpectNum());
		}else {
			log.error("新增预约游戏人数失败");
			return R.failed(6, "新增预约游戏人数失败");
		}
		boolean flag = this.save(gameAppointmentScheduleUser);
		if (!flag) {
			return R.failed(6, "新增预约游戏人数失败");
		}
		return R.ok(1,"新增预约游戏人数成功");
	}

	@Override
	public R gameEditSchedule(GameAppointmentScheduleUserReq req) {
		GameAppointmentScheduleUser appointmentScheduleUser = this.getOne(Wrappers.<GameAppointmentScheduleUser>lambdaQuery().notIn(GameAppointmentScheduleUser::getId,req.getId()).eq(GameAppointmentScheduleUser::getIsDelete,1).eq(GameAppointmentScheduleUser::getType, req.getType()).eq(GameAppointmentScheduleUser::getConventionName, req.getConventionName()));
		if (Objects.nonNull(appointmentScheduleUser)) {
			if(!req.getId().equals(appointmentScheduleUser.getId().toString())) {
				log.info("标签来源/规则名称已存在");
				return R.failed(null, 1, "标签来源/规则名称已存在");
			}
		}
		GameAppointmentScheduleUser gameAppointmentScheduleUser =new GameAppointmentScheduleUser();
		gameAppointmentScheduleUser.setId(Long.valueOf(req.getId()));
		gameAppointmentScheduleUser.setType(req.getType());
		gameAppointmentScheduleUser.setConventionTypeId(Integer.valueOf(req.getConventionTypeId()));
		gameAppointmentScheduleUser.setConventionName(req.getConventionName());
		gameAppointmentScheduleUser.setUpdateTime(new Date());
		if("2".equals(req.getConventionTypeId())){
			gameAppointmentScheduleUser.setNumber(req.getNumber());
		} else if("3".equals(req.getConventionTypeId())){
			if(StringUtils.isNotBlank(req.getScheduleHour()) && req.getScheduleHour().split("-").length >=2) {
				gameAppointmentScheduleUser.setScheduleType(req.getScheduleType());
				gameAppointmentScheduleUser.setScheduleHour(req.getScheduleHour());
				String startHour = req.getScheduleHour().split("-")[0];
				String endHour = req.getScheduleHour().split("-")[1];
				gameAppointmentScheduleUser.setStartHour(startHour);
				gameAppointmentScheduleUser.setEndHour(endHour);
			}
			gameAppointmentScheduleUser.setIsStart(Integer.valueOf(req.getIsStart()));
			gameAppointmentScheduleUser.setStartTime(req.getStartTime());
			gameAppointmentScheduleUser.setEndTime(req.getEndTime());
			gameAppointmentScheduleUser.setFrequency(Long.valueOf(req.getFrequency()));
			gameAppointmentScheduleUser.setTimeUnit(Integer.valueOf(req.getTimeUnit()));
			gameAppointmentScheduleUser.setMinNum(Long.valueOf(req.getMinNum()));
			gameAppointmentScheduleUser.setMaxNum(Long.valueOf(req.getMaxNum()));
			gameAppointmentScheduleUser.setExpectNum(req.getExpectNum());
		}else {
			log.error("编辑预约游戏人数失败");
			return R.failed(6, "编辑预约游戏人数失败");
		}
		boolean flag = this.updateById(gameAppointmentScheduleUser);
		if (!flag) {
			return R.failed(6,"编辑预约游戏人数失败");
		}

		return R.ok(1,"编辑预约游戏人数成功");
	}

	@Override
	public R isEnable(GameAppointmentScheduleUserReq req) {
		GameAppointmentScheduleUser gameAppointmentScheduleUser = new GameAppointmentScheduleUser();
		gameAppointmentScheduleUser.setId(Long.valueOf(req.getId()));
		gameAppointmentScheduleUser.setType(req.getType());
		gameAppointmentScheduleUser.setIsStart(Integer.valueOf(req.getIsStart()));
		boolean flag = this.updateById(gameAppointmentScheduleUser);
		if (flag) {
			return R.ok(1,"修改成功");
		}
		return R.failed(6,"修改失败");
	}

	@Override
	public R gameDelSchedule(GameAppointmentScheduleUserReq req) {
		GameAppointmentScheduleUser gameAppointmentScheduleUser = new GameAppointmentScheduleUser();
		gameAppointmentScheduleUser.setId(Long.valueOf(req.getId()));
		gameAppointmentScheduleUser.setType(req.getType());
		gameAppointmentScheduleUser.setIsStart(1);
		gameAppointmentScheduleUser.setIsDelete(2);
		boolean flag = this.updateById(gameAppointmentScheduleUser);
		if (flag) {
			return R.ok(1,"删除成功");
		}
		return R.failed(6,"删除失败");
	}


	//将实际在线人数加入list
	public GameAppointmentScheduleUser  addOnlineNum(String type) {
		GameAppointmentScheduleUser gameAppointmentScheduleUser = new GameAppointmentScheduleUser();
		GameAppointmentReq gameAppointmentReq = new GameAppointmentReq();
		gameAppointmentReq.setType(Integer.valueOf(type));
		Long onlineNum = gameAppointmentMapper.selectAllTotal(gameAppointmentReq);
		gameAppointmentScheduleUser.setType(type);
		gameAppointmentScheduleUser.setIsStart(2);
		gameAppointmentScheduleUser.setConventionTypeId(1);
		gameAppointmentScheduleUser.setConventionName("实时预约");
		gameAppointmentScheduleUser.setIsDelete(1);
		gameAppointmentScheduleUser.setNumber(onlineNum);
		return gameAppointmentScheduleUser;
	}


	public static Date getMinAfter(Date date,int minuteTimes) {
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		now.add(Calendar.SECOND,minuteTimes);
		Date afterMin = now.getTime();
		return afterMin;
	}

}


