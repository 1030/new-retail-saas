package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.WanGameChannelInfoDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WanGameChannelInfoMapper extends BaseMapper<WanGameChannelInfoDO> {
}
