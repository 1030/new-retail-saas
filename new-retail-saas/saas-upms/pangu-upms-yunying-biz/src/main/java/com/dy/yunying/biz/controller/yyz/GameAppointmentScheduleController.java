package com.dy.yunying.biz.controller.yyz;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.req.yyz.GameAppointmentReq;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleReq;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleService;
import com.dy.yunying.biz.utils.ValidatorUtil;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * 游戏预约进度表
 *
 * @author leisw
 * @version 2022-05-09 17:14:29
 * table: game_appointment_schedule
 */
@Inner(value = false)
@RestController
@RequiredArgsConstructor
@RequestMapping("/gameSchedule")
@Slf4j
public class GameAppointmentScheduleController {

	private final GameAppointmentScheduleService gameAppointmentScheduleService;

	//	@SysLog("预约进度列表")
	@RequestMapping("/getPage")
	public R getPage(@RequestBody GameAppointmentScheduleReq req) {
		return this.gameAppointmentScheduleService.getPage(req);
	}

//	@SysLog("新增游戏预约进度")
	@RequestMapping("/gameAddSchedule")
	public R gameAddSchedule(@RequestBody GameAppointmentScheduleReq req) {
		if (Objects.isNull(req)) {
			return R.failed(null,  "请求参数错误");
		}
		if (StringUtils.isBlank(req.getType())){
			return R.failed("游戏预约进度人数表游戏类型不能为空");
		}
		return this.gameAppointmentScheduleService.gameAddSchedule(req);
	}

//	@SysLog("编辑游戏预约进度")
	@RequestMapping("/gameEditSchedule")
	public R gameEditSchedule(@RequestBody GameAppointmentScheduleReq req){
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if (StringUtils.isBlank(req.getId())){
			return R.failed("游戏预约进度表主键不能为空");
		}
		return this.gameAppointmentScheduleService.gameEditSchedule(req);
	}


/*	//	@SysLog("预约总数")
	@RequestMapping("/allTotal")
	public R allTotal(@RequestBody GameAppointmentScheduleReq req) {
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if(Objects.isNull(req.getType())){
			//默认预约类型：幻兽仓角预约
			req.setType(Constant.TYPE_1.toString());
		}
		return this.gameAppointmentScheduleService.allTotal(req);
	}*/

	@RequestMapping("/getLineList")
	public R getLineList(@RequestBody GameAppointmentScheduleReq req) {
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if(Objects.isNull(req.getType())){
			return R.failed(null, 2, "游戏类型不能为空");
		}
		if(Objects.isNull(req.getStartTime())){
			return R.failed(null, 2, "开始时间不能为空");
		}
		if(Objects.isNull(req.getEndTime())){
			return R.failed(null, 2, "结束时间不能为空");
		}
		return this.gameAppointmentScheduleService.getLineList(req);
	}





	/***
	 * 校验参数
	 * @param req
	 * @return
	 */
	public R checkRequest(GameAppointmentReq req) {
		if (StringUtils.isBlank(req.getMobile()) || Objects.isNull(req.getCodeType())
				|| StringUtils.isBlank(req.getCaptcha())) {
			return R.failed("请求参数不合法");
		}
		if (!ValidatorUtil.isMobile(req.getMobile())) {
			return R.failed("手机号格式不正确");
		}
		return null;
	}
}


