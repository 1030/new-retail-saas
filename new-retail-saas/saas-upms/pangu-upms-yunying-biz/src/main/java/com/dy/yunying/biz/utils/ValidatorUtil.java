package com.dy.yunying.biz.utils;

import java.util.regex.Pattern;

/**
 * @Author: kyf
 * @Date: 2020/6/15 9:55
 */
public class ValidatorUtil {

    /**
     * 正则表达式：验证用户名
     */
    public static final String REGEX_USERNAME = "^[a-zA-Z]\\w{1,17}$";

    /**
     * 正则表达式：验证密码
     */
    public static final String REGEX_PASSWORD = "^[a-zA-Z0-9]{8,20}$";

    /**
     * 正则表达式：验证手机号
     */
    public static final String REGEX_MOBILE = "^1[3456789]\\d{9}$";

    /**
     * 正则表达式：验证邮箱
     */
    public static final String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

    /**
     * 正则表达式：验证汉字
     */
    public static final String REGEX_CHINESE = "^[\u4e00-\u9fa5],{0,}$";

    /**
     * 正则表达式：验证身份证
     */
    public static final String REGEX_ID_CARD = "(^\\d{18}$)|(^\\d{15}$)";

    /**
     * 正则表达式：验证URL
     */
    public static final String REGEX_URL = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";

    /**
     * 正则表达式：验证IP地址
     */
    public static final String REGEX_IP_ADDR = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";

    /**
     * 正则表达式：验证input,只能是数字字母和英文逗号
     */
    public static final String REGEX_INPUT_NAME = "^[A-Za-z0-9\\,]+$";

    /***
     *  验证返回0-100且保留一位小数
     */
    public static final String REGEX_REABTE_NUM = "^(\\d|[1-9]\\d|100)(\\.\\d{1})?$";

    /***
     * 地址不包含https|http|ftp|rtsp|mms
     */
    public static final String REGEX_START_ADDR = "^((?!(https|http|ftp|rtsp|mms)).)*$";

    /***
     * 地址结尾以com|cn
     */
    public static final String REGEX_EDN_ADDR = "\\.(com|cn)$";

    /**
     * 验证QQ号
     */
    public static final String REGEX_QQ = "^[1-9][0-9]{4,9}$";

    /***
     * 昵称包含中英文和数字
     */
    public static final String REGEX_NICK_NAME = "^[A-Za-z0-9\u4e00-\u9fa5]+$";

    /**
     * 校验用户名
     *
     * @param username
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isUsername(String username) {
        return Pattern.matches(REGEX_USERNAME, username);
    }

    /**
     * 校验密码
     *
     * @param password
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isPassword(String password) {
        return Pattern.matches(REGEX_PASSWORD, password);
    }

    /**
     * 校验手机号
     *
     * @param mobile
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isMobile(String mobile) {
        return Pattern.matches(REGEX_MOBILE, mobile);
    }

    /**
     * 校验邮箱
     *
     * @param email
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isEmail(String email) {
        return Pattern.matches(REGEX_EMAIL, email);
    }


    /**
     * 验证QQ号码
     *
     * @param qq
     * @return true合法 false不合法
     */
    public static boolean isQQ(String qq) {
        return Pattern.matches(REGEX_QQ, qq);
    }

    /**
     * 校验汉字
     *
     * @param chinese
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isChinese(String chinese) {
        return Pattern.matches(REGEX_CHINESE, chinese);
    }

    /**
     * 校验身份证
     *
     * @param idCard
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isIDCard(String idCard) {
        return Pattern.matches(REGEX_ID_CARD, idCard);
    }

    /**
     * 校验URL
     *
     * @param url
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isUrl(String url) {
        return Pattern.matches(REGEX_URL, url);
    }

    /**
     * 校验IP地址
     *
     * @param ipAddr
     * @return
     */
    public static boolean isInputName(String ipAddr) {
        return Pattern.matches(REGEX_IP_ADDR, ipAddr);
    }

    /**
     * 校验IP地址
     *
     * @param userName
     * @return
     */
    public static boolean isMatchName(String userName) {
        return Pattern.matches(REGEX_INPUT_NAME, userName);
    }

    /**
     * 校验返点格式
     *
     * @param
     * @return
     */
    public static boolean isRebateNum(String rebate) {
        return Pattern.matches(REGEX_REABTE_NUM, rebate);
    }

    /**
     * 校验返点格式
     *
     * @param
     * @return
     */
    public static boolean isDomain(String domainAddr) {
        if (Pattern.matches(REGEX_START_ADDR, domainAddr) && Pattern.matches(REGEX_EDN_ADDR, domainAddr)) {
            return true;
        }
        return false;
    }

    /**
     * 校验昵称
     *
     * @param nickName
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isNickName(String nickName) {
        return Pattern.matches(REGEX_NICK_NAME, nickName);
    }


   /* public static void main(String[] args) {
        String username = "fdsdfsdj";
        System.out.println(ValidateUtil.isUsername(username));
        System.out.println(ValidateUtil.isChinese(username));
    }*/

}
