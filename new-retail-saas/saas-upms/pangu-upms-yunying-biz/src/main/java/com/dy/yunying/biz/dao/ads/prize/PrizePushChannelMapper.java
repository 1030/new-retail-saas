package com.dy.yunying.biz.dao.ads.prize;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.prize.PrizePushChannel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 奖励与渠道关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:48
 * table: prize_push_channel
 */
@Mapper
public interface PrizePushChannelMapper extends BaseMapper<PrizePushChannel> {
}


