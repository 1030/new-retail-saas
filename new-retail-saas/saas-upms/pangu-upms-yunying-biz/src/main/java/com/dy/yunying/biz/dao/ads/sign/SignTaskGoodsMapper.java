package com.dy.yunying.biz.dao.ads.sign;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.sign.SignTaskGoodsDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.dy.yunying.biz.dao.ads.sign..SignTaskGoods
 */
@Mapper
public interface SignTaskGoodsMapper extends BaseMapper<SignTaskGoodsDO> {

}




