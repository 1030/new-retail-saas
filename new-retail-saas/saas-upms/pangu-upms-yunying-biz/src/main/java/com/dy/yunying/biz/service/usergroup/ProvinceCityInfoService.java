

package com.dy.yunying.biz.service.usergroup;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.usergroup.ProvinceCityInfo;


public interface ProvinceCityInfoService extends IService<ProvinceCityInfo> {
}


