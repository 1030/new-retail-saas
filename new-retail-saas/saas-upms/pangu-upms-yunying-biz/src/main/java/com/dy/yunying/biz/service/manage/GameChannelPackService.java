package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.*;
import com.dy.yunying.api.req.*;
import com.dy.yunying.api.vo.ChannelManageVo;
import com.dy.yunying.api.vo.WanChannelPackPageVO;
import com.dy.yunying.api.vo.WanChannelPackVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;
import java.util.Map;


/**
 * @Author: hjl
 * @Date: 2020/7/21 13:52
 */
public interface GameChannelPackService {

	IPage<WanChannelPackPageVO> page(WanChannelPackReq req);

	void pack(GameChannelPackReq gameChannelPackReq);

	/**
	 * 按分包编码批量生成包（异步）
	 *
	 * @param gameVersion
	 * @param channelVO
	 * @param packCodeList
	 * @param req
	 * @return
	 */
	void batchPackAsync(GameVersionDO gameVersion, ChannelManageVo channelVO, List<String> packCodeList, GameChannelPackReq req);

	WanChannelPackDO updateByUK(WanChannelPackDO wanChannelPackDO);

	List<String> getChlCodeByGameId(Long gameId);

	/**
	 * 生成子游戏+主渠道 基础测试包
	 *
	 * @param req
	 */
	void packBase(GameBasePackReq req);

	void update(GameChannelPackUpdateReq req);

	WanChannelPackVO pack(Long adId, Integer userId, Integer appType);

	WanChannelPackVO getVOByPK(Long packId);

	/**
	 * 按分包编码批量生成包（同步）
	 *
	 * @param gameVersion
	 * @param channelVO
	 * @param packCodeList
	 * @param req
	 * @return
	 */
	List<WanChannelPackVO> batchPackSync(GameVersionDO gameVersion, ChannelManageVo channelVO, List<String> packCodeList, GameChannelPackReq req);

	/**
	 * 获取分包监测链接
	 *
	 * @return
	 */
	String getDetectionUrl(AdvertiserMonitorInfoDO record, WanChannelPackVO wanChannelPackVO, ChannelManageVo channelVO);
	String getDisplayTrackUrl(AdvertiserMonitorInfoDO record, WanChannelPackVO wanChannelPackVO, ChannelManageVo channelVO);

	Map<Long, WanChannelPackVO> pack(AdvertiserMonitorInfoDO adInfo, GameVersionDO gameVersion, ChannelManageVo channelVO, List<Long> adIdList, Integer userId);

	List<WanChannelPackVO> latestList(WanChannelPackReq req);

	List<WanGameChannelInfoDO> queryPkByCodeName(String codeName, String chl);

	List<Map<String, String>> queryAppChlName(List<String> appChlArr);

	List<Map<String, String>> queryAppchlList(WanAppChlReq req);

	List<Map<String, String>> queryCpsAppchlList(WanAppChlReq req);

	/**
	 * @description: 星图监测链接
	 * @author yuwenfeng
	 * @date 2021/12/14 13:46
	 */
    String getXtMonitorUrl(AdvertiserMonitorInfoDO record, WanChannelPackVO wanChannelPackVO, ChannelManageVo channelManageVo);

	/**
	 * cps分包
	 * @param gameChannelPackReq
	 */
	void cpsPack(GameChannelPackReq gameChannelPackReq);

	/**
	 * cps渠道分包列表（有权限）
	 * @param req
	 * @return
	 */
	IPage<WanChannelPackPageVO> cpsPage(WanChannelCpsPackReq req);

	/**
	 * 渠道相关信息
	 * @param req
	 * @return
	 */
	List<SubChannelInfoDO> queryChlInfoListByAuth(WanAppChlReq req);

	/**
	 * cps子渠道列表（有权限）
	 * @param req
	 * @return
	 */
	List<Map<String, String>> queryCpsSubchlList(WanAppChlReq req);
}