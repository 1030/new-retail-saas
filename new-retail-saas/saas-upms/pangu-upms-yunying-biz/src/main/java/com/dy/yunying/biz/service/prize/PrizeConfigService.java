package com.dy.yunying.biz.service.prize;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.prize.PrizeConfig;
import com.dy.yunying.api.req.prize.PrizeConfigReq;
import com.dy.yunying.api.resp.prize.PrizeConfigRes;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 奖励配置表
 *
 * @author chenxiang
 * @version 2022-04-25 16:25:44 table: prize_config
 */
public interface PrizeConfigService extends IService<PrizeConfig> {

	/**
	 * 编辑-保存奖励配置
	 * @param configReq
	 * @return
	 */
	R<PrizeConfigRes> savePrizeConfig(PrizeConfigReq configReq);

	/**
	 * 获取所有的奖励配置根据奖励推送ID
	 * @param record
	 * @return
	 */
	R<List<PrizeConfigRes>> getAllPrizeConfig(PrizeConfigReq record);

	/**
	 * ·
	 * @param id
	 * @return
	 */
	R doRemoveById(Long id);

}
