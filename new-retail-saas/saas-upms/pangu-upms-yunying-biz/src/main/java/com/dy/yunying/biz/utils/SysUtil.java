package com.dy.yunying.biz.utils;

import com.dy.yunying.api.enums.PrizePushHbTypeEnum;
import com.dy.yunying.api.req.prize.PrizeConfigReq;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * 运行系统工具类
 *
 * @Author: hjl
 * @Date: 2020/9/4 17:38
 */
@Slf4j
public class SysUtil {

    private static String OS = System.getProperty("os.name").toLowerCase();

    private static SysUtil _instance = new SysUtil();

    private SysUtil() {
    }

    /**
     * 判断当前运行系统是否是 Linux
     * @return
     */
    public static boolean isLinux() {
        return OS.indexOf("linux") >= 0;
    }

    /**
     * 判断当前运行系统是否是 Windows
     * @return
     */
    public static boolean isWindows() {
        return OS.indexOf("windows") >= 0;
    }

    public static void exec(String command) throws IOException {
        // linux 设置文件读取权限
        if (SysUtil.isLinux() && StringUtils.isNotBlank(command)) {
            Runtime.getRuntime().exec(command);
        }
    }

    public static void main(String[] args) {

    }

}
