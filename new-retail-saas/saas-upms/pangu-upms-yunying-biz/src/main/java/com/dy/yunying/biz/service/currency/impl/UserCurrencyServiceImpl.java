package com.dy.yunying.biz.service.currency.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.currency.UserCurrency;
import com.dy.yunying.biz.dao.manage.currency.UserCurrencyMapper;
import com.dy.yunying.biz.service.currency.UserCurrencyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
* @author hejiale
* @description 针对表【user_currency(账号平台币信息)】的数据库操作Service实现
* @createDate 2022-03-23 15:40:47
*/
@Service
@RequiredArgsConstructor
@Slf4j
public class UserCurrencyServiceImpl extends ServiceImpl<UserCurrencyMapper, UserCurrency>
    implements UserCurrencyService {

}




