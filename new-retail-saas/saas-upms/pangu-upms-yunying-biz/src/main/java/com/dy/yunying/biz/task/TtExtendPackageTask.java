package com.dy.yunying.biz.task;

import com.dy.yunying.biz.service.manage.TtExtendPackageService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ：lile
 * @date ：2021/8/5 20:39
 * @description：
 * @modified By：
 */
@Component
public class TtExtendPackageTask {

	@Autowired
	private TtExtendPackageService ttExtendPackageService;


	/**
	 * 查询头条应用信息
	 *
	 * @param param
	 * @return
	 */
	@XxlJob("queryExtendPackage")
	public ReturnT<String> queryExtendPackage(String param) {
		try {
			ttExtendPackageService.queryExtendPackage(param);
		} catch (Exception e) {
			e.printStackTrace();
			XxlJobLogger.log("-----------------查询头条分包明细异常---------------------");
		}
		return ReturnT.SUCCESS;
	}


}
