package com.dy.yunying.biz.controller.currency;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.dto.currency.CurrencyOrderDTO;
import com.dy.yunying.api.vo.CurrencyOrderVO;
import com.dy.yunying.biz.service.manage.WanRechargeOrderService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 余额订单
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/currencyOrder")
@Api(value = "currencyOrder", tags = "余额订单")
public class CurrencyOrderController {

	private final WanRechargeOrderService wanRechargeOrderService;

	/**
	 * 订单列表
	 *
	 * @return
	 */
	@PostMapping("/list")
	public R<Page<CurrencyOrderDTO>> list(@RequestBody CurrencyOrderVO order) {
		final Page<CurrencyOrderDTO> orderPage = wanRechargeOrderService.getCurrencyOrderPage(order, order);
		return R.ok(orderPage);
	}

	/**
	 * 汇总数据
	 *
	 * @param order
	 * @return
	 */
	@PostMapping("/total")
	public R<Map<String, Object>> total(@RequestBody CurrencyOrderVO order) {
		final Map<String, Object> orderTotal = wanRechargeOrderService.getCurrencyOrderTotal(order);
		return R.ok(orderTotal);
	}

	/**
	 * 余额订单导出数据
	 *
	 * @param order
	 * @param request
	 * @param response
	 * @return
	 */
	@GetMapping("/export")
	public void export(CurrencyOrderVO order, HttpServletRequest request, HttpServletResponse response) {
		try {
			final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			String sheetName = "游豆订单报表";
			List<CurrencyOrderDTO> list = wanRechargeOrderService.getCurrencyOrderList(order);
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(list);
			// 将金额字段四舍五入保留两位小数
			amountWithTwoDecimalPlaces(resultListMap, "rechargeAmount", "currencyAmount");
			resultListMap.forEach(e -> e.entrySet().stream().filter(en -> en.getValue() instanceof Date).forEach(en -> e.put(en.getKey(), sdf.format(en.getValue()))));
			// 导出
			ExportUtils.exportExcelData(request, response, String.format("%s-%s.xlsx", sheetName, DateUtils.getCurrentTimeNoUnderline()), sheetName, order.getTitles(), order.getColumns(), resultListMap);
		} catch (BusinessException e) {
			log.error("currencyOrder is error", e);
			throw e;
		} catch (Exception e) {
			log.error("currencyOrder is error", e);
			throw new BusinessException("导出异常");
		}
	}

	private static void amountWithTwoDecimalPlaces(List<Map<String, Object>> resultListMap, String... columns) {
		if (null == resultListMap || null == columns) {
			return;
		}
		for (Map<String, Object> map : resultListMap) {
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				final String key = entry.getKey();
				final Object value = entry.getValue();
				if (ArrayUtils.contains(columns, key) && value instanceof BigDecimal) {
					map.put(key, ((BigDecimal) value).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
				}
			}
		}
	}

}
