package com.dy.yunying.biz.controller.data;


import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.dto.AdKanbanDto;
import com.dy.yunying.api.dto.AdOverviewDto;
import com.dy.yunying.api.entity.ExcelData;
import com.dy.yunying.api.vo.AdKanbanOverviewVo;
import com.dy.yunying.api.vo.AdPlanOverviewVo;
import com.dy.yunying.biz.service.data.AdKanbanService;
import com.dy.yunying.biz.service.data.AdOverviewService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/kanban")
@Slf4j
public class AdKanbanController {

    @Autowired
    private AdKanbanService kanbanService;

    /**
     * 看板概览数据
     *
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping("/getKanbanStatistic")
    public R selectAdOverviewSource(@Valid @RequestBody AdKanbanDto req) {
			if(req.getCurrent() <= 0 ||  req.getSize() <= 0){
				return  R.failed("分页信息不允许为空");
			}
			return  kanbanService.selectKanbanStatitic(req);
    }

	/**
	 * 看板概览导出
	 * @param req
	 * @return
	 */
	@ResponseExcel(name="看板导出",sheet = "看板导出")
	@RequestMapping("/excelPlanStatistic")
    public R excelPlanStatistic(@Valid @RequestBody AdKanbanDto req, HttpServletResponse response, HttpServletRequest request) {
		String sheetName = "看板导出";
		try {
			List<AdKanbanOverviewVo> list = kanbanService.excelKanbanStatitic(req);
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(list);

			// 查询汇总数据
			req.setCycleType(4);
			req.setQueryColumn("");
			List<Map<String, Object>> collectListMap = MapUtils.objectsToMaps(kanbanService.excelKanbanStatitic(req));
			for (Map<String, Object> map : collectListMap) {
				map.put(req.getColumns().split(",")[0], "汇总");
			}
			resultListMap.addAll(collectListMap);

			// 为百分比的值拼接百分号
			final String defaultValue = "0.00%";
			ExportUtils.spliceSuffix(resultListMap, Constant.PERCENT, defaultValue, "roiratio", "sharing");
			// 导出
			ExportUtils.exportExcelData(request,response,"看板导出-" + DateUtils.getCurrentTimeNoUnderline() + ".xlsx", sheetName,req.getTitles(),req.getColumns(),resultListMap);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
