

package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.req.usergroup.UserGroupDetailReq;
import com.dy.yunying.api.vo.usergroup.UserGroupDetailVo;
import com.dy.yunying.api.vo.usergroup.UserGroupExportVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface UserGroupDetailService {


	R list(UserGroupDetailReq req);

	R count(UserGroupDetailReq req);

    List<UserGroupExportVo> exportRecord(UserGroupDetailReq req);
}


