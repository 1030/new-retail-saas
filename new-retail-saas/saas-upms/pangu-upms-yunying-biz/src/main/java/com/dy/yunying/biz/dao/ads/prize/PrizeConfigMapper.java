package com.dy.yunying.biz.dao.ads.prize;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.prize.PrizeConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 奖励配置表
 * @author  chenxiang
 * @version  2022-04-25 16:25:44
 * table: prize_config
 */
@Mapper
public interface PrizeConfigMapper extends BaseMapper<PrizeConfig> {
	List<PrizeConfig> selectPrizeConfigByRangePrizePushId(@Param("prizePushIds") List<Long> prizePushIds);

	void updateGitCodeAndGitAmount(Long id);
}


