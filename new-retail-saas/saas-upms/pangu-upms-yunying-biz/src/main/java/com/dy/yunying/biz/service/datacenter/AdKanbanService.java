package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.AdKanbanDto;
import com.dy.yunying.api.datacenter.vo.AdKanbanOverviewVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface AdKanbanService {
	R selectKanbanStatitic(AdKanbanDto req);

	List<AdKanbanOverviewVo> excelKanbanStatitic(AdKanbanDto req);

	R countDataTotal(AdKanbanDto req);
}
