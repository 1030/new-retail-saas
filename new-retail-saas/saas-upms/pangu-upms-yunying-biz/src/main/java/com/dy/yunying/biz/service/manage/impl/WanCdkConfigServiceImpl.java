package com.dy.yunying.biz.service.manage.impl;

import ch.qos.logback.core.joran.util.beans.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.WanCdkConfig;
import com.dy.yunying.api.resp.WanCdkConfigRes;
import com.dy.yunying.api.resp.WanCdkExportData;
import com.dy.yunying.biz.dao.manage.WanCdkConfigMapper;
import com.dy.yunying.biz.service.manage.WanCdkConfigService;
import com.dy.yunying.biz.utils.DateUtils;
import com.google.api.client.util.Lists;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 代金券导出数据配置表
 * @author  chenxiang
 * @version  2022-04-18 16:23:05
 * table: cdk_grant_config
 */
@Log4j2
@Service("wanCdkConfigService")
@RequiredArgsConstructor
public class WanCdkConfigServiceImpl extends ServiceImpl<WanCdkConfigMapper, WanCdkConfig> implements WanCdkConfigService {

	private final WanCdkConfigMapper wanCdkConfigMapper;

	/**
	 * 获取数据
	 * @param WanCdkConfig
	 * @return
	 */
	public List<WanCdkExportData> getData(WanCdkConfig wanCdkConfig){
		List<WanCdkExportData> list = Lists.newArrayList();
		WanCdkConfigRes cdkConfigRes = new WanCdkConfigRes();
		BeanUtils.copyProperties(wanCdkConfig,cdkConfigRes);
		// 指定天数类型：1当天，2指定天
		if (1 == cdkConfigRes.getTimeType().intValue()){
			String dayStr = DateUtils.dateToString(new Date(), DateUtils.YYYY_MM_DD);
			cdkConfigRes.setStartTimeStr(dayStr + DateUtils.START_HHMMSS);
			cdkConfigRes.setEndTimeStr(dayStr + DateUtils.END_HHMMSS);
		}else{
			if (Objects.nonNull(cdkConfigRes.getStartTime()) && Objects.nonNull(cdkConfigRes.getEndTime())) {
				cdkConfigRes.setStartTimeStr(DateUtils.dateToString(cdkConfigRes.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS));
				cdkConfigRes.setEndTimeStr(DateUtils.dateToString(cdkConfigRes.getEndTime(), DateUtils.YYYY_MM_DD_HH_MM_SS));
			}else{
				return list;
			}
		}
		return wanCdkConfigMapper.selectUserData(cdkConfigRes);
	}
}


