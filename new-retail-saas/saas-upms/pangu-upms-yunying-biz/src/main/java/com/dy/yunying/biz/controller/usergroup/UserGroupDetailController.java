package com.dy.yunying.biz.controller.usergroup;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.dy.yunying.api.enums.DimensionEnum;
import com.dy.yunying.api.req.usergroup.UserGroupDetailReq;
import com.dy.yunying.api.vo.hbdata.*;
import com.dy.yunying.api.vo.usergroup.UserGroupDetailVo;
import com.dy.yunying.api.vo.usergroup.UserGroupExportVo;
import com.dy.yunying.biz.service.datacenter.UserGroupDetailService;
import com.dy.yunying.biz.service.manage.GameService;
import com.dy.yunying.biz.service.usergroup.UserGroupDataService;
import com.dy.yunying.biz.service.usergroup.UserGroupLogService;
import com.dy.yunying.biz.service.usergroup.UserGroupService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.admin.api.feign.RemoteUserService;
import com.pig4cloud.pig.common.core.fastjson.BigDecimalValueFilter;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * 用户群组详情
 * @author zhuxm
 * @date 2022-04-29 15:40:05
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/userGroupDetail")
@Api(value = "userGroupDetail", tags = "用户管理-用户群组-详情")
@Slf4j
public class UserGroupDetailController {

	private final UserGroupService userGroupService;
	private final RemoteUserService remoteUserService;
	private final GameService gameService;


	private final UserGroupLogService userGroupLogService;

	private final UserGroupDataService userGroupDataService;


	private final UserGroupDetailService userGroupDetailService;


	@ApiOperation(value = "用户群组详情列表", notes = "用户群组详情列表")
	@RequestMapping("/list")
	public R list(@Valid @RequestBody UserGroupDetailReq req) {
		try {
			//筛选维度：1账号，2角色
			Integer dimension = req.getDimension();
			switch (DimensionEnum.getOneByType(dimension)){
				case ACCOUNT:
				case ROLEID:
					return userGroupDetailService.list(req);
				default:
					return R.failed("接口异常，请联系管理员");
			}
		} catch (Exception e) {
			log.error("用户管理-用户群组列表-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}


	@RequestMapping(value = "/count")
	public R countDataTotal(@Valid @RequestBody UserGroupDetailReq req) {
		return userGroupDetailService.count(req);
	}

	/**
	 * 用户群组详情列表明细导出
	 * @param req
	 * @return
	 */
	@SysLog("用户群组详情列表导出")
	@ResponseExcel(name="用户群组详情列表导出",sheet = "用户群组详情列表导出")
	@RequestMapping("/export")
	@PreAuthorize("@pms.hasPermission('usergroup_detail_export')")
	public void export(@Valid @RequestBody UserGroupDetailReq req, HttpServletResponse response, HttpServletRequest request) {
		String sheetName = "用户群组明细";
		try {
			List<UserGroupExportVo> list = userGroupDetailService.exportRecord(req);

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setCharacterEncoding("utf-8");
			String fileName = URLEncoder.encode("用户群组详情列表-"+ DateUtils.getCurrentTimeNoUnderline(), "UTF-8").replaceAll("\\+", "%20");
			response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
			response.setHeader("file-name", fileName);
			ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
			WriteSheet writeSheetAccount = EasyExcel.writerSheet(sheetName).excludeColumnFiledNames(Lists.newArrayList("roleId","roleName","areaid","roleRegTime")).head(UserGroupExportVo.class).build();
			WriteSheet writeSheetRoleId = EasyExcel.writerSheet(sheetName).head(UserGroupExportVo.class).build();

			//筛选维度：1账号，2角色
			Integer dimension = req.getDimension();
			switch (DimensionEnum.getOneByType(dimension)){
				case ACCOUNT:
					// 导出
					excelWriter.write(list,writeSheetAccount);
					break;
				case ROLEID:
					// 导出
					excelWriter.write(list,writeSheetRoleId);
					break;
				default:
					log.error("接口异常，请联系管理员");
					break;
			}
			excelWriter.finish();
		} catch (Exception e) {
			log.error("用户群组详情列表导出失败,", e);
		}
	}

}
