package com.dy.yunying.biz.service.yyz;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.GameAppointmentSchedule;
import com.dy.yunying.api.entity.GameAppointmentScheduleUser;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleReq;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleUserReq;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;


/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment_schedule
 */
public interface GameAppointmentScheduleUserService extends IService<GameAppointmentScheduleUser> {

	/**
	 * 预约列表 - 分页
	 *
	 * @param req
	 * @return
	 */
	R getPage(GameAppointmentScheduleUserReq req);


	/**
	 * 获得所有的预约规则和人数
	 *
	 * @param req
	 * @return
	 */
	List<GameAppointmentScheduleUser> getOnlineList(GameAppointmentSchedule req);

	/**
	 * 新增
	 *
	 * @param req
	 * @return
	 */
	R gameAddSchedule(GameAppointmentScheduleUserReq req);

	/**
	 * 修改
	 *
	 * @param req
	 * @return
	 */
	R gameEditSchedule(GameAppointmentScheduleUserReq req);


	R isEnable(GameAppointmentScheduleUserReq req);
	/**
	 * 删除
	 *
	 * @param req
	 * @return
	 */
	R gameDelSchedule(GameAppointmentScheduleUserReq req);

}


