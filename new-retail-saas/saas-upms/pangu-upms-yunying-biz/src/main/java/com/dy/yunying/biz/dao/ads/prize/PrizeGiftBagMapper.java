package com.dy.yunying.biz.dao.ads.prize;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.prize.PrizeGiftBag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 奖品礼包码
 * @author  chenxiang
 * @version  2022-04-25 16:25:54
 * table: prize_gift_bag
 */
@Mapper
public interface PrizeGiftBagMapper extends BaseMapper<PrizeGiftBag> {

	/**
	 * 将删除状态重置为启用
	 * @param sourceId
	 * @param usable
	 */
	void updateDeleted(@Param("sourceId") Long sourceId, @Param("usable") Integer usable);
}


