package com.dy.yunying.biz.service.gametask;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.gametask.GameTaskConfig;
import com.dy.yunying.api.req.gametask.GameTaskConfigReq;
import com.dy.yunying.api.req.gametask.GameTaskSelectReq;
import com.dy.yunying.api.req.gametask.ImportGameTaskReq;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
*
*/
public interface GameTaskConfigService extends IService<GameTaskConfig> {

    R importGameTaskConfig(ImportGameTaskReq importGameTaskReq) throws IOException;

    R getGameTaskListByParentGameIds(List<Long> parentGameIds);

	R getPage(GameTaskSelectReq gameTaskSelectReq);

	/**
	 * 创建编辑接口
	 * @param gameTaskConfigReq
	 * @return
	 */
	R createEdit(GameTaskConfigReq gameTaskConfigReq);

	R getAppIdByParentGameId(Long parentGameId);
}
