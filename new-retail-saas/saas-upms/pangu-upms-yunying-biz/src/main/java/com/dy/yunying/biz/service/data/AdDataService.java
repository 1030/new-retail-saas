package com.dy.yunying.biz.service.data;

import com.dy.yunying.api.dto.AdDataDto;
import com.dy.yunying.api.vo.AdDataVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

public interface AdDataService {

	R selectAdDataSource(AdDataDto req);

	List<AdDataVo> excelAdDataSource(AdDataDto req);
}
