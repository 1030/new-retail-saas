package com.dy.yunying.biz.service.manage;

import com.dy.yunying.api.entity.WanGameImgsDO;
import com.dy.yunying.api.req.WanGameImgsReq;

import java.util.List;

public interface WanGameImgsService {

	int deleteByPrimaryKey(Long imgid);

	int insert(WanGameImgsDO record);

	int insertSelective(WanGameImgsDO record);

	WanGameImgsDO selectByPrimaryKey(Long imgid);

	int updateByPrimaryKeySelective(WanGameImgsDO record);

	int updateByPrimaryKey(WanGameImgsDO record);

	/**
	 * 批量新增图片
	 *
	 * @author zhangtao
	 * @date Nov 19, 2016 10:59:48 AM
	 */
	boolean insertWanGameImgsList(List<WanGameImgsReq> list);

	/**
	 * 按游戏id删除游戏图片
	 *
	 * @author zhangtao
	 * @date Nov 19, 2016 11:01:07 AM
	 */
	public boolean deleteWanGameImgs(WanGameImgsReq entity);

}
