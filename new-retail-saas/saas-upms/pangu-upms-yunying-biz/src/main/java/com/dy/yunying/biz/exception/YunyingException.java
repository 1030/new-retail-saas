package com.dy.yunying.biz.exception;

import lombok.Data;

/**
 * @Author yangyh
 */
@Data
public class YunyingException extends RuntimeException {
	private Integer code;

	public YunyingException(String message) {
		super(message);
	}

	public YunyingException(Integer code, String message) {
		super(message);
		this.code = code;
	}
}
