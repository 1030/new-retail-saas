package com.dy.yunying.biz.dao.ads.prize;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.prize.PrizePushArea;
import org.apache.ibatis.annotations.Mapper;

/**
 * 奖励与区服关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:36
 * table: prize_push_area
 */
@Mapper
public interface PrizePushAreaMapper extends BaseMapper<PrizePushArea> {
}


