package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.dto.AccountManageDTO;
import com.dy.yunying.api.dto.AccountProhibitDTO;
import com.dy.yunying.api.vo.AccountManageVO;
import com.pig4cloud.pig.common.core.mybatis.Page;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author sunyq
 * @date 2022/8/19 15:10
 */
@Mapper
public interface AccountManageMapper {
	/**
	 * 根据条件查询
	 * @param accountManageDTO
	 * @return
	 */
	Page<AccountManageVO> selectWanUserPageByCondition(AccountManageDTO accountManageDTO);

	/**
	 * 查询总数
	 * @param accountManageDTO
	 * @return
	 */
	long selectTotalByCondition(AccountManageDTO accountManageDTO);
}
