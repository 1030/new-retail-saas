package com.dy.yunying.biz.controller.doris;

import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.datacenter.dto.AdDataDto;
import com.dy.yunying.api.datacenter.vo.AdDataVo;
import com.dy.yunying.biz.service.doris.AdDataDorisService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 广告数据报表相关接口
 *
 * @author ：lile
 * @date ：2021/6/17 13:58
 * @description：
 * @modified By：
 */
@Slf4j
@RestController("adData2")
@RequestMapping("/dataCenter2/adData")
@RequiredArgsConstructor
public class AdDataController2 {

	private final AdDataDorisService adDataService;

	/**
	 * 广告数据报表总数
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping(value = "/count")
	public R countDataTotal(@Valid AdDataDto req) {
		return adDataService.countDataTotal(req);
	}

	/**
	 * 广告数据报表分页数据
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping("/list")
	public R selectAdDataSource(@Valid AdDataDto req) {
		return adDataService.selectAdDataSource(req);
	}

	/**
	 * 广告数据报表导出
	 *
	 * @param req
	 * @return
	 */
	@ResponseExcel(name = "广告数据报表导出", sheet = "广告数据报表导出")
	@RequestMapping("/excel")
	public R excelAdDataSource(@Valid AdDataDto req, HttpServletResponse response, HttpServletRequest request) {
		String sheetName = "广告数据报表导出";
		try {
			List<AdDataVo> list = adDataService.excelAdDataSource(req);
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps(list);
			// 为百分比的值拼接百分号
			final String defaultValue = "0.00%";
			ExportUtils.spliceSuffix(resultListMap, Constant.PERCENT, defaultValue, "regRatio", "roi1", "weekRoi", "weekRoi", "monthRoi", "allRoi", "regPayRatio", "retention2Ratio", "createRoleRate", "certifiedRate", "activePayRate", "periodPayRate");
			// 导出
			ExportUtils.exportExcelData(request, response, "广告数据报表-" + DateUtils.getCurrentTimeNoUnderline() + ".xlsx", sheetName, req.getTitles(), req.getColumns(), resultListMap);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
