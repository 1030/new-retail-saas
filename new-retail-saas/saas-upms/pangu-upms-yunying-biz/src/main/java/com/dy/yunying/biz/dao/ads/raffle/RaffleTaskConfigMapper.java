package com.dy.yunying.biz.dao.ads.raffle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.raffle.RaffleTaskConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * 抽奖任务配置表
 * @author  chenxiang
 * @version  2022-11-08 15:59:48
 * table: raffle_task_config
 */
@Mapper
public interface RaffleTaskConfigMapper extends BaseMapper<RaffleTaskConfig> {
}


