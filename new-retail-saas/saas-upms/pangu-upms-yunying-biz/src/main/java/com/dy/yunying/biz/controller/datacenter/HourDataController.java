package com.dy.yunying.biz.controller.datacenter;

import com.dy.yunying.api.datacenter.dto.HourDataDto;
import com.dy.yunying.api.enums.HourDataKpiEnum;
import com.dy.yunying.biz.service.datacenter.HourDataService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Objects;

/**
 * 分时数据报表相关接口
 *
 * @ClassName HourDataController
 * @Description done
 * @Author nieml
 * @Time 2021/6/24 14:48
 * @Version 1.0
 **/

@RestController("hourData")
@RequestMapping("/dataCenter/hourData")
@Slf4j
public class HourDataController {

	@Autowired
	private HourDataService hourDataService;

	/**
	 * 分时报表数据
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping(value = "/list")
	public R selectHourData(@Valid HourDataDto req) {
		try {
			//校验参数
			Long startTime = req.getRsTime();
			Long endTime = req.getReTime();
			if (Objects.isNull(startTime) || startTime <= 0) {
				throw new Exception("查询开始日期不允许为空");
			}
			if (Objects.isNull(endTime) || endTime <= 0) {
				throw new Exception("查询结束日期不允许为空");
			}
			String hourDataKpi = req.getHourDataKpi();
			if (StringUtils.isBlank(hourDataKpi) ||
//					(!HourDataKpiEnum.REG.getType().equals(hourDataKpi) && !HourDataKpiEnum.PAY.getType().equals(hourDataKpi) && !HourDataKpiEnum.LTV.getType().equals(hourDataKpi))) {
					(!HourDataKpiEnum.REG.getType().equals(hourDataKpi)
							&& !HourDataKpiEnum.PAY.getType().equals(hourDataKpi)
							&& !HourDataKpiEnum.LTV.getType().equals(hourDataKpi)
							&& !HourDataKpiEnum.PAYDEVICES.getType().equals(hourDataKpi)
					)
			)
			{
				throw new Exception("请选择指标");
			}
			return hourDataService.selectHourData(req);
		} catch (Exception e) {
			log.error("selectHourData:{}", e);
			return R.failed(e.getMessage());
		}
	}

}
