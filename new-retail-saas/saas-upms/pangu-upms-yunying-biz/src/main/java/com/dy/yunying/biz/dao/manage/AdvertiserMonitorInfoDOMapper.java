package com.dy.yunying.biz.dao.manage;


import com.dy.yunying.api.entity.AdvertiserMonitorInfoDO;

public interface AdvertiserMonitorInfoDOMapper {
    int deleteByPrimaryKey(Long adid);

    int insert(AdvertiserMonitorInfoDO record);

    int insertSelective(AdvertiserMonitorInfoDO record);

    AdvertiserMonitorInfoDO selectByPrimaryKey(Long adid);

    int updateByPrimaryKeySelective(AdvertiserMonitorInfoDO record);

    int updateByPrimaryKey(AdvertiserMonitorInfoDO record);

}