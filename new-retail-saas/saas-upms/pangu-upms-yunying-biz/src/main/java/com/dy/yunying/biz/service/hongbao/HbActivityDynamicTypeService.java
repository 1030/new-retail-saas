package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbActivityDynamicType;

/**
 * 活动动态类型
 */
public interface HbActivityDynamicTypeService extends IService<HbActivityDynamicType> {
}
