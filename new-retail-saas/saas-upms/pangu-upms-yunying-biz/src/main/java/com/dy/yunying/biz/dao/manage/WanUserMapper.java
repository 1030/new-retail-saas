package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.WanUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * WanUser
 * @author  zhuxm
 * @version  2022-01-13 16:01:46
 * table: wan_user
 */
@Mapper
public interface WanUserMapper extends BaseMapper<WanUser> {
}


