package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.dto.AccountProhibitDTO;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author sunyq
 * @date 2022/8/19 15:10
 */
@Mapper
public interface AccountProhibitMapper {

	AccountProhibitDTO selectByUK(AccountProhibitDTO dto);

	AccountProhibitDTO selectByPrimaryKey(Long id);

	void insertSelective(AccountProhibitDTO dto);

	int updateByPrimaryKeySelective(AccountProhibitDTO dto);
}
