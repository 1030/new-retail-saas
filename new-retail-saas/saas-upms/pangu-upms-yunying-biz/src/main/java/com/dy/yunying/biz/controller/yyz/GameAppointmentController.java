package com.dy.yunying.biz.controller.yyz;

import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.req.yyz.GameAppointmentImportSendReq;
import com.dy.yunying.api.req.yyz.GameAppointmentQueryReq;
import com.dy.yunying.api.req.yyz.GameAppointmentQueryReqList;
import com.dy.yunying.api.req.yyz.GameAppointmentReq;
import com.dy.yunying.biz.service.yyz.GameAppointmentService;
import com.dy.yunying.biz.utils.ValidatorUtil;
import com.pig4cloud.pig.api.util.NoRepeatSubmit;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment
 */
@Inner(value = false)
@RestController
@RequiredArgsConstructor
@RequestMapping("/gameAppointment")
@Slf4j
public class GameAppointmentController {

	private final GameAppointmentService gameAppointmentService;

//	@SysLog("预约列表")
	@RequestMapping("/getPage")
	public R page(@RequestBody GameAppointmentQueryReq req) {
		if (Objects.isNull(req)) {
			return R.failed("请求参数不合法");
		}
		return this.gameAppointmentService.getPage(req);
	}

	//	@SysLog("预约列表")
	@RequestMapping("/getSendSmsPage")
	public R sendSmsPage(@RequestBody GameAppointmentQueryReq req) {
		if (Objects.isNull(req)) {
			return R.failed("请求参数不合法");
		}
		return this.gameAppointmentService.getPage(req);
	}

//	@SysLog("手机号预约")
	@RequestMapping("/add")
	public R add(@RequestBody GameAppointmentReq req) {
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if(Objects.isNull(req.getType())){
			//默认预约类型：幻兽仓角预约
			req.setType(Constant.TYPE_1);
		}
		return this.gameAppointmentService.addAppointment(req);
	}

//	@SysLog("预约总数(幻兽苍角)")
	@RequestMapping("/total")
	@NoRepeatSubmit(lockTime = 50)
	public R total() {
		return this.gameAppointmentService.total();
	}


	//	@SysLog("预约总数")
	@RequestMapping("/allTotal")
	public R allTotal(@RequestBody GameAppointmentReq req) {
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if(Objects.isNull(req.getType())){
			//默认预约类型：幻兽仓角预约
			req.setType(Constant.TYPE_1);
		}
		return this.gameAppointmentService.allTotal(req);
	}

//	@SysLog("批量短信")
	@RequestMapping("/batchMsg")
	public R batchMsg(@RequestBody GameAppointmentReq req) {
		if (Objects.isNull(req)) {
			return R.failed("请求参数错误");
		}
		if (Objects.isNull(req.getSendType())) {
			return R.failed("请求参数不合法");
		}
		return this.gameAppointmentService.batchMsg(req);
	}

//	@SysLog("短信code下拉框")
	@RequestMapping("/sendCodeList")
	public R sendCodeList() {
		return this.gameAppointmentService.sendCodeList();
	}

	//	@SysLog("短信code下拉框")
	@RequestMapping("/getGameList")
	public R getGameList() {
		return this.gameAppointmentService.getGameList();
	}


	/**
	 * 用户信息导出
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping("/export")
	public void export(HttpServletResponse response, @RequestBody GameAppointmentQueryReqList req) {

		gameAppointmentService.export(response,req);
	}


	/***
	 * 校验参数
	 * @param req
	 * @return
	 */
	public R checkRequest(GameAppointmentReq req) {
		if (StringUtils.isBlank(req.getMobile()) || Objects.isNull(req.getCodeType())
				|| StringUtils.isBlank(req.getCaptcha())) {
			return R.failed("请求参数不合法");
		}
		if (!ValidatorUtil.isMobile(req.getMobile())) {
			return R.failed("手机号格式不正确");
		}
		return null;
	}
}


