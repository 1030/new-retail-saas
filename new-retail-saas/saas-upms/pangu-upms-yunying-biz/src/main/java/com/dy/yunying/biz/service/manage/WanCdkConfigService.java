package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.WanCdkConfig;
import com.dy.yunying.api.resp.WanCdkExportData;

import java.util.List;

/**
 * 代金券导出数据配置表
 * @author  chenxiang
 * @version  2022-04-18 16:23:05
 * table: cdk_grant_config
 */
public interface WanCdkConfigService extends IService<WanCdkConfig> {
	/**
	 * 获取数据
	 * @param wanCdkConfig
	 * @return
	 */
	List<WanCdkExportData> getData(WanCdkConfig wanCdkConfig);
}


