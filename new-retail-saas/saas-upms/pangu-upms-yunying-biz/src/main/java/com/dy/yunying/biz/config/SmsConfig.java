package com.dy.yunying.biz.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author chengang
 * @Date 2022/1/18
 */
@Configuration
@ComponentScan(basePackages = {"com.sjda.framework.sms"})
public class SmsConfig {

}
