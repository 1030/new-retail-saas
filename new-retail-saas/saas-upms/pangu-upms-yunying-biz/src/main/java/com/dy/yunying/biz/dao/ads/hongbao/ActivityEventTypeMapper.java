package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.ActivityEventType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ActivityEventTypeMapper extends BaseMapper<ActivityEventType> {

}
