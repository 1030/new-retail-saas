package com.dy.yunying.biz.datasource;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

//数据源1
@Configuration
//配置mybatis的接口类放的地方
@MapperScan(basePackages = "com.dy.yunying.biz.dao.manage", sqlSessionFactoryRef = "main3399SqlSessionFactory")
public class Main3399DataSourceConfig {

	@Autowired
	private MybatisPlusInterceptor mybatisPlusInterceptor;

	@Primary
	// 将这个对象放入Spring容器中
	@Bean(name = "main3399DataSource")
	// 表示这个数据源是默认数据源
	// 读取application.properties中的配置参数映射成为一个对象
	// prefix表示参数的前缀
	@ConfigurationProperties(prefix = "spring.datasource.main3399")
	public DataSource getDateSource1() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "main3399SqlSessionFactory")
	// 表示这个数据源是默认数据源
	// @Qualifier表示查找Spring容器中名字为main3399DataSource的对象
	public MybatisSqlSessionFactoryBean iProcessSqlSessionFactory(
			@Qualifier("main3399DataSource") DataSource datasource) throws Exception {
		MybatisSqlSessionFactoryBean sqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(getDateSource1());
		MybatisConfiguration configuration = new MybatisConfiguration();
		//configuration.setMapUnderscoreToCamelCase(false);
		sqlSessionFactoryBean.setConfiguration(configuration);
		sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/mapper/manage/**/*.xml"));

		//设置 MyBatis-Plus 分页插件
		Interceptor[] plugins = {mybatisPlusInterceptor};
		sqlSessionFactoryBean.setPlugins(plugins);

		return sqlSessionFactoryBean;

	}

	@Bean(name = "main3399TransactionManager")
	public DataSourceTransactionManager getTransactionManager(@Qualifier("main3399DataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean("main3399SqlSessionTemplate")
	// 表示这个数据源是默认数据源
	public SqlSessionTemplate test1sqlsessiontemplate(
			@Qualifier("main3399SqlSessionFactory") SqlSessionFactory sessionfactory) {
		return new SqlSessionTemplate(sessionfactory);
	}
}