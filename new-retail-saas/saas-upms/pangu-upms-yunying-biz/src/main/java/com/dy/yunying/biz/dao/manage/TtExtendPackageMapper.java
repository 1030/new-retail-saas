package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.TtExtendPackageDO;

public interface TtExtendPackageMapper extends BaseMapper<TtExtendPackageDO> {
}
