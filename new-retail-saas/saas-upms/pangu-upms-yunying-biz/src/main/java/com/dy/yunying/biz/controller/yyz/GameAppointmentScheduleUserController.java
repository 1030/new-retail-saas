package com.dy.yunying.biz.controller.yyz;

import com.dy.yunying.api.req.yyz.GameAppointmentReq;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReq;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleUserReq;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleGearsService;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleUserService;
import com.dy.yunying.biz.utils.ValidatorUtil;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * 游戏预约进度档位表
 *
 * @author leisw
 * @version 2022-05-09 17:14:29
 * table: game_appointment_schedule_gears
 */
@Inner(value = false)
@RestController
@RequiredArgsConstructor
@RequestMapping("/gameScheduleUser")
@Slf4j
public class GameAppointmentScheduleUserController {

	private final GameAppointmentScheduleUserService gameAppointmentScheduleUserService;

	//	@SysLog("预约进度人数列表")
	@RequestMapping("/getPage")
	public R page(@RequestBody GameAppointmentScheduleUserReq req) {
		if (StringUtils.isBlank(req.getType())){
			return R.failed("游戏预约进度人数表游戏类型不能为空");
		}
		return this.gameAppointmentScheduleUserService.getPage(req);
	}

//	@SysLog("新增游戏人数进度")
	@RequestMapping("/gameAddSchedule")
	public R gameAddSchedule(@RequestBody GameAppointmentScheduleUserReq req) {
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if (StringUtils.isBlank(req.getType())){
			return R.failed("游戏预约进度人数表游戏类型不能为空");
		}
		if (StringUtils.isBlank(req.getConventionTypeId())){
			return R.failed("游戏预约进度人数表类型不能为空");
		}
		if (StringUtils.isBlank(req.getConventionName())){
			return R.failed("游戏预约进度人数表来源名称不能为空");
		}
		if (StringUtils.isBlank(req.getIsStart())){
			return R.failed("游戏预约进度人数表启动开关不能为空");
		}
		return this.gameAppointmentScheduleUserService.gameAddSchedule(req);
	}

//	@SysLog("编辑游戏人数进度")
	@RequestMapping("/gameEditSchedule")
	public R gameEditSchedule(@RequestBody GameAppointmentScheduleUserReq req){
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if (StringUtils.isBlank(req.getId())){
			return R.failed("游戏预约进度人数表主键不能为空");
		}
		if (StringUtils.isBlank(req.getType())){
			return R.failed("游戏预约进度人数表游戏类型不能为空");
		}
		if (StringUtils.isBlank(req.getConventionTypeId())){
			return R.failed("游戏预约进度人数表类型不能为空");
		}
		if (StringUtils.isBlank(req.getConventionName())){
			return R.failed("游戏预约进度人数表来源名称不能为空");
		}
		if (StringUtils.isBlank(req.getIsStart())){
			return R.failed("游戏预约进度人数表启动开关不能为空");
		}
		return this.gameAppointmentScheduleUserService.gameEditSchedule(req);
	}


	//	@SysLog("删除游戏人数进度")
	@RequestMapping("/isEnable")
	public R isEnable(@RequestBody GameAppointmentScheduleUserReq req){
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if (StringUtils.isBlank(req.getId())){
			return R.failed("游戏预约进度表主键不能为空");
		}
		if (StringUtils.isBlank(req.getType())){
			return R.failed("游戏预约进度人数表游戏类型不能为空");
		}
		if (StringUtils.isBlank(req.getIsStart())){
			return R.failed("游戏预约进度人数表启动开关不能为空");
		}
		return this.gameAppointmentScheduleUserService.isEnable(req);
	}

	//	@SysLog("删除游戏人数进度")
	@RequestMapping("/gameDelSchedule")
	public R gameDelSchedule(@RequestBody GameAppointmentScheduleUserReq req){
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if (StringUtils.isBlank(req.getId())){
			return R.failed("游戏预约进度表主键不能为空");
		}
		if (StringUtils.isBlank(req.getType())){
			return R.failed("游戏预约进度人数表游戏类型不能为空");
		}
		return this.gameAppointmentScheduleUserService.gameDelSchedule(req);
	}

}


