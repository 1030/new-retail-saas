package com.dy.yunying.biz.service.currency;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.currency.UserCurrencyTmp;

/**
* @author hejiale
* @description 针对表【user_currency_tmp(账号临时平台币信息)】的数据库操作Service
* @createDate 2022-03-25 09:56:35
*/
public interface UserCurrencyTmpService extends IService<UserCurrencyTmp> {

}
