package com.dy.yunying.biz.controller.manage;

import com.dy.yunying.api.entity.WanGameChannelPlatformDO;
import com.dy.yunying.api.req.GamePlatformReq;
import com.dy.yunying.biz.service.manage.GameChannelPlatformService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 获取子游戏
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/gamePlatform")
@Api(value = "gamePlatform", tags = "获取子游戏")
public class GameChannelPlatformController {

    @Autowired
    private GameChannelPlatformService gameChannelPlatformService;

    /**
     * 获取子游戏-广告平台 上报信息
     * @return
     */
    @RequestMapping("/getByPlatformAndGameid")
    public R getByPlatformAndGameid(@RequestBody GamePlatformReq req){
        try {
            Integer gameId = req.getGameId();
            Integer platformid = req.getPlatformid();
			Integer appid = req.getAppId();
            // 参数校验
            if (platformid == null) {
                return R.failed("未获取到平台ID");
            }
            if (null == gameId && null == appid) {
                return R.failed("未获取到子游戏ID或应用ID");
            }
            WanGameChannelPlatformDO data = gameChannelPlatformService.getByPlatformAndGameid(platformid, gameId, appid);
            return R.ok(data);
        } catch (Exception e) {
            log.error("获取上报信息异常：", e);
			return R.failed(e.getMessage());
        }
    }

    /**
     * 获取子游戏-广告平台 上报信息
     * @return
     */
    @RequestMapping("/getByGameidAndParentChlId")
    public R getByGameidAndParentChlId(@RequestBody GamePlatformReq req){
        try {
            Integer gameId = req.getGameId();
            Integer parentChlId = req.getParentChlId();
            // 参数校验
            if (gameId == null || parentChlId == null) {
				return R.failed("参数不合法");
            }
            WanGameChannelPlatformDO data = gameChannelPlatformService.getByGameidAndParentChlId(gameId, parentChlId);
            return R.ok(data);
        } catch (Exception e) {
            log.error("获取上报信息异常：", e);
        }
        return R.failed("查询异常");
    }
}
