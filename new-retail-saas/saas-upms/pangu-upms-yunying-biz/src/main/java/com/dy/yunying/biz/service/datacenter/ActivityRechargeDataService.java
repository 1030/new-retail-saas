package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.ActivityRechargeDataDto;
import com.dy.yunying.api.datacenter.vo.ActivityRechargeDataVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 活跃付费明细相关方法
 */
public interface ActivityRechargeDataService {

	/**
	 * 活跃付费明细分页查询数据
	 *
	 * @param req
	 * @return
	 */
	 R<List<ActivityRechargeDataVO>> page(ActivityRechargeDataDto req) ;

	/**
	 * 活跃付费明细汇总查询
	 *
	 * @param req
	 * @return
	 */
	 R<List<ActivityRechargeDataVO>> collect(ActivityRechargeDataDto req);
	/**
	 * 活跃付费明细总条数查询
	 *
	 * @param req
	 * @return
	 */
	 R count(ActivityRechargeDataDto req);

}
