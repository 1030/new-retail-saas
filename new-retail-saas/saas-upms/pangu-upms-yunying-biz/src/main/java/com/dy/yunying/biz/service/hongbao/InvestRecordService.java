package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbCashConfig;
import com.dy.yunying.api.entity.hongbao.HbInvestRecord;
import com.dy.yunying.api.req.hongbao.ActivityIdReq;
import com.dy.yunying.api.req.hongbao.ApplyInvestReq;
import com.dy.yunying.api.req.hongbao.InvertMoneyReq;
import com.dy.yunying.api.resp.hongbao.AssetsData;
import com.dy.yunying.api.resp.hongbao.InvestRecordVo;
import com.pig4cloud.pig.common.core.mybatis.Page;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @author yuwenfeng
 * @description: 注资记录服务
 * @date 2021/10/23 10:47
 */
public interface InvestRecordService extends IService<HbInvestRecord> {

	Page<InvestRecordVo> selectInvestRecordPage(Page<HbInvestRecord> page, QueryWrapper<HbInvestRecord> query);

	R addInvestRecord(InvertMoneyReq invertMoneyReq);

	R applyInvestRecord(ApplyInvestReq applyInvestReq);

	AssetsData selectAssetsCount(ActivityIdReq activityReq);
}
