package com.dy.yunying.biz.service.manage;

import com.dy.yunying.api.entity.AdvertiserMonitorInfoDO;
import com.dy.yunying.api.req.AdMonitorReq;
import com.dy.yunying.api.vo.AdvertiserMonitorVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.Map;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdConvertService.java
 * @createTime 2021年06月02日 13:59:00
 */
public interface AdvertiseMonitorService {

	R page(AdMonitorReq req);

	AdvertiserMonitorVO selectByPrimaryKey(Long adid);

	R insertAndPack(AdvertiserMonitorInfoDO record);

	boolean isAdNameExist(AdvertiserMonitorInfoDO record);

	boolean batchCopy(Long adid, int batchSize);

	boolean batchRepack(String adids);

	boolean update(AdvertiserMonitorInfoDO record);

	AdvertiserMonitorInfoDO selectAdMonitor(Map<String,Object> param);

    R getCallBackUrlByConvertId(String adPlatformId);

    R clickTouTiaoUrl(String touTiaoUrl);
}
