package com.dy.yunying.biz.service.manage.impl;

import com.dy.yunying.api.entity.ParentGameDO;
import com.dy.yunying.api.entity.WanGameSupplierDO;
import com.dy.yunying.api.req.GameSupplierReq;
import com.dy.yunying.api.req.WanGameReq;
import com.dy.yunying.api.vo.WanGameReqVo;
import com.dy.yunying.biz.dao.manage.ext.WanGameSupplierDOMapperExt;
import com.dy.yunying.biz.service.manage.WanGameSupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author ：lile
 * @date ：2021/5/29 18:00
 * @description：
 * @modified By：
 */
@Service
public class WanGameSupplierServiceImpl implements WanGameSupplierService {
	@Autowired
	private WanGameSupplierDOMapperExt mapper;

	@Override
	public int deleteByPrimaryKey(Integer gid) {
		return mapper.deleteByPrimaryKey(gid);
	}

	@Override
	public int insert(WanGameSupplierDO record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(WanGameSupplierDO record) {
		return mapper.insertSelective(record);
	}

	@Override
	public WanGameSupplierDO selectByPrimaryKey(Integer gid) {
		return mapper.selectByPrimaryKey(gid);
	}

	@Override
	public int updateByPrimaryKeySelective(WanGameSupplierDO record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKeyWithBLOBs(WanGameSupplierDO record) {
		return mapper.updateByPrimaryKeyWithBLOBs(record);
	}

	@Override
	public int updateByPrimaryKey(WanGameSupplierDO record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public int updateExchangeUrl(GameSupplierReq req) {
		return mapper.updateExchangeUrl(req);
	}

	@Override
	public int addGameSupplier(WanGameReqVo entity, ParentGameDO parentGame) {
		//添加wan_game_supplier配置
		WanGameSupplierDO record = new WanGameSupplierDO();
		record.setGid(Integer.valueOf(entity.getId().toString()));
		record.setTerminalType(Short.parseShort(String.valueOf(entity.getTerminaltype())));
		record.setName(entity.getGname());
		record.setQueryKey(parentGame.getQueryKey());
		record.setExchangeKey(parentGame.getExchangeKey());
		record.setExchangeUrl(parentGame.getExchangeUrl());
		record.setExchangeClass("com.sjda.task.gameexchange.instance.BaseRechargeApi"); // TODO 暂时写死 20200905
		record.setStatus((short) 0);
		record.setCreatetime(new Date());
		return mapper.insertSelective(record);
	}


}
