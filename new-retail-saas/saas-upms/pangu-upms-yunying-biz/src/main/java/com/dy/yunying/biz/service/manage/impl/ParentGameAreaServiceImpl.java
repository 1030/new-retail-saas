package com.dy.yunying.biz.service.manage.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.ParentGameArea;
import com.dy.yunying.biz.dao.manage.ParentGameAreaMapper;
import com.dy.yunying.biz.service.manage.ParentGameAreaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 主游戏区服关联表
 * @author  chengang
 * @version  2021-10-27 10:42:03
 * table: parent_game_area
 */
@Log4j2
@Service("parentGameAreaService")
@RequiredArgsConstructor
public class ParentGameAreaServiceImpl extends ServiceImpl<ParentGameAreaMapper, ParentGameArea> implements ParentGameAreaService {

	
	
}


