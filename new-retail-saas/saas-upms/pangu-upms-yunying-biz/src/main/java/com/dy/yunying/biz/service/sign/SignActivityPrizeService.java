

package com.dy.yunying.biz.service.sign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.dto.sign.SignActivityPrizeDto;
import com.dy.yunying.api.entity.sign.SignActivityPrize;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 签到活动奖品表
 *
 * @author chengang
 * @version 2021-12-01 10:14:16
 * table: sign_activity_prize
 */
public interface SignActivityPrizeService extends IService<SignActivityPrize> {

	/**
	 * 获取分页列表
	 *
	 * @param record
	 * @return
	 */
	Page<SignActivityPrize> getPage(SignActivityPrizeDto record);

	R editPrize(SignActivityPrizeDto dto);

}


