package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.vo.RealTimeKanbanSummaryVO;
import com.dy.yunying.api.datacenter.vo.RealTimeKanbanTimeSharingVO;

import java.util.List;
import java.util.Map;

/**
 * @author sunyq
 * @date 2022/8/17 9:28
 */
public interface RealTimeKanbanService {
	/**
	 * 实时看板汇总
	 * @return
	 */
	List<RealTimeKanbanSummaryVO> summary();
	/**
	 * 实时看板分时
	 * @return
	 */
	List<RealTimeKanbanTimeSharingVO> timeSharing();
}
