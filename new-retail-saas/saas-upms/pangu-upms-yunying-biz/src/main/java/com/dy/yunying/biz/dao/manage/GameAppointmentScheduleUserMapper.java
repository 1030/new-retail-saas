package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.GameAppointmentScheduleUser;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleUserReq;
import com.dy.yunying.api.resp.yyz.GameAppointmentScheduleUserRes;
import org.apache.ibatis.annotations.Mapper;


/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment
 */
@Mapper
public interface GameAppointmentScheduleUserMapper extends BaseMapper<GameAppointmentScheduleUser> {





	IPage<GameAppointmentScheduleUser> selectGameAppointmentUserList(GameAppointmentScheduleUserReq req);

}


