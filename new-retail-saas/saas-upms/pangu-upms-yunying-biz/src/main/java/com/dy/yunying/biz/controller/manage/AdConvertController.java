package com.dy.yunying.biz.controller.manage;

import com.dy.yunying.api.entity.AdvertiserMonitorInfoDO;
import com.dy.yunying.api.req.AdMonitorReq;
import com.dy.yunying.api.req.GetCallBackUrlByConvertIdReq;
import com.dy.yunying.api.req.TouTiaoClickReq;
import com.dy.yunying.api.vo.AdvertiserMonitorVO;
import com.dy.yunying.biz.service.manage.AdvertiseMonitorService;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName ConversionController.java
 * @createTime 2021年06月02日 13:56:00
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/convert")
public class AdConvertController {

	@Autowired
	private AdvertiseMonitorService advertiseMonitorService;

	/**
	 * 按条件查询 广告列表
	 *
	 * @return
	 */
	@SysLog("联调转化列表")
	@RequestMapping("/page")
	public R page(@RequestBody AdMonitorReq req){
		try {
			return advertiseMonitorService.page(req);
		} catch (Exception e) {
			log.error("分页查询广告信息异常：", e);
		}
		return R.failed();
	}

	/**
	 *新增广告
	 * */
	@SysLog("新建转化跟踪")
	@RequestMapping(value = "/add")
	public R insert(@RequestBody AdvertiserMonitorInfoDO record) {
		//非空检验
		R emptyCheckResult = this.emptyCheck(record);
		if (emptyCheckResult.getCode() != 0){
			return emptyCheckResult;
		}
		R result = advertiseMonitorService.insertAndPack(record);
		return result;
	}
	/**
	 * 编辑广告  根据id update记录
	 * @param record
	 * @return
	 */
	@SysLog("编辑转化跟踪")
	@RequestMapping(value = "/edit")
	public R update(@RequestBody AdvertiserMonitorInfoDO record) {
		//主键检验
		Long adid = record.getAdid();
		String adName = record.getAdname();
		if (adid == null || StringUtils.isBlank(adName)){
			return R.failed("参数不合法");
		}
		//adname 重复检验
		if (advertiseMonitorService.isAdNameExist(record)) {
			return R.failed("广告计划名称已存在");
		}

		boolean result = advertiseMonitorService.update(record);
		if (result) {
			return R.ok("更新成功");
		} else {
			return R.failed("更新失败");
		}
	}

	/**
	 * 批量复制
	 * @param param
	 * @return
	 */
	@SysLog("批量复制转化跟踪")
	@RequestMapping(value = "/batchCopy")
	public R batchCopy(@RequestBody Map<String,String> param) {
		if (param.get("adid") == null){
			return R.failed("未提供adid");
		}
		if (param.get("batchSize") == null) {
			return R.failed("未提供复制的个数");
		}

		Long adid = Long.valueOf(param.get("adid"));
		int batchSize = Integer.valueOf(param.get("batchSize"));
		boolean result = advertiseMonitorService.batchCopy(adid, batchSize);
		if (result) {
			return R.ok("复制成功");
		} else {
			return R.failed("复制失败");
		}
	}

	/**
	 * 批量更新
	 * @param param
	 * @return
	 */
	@SysLog("批量更新转化跟踪")
	@RequestMapping(value = "/batchRepack")
	public R batchRepack(@RequestBody Map<String,String> param) {
		String adids = param.get("adids");
		if (StringUtils.isBlank(adids)){
			return R.failed("未提供 广告id列表");
		}
		boolean result = advertiseMonitorService.batchRepack(adids);
		if (result) {
			return R.ok("更新成功");
		} else {
			return R.failed("更新失败");
		}
	}

	@SysLog("根据转化ID获取头条链接")
	@RequestMapping("/getCallBackUrlByConvertId")
	public R getCallBackUrlByConvertId(@Valid @RequestBody GetCallBackUrlByConvertIdReq req){
		try {
			return advertiseMonitorService.getCallBackUrlByConvertId(req.getAdPlatformId());
		} catch (Exception e) {
			log.error("根据转化ID获取头条链接：", e);
		}
		return R.failed();
	}

	@SysLog("模拟点击头条链接")
	@RequestMapping("/clickTouTiaoUrl")
	public R clickTouTiaoUrl(@Valid @RequestBody TouTiaoClickReq req){
		try {
			return advertiseMonitorService.clickTouTiaoUrl(req.getTouTiaoUrl());
		} catch (Exception e) {
			log.error("模拟点击头条链接：", e);
		}
		return R.failed();
	}

	/**
    非空检验
    */
	public R emptyCheck(AdvertiserMonitorInfoDO record) {
		if (record.getGid() == null) {
			return R.failed("请选择父游戏");
		}
		if (record.getChildGid() == null) {
			return R.failed("请选择子游戏");
		}
		if (StringUtils.isEmpty(record.getChl())) {
			return R.failed("未提供主渠道");
		}
		if (record.getPackId() == null) {
			return R.failed("请选择分包");
		}
		if (StringUtils.isEmpty(record.getAdvertiserId())) {
			return R.failed("未提供广告账户");
		}
		if (StringUtils.isEmpty(record.getAdname())) {
			return R.failed("未提供广告计划名称");
		}
		if (record.getAdname().length() > 100) {
			return R.failed("广告计划名称过长");
		}
		if (Objects.isNull(record.getPlatformId())){
			return R.failed("平台ID不能为空");
		}
		if (Objects.isNull(record.getAppType())){
			return R.failed("母包类型不能为空");
		}
		if (PlatformTypeEnum.TT.getValue().equals(record.getPlatformId())){
			// 头条 百度
			//adname 重复检验
			if (advertiseMonitorService.isAdNameExist(record)) {
				return R.failed("监测链接组名称已存在");
			}
		}else if (PlatformTypeEnum.GDT.getValue().equals(record.getPlatformId())){
			//
		}else if(PlatformTypeEnum.KS.getValue().equals(record.getPlatformId())){
			Map<String,Object> param = new HashMap<>();
			param.put("advertiserId",record.getAdvertiserId());
			param.put("gameid",String.valueOf(record.getChildGid()));
			// 验证同一个子游戏只能创建一个联调转化
			AdvertiserMonitorInfoDO advertiserMonitorInfoDO = advertiseMonitorService.selectAdMonitor(param);
			if (Objects.nonNull(advertiserMonitorInfoDO)){
				return R.failed("同一个广告账户同一个子游戏只能创建一个联调转化(应用)");
			}
		}else if (PlatformTypeEnum.BD.getValue().equals(record.getPlatformId())) {
			// 百度
			if (record.getAdname().length() > 50) {
				return R.failed("转化名称限制1-50字符且不可重复");
			}
			//adname 重复检验
			if (advertiseMonitorService.isAdNameExist(record)) {
				return R.failed("转化名称已存在");
			}
		}else{
			if (StringUtils.isBlank(PlatformTypeEnum.codeByValue(record.getPlatformId()))){
				return R.failed("无效的平台ID");
			}
		}
		if (StringUtils.isEmpty(record.getDomainAddr())) {
			return R.failed("未提供广告域名地址");
		}
		return R.ok();
	}

}
