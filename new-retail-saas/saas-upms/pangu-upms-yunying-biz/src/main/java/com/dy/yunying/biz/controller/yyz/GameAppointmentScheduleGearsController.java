package com.dy.yunying.biz.controller.yyz;

import com.dy.yunying.api.req.yyz.GameAppointmentReq;
import com.dy.yunying.api.req.yyz.GameAppointmentScheduleGearsReq;
import com.dy.yunying.biz.service.yyz.GameAppointmentScheduleGearsService;
import com.dy.yunying.biz.utils.ValidatorUtil;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * 游戏预约进度档位表
 *
 * @author leisw
 * @version 2022-05-09 17:14:29
 * table: game_appointment_schedule_gears
 */
@Inner(value = false)
@RestController
@RequiredArgsConstructor
@RequestMapping("/gameScheduleGears")
@Slf4j
public class GameAppointmentScheduleGearsController {

	private final GameAppointmentScheduleGearsService gameAppointmentScheduleGearsService;

	//	@SysLog("预约进度档位列表")
	@RequestMapping("/getPage")
	public R page(@RequestBody GameAppointmentScheduleGearsReq req) {
		if (StringUtils.isBlank(req.getType())){
			return R.failed("游戏类型不能为空");
		}
		return this.gameAppointmentScheduleGearsService.getPage(req);
	}

//	@SysLog("新增游戏预约进度")
	@RequestMapping("/gameAddSchedule")
	public R gameAddSchedule(@RequestBody GameAppointmentScheduleGearsReq req) {
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if (StringUtils.isBlank(req.getType())){
			return R.failed("游戏类型不能为空");
		}
		return this.gameAppointmentScheduleGearsService.gameAddSchedule(req);
	}

//	@SysLog("编辑游戏预约进度")
	@RequestMapping("/gameEditSchedule")
	public R gameEditSchedule(@RequestBody GameAppointmentScheduleGearsReq req){
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if (StringUtils.isBlank(req.getId())){
			return R.failed("游戏预约进度表主键不能为空");
		}
		return this.gameAppointmentScheduleGearsService.gameEditSchedule(req);
	}

	//	@SysLog("删除游戏预约进度")
	@RequestMapping("/gameDelSchedule")
	public R gameDelSchedule(@RequestBody GameAppointmentScheduleGearsReq req){
		if (Objects.isNull(req)) {
			return R.failed(null, 2, "请求参数错误");
		}
		if (StringUtils.isBlank(req.getId())){
			return R.failed("游戏预约进度表主键不能为空");
		}
		if (StringUtils.isBlank(req.getType())){
			return R.failed("游戏类型不能为空");
		}
		return this.gameAppointmentScheduleGearsService.gameDelSchedule(req);
	}

	/***
	 * 校验参数
	 * @param req
	 * @return
	 */
	public R checkRequest(GameAppointmentReq req) {
		if (StringUtils.isBlank(req.getMobile()) || Objects.isNull(req.getCodeType())
				|| StringUtils.isBlank(req.getCaptcha())) {
			return R.failed("请求参数不合法");
		}
		if (!ValidatorUtil.isMobile(req.getMobile())) {
			return R.failed("手机号格式不正确");
		}
		return null;
	}
}


