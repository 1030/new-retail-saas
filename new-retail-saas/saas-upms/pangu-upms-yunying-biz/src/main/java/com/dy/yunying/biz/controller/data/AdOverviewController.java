package com.dy.yunying.biz.controller.data;


import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.dto.AdOverviewDto;
import com.dy.yunying.api.dto.AdOverviewDto2;
import com.dy.yunying.api.vo.AdDataAnalysisVO;
import com.dy.yunying.biz.service.data.AdOverviewService;
import com.dy.yunying.biz.utils.DateUtils;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.MapUtils;
import com.pig4cloud.pig.common.core.exception.BusinessException;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@RestController
@RequestMapping("/adOverview")
@Slf4j
public class AdOverviewController {


    @Autowired
    private AdOverviewService adOverviewService;

	/**
	 * 广告概览数据
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@PostMapping("/list")
	public R list(@Valid @RequestBody AdOverviewDto2 req) {
		return adOverviewService.list(req);
	}

    /**
     * 广告概览数据
     *
     * @param req
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/getPlanStatistic")
    public R selectAdOverviewSource(@Valid AdOverviewDto req) {
			if(req.getCurrent() <= 0 ||  req.getSize() <= 0){
				return  R.failed("分页信息不允许为空");
			}
			return  adOverviewService.selectAdOverviewSource(req);
    }

	/**
	 * 广告概览导出
	 * @param req
	 * @return
	 */
	@ResponseExcel(name="广告数据分析报表导出",sheet = "广告数据分析报表导出")
	@GetMapping("/excelPlanStatistic")
    public R excelPlanStatistic(AdOverviewDto2 req, HttpServletResponse response, HttpServletRequest request) {
		try{
			String sheetName = "广告数据分析报表";
			R<AdDataAnalysisVO> result=adOverviewService.list(req);
			// list对象转listMap
			List<Map<String, Object>> resultListMap = MapUtils.objectsToMaps((List)result.getData());
			resultListMap.forEach(v->{
				changeValue(v,"activationRatio");
				changeValue(v,"clickActiveRatio");
				changeValue(v,"clickRatio");
				changeValue(v,"regPayRatio");
				changeValue(v,"regRatio");
				changeValue(v,"retention2Ratio");
				changeValue(v,"allRoi");
				changeValue(v,"monthRoi");
				changeValue(v,"weekRoi");
				changeValue(v,"roi1");
			});

			// 导出
			ExportUtils.exportExcelData(request,response,String.format("%s-%s.xlsx",sheetName,DateUtils.getCurrentTimeNoUnderline()), sheetName,req.getTitles(),req.getColumns(),resultListMap);
		} catch (BusinessException e) {
			log.error("excelPlanStatistic is error", e);
			throw e;
		} catch (Exception e) {
			log.error("excelPlanStatistic is error", e);
			throw new BusinessException("导出异常");
		}
      return null;
    }
    private void changeValue(Map<String, Object> v,String fieldName){
		BigDecimal ratio = (BigDecimal) v.get(fieldName);
		if(!Objects.isNull(ratio)){
			v.put(fieldName,String.format("%s%s",ratio.doubleValue(), Constant.PERCENT));
		}
	}

}
