package com.dy.yunying.biz.service.manage;

import com.dy.yunying.api.entity.WanGameChannelPlatformDO;

/**
 * @Author: hjl
 * @Date: 2020/7/21 13:52
 */
public interface GameChannelPlatformService {

	WanGameChannelPlatformDO getByPlatformAndGameid(Integer platformid, Integer gameid, Integer appid);

	WanGameChannelPlatformDO getByGameidAndParentChlId(Integer gameId, Integer parentChlId);

	WanGameChannelPlatformDO saveByUK(WanGameChannelPlatformDO wanGameChannelPlatformDO);
}