//package com.dy.yunying.biz.controller.common;
//
//import com.dy.yunying.api.req.hongbao.FileUploadReq;
//import com.dy.yunying.api.resp.hongbao.FileUploadRes;
//import com.dy.yunying.biz.utils.DateUtils;
//import com.dy.yunying.biz.utils.UploadUtils;
//import com.pig4cloud.pig.common.core.exception.BusinessException;
//import com.pig4cloud.pig.common.core.util.R;
//import com.sjda.framework.common.utils.StringUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.util.UUID;
//
///**
// * @author ：lile
// * @date ：2021/10/27 17:31
// * @description：
// * @modified By：
// */
//@Slf4j
//@RestController
//@RequestMapping("/file")
//public class FileController {
//
//	// 相对路劲地址
//	@Value(value = "${popup.image.uploadUrl}")
//	private String uploadUrl;
//
//	// 域名 前缀
//	@Value(value = "${popup.image.prefixUrl}")
//	private String prefixUrl;
//
//	/**
//	 * 图标上传
//	 *
//	 * @param file
//	 * @return
//	 */
//	@RequestMapping(value = "/imageUpload", method = RequestMethod.POST)
//	public R iconUpload(@RequestParam(value = "file", required = false) MultipartFile file) {
//		try {
//			FileUploadReq req = new FileUploadReq();
//			req.setDirPath("/image");
//			return upload(file, req);
//		} catch (Exception e) {
//			log.error("图片上传异常：", e);
//			return R.failed(e.getMessage());
//		}
//	}
//
//	/**
//	 * 文件上传服务
//	 *
//	 * @param file
//	 * @return
//	 */
//	public R upload(@RequestParam(value = "file", required = false) MultipartFile file, FileUploadReq req) {
//		long startTime = DateUtils.getCurrentDate().getTime();
//		FileUploadRes res = new FileUploadRes();
//		try {
//			String dirPath = req.getDirPath();
//			// 参数校验
//			if (StringUtils.isBlank(dirPath)) {
//				return R.failed("参数不合法");
//			}
//			if (file == null || file.isEmpty()) {
//				return R.failed("文件不能为空");
//			}
//
//			//获取文件名称
//			String filename = file.getOriginalFilename();
//			//获取后缀名
//			String suffix = filename.substring(filename.lastIndexOf(".") + 1);
//
//			// 生成上传到服务器上的文件名 使用MD5对当前上传时间进行加密的方式
//			UUID uuid = UUID.randomUUID();
//			String realName = uuid + "." + suffix;
//
//
//			// 相对路径
//			String fileRelaPath = dirPath + "/" + realName;
//
//			// 临时绝对路径
//			String realPath = uploadUrl + dirPath;
//			String fileUrl = prefixUrl + fileRelaPath;
//
//			// 文件保存到指定目录
//			UploadUtils.fileUpload(file, realPath, realName);
//
//			res.setPath(fileRelaPath);
//			res.setUrl(fileUrl);
//			return R.ok(res);
//		} catch (BusinessException e) {
//			log.error("文件上传异常：", e);
//			return R.failed(e.getMessage());
//		} catch (Exception e) {
//			log.error("文件上传异常：", e);
//		} finally {
//			long endTime = DateUtils.getCurrentDate().getTime();
//			log.info("文件上传，耗时：{}", endTime - startTime);
//		}
//		return R.failed();
//	}
//}
