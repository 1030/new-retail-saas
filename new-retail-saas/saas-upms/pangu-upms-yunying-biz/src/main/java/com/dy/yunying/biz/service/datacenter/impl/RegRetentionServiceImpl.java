package com.dy.yunying.biz.service.datacenter.impl;

import com.dy.yunying.api.datacenter.dto.RegRetentionDto;
import com.dy.yunying.api.datacenter.vo.RegRetentionVO;
import com.dy.yunying.biz.dao.datacenter.impl.RegRetentionDao;
import com.dy.yunying.biz.service.datacenter.RegRetentionService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * dogame 注册留存率
 * @author hma
 * @date 2022/8/19 15:37
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RegRetentionServiceImpl implements RegRetentionService {

	private final RegRetentionDao regRetentionDao;

	@Override
	public R<List<RegRetentionVO>> page(RegRetentionDto req) {
		List<RegRetentionVO> list;
		try {
			list = regRetentionDao.list(req);
		} catch (Exception e) {
			log.error("注册留存率数据查询失败：{}",e);
			return R.failed("注册留存率数据查询失败...");
		}
		return R.ok(list) ;
	}

	@Override
	public R count(RegRetentionDto req) {
		return R.ok(regRetentionDao.count(req));
	}

	@Override
	public R<List<RegRetentionVO>> collect(RegRetentionDto req) {
		//TODO
		Long count = regRetentionDao.count(req);
		if (count==0){
			return R.ok(new ArrayList<>());
		}

		return R.ok(regRetentionDao.collect(req));
	}

}
