package com.dy.yunying.biz.controller.data;

import com.dy.yunying.api.dto.AdRecoveryDto;
import com.dy.yunying.biz.service.data.AdRecoveryService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * @author ：lile
 * @date ：2021/6/22 11:00
 * @description：
 * @modified By：
 */
@RestController
@RequestMapping("/adRecovery")
@Slf4j
public class AdRecoveryController {

	@Autowired
	private AdRecoveryService adRecoveryService;


	/**
	 * 回收分析 列表
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping("/list")
	public R selectAdRecoverySource(@Valid AdRecoveryDto req) {
		return adRecoveryService.selectAdRecoverySource(req);
	}

}
