package com.dy.yunying.biz.dao.manage;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.ParentGameArea;
import org.apache.ibatis.annotations.Mapper;

/**
 * 主游戏区服关联表
 * @author  chengang
 * @version  2021-10-27 10:42:03
 * table: parent_game_area
 */
@Mapper
public interface ParentGameAreaMapper extends BaseMapper<ParentGameArea> {
}


