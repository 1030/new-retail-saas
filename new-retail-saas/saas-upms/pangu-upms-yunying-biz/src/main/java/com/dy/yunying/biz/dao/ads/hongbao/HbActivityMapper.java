/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.hongbao.HbActivity;
import com.dy.yunying.api.req.hongbao.KvpicActivityReq;
import com.dy.yunying.api.req.hongbao.SelectActivityPageReq;
import com.dy.yunying.api.resp.hongbao.AdminUserVo;
import com.dy.yunying.api.vo.hongbao.ActivityVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 红包活动表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:18
 */
@Mapper
public interface HbActivityMapper extends BaseMapper<HbActivity> {

    Page<ActivityVo> selectActivityVoPage(Page<HbActivity> page, @Param(Constants.WRAPPER) QueryWrapper<SelectActivityPageReq> query);

    List<AdminUserVo> selectAdminUserList(@Param("tenantId") Integer tenantId);

	/**
	 *  实体加了 @TableLogic 自动过滤了删除的记录，活动数据不需要过滤
	 * @param ids
	 * @return
	 */
	List<HbActivity> selectListByIds(List<Long> ids);

	int editKvpicActivity(KvpicActivityReq kvpicActivityReq);
}
