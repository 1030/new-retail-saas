package com.dy.yunying.biz.dao.manage.ext;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.entity.AdvertiserMonitorInfoDO;
import com.dy.yunying.api.req.AdMonitorReq;
import com.dy.yunying.api.vo.AdvertiserMonitorVO;
import com.dy.yunying.biz.dao.manage.AdvertiserMonitorInfoDOMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName AdvertiserMonitorInfoDOMapperExt.java
 * @createTime 2021年06月03日 11:19:00
 */
@Mapper
@Repository
public interface AdvertiserMonitorInfoDOMapperExt extends AdvertiserMonitorInfoDOMapper {

	IPage<AdvertiserMonitorVO> page(AdMonitorReq req);

	int insertSelective2(AdvertiserMonitorInfoDO record);

	/**
	 * EXT
	 * 分页查询广告
	 * */
	Page<AdvertiserMonitorInfoDO> selectPagedAds(AdMonitorReq req);

	/**
	 * Ext
	 * 按照广告名称查询
	 * */
	List<AdvertiserMonitorInfoDO> selectByAdName(AdvertiserMonitorInfoDO record);

	/**
	 * 广告ID和广告名称模糊查找筛选器
	 * @param req
	 * @return
	 */
	List<Map<String, Object>> queryAdsByParam(AdMonitorReq req);

	/**
	 * 分包编码 查找对应的广告位名称
	 *
	 * @param map
	 * @return
	 */
	List<AdvertiserMonitorVO> queryAdnameFromAppchl(Map<String, Object> map);

	/**
	 * 分包渠道找对应投放人
	 *
	 * @param map
	 * @return
	 */
	List<AdvertiserMonitorVO> queryMangenameFromAppchl(Map<String, Object> map);

	/**
	 * 分包渠道找对应子游戏名称
	 *
	 * @param map
	 * @return
	 */
	List<AdvertiserMonitorVO> queryGamenameFromAppchl(Map<String, Object> map);

	/***
	 * 投放人查分包渠道
	 * @param userId
	 * @return
	 */
	List<Map> findAppChlByManagerUid(@Param("userId") Integer userId);

	/***
	 * 投放人查广告ID
	 * @param userId
	 * @return
	 */
	List<Long> findAdIdByManagerUid(@Param("userId") Integer userId);

	/***
	 * 批量修改打包状态 为正在打包
	 * @param adids
	 * @return
	 */
	void uptPackStateByIds(@Param("adids") String adids);

	List<AdvertiserMonitorInfoDO> getByPKs(@Param("adids") String adids);

	int batchInsert(List<AdvertiserMonitorInfoDO> list);

	int batchUpdate(List<AdvertiserMonitorInfoDO> list);

	AdvertiserMonitorInfoDO selectAdMonitor(Map<String,Object> param);
}
