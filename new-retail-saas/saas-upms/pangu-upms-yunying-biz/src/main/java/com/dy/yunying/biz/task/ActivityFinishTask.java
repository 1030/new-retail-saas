package com.dy.yunying.biz.task;

import com.dy.yunying.biz.service.hongbao.ActivityService;
import com.dy.yunying.biz.service.manage.TtExtendPackageService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author yuwenfeng
 * @description: 活动到时下线
 * @date 2021/11/3 9:27
 */
@Component
public class ActivityFinishTask {

	/**
	 * 暂部开启活动自动下线
	 */

	@Autowired
	private ActivityService activityService;

	@XxlJob("activityFinishJob")
	public ReturnT<String> activityFinishJob(String param) {
		try {
			XxlJobLogger.log("--活动到时下线--开始--");
			activityService.activityFinishJob(param);
			XxlJobLogger.log("--活动到时下线--完成--");
		} catch (Exception e) {
			e.printStackTrace();
			XxlJobLogger.log("--活动到时下线--异常{}", e.getMessage());
		}
		return ReturnT.SUCCESS;
	}

	/**
	 * 抽奖活动自动下线
	 */
	@XxlJob("raffleActivityFinishJob")
	public ReturnT<String> raffleActivityFinishJob(String param) {
		try {
			XxlJobLogger.log("--抽奖活动到时下线--开始--");
			activityService.raffleActivityFinishJob(param);
			XxlJobLogger.log("--抽奖活动到时下线--完成--");
		} catch (Exception e) {
			e.printStackTrace();
			XxlJobLogger.log("--抽奖活动到时下线--异常{}", e.getMessage());
		}
		return ReturnT.SUCCESS;
	}

	/**
	 * 活动过期消息通知
	 * @param param
	 * @return
	 */
	@XxlJob("activityFinishSendMsgJob")
	public ReturnT<String> activityFinishSendMsgJob(String param) {
		try {
			XxlJobLogger.log(">>>活动过期消息通知");
			activityService.activityFinishSendMsgJob(param);
		} catch (Exception e) {
			e.printStackTrace();
			XxlJobLogger.log(">>>活动过期消息通知--异常{}", e.getMessage());
		}
		return ReturnT.SUCCESS;
	}

}
