package com.dy.yunying.biz.dao.manage.game;


import com.dy.yunying.api.entity.game.WanGameConfig;

/**
* @author hejiale
* @description 针对表【wan_game_config(子游戏)】的数据库操作Mapper
* @createDate 2022-04-21 14:27:58
* @Entity generate.entity.WanGameConfig
*/
public interface WanGameConfigMapper {

    int deleteByPrimaryKey(Long id);

    int insert(WanGameConfig record);

    int insertSelective(WanGameConfig record);

    WanGameConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WanGameConfig record);

    int updateByPrimaryKey(WanGameConfig record);

}
