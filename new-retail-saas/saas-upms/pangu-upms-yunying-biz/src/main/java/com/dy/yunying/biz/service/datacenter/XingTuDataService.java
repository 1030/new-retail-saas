package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.dto.XingTuDataDTO;
import com.dy.yunying.api.vo.XingTuDataVO;

import java.util.List;

/**
 * 星图统计接口
 */
public interface XingTuDataService {

	/**
	 * 星图报表总数
	 *
	 * @param xingtu
	 * @return
	 */
	int count(XingTuDataVO xingtu);

	/**
	 * 星图报表分页
	 *
	 * @param xingtu 查询条件
	 * @param isPage 是否分页
	 * @return
	 */
	List<XingTuDataDTO> page(XingTuDataVO xingtu, boolean isPage);

}
