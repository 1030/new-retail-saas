package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.ParentGameDO;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ParentGameDOMapper extends BaseMapper<ParentGameDO> {
	int deleteByPrimaryKey(Long id);

	int insert(ParentGameDO record);

	int insertSelective(ParentGameDO record);

	ParentGameDO selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(ParentGameDO record);

	int updateByPrimaryKey(ParentGameDO record);

	List<ParentGameDO> getParentGameByPgids(@Param("ids") Set<Long> ids);
}