package com.dy.yunying.biz.controller.sign;

import com.dy.yunying.api.req.sign.SignActivityDataReq;
import com.dy.yunying.biz.service.hbdata.SignActiveDataService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chenxiang
 * @className SignActiveDataController
 * @date 2021/12/2 20:31
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/signData")
@Api(value = "signData", tags = "签到活动数据")
@Slf4j
public class SignActiveDataController {
	@Autowired
	private SignActiveDataService signActiveDataService;

	/**
	 * 签到活动数据
	 * @param req
	 * @return
	 */
	@RequestMapping("/dataList")
	public R dataList(@RequestBody SignActivityDataReq req){
		return signActiveDataService.dataList(req);
	}
}
