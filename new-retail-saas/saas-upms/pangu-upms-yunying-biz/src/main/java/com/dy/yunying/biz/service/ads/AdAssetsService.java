package com.dy.yunying.biz.service.ads;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.api.entity.AdAssets;

/**
 * 广告资产表
 * @author  chenxiang
 * @version  2022-04-06 10:39:18
 * table: ad_assets
 */
public interface AdAssetsService extends IService<AdAssets> {
}


