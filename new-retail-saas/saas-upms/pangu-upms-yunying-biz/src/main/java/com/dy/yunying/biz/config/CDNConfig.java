package com.dy.yunying.biz.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * CDN配置
 * 动态刷新
 *
 * 访问密钥（AK/SK，Access Key ID/Secret Access Key）包含访问密钥ID（AK）和秘密访问密钥（SK）两部分
 * @Author: hjl
 * @Date: 2021/11/23 16:54
 */
@RefreshScope
@ConfigurationProperties("cdn.huawei")
@Component
public class CDNConfig {

	/**
	 * 刷新开关
	 */
	public static String cdn_refresh_flag;

	/**
	 * 访问密钥ID（AK）
	 */
	public static String ak;

	/**
	 * 秘密访问密钥（SK）
	 */
	public static String sk;

	/**
	 * 秘密访问密钥（SK）
	 */
	public static String enterprise_project_id = "all";

	public void setCdn_refresh_flag(String cdn_refresh_flag) {
		CDNConfig.cdn_refresh_flag = cdn_refresh_flag;
	}

	public void setAk(String ak) {
		CDNConfig.ak = ak;
	}

	public void setSk(String sk) {
		CDNConfig.sk = sk;
	}

	public void setEnterprise_project_id(String enterprise_project_id) {
		CDNConfig.enterprise_project_id = enterprise_project_id;
	}
}