package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbRedpackConfig;
import com.dy.yunying.api.resp.hongbao.CashExportVo;
import com.dy.yunying.api.resp.hongbao.RedPackExportVo;
import com.dy.yunying.api.vo.hongbao.HbRedpackConfigVo;
import com.pig4cloud.pig.common.core.util.R;
import org.apache.ibatis.annotations.Param;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.List;

/**
 * 红包配置表
 * @author  chenxiang
 * @version  2021-10-25 14:19:00
 * table: hb_redpack_config
 */
public interface HbRedpackConfigService extends IService<HbRedpackConfig> {
	/**
	 * 红包列表
	 * @param vo
	 * @return
	 */
	R getPage(HbRedpackConfigVo vo);
	/**
	 * 添加红包
	 * @param vo
	 * @return
	 */
	R addEditRedPack(HbRedpackConfigVo vo);
	/**
	 * 红包详情
	 * @param vo
	 * @return
	 */
	R details(HbRedpackConfigVo vo);
	/**
	 * 删除红包
	 * @param vo
	 * @return
	 */
	R delRedPack(HbRedpackConfigVo vo);
	/**
	 * 统计活动现金价值
	 * @param vo
	 * @return
	 */
	R getCashValues(HbRedpackConfigVo vo);

	List<RedPackExportVo> export4RedPack(Long activityId);

	List<CashExportVo> export4Cash(Long activityId);

	/**
	 * 活动下线导出礼包码时将未领取完的礼包码总数量更新未已领取数量
	 * @param activityId
	 * @return
	 */
	int updateHasCounts(@Param("activityId") Long activityId);
}


