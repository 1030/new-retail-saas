package com.dy.yunying.biz.task;

import com.dy.yunying.biz.service.hongbao.ActivityService;
import com.dy.yunying.biz.service.sign.SignActivityService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author yuwenfeng
 * @description: 签到活动到时下线
 * @date 2021/11/3 9:27
 */
@Component
public class SignActivityFinishTask {

	@Autowired
	private SignActivityService signActivityService;

	@XxlJob("signActivityFinishJob")
	public ReturnT<String> activityFinishJob(String param) {
		try {
			XxlJobLogger.log("--签到活动到时下线--开始--");
			signActivityService.activityFinishJob(param);
			XxlJobLogger.log("--签到活动到时下线--完成--");
		} catch (Exception e) {
			e.printStackTrace();
			XxlJobLogger.log("--签到活动到时下线--异常{}", e.getMessage());
		}
		return ReturnT.SUCCESS;
	}

}
