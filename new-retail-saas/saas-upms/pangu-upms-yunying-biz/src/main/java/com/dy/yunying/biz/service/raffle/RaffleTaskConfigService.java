package com.dy.yunying.biz.service.raffle;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.raffle.RaffleTaskConfig;
import com.dy.yunying.api.req.raffle.RaffleTaskConfigReq;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 抽奖任务配置表
 * @author  chenxiang
 * @version  2022-11-08 15:59:48
 * table: raffle_task_config
 */
public interface RaffleTaskConfigService extends IService<RaffleTaskConfig> {
	/**
	 * 列表
	 * @param record
	 * @return
	 */
	R getList(RaffleTaskConfigReq record);
	/**
	 * 新增
	 * @param record
	 * @return
	 */
	R addOrEdit(RaffleTaskConfig record);
	/**
	 * 删除
	 * @param record
	 * @return
	 */
	R del(RaffleTaskConfigReq record);

	void saveBatchList(List<RaffleTaskConfig> list);
}


