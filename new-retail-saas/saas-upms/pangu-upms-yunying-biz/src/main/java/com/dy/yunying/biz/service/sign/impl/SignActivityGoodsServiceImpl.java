package com.dy.yunying.biz.service.sign.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.sign.SignActivityGoods;
import com.dy.yunying.biz.dao.ads.sign.SignActivityGoodsMapper;
import com.dy.yunying.biz.service.sign.SignActivityGoodsService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 签到活动物品表
 * @author  chengang
 * @version  2021-12-01 10:14:07
 * table: sign_activity_goods
 */
@Log4j2
@Service("signActivityGoodsService")
@RequiredArgsConstructor
public class SignActivityGoodsServiceImpl extends ServiceImpl<SignActivityGoodsMapper, SignActivityGoods> implements SignActivityGoodsService {
	
	

	
}


