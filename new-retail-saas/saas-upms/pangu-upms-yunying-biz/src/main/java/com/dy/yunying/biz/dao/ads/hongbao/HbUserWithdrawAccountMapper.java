package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbUserWithdrawAccount;
import org.apache.ibatis.annotations.Mapper;

/**
 * 提现记录
 *
 * @author zhuxm
 * @version 2021-10-28 20:25:44
 */
@Mapper
public interface HbUserWithdrawAccountMapper extends BaseMapper<HbUserWithdrawAccount> {
}


