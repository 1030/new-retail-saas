package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.req.WanGameBasePackageReq;
import com.dy.yunying.api.vo.GamePackageManageVO;
import org.springframework.stereotype.Repository;

public interface WanGameBasePackageDOMapper {
    /*查询基础包列表*/
	IPage<GamePackageManageVO> queryWanGameBasePackageList(WanGameBasePackageReq req);

}
