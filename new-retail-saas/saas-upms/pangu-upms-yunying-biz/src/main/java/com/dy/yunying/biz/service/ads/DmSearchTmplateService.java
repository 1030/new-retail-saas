/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.service.ads;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.DmSearchTmplate;

/**
 * 搜索模板表
 *
 * @author xuzilei
 * @date 2021-06-17 16:45:36
 */
public interface DmSearchTmplateService extends IService<DmSearchTmplate> {

}
