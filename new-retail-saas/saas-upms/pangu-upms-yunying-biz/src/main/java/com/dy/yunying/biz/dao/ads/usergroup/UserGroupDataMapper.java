package com.dy.yunying.biz.dao.ads.usergroup;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.usergroup.UserGroupData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserGroupDataMapper extends BaseMapper<UserGroupData> {
}
