package com.dy.yunying.biz.controller.gametask;

import com.dy.yunying.api.entity.gametask.ExportGameTaskConfig;
import com.dy.yunying.api.req.gametask.GameTaskConfigReq;
import com.dy.yunying.api.req.gametask.GameTaskSelectReq;
import com.dy.yunying.api.req.gametask.ImportGameTaskReq;
import com.dy.yunying.api.req.gametask.ParentGameReq;
import com.dy.yunying.biz.service.gametask.GameTaskConfigService;
import com.dy.yunying.biz.utils.ExportUtils;
import com.dy.yunying.biz.utils.StringUtil;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.annotation.Inner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * 游戏任务配置
 * @author sunyq
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/gameTaskConfig")
public class GameTaskConfigController {

	private final GameTaskConfigService gameTaskConfigService;

	/**
	 * 分页列表
	 * @param gameTaskSelectReq
	 * @return
	 */
	@RequestMapping("/getPage")
	public R getPage(@RequestBody GameTaskSelectReq gameTaskSelectReq) {
		return gameTaskConfigService.getPage(gameTaskSelectReq);
	}

	/**
	 * execl文件导入
	 * @param importGameTaskReq
	 * @return
	 */
	@RequestMapping("/import")
	@SysLog("导入游戏任务配置")
	public R importGameTaskConfig(ImportGameTaskReq importGameTaskReq){
		try {
			return  gameTaskConfigService.importGameTaskConfig(importGameTaskReq);
		} catch (IOException e) {
			log.error("导入游戏任务配置失败 ，失败原因 "+ e.toString());
			return R.failed("导入失败,失败原因: "+e.toString());
		}
	}

	@RequestMapping("/export")
	public R exportTemp(HttpServletResponse response) {
		String fileName = "游戏任务配置模板.xlsx";
		String sheetName = "游戏任务配置";
		try {
			ExportUtils.exportExcelTemplate(fileName,sheetName,ExportGameTaskConfig.class,response);
		} catch (IOException e) {
			log.error("游戏配置模板导出异常，异常原因： "+e.toString());
		}
		return null;
	}


	@RequestMapping("/create")
	@SysLog("创建游戏任务配置")
	public R create(@Valid @RequestBody GameTaskConfigReq gameTaskConfigReq){
		// 验证入参
		R result = checkParams(gameTaskConfigReq);
		if (CommonConstants.FAIL.equals(result.getCode())) {
			return result;
		}
		gameTaskConfigReq.setId(null);
		return gameTaskConfigService.createEdit(gameTaskConfigReq);
	}

	private R checkParams(GameTaskConfigReq gameTaskConfigReq) {
		if (gameTaskConfigReq.getParentGameId() == null) {
			return R.failed("父游戏不能为空");
		}
		if (StringUtil.isBlank(gameTaskConfigReq.getAppId())){
			return R.failed("APPID不能为空，请检查当前游戏是否配置APPID");
		}
		if (StringUtil.isBlank(gameTaskConfigReq.getTaskId())){
			return R.failed("任务id不能为空");
		}
		if (StringUtil.isBlank(gameTaskConfigReq.getTaskDescription())){
			return R.failed("任务描述不能为空");
		}
		return R.ok();
	}


	@RequestMapping("/edit")
	@SysLog("编辑游戏任务配置")
	public R edit(@Valid @RequestBody GameTaskConfigReq gameTaskConfigReq){
		if (gameTaskConfigReq.getId() == null){
			return R.failed("游戏配置主键id不能为空");
		}
		// 验证入参
		R result = checkParams(gameTaskConfigReq);
		if (CommonConstants.FAIL.equals(result.getCode())) {
			return result;
		}
		return gameTaskConfigService.createEdit(gameTaskConfigReq);
	}


	@RequestMapping("/delete")
	@SysLog("删除游戏任务配置")
	public R delete(@RequestParam("id") Long id){
		gameTaskConfigService.removeById(id);
		return R.ok(0,"删除游戏配置成功");
	}


	@RequestMapping("/gameTaskList")
	public R getGameTaskListByParentGameIds(@RequestBody ParentGameReq parentGameReq){
		if (CollectionUtils.isEmpty(parentGameReq.getParentGameIds())){
			return R.failed("父游戏id集合不能为空");
		}
		return gameTaskConfigService.getGameTaskListByParentGameIds(parentGameReq.getParentGameIds());
	}

	@RequestMapping("/getAppIdByParentGameId")
	public R getAppIdByParentGameId(@RequestParam("parentGameId") Long parentGameId){
		return gameTaskConfigService.getAppIdByParentGameId(parentGameId);
	}



}
