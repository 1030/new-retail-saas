/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.dy.yunying.biz.service.sign.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.entity.ParentGameArea;
import com.dy.yunying.api.entity.hongbao.HbActivityArea;
import com.dy.yunying.api.entity.hongbao.HbGiftBag;
import com.dy.yunying.api.entity.hongbao.PopupNotice;
import com.dy.yunying.api.entity.sign.SignActivity;
import com.dy.yunying.api.entity.sign.SignActivityPrize;
import com.dy.yunying.api.entity.sign.SignPrizeGoods;
import com.dy.yunying.api.entity.sign.SignTaskConfigDO;
import com.dy.yunying.api.enums.ActivityStatusEnum;
import com.dy.yunying.api.enums.SourceTypeEnum;
import com.dy.yunying.api.req.hongbao.OnlineStatusReq;
import com.dy.yunying.api.req.hongbao.SelectSignActivityPageReq;
import com.dy.yunying.api.req.sign.SignActivityReq;
import com.dy.yunying.api.resp.hongbao.NodeData;
import com.dy.yunying.api.resp.hongbao.SignActivityVo;
import com.dy.yunying.biz.dao.ads.hongbao.HbActivityAreaMapper;
import com.dy.yunying.biz.dao.ads.hongbao.PopupNoticeMapper;
import com.dy.yunying.biz.dao.ads.sign.SignActivityMapper;
import com.dy.yunying.biz.dao.ads.sign.SignActivityPrizeMapper;
import com.dy.yunying.biz.dao.manage.ParentGameAreaMapper;
import com.dy.yunying.biz.dao.manage.ext.ParentGameDOMapperExt;
import com.dy.yunying.biz.service.hongbao.HbGiftBagService;
import com.dy.yunying.biz.service.sign.SignActivityPrizeService;
import com.dy.yunying.biz.service.sign.SignActivityService;
import com.dy.yunying.biz.service.sign.SignPrizeGoodsService;
import com.dy.yunying.biz.service.sign.SignTaskConfigService;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 签到活动表
 *
 * @author yuwenfeng
 * @date 2021-11-30 19:46:19
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SignActivityServiceImpl extends ServiceImpl<SignActivityMapper, SignActivity> implements SignActivityService {

	private final ParentGameAreaMapper parentGameAreaMapper;

	private final ParentGameDOMapperExt parentGameDOMapperExt;

	private final HbActivityAreaMapper activityAreaMapper;

	private final SignActivityPrizeService signActivityPrizeService;

	private final SignActivityPrizeMapper signActivityPrizeMapper;

	private final HbGiftBagService hbGiftBagService;

	private final SignTaskConfigService signTaskConfigService;

	private final PopupNoticeMapper popupNoticeMapper;

	private final SignPrizeGoodsService signPrizeGoodsService;

	@Value("${notice_sign_activity_url}")
	private String noticeSignActivityUrl;

	@Value("${wechat_sign_activity_url}")
	private String wechatSignActivityUrl;

	@Override
	public Page<SignActivityVo> selectSignActivityPage(Page<SignActivity> page, QueryWrapper<SignActivity> query) {
		Page<SignActivity> pageVo = baseMapper.selectPage(page, query);
		Page<SignActivityVo> result = new Page<SignActivityVo>(page.getCurrent(), page.getSize());
		if (CollectionUtils.isNotEmpty(pageVo.getRecords())) {
			result.setTotal(pageVo.getTotal());
			result.setCountId(pageVo.getCountId());
			result.setPages(pageVo.getPages());
			Date nowTime = new Date();
			List<ParentGameArea> areaList = parentGameAreaMapper.selectList(new QueryWrapper<ParentGameArea>());
			List<NodeData> nodeList = parentGameDOMapperExt.selectMainGameList();
			List<Long> signActivityIdResult = pageVo.getRecords().stream().map(SignActivity::getId).collect(Collectors.toList());
			//游戏区服
			List<HbActivityArea> allActivityAreaList = activityAreaMapper.selectList(new QueryWrapper<HbActivityArea>().in("activity_id", signActivityIdResult).eq("source_type", SourceTypeEnum.SIGN.getType()).orderByAsc("area_id"));
			List<SignActivityVo> signActivityVoList = new ArrayList<SignActivityVo>();
			StringBuffer sbStr;
			for (SignActivity signActivity : pageVo.getRecords()) {
				final SignActivityVo vo = JSON.parseObject(JSON.toJSONString(signActivity), SignActivityVo.class);
				vo.setFinishDate(DateUtils.getFirstSecondOfOneDay(signActivity.getFinishTime()).getTime());
				sbStr = new StringBuffer();
				if (CollectionUtils.isNotEmpty(nodeList)) {
					for (NodeData nodeData : nodeList) {
						if (vo.getParentGameId().toString().equals(nodeData.getNodeId())) {
							sbStr.append(nodeData.getNodeName());
							sbStr.append("：");
						}
					}
				}
				//游戏区服
				List<HbActivityArea> activityAreaList = allActivityAreaList.stream().filter(ac -> ac.getActivityId().equals(vo.getId())).collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(activityAreaList)) {
					Long[] areaDataList = new Long[activityAreaList.size()];
					int i = 0;
					for (HbActivityArea activityArea : activityAreaList) {
						areaDataList[i] = activityArea.getAreaId();
						i++;
						if (CollectionUtils.isNotEmpty(areaList)) {
							for (ParentGameArea gameArea : areaList) {
								if (activityArea.getAreaId().equals(gameArea.getAreaId()) && vo.getParentGameId().equals(gameArea.getParentGameId())) {
									sbStr.append(gameArea.getAreaName());
									sbStr.append("，");
								}
							}
						}
					}
					vo.setAreaDataList(areaDataList);
				}
				String rangeName = sbStr.toString();
				if (rangeName.length() > 0) {
					if (rangeName.lastIndexOf("：") == rangeName.length() - 1) {
						rangeName = rangeName.substring(0, rangeName.length() - 1);
					}
					if (rangeName.lastIndexOf("，") == rangeName.length() - 1) {
						rangeName = rangeName.substring(0, rangeName.length() - 1);
					}
				}
				vo.setRangeName(rangeName);
				vo.setNoticeSignActivityUrl(noticeSignActivityUrl + vo.getId());
				vo.setWechatSignActivityUrl(wechatSignActivityUrl + vo.getId());
				signActivityVoList.add(vo);
			}
			result.setRecords(signActivityVoList);
		}
		return result;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R addSignActivity(SignActivityReq signActivityReq) {
		Date startTime = DateUtils.stringToStartDate(signActivityReq.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == startTime) {
			return R.failed("开始时间不合法");
		}
		if (startTime.getTime() < DateUtils.getStartTimeStamp()) {
			return R.failed("开始时间不能比当前日期0点小");
		}
		Date finishTime = DateUtils.stringToEndDate(signActivityReq.getFinishTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == finishTime) {
			return R.failed("结束时间不合法");
		}
		if (finishTime.getTime() <= startTime.getTime()) {
			return R.failed("结束时间不能比开始时间小");
		}
		if (finishTime.getTime() < System.currentTimeMillis()) {
			return R.failed("结束时间不能比当前时间小");
		}
		if (null != signActivityReq.getAreaList() && signActivityReq.getAreaList().length > 0) {
			for (Long area : signActivityReq.getAreaList()) {
				List<SignActivity> repeatList = activityAreaMapper.selectCountSignActivityRepeat(signActivityReq.getParentGameId(), area, SourceTypeEnum.SIGN.getType(), ActivityStatusEnum.ACTIVITING.getStatus(), DateUtils.dateToString(startTime, DateUtils.YYYY_MM_DD_HH_MM_SS), DateUtils.dateToString(finishTime, DateUtils.YYYY_MM_DD_HH_MM_SS));
				if (CollectionUtils.isNotEmpty(repeatList)) {
					return R.failed("活动时间或父游戏或区服与已有同类型活动重叠，操作失败");
				}
			}
		} else {
			return R.failed("区服范围不能为空");
		}
		int signDays = DateUtils.betweenDays(startTime, finishTime);
		if (signDays > 365) {
			return R.failed("签到天数不能大于365天");
		}
		signDays = signDays + 1;
		SignActivity info = JSON.parseObject(JSON.toJSONString(signActivityReq), SignActivity.class);
		info.setStartTime(startTime);
		info.setFinishTime(finishTime);
		info.setActivityStatus(ActivityStatusEnum.READY.getStatus());
		info.setSignDays(signDays);
		info.setActivityName(signActivityReq.getActivityName().trim());
		info.setCreateTime(new Date());
		info.setCreateId(SecurityUtils.getUser().getId().longValue());
		info.setUpdateTime(new Date());
		info.setUpdateId(SecurityUtils.getUser().getId().longValue());
		baseMapper.insert(info);
		if (null != signActivityReq.getAreaList() && signActivityReq.getAreaList().length > 0) {
			generateSignActivityArea(info.getId(), signActivityReq.getParentGameId(), signActivityReq.getAreaList());
		}
		//添加奖励配置数据
		generateSignActivityPrize(signDays, startTime, info.getId());
		return R.ok();
	}

	@Transactional(rollbackFor = Exception.class)
	public void generateSignActivityArea(Long activityId, Long parentGameId, Long[] areaList) {
		activityAreaMapper.delete(new QueryWrapper<HbActivityArea>().eq("activity_id", activityId).eq("source_type", SourceTypeEnum.SIGN.getType()));
		HbActivityArea activityArea;
		for (Long areaId : areaList) {
			activityArea = new HbActivityArea();
			activityArea.setActivityId(activityId);
			activityArea.setSourceType(SourceTypeEnum.SIGN.getType());
			activityArea.setGameId(parentGameId);
			activityArea.setAreaId(areaId);
			activityArea.setAreaRange(2);
			activityArea.setCreateTime(new Date());
			activityArea.setCreateId(SecurityUtils.getUser().getId().longValue());
			activityArea.setUpdateTime(new Date());
			activityArea.setUpdateId(SecurityUtils.getUser().getId().longValue());
			activityAreaMapper.insert(activityArea);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void generateSignActivityPrize(int signDays, Date startTime, Long activityId) {
		//删除已经导入的礼包码
		List<SignActivityPrize> activityPrizeList = signActivityPrizeMapper.selectList(Wrappers.<SignActivityPrize>lambdaQuery()
				.eq(SignActivityPrize::getActivityId, activityId)
				.eq(SignActivityPrize::getDeleted, 0));
		List<Long> prizeIds = activityPrizeList.stream().map(SignActivityPrize::getId).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(prizeIds)) {
			hbGiftBagService.remove(Wrappers.<HbGiftBag>lambdaQuery().in(HbGiftBag::getObjectId, prizeIds).eq(HbGiftBag::getType, 3));
		}
		//逻辑删除奖品配置
		signActivityPrizeMapper.updatePrizeByActivityId(activityId);
		SignActivityPrize signPrize;
		List<SignActivityPrize> signPrizeList = new ArrayList<SignActivityPrize>();
		for (int i = 0; i < signDays; i++) {
			signPrize = new SignActivityPrize();
			signPrize.setActivityId(activityId);
			signPrize.setReceiveSn(i + 1);
			signPrize.setReceiveDate(DateUtils.addDays(startTime, i));
			signPrize.setCreateTime(new Date());
			signPrize.setCreateId(SecurityUtils.getUser().getId().longValue());
			signPrize.setUpdateTime(new Date());
			signPrize.setUpdateId(SecurityUtils.getUser().getId().longValue());
			signPrizeList.add(signPrize);
		}
		signActivityPrizeService.saveBatch(signPrizeList);
	}

	@Override
	@Transactional(rollbackFor = Exception.class, transactionManager = "adsTransactionManager")
	public R editSignActivity(SignActivityReq signActivityReq) {
		if (null == signActivityReq.getId() || signActivityReq.getId() <= 0) {
			return R.failed("签到活动ID不能为空");
		}
		Date startTime = DateUtils.stringToStartDate(signActivityReq.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == startTime) {
			return R.failed("开始时间不合法");
		}
		if (startTime.getTime() < DateUtils.getStartTimeStamp()) {
			return R.failed("开始时间不能比当前日期0点小");
		}
		Date finishTime = DateUtils.stringToEndDate(signActivityReq.getFinishTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == finishTime) {
			return R.failed("结束时间不合法");
		}
		if (finishTime.getTime() <= startTime.getTime()) {
			return R.failed("结束时间不能比开始时间小");
		}
		if (finishTime.getTime() < System.currentTimeMillis()) {
			return R.failed("结束时间不能比当前时间小");
		}
		if (null != signActivityReq.getAreaList() && signActivityReq.getAreaList().length > 0) {
			for (Long area : signActivityReq.getAreaList()) {
				List<SignActivity> repeatList = activityAreaMapper.selectCountSignActivityRepeat(signActivityReq.getParentGameId(), area, SourceTypeEnum.SIGN.getType(), ActivityStatusEnum.ACTIVITING.getStatus(), DateUtils.dateToString(startTime, DateUtils.YYYY_MM_DD_HH_MM_SS), DateUtils.dateToString(finishTime, DateUtils.YYYY_MM_DD_HH_MM_SS));
				if (CollectionUtils.isNotEmpty(repeatList)) {
					for (SignActivity activity : repeatList) {
						if (!signActivityReq.getId().equals(activity.getId())) {
							return R.failed("活动时间或父游戏或区服与已有同类型活动重叠，操作失败");
						}
					}
				}
			}
		} else {
			return R.failed("区服范围不能为空");
		}
		int signDays = DateUtils.betweenDays(startTime, finishTime);
		if (signDays > 365) {
			return R.failed("签到天数不能大于365天");
		}
		signDays = signDays + 1;
		SignActivity info = baseMapper.selectOne(new QueryWrapper<SignActivity>().eq("id", signActivityReq.getId()).eq("activity_status", ActivityStatusEnum.READY.getStatus()));
		if (null == info) {
			return R.failed("当前状态无法进行编辑");
		}
		//更新奖品配置数据,时间有改变时
		if (!DateUtils.dateToString(info.getStartTime()).equals(DateUtils.dateToString(startTime)) || !DateUtils.dateToString(info.getFinishTime()).equals(DateUtils.dateToString(finishTime))) {
			generateSignActivityPrize(signDays, startTime, info.getId());
			//清空任务配置
			signTaskConfigService.deleteTaskConfigByActivityId(info.getId());
		}
		info = JSON.parseObject(JSON.toJSONString(signActivityReq), SignActivity.class);
		info.setStartTime(startTime);
		info.setFinishTime(finishTime);
		info.setActivityStatus(ActivityStatusEnum.READY.getStatus());
		info.setSignDays(signDays);
		info.setActivityName(signActivityReq.getActivityName().trim());
		info.setUpdateTime(new Date());
		info.setUpdateId(SecurityUtils.getUser().getId().longValue());
		baseMapper.updateById(info);
		if (null != signActivityReq.getAreaList() && signActivityReq.getAreaList().length > 0) {
			generateSignActivityArea(info.getId(), signActivityReq.getParentGameId(), signActivityReq.getAreaList());
		}
		return R.ok();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R onlineSignActivity(OnlineStatusReq req) {
		SignActivity info;
		if (ActivityStatusEnum.ACTIVITING.getStatus().equals(req.getStatus())) {
			info = baseMapper.selectOne(new QueryWrapper<SignActivity>().eq("id", req.getId()).eq("activity_status", ActivityStatusEnum.READY.getStatus()));
			if (null == info) {
				return R.failed("当前状态无法上线");
			}
			//校验奖品是否配置
			int num = signActivityPrizeMapper.selectPrizeFullCount(info.getId());
			if (num < info.getSignDays()) {
				return R.failed("有档次奖品未配置，请先配置奖品");
			}
		} else if (ActivityStatusEnum.CLOSE.getStatus().equals(req.getStatus())) {
			info = baseMapper.selectOne(new QueryWrapper<SignActivity>().eq("id", req.getId()).eq("activity_status", ActivityStatusEnum.ACTIVITING.getStatus()));
			if (null == info) {
				return R.failed("当前状态无法下线");
			}
			//下线关联该活动的公告
//			closePopupNotice(info.getId());
		} else {
			return R.failed("操作参数不合法");
		}
		info.setActivityStatus(req.getStatus());
		info.setUpdateTime(new Date());
		info.setUpdateId(SecurityUtils.getUser().getId().longValue());
		baseMapper.updateById(info);
		return R.ok();
	}

	/**
	 * @description: 下线关联该活动的公告
	 * @author yuwenfeng
	 * @date 2021/12/9 13:55
	 */
//	private void closePopupNotice(Long signActivityId) {
//		List<PopupNotice> noticeList = popupNoticeMapper.selectList(new QueryWrapper<PopupNotice>().eq("img_url", noticeSignActivityUrl + signActivityId));
//		if (CollectionUtils.isNotEmpty(noticeList)) {
//			for (PopupNotice popupNotice : noticeList) {
//				popupNotice.setPopupStatus(ActivityStatusEnum.CLOSE.getStatus());
//				popupNotice.setUpdateTime(new Date());
//				popupNoticeMapper.updateById(popupNotice);
//			}
//		}
//	}

	@Override
	public List<NodeData> selectAreaListByPgameIds(String pGgmeIds) {
		List<ParentGameArea> gameAreaList = parentGameAreaMapper.selectList(new QueryWrapper<ParentGameArea>().eq("parent_game_id", pGgmeIds).gt("area_id", 0).eq("deleted", Constant.DEL_NO));
		return convertList(gameAreaList);
	}

	@Override
	public void activityFinishJob(String param) {
		List<SignActivity> list = baseMapper.selectList(new QueryWrapper<SignActivity>().eq("activity_status", ActivityStatusEnum.ACTIVITING.getStatus()).le("finish_time", DateUtils.getcurrentTime()));
		if (CollectionUtils.isNotEmpty(list)) {
			for (SignActivity activity : list) {
				activity.setActivityStatus(ActivityStatusEnum.CLOSE.getStatus());
				activity.setUpdateTime(new Date());
				baseMapper.updateById(activity);
				//下线关联该活动的公告
//				closePopupNotice(activity.getId());
			}
		}
	}

	/**
	 * 签到活动下拉列表查询
	 *
	 * @param activityReq
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, transactionManager = "adsTransactionManager")
	@Override
	public List<SignActivity> selectActivityList(SelectSignActivityPageReq activityReq) {
		return this.list(Wrappers.<SignActivity>lambdaQuery().select(SignActivity::getId, SignActivity::getActivityName).eq(SignActivity::getDeleted, 0));
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R copySignActivity(SignActivityReq signActivityReq) {
		Long oldActivityId = signActivityReq.getId();
		if (null == oldActivityId) {
			return R.failed("复制的签到活动ID不能为空");
		}
		Date startTime = DateUtils.stringToStartDate(signActivityReq.getStartTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == startTime) {
			return R.failed("开始时间不合法");
		}
		if (startTime.getTime() < DateUtils.getStartTimeStamp()) {
			return R.failed("开始时间不能比当前日期0点小");
		}
		Date finishTime = DateUtils.stringToEndDate(signActivityReq.getFinishTime(), DateUtils.YYYY_MM_DD_HH_MM_SS);
		if (null == finishTime) {
			return R.failed("结束时间不合法");
		}
		if (finishTime.getTime() <= startTime.getTime()) {
			return R.failed("结束时间不能比开始时间小");
		}
		if (finishTime.getTime() < System.currentTimeMillis()) {
			return R.failed("结束时间不能比当前时间小");
		}
		if (null != signActivityReq.getAreaList() && signActivityReq.getAreaList().length > 0) {
			for (Long area : signActivityReq.getAreaList()) {
				List<SignActivity> repeatList = activityAreaMapper.selectCountSignActivityRepeat(signActivityReq.getParentGameId(), area, SourceTypeEnum.SIGN.getType(), ActivityStatusEnum.ACTIVITING.getStatus(), DateUtils.dateToString(startTime, DateUtils.YYYY_MM_DD_HH_MM_SS), DateUtils.dateToString(finishTime, DateUtils.YYYY_MM_DD_HH_MM_SS));
				if (CollectionUtils.isNotEmpty(repeatList)) {
					return R.failed("活动时间或父游戏或区服与已有同类型活动重叠，操作失败");
				}
			}
		} else {
			return R.failed("区服范围不能为空");
		}
		int signDays = DateUtils.betweenDays(startTime, finishTime);
		if (signDays > 365) {
			return R.failed("签到天数不能大于365天");
		}
		signDays = signDays + 1;
		SignActivity info = new SignActivity();
		//先设置为空
		signActivityReq.setId(null);
		BeanUtils.copyProperties(signActivityReq, info);
		info.setStartTime(startTime);
		info.setFinishTime(finishTime);
		info.setActivityStatus(ActivityStatusEnum.READY.getStatus());
		info.setSignDays(signDays);
		info.setCreateId(SecurityUtils.getUser().getId().longValue());
		info.setUpdateId(SecurityUtils.getUser().getId().longValue());
		baseMapper.insert(info);
		if (null != signActivityReq.getAreaList() && signActivityReq.getAreaList().length > 0) {
			//添加屈服
			generateSignActivityArea(info.getId(), signActivityReq.getParentGameId(), signActivityReq.getAreaList());
		}
		//添加奖励配置数据
		copySignActivityPrize(oldActivityId, info.getId(), startTime);
		//复制任务数据
		//generateSignActivityTask(oldActivityId, info.getId());
		return R.ok();
	}

	/**
	 * 批量复制签到奖励
	 * @param activityId
	 */
	@Transactional(rollbackFor = Exception.class)
	public void copySignActivityPrize(Long oldActivityId, Long activityId, Date startTime) {
		List<SignActivityPrize> list = signActivityPrizeMapper.selectList(Wrappers.<SignActivityPrize>lambdaQuery().eq(SignActivityPrize :: getActivityId, oldActivityId).eq(SignActivityPrize :: getDeleted, 0));
		if(CollectionUtils.isNotEmpty(list)){
			list.stream().forEach(e -> {
				Long oldActivityPrizeId = e.getId();
				e.setActivityId(activityId);
				e.setCreateId(SecurityUtils.getUser().getId().longValue());
				e.setUpdateId(null);
				e.setCreateTime(new Date());
				e.setUpdateTime(null);
				e.setReceiveDate(DateUtils.addDays(startTime, e.getReceiveSn()-1));
				signActivityPrizeMapper.insert(e);
				//新的签到活动奖品ID
				Long activityPrizeId = e.getId();
				List<SignPrizeGoods> signPrizeGoodsList =  signPrizeGoodsService.list(Wrappers.<SignPrizeGoods>lambdaQuery().eq(SignPrizeGoods :: getPrizeId, oldActivityPrizeId).eq(SignPrizeGoods :: getDeleted, 0));
				if(CollectionUtils.isNotEmpty(signPrizeGoodsList)){
					signPrizeGoodsList.stream().forEach(goods -> {
						//设置新活动ID
						goods.setPrizeId(activityPrizeId);
						goods.setCreateId(SecurityUtils.getUser().getId().longValue());
						goods.setUpdateId(null);
						goods.setCreateTime(new Date());
						goods.setUpdateTime(new Date());
					});
					signPrizeGoodsService.saveBatch(signPrizeGoodsList);
				}
			});
		}
	}

	/**
	 * 复制任务数据
	 * @param oldActivityId
	 * @param id
	 */
	@Transactional(rollbackFor = Exception.class)
	public void generateSignActivityTask(Long oldActivityId, Long id){
		//原有的任务数据
		List<SignTaskConfigDO> list = signTaskConfigService.list(Wrappers.<SignTaskConfigDO>query().lambda().eq(SignTaskConfigDO::getActivityId, oldActivityId).eq(SignTaskConfigDO::getDeleted, 0));
		list.stream().forEach(e -> {
			//设置新活动ID
			e.setActivityId(id);
			e.setCreateId(SecurityUtils.getUser().getId().longValue());
			e.setUpdateId(null);
			e.setCreateTime(new Date());
			e.setUpdateTime(null);
		});
		signTaskConfigService.saveBatch(list);
	}

	private List<NodeData> convertList(List<ParentGameArea> gameAreaList) {
		if (CollectionUtils.isNotEmpty(gameAreaList)) {
			List<NodeData> nodeList = new ArrayList<NodeData>();
			NodeData node;
			for (ParentGameArea gameArea : gameAreaList) {
				node = new NodeData();
				node.setNodeId(gameArea.getAreaId().toString());
				node.setNodeName(gameArea.getAreaName());
				nodeList.add(node);
			}
			return nodeList;
		}
		return null;
	}
}
