package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.PopupNoticeGame;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PopupNoticeGameMapper extends BaseMapper<PopupNoticeGame> {

	int insertBatchPopupNoticeGame(List<PopupNoticeGame> popupNoticeGameList);
}
