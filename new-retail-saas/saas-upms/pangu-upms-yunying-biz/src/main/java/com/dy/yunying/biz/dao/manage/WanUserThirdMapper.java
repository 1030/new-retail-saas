package com.dy.yunying.biz.dao.manage;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.WanUserThird;

/**
 * @Entity com.dy.yunying.biz.domain.WanUserThird
 */
public interface WanUserThirdMapper extends BaseMapper<WanUserThird> {

}




