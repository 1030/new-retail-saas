package com.dy.yunying.biz.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author chengang
 * @Date 2021/12/27
 */
@RefreshScope
@Configuration
@Data
public class YunYingProperties {

	@Value(value = "${refresh.demo:defaultValue}")
	private String refreshDemo;

	// 30天  注册表
	@Value("${ninetydeviceregtable:dwd_200_pangu_atrb_device_reg_20220307}")
	private String ninetydeviceregtable;

	// 30天  计划日流水表
	@Value("${adidrebatetable:v_dwd_200_pangu_adid_rebate_day_20220307}")
	private String adidrebatetable;

	// 30天 创意日报表
	@Value("${creativeDayReport:dwd_200_pangu_creative_rebate_day_report_20220307}")
	private String creativeDayReport;

	// 30天 体验版素材日报表
	@Value("${materialDayReport:dwd_200_pangu_oe_material_rebate_day_report_20220307}")
	private String materialDayReport;

	// 30天  渠道和计划关系表
	@Value("${adptypetable:dwd_200_pangu_ad_ptype_2022030701}")
	private String adptypetable;

	// 30天  设备注册登录中间表
	@Value("${deviceregisterlogintable:dwd_200_pangu_atrb_device_register_login_20220307}")
	private String deviceregisterlogintable;

	// 30天  设备注册登录中间表
	@Value("${deviceregisterRechargetable:dwd_200_pangu_device_register_recharge_20220413}")
	private String deviceregisterRechargetable;

	// 新增账户注册表
	@Value("${accountregistertable:dwd_200_pangu_atrb_account_reg_20220307}")
	private String accountregistertable;

	// 广告计划信息表
	@Value("${panguadidtable:v_dwd_200_pangu_adid_new}")
	private String panguadidtable;

	@Value("${redpack_export_status:true}")
	private Boolean redPackExportStatus;

	@Value("${pack.api_v3_detection_domain}")
	private String api_v3_detection_domain;

	@Value(value = "${hbdxtable:v_event_1433669519229980672}")
	private String hbdxtable;

	@Value("${sms.tencent.secretId}")
	private String secretId;

	@Value("${sms.tencent.secretKey}")
	private String secretKey;

	@Value("${sms.tencent.signName}")
	private String signName;

	@Value("${sms.tencent.smsSdkAppid}")
	private String smsSdkAppid;

	@Value("${sms.tencent.templateCode}")
	private String templateCode;

	@Value(value = "${common_popup_times:999}")
	private int commonPopupTimes;

	@Value("${yyz.frequency.number}")
	private String number;

	@Value("${yyz.taptap.subscribe.url}")
	private String tapTapUrl;

	@Value("${yyz.taptap.subscribe.param}")
	private String tapTapParam;

	@Value("${yyy.isopen.status}")
	private boolean isOpen;

	@Value("${yyy.taptap.subscribe:30249}")
	private Long subscribe;

	@Value(value = "${notice_dx_view}")
	private String noticeDxView;

	@Value(value = "${dongxin_appid}")
	private String dongxinAppid;

	@Value(value = "${dongxin_analyse_url}")
	private String dongxinAnalyseUrl;

	//群组功能表dim_operation_mysql_user_group_data_20230207
	@Value("${dimOperationMysqlUserGroupData:dim_operation_mysql_user_group_data_20230207}")
	private String dimOperationMysqlUserGroupData;

	//群组功能表dim_operation_mysql_user_group_20230207
	@Value("${dimOperationMysqlUserGroup:dim_operation_mysql_user_group_20230207}")
	private String dimOperationMysqlUserGroup;

	//群组功能表ds_dy_operation_account
	@Value("${dsDyOperationAccount:ds_dy_operation_account}")
	private String dsDyOperationAccount;

	//群组功能视图v_dy_operation_Account
	@Value("${vOperationAccountTable:v_operation_account_20230207}")
	private String vOperationAccountTable;

	//群组功能表ds_dy_operation_role
	@Value("${dsDyOperationRole:ds_dy_operation_role}")
	private String dsDyOperationRole;

	//群组功能视图v_dy_operation_role
	@Value("${vOperationRoleTable:v_operation_role_20230207}")
	private String vOperationRoleTable;

	// 设备每日在线时长表
	@Value("${deviceOnlineTimetable:dwd_200_pangu_atrb_device_online_time}")
	private String deviceOnlineTimetable;

	@Value("${gameOperationRoleInfoTable:game_operation_role_info}")
	private String gameOperationRoleInfoTable;

	@Value("${vGameOperationRoleRollInfoTable:v_game_operation_role_roll_info}")
	private String vGameOperationRoleRollInfoTable;

	@Value("${vGameOperationDeviceRollInfoTable:v_game_operation_device_roll_info}")
	private String vGameOperationDeviceRollInfoTable;

	@Value("${vGameOperationRoleRechargeInfoTable:v_game_operation_role_recharge_info}")
	private String vGameOperationRoleRechargeInfoTable;
	/**
	 * 角色滚服付费视图
	 */
	@Value("${vGameOperationRoleRollRechargeTable:v_game_operation_role_roll_recharge}")
	private String vGameOperationRoleRollRechargeTable;
	/**
	 * 设备滚服付费视图
	 */
	@Value("${vGameOperationDeviceRollRechargeTable:v_game_operation_device_roll_recharge}")
	private String vGameOperationDeviceRollRechargeTable;
	/**
	 *项目id(每日运营【数据中心】对应的项目id)
	 */

	@Value(value = "${dx_app_code:212002}")
	private Integer dxAPPCode;

	/**
	 * 自有人群包下载地址
	 */
	@Value("${self_custom_audience_download_url:http://test.pangu.3367.com/upload/selfCustomAudience/}")
	private String downloadUrl;

	/**
	 * 自有人群包上传地址
	 * /data/wwwroot/ad-platform/upload/selfCustomAudience/
	 */
	@Value("${self_custom_audience_upload_path:/data/wwwroot/ad-platform/upload/selfCustomAudience/}")
	private String uploadPath;

	/**
	 * 每日运营设备注册表
	 */
	@Value(value = "${dailyDataDeviceRegTableData:dwd_daily_operation_device_reg_20220725}")
	private String dailyDataDeviceRegTableData;

	/**
	 * 每日运营用户注册表
	 */
	@Value(value = "${dailyDataAccountRegTableData:dwd_daily_operation_account_reg_20220725}")
	private String dailyDataAccountRegTableData;


	/**
	 * 每日运营用户注册表
	 */
	@Value(value = "${dailyDataRoleRegTable:dwd_daily_operation_create_role_20220725}")
	private String dailyDataRoleRegTable;


	/**
	 * 每日运营活跃用户表
	 */
	@Value(value = "${dailyDataActiveTableData:dws_daily_operation_account_active_recharge_20220725}")
	private String dailyDataActiveTableData;


	/**
	 * 每日运营角色活跃充值表
	 */
	@Value(value = "${dailyDataRoleActiveTable:dws_daily_operation_role_active_recharge_20220725}")
	private String dailyDataRoleActiveTable;




	/**
	 * 每日运营设备活跃充值表
	 */
	@Value(value = "${dailyDataDeviceActiveTable:dws_daily_operation_device_active_recharge_20220818}")
	private String dailyDataDeviceActiveTable;




	/**
	 * 每日运营设备首冲充值表
	 */
	@Value(value = "${dailyDataDeviceFirstTable:dwd_daily_operation_device_first_recharge_20220725}")
	private String dailyDataDeviceFirstTable;




	/**
	 * 每日运营角色首冲表
	 */
	@Value(value = "${dailyDataRoleFirstTable:dwd_daily_operation_role_first_recharge_20220725}")
	private String dailyDataRoleFirstTable;



	/**
	 * 每日运营账号首冲表
	 */
	@Value(value = "${dailyDataAccountFirstTable:dwd_daily_operation_account_first_recharge_20220725}")
	private String dailyDataAccountFirstTable;

	@Value("${export.excel.template.path:/data/wwwroot/3367/3367-mgr-api-v3}")
	private String reportPath;

	/**
	 * clickhouse ip 视图 表
	 */

	@Value(value = "${ipTable:v_dim_ip}")
	private String ipTable;


	/**
	 * clikchouset父子渠道列表视图
	 */
	@Value(value = "${promotionChannelV3Table:dim_200_pangu_mysql_wan_promotion_channel_v3}")
	private String promotionChannelV3Table;


	/**
	 * clikchouset游戏角色视图表
	 */
	@Value(value = "${gameRoleTable:v_odsmysql_game_role}")
	private String gameRoleTable;

	/**
	 * clikchouset游戏表视图
	 */
	@Value(value = "${wanGameTable:dim_200_pangu_mysql_wan_game}")
	private String wanGameTable;


	/**
	 * clikchouset父游戏表视图
	 */
	@Value(value = "${parentGameTable:dim_200_pangu_mysql_parent_game}")
	private String parentGameTable;


	/**
	 * clikchouset广告账号视图表
	 */
	@Value(value = "${adAccountTable:dim_200_pangu_mysql_ad_account}")
	private String adAccountTable;


	/**
	 * clikchouset用户视图表
	 */
	@Value(value = "${sysUserTable:dim_200_pangu_mysql_sys_user}")
	private String sysUserTable;


	/**
	 * 订单充值状态描述
	 */
	@Value(value = "${rechargeStatus0:预订单}")
	private String rechargeStatus0;
	/**
	 * 订单充值状态
	 */
	@Value(value = "${rechargeStatus1:未支付}")
	private String rechargeStatus1;
	/**
	 * 订单充值状态
	 */
	@Value(value = "${rechargeStatus2:支付成功}")
	private String rechargeStatus2;
	/**
	 * 订单充值状态
	 */
	@Value(value = "${rechargeStatus3:支付失败}")
	private String rechargeStatus3;
	/**
	 * 订单充值状态
	 */
	@Value(value = "${rechargeStatus4:异常订单}")
	private String rechargeStatus4;
	/**
	 * 订单充值状态
	 */
	@Value(value = "${rechargeStatus5:退款}")
	private String rechargeStatus5;
	/**
	 * 订单充值状态
	 */
	@Value(value = "${rechargeStatus6:操作中}")
	private String rechargeStatus6;

	/**
	 * 订单充值状态
	 */
	@Value(value = "${rechargeStatus7:取消支付}")
	private String rechargeStatus7;

	/**
	 * 订单充值状态
	 */
	@Value(value = "${rechargeStatus8:支付超时}")
	private String rechargeStatus8;

	@Value(value = "${cpsParentChannelId:0}")
	private Integer cpsParentChannelId;


	@Value(value = "${tenant.cpsTenantId:0}")
	private Integer tenantId;

	@Value(value = "${roleId:30}")
	private Integer roleId;

	@Value("${activity.event.topic.name}")
	private String activityEventTopic;

	@Value(value = "${cpsUserPackTable:v_dim_3399_mysql_cps_user_pack}")
	private String cpsUserPackTable;

	@Value(value = "${cpsBuckleRateTable:v_dim_3399_mysql_cps_buckle_rate}")
	private String cpsBuckleRateTable;

	@Value(value = "${buckleRateCalculationThreshold:1000}")
	private Integer buckleRateCalculationThreshold;

	@Value(value = "${dimDayInfo:dc_dim.dim_day_week}")
	private String dimDayInfo;

	@Value("${weChatGameIds:0}")
	private String weChatGameIds;
	@Value("${raffle.activity.num:8}")
	private Integer raffleActivityNum;

	@Value("${raffle.activity.kvpic:}")
	private String raffleActivityKvpic;

	@Value("${raffle.activity.linkage:}")
	private String raffleActivityLinkage;

	/**
	 * 不能使用hbdxtable 这个属性是临时兼容红包活动数据新建的视图。每次动态扩展的列都需要改视图，故新业务使用原事件表
	 */
	@Value(value = "${dx.event.table:v_event_1433669519229980672}")
	private String dxEventTable;

	@Value("${alipay.withdraw.appid:2021000122613442}")
	private String aliPayWithdrawAppId;


	/**
	 * clickhouse素材标签关联表
	 */
	@Value(value = "${adTagRelateTable:v_odsmysql_ad_tag_relate}")
	private String adTagRelateTable;

	/**
	 * clickhouse渠道用户部门表
	 */
	@Value(value = "${channelUserDeptGroupTable:v_dim_200_pangu_channel_user_dept_group}")
	private String channelUserDeptGroupTable;

	/**
	 * clickhouse dc_dwd/dc_dwd_3399实名信息视图
	 */
	@Value(value = "${wan.user.realname.view:v_odsmysql_wan_user_realname}")
	private String wanUserRealnameView;


	/**
	 * 广电通落地页前缀
	 */
	@Value(value = "${gdtPagePrefix:https://h5.gdt.qq.com/xjviewer/nemo/}")
	private String gdtPagePrefix;



	/**
	 * 时间维度固定为近7日
	 */
	@Value(value = "${future.time:7}")
	private Integer futureTime;
	/**
	 * 消耗满足大于1000且小于10000（不限渠道）
	 */
	@Value(value = "${future.minCost:1000}")
	private String futureMinCost;
	/**
	 * 消耗满足大于1000且小于10000（不限渠道）
	 */
	@Value(value = "${future.maxCost:10000}")
	private String futureMaxCost;

	/**
	 * 成本潜力素材：注册成本低于100
	 */
	@Value(value = "${future.regCost:100}")
	private String futureRegCost;

	/**
	 * 付费潜力素材：首次付费成本低于1000
	 */
	@Value(value = "${future.payCost:1000}")
	private String futurePayCost;

	/**
	 * ROI潜力素材：首日ROI大于5%
	 */
	@Value(value = "${future.firstRoi:5}")
	private String futureFirstRoi;

	/**
	 * 后续潜力素材：累计ROI大于15%
	 */
	@Value(value = "${future.totalRoi:15}")
	private String futureTotalRoi;

	@Value(value = "${platFormCode}")
	private String platFormCode;

}
