package com.dy.yunying.biz.service.manage.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.WanUser;
import com.dy.yunying.biz.dao.manage.WanUserMapper;
import com.dy.yunying.biz.service.manage.WanUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * WanUser
 * @author  zhuxm
 * @version  2022-01-13 16:01:46
 * table: wan_user
 */
@Log4j2
@Service("wanUserService")
@RequiredArgsConstructor
public class WanUserServiceImpl extends ServiceImpl<WanUserMapper, WanUser> implements WanUserService {
}


