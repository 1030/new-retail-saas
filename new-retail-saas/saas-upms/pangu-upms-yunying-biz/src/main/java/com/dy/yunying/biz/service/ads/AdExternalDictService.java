package com.dy.yunying.biz.service.ads;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.AdExternalDict;

/**
 * 转化目标字典表
 * @author  chenxiang
 * @version  2022-07-04 13:58:21
 * table: ad_external_dict
 */
public interface AdExternalDictService extends IService<AdExternalDict> {
}


