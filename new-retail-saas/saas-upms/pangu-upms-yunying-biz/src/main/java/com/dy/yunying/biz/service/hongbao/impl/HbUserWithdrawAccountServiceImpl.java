package com.dy.yunying.biz.service.hongbao.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbUserWithdrawAccount;
import com.dy.yunying.biz.dao.ads.hongbao.HbUserWithdrawAccountMapper;
import com.dy.yunying.biz.service.hongbao.HbUserWithdrawAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service("hbUserWithdrawAccountService")
@RequiredArgsConstructor
public class HbUserWithdrawAccountServiceImpl extends ServiceImpl<HbUserWithdrawAccountMapper, HbUserWithdrawAccount> implements HbUserWithdrawAccountService {
	private final HbUserWithdrawAccountMapper hbUserWithdrawAccountMapper;
}
