package com.dy.yunying.biz.service.cps;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.cps.CpsNodeData;
import com.dy.yunying.api.cps.CpsUserAuthPackReq;
import com.dy.yunying.api.cps.CpsUserPack;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * cps用户分包授权
 * @author sunyq
 */
public interface CpsUserAuthPackService extends IService<CpsUserPack> {
	/**
	 * 分包渠道授权列表
	 * @return
	 */
	List<CpsNodeData> selectChannelTree(Integer userId);

	R saveAuthPack(CpsUserAuthPackReq cpsUserAuthPackReq);
}
