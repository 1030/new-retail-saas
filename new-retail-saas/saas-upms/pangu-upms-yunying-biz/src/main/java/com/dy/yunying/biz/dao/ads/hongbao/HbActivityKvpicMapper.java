package com.dy.yunying.biz.dao.ads.hongbao;

import com.dy.yunying.api.entity.hongbao.HbActivityKvpic;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.vo.HbActivityKvpicVO;
import org.apache.ibatis.annotations.Mapper;

/**
 * kv图库表(hb_activity_kvpic)数据Mapper
 *
 * @author zjz
 * @since 2022-06-18 17:55:04
 * @description 由 zjz 创建
*/
@Mapper
public interface HbActivityKvpicMapper extends BaseMapper<HbActivityKvpic> {

	Page<HbActivityKvpic> selectListBySelective(HbActivityKvpicVO hbActivityKvpicVO);

}
