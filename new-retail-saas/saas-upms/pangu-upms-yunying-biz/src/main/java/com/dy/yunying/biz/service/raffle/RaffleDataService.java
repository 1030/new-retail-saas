package com.dy.yunying.biz.service.raffle;

import com.dy.yunying.api.req.raffle.RaffleDataReq;
import com.dy.yunying.api.resp.raffle.RaffleDataDetail;
import com.dy.yunying.api.resp.raffle.RaffleDataSummary;

import java.util.List;

public interface RaffleDataService {

	List<RaffleDataDetail> page(RaffleDataReq req,Boolean page);

	RaffleDataSummary summary(RaffleDataReq req);

	Long count(RaffleDataReq req);
}
