package com.dy.yunying.biz.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dy.yunying.biz.config.YunYingProperties;
import com.dy.yunying.biz.utils.mongo.MongoDBUtil;

import lombok.extern.slf4j.Slf4j;


/**
 * 游豆明细
 * @author chengj
 *
 */
@Component
@Slf4j
public class UserCurrencyRecordMongo {
	
	@Autowired
	private YunYingProperties yunYingProperties;
	
	/**
	 * 3399 平台币(游豆)收支记录集合名
	 */
	private static final String USER_CURRENCY_RECORD = "user_currency_record";
	
	public String getCollctionName(){
		return MongoDBUtil.getCollectionName(USER_CURRENCY_RECORD, yunYingProperties.getPlatFormCode());
	}
	

	
}
