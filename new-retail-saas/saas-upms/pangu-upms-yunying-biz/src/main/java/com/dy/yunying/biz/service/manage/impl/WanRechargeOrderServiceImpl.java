package com.dy.yunying.biz.service.manage.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.dto.currency.CurrencyOrderDTO;
import com.dy.yunying.api.entity.WanRechargeOrder;
import com.dy.yunying.api.vo.CurrencyOrderVO;
import com.dy.yunying.biz.dao.manage.WanRechargeOrderMapper;
import com.dy.yunying.biz.service.manage.WanRechargeOrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 用户充值记录表
 *
 * @author zhuxm
 * @version 2022-01-13 16:02:25
 * table: wan_recharge_order
 */
@Log4j2
@Service("wanRechargeOrderService")
@RequiredArgsConstructor
public class WanRechargeOrderServiceImpl extends ServiceImpl<WanRechargeOrderMapper, WanRechargeOrder> implements WanRechargeOrderService {

	private final WanRechargeOrderMapper wanRechargeOrderMapper;

	/**
	 * 获取余额订单分页列表
	 *
	 * @param page
	 * @param order
	 * @return
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	@Override
	public Page<CurrencyOrderDTO> getCurrencyOrderPage(Page<Object> page, CurrencyOrderVO order) {
		CurrencyOrderDTO queryBean = new CurrencyOrderDTO()
				.setRechargeStartTime(order.getRechargeStartTime())
				.setRechargeEndTime(order.getRechargeEndTime())
				.setRegisterStartTime(order.getRegisterStartTime())
				.setRegisterEndTime(order.getRegisterEndTime())
				.setUsername(order.getUsername())
				.setIp(order.getIp())
				.setOrderno(order.getOrderno())
				.setTradeno(order.getTradeno())
				.setRechargeStatus(order.getRechargeStatus())
				.setRechargeType(order.getRechargeType())
				.setPgids(order.getPgids())
				.setGameids(order.getGameids());
		page.addOrder(OrderItem.desc("reqtime"));
		return wanRechargeOrderMapper.currencyOrderPage(page, queryBean);
	}

	/**
	 * 获取余额订单列表
	 *
	 * @param order
	 * @return
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	@Override
	public List<CurrencyOrderDTO> getCurrencyOrderList(CurrencyOrderVO order) {
		CurrencyOrderDTO queryBean = new CurrencyOrderDTO()
				.setRechargeStartTime(order.getRechargeStartTime())
				.setRechargeEndTime(order.getRechargeEndTime())
				.setRegisterStartTime(order.getRegisterStartTime())
				.setRegisterEndTime(order.getRegisterEndTime())
				.setUsername(order.getUsername())
				.setIp(order.getIp())
				.setOrderno(order.getOrderno())
				.setTradeno(order.getTradeno())
				.setRechargeStatus(order.getRechargeStatus())
				.setRechargeType(order.getRechargeType())
				.setPgids(order.getPgids())
				.setGameids(order.getGameids());
		return wanRechargeOrderMapper.currencyOrderList(queryBean);
	}

	/**
	 * 汇总数据
	 *
	 * @param order
	 * @return
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	@Override
	public Map<String, Object> getCurrencyOrderTotal(CurrencyOrderVO order) {
		CurrencyOrderDTO queryBean = new CurrencyOrderDTO()
				.setRechargeStartTime(order.getRechargeStartTime())
				.setRechargeEndTime(order.getRechargeEndTime())
				.setRegisterStartTime(order.getRegisterStartTime())
				.setRegisterEndTime(order.getRegisterEndTime())
				.setUsername(order.getUsername())
				.setIp(order.getIp())
				.setOrderno(order.getOrderno())
				.setTradeno(order.getTradeno())
				.setRechargeStatus(order.getRechargeStatus())
				.setRechargeType(order.getRechargeType())
				.setPgids(order.getPgids())
				.setGameids(order.getGameids());
		final Map<String, Object> resultMap = wanRechargeOrderMapper.currencyOrderTotal(queryBean);

		Object totalRechargeAmount = resultMap.get("totalRechargeAmount");
		BigDecimal amount = (null == totalRechargeAmount ? new BigDecimal("0") : (BigDecimal) totalRechargeAmount).setScale(2, BigDecimal.ROUND_HALF_UP);
		resultMap.put("totalRechargeAmount", amount.toString());

		return resultMap;
	}

}


