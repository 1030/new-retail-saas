/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.controller.manage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.AdvertiserDomain;
import com.dy.yunying.api.req.AdvertiserDomainReq;
import com.dy.yunying.biz.service.manage.AdvertiserDomainService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import com.pig4cloud.pig.api.util.StringUtils;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * 广告域名管理
 *
 * @author pigx code generator
 * @date 2021-06-04 15:53:15
 */
@RestController
@AllArgsConstructor
@RequestMapping("/domain" )
@Api(value = "domain", tags = "广告域名管理")
public class AdvertiserDomainController {

    private final AdvertiserDomainService advertiserDomainService;

	/**
	 * 列表-分页
	 * @param req
	 * @return
	 */
	@SysLog("广告域名管理")
	@RequestMapping("/getPage")
    public R getPage(@RequestBody AdvertiserDomainReq req){
		QueryWrapper<AdvertiserDomain> wrapper = new QueryWrapper<>();
		wrapper.notIn("status",2);
		wrapper.orderByDesc("create_time");
		IPage<AdvertiserDomain> iPage = advertiserDomainService.page(req,wrapper);
		return R.ok(iPage);
	}
	/**
	 * 列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/getList")
    public R getList(AdvertiserDomain req){
		QueryWrapper<AdvertiserDomain> wrapper = new QueryWrapper<>();
		wrapper.eq("status",1);
		wrapper.orderByDesc("is_default");
		List<AdvertiserDomain> list = advertiserDomainService.list(wrapper);
		return R.ok(list);
	}
	/**
	 * 添加域名
	 * @param req
	 * @return
	 */
	@SysLog("添加域名")
	@RequestMapping("/add")
    public R add(@RequestBody AdvertiserDomain req){
		if (StringUtils.isBlank(req.getDomainAddr())){
			return R.failed("域名不能为空");
		}
		if (Objects.isNull(req.getStatus())){
			return R.failed("请选择状态");
		}
		// 验证域名是否重复
		R r = checkDomain(req);
		if (r.getCode() != 0){
			return r;
		}
		if (Objects.isNull(req.getIsDefault())){
			req.setIsDefault(0);
		}
		if (1 == req.getIsDefault()){
			QueryWrapper<AdvertiserDomain> wrapper = new QueryWrapper<>();
			AdvertiserDomain domain = new AdvertiserDomain();
			domain.setIsDefault(0);
			advertiserDomainService.update(domain,wrapper);
		}
		req.setCreateBy(SecurityUtils.getUser().getId());
		req.setCreateUser(SecurityUtils.getUser().getUsername());
		req.setCreateTime(new Date());
		req.setUpdateBy(SecurityUtils.getUser().getId());
		req.setUpdateUser(SecurityUtils.getUser().getUsername());
		req.setUpdateTime(new Date());
		boolean flag = advertiserDomainService.save(req);
		if (flag) {
			return R.ok();
		}
		return R.failed();
	}
	/**
	 * 编辑
	 * @param req
	 * @return
	 */
	@SysLog("编辑域名")
	@RequestMapping("/edit")
    public R edit(@RequestBody AdvertiserDomain req){
		if (Objects.isNull(req.getDomainId())){
			return R.failed("主键ID不能为空");
		}
		if (StringUtils.isBlank(req.getDomainAddr())){
			return R.failed("域名不能为空");
		}
		if (Objects.isNull(req.getStatus())){
			return R.failed("请选择状态");
		}
		// 验证域名是否重复
		R r = checkDomain(req);
		if (r.getCode() != 0){
			return r;
		}
		if (Objects.isNull(req.getIsDefault())){
			req.setIsDefault(0);
		}
		if (1 == req.getIsDefault()){
			QueryWrapper<AdvertiserDomain> wrapper = new QueryWrapper<>();
			AdvertiserDomain domain = new AdvertiserDomain();
			domain.setIsDefault(0);
			advertiserDomainService.update(domain,wrapper);
		}
		req.setUpdateBy(SecurityUtils.getUser().getId());
		req.setUpdateUser(SecurityUtils.getUser().getUsername());
		req.setUpdateTime(new Date());
		boolean flag = advertiserDomainService.updateById(req);
		if (flag) {
			return R.ok();
		}
		return R.failed();
	}
	/**
	 * 删除
	 * @param req
	 * @return
	 */
	@SysLog("删除域名")
	@RequestMapping("/del")
    public R del(@RequestBody AdvertiserDomain req){
		if (Objects.isNull(req.getDomainId())){
			return R.failed("主键ID不能为空");
		}
		boolean flag = advertiserDomainService.removeById(req.getDomainId());
		if (flag) {
			return R.ok();
		}
		return R.failed();
	}

	/**
	 * 验证域名是否重复
	 * @param req
	 * @return
	 */
	public R checkDomain(AdvertiserDomain req){
		QueryWrapper<AdvertiserDomain> wrapper = new QueryWrapper<>();
		wrapper.eq("domain_addr",req.getDomainAddr());
		wrapper.notIn("status",2);
		if (Objects.nonNull(req.getDomainId())){
			wrapper.notIn("domain_id",req.getDomainId());
		}
		int count = advertiserDomainService.count(wrapper);
		if (count > 0){
			return R.failed("此域名已存在");
		}
		return R.ok();
	}
}
