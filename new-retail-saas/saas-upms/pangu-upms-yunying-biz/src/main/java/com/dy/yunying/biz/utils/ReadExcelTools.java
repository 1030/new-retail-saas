package com.dy.yunying.biz.utils;

/*import lombok.extern.log4j.Log4j2;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.apache.poi.ss.usermodel.CellType.*;*/

import lombok.extern.log4j.Log4j2;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;

/**
 * @Author: kyf
 * @Date: 2020/06/29 17:45
 */
@Log4j2
public class ReadExcelTools {

	private final static String xls = "xls";
	private final static String xlsx = "xlsx";

	/**
	 * 读入excel文件，解析后返回
	 *
	 * @param file
	 * @param sheetNumInt (<0 小于0，查询全部的sheet页；否则查询对应单个sheet页)
	 * @throws IOException
	 */
	public static List<String[]> readExcel(MultipartFile file, int sheetNumInt) throws IOException {
		// 检查文件
		checkFile(file);
		// 获得Workbook工作薄对象
		Workbook workbook = getWorkBook(file);
		// 创建返回对象，把每行中的值作为一个数组，所有行作为一个集合返回
		List<String[]> list = new ArrayList<>(160);
		if (workbook != null) {
			if (sheetNumInt < 0) {
				for (int sheetNum = 0; sheetNum < workbook.getNumberOfSheets(); sheetNum++) {
					getSheetDataList(workbook, sheetNum, list);
				}
			} else {
				getSheetDataList(workbook, sheetNumInt, list);
			}
		}
		return list;
	}

	private static void getSheetDataList(Workbook workbook, int sheetNum, List<String[]> list) throws IOException {
		// 获得当前sheet工作表
		Sheet sheet = workbook.getSheetAt(sheetNum);
		if (sheet == null) {
			return;
		}
		// 获得当前sheet的开始行
		int firstRowNum = sheet.getFirstRowNum();
		// 获得当前sheet的结束行
		int lastRowNum = sheet.getLastRowNum();

		//获取数据行的开始列和结束列
		Row titleRow = sheet.getRow(1);
		if (titleRow == null) {
			throw new FileNotFoundException("文件为空！");
		}
		int firstCellNum = titleRow.getFirstCellNum();
		int lastCellNum = titleRow.getLastCellNum();// 为空列获取

		// 循环除了第一,二行的所有行
		for (int rowNum = firstRowNum + 1; rowNum <= lastRowNum; rowNum++) { // 为了过滤到第一行因为第一行是数据库的列
			// 获得当前行
			Row row = sheet.getRow(rowNum);
			if (row == null) {
				continue;
			}
			// int lastCellNum = row.getPhysicalNumberOfCells();//为空列不获取
			// String[] cells = new String[row.getPhysicalNumberOfCells()];
			String[] cells = new String[lastCellNum];
			// 循环当前行
			for (int cellNum = firstCellNum; cellNum < lastCellNum; cellNum++) {
				Cell cell = row.getCell(cellNum);
				cells[cellNum] = getCellValue(cell);
			}
			list.add(cells);
		}
	}

	private static void checkFile(MultipartFile file) throws IOException {
		// 判断文件是否存在
		if (null == file) {
			throw new FileNotFoundException("文件不存在！");
		}
		// 获得文件名
		String fileName = file.getOriginalFilename();
		// 判断文件是否是excel文件
		if (!fileName.endsWith(xls) && !fileName.endsWith(xlsx)) {
			throw new IOException(fileName + "不是excel文件");
		}
	}

	private static Workbook getWorkBook(MultipartFile file) {
		// 获得文件名
		String fileName = file.getOriginalFilename();
		// 创建Workbook工作薄对象，表示整个excel
		Workbook workbook = null;
		try {
			// 获取excel文件的io流
			InputStream is = file.getInputStream();
			// 根据文件后缀名不同(xls和xlsx)获得不同的Workbook实现类对象
			if (fileName.endsWith(xls)) {
				// 2003
				workbook = new HSSFWorkbook(is);
			} else if (fileName.endsWith(xlsx)) {
				// 2007
				workbook = new XSSFWorkbook(is);
			}
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return workbook;
	}

	private static String getCellValue(Cell cell) {
		String cellValue = "";
		if (cell == null) {
			return cellValue;
		}
		// 把数字当成String来读，避免出现1读成1.0的情况
		if (cell.getCellType() == NUMERIC) {
			cell.setCellType(STRING);
		}
		// 判断数据的类型
		switch (cell.getCellType()) {
			case NUMERIC: // 数字
				cellValue = String.valueOf(cell.getNumericCellValue());
				break;
			case STRING: // 字符串
				cellValue = String.valueOf(cell.getStringCellValue());
				break;
			case BOOLEAN: // Boolean
				cellValue = String.valueOf(cell.getBooleanCellValue());
				break;
			case FORMULA: // 公式
				// cellValue = String.valueOf(cell.getCellFormula());
				cellValue = String.valueOf(cell.getStringCellValue());
				break;
			case BLANK: // 空值
				cellValue = "";
				break;
			case ERROR: // 故障
				cellValue = "非法字符";
				break;
			default:
				cellValue = "未知类型";
				break;
		}
		return cellValue;
	}
}
