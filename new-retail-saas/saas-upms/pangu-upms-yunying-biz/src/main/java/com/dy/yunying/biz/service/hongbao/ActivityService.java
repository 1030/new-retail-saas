package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbActivity;
import com.dy.yunying.api.entity.hongbao.HbCashLimit;
import com.dy.yunying.api.req.hongbao.*;
import com.dy.yunying.api.resp.hongbao.NodeData;
import com.dy.yunying.api.vo.hongbao.ActivityVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * @author yuwenfeng
 * @description: 红包活动服务
 * @date 2021/10/23 10:47
 */
public interface ActivityService extends IService<HbActivity> {
	Page<ActivityVo> selectActivityPage(Page<HbActivity> page, QueryWrapper<SelectActivityPageReq> query);

	R addActivity(ActivityReq activityReq);

	R editActivity(ActivityReq activityReq);

	R onlineActivity(OnlineActivityReq activityReq);

	R editCashLimit(SetCashLimitReq activityReq);

	List<NodeData> selectGameTree();

	List<NodeData> selectAreaTree();

	List<NodeData> selectChannelTree();

	List<Long> selectActivityByGame(String gameName);

	/**
	 * 统计当前活动的资产
	 */
	void sumActivity(Long activityId);

	void activityFinishJob(String param);

	/**
	 * 抽奖活动下线
	 * @param param
	 */
	void raffleActivityFinishJob(String param);

	/**
	 * 活动过期消息通知
	 *
	 * @param param
	 * @return
	 */
	void activityFinishSendMsgJob(String param);

	R<List<NodeData>> selectAreaTreeByPgameIds(String pGgmeIds);

	R<HbCashLimit> selectCashLimitByAidAndType(SelectCashLimitReq req);

	R copyActivity(CopyActivityReq activityReq);

	/**
	 * 红包活动下拉列表查询
	 *
	 * @param activityReq
	 * @return
	 */
	List<HbActivity> selectActivityList(SelectActivityPageReq activityReq);

    R saveOrUpdateDynamicType(DynamicTypeReq dynamicTypeReq);

	R deleteDynamicType(DynamicTypeReq dynamicTypeReq);

	R editKvpicActivity(KvpicActivityReq kvpicActivityReq);
}
