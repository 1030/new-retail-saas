package com.dy.yunying.biz.controller.raffle;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.dy.yunying.api.entity.raffle.RaffleReceiveRecord;
import com.dy.yunying.api.req.raffle.RaffleReceiveRecordReq;
import com.dy.yunying.api.resp.raffle.RaffleReceiveRecordExportRes;
import com.dy.yunying.biz.service.raffle.RaffleReceiveRecordService;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

/**
 * 领取记录信息表
 * @author  chenxiang
 * @version  2022-11-08 16:01:24
 * table: raffle_receive_record
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/raffleReceiveRecord")
public class RaffleReceiveRecordController {
	
    private final RaffleReceiveRecordService raffleReceiveRecordService;

	/**
	 * 分页
	 * @param record
	 * @return
	 */
	@RequestMapping("/page")
	public R getPage(@RequestBody RaffleReceiveRecordReq record){
		return raffleReceiveRecordService.getPage(record);
	}
	/**
	 * 礼品分页
	 * @param record
	 * @return
	 */
	@RequestMapping("/giftPage")
	public R giftPage(@RequestBody RaffleReceiveRecordReq record){
		record.setGiftType("4");
		return raffleReceiveRecordService.getPage(record);
	}
	/**
	 * 发货
	 * @param record
	 * @return
	 */
	@RequestMapping("/deliverGoods")
	@SysLog("发货")
	@PreAuthorize("@pms.hasPermission('RAFFLE_RECORD_DELIVERGOODS')")
	public R deliverGoods(@RequestBody RaffleReceiveRecordReq record){
		if (StringUtils.isBlank(record.getId())){
			return R.failed("未获取到主键ID");
		}
		if (StringUtils.isBlank(record.getExpressName()) || StringUtils.isBlank(record.getExpressCode())){
			return R.failed("请填写物流名称和订单号");
		}
		if (record.getExpressName().length() > 200 || record.getExpressCode().length() > 200){
			return R.failed("物流名称或订单号过长");
		}
		RaffleReceiveRecord receiveRecord = new RaffleReceiveRecord();
		receiveRecord.setId(Long.valueOf(record.getId()));
		receiveRecord.setExpressName(record.getExpressName());
		receiveRecord.setExpressCode(record.getExpressCode());
		receiveRecord.setRecordStatus(4);
		receiveRecord.setDeliverTime(new Date());
		receiveRecord.setUpdateId(SecurityUtils.getUser().getId().longValue());
		receiveRecord.setUpdateTime(new Date());
		return raffleReceiveRecordService.edit(receiveRecord);
	}
	/**
	 * 驳回
	 * @param record
	 * @return
	 */
	@RequestMapping("/reject")
	@SysLog("驳回")
	@PreAuthorize("@pms.hasPermission('RAFFLE_RECORD_REJECT')")
	public R reject(@RequestBody RaffleReceiveRecordReq record){
		if (StringUtils.isBlank(record.getId())){
			return R.failed("未获取到主键ID");
		}
		if (StringUtils.isBlank(record.getForbidReason())){
			return R.failed("请填写驳回原因");
		}
		if (record.getForbidReason().length() > 500){
			return R.failed("驳回原因过长");
		}
		RaffleReceiveRecord receiveRecord = new RaffleReceiveRecord();
		receiveRecord.setId(Long.valueOf(record.getId()));
		receiveRecord.setForbidReason(record.getForbidReason());
		receiveRecord.setRecordStatus(3);
		receiveRecord.setUpdateId(SecurityUtils.getUser().getId().longValue());
		receiveRecord.setUpdateTime(new Date());
		return raffleReceiveRecordService.edit(receiveRecord);
	}

	/**
	 * 导出
	 * @param record
	 * @param response
	 */
	@RequestMapping("/export")
	@SysLog("抽奖礼品导出")
	@PreAuthorize("@pms.hasPermission('RAFFLE_RECORD_EXPORT')")
	public void export(@RequestBody RaffleReceiveRecordReq record, HttpServletResponse response){
		try {
			List<RaffleReceiveRecordExportRes> list = raffleReceiveRecordService.selectExportList(record);

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setCharacterEncoding("utf-8");
			String fileName = URLEncoder.encode("抽奖礼品领取记录-"+ DateUtils.getCurrentTimeNoUnderline(), "UTF-8").replaceAll("\\+", "%20");
			response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
			response.setHeader("file-name", fileName);

			ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
			WriteSheet writeSheet0 = EasyExcel.writerSheet(0, "抽奖礼品领取记录").head(RaffleReceiveRecordExportRes.class).build();

			excelWriter.write(list, writeSheet0);
			excelWriter.finish();
		} catch (Exception e) {
			log.error(">>>抽奖礼品记录导出异常：{}", e);
		}
	}
}


