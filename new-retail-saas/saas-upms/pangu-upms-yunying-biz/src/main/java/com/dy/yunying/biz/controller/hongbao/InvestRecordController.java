/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.controller.hongbao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dy.yunying.api.constant.Constant;
import com.dy.yunying.api.entity.hongbao.HbInvestRecord;
import com.dy.yunying.api.req.hongbao.ActivityIdReq;
import com.dy.yunying.api.req.hongbao.ApplyInvestReq;
import com.dy.yunying.api.req.hongbao.InvertMoneyReq;
import com.dy.yunying.api.resp.hongbao.AssetsData;
import com.dy.yunying.api.resp.hongbao.InvestRecordVo;
import com.dy.yunying.biz.config.CheckRepeatSubmit;
import com.dy.yunying.biz.service.hongbao.InvestRecordService;
import com.pig4cloud.pig.common.core.mybatis.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 注资记录表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:19
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/investrecord")
@Api(value = "investrecord", tags = "注资记录管理")
@Slf4j
public class InvestRecordController {

	private final InvestRecordService investRecordService;

	@ApiOperation(value = "活动资产总计", notes = "活动资产总计")
	@GetMapping("/selectAssetsCount")
	@PreAuthorize("@pms.hasPermission('hongbao_hbinvestrecord_count')")
	public R<AssetsData> selectAssetsCount(@Valid ActivityIdReq activityReq) {
		try {
			return R.ok(investRecordService.selectAssetsCount(activityReq));
		} catch (Exception e) {
			log.error("注资记录管理-活动资产总计-接口", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "注资记录", notes = "注资记录")
	@GetMapping("/selectInvestRecordPage")
	@PreAuthorize("@pms.hasPermission('hongbao_hbinvestrecord_get')")
	public R<Page<InvestRecordVo>> selectInvestRecordPage(Page<HbInvestRecord> page, @Valid ActivityIdReq activityReq) {
		try {
			QueryWrapper<HbInvestRecord> query = new QueryWrapper<HbInvestRecord>();
			query.eq("hir.activity_id", activityReq.getId());
			query.eq("hir.deleted", Constant.DEL_NO);
			query.orderByDesc("hir.create_time");
			return R.ok(investRecordService.selectInvestRecordPage(page, query));
		} catch (Exception e) {
			log.error("注资记录管理-注资记录-接口", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@CheckRepeatSubmit
	@ApiOperation(value = "新增注资记录", notes = "新增注资记录")
	@SysLog("新增注资记录")
	@PostMapping("addInvestRecord")
	@PreAuthorize("@pms.hasPermission('hongbao_hbinvestrecord_add')")
	public R addInvestRecord(@Valid @RequestBody InvertMoneyReq invertMoneyReq) {
		try {
			return investRecordService.addInvestRecord(invertMoneyReq);
		} catch (Exception e) {
			log.error("注资记录管理-新增注资记录-接口", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@CheckRepeatSubmit
	@ApiOperation(value = "审核注资记录", notes = "审核注资记录")
	@SysLog("审核注资记录")
	@PostMapping("applyInvestRecord")
	@PreAuthorize("@pms.hasPermission('hongbao_hbinvestrecord_apply')")
	public R applyInvestRecord(@Valid @RequestBody ApplyInvestReq applyInvestReq) {
		try {
			return investRecordService.applyInvestRecord(applyInvestReq);
		} catch (Exception e) {
			log.error("注资记录管理-审核注资记录-接口", e);
			return R.failed("接口异常，请联系管理员");
		}
	}
}
