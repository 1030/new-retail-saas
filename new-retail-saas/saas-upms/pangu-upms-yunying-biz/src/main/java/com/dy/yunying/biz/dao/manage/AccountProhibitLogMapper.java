package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.dto.AccountProhibitLogDTO;

public interface AccountProhibitLogMapper extends BaseMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AccountProhibitLogDTO record);

    int insertSelective(AccountProhibitLogDTO record);

	AccountProhibitLogDTO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AccountProhibitLogDTO record);

    int updateByPrimaryKey(AccountProhibitLogDTO record);
}