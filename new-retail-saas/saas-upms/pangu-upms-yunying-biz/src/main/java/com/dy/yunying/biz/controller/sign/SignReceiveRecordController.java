package com.dy.yunying.biz.controller.sign;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yunying.api.dto.sign.SignReceiveRecordDto;
import com.dy.yunying.api.entity.sign.SignReceiveRecord;
import com.dy.yunying.biz.service.sign.SignReceiveRecordService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 奖品领取记录表
 * @author  chengang
 * @version  2021-12-01 10:14:36
 * table: sign_receive_record
 */
@RestController
@RequestMapping("/record")
@RequiredArgsConstructor
public class SignReceiveRecordController {
	
    private final SignReceiveRecordService signReceiveRecordService;

	
	@GetMapping("/list")
	public R getList(Page page, SignReceiveRecordDto dto) {
		LambdaQueryWrapper<SignReceiveRecord> query = new LambdaQueryWrapper<>();
		query.and(wrapper -> wrapper.eq(SignReceiveRecord::getDeleted, 0));
		query.and(dto.getParentGameId() != null,wrapper -> wrapper.eq(SignReceiveRecord::getParentGameId,dto.getParentGameId()));
		query.and(StringUtils.isNotBlank(dto.getUserName()),wrapper -> wrapper.like(SignReceiveRecord::getUserName,dto.getUserName()));
		query.and(StringUtils.isNotBlank(dto.getRoleId()),wrapper -> wrapper.eq(SignReceiveRecord::getRoleId,dto.getRoleId()));
		query.and(StringUtils.isNotBlank(dto.getRoleName()),wrapper -> wrapper.like(SignReceiveRecord::getRoleName,dto.getRoleName()));
		query.and(dto.getBusinessType() != null,wrapper -> wrapper.eq(SignReceiveRecord::getBusinessType,dto.getBusinessType()));
		query.orderByDesc(SignReceiveRecord::getReceiveTime);
		IPage<SignReceiveRecord> listPage = signReceiveRecordService.page(page, query);
		return R.ok(listPage);
	}

}


