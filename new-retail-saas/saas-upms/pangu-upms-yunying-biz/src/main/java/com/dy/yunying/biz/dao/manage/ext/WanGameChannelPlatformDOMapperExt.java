package com.dy.yunying.biz.dao.manage.ext;

import com.dy.yunying.api.entity.WanGameChannelPlatformDO;
import com.dy.yunying.biz.dao.manage.WanGameChannelPlatformDOMapper;
import org.apache.ibatis.annotations.Param;

public interface WanGameChannelPlatformDOMapperExt extends WanGameChannelPlatformDOMapper {

	WanGameChannelPlatformDO getByPlatformAndGameid(@Param("platformid") Integer platformid, @Param("gameid") Integer gameid, @Param("appid") Integer appid);

}