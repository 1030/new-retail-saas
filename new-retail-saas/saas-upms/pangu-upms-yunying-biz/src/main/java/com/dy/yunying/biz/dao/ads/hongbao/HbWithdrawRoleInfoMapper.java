package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbWithdrawRoleInfoDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author MyPC
 * @description 针对表【hb_withdraw_role_info(提现档次与角色关系表)】的数据库操作Mapper
 * @createDate 2022-07-05 17:06:21
 * @Entity generator.domain.HbWithdrawRoleInfo
 */
@Mapper
public interface HbWithdrawRoleInfoMapper extends BaseMapper<HbWithdrawRoleInfoDO> {

	/**
	 * 一次通过提现
	 *
	 * @param record
	 * @return
	 */
	int updatePassedOnceByRoleKey(HbWithdrawRoleInfoDO record);

	/**
	 * 一次拒绝提现
	 *
	 * @param record
	 * @return
	 */
	int updateRejectedOnceByRoleKey(HbWithdrawRoleInfoDO record);

}




