package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbReceivingAddress;

/**
 * 实物奖品领取地址记录
 * @author  chenxiang
 * @version  2021-10-25 14:20:46
 * table: hb_receiving_address
 */
public interface HbReceivingAddressService extends IService<HbReceivingAddress> {
	
}


