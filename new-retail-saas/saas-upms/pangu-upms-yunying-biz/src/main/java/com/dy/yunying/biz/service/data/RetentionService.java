package com.dy.yunying.biz.service.data;

import com.dy.yunying.api.dto.RetentionDto;
import com.pig4cloud.pig.common.core.util.R;


/**
 * @description:
 * @author: nml
 * @time: 2021/6/21 16:56
 **/

public interface RetentionService {

	/*
	* 查询留存数据
	* */
	R selectRetentionData(RetentionDto req);

}
