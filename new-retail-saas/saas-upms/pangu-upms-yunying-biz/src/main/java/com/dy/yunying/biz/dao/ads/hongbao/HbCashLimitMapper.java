/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.dao.ads.hongbao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.hongbao.HbCashLimit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 提现规则限制表
 *
 * @author yuwenfeng
 * @date 2021-10-23 10:31:19
 */
@Mapper
public interface HbCashLimitMapper extends BaseMapper<HbCashLimit> {

	int insertBatch(List<HbCashLimit> cashLimitList);
}
