package com.dy.yunying.biz.service.hbdata;

import com.dy.yunying.api.req.sign.SignActivityDataReq;
import com.pig4cloud.pig.common.core.util.R;

/**
 * @author chenxiang
 * @className SignActiveDataService
 * @date 2021/12/2 20:54
 */
public interface SignActiveDataService {
	/**
	 * 签到活动数据
	 * @param req
	 * @return
	 */
	R dataList(SignActivityDataReq req);
}
