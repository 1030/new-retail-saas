package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbUserWithdrawAccount;

public interface HbUserWithdrawAccountService extends IService<HbUserWithdrawAccount> {
}
