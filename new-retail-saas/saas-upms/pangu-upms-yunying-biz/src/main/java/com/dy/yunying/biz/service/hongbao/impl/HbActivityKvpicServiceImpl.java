package com.dy.yunying.biz.service.hongbao.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbActivityCenterDO;
import com.dy.yunying.api.entity.hongbao.HbActivityKvpic;
import com.dy.yunying.api.vo.HbActivityCenterVO;
import com.dy.yunying.api.vo.HbActivityKvpicVO;
import com.dy.yunying.biz.dao.ads.hongbao.HbActivityKvpicMapper;
import com.dy.yunying.biz.service.hongbao.HbActivityKvpicService;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Objects;

/**
 * kv图库表服务接口实现
 *
 * @author zjz
 * @description 由 zjz 创建
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class HbActivityKvpicServiceImpl  extends ServiceImpl<HbActivityKvpicMapper, HbActivityKvpic> implements HbActivityKvpicService {


	/**
	 * 活动中心分页列表
	 *
	 * @param hbActivityKvpicVO
	 * @return
	 */
	@Override
	public Page<HbActivityKvpic> getKvPage(HbActivityKvpicVO hbActivityKvpicVO) {
		return this.baseMapper.selectListBySelective(hbActivityKvpicVO);
	}

	/**
	 * 保存
	 *
	 * @param hbActivityKvpic
	 */
	@Override
	public void doSave(HbActivityKvpic hbActivityKvpic) {
		final Long userId = Integer.toUnsignedLong(SecurityUtils.getUser().getId());
		hbActivityKvpic.setCreateUser(userId);
		hbActivityKvpic.setUpdateUser(userId);
		hbActivityKvpic.setCreateTime(new Date());
		hbActivityKvpic.setUpdateTime(new Date());
		this.save(hbActivityKvpic);
	}


	/**
	 * 编辑
	 * @param hbActivityKvpic
	 */
	@Transactional(rollbackFor = Exception.class, transactionManager = "adsTransactionManager")
	@Override
	public void doEdit(HbActivityKvpic hbActivityKvpic) {
		final Long userId = Integer.toUnsignedLong(SecurityUtils.getUser().getId());
		hbActivityKvpic.setUpdateUser(userId);
		hbActivityKvpic.setUpdateTime(new Date());
		this.updateById(hbActivityKvpic);
	}

	/**
	 * 删除
	 * @param id
	 */
	@Override
	public void doDelete(Long id) {
		this.update(Wrappers.<HbActivityKvpic>lambdaUpdate().set(HbActivityKvpic::getDeleted, 1).eq(HbActivityKvpic::getId, id));
	}


}