package com.dy.yunying.biz.service.manage;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.dto.currency.CurrencyOrderDTO;
import com.dy.yunying.api.entity.WanRechargeOrder;
import com.dy.yunying.api.vo.CurrencyOrderVO;

import java.util.List;
import java.util.Map;

/**
 * 用户充值记录表
 *
 * @author zhuxm
 * @version 2022-01-13 16:02:25
 * table: wan_recharge_order
 */
public interface WanRechargeOrderService extends IService<WanRechargeOrder> {

	/**
	 * 获取余额订单分页列表
	 *
	 * @param page
	 * @param order
	 * @return
	 */
	Page<CurrencyOrderDTO> getCurrencyOrderPage(Page<Object> page, CurrencyOrderVO order);

	/**
	 * 获取余额订单列表
	 *
	 * @param order
	 * @return
	 */
	List<CurrencyOrderDTO> getCurrencyOrderList(CurrencyOrderVO order);

	/**
	 * 汇总数据
	 *
	 * @param order
	 * @return
	 */
	Map<String, Object> getCurrencyOrderTotal(CurrencyOrderVO order);

}


