package com.dy.yunying.biz.service.cps;

import com.pig4cloud.pig.common.core.util.R;
import com.dy.yunying.api.cps.vo.DataReportVO;
import com.dy.yunying.api.cps.dto.DataReportDto;

import java.util.List;

public interface DataReportService {

	/**
	 * cps数据报表数据总数
	 * @param req
	 * @return
	 */
	R count(DataReportDto req);

	/**
	 * cps数据报表列表
	 *
	 * @param req
	 * @return
	 */
	R<List<DataReportVO>> page(DataReportDto req);

	/**
	 * cps数据报表汇总
	 *
	 * @param req
	 * @return
	 */
	R<List<DataReportVO>> collect(DataReportDto req);

}
