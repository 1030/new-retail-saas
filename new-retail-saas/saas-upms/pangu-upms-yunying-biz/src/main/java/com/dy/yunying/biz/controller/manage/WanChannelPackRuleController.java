package com.dy.yunying.biz.controller.manage;

import com.dy.yunying.biz.service.manage.WanChannelPackRuleService;
import com.dy.yunying.api.dto.BatchSettingRuleDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 分包回传规则表服务控制器
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/packRule")
public class WanChannelPackRuleController {
    private final WanChannelPackRuleService wanChannelPackRuleService;

	@PostMapping("/batchSetting")
	@PreAuthorize("@pms.hasPermission('page_reture_rule')")
	public R batchSetting(@RequestBody BatchSettingRuleDto req) {
		return wanChannelPackRuleService.batchSetting(req);
	}
}