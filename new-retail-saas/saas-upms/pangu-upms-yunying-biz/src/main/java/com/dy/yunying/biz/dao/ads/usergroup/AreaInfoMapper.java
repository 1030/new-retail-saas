package com.dy.yunying.biz.dao.ads.usergroup;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.usergroup.AreaInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AreaInfoMapper extends BaseMapper<AreaInfo> {
}
