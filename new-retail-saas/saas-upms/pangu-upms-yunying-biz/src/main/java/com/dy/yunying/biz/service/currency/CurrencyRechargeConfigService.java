package com.dy.yunying.biz.service.currency;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.CurrencyRechargeConfigDO;
import com.dy.yunying.api.vo.CurrencyRechargeConfigVO;

/**
 * @author MyPC
 * @description 针对表【currency_recharge_config(游豆充值配置表)】的数据库操作Service
 * @createDate 2022-03-28 11:22:30
 */
public interface CurrencyRechargeConfigService extends IService<CurrencyRechargeConfigDO> {

	/**
	 * 分页列表
	 *
	 * @param config
	 * @return
	 */
	Page<CurrencyRechargeConfigDO> list(CurrencyRechargeConfigVO config);

	/**
	 * 保存
	 *
	 * @param config
	 */
	void save(CurrencyRechargeConfigVO config) throws Exception;

	/**
	 * 修改
	 *
	 * @param config
	 */
	void edit(CurrencyRechargeConfigVO config) throws Exception;

	/**
	 * 删除
	 *
	 * @param id
	 */
	void remove(Long id) throws Exception;

}
