package com.dy.yunying.biz.utils;

/**
 * class info
 *
 * @author sunyouquan
 * @date 2022/5/14 18:45
 */
public class StringUtil {
	private static final String NULL_STR = "null";

	public static boolean isNotBlank(String str){
		if (org.apache.commons.lang3.StringUtils.isNotBlank(str) && !NULL_STR.equalsIgnoreCase(str)){
			return true;
		}
		return false;

	}

	public static boolean isBlank(String str){
		if (org.apache.commons.lang3.StringUtils.isBlank(str) || NULL_STR.equalsIgnoreCase(str)){
			return true;
		}
		return false;
	}


}
