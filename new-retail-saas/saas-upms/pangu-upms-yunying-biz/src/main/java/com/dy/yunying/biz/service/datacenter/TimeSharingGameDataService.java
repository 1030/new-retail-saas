package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.TimeSharingGameDTO;
import com.dy.yunying.api.datacenter.vo.TimeSharingGameDataVO;

import java.util.List;

/**
 * @author sunyq
 * @date 2022/8/17 14:58
 */
public interface TimeSharingGameDataService {
	/**
	 * 列表
	 * @param req
	 * @return
	 */
	List<TimeSharingGameDataVO> list(TimeSharingGameDTO req);
	/**
	 * 汇总
	 * @param req
	 * @return
	 */
	TimeSharingGameDataVO collect(TimeSharingGameDTO req);
}
