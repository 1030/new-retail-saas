

package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbActivityRoleInfo;

/**
 * 活动角色信息表
 *
 * @author chengang
 * @version 2021-10-28 12:02:13
 * table: hb_activity_role_info
 */
public interface HbActivityRoleInfoService extends IService<HbActivityRoleInfo> {

	void send(String roleid, byte[] content);
}


