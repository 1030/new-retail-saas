package com.dy.yunying.biz.dao.manage.ext;


import com.dy.yunying.api.entity.TtExtendPackageDO;
import com.dy.yunying.biz.dao.manage.TtExtendPackageMapper;
import com.pig4cloud.pig.api.entity.TtAppManagement;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TtExtendPackageMapperExt extends TtExtendPackageMapper {

	void updateByChannelId(TtExtendPackageDO ttExtendPackageDO);

	TtAppManagement getPackageIdByAppId(@Param("appId") String appid);

	void saveBatchExtendPackage(List<TtExtendPackageDO> extendPackageDOList);

	void saveExtendPackage(TtExtendPackageDO ttExtendPackageDO);

	List<TtExtendPackageDO> queryAdvertiserIdByStatus();

	TtExtendPackageDO getStatusByChannelId(@Param("channelId") String channelId);
}
