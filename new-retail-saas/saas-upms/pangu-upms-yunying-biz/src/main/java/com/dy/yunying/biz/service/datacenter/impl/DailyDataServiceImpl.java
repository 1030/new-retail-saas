package com.dy.yunying.biz.service.datacenter.impl;

import com.dy.yunying.api.datacenter.dto.DailyDataDto;
import com.dy.yunying.api.datacenter.vo.DailyDataVO;
import com.dy.yunying.biz.dao.datacenter.impl.DailyDataDao;
import com.dy.yunying.biz.service.datacenter.DailyDataService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author sunyq
 * @date 2022/6/21 15:37
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DailyDataServiceImpl implements DailyDataService {

	private final DailyDataDao dailyDataDao;

	@Override
	public R<List<DailyDataVO>> page(DailyDataDto req) {
		List<DailyDataVO> list;
		try {
			list = dailyDataDao.list(req);
		} catch (Exception e) {
			log.error("");
			return R.failed("每日运营数据查询失败...");
		}
		return R.ok(list) ;
	}

	@Override
	public R count(DailyDataDto req) {
		return R.ok(dailyDataDao.count(req));
	}

	@Override
	public R<List<DailyDataVO>> collect(DailyDataDto req) {
		//TODO
		Long count = dailyDataDao.count(req);
		if (count==0){
			return R.ok(new ArrayList<>());
		}

		return R.ok(dailyDataDao.collect(req));
	}

}
