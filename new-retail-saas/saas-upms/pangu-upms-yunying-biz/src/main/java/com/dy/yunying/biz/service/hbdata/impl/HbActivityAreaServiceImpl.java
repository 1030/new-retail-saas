package com.dy.yunying.biz.service.hbdata.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.hongbao.HbActivityArea;
import com.dy.yunying.biz.dao.ads.hongbao.HbActivityAreaMapper;
import com.dy.yunying.biz.service.hbdata.HbActivityAreaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 抽奖奖品配置表服务接口实现
 *
 * @author zjz
 * @since 2022-11-07 18:36:03
 * @description 由 zjz 创建
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class HbActivityAreaServiceImpl extends ServiceImpl<HbActivityAreaMapper, HbActivityArea> implements HbActivityAreaService {

	@Override
	public void saveBatchList(List<HbActivityArea> list){
		 this.saveBatch(list,1000);
	}

}