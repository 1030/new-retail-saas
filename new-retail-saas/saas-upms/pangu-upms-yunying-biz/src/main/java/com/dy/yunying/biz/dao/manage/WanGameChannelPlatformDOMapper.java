package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.WanGameChannelPlatformDO;

public interface WanGameChannelPlatformDOMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(WanGameChannelPlatformDO record);

	int insertSelective(WanGameChannelPlatformDO record);

	WanGameChannelPlatformDO selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(WanGameChannelPlatformDO record);

	int updateByPrimaryKey(WanGameChannelPlatformDO record);
}