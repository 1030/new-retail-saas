package com.dy.yunying.biz.service.datacenter;

import com.dy.yunying.api.datacenter.dto.DailyDataDto;
import com.dy.yunying.api.datacenter.vo.DailyDataVO;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * @author sunyq
 * @date 2022/6/21 15:36
 */
public interface DailyDataService {
	/**
	 * 列表数据
	 * @param req
	 * @return
	 */
	R<List<DailyDataVO>> page(DailyDataDto req);

	/**
	 * 总数量
	 * @param req
	 * @return
	 */
	R count(DailyDataDto req);

	/**
	 * 汇总数据
	 * @param req
	 * @return
	 */
	R<List<DailyDataVO>> collect(DailyDataDto req);
}
