package com.dy.yunying.biz.service.ads.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.AdExternalDict;
import com.dy.yunying.biz.dao.ads.AdExternalDictMapper;
import com.dy.yunying.biz.service.ads.AdExternalDictService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 转化目标字典表
 * @author  chenxiang
 * @version  2022-07-04 13:58:21
 * table: ad_external_dict
 */
@Log4j2
@Service("adExternalDictService")
@RequiredArgsConstructor
public class AdExternalDictServiceImpl extends ServiceImpl<AdExternalDictMapper, AdExternalDict> implements AdExternalDictService {
}


