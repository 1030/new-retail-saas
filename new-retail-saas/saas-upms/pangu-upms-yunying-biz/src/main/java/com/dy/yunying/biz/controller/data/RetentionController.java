package com.dy.yunying.biz.controller.data;

import com.dy.yunying.api.constant.RetentionKpiEnum;
import com.dy.yunying.api.dto.RetentionDto;
import com.dy.yunying.biz.service.data.RetentionService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.Objects;

/**
 * @ClassName RetentionController
 * @Description done
 * @Author nieml
 * @Time 2021/6/21 15:53
 * @Version 1.0
 **/

@RestController
@RequestMapping("/retention")
@Slf4j
public class RetentionController {

	@Autowired
	private RetentionService retentionService;

	/**
	 * 留存数据
	 *
	 * @param req
	 * @return
	 */
	@ResponseBody
	@GetMapping(value = "/getRetention")
	public R selectRetentionData(@Valid RetentionDto req) {
		try {
			//校验参数
			Long sTime = req.getSTime();
			Long eTime = req.getETime();
			if (Objects.isNull(sTime) || sTime <= 0) {
				throw new Exception("请选择起始日期");
			}
			if (Objects.isNull(eTime) || eTime <= 0) {
				throw new Exception("请选择结束日期");
			}

			String retentionKpi = req.getRetentionKpi();
			if (StringUtils.isBlank(retentionKpi) || (!RetentionKpiEnum.REG.getType().equals(retentionKpi) && !RetentionKpiEnum.PAY.getType().equals(retentionKpi))) {
				throw new Exception("请选择指标");
			}
			return retentionService.selectRetentionData(req);
		} catch (Exception e) {
			log.error("selectRetentionData:{}",e);
			return R.failed(e.getMessage());
		}
	}

}
