package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.PromotionChannelDO;

public interface PromotionChannelDOMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PromotionChannelDO record);

    int insertSelective(PromotionChannelDO record);

    PromotionChannelDO selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PromotionChannelDO record);

    int updateByPrimaryKey(PromotionChannelDO record);
}