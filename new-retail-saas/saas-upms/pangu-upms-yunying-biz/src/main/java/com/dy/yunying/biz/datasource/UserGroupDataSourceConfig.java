package com.dy.yunying.biz.datasource;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

//数据源2
@Configuration
public class UserGroupDataSourceConfig {

	@Autowired
	private MybatisPlusInterceptor mybatisPlusInterceptor;

	@Bean(name = "userGroupSessionTemplate")
	public JdbcTemplate clickhouseTemplate(
			@Qualifier("userGroupDataSource") DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	// 将这个对象放入Spring容器中
	@Bean(name = "userGroupDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.usergroup")
	public DataSource getDateSource2() {
		return DataSourceBuilder.create().build();
	}

}