package com.dy.yunying.biz.service.yyz;

import com.dy.yunying.api.entity.GameAppointment;
import com.dy.yunying.api.req.yyz.GameAppointmentImportSendReq;
import com.dy.yunying.api.req.yyz.MsgReq;
import com.dy.yunying.api.req.yyz.SendMassageReq;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * @Author: kyf
 * @Date: 2020/12/9 16:46
 * @ClassName SendMassageService
 * @Description 发送消息
 */
public interface SendMassageService {

	/**
	 * 、
	 * 消息发送接口
	 *
	 * @param req
	 * @return
	 */
	R sendSms(MsgReq req);

	/**
	 * 批量发送短信--腾讯
	 * @param list
	 * @param sendType
	 * @return
	 */
	R batchSendMsg(List<GameAppointment> list, Integer sendType);

	/**
	 * 批量发送短信--博士通
	 * @param req
	 * @return
	 */
	R batchSendMsg(SendMassageReq req);

	/***
	 * 导入电话并发出短信
	 * @param req
	 * @return
	 */
	R bathImportSend(GameAppointmentImportSendReq req);

}
