package com.dy.yunying.biz.dao.manage.ext;


import com.dy.yunying.api.cps.CpsNodeData;
import com.dy.yunying.api.resp.hongbao.NodeData;
import com.dy.yunying.api.resp.hongbao.NodeParentData;
import com.dy.yunying.biz.dao.manage.PromotionChannelDOMapper;

import java.util.List;
import java.util.Set;

public interface PromotionChannelDOMapperExt extends PromotionChannelDOMapper {

	List<NodeData> selectMainChannel();

	List<NodeData> selectChannelByPcode(String parentCode);

	List<NodeParentData> selectAllChannel();

	List<CpsNodeData> selectByConditions(Set<String> parentCodeSet);
}