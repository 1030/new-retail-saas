package com.dy.yunying.biz.controller.znfx;

import com.dy.yunying.api.dto.RoiTopTenDto;
import com.dy.yunying.api.req.FutureMaterialReq;
import com.dy.yunying.api.vo.FutureMaterialVo;
import com.dy.yunying.api.vo.RoiTopTenVo;
import com.dy.yunying.biz.service.znfx.TopTenService;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.common.core.mybatis.Page;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: zhuxm
 * @time: 2023/3/21 14:03
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/future")
@Api(value = "create", tags = "创意top10")
@Slf4j
public class FutureMaterialController {

	private final TopTenService topTenService;

	@ApiOperation(value = "图形数据查询", notes = "图形数据查询")
	@PostMapping("/material")
	public R futureMaterial(@RequestBody FutureMaterialReq req) {
		try {
			if (null == req.getFutureType()){
				return R.failed("潜力类型不能为空");
			}
			return topTenService.futureMaterial(req);
		} catch (Exception e) {
			log.error("潜力-接口：{}", e.getMessage());
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "图形数据查询", notes = "图形数据查询")
	@PostMapping("/count")
	public R futureCount(@RequestBody FutureMaterialReq req) {
		if (null == req.getFutureType()){
			return R.failed("潜力类型不能为空");
		}
		return R.ok(topTenService.futureCount(req));
		//return R.ok(1);
	}

}
