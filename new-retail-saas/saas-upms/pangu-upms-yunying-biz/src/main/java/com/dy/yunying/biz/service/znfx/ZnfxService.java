package com.dy.yunying.biz.service.znfx;

import com.dy.yunying.api.entity.znfx.ChartVO;
import com.dy.yunying.api.req.znfx.ZnfxReq;

import java.util.List;

public interface ZnfxService {

	List<ChartVO> selectChartData(ZnfxReq znfxReq);

	/**
	 * 计划用户分析图形数据查询
	 * @param znfxReq
	 * @return
	 */
	List<ChartVO> planUserSearch(ZnfxReq znfxReq);
}
