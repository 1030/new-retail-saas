package com.dy.yunying.biz.dao.manage.ext;


import com.dy.yunying.api.req.WanCmsConfigReq;
import com.dy.yunying.biz.dao.manage.WanCmsConfigDOMapper;

public interface WanCmsConfigDOMapperExt extends WanCmsConfigDOMapper {
	/**
	 * 插入cms配置
	 *
	 * @return
	 */
	int insertWanCmsConfig(WanCmsConfigReq req);

}