package com.dy.yunying.biz.service.manage.impl;

import com.dy.yunying.api.dto.AccountProhibitLogDTO;
import com.dy.yunying.biz.dao.manage.AccountProhibitLogMapper;
import com.dy.yunying.biz.service.manage.ProhibitLogService;
import com.dy.yunying.biz.utils.DateUtils;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 游戏版本
 *
 * @Author: leisw
 * @Date: 2020/7/21 13:52
 */
@Transactional
@Slf4j
@Service
public class ProhibitLogServiceImpl implements ProhibitLogService {

    @Autowired
    private AccountProhibitLogMapper accountProhibitLogMapper;

	@Override
	public AccountProhibitLogDTO save(AccountProhibitLogDTO record) {
		Long userId = SecurityUtils.getUser().getId().longValue();
		Date now = DateUtils.getCurrentDate();
		record.setCreator(userId);
		record.setCreateTime(now);

		record.setId(null);
		accountProhibitLogMapper.insertSelective(record);
		return record;
	}
}