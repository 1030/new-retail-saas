/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.service.hongbao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.hongbao.HbActivityNotice;
import com.dy.yunying.api.req.hongbao.ActivityNoticeReq;
import com.dy.yunying.api.req.hongbao.OnlineActivityNoticeReq;
import com.pig4cloud.pig.common.core.util.R;

/**
 * 活动公告表
 *
 * @author yuwenfeng
 * @date 2021-11-03 15:26:05
 */
public interface ActivityNoticeService extends IService<HbActivityNotice> {

	R addNotice(ActivityNoticeReq activityNotice);

	R editNotice(ActivityNoticeReq activityNotice);

	R onlineNotice(OnlineActivityNoticeReq onlineActivityNoticeReq);
}
