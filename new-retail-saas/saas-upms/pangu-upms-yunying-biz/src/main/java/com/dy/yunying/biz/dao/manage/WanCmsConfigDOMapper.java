package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.WanCmsConfigDO;

public interface WanCmsConfigDOMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(WanCmsConfigDO record);

	int insertSelective(WanCmsConfigDO record);

	WanCmsConfigDO selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(WanCmsConfigDO record);

	int updateByPrimaryKey(WanCmsConfigDO record);
}