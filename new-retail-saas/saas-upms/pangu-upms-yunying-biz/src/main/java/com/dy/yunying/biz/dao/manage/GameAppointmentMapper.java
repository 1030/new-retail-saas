package com.dy.yunying.biz.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.entity.GameAppointment;
import com.dy.yunying.api.entity.GameAppointmentScheduleGearsPage;
import com.dy.yunying.api.req.yyz.*;
import com.dy.yunying.api.resp.yyz.GameAppointmentPageRes;
import com.pig4cloud.pig.common.core.mybatis.Page;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 游戏预约表
 *
 * @author kongyanfang
 * @version 2022-01-17 17:14:29
 * table: game_appointment
 */
@Mapper
public interface GameAppointmentMapper extends BaseMapper<GameAppointment> {

	Integer selectTotal();

	Long selectAllTotal(GameAppointmentReq req);

	List<SendTemplateCodeReq> selectTemplateList();

	List<GameCodeReq> selectGameList();

	String getGameName(String type);

	IPage<GameAppointmentPageRes> selectGameAppointmentPage(GameAppointmentQueryReq req);

	List<GameAppointmentQueryList> selectGameAppointmentList(GameAppointmentQueryReqList req);

	Long selectRuleAllTotal(GameAppointmentReq req);

}


