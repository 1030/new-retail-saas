package com.dy.yunying.biz.service.sign.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.sign.SignReceiveRecord;
import com.dy.yunying.biz.dao.ads.sign.SignReceiveRecordMapper;
import com.dy.yunying.biz.service.sign.SignReceiveRecordService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 奖品领取记录表
 * @author  chengang
 * @version  2021-12-01 10:14:36
 * table: sign_receive_record
 */
@Log4j2
@Service("signReceiveRecordService")
@RequiredArgsConstructor
public class SignReceiveRecordServiceImpl extends ServiceImpl<SignReceiveRecordMapper, SignReceiveRecord> implements SignReceiveRecordService {
	
	

	
	
}


