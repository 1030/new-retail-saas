

package com.dy.yunying.biz.service.usergroup;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.usergroup.UserGroupData;
import com.dy.yunying.api.req.usergroup.UserGroupDataReq;
import com.dy.yunying.api.vo.usergroup.UserGroupDataVo;
import com.dy.yunying.api.vo.usergroup.UserGroupVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;


public interface UserGroupDataService extends IService<UserGroupData> {


	R saveOrUpdateUserGroupData(UserGroupDataVo vo) throws Throwable;

	R uploadUserGroupData(UserGroupDataReq req) throws Exception;

	R delete(Long id);

	R batchDelete(List<String> list);
}


