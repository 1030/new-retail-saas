package com.dy.yunying.biz.dao.manage;

import com.dy.yunying.api.entity.ParentGameVersionDO;

public interface ParentGameVersionDOMapper {
	int deleteByPrimaryKey(Long versionId);

	int insert(ParentGameVersionDO record);

	int insertSelective(ParentGameVersionDO record);

	ParentGameVersionDO selectByPrimaryKey(Long versionId);

	int updateByPrimaryKeySelective(ParentGameVersionDO record);

	int updateByPrimaryKey(ParentGameVersionDO record);
}