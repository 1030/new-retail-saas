/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.dy.yunying.biz.service.sign;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.sign.SignActivity;
import com.dy.yunying.api.req.hongbao.OnlineStatusReq;
import com.dy.yunying.api.req.hongbao.SelectSignActivityPageReq;
import com.dy.yunying.api.req.sign.SignActivityReq;
import com.dy.yunying.api.resp.hongbao.NodeData;
import com.dy.yunying.api.resp.hongbao.SignActivityVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * 签到活动表
 *
 * @author yuwenfeng
 * @date 2021-11-30 19:46:19
 */
public interface SignActivityService extends IService<SignActivity> {

	Page<SignActivityVo> selectSignActivityPage(Page<SignActivity> page, QueryWrapper<SignActivity> query);

	R addSignActivity(SignActivityReq signActivityReq);

	R editSignActivity(SignActivityReq signActivityReq);

	R onlineSignActivity(OnlineStatusReq req);

	List<NodeData> selectAreaListByPgameIds(String pGgmeIds);

	void activityFinishJob(String param);

	/**
	 * 签到活动下拉列表查询
	 *
	 * @param activityReq
	 * @return
	 */
	List<SignActivity> selectActivityList(SelectSignActivityPageReq activityReq);

	R copySignActivity(SignActivityReq signActivityReq);
}
