package com.dy.yunying.biz.controller.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.yunying.api.dto.AdmonitorRuleDto;
import com.dy.yunying.api.entity.AdmonitorRule;
import com.dy.yunying.biz.service.ads.AdmonitorRuleService;
import com.pig4cloud.pig.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @ClassName AdmonitorController
 * @Description todo
 * @Author yangyh
 * @Time 2021/6/18 17:17
 * @Version 1.0
 **/
@Slf4j
@RestController
@RequestMapping("/admonitor")
@Api(value = "admonitor", tags = "监控管理模块")
public class AdmonitorController {

	@Autowired
	private AdmonitorRuleService admonitorRuleService;

	@PostMapping(value = "/saveAdmonitorRule")
	public R<Boolean> saveAdmonitorRule(@RequestBody @Valid AdmonitorRule admonitorRule) {
		return admonitorRuleService.saveAdmonitorRule(admonitorRule);
	}

	@PostMapping(value = "/updateAdmonitorRule")
	public R<Boolean> updateAdmonitorRule(@RequestBody @Valid AdmonitorRule admonitorRule) {
		return admonitorRuleService.updateAdmonitorRule(admonitorRule);
	}

	@PostMapping(value = "/updateAdmonitorRuleStatus")
	public R<Boolean> updateAdmonitorRuleStatus(@RequestParam(name = "id") Integer id,@RequestParam(name = "monitorStatus")  Integer monitorStatus) {
		return admonitorRuleService.updateAdmonitorRuleStatus(id,monitorStatus);
	}

	@PostMapping(value = "/deleteAdmonitorRule")
	public R<Boolean> deleteAdmonitorRule(@RequestParam(name = "id") Integer id) {
		return admonitorRuleService.deleteAdmonitorRule(id);
	}

	@GetMapping(value = "/getAdmonitorRuleById")
	public R<AdmonitorRule> getAdmonitorRuleById(Integer id) {
		return admonitorRuleService.getAdmonitorRuleById(id);
	}

	@GetMapping(value = "/queryAdmonitorRules")
	public R<IPage<AdmonitorRule>> queryAdmonitorRules(AdmonitorRuleDto admonitorRuleDto) {
		return admonitorRuleService.queryAdmonitorRules(admonitorRuleDto);
	}


}
