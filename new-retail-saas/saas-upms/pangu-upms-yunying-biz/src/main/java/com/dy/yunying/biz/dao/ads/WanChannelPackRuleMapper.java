package com.dy.yunying.biz.dao.ads;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.yunying.api.entity.WanChannelPackRule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 分包回传规则表(wan_channel_pack_rule)数据Mapper
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
*/
@Mapper
public interface WanChannelPackRuleMapper extends BaseMapper<WanChannelPackRule> {

	List<WanChannelPackRule> selectAllListByCodes(@Param("codes") List<String> codes);

	int updateByCodes(@Param("list") List<String> list, @Param("backhaulType") Integer backhaulType);

}
