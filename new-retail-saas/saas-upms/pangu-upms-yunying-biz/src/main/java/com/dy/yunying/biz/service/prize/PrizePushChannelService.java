package com.dy.yunying.biz.service.prize;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.prize.PrizePushChannel;

/**
 * 奖励与渠道关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:48
 * table: prize_push_channel
 */
public interface PrizePushChannelService extends IService<PrizePushChannel> {
}


