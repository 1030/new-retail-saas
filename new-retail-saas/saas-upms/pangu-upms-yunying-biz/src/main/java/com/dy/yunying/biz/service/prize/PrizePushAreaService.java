package com.dy.yunying.biz.service.prize;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yunying.api.entity.prize.PrizePushArea;

/**
 * 奖励与区服关系表
 * @author  chenxiang
 * @version  2022-04-26 10:20:36
 * table: prize_push_area
 */
public interface PrizePushAreaService extends IService<PrizePushArea> {
}


