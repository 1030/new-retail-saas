package com.dy.yunying.biz.service.manage.impl;
import java.util.*;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.yunying.api.entity.WanChannelPackRule;
import com.dy.yunying.biz.dao.ads.WanChannelPackRuleMapper;
import com.dy.yunying.biz.dao.manage.WanChannelPackDOMapper;
import com.dy.yunying.biz.dao.manage.ext.WanChannelPackDOMapperExt;
import com.dy.yunying.biz.service.manage.WanChannelPackRuleService;
import com.dy.yunying.api.dto.BatchSettingRuleDto;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

/**
 * 分包回传规则表服务接口实现
 *
 * @author zjz
 * @since 2023-02-23
 * @description 由 zjz 创建
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class WanChannelPackRuleServiceImpl extends ServiceImpl<WanChannelPackRuleMapper, WanChannelPackRule> implements WanChannelPackRuleService {
    private final WanChannelPackRuleMapper wanChannelPackRuleMapper;
	private final WanChannelPackDOMapperExt wanChannelPackMapper;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R batchSetting(BatchSettingRuleDto req){
		if(req.getBackhaulType()==null){
			R.failed("回传类型不能为空");
		}
		if(StringUtils.isBlank(req.getCodes())){
			R.failed("分包ID集合不能为空");
		}
		if(req.getBackhaulType()==1 && CollectionUtils.isEmpty(req.getPackRuleList())){
			R.failed("分包回传规则不能为空");
		}
		List<String> codeList = Arrays.stream(req.getCodes().split(",")).collect(Collectors.toList());
		if(req.getBackhaulType()==0){
			wanChannelPackMapper.updateByCode(codeList, req.getBackhaulType());
			wanChannelPackRuleMapper.updateByCodes(codeList,req.getBackhaulType());
			return R.ok();
		}
		for (WanChannelPackRule packRule : req.getPackRuleList()) {
			if(packRule.getEventType()==null){
				return R.failed("事件类型不能为空");
			}
			if(packRule.getTimeScope()==null){
				return R.failed("时间范围不能为空");
			}
			if(packRule.getTimeScope()==1&&packRule.getTimeValue()==null){
				return R.failed("时间范围为大于N天时，N值不能为空");
			}
			if(packRule.getEventType()==3&&packRule.getBackhaulRule()==null){
				return R.failed("事件类型为付费时，回传频次类型不能为空");
			}
			if(packRule.getEventType()==3&&packRule.getBackhaulRule()==1&&packRule.getBackhaulFrequency()==null){
				return R.failed("限制回传频次时，回传频次不能为空");
			}
		}

		List<WanChannelPackRule> oldrule =  wanChannelPackRuleMapper.selectAllListByCodes(codeList);
		Map<String,Long> oldMap = oldrule.stream().collect(Collectors.toMap(a -> a.getPackCode()+"-*-*-"+a.getEventType(), WanChannelPackRule::getId));
		Date now = new Date();
		Long userid = SecurityUtils.getUser().getId().longValue();
		List<WanChannelPackRule> savepack = new ArrayList<>();
		List<Long> upadteIds = new ArrayList<>();
		req.getPackRuleList().forEach(c->{
			Arrays.stream(req.getCodes().split(",")).distinct().forEach(a -> {
				WanChannelPackRule pack =  new WanChannelPackRule();
				pack.setPackCode(a);
				pack.setEventType(c.getEventType());
				pack.setTimeScope(c.getTimeScope());
				pack.setTimeValue(c.getTimeValue());
				pack.setBackhaulRule(c.getBackhaulRule());
				pack.setBackhaulFrequency(c.getBackhaulFrequency());
				pack.setBackhaulType(req.getBackhaulType());
				String mapkey = a+"-*-*-"+c.getEventType();
				if(oldMap.containsKey(mapkey)){
					pack.setId(oldMap.get(mapkey));
					upadteIds.add(oldMap.get(mapkey));
				}else {
					pack.setCreateTime(now);
					pack.setCreateId(userid);
				}
				pack.setDeleted(0);
				pack.setUpdateTime(now);
				pack.setUpdateId(userid);
				savepack.add(pack);
			});
		});
		//将之前这次没有更新的数据 设置删除
		List<WanChannelPackRule> delList = oldrule.stream().filter(a -> !upadteIds.contains(a.getId()) && a.getDeleted()==0).collect(Collectors.toList());
		delList.forEach(a -> a.setDeleted(1));
		savepack.addAll(delList);
		this.saveOrUpdateBatch(savepack);
		wanChannelPackMapper.updateByCode(codeList, req.getBackhaulType());
		return R.ok();
	}

}