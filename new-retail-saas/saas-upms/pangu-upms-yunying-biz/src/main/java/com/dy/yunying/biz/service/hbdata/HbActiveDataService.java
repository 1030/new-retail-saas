package com.dy.yunying.biz.service.hbdata;

import com.dy.yunying.api.req.hongbao.HbActiveDataReq;
import com.pig4cloud.pig.common.core.util.R;

public interface HbActiveDataService {
	R popupList(HbActiveDataReq req);

	R floatClick(HbActiveDataReq req);

	R activeTypePU(HbActiveDataReq req);

	R activeCash(HbActiveDataReq req);

	R activeRule(HbActiveDataReq req);

	R activeFinish(HbActiveDataReq req);

	R activeCashConfig(HbActiveDataReq req);

	R activeHbDraw(HbActiveDataReq req);

	R activeHbProfit(HbActiveDataReq req);

    R messageList(HbActiveDataReq req);

	R invitePvList(HbActiveDataReq req);

	R invitedList(HbActiveDataReq req);

	R activityTaskDetail(HbActiveDataReq req);

	R activityVisitDetail(HbActiveDataReq req);
}
