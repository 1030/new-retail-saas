package com.pig4cloud.pig.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.admin.api.entity.SysTenant;
import com.pig4cloud.pig.admin.api.vo.SysTenantVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName SysTenantMapper.java
 * @createTime 2021年09月15日 15:11:00
 */
@Mapper
public interface SysTenantMapper extends BaseMapper<SysTenant> {
	/**
	 * 租户列表
	 * @param req
	 * @return
	 */
	List<SysTenant> selectTenantList(SysTenantVo req);
}
