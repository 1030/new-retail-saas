package com.pig4cloud.pig.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.api.client.util.Lists;
import com.pig4cloud.pig.admin.api.entity.*;
import com.pig4cloud.pig.admin.api.vo.SysTenantVo;
import com.pig4cloud.pig.admin.mapper.SysTenantMapper;
import com.pig4cloud.pig.admin.service.*;
import com.pig4cloud.pig.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName SysTenantServiceImpl.java
 * @createTime 2021年09月15日 15:09:00
 */
@Service
@AllArgsConstructor
public class SysTenantServiceImpl extends ServiceImpl<SysTenantMapper, SysTenant> implements SysTenantService {

	private final SysTenantMapper sysTenantMapper;

	private final SysRoleService sysRoleService;

	private final SysMenuService sysMenuService;

	private final SysRoleMenuService sysRoleMenuService;

	private final SysUserTenantService sysUserTenantService;

	private final SysUserRoleService sysUserRoleService;

	/**
	 * 租户列表
	 * @return
	 * @param req
	 */
	@Override
	public List<SysTenant> getList(SysTenantVo req){
		return sysTenantMapper.selectTenantList(req);
	}
	/**
	 * 添加租户
	 * @param req
	 * @return
	 */
	@Override
	public R addTenant(SysTenantVo req){
		try {
			SysTenant sysTenant = new SysTenant();
			sysTenant.setName(req.getName());
			sysTenant.setIndexUrl(req.getIndexUrl());
			sysTenant.setStartTime(new Date());
			sysTenant.setEndTime(new Date());
			sysTenant.setCreateTime(new Date());
			sysTenant.setUpdateTime(new Date());
			// 新增租户
			sysTenantMapper.insert(sysTenant);
			// 添加默认角色，默认菜单
			addRoleAndMenu(sysTenant);
			return R.ok();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return R.failed();
	}
	public void addRoleAndMenu(SysTenant sysTenant){
		// 角色菜单权限
		List<SysRoleMenu> sysRoleMenuList = Lists.newArrayList();

		SysRole sysRole = new SysRole();
		sysRole.setRoleName(sysTenant.getName() + "管理员");
		sysRole.setRoleCode("ROLE_ADMIN_" + sysTenant.getId());
		sysRole.setRoleDesc("管理员");
		sysRole.setTenantId(sysTenant.getId());
		// 添加默认角色
		sysRoleService.save(sysRole);

		// 添加默认菜单 - 权限管理
		SysMenu sysMenuOne = new SysMenu();
		sysMenuOne.setName("权限管理");
		sysMenuOne.setPath("/user");
		sysMenuOne.setParentId(-1);
		sysMenuOne.setIcon("icon-yanzhengma");
		sysMenuOne.setType("0");
		sysMenuOne.setSort(1);
		sysMenuOne.setTenantId(sysTenant.getId());
		sysMenuService.save(sysMenuOne);


		// 添加默认菜单 - 系统管理
		SysMenu sysMenuSys = new SysMenu();
		sysMenuSys.setName("系统管理");
		sysMenuSys.setPath("/admin");
		sysMenuSys.setParentId(-1);
		sysMenuSys.setIcon("icon-add");
		sysMenuSys.setType("0");
		sysMenuSys.setSort(2);
		sysMenuSys.setTenantId(sysTenant.getId());
		sysMenuService.save(sysMenuSys);

		//插入 1:日志管理，2:字典管理，3:文件管理
		for (int i = 1; i<=3; i++) {
			SysMenu menu = new SysMenu();
			String name = "日志管理";
			String path = "/admin/log/index";
			if (i == 2) {
				name = "字典管理";
				path = "/admin/dict/index";
			} else if (i ==3) {
				name = "文件管理";
				path = "/admin/file/index";
			}
			menu.setName(name);
			menu.setPath(path);
			menu.setParentId(sysMenuSys.getMenuId());
			menu.setIcon("");
			menu.setType("0");
			menu.setTenantId(sysTenant.getId());
			sysMenuService.save(menu);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), menu.getMenuId());

			if (i == 1) {
				SysMenu subMenu = new SysMenu();
				subMenu.setName("日志删除");
				subMenu.setPermission("sys_log_del");
				subMenu.setParentId(menu.getMenuId());
				subMenu.setType("1");
				subMenu.setSort(i);
				subMenu.setTenantId(sysTenant.getId());
				sysMenuService.save(subMenu);
				addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), subMenu.getMenuId());
			} else if (i ==2) {
				// j  1:字典删除 2:字典新增  3:字典修改
				for (int j = 1; j<=3; j++) {
					SysMenu subMenu = new SysMenu();
					String subName = "字典删除";
					String permission = "sys_dict_del";
					if (i == 2) {
						subName = "字典新增";
						permission = "sys_dict_add";
					} else if (i ==3) {
						subName = "字典修改";
						permission = "sys_dict_edit";
					}
					subMenu.setName(subName);
					subMenu.setPermission(permission);
					subMenu.setParentId(menu.getMenuId());
					subMenu.setType("1");
					subMenu.setSort(i);
					subMenu.setTenantId(sysTenant.getId());
					sysMenuService.save(subMenu);
					addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), subMenu.getMenuId());
				}

			} else if (i ==3) {
				SysMenu subMenu = new SysMenu();
				subMenu.setName("删除文件");
				subMenu.setPermission("sys_file_del");
				subMenu.setParentId(menu.getMenuId());
				subMenu.setType("1");
				subMenu.setSort(i);
				subMenu.setTenantId(sysTenant.getId());
				sysMenuService.save(subMenu);
				addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), subMenu.getMenuId());
			}
		}


		addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuOne.getMenuId());
		addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuSys.getMenuId());

		if (true){
			SysMenu sysMenuTwo = new SysMenu();
			sysMenuTwo.setName("用户管理");
			sysMenuTwo.setPath("/admin/user/index");
			sysMenuTwo.setParentId(sysMenuOne.getMenuId());
			sysMenuTwo.setIcon("icon-yonghuguanli");
			sysMenuTwo.setType("0");
			sysMenuTwo.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuTwo);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuTwo.getMenuId());

			SysMenu sysMenuAdd = new SysMenu();
			sysMenuAdd.setName("用户新增");
			sysMenuAdd.setPermission("sys_user_add");
			sysMenuAdd.setParentId(sysMenuTwo.getMenuId());
			sysMenuAdd.setType("1");
			sysMenuAdd.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuAdd);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuAdd.getMenuId());

			SysMenu sysMenuEdit = new SysMenu();
			sysMenuEdit.setName("用户修改");
			sysMenuEdit.setPermission("sys_user_edit");
			sysMenuEdit.setParentId(sysMenuTwo.getMenuId());
			sysMenuEdit.setType("1");
			sysMenuEdit.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuEdit);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuEdit.getMenuId());

			SysMenu sysMenuDel = new SysMenu();
			sysMenuDel.setName("用户删除");
			sysMenuDel.setPermission("sys_user_del");
			sysMenuDel.setParentId(sysMenuTwo.getMenuId());
			sysMenuDel.setType("1");
			sysMenuDel.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuDel);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuDel.getMenuId());
		}
		if (true){
			SysMenu sysMenuTwo = new SysMenu();
			sysMenuTwo.setName("菜单管理");
			sysMenuTwo.setPath("/admin/menu/index");
			sysMenuTwo.setParentId(sysMenuOne.getMenuId());
			sysMenuTwo.setIcon("icon-caidanguanli");
			sysMenuTwo.setType("0");
			sysMenuTwo.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuTwo);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuTwo.getMenuId());

			SysMenu sysMenuAdd = new SysMenu();
			sysMenuAdd.setName("菜单新增");
			sysMenuAdd.setPermission("sys_menu_add");
			sysMenuAdd.setParentId(sysMenuTwo.getMenuId());
			sysMenuAdd.setType("1");
			sysMenuAdd.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuAdd);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuAdd.getMenuId());

			SysMenu sysMenuEdit = new SysMenu();
			sysMenuEdit.setName("菜单修改");
			sysMenuEdit.setPermission("sys_menu_edit");
			sysMenuEdit.setParentId(sysMenuTwo.getMenuId());
			sysMenuEdit.setType("1");
			sysMenuEdit.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuEdit);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuEdit.getMenuId());

			SysMenu sysMenuDel = new SysMenu();
			sysMenuDel.setName("菜单删除");
			sysMenuDel.setPermission("sys_menu_del");
			sysMenuDel.setParentId(sysMenuTwo.getMenuId());
			sysMenuDel.setType("1");
			sysMenuDel.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuDel);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuDel.getMenuId());
		}
		if (true){
			SysMenu sysMenuTwo = new SysMenu();
			sysMenuTwo.setName("角色管理");
			sysMenuTwo.setPath("/admin/role/index");
			sysMenuTwo.setParentId(sysMenuOne.getMenuId());
			sysMenuTwo.setIcon("icon-jiaoseguanli");
			sysMenuTwo.setType("0");
			sysMenuTwo.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuTwo);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuTwo.getMenuId());

			SysMenu sysMenuAdd = new SysMenu();
			sysMenuAdd.setName("角色新增");
			sysMenuAdd.setPermission("sys_role_add");
			sysMenuAdd.setParentId(sysMenuTwo.getMenuId());
			sysMenuAdd.setType("1");
			sysMenuAdd.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuAdd);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuAdd.getMenuId());

			SysMenu sysMenuEdit = new SysMenu();
			sysMenuEdit.setName("角色修改");
			sysMenuEdit.setPermission("sys_role_edit");
			sysMenuEdit.setParentId(sysMenuTwo.getMenuId());
			sysMenuEdit.setType("1");
			sysMenuEdit.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuEdit);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuEdit.getMenuId());

			SysMenu sysMenuDel = new SysMenu();
			sysMenuDel.setName("角色删除");
			sysMenuDel.setPermission("sys_role_del");
			sysMenuDel.setParentId(sysMenuTwo.getMenuId());
			sysMenuDel.setType("1");
			sysMenuDel.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuDel);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuDel.getMenuId());

			SysMenu sysMenuPerm = new SysMenu();
			sysMenuPerm.setName("分配权限");
			sysMenuPerm.setPermission("sys_role_perm");
			sysMenuPerm.setParentId(sysMenuTwo.getMenuId());
			sysMenuPerm.setType("1");
			sysMenuPerm.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuPerm);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuPerm.getMenuId());
		}
		if (true){
			SysMenu sysMenuTwo = new SysMenu();
			sysMenuTwo.setName("部门管理");
			sysMenuTwo.setPath("/admin/dept/index");
			sysMenuTwo.setParentId(sysMenuOne.getMenuId());
			sysMenuTwo.setIcon("icon-web-icon-");
			sysMenuTwo.setType("0");
			sysMenuTwo.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuTwo);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuTwo.getMenuId());

			SysMenu sysMenuAdd = new SysMenu();
			sysMenuAdd.setName("部门新增");
			sysMenuAdd.setPermission("sys_dept_add");
			sysMenuAdd.setParentId(sysMenuTwo.getMenuId());
			sysMenuAdd.setType("1");
			sysMenuAdd.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuAdd);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuAdd.getMenuId());

			SysMenu sysMenuEdit = new SysMenu();
			sysMenuEdit.setName("部门修改");
			sysMenuEdit.setPermission("sys_dept_edit");
			sysMenuEdit.setParentId(sysMenuTwo.getMenuId());
			sysMenuEdit.setType("1");
			sysMenuEdit.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuEdit);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuEdit.getMenuId());

			SysMenu sysMenuDel = new SysMenu();
			sysMenuDel.setName("部门删除");
			sysMenuDel.setPermission("sys_dept_del");
			sysMenuDel.setParentId(sysMenuTwo.getMenuId());
			sysMenuDel.setType("1");
			sysMenuDel.setTenantId(sysTenant.getId());
			sysMenuService.save(sysMenuDel);
			addSysRoleMenu(sysRoleMenuList, sysRole.getRoleId(), sysMenuDel.getMenuId());
		}
		// 角色增加菜单权限
		sysRoleMenuService.saveBatch(sysRoleMenuList);
		// 给管理员添加登陆权限
		SysUserTenant sysUserTenant = new SysUserTenant();
		sysUserTenant.setUserId(1);
		sysUserTenant.setTenantId(sysTenant.getId());
		sysUserTenant.setStatus(1);
		sysUserTenantService.save(sysUserTenant);
		// 给管理员绑定角色关系
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRole.setUserId(1);
		sysUserRole.setRoleId(sysRole.getRoleId());
		sysUserRole.setTenantId(sysTenant.getId());
		sysUserRoleService.save(sysUserRole);
	}

	/**
	 * 角色增加菜单权限
	 * @param sysRoleMenuList
	 * @param roleId
	 * @param menuId
	 */
	public void addSysRoleMenu(List<SysRoleMenu> sysRoleMenuList, Integer roleId, Integer menuId){
		SysRoleMenu sysRoleMenu = new SysRoleMenu();
		sysRoleMenu.setRoleId(roleId);
		sysRoleMenu.setMenuId(menuId);
		sysRoleMenuList.add(sysRoleMenu);
	}
}
