package com.pig4cloud.pig.admin.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;


/**
 * @Description
 * @Author chengang
 * @Date 2021/12/27
 */
@RefreshScope
@Configuration
@Data
public class AdminProperties {

	@Value(value = "${refresh.demo:defaultValue}")
	private String refreshDemo;

	@Value(value = "${tenant.cpsTenantId:19}")
	private Integer cpsTenantId;


}
