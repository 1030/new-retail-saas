/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.admin.controller;

import com.pig4cloud.pig.admin.api.dto.DeptTree;
import com.pig4cloud.pig.admin.api.entity.SysDept;
import com.pig4cloud.pig.admin.service.SysDeptService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门管理 前端控制器
 * </p>
 *
 * @author lengleng
 * @since 2018-01-20
 */
@RestController
@AllArgsConstructor
@RequestMapping({"/dept", "/cpsRole/dept"})
@Api(value = "dept", tags = "部门管理模块")
public class SysDeptController {
	private final SysDeptService sysDeptService;

	/**
	 * 通过ID查询
	 *
	 * @param id ID
	 * @return SysDept
	 */
	@GetMapping("/{id}")
	public R getById(@PathVariable Integer id) {
		return R.ok(sysDeptService.getById(id));
	}


	/**
	 * 返回树形菜单集合
	 *
	 * @return 树形菜单
	 */
	@GetMapping(value = "/tree")
	public R getTree() {
		return R.ok(sysDeptService.selectTree());
	}

	/**
	 * 添加
	 *
	 * @param sysDept 实体
	 * @return success/false
	 */
	@SysLog("添加部门")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('sys_dept_add')")
	public R save(@Valid @RequestBody SysDept sysDept) throws Exception {
		return R.ok(sysDeptService.saveDept(sysDept));
	}

	/**
	 * 删除
	 *
	 * @param id ID
	 * @return success/false
	 */
	@SysLog("删除部门")
	@DeleteMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('sys_dept_del')")
	public R removeById(@PathVariable Integer id) {

		return sysDeptService.removeDeptById1(id);
//		return R.ok(sysDeptService.removeDeptById(id));
	}

	/**
	 * 编辑
	 *
	 * @param sysDept 实体
	 * @return success/false
	 */
	@SysLog("编辑部门")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('sys_dept_edit')")
	public R update(@Valid @RequestBody SysDept sysDept) throws Exception {
		sysDept.setUpdateTime(LocalDateTime.now());
		return R.ok(sysDeptService.updateDeptById(sysDept));
	}


	/**
	 *查询用户隶属部门信息，返回树形部门集合
	 * @param params 部门查询参数
	 * @return
	 */
	@Inner
	@SysLog("查询用户隶属部门信息，返回树形部门集合")
	@RequestMapping(value = "/getUserDeptTree")
	public R<List<DeptTree>>  getUserDeptTree(@RequestParam Map<String, Object> params) {
		String deptName=null;
		String dept=null;
		Object deptArr=params.get("deptArr");
		if(null == deptArr){
         	return R.failed("当前用户没有部门权限");
		 }
		dept=deptArr.toString();
		deptName=params.get("deptName")!= null ?params.get("deptName").toString():null;
	    List<DeptTree> deptTreeList=sysDeptService.getUserDeptTree(deptName,dept);
		return R.ok(deptTreeList);
	}



	/**
	 *查询用户隶属部门信息，返回树形部门集合
	 * @param params 部门查询参数
	 * @return
	 */
	@Inner
	@SysLog("查询用户隶属部门信息，返回树形部门集合")
	@RequestMapping(value = "/getUserDeptList")
	public R<List<SysDept>>  getUserDeptList(@RequestParam Map<String, Object> params) {
		String deptName=null;
		String dept=null;
		Object deptArr=params.get("deptArr");
		if(null == deptArr){
			return R.failed("当前用户没有部门权限");
		}
		dept=deptArr.toString();
		deptName=params.get("deptName")!= null ?params.get("deptName").toString():null;
		return R.ok(sysDeptService.getUserDeptList(deptName,dept));
	}

}
