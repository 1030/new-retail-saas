package com.pig4cloud.pig.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.admin.api.entity.SysTenant;
import com.pig4cloud.pig.admin.api.vo.SysTenantVo;
import com.pig4cloud.pig.common.core.util.R;

import java.util.List;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName SysTenantService.java
 * @createTime 2021年09月15日 15:09:00
 */
public interface SysTenantService extends IService<SysTenant> {
	/**
	 * 租户列表
	 * @param req
	 * @return
	 */
	List<SysTenant> getList(SysTenantVo req);
	/**
	 * 添加租户
	 * @param req
	 * @return
	 */
	R addTenant(SysTenantVo req);
}
