package com.pig4cloud.pig.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.admin.api.entity.SysFsUser;
import com.pig4cloud.pig.admin.api.vo.SysFsUserVO;

import java.util.List;
import java.util.Map;

/**
 * @Description: hma
 * @Author: hma
 * @Date:   2023-03-17
 * @Version: V1.0
 */
public interface SysFsUserService extends IService<SysFsUser> {

	/**
	 * 查询所有的飞书用户
	 * @return
	 */
	List<Map<String,Object>> getFsUserList();

}
