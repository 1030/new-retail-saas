/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.admin.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.admin.api.entity.SysDict;
import com.pig4cloud.pig.admin.api.entity.SysDictItem;
import com.pig4cloud.pig.admin.api.vo.SysDictItemAll;
import com.pig4cloud.pig.admin.service.SysDictItemService;
import com.pig4cloud.pig.admin.service.SysDictService;
import com.pig4cloud.pig.common.core.constant.CacheConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author lengleng
 * @since 2019-03-19
 */
@RestController
@AllArgsConstructor
@RequestMapping({"/dict", "/cpsRole/dict"})
@Api(value = "dict", tags = "字典管理模块")
public class SysDictController {
	private final SysDictService sysDictService;
	private final SysDictItemService sysDictItemService;

	/**
	 * 通过ID查询字典信息
	 *
	 * @param id ID
	 * @return 字典信息
	 */
	@GetMapping("/{id}")
	public R getById(@PathVariable Integer id) {
		return R.ok(sysDictService.getById(id));
	}

	/**
	 * 分页查询字典信息
	 *
	 * @param page 分页对象
	 * @return 分页对象
	 */
	@GetMapping("/page")
	public R<IPage> getDictPage(Page page, SysDict sysDict) {
		return R.ok(sysDictService.page(page, Wrappers.query(sysDict)));
	}

	/**
	 * 通过字典类型查找字典
	 *
	 * @param type 类型
	 * @return 同类型字典
	 */
	@GetMapping("/type/{type}")
	@Cacheable(value = CacheConstants.DICT_DETAILS, key = "#type", unless = "#result == null")
	public R getDictByType(@PathVariable String type) {
		return R.ok(sysDictItemService.list(Wrappers
				.<SysDictItem>query().lambda()
				.eq(SysDictItem::getType, type)));
	}

	/**
	 * 通过字典类型查找字典--内部调用-避免前端修改不使用 @inner
	 *
	 * @param type 类型
	 * @return 同类型字典
	 */
	@Inner
	@GetMapping("/typeInner/{type}")
	@Cacheable(value = CacheConstants.DICT_DETAILS, key = "#type", unless = "#result == null")
	public R getDictByType4Inner(@PathVariable String type) {
		return R.ok(sysDictItemService.list(Wrappers
				.<SysDictItem>query().lambda()
				.eq(SysDictItem::getType, type)));
	}

	/**
	 * 添加字典
	 *
	 * @param sysDict 字典信息
	 * @return success、false
	 */
	@SysLog("添加字典")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('sys_dict_add')")
	public R save(@Valid @RequestBody SysDict sysDict) {
		return R.ok(sysDictService.save(sysDict));
	}

	/**
	 * 删除字典，并且清除字典缓存
	 *
	 * @param id ID
	 * @return R
	 */
	@SysLog("删除字典")
	@DeleteMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('sys_dict_del')")
	public R removeById(@PathVariable Integer id) {
		return sysDictService.removeDict(id);
	}

	/**
	 * 修改字典
	 *
	 * @param sysDict 字典信息
	 * @return success/false
	 */
	@PutMapping
	@SysLog("修改字典")
	@PreAuthorize("@pms.hasPermission('sys_dict_edit')")
	public R updateById(@Valid @RequestBody SysDict sysDict) {
		return sysDictService.updateDict(sysDict);
	}

	/**
	 * 分页查询
	 *
	 * @param page        分页对象
	 * @param sysDictItem 字典项
	 * @return
	 */
	@GetMapping("/item/page")
	public R getSysDictItemPage(Page page, SysDictItem sysDictItem) {
		return R.ok(sysDictItemService.page(page, Wrappers.query(sysDictItem)));
	}


	/**
	 * 通过id查询字典项
	 *
	 * @param id id
	 * @return R
	 */
	@GetMapping("/item/{id}")
	public R getDictItemById(@PathVariable("id") Integer id) {
		return R.ok(sysDictItemService.getById(id));
	}

	/**
	 * 新增字典项
	 *
	 * @param sysDictItem 字典项
	 * @return R
	 */
	@SysLog("新增字典项")
	@PostMapping("/item")
	@CacheEvict(value = CacheConstants.DICT_DETAILS, allEntries = true)
	public R save(@RequestBody SysDictItem sysDictItem) {
		return R.ok(sysDictItemService.save(sysDictItem));
	}

	/**
	 * 修改字典项
	 *
	 * @param sysDictItem 字典项
	 * @return R
	 */
	@SysLog("修改字典项")
	@PutMapping("/item")
	public R updateById(@RequestBody SysDictItem sysDictItem) {
		return sysDictItemService.updateDictItem(sysDictItem);
	}

	/**
	 * 通过id删除字典项
	 *
	 * @param id id
	 * @return R
	 */
	@SysLog("删除字典项")
	@DeleteMapping("/item/{id}")
	public R removeDictItemById(@PathVariable Integer id) {
		return sysDictItemService.removeDictItem(id);
	}



	/**
	 * 查询所有数据字典
	 */
	@GetMapping("/all")
	public R getAll(SysDictItem sysDictItem) {
		Map<String, List<SysDictItemAll>> voMap  = new HashMap<>();
		List<SysDict> list = sysDictService.list();


		List<SysDictItem> itemList = sysDictItemService.list();

		list.forEach(dict -> {
			String type = dict.getType();

			List<SysDictItemAll> newItemList = new ArrayList<>();
			itemList.forEach( item -> {
				if(type.equals(item.getType())) {
					SysDictItemAll sysDictItemAll = new SysDictItemAll();
					BeanUtils.copyProperties(item, sysDictItemAll);
					newItemList.add(sysDictItemAll);
				}
			});

			//按照序号升序排序
			Comparator<SysDictItemAll> bySort = Comparator.comparing(SysDictItemAll::getSort);
			//按照id升序排序
			Comparator<SysDictItemAll> byId = Comparator.comparing(SysDictItemAll::getId);




			//将list先按照"序号"升序再按照"id"升序排列
			newItemList.sort(bySort.thenComparing(byId));

			voMap.put(type, newItemList);
		});
		return R.ok(voMap);
	}



	/**
	 * 根据条件查询字典详情数据
	 */
	@GetMapping("/getDictItemList")
	public R<List<SysDictItem>> getDictItemList(SysDictItem sysDictItem) {
		QueryWrapper queryWrapper=new QueryWrapper(sysDictItem);
		List<SysDictItem> selectList =sysDictItemService.getBaseMapper().selectList(queryWrapper);
		return R.ok(selectList);
	}



	/**
	 * 查询行业分类
	 * @return
	 */
	@GetMapping("/getIndustryClassify")
	public R getIndustryClassify() {
		return sysDictItemService.getIndustryClassify();
	}
}
