package com.pig4cloud.pig.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.admin.api.entity.SysHostTenant;
import com.pig4cloud.pig.admin.mapper.SysHostTenantMapper;
import com.pig4cloud.pig.admin.service.SysHostTenantService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class SysHostTenantServiceImpl extends ServiceImpl<SysHostTenantMapper, SysHostTenant> implements SysHostTenantService {

	private final SysHostTenantMapper sysHostTenantMapper;
}
