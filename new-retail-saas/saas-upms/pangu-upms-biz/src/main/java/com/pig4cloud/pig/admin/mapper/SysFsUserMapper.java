package com.pig4cloud.pig.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.admin.api.entity.SysFsUser;

/**
 * @Description: 系统飞书关联表
 * @Author: hma
 * @Date:   2023-03-17
 * @Version: V1.0
 */
public interface SysFsUserMapper extends BaseMapper<SysFsUser> {

}
