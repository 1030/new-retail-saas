/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.admin.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.common.collect.Lists;
import com.pig4cloud.pig.admin.api.entity.DmSearchTmplate;
import com.pig4cloud.pig.admin.api.entity.SysTabCol;
import com.pig4cloud.pig.admin.api.entity.TemplateInfo;
import com.pig4cloud.pig.admin.api.vo.*;
import com.pig4cloud.pig.admin.service.DmSearchTmplateService;
import com.pig4cloud.pig.admin.service.SysTabColService;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.constant.enums.PlanAttrStatTypeEnum;
import com.pig4cloud.pig.common.core.constant.enums.PlatformTypeEnum;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StopWatch;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhuxm
 * @date 2021/02/26
 */
@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping({"/tabcol", "/cpsRole/tabcol"})
@Api(value = "tabcol", tags = "动态表头模块")
public class TabColController {
	@Value("${spring.profiles.active:''}")
	private String ENV;
	private final SysTabColService sysTabColService;

	private final StringRedisTemplate stringRedisTemplate;
	private final static String APLAN_ATTR = "APLAN_ATTR";
	private final static String APLAN_COMMON = "APLAN_COMMON";
	private final static String EMPTY_STR = "";

	private final DmSearchTmplateService dmSearchTmplateService;

	/**
	 * 返回当前用户的树形菜单集合
	 *
	 * @param alias 表头别名
	 * @return 当前用户的树形菜单
	 */
	@GetMapping("/{alias}")
	public R getUserTabCol(@PathVariable String alias, String sourceName, String queryColumn, Integer operate) {

		// 1 operate  查询表头  2  查询自定义列
		// 获取符合条件的表头
		SysTabCol sysTabCol = sysTabColService.getOne(Wrappers.<SysTabCol>query().lambda().eq(SysTabCol::getAlias, alias)
				.and(wq -> wq.eq(SysTabCol::getEnv, ENV).or().eq(SysTabCol::getEnv, EMPTY_STR).or().isNull(SysTabCol::getEnv))
				.orderByDesc(SysTabCol::getEnv)
				.last("limit 1"));
		if (sysTabCol == null) {
			return R.failed("表头信息为空");
		}
		Integer userId = SecurityUtils.getUser().getId();
		//获取用户隐藏的列
		List<String> hideColList = new ArrayList<>();
		String hideCols = stringRedisTemplate.opsForValue().get(CommonConstants.AD_HIDE_COLS_ + alias + "_" + userId);
		if (StringUtils.isNotBlank(hideCols)) {
			hideColList.addAll(Arrays.asList(StringUtils.split(hideCols, ",")));
		}

		String content = sysTabCol.getContent();
		if (StringUtils.isBlank(content)) {
			R.failed("表头信息内容为空");
		}

		List<SysTabColField> fieldList = JSONObject.parseArray(content, SysTabColField.class);
		if (StringUtils.isNotBlank(sourceName)) {
			fieldList = fieldList.stream()
					.filter(field -> Boolean.FALSE ? field.getAttrs().getString("label").equals(sourceName) :
							field.getAttrs().getString("label").contains(sourceName)).collect(Collectors.toList());
		}
		for (SysTabColField tabColField : fieldList) {
			if (null == operate || 2 != operate) {
				// 选择渠道类型显示渠道code和名称
				if (StringUtils.isNotBlank(queryColumn) && queryColumn.indexOf("parentchl") >= 0) {
					queryColumn += ",parentchlname,parentchlName";
				}
				//查询自定义列不受动态表头的影响
				//特殊处理动态展示列
				switch (alias) {
					case "planattr":
						break;
					default:
						extracted(queryColumn, tabColField);
				}

			}

			if (CollectionUtils.isNotEmpty(hideColList)) {
				//某些列是否需要影藏
				if (hideColList.contains(tabColField.getAttrs().getString("prop"))) {
					tabColField.getAttrs().put("hidden", true);
				}
			}

		}

		return R.ok(fieldList);
	}

	private void extracted(String queryColumn, SysTabColField tabColField) {
		//动态表头默认不展示，但点击选中后方可中展示
		if (Boolean.TRUE == tabColField.getAttrs().getBoolean("dynamic")) {
			//处理动态表头隐藏
			tabColField.getAttrs().put("hidden", true);

		}
		if (StringUtils.isNotBlank(queryColumn)) {
			//需要动态展示某些列，将库里面的数据设置为不隐藏【当类别只选择广告计划的时候 列表显示广告计划对应的属性，当列表多选包含广告计划，或者不包含 都不显示广告计划对应的列】
			List<String> showColumn = Arrays.asList(queryColumn.split(","));
			if (showColumn.size() == 1 && showColumn.get(0).equals("adid")) {
				if (showColumn.contains(tabColField.getAttrs().getString("prop")) || showColumn.contains(tabColField.getAttrs().getString("opt")) || StringUtils.equals(tabColField.getAttrs().getString("showAd"), "adid")) {
					tabColField.getAttrs().put("hidden", false);
				}
				//处理固定
				if (StringUtils.equals(tabColField.getAttrs().getString("showFixed"), "adid")){
					tabColField.getAttrs().put("fixed", false);
				}
			} else {
				if (showColumn.contains(tabColField.getAttrs().getString("prop")) || showColumn.contains(tabColField.getAttrs().getString("opt"))) {
					if (!StringUtils.equals(tabColField.getAttrs().getString("prop"), "adid") && !StringUtils.equals(tabColField.getAttrs().getString("opt"), "adid")) {
						//对应的广告列不显示
						tabColField.getAttrs().put("hidden", false);
					}
					//针对选中多列，当其中包含广告计划的时候，需要将广告计划id,广告计划名称显示出来
					if (showColumn.contains("adid")) {
						if (StringUtils.equals(tabColField.getAttrs().getString("show"), "adid")) {
							tabColField.getAttrs().put("hidden", false);
						}
					}

				}
			}
		}
	}

	/**
	 * 计划属性分析表pc端表头
	 *
	 * @param queryColumn
	 * @param ctype
	 * @param sourceName
	 * @return
	 */
	@RequestMapping("/planattr")
	public R getPlanAttrStatUserTabColPc(@RequestParam String queryColumn, @RequestParam String ctype, String sourceName) {

		// 1 operate  查询表头  2  查询自定义列
		// 获取符合条件的表头

		SysTabCol sysTabCol = sysTabColService.getOne(Wrappers.<SysTabCol>query().lambda().eq(SysTabCol::getAlias,
				PlanAttrStatTypeEnum.ADID.V().equals(queryColumn) && (PlatformTypeEnum.TT.getValue().equals(ctype) || PlatformTypeEnum.GDT.getValue().equals(ctype)) ? String.format("%s_%s", APLAN_ATTR, ctype) : APLAN_ATTR)
				.and(wq -> wq.eq(SysTabCol::getEnv, ENV).or().eq(SysTabCol::getEnv, EMPTY_STR).or().isNull(SysTabCol::getEnv))
				.orderByDesc(SysTabCol::getEnv)
				.last("limit 1"));
		if (sysTabCol == null) {
			return R.failed("表头信息为空");
		}

		return getPlanAttrStatUserTabCol(queryColumn, ctype, sourceName, sysTabCol);
	}

	/**
	 *
	 * @param queryColumn
	 * @param ctype
	 * @param sourceName
	 * @return
	 */
	@RequestMapping("/planattr_h5")
	public R getPlanAttrStatUserTabColH5(@RequestParam String queryColumn, @RequestParam String ctype, String sourceName) {

		// 1 operate  查询表头  2  查询自定义列
		// 获取符合条件的表头

		SysTabCol sysTabCol = sysTabColService.getOne(Wrappers.<SysTabCol>query().lambda().eq(SysTabCol::getAlias,
				PlanAttrStatTypeEnum.ADID.V().equals(queryColumn) && (PlatformTypeEnum.TT.getValue().equals(ctype) || PlatformTypeEnum.GDT.getValue().equals(ctype)) ? String.format("%s_h5_%s", APLAN_ATTR, ctype) : APLAN_ATTR)
				.and(wq -> wq.eq(SysTabCol::getEnv, ENV).or().eq(SysTabCol::getEnv, EMPTY_STR).or().isNull(SysTabCol::getEnv))
				.orderByDesc(SysTabCol::getEnv)
				.last("limit 1"));
		if (sysTabCol == null) {
			return R.failed("表头信息为空");
		}

		return getPlanAttrStatUserTabCol(queryColumn, ctype, sourceName, sysTabCol);
	}

	private R getPlanAttrStatUserTabCol(String queryColumn, String ctype, String sourceName, SysTabCol sysTabCol) {

		Integer userId = SecurityUtils.getUser().getId();
		//获取用户隐藏的列
		List<String> hideColList = new ArrayList<>();
		String hideCols = stringRedisTemplate.opsForValue().get(String.format("%s%s_%s_%s_%s", CommonConstants.AD_HIDE_COLS_, APLAN_ATTR, queryColumn, ctype, userId));
		if (StringUtils.isNotBlank(hideCols)) {
			hideColList.addAll(Arrays.asList(StringUtils.split(hideCols, ",")));
		}

		String content = sysTabCol.getContent();
		if (StringUtils.isBlank(content)) {
			R.failed("表头信息内容为空");
		}
		Map<String, Object> planAttr = JSONObject.parseObject(content);
		Map<String, List<Object>> resultTmp = new HashMap<>();
		if (StringUtils.isNotBlank(sourceName)) {
			JSONArray attrArr = (JSONArray) planAttr.get(APLAN_COMMON);
			attrArr = Objects.isNull(attrArr) ? new JSONArray() : attrArr;
			List<Object> filterCommonAttr = attrArr.stream().filter(v -> ((String) ((JSONObject) ((JSONObject) v).get("attrs")).get("label")).contains(sourceName)).collect(Collectors.toList());
			resultTmp.put(APLAN_COMMON, filterCommonAttr);
			if (PlanAttrStatTypeEnum.ADID.V().equals(queryColumn)) {
				attrArr = (JSONArray) planAttr.get(ctype);
				attrArr = Objects.isNull(attrArr) ? new JSONArray() : attrArr;
				List<Object> filterCtypeAttr = attrArr.stream().filter(v ->
						((String) ((JSONObject) ((JSONObject) v).get("attrs")).get("label")).contains(sourceName)).collect(Collectors.toList());
				resultTmp.put(ctype, filterCtypeAttr);
			}
		} else {
			JSONArray attrArr = (JSONArray) planAttr.get(APLAN_COMMON);
			attrArr = Objects.isNull(attrArr) ? new JSONArray() : attrArr;
			resultTmp.put(APLAN_COMMON, (attrArr.stream().collect(Collectors.toList())));
			if (PlanAttrStatTypeEnum.ADID.V().equals(queryColumn)) {
				attrArr = (JSONArray) planAttr.get(ctype);
				attrArr = Objects.isNull(attrArr) ? new JSONArray() : attrArr;
				resultTmp.put(ctype, attrArr.stream().collect(Collectors.toList()));
			}

		}
		List<Object> tmp = Lists.newArrayList();
		tmp.addAll(resultTmp.get(APLAN_COMMON));
		if (PlanAttrStatTypeEnum.ADID.V().equals(queryColumn)) {
			tmp.addAll(resultTmp.get(ctype));
		}
		tmp.stream().forEach(
				v -> {
					JSONObject jo = (JSONObject) ((JSONObject) v).get("attrs");
					jo.put("hidden", hideColList.contains(jo.get("prop")));
				}
		);
		return R.ok(resultTmp);
	}

	/**
	 * 返回当前用户的树形菜单集合
	 *
	 * @param ctype 渠道类别
	 * @return 当前用户的树形菜单
	 */
	/*@RequestMapping("/planattr")
	public R getPlanAttrStatUserTabCol(@RequestParam String queryColumn, @RequestParam String ctype, String sourceName, Integer operate) {

		// 1 operate  查询表头  2  查询自定义列
		// 获取符合条件的表头

		SysTabCol sysTabCol = sysTabColService.getOne(Wrappers.<SysTabCol>query().lambda().eq(SysTabCol::getAlias,
				PlanAttrStatTypeEnum.ADID.V().equals(queryColumn) && (PlatformTypeEnum.TT.getValue().equals(ctype) || PlatformTypeEnum.GDT.getValue().equals(ctype)) ? String.format("%s_%s", APLAN_ATTR, ctype) : APLAN_ATTR)
				.and(wq -> wq.eq(SysTabCol::getEnv, ENV).or().eq(SysTabCol::getEnv, EMPTY_STR).or().isNull(SysTabCol::getEnv))
				.orderByDesc(SysTabCol::getEnv)
				.last("limit 1"));
		if (sysTabCol == null) {
			return R.failed("表头信息为空");
		}
		Integer userId = SecurityUtils.getUser().getId();
		//获取用户隐藏的列
		List<String> hideColList = new ArrayList<>();
		String hideCols = stringRedisTemplate.opsForValue().get(String.format("%s%s_%s_%s_%s", CommonConstants.AD_HIDE_COLS_, APLAN_ATTR, queryColumn, ctype, userId));
		if (StringUtils.isNotBlank(hideCols)) {
			hideColList.addAll(Arrays.asList(StringUtils.split(hideCols, ",")));
		}

		String content = sysTabCol.getContent();
		if (StringUtils.isBlank(content)) {
			R.failed("表头信息内容为空");
		}
		Map<String, Object> planAttr = JSONObject.parseObject(content);
		Map<String, List<Object>> resultTmp = new HashMap<>();
		if (StringUtils.isNotBlank(sourceName)) {
			JSONArray attrArr = (JSONArray) planAttr.get(APLAN_COMMON);
			attrArr = Objects.isNull(attrArr) ? new JSONArray() : attrArr;
			List<Object> filterCommonAttr = attrArr.stream().filter(v -> ((String) ((JSONObject) ((JSONObject) v).get("attrs")).get("label")).contains(sourceName)).collect(Collectors.toList());
			resultTmp.put(APLAN_COMMON, filterCommonAttr);
			if (PlanAttrStatTypeEnum.ADID.V().equals(queryColumn)) {
				attrArr = (JSONArray) planAttr.get(ctype);
				attrArr = Objects.isNull(attrArr) ? new JSONArray() : attrArr;
				List<Object> filterCtypeAttr = attrArr.stream().filter(v ->
						((String) ((JSONObject) ((JSONObject) v).get("attrs")).get("label")).contains(sourceName)).collect(Collectors.toList());
				resultTmp.put(ctype, filterCtypeAttr);
			}
		} else {
			JSONArray attrArr = (JSONArray) planAttr.get(APLAN_COMMON);
			attrArr = Objects.isNull(attrArr) ? new JSONArray() : attrArr;
			resultTmp.put(APLAN_COMMON, (attrArr.stream().collect(Collectors.toList())));
			if (PlanAttrStatTypeEnum.ADID.V().equals(queryColumn)) {
				attrArr = (JSONArray) planAttr.get(ctype);
				attrArr = Objects.isNull(attrArr) ? new JSONArray() : attrArr;
				resultTmp.put(ctype, attrArr.stream().collect(Collectors.toList()));
			}

		}
		List<Object> tmp = Lists.newArrayList();
		tmp.addAll(resultTmp.get(APLAN_COMMON));
		if (PlanAttrStatTypeEnum.ADID.V().equals(queryColumn)) {
			tmp.addAll(resultTmp.get(ctype));
		}
		tmp.stream().forEach(
				v -> {
					JSONObject jo = (JSONObject) ((JSONObject) v).get("attrs");
					jo.put("hidden", hideColList.contains(jo.get("prop")));
				}
		);
		return R.ok(resultTmp);
	}*/

	/**
	 * 计划属性分析表pc端自定义列保存
	 *
	 * @param hideColsVO
	 * @return
	 */
	@RequestMapping(value = "/savePlanAttrHideCols")
	public R savePlanAttrHideCols(@Validated @RequestBody HideColsPlanAttrVO hideColsVO) {
		if (StringUtils.isBlank(hideColsVO.getCtype())) {
			return R.failed("渠道类型不能为空");
		}
		String ctype = hideColsVO.getCtype();
		String queryColumn = hideColsVO.getQueryColumn();
		String hideCols = hideColsVO.getHideCols();
		try {
			Integer userId = SecurityUtils.getUser().getId();
			stringRedisTemplate.opsForValue().set(String.format("%s%s_%s_%s_%s", CommonConstants.AD_HIDE_COLS_, APLAN_ATTR, queryColumn, ctype, userId), hideCols);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return R.failed("隐藏列设置失败");
		}
		return R.ok("隐藏列设置成功");
	}

	/**
	 * 计划属性分析表h5端自定义列保存
	 *
	 * @param hideColsVO
	 * @return
	 */
	@RequestMapping(value = "/savePlanAttrHideCols_h5")
	public R savePlanAttrHideColsH5(@Validated @RequestBody HideColsPlanAttrVO hideColsVO) {
		if (StringUtils.isBlank(hideColsVO.getCtype())) {
			return R.failed("渠道类型不能为空");
		}
		String ctype = hideColsVO.getCtype();
		String queryColumn = hideColsVO.getQueryColumn();
		String hideCols = hideColsVO.getHideCols();
		try {
			Integer userId = SecurityUtils.getUser().getId();
			stringRedisTemplate.opsForValue().set(String.format("%s%s_h5_%s_%s_%s", CommonConstants.AD_HIDE_COLS_, APLAN_ATTR, queryColumn, ctype, userId), hideCols);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return R.failed("隐藏列设置失败");
		}
		return R.ok("隐藏列设置成功");
	}

	@RequestMapping(value = "/saveHideCols")
	public R saveHideCols(@Valid @RequestBody HideColsVO hideColsVO) {
		String alias = hideColsVO.getAlias();
		String hideCols = hideColsVO.getHideCols();
		log.info("hideCols----{}", hideCols);
		try {
			if (StringUtils.isEmpty(alias)) {
				return R.failed("请输入表头别名");
			}
			Integer userId = SecurityUtils.getUser().getId();
			stringRedisTemplate.opsForValue().set(CommonConstants.AD_HIDE_COLS_ + alias + "_" + userId, hideCols);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return R.failed("隐藏列设置失败");
		}
		return R.ok("隐藏列设置成功");
	}




	/**
	 * 返回当前用户的动态表头
	 *
	 * @param alias 表头别名
	 * @return 当前用户的树形菜单
	 */
	@GetMapping("/v2/{alias}")
	public R getDynamicTabCol(@PathVariable String alias,String queryColumn, Integer operate, Long templateId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		// 1 operate  查询表头  2  查询自定义列
		// 获取符合条件的表头
		SysTabCol sysTabCol = sysTabColService.getOne(Wrappers.<SysTabCol>query().lambda().eq(SysTabCol::getAlias, alias)
				.and(wq -> wq.eq(SysTabCol::getEnv, ENV).or().eq(SysTabCol::getEnv, EMPTY_STR).or().isNull(SysTabCol::getEnv))
				.orderByDesc(SysTabCol::getEnv)
				.last("limit 1"));
		if (sysTabCol == null) {
			return R.failed("表头信息为空");
		}
		Integer userId = SecurityUtils.getUser().getId();
		//获取用户隐藏的列
//		List<String> hideColList = new ArrayList<>();
//		String hideCols = stringRedisTemplate.opsForValue().get(CommonConstants.AD_HIDE_COLS_ + alias + "_" + userId);
//		if (StringUtils.isNotBlank(hideCols)) {
//			hideColList.addAll(Arrays.asList(StringUtils.split(hideCols, ",")));
//		}
		//获取顺序列
		List<SysTabColField> orderSysTabColFieldList = new ArrayList<>();

		String orderCols;
		//获取排序的顺序列
		if (templateId != null){
			//从缓存中获取排序列信息
			orderCols = stringRedisTemplate.opsForValue().get(CommonConstants.AD_ORDER_COLS_ + userId+"_"+templateId);
			if (StringUtils.isEmpty(orderCols)){
				// 兼容之前配置的模板信息
				DmSearchTmplate dmSearchTmplate = dmSearchTmplateService.getById(templateId);
				String info = dmSearchTmplate.getInfo();
				TemplateInfo templateInfo = JSONObject.parseObject(info, TemplateInfo.class);
				List<String> customColumn = templateInfo.getCustomColumn();
				if (CollectionUtils.isNotEmpty(customColumn)){
					orderCols = String.join(",",customColumn);
				}
			}
		} else {
			orderCols = stringRedisTemplate.opsForValue().get(CommonConstants.AD_ORDER_COLS_ + alias + "_" + userId);
		}
		String content = sysTabCol.getContent();
		if (StringUtils.isBlank(content)) {
			R.failed("表头信息内容为空");
		}

		List<SysTabColField> fieldList = JSONObject.parseArray(content, SysTabColField.class);

		for (SysTabColField tabColField : fieldList) {
			//
			if (null == operate || 2 != operate) {
				// 选择渠道类型显示渠道code和名称
				if (StringUtils.isNotBlank(queryColumn) && queryColumn.indexOf("parentchl") >= 0) {
					queryColumn += ",parentchlname,parentchlName";
				}
				extracted(queryColumn, tabColField);
			}

			if (StringUtils.isNotEmpty(orderCols)){
				String[] split = orderCols.split(",");
				List<String> list = Arrays.asList(split);
				if (!list.contains(tabColField.getAttrs().getString("prop"))) {
					tabColField.getAttrs().put("hidden", true);
				}
			} else {
				if (tabColField.getAttrs().getBoolean("hidden") == null || !tabColField.getAttrs().getBoolean("hidden")){
					orderSysTabColFieldList.add(tabColField);
				}
			}
		}
		//排序
		if (StringUtils.isNotEmpty(orderCols) && !CommonConstants.AD_DYNAMIC_NONE.equals(orderCols)){
			String[] split = orderCols.split(",");
			for (int i = 0; i <split.length ; i++) {
				for (SysTabColField sysTabColField : fieldList) {
					if (split[i].equals(sysTabColField.getAttrs().getString("prop")) && (sysTabColField.getAttrs().getBoolean("hidden") == null || !sysTabColField.getAttrs().getBoolean("hidden"))){
						orderSysTabColFieldList.add(sysTabColField);
						break;
					}
				}
			}
		}
		DynamicTableColumnsVO dynamicTableColumnsVO = new DynamicTableColumnsVO();
		dynamicTableColumnsVO.setBaseTabColFiledList(fieldList);
		dynamicTableColumnsVO.setOrderTabColFiledList(orderSysTabColFieldList);
		stopWatch.stop();
		log.info("获取动态表头耗时,{}",stopWatch.getTotalTimeMillis());
		return R.ok(dynamicTableColumnsVO);
	}



	@RequestMapping(value = "/saveOrderCols")
	public R saveOrderCols(@Valid @RequestBody OrderColsVO orderColsVO) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		String alias = orderColsVO.getAlias();
		String orderCols = orderColsVO.getOrderCols();
		log.info("orderCols----{}", orderCols);
		try {
			if (StringUtils.isEmpty(alias)) {
				return R.failed("请输入表头别名");
			}
			Integer userId = SecurityUtils.getUser().getId();
			String key = CommonConstants.AD_ORDER_COLS_ + alias + "_" + userId;
			if (StringUtils.isNotEmpty(orderCols)){
				stringRedisTemplate.opsForValue().set(key, orderCols);
			} else {
				stringRedisTemplate.opsForValue().set(key,CommonConstants.AD_DYNAMIC_NONE);
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return R.failed("列排序设置失败");
		}
		stopWatch.stop();
		log.info("保存耗时,:{}",stopWatch.getTotalTimeMillis());
		return R.ok("列排序设置成功");
	}
}
