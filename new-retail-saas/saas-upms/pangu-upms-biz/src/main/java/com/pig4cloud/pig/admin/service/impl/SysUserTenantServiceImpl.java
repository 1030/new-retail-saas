package com.pig4cloud.pig.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.admin.api.entity.SysUserTenant;
import com.pig4cloud.pig.admin.mapper.SysUserTenantMapper;
import com.pig4cloud.pig.admin.service.SysUserTenantService;
import com.pig4cloud.pig.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 用户租户表
 * @author  chengang
 * @version  2021-09-15 19:28:51
 * table: sys_user_tenant
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class SysUserTenantServiceImpl extends ServiceImpl<SysUserTenantMapper, SysUserTenant> implements SysUserTenantService {
}


