/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.admin.controller;

import com.pig4cloud.pig.admin.service.SysFsUserService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 飞书用户管理 前端控制器
 * </p>
 *
 * @author hma
 * @since 2023-03-20
 */
@RestController
@AllArgsConstructor
@RequestMapping("/fs")
@Api(value = "fs", tags = "飞书用户管理模块")
public class SysFsUserController {
	private final SysFsUserService fsUserService;




	/**
	 * 查询飞书用户
	 *
	 * @return
	 */
	@Inner
	@GetMapping(value = "/getSysFsUserList")
	public R<List<Map<String,Object>>> getSysFsUserList() {
		return R.ok(fsUserService.getFsUserList());
	}


}
