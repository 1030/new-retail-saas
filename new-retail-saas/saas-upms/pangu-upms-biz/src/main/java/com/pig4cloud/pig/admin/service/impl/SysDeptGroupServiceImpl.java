/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.admin.api.entity.SysDeptGroup;
import com.pig4cloud.pig.admin.api.entity.SysUserTenant;
import com.pig4cloud.pig.admin.mapper.SysDeptGroupMapper;
import com.pig4cloud.pig.admin.service.SysDeptGroupService;
import com.pig4cloud.pig.admin.service.SysUserTenantService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @author pigx code generator
 * @date 2021-07-08 13:49:11
 */
@Service
@RequiredArgsConstructor
public class SysDeptGroupServiceImpl extends ServiceImpl<SysDeptGroupMapper, SysDeptGroup> implements SysDeptGroupService {

	private final SysUserTenantService sysUserTenantService;

	/**
	 * 删除组别
	 *
	 * @param id
	 * @return
	 */
	@Transactional
	@Override
	public R remoteDeptGroup(Integer id) {
		// 判断组别是否关联了用户
		int userCount = sysUserTenantService.count(Wrappers.<SysUserTenant>lambdaQuery().eq(SysUserTenant::getDeptGroupId, id));
		if (userCount > 0) {
			return R.failed("请先删除用户与组别的关联，然后再删除该组别");
		}

		this.removeById(id);
		return R.ok();
	}

	@Transactional
	@Override
	public boolean saveOrUpDateSysDeptGroups(List<SysDeptGroup> sysDeptGroups) throws Exception {
		if (CollectionUtils.isEmpty(sysDeptGroups)) {
			return false;
		}
		PigUser user = SecurityUtils.getUser();
		for (SysDeptGroup group : sysDeptGroups) {
			LambdaQueryWrapper<SysDeptGroup> queryWrapper = new LambdaQueryWrapper();
			queryWrapper.eq(SysDeptGroup::getName, group.getName());
			queryWrapper.eq(SysDeptGroup::getDeptId, group.getDeptId());
			if (Objects.nonNull(group.getId())) {
				queryWrapper.apply("id not in (" + group.getId() + ")");
			}
			int num = this.count(queryWrapper);
			if (num > 0) {
				throw new Exception(String.format("[%s]组别已存在", group.getName()));
			}
			if (Objects.isNull(group.getId())) {
				group.setCreateTime(LocalDateTime.now());
				group.setCreateUserId(user.getId());
				group.setUpdateTime(LocalDateTime.now());
				group.setUpdateUserId(user.getId());
				this.save(group);
			} else {
				this.updateById(group);
			}
		}
		return true;
	}

	@Transactional
	@Override
	public boolean delSysDeptGroupByDeptId(Integer deptId) {
		LambdaQueryWrapper<SysDeptGroup> queryWrapper = new LambdaQueryWrapper<>(SysDeptGroup.class);
		queryWrapper.eq(SysDeptGroup::getDeptId, deptId);
		this.remove(queryWrapper);
		return false;
	}
}
