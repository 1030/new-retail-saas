/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.admin.service.impl;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.admin.api.dto.UserDTO;
import com.pig4cloud.pig.admin.api.dto.UserInfo;
import com.pig4cloud.pig.admin.api.entity.*;
import com.pig4cloud.pig.admin.api.vo.MenuVO;
import com.pig4cloud.pig.admin.api.vo.UserSelectVO;
import com.pig4cloud.pig.admin.api.vo.UserVO;
import com.pig4cloud.pig.admin.config.AdminProperties;
import com.pig4cloud.pig.admin.mapper.SysUserMapper;
import com.pig4cloud.pig.admin.service.*;
import com.pig4cloud.pig.common.core.constant.CacheConstants;
import com.pig4cloud.pig.common.core.constant.CommonConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.tenant.TenantContextHolder;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lengleng
 * @date 2017/10/31
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
	private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();



	private static final Map<String, String> ORDER_COLUMN_MAPPING = new HashMap<String, String>() {{
		put("lastOperateTime", "last_operate_time");
		put("user.createTime", "create_time");
	}};

	private final SysMenuService sysMenuService;
	private final SysRoleService sysRoleService;
	private final SysDeptService sysDeptService;
	private final SysUserRoleService sysUserRoleService;
	private final SysDeptRelationService sysDeptRelationService;
	private final SysUserTenantService sysUserTenantService;
	private final  AdminProperties adminProperties;
	/**
	 * 保存用户信息
	 *
	 * @param userDto DTO 对象
	 * @return success/fail
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean saveUser(UserDTO userDto) {
		SysUser sysUser = new SysUser();
		BeanUtils.copyProperties(userDto, sysUser);
		sysUser.setDelFlag(CommonConstants.STATUS_NORMAL);
		sysUser.setPassword(ENCODER.encode(userDto.getPassword()));
		baseMapper.insert(sysUser);

		SysUserTenant sysUserTenant = new SysUserTenant();
		sysUserTenant.setUserId(sysUser.getUserId());
		sysUserTenant.setDeptId(sysUser.getDeptId());
		sysUserTenant.setDeptGroupId(sysUser.getDeptGroupId());
		sysUserTenant.setTenantId(TenantContextHolder.getTenantId());
		sysUserTenant.setStatus(userDto.getTenantStatus() == null ? 0 : userDto.getTenantStatus());
		sysUserTenantService.save(sysUserTenant);

		List<SysUserRole> userRoleList = userDto.getRole()
				.stream().map(roleId -> {
					SysUserRole userRole = new SysUserRole();
					userRole.setUserId(sysUser.getUserId());
					userRole.setRoleId(roleId);
					return userRole;
				}).collect(Collectors.toList());
		return sysUserRoleService.saveBatch(userRoleList);
	}

	/**
	 * 通过查用户的全部信息
	 *
	 * @param sysUser 用户
	 * @return
	 */
	@Override
	public UserInfo findUserInfo(SysUser sysUser) {
		UserInfo userInfo = new UserInfo();
		//根据userId查询部门ID
		SysUserTenant sysUserTenant = sysUserTenantService.getOne(Wrappers.<SysUserTenant>lambdaQuery().eq(SysUserTenant::getUserId, sysUser.getUserId())
				.eq(SysUserTenant::getTenantId, TenantContextHolder.getTenantId()));
		//新租户下的用户可能还没有设置部门或组别
		if (sysUserTenant != null && sysUserTenant.getDeptId() != null) {
			sysUser.setDeptId(sysUserTenant.getDeptId());
		}
		if (sysUserTenant != null && sysUserTenant.getDeptGroupId() != null) {
			sysUser.setDeptGroupId(sysUserTenant.getDeptGroupId());
		}
		userInfo.setSysUser(sysUser);
		//设置角色列表  （ID）
		List<Integer> roleIds = sysRoleService.findRolesByUserId(sysUser.getUserId())
				.stream()
				.map(SysRole::getRoleId)
				.collect(Collectors.toList());
		userInfo.setRoles(ArrayUtil.toArray(roleIds, Integer.class));

		//设置权限列表（menu.permission）
		Set<String> permissions = new HashSet<>();
		roleIds.forEach(roleId -> {
			List<String> permissionList = sysMenuService.findMenuByRoleId(roleId)
					.stream()
					.filter(menuVo -> StringUtils.isNotEmpty(menuVo.getPermission()))
					.map(MenuVO::getPermission)
					.collect(Collectors.toList());
			permissions.addAll(permissionList);
		});
		userInfo.setPermissions(ArrayUtil.toArray(permissions, String.class));
		return userInfo;
	}

	/**
	 * 分页查询用户信息（含有角色信息）
	 *
	 * @param page    分页对象
	 * @param userDTO 参数列表
	 * @return
	 */
	@Override
	public IPage getUsersWithRolePage(Page page, UserDTO userDTO) {
		Integer tenantId=TenantContextHolder.getTenantId();
		Integer cpsTenantId=adminProperties.getCpsTenantId();
		userDTO.setTenantId(tenantId);
		Integer tenantScope=1;
		if(cpsTenantId.equals(tenantId)){
			tenantScope=2;

		}
		userDTO.setTenantScope(tenantScope);
		userDTO.setCpsTenantId(cpsTenantId);
		final String orderColumn = ORDER_COLUMN_MAPPING.get(userDTO.getOrderColumn());
		if (StringUtils.isNotBlank(orderColumn)) {
			final String orderAsc = userDTO.getOrderAsc();
			page.setOrders(Collections.singletonList(new OrderItem(orderColumn, "desc".equals(orderAsc))));
		} else {
			page.setOrders(Collections.singletonList(new OrderItem("user.create_time", false)));
		}
		return baseMapper.getUserVosPage(page, userDTO);
	}

	/**
	 * 分页查询cps及非cps租户用户信息（含有角色信息）
	 *
	 * @param page    分页对象
	 * @param userDTO 参数列表
	 * @return
	 */
	@Override
	public IPage getCpsOrNonCusUserVosPage(Page page, UserDTO userDTO) {
		Integer tenantId=TenantContextHolder.getTenantId();
		Integer cpsTenantId=adminProperties.getCpsTenantId();
		userDTO.setTenantId(tenantId);
		Integer tenantScope=1;
		if(cpsTenantId.equals(tenantId)){
			tenantScope=2;

		}
		userDTO.setTenantScope(tenantScope);
		userDTO.setCpsTenantId(cpsTenantId);
		final String orderColumn = ORDER_COLUMN_MAPPING.get(userDTO.getOrderColumn());
		if (StringUtils.isNotBlank(orderColumn)) {
			final String orderAsc = userDTO.getOrderAsc();
			page.setOrders(Collections.singletonList(new OrderItem(orderColumn, "desc".equals(orderAsc))));
		} else {
			page.setOrders(Collections.singletonList(new OrderItem("user.create_time", false)));
		}
		return baseMapper.getCpsOrNonCusUserVosPage(page, userDTO);
	}
	/**
	 * 所有用户信息（下拉列表）-- 不带（已删除的用户，项目未激活）
	 * @return
	 */
	@Override
	public List<UserSelectVO> getUserSelectList() {
		UserDTO  userDTO=new UserDTO();
		Integer tenantId=TenantContextHolder.getTenantId();
		Integer cpsTenantId=adminProperties.getCpsTenantId();
		userDTO.setTenantId(tenantId);
		Integer tenantScope=1;
		if(cpsTenantId.equals(tenantId)){
			tenantScope=2;
		}
		userDTO.setTenantScope(tenantScope);
		userDTO.setCpsTenantId(cpsTenantId);
		//查询激活的用户
		userDTO.setTenantStatus(1);
		List<UserVO> userList=	baseMapper.getUserVosPage(userDTO);
		List<UserSelectVO> list = new ArrayList<>();
		for (UserVO sysUser : userList) {
			UserSelectVO userSelectVO = new UserSelectVO();
			BeanUtils.copyProperties(sysUser, userSelectVO);
			list.add(userSelectVO);
		}
		return  list;
	}

	/**
	 * 通过ID查询用户信息
	 *
	 * @param id 用户ID
	 * @return 用户信息
	 */
	@Override
	public UserVO selectUserVoById(Integer id) {
		return baseMapper.getUserVoById(id);
	}

	/**
	 * 删除用户
	 *
	 * @param sysUser 用户
	 * @return Boolean
	 */
	@Override
	@CacheEvict(value = CacheConstants.USER_DETAILS, key = "#sysUser.username")
	public Boolean deleteUserById(SysUser sysUser) {
		sysUserRoleService.deleteByUserId(sysUser.getUserId());
		//删除用户、部门、租户的关系
		sysUserTenantService.remove(Wrappers.<SysUserTenant>lambdaQuery()
				.eq(SysUserTenant::getTenantId, SecurityUtils.getTenantId())
				.eq(SysUserTenant::getUserId, sysUser.getUserId()));
		this.removeById(sysUser.getUserId());
		return Boolean.TRUE;
	}

	@Override
	@CacheEvict(value = CacheConstants.USER_DETAILS, key = "#userDto.username")
	public R<Boolean> updateUserInfo(UserDTO userDto) {
		UserVO userVO = baseMapper.getUserVoByUsername(userDto.getUsername());
		if (userVO == null) {
			return R.failed("用户不存在");
		}
		SysUser sysUser = new SysUser();
		if (StrUtil.isNotBlank(userDto.getPassword())
				&& StrUtil.isNotBlank(userDto.getNewpassword1())) {
			if (StrUtil.equals(userDto.getNewpassword1(), userDto.getNewpassword2())) {
				if (ENCODER.matches(userDto.getPassword(), userVO.getPassword())) {
					sysUser.setPassword(ENCODER.encode(userDto.getNewpassword1()));
				} else {
					log.warn("原密码错误，修改密码失败:{}", userDto.getUsername());
					return R.ok(Boolean.FALSE, "原密码错误，修改失败");
				}
			} else {
				log.warn("密码和确认密码不一致，修改密码失败:{}", userDto.getUsername());
				return R.ok(Boolean.FALSE, "密码和确认密码不一致，修改失败");
			}
		}
		sysUser.setPhone(userDto.getPhone());
		sysUser.setUserId(userVO.getUserId());
		sysUser.setAvatar(userDto.getAvatar());
		sysUser.setLastLoginTime(userDto.getLastLoginTime());
		sysUser.setLastOperateTime(userDto.getLastOperateTime());

		return R.ok(this.updateById(sysUser));
	}

	@Override
	@CacheEvict(value = CacheConstants.USER_DETAILS, key = "#userDto.username")
	public Boolean updateUser(UserDTO userDto) {
		SysUser sysUser = new SysUser();
		BeanUtils.copyProperties(userDto, sysUser);
		sysUser.setStatus(userDto.getTenantStatus() == null ? 0 : userDto.getTenantStatus());
		sysUser.setUpdateTime(LocalDateTime.now());

		if (StrUtil.isNotBlank(userDto.getPassword())) {
			sysUser.setPassword(ENCODER.encode(userDto.getPassword()));
		}
		this.updateById(sysUser);

		// 处理用户，租户，部门，组别，关系表
		dealSysUserTenant(sysUser);

		sysUserRoleService.remove(Wrappers.<SysUserRole>update().lambda()
				.eq(SysUserRole::getUserId, userDto.getUserId()));
		userDto.getRole().forEach(roleId -> {
			SysUserRole userRole = new SysUserRole();
			userRole.setUserId(sysUser.getUserId());
			userRole.setRoleId(roleId);
			userRole.insert();
		});
		return Boolean.TRUE;
	}

	public void dealSysUserTenant(SysUser sysUser) {
		Integer tenantId = TenantContextHolder.getTenantId();
		SysUserTenant sysUserTenant = sysUserTenantService.getOne(Wrappers.<SysUserTenant>query().lambda().eq(SysUserTenant::getTenantId, tenantId).eq(SysUserTenant::getUserId, sysUser.getUserId()).last("LIMIT 1"));
		if (Objects.isNull(sysUserTenant)) {
			SysUserTenant sysUserTenantAdd = new SysUserTenant();
			sysUserTenantAdd.setUserId(sysUser.getUserId());
			sysUserTenantAdd.setDeptId(sysUser.getDeptId());
			sysUserTenantAdd.setDeptGroupId(sysUser.getDeptGroupId());
			sysUserTenantAdd.setTenantId(tenantId);
			sysUserTenantAdd.setStatus(sysUser.getStatus());
			sysUserTenantService.save(sysUserTenantAdd);
		} else {
			QueryWrapper<SysUserTenant> wrapper = new QueryWrapper<>();
			wrapper.eq("tenant_id", tenantId);
			wrapper.eq("user_id", sysUser.getUserId());
			sysUserTenant.setDeptId(sysUser.getDeptId());
			sysUserTenant.setDeptGroupId(sysUser.getDeptGroupId());
			sysUserTenant.setStatus(sysUser.getStatus());
			sysUserTenantService.update(sysUserTenant, wrapper);
		}

	}

	/**
	 * 查询上级部门的用户信息
	 *
	 * @param username 用户名
	 * @return R
	 */
	@Override
	public List<SysUser> listAncestorUsers(String username) {
		SysUser sysUser = this.getOne(Wrappers.<SysUser>query().lambda()
				.eq(SysUser::getUsername, username));

		SysDept sysDept = sysDeptService.getById(sysUser.getDeptId());
		if (sysDept == null) {
			return null;
		}

		Integer parentId = sysDept.getParentId();
		return this.list(Wrappers.<SysUser>query().lambda()
				.eq(SysUser::getDeptId, parentId));
	}

	/**
	 * 获取当前用户的子部门信息
	 *
	 * @return 子部门列表
	 */
	private List<Integer> getChildDepts() {
		Integer deptId = SecurityUtils.getUser().getDeptId();
		//获取当前部门的子部门
		return sysDeptRelationService
				.list(Wrappers.<SysDeptRelation>query().lambda()
						.eq(SysDeptRelation::getAncestor, deptId))
				.stream()
				.map(SysDeptRelation::getDescendant)
				.collect(Collectors.toList());
	}

	/**
	 * 根据用户ID列表获取用户姓名
	 *
	 * @param userIds
	 * @return
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	@Override
	public List<SysUser> getUserIdAndNameByIds(List<Integer> userIds) {
		if (CollectionUtils.isEmpty(userIds)) {
			return Collections.emptyList();
		}
		return this.list(Wrappers.<SysUser>lambdaQuery().select(SysUser::getUserId, SysUser::getRealName).in(SysUser::getUserId, userIds).eq(SysUser::getDelFlag, NumberUtils.INTEGER_ZERO));
	}

	@Override
	public List<UserVO> getUserSelectList4All() {
		return baseMapper.getUserSelectList4All();
	}

	@Override
	public List<SysUser> getUserIdAndNameByList(List<Integer> userIds) {
		if (CollectionUtils.isEmpty(userIds)) {
			return Collections.emptyList();
		}
		return this.list(Wrappers.<SysUser>lambdaQuery()
				.select(SysUser::getUserId, SysUser::getRealName)
				.in(SysUser::getUserId, userIds)
				.eq(SysUser::getDelFlag, NumberUtils.INTEGER_ZERO)
		);

	}


}
