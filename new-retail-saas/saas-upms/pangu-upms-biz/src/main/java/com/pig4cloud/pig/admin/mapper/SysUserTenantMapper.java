package com.pig4cloud.pig.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.pig.admin.api.entity.SysUserTenant;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户租户表
 * @author  chengang
 * @version  2021-09-15 19:28:51
 * table: sys_user_tenant
 */
@Mapper
public interface SysUserTenantMapper extends BaseMapper<SysUserTenant> {
	
	//单表基本CURD操作BaseMapper已经存在,请尽量不用重载父类已定义的方法
}


