package com.pig4cloud.pig.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.admin.api.entity.SysTabCol;
import com.pig4cloud.pig.admin.mapper.SysTabColMapper;
import com.pig4cloud.pig.admin.service.SysTabColService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class SysTabColServiceImpl extends ServiceImpl<SysTabColMapper, SysTabCol>  implements SysTabColService {

}
