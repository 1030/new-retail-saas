

package com.pig4cloud.pig.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.admin.api.entity.SysUserTenant;

/**
 * 用户租户表
 * @author  chengang
 * @version  2021-09-15 19:28:51
 * table: sys_user_tenant
 */
public interface SysUserTenantService extends IService<SysUserTenant> {
}


