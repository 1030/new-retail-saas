package com.pig4cloud.pig.admin.service.impl;


import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.api.client.util.Lists;
import com.pig4cloud.pig.admin.api.entity.SysFsUser;
import com.pig4cloud.pig.admin.mapper.SysFsUserMapper;
import com.pig4cloud.pig.admin.service.SysFsUserService;
import com.pig4cloud.pig.common.core.util.MapUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Description: 系统飞书用户
 * @Author: hma
 * @Date:   2023-03-17
 * @Version: V1.0
 */
@Service
public class SysFsUserServiceImpl extends ServiceImpl<SysFsUserMapper, SysFsUser> implements SysFsUserService {


	/**
	 * 查询所有的飞书用户
	 * @return
	 */
	@Override
	public  List<Map<String,Object>>  getFsUserList(){
		List<Map<String,Object>> mapList=this.listMaps(Wrappers.<SysFsUser>lambdaQuery().eq(SysFsUser::getDeleted, 0).select(SysFsUser::getFsUserId,SysFsUser::getRealName));
		List<Map<String, Object>> transMap= Lists.newArrayList();
		if(CollectionUtils.isNotEmpty(mapList)){
			mapList.forEach(m->{
				transMap.add(MapUtils.toReplaceKeyLow(m));

			});
		}
		return transMap;
	 }

}
