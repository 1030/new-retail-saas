package com.pig4cloud.pig.juhe.api.request;

import com.baomidou.mybatisplus.annotation.TableId;
import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 聚合游戏包
 * @date 2021/9/8 14:16
 */
@Data
@ApiModel(value = "聚合游戏包")
public class JuheGamePageRequest implements Serializable {

	@ApiModelProperty(value = "游戏ID")
	@NotNull(message = "游戏ID不能为空")
	private Long gameId;

	@ApiModelProperty(value = "更新说明")
	@Size(max = 255, message = "更新说明长度不合法")
	private String pageContext;

	@ApiModelProperty(value = "母包地址")
	@NotBlank(message = "{母包地址不能为空}")
	private String pageUrl;
}
