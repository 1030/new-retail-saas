package com.pig4cloud.pig.juhe.api.request;

import com.pig4cloud.pig.juhe.api.groups.Insert;
import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 渠道属性字段
 * @date 2021/9/6 17:15
 */
@Data
@ApiModel(value = "渠道属性值")
public class AttrValueRequest implements Serializable {

	@ApiModelProperty(value="属性KEY")
	@NotBlank( message = "属性KEY不能为空")
	@Size(max = 50, message = "属性KEY不合法")
	private String attrKey;

	@ApiModelProperty(value="属性值")
	@NotBlank(message = "属性值不能为空")
	@Size(max = 50, message = "属性值长度不合法")
	private String attrValue;
}
