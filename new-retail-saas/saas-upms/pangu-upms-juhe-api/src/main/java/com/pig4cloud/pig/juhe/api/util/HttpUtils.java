package com.pig4cloud.pig.juhe.api.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 通用http发送方法
 *
 * @author yuwenfeng
 */
public class HttpUtils {

	public static int DEFAULT_CONNECT_TIMEOUT = 5000;
	public static int DEFAULT_SOCKET_TIMEOUT = 5000;
	public static String CONTENT_CHARSET_UTF8 = "UTF-8";

	public static String doPost(String url, Map<String, String> paramsMap) {
		CloseableHttpClient client = null;
		String resultStr = null;
		try {
			client = HttpClients.createDefault();
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(DEFAULT_SOCKET_TIMEOUT).setConnectTimeout(DEFAULT_CONNECT_TIMEOUT).build();
			HttpPost post = new HttpPost(url);
			post.setConfig(requestConfig);
			List<NameValuePair> formparams = new ArrayList();
			String name = null;
			if (paramsMap != null) {
				Iterator it = paramsMap.keySet().iterator();
				while (it.hasNext()) {
					name = (String) it.next();
					formparams.add(new BasicNameValuePair(name, (String) paramsMap.get(name)));
				}
			}
			UrlEncodedFormEntity uefEntity = new UrlEncodedFormEntity(formparams, CONTENT_CHARSET_UTF8);
			post.setEntity(uefEntity);
			HttpResponse response = client.execute(post);
			final HttpEntity entity = response.getEntity();
			if (entity != null) {
				resultStr = EntityUtils.toString(entity);
				return resultStr;
			}
		} catch (Exception var13) {
			throw new RuntimeException(var13);
		}
		return null;
	}

	public static String doPost(String url, Map<String, String> paramsMap, int socketTimeout, int connectTimeout) {
		CloseableHttpClient client = null;
		String resultStr = null;
		try {
			client = HttpClients.createDefault();
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(socketTimeout).setConnectTimeout(connectTimeout).build();
			HttpPost post = new HttpPost(url);
			post.setConfig(requestConfig);
			List<NameValuePair> formparams = new ArrayList();
			String name = null;
			if (paramsMap != null) {
				Iterator it = paramsMap.keySet().iterator();
				while (it.hasNext()) {
					name = (String) it.next();
					formparams.add(new BasicNameValuePair(name, (String) paramsMap.get(name)));
				}
			}
			UrlEncodedFormEntity uefEntity = new UrlEncodedFormEntity(formparams, CONTENT_CHARSET_UTF8);
			post.setEntity(uefEntity);
			HttpResponse response = client.execute(post);
			final HttpEntity entity = response.getEntity();
			if (entity != null) {
				resultStr = EntityUtils.toString(entity);
				return resultStr;
			}
		} catch (Exception var13) {
			throw new RuntimeException(var13);
		}
		return null;
	}

}
