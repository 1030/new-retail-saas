/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 聚合用户表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Data
@TableName("juhe_user")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合用户表")
public class JuheUser extends BaseEntity {

    /**
     * 聚合用户ID
     */
	@TableId(value = "open_id", type = IdType.AUTO)
    @ApiModelProperty(value="聚合用户ID")
    private Long openId;

    /**
     * 渠道ID
     */
    @ApiModelProperty(value="渠道ID")
    private Long channelId;

    /**
     * 游戏ID
     */
    @ApiModelProperty(value="游戏ID")
    private Long gameId;

    /**
     * 渠道方用户ID
     */
    @ApiModelProperty(value="渠道方用户ID")
    private String userId;

    /**
     * 注册时间
     */
    @ApiModelProperty(value="注册时间")
    private Date regTime;

    /**
     * 注册IP
     */
    @ApiModelProperty(value="注册IP")
    private String regIp;

    /**
     * 最后登录时间
     */
    @ApiModelProperty(value="最后登录时间")
    private Date lastLoginTime;

    /**
     * 最后登录IP
     */
    @ApiModelProperty(value="最后登录IP")
    private String lastLoginIp;


}
