/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 聚合打包记录表
 *
 * @author yuwenfeng
 * @date 2021-09-15 13:47:28
 */
@Data
@TableName("juhe_pack")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合打包记录表")
public class JuhePack extends BaseEntity {

    /**
     * 记录ID
     */
	@TableId(value = "pack_id", type = IdType.AUTO)
    @ApiModelProperty(value="记录ID")
    private Long packId;

    /**
     * 渠道ID
     */
    @ApiModelProperty(value="渠道ID")
    private Long channelId;

    /**
     * 游戏ID
     */
    @ApiModelProperty(value="游戏ID")
    private Long gameId;

	/**
	 * 母包ID
	 */
	@ApiModelProperty(value="母包ID")
	private Long pageId;

    /**
     * 版本号
     */
    @ApiModelProperty(value="版本号")
    private String packVersion;

    /**
     * 打包状态(1打包中 2打包成功 3打包失败)
     */
    @ApiModelProperty(value="打包状态(1打包中 2打包成功 3打包失败)")
    private Integer packStatus;

    /**
     * 下载地址
     */
    @ApiModelProperty(value="下载地址")
    private String packUrl;


}
