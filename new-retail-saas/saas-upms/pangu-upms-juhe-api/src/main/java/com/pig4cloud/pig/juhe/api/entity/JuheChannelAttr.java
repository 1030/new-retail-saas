/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 聚合渠道商属性表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Data
@TableName("juhe_channel_attr")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合渠道商属性表")
public class JuheChannelAttr extends BaseEntity {

    /**
     * 属性ID
     */
	@TableId(value = "attr_id", type = IdType.AUTO)
    @ApiModelProperty(value="属性ID")
    private Long attrId;

    /**
     * 渠道ID
     */
    @ApiModelProperty(value="渠道ID")
    private Long channelId;

    /**
     * 属性KEY
     */
    @ApiModelProperty(value="属性KEY")
    private String attrKey;

    /**
     * 属性说明
     */
    @ApiModelProperty(value="属性说明")
    private String attrName;

	/**
	 * 属性描述
	 */
	@ApiModelProperty(value="属性描述")
	private String attrDesc;

    /**
     * 是否必填(1是 0否)
     */
    @ApiModelProperty(value="是否必填(1是 0否)")
    private Integer attrRequired;


}
