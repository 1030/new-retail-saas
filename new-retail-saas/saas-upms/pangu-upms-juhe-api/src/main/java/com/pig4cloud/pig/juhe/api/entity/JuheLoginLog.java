/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 聚合登录日志
 *
 * @author yuwenfeng
 * @date 2022-02-21 14:41:01
 */
@Data
@TableName("juhe_login_log")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合登录日志")
public class JuheLoginLog extends BaseEntity {

    /**
     * 日志ID
     */
	@TableId(value = "log_id", type = IdType.AUTO)
    @ApiModelProperty(value="日志ID")
    private Long logId;

    /**
     * 设备ID
     */
    @ApiModelProperty(value="设备ID")
    private Long deviceId;

    /**
     * 聚合用户ID
     */
    @ApiModelProperty(value="聚合用户ID")
    private Long openId;


}
