package com.pig4cloud.pig.juhe.api.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yuwenfeng
 * @description: 聚合用户信息
 * @date 2021/9/3 11:10
 */
@Data
@ApiModel(value = "聚合打包记录")
public class JuhePackVo implements Serializable {

	@ApiModelProperty(value="记录ID")
	private Long packId;

	/**
	 * 渠道ID
	 */
	@ApiModelProperty(value="渠道ID")
	private Long channelId;

	/**
	 * 游戏ID
	 */
	@ApiModelProperty(value="游戏ID")
	private Long gameId;

	/**
	 * 母包ID
	 */
	@ApiModelProperty(value="母包ID")
	private Long pageId;

	/**
	 * 版本号
	 */
	@ApiModelProperty(value="版本号")
	private String packVersion;

	/**
	 * 打包状态(1打包中 2打包成功 3打包失败)
	 */
	@ApiModelProperty(value="打包状态(1打包中 2打包成功 3打包失败)")
	private Integer packStatus;

	/**
	 * 下载地址
	 */
	@ApiModelProperty(value="下载地址")
	private String packUrl;

	@ApiModelProperty(value = "渠道名称")
	private String channelName;

	@ApiModelProperty(value = "渠道SDK名称")
	private String channelSdkName;

	@ApiModelProperty(value = "游戏名称")
	private String gameName;

	@ApiModelProperty(value = "创建人")
	private String createBy;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;
}
