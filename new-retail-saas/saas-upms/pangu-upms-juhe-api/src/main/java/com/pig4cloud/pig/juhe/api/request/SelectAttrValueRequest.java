package com.pig4cloud.pig.juhe.api.request;

import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 游戏渠道配置入参
 * @date 2021/9/7 14:28
 */
@Data
@ApiModel(value = "查询游戏渠道配置信息")
public class SelectAttrValueRequest implements Serializable {

	@ApiModelProperty(value = "渠道ID")
	@NotNull(message = "渠道ID不能为空")
	private Long channelId;

	@ApiModelProperty(value = "游戏ID")
	@NotNull(message = "游戏ID不能为空")
	private Long gameId;

}
