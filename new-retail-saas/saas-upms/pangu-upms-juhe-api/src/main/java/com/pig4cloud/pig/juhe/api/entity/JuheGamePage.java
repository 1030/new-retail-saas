/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 聚合游戏母包表
 *
 * @author yuwenfeng
 * @date 2021-09-08 14:00:01
 */
@Data
@TableName("juhe_game_page")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合游戏母包表")
public class JuheGamePage extends BaseEntity {

	/**
	 * 母包ID
	 */
	@TableId(value = "page_id", type = IdType.AUTO)
	@ApiModelProperty(value = "母包ID")
	private Long pageId;

	/**
	 * 游戏ID
	 */
	@ApiModelProperty(value = "游戏ID")
	private Long gameId;

	/**
	 * 包名
	 */
	@ApiModelProperty(value = "包名")
	private String pageName;

	/**
	 * 版本号
	 */
	@ApiModelProperty(value = "版本号")
	private String pageVersion;

	/**
	 * 更新说明
	 */
	@ApiModelProperty(value = "更新说明")
	private String pageContext;

	/**
	 * 母包地址
	 */
	@ApiModelProperty(value = "母包地址")
	private String pageUrl;

	/**
	 * 母包图标
	 */
	@ApiModelProperty(value = "母包图标")
	private String pageIcon;

	/**
	 * 母包包名
	 */
	@ApiModelProperty(value = "母包包名")
	private String pagePackage;

	/**
	 * 母包信息
	 */
	@ApiModelProperty(value = "母包信息")
	private String pageInfo;

	/**
	 * 状态(1解析中 2解析成功 3解析失败)
	 */
	@ApiModelProperty(value="状态(1解析中 2解析成功 3解析失败)")
	private Integer pageStatus;
}
