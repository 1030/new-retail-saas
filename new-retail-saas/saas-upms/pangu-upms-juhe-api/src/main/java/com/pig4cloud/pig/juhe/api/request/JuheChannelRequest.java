package com.pig4cloud.pig.juhe.api.request;

import com.baomidou.mybatisplus.annotation.TableId;
import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 聚合渠道属性
 * @date 2021/9/6 17:12
 */
@Data
@ApiModel(value = "新增、编辑聚合渠道")
public class JuheChannelRequest implements Serializable {

	@ApiModelProperty(value = "渠道商ID")
	@NotNull(groups = Update.class, message = "渠道商ID不能为空")
	private Long channelId;

	@ApiModelProperty(value = "渠道名称")
	@NotBlank(message = "渠道名称不能为空")
	private String channelName;

	@ApiModelProperty(value = "SDK名称")
	@NotBlank(message = "SDK名称不能为空")
	private String channelSdkName;

	@ApiModelProperty(value = "SDK版本")
	@NotBlank(message = "SDK版本不能为空")
	private String channelVersion;

	@ApiModelProperty(value = "登录认证地址")
	private String loginAuthUrl;

	@ApiModelProperty(value = "支付回调地址")
	private String payBackUrl;

	@ApiModelProperty(value = "渠道下单地址")
	private String orderUrl;

	@ApiModelProperty(value = "属性集合不能为空")
	@NotNull(message = "属性集合不能为空")
	private List<ChannelAttrRequest> attrList;
}
