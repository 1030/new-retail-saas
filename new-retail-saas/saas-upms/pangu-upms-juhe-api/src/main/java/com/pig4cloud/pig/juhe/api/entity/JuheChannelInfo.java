/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 聚合渠道商表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Data
@TableName("juhe_channel_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合渠道商表")
public class JuheChannelInfo extends BaseEntity {

	/**
	 * 主键ID
	 */
	@TableId(value = "channel_id", type = IdType.AUTO)
	@ApiModelProperty(value = "渠道商ID")
	private Long channelId;

	/**
	 * 渠道名称
	 */
	@ApiModelProperty(value = "渠道名称")
	private String channelName;

	/**
	 * SDK名称
	 */
	@ApiModelProperty(value = "渠道SDK名称")
	private String channelSdkName;

	/**
	 * SDK版本
	 */
	@ApiModelProperty(value = "SDK版本")
	private String channelVersion;

	/**
	 * 登录认证地址
	 */
	@ApiModelProperty(value = "登录认证地址")
	private String loginAuthUrl;
	/**
	 * 支付回调地址
	 */
	@ApiModelProperty(value = "支付回调地址")
	private String payBackUrl;
	/**
	 * 渠道下单地址
	 */
	@ApiModelProperty(value = "渠道下单地址")
	private String orderUrl;

}
