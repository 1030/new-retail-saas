package com.pig4cloud.pig.juhe.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yuwenfeng
 * @description: 聚合用户信息
 * @date 2021/9/3 11:10
 */
@Data
@ApiModel(value = "聚合用户信息")
public class JuheUserVo implements Serializable {

	@ApiModelProperty(value = "聚合用户ID")
	private Long openId;

	@ApiModelProperty(value = "渠道ID")
	private Long channelId;

	@ApiModelProperty(value = "游戏ID")
	private Long gameId;

	@ApiModelProperty(value = "渠道方用户ID")
	private String userId;

	@ApiModelProperty(value = "注册时间")
	private Date regTime;

	@ApiModelProperty(value = "注册IP")
	private String regIp;

	@ApiModelProperty(value = "最后登录时间")
	private Date lastLoginTime;

	@ApiModelProperty(value = "最后登录IP")
	private String lastLoginIp;

	@ApiModelProperty(value = "渠道名称")
	private String channelName;
	@ApiModelProperty(value = "游戏名称")
	private String gameName;
}
