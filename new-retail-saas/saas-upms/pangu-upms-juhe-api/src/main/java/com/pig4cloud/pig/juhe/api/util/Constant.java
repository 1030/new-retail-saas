package com.pig4cloud.pig.juhe.api.util;

import java.util.LinkedList;

/**
 * @author yuwenfeng
 * @description: 常量类
 * @date 2021/9/14 15:24
 */
public class Constant {

	public static final String APK = "apk";
	public static final String IPA = "ipa";
	public static final String JSON = "json";
	public static final String PNG = "png";

	//文件存储相对目录
	public static final String FILE_PACKAGE_IN = "packageIn";

	//文件存储相对目录
	public static final String FILE_PACKAGE_OUT = "packageOut";

	//文件存储相对目录
	public static final String FILE_IMAGES_PATH = "images";

	public static final String PACKAGE_PACK_KEY = "outPackagePath";

	public static final String PACKAGE_PACK_ID = "packId";

	public static final String PACKAGE_GAME_NAME = "juheGameName";

	public static final String PACKAGE_CHANNEL_NAME = "juheChannelName";

	public static final int PACKAGE_STATUS_WAIT = 1;
	public static final int PACKAGE_STATUS_SUCC = 2;
	public static final int PACKAGE_STATUS_FAIL = 3;

	public static final int PAGE_STATUS_WAIT = 1;
	public static final int PAGE_STATUS_SUCC = 2;
	public static final int PAGE_STATUS_FAIL = 3;

	public static final int DEL_YES = 1;
	public static final int DEL_NO = 0;

	/**
	 * 第三方标识-华为
	 */
	public static final String OPEN_TYPE_HUAWEI = "huawei";

	public static final String HUAWEI_JSON_KEY = "package_jsonPath";

	public static final String URL_REGEXP ="^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+$";

}
