package com.pig4cloud.pig.juhe.api.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 查询聚合订单列表
 * @date 2021/9/7 14:28
 */
@Data
@ApiModel(value = "查询聚合订单列表")
public class SelectJuheOrderRequest implements Serializable {

	/**
	 * 渠道ID
	 */
	@ApiModelProperty(value = "渠道ID")
	private Long channelId;

	/**
	 * 游戏ID
	 */
	@ApiModelProperty(value = "游戏ID")
	private Long gameId;

	/**
	 * 订单流水号
	 */
	@ApiModelProperty(value = "订单流水号")
	private String orderSerial;

	/**
	 * 聚合用户ID
	 */
	@ApiModelProperty(value = "聚合用户ID")
	private Long openId;

	/**
	 * 游戏订单号
	 */
	@ApiModelProperty(value = "游戏订单号")
	private String gameOrderSerial;

	@ApiModelProperty(value="渠道订单号")
	private String channelOrderSerial;

	/**
	 * 订单状态 0：预订单 1：未支付 2：支付成功 3：支付失败 4：异常订单 5：退款 6：操作中
	 */
	@ApiModelProperty(value = "订单状态 0：预订单 1：未支付 2：支付成功 3：支付失败 4：异常订单 5：退款 6：操作中")
	private Integer orderStatus;

	/**
	 * 用户余额
	 */
	@ApiModelProperty(value = "用户余额")
	private Double userBalance;

	/**
	 * vip等级
	 */
	@ApiModelProperty(value = "vip等级")
	private String vipLevel;

	/**
	 * 角色等级
	 */
	@ApiModelProperty(value = "角色等级")
	private String roleLevel;

	/**
	 * 工会，帮派
	 */
	@ApiModelProperty(value = "工会，帮派")
	private String gangsName;

	@ApiModelProperty(value = "角色名称")
	private String roleName;

	@ApiModelProperty(value = "角色id")
	private String roleId;

	@ApiModelProperty(value = "服务器ID")
	private String serverId;

	@ApiModelProperty(value = "所在服务器")
	private String serverName;

	@ApiModelProperty(value = "开始时间")
	private String sTime;

	@ApiModelProperty(value = "结束时间")
	private String eTime;
}
