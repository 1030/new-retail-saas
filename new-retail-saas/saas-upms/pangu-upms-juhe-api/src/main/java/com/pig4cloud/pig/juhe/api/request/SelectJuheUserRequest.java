package com.pig4cloud.pig.juhe.api.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description: 用户查询列表
 * @author yuwenfeng
 * @date 2022/7/4 10:15
 */
@Data
@ApiModel(value = "查询用户查询列表")
public class SelectJuheUserRequest implements Serializable {


	@ApiModelProperty(value = "渠道ID")
	private Long channelId;

	@ApiModelProperty(value = "游戏ID")
	private Long gameId;

	@ApiModelProperty(value="聚合用户ID")
	private Long openId;

	@ApiModelProperty(value="渠道方用户ID")
	private String userId;

	@ApiModelProperty(value = "开始时间")
	private String sTime;

	@ApiModelProperty(value = "结束时间")
	private String eTime;
}
