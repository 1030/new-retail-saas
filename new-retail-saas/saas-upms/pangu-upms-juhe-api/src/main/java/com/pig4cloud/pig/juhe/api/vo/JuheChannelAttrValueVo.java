/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 聚合渠道属性值
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Data
@ApiModel(value = "聚合渠道属性值")
public class JuheChannelAttrValueVo implements Serializable {

	@ApiModelProperty(value = "属性ID")
	private Long attrId;

	@ApiModelProperty(value = "属性KEY")
	private String attrKey;

	@ApiModelProperty(value = "属性说明")
	private String attrName;

	@ApiModelProperty(value="属性描述")
	private String attrDesc;

	@ApiModelProperty(value = "是否必填(1是 0否)")
	private Integer attrRequired;

	@ApiModelProperty(value = "属性值")
	private String attrValue;

}
