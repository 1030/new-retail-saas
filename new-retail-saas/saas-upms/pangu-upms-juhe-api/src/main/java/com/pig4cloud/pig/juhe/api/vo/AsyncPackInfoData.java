package com.pig4cloud.pig.juhe.api.vo;

import com.pig4cloud.pig.juhe.api.entity.JuheChannelInfo;
import com.pig4cloud.pig.juhe.api.entity.JuheGameChannel;
import com.pig4cloud.pig.juhe.api.entity.JuheGameInfo;
import com.pig4cloud.pig.juhe.api.entity.JuheGamePage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 分包信息载体
 * @date 2021/9/3 11:10
 */
@Data
public class AsyncPackInfoData implements Serializable {

	private Long packId;

	private JuheChannelInfo channelInfo;

	private JuheGameChannel gameChannel;

	private JuheGamePage gamePage;

	private List<JuheChannelAttrValueVo> attrValueList;

	private JuheGameInfo gameInfo;

}
