/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 聚合设备uuid表
 *
 * @author yuwenfeng
 * @date 2022-02-21 14:41:01
 */
@Data
@TableName("juhe_device_uuid")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合设备uuid表")
public class JuheDeviceUuid extends BaseEntity {

    /**
     * 主键id
     */
	@TableId(value = "device_id", type = IdType.AUTO)
    @ApiModelProperty(value="主键id")
    private Long deviceId;

    /**
     * 苹果idfa
     */
    @ApiModelProperty(value="苹果idfa")
    private String iosIdfa;

    /**
     * 苹果openudid
     */
    @ApiModelProperty(value="苹果openudid")
    private String iosOpenudid;

    /**
     * 安卓mac
     */
    @ApiModelProperty(value="安卓mac")
    private String androidMac;

    /**
     * 安卓imei
     */
    @ApiModelProperty(value="安卓imei")
    private String androidImei;

    /**
     * 安卓mobileId
     */
    @ApiModelProperty(value="安卓mobileId")
    private String androidMobileId;

    /**
     * 安卓oaid
     */
    @ApiModelProperty(value="安卓oaid")
    private String androidOaid;

    /**
     * 设备os
     */
    @ApiModelProperty(value="设备os")
    private String deviceOs;

    /**
     * uuid
     */
    @ApiModelProperty(value="uuid")
    private String deviceUuid;

    /**
     * utdid
     */
    @ApiModelProperty(value="utdid")
    private String deviceUtdid;

}
