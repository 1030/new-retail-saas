package com.pig4cloud.pig.juhe.api.request;

import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 游戏渠道配置入参
 * @date 2021/9/7 14:28
 */
@Data
@ApiModel(value = "新增、编辑游戏渠道配置入参")
public class JuheGameChannelRequest implements Serializable {

	@ApiModelProperty(value = "记录ID")
	@NotNull(groups = Update.class, message = "记录ID不能为空")
	private Long recordId;

	@ApiModelProperty(value = "渠道ID")
	@NotNull(message = "渠道ID不能为空")
	private Long channelId;

	@ApiModelProperty(value = "游戏ID")
	@NotNull(message = "游戏ID不能为空")
	private Long gameId;

	@ApiModelProperty(value = "渠道图标")
	private String iconPath;

	@ApiModelProperty(value = "隐私协议地址")
	private String privacyUrl;

	@ApiModelProperty(value = "游戏发货地址")
	private String payCallbackUrl;

	@NotNull(message = "隐私开关不能为空")
	@ApiModelProperty(value = "隐私开关(1开 0关)")
	private Integer privacyOff;

	@ApiModelProperty(value = "属性值集合不能为空")
	@NotNull(message = "属性值集合不能为空")
	private List<AttrValueRequest> attrValueList;
}
