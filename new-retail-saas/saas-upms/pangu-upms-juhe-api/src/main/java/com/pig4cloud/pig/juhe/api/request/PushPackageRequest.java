package com.pig4cloud.pig.juhe.api.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 推送母包至运营平台
 * @date 2022/7/4 15:39
 */
@Data
@ApiModel(value = "推送母包至运营平台")
public class PushPackageRequest implements Serializable {

	@ApiModelProperty(value = "记录ID")
	@NotNull(message = "记录ID不能为空")
	private Long packId;

	@ApiModelProperty(value = "包名")
	@NotBlank(message = "包名不能为空")
	private String pkName;

	@ApiModelProperty(value = "父游戏ID")
	@NotNull(message = "父游戏ID不能为空")
	private Long parentGameId;

	@ApiModelProperty(value = "版本编码")
	private Integer versionCode;

	@ApiModelProperty(value = "更新说明")
	private String context;
}
