/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 聚合订单表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Data
@TableName("juhe_order_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合订单表")
public class JuheOrderInfo extends BaseEntity {

    /**
     * 聚合订单id
     */
	@TableId(value = "order_id", type = IdType.AUTO)
    @ApiModelProperty(value="聚合订单id")
    private Long orderId;

    /**
     * 订单类型 1商品 2计费点
     */
    @ApiModelProperty(value="订单类型 1商品 2计费点")
    private Integer orderType;

    /**
     * 渠道ID
     */
    @ApiModelProperty(value="渠道ID")
    private Long channelId;

    /**
     * 游戏ID
     */
    @ApiModelProperty(value="游戏ID")
    private Long gameId;

    /**
     * 订单流水号
     */
    @ApiModelProperty(value="订单流水号")
    private String orderSerial;

    /**
     * 聚合用户ID
     */
    @ApiModelProperty(value="聚合用户ID")
    private Long openId;

    /**
     * 订单付费金额
     */
    @ApiModelProperty(value="订单付费金额")
    private Long orderFee;

    /**
     * 商品编码
     */
    @ApiModelProperty(value="商品编码")
    private String goodsCode;

    /**
     * 商品名称
     */
    @ApiModelProperty(value="商品名称")
    private String goodsName;

    /**
     * 商品描述
     */
    @ApiModelProperty(value="商品描述")
    private String goodsDesc;

	/**
	 * 游戏订单号
	 */
	@ApiModelProperty(value="渠道订单号")
	private String channelOrderSerial;

    /**
     * 游戏订单号
     */
    @ApiModelProperty(value="游戏订单号")
    private String gameOrderSerial;

    /**
     * 订单状态 0：预订单 1：未支付 2：支付成功 3：支付失败 4：异常订单 5：退款 6：操作中
     */
    @ApiModelProperty(value="订单状态 0：预订单 1：未支付 2：支付成功 3：支付失败 4：异常订单 5：退款 6：操作中")
    private Integer orderStatus;

    /**
     * 用户余额
     */
    @ApiModelProperty(value="用户余额")
    private Double userBalance;

    /**
     * vip等级
     */
    @ApiModelProperty(value="vip等级")
    private String vipLevel;

    /**
     * 角色等级
     */
    @ApiModelProperty(value="角色等级")
    private String roleLevel;

    /**
     * 工会，帮派
     */
    @ApiModelProperty(value="工会，帮派")
    private String gangsName;

    /**
     * 角色名称
     */
    @ApiModelProperty(value="角色名称")
    private String roleName;

    /**
     * 角色id
     */
    @ApiModelProperty(value="角色id")
    private String roleId;

    /**
     * 服务器ID
     */
    @ApiModelProperty(value="服务器ID")
    private String serverId;

    /**
     * 所在服务器
     */
    @ApiModelProperty(value="所在服务器")
    private String serverName;

    /**
     * 透传信息
     */
    @ApiModelProperty(value="透传信息")
    private String traInfo;

    /**
     * 订单支付时间
     */
    @ApiModelProperty(value="订单支付时间")
    private Date payTime;

    /**
     * 游戏回调状态 1待回调 2：回调成功 3：回调失败
     */
    @ApiModelProperty(value="游戏回调状态 1待回调 2：回调成功 3：回调失败")
    private Integer gameStatus;


	@ApiModelProperty(value = "剩余回调次数")
	private Integer noticeNum;

	@ApiModelProperty(value="发货时间")
	private Date deliverTime;
}
