package com.pig4cloud.pig.juhe.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 游戏渠道配置
 * @date 2021/9/7 9:57
 */
@Data
@ApiModel(value = "游戏渠道配置")
public class JuheGameChannelVo implements Serializable {

	@ApiModelProperty(value = "主键ID")
	private Long recordId;

	@ApiModelProperty(value = "渠道ID")
	private Long channelId;

	@ApiModelProperty(value = "游戏ID")
	private Long gameId;

	@ApiModelProperty(value = "渠道图标")
	private String iconPath;

	@ApiModelProperty(value = "隐私协议地址")
	private String privacyUrl;

	@ApiModelProperty(value = "游戏发货地址")
	private String payCallbackUrl;

	@ApiModelProperty(value = "隐私开关(1开 0关)")
	private Integer privacyOff;

	@ApiModelProperty(value = "渠道名称")
	private String channelName;

	@ApiModelProperty(value = "渠道SDK名称")
	private String channelSdkName;

	@ApiModelProperty(value = "游戏名称")
	private String gameName;

	@ApiModelProperty(value = "游戏APP_ID")
	private String appId;

	@ApiModelProperty(value = "游戏渠道属性配置集合")
	private List<JuheChannelAttrValueVo> attrValueList;
}
