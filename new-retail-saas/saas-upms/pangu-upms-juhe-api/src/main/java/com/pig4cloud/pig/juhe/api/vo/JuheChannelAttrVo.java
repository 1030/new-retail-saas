package com.pig4cloud.pig.juhe.api.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.pig4cloud.pig.juhe.api.entity.JuheChannelAttr;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 聚合渠道信息
 * @date 2021/9/3 16:50
 */
@Data
@ApiModel(value = "聚合渠道属性信息")
public class JuheChannelAttrVo implements Serializable {

	@ApiModelProperty(value = "主键ID")
	private Long channelId;

	@ApiModelProperty(value = "渠道名称")
	private String channelName;

	@ApiModelProperty(value = "SDK名称")
	private String channelSdkName;

	@ApiModelProperty(value = "SDK版本")
	private String channelVersion;

	@ApiModelProperty(value = "登录认证地址")
	private String loginAuthUrl;

	@ApiModelProperty(value = "支付回调地址")
	private String payBackUrl;

	@ApiModelProperty(value = "渠道下单地址")
	private String orderUrl;

	@ApiModelProperty(value = "渠道属性集合")
	private List<JuheChannelAttr> attrList;
}
