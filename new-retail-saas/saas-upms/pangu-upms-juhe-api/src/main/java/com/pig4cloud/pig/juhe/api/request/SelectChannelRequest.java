package com.pig4cloud.pig.juhe.api.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 查询渠道列表
 * @date 2021/9/7 14:28
 */
@Data
@ApiModel(value = "查询渠道列表")
public class SelectChannelRequest implements Serializable {

	@ApiModelProperty(value = "渠道SDK名称")
	private String channelSdkName;

}
