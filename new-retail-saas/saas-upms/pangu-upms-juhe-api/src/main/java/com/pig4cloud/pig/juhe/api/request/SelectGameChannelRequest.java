package com.pig4cloud.pig.juhe.api.request;

import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 查询游戏渠道配置列表
 * @date 2021/9/7 14:28
 */
@Data
@ApiModel(value = "查询游戏渠道配置列表")
public class SelectGameChannelRequest implements Serializable {

	@ApiModelProperty(value="渠道ID")
	private Long channelId;

	@ApiModelProperty(value="游戏ID")
	private Long gameId;

}
