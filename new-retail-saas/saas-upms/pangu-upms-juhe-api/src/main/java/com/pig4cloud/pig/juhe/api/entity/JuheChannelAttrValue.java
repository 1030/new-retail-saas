/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import com.pig4cloud.pig.juhe.api.groups.Insert;
import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 聚合渠道商属性值表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Data
@TableName("juhe_channel_attr_value")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合渠道商属性值表")
public class JuheChannelAttrValue extends BaseEntity {

	/**
	 * 记录ID
	 */
	@TableId(value = "record_id", type = IdType.AUTO)
	@ApiModelProperty(value = "记录ID")
	@NotNull(groups = Update.class, message = "记录ID不能为空")
	private Long recordId;

	/**
	 * 属性ID
	 */
	@ApiModelProperty(value = "属性ID")
	@NotNull(groups = Insert.class, message = "属性ID不能为空")
	private Long attrId;

	/**
	 * 渠道ID
	 */
	@ApiModelProperty(value = "渠道ID")
	@NotNull(groups = Insert.class, message = "渠道ID不能为空")
	private Long channelId;

	/**
	 * 游戏ID
	 */
	@ApiModelProperty(value = "游戏ID")
	@NotNull(groups = Insert.class, message = "游戏ID不能为空")
	private Long gameId;

	/**
	 * 属性值
	 */
	@ApiModelProperty(value = "属性值")
	@NotBlank(message = "{属性值不能为空}")
	private String attrValue;

	/**
	 * 是否删除(1是 0否)
	 */
	@ApiModelProperty(value="是否删除(1是 0否)")
	private Integer delFlag;
}
