package com.pig4cloud.pig.juhe.api.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 查询渠道列表
 * @date 2021/9/7 14:28
 */
@Data
@ApiModel(value = "查询游戏渠道列表")
public class SelectPackChannelRequest implements Serializable {

	@ApiModelProperty(value = "游戏ID")
	@NotNull(message = "游戏ID不能为空")
	private Long gameId;

	@ApiModelProperty(value = "渠道SDK名称")
	private String channelSdkName;

}
