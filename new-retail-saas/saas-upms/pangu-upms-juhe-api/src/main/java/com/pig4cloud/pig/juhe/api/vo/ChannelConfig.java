package com.pig4cloud.pig.juhe.api.vo;

import com.pig4cloud.pig.juhe.api.entity.JuheChannelAttr;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 聚合渠道信息
 * @date 2021/9/3 16:50
 */
@Data
@ApiModel(value = "打包工具渠道配置")
public class ChannelConfig implements Serializable {

	private String name;
	private String cname;
	private String channelid;
	private String sdkversion;
	private String updateversion;
	private String updatetime;

}
