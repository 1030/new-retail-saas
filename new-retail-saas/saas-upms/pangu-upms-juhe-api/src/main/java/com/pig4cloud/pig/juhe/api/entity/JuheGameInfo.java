/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 聚合游戏表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Data
@TableName("juhe_game_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合游戏表")
public class JuheGameInfo extends BaseEntity {

	/**
	 * 主键ID
	 */
	@TableId(value = "game_id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键ID")
	private Long gameId;

	/**
	 * 游戏名称
	 */
	@ApiModelProperty(value = "游戏名称")
	private String gameName;

	/**
	 * 聚合APP_ID
	 */
	@ApiModelProperty(value = "聚合APP_ID")
	private String appId;

	/**
	 * 聚合APP_KEY
	 */
	@ApiModelProperty(value = "聚合APP_KEY")
	private String appKey;

	/**
	 * 聚合APP_SECRET
	 */
	@ApiModelProperty(value = "聚合APP_SECRET")
	private String appSecret;

	/**
	 * 支付回调地址(发货地址)
	 */
	@ApiModelProperty(value = "支付回调地址(发货地址)")
	private String payNotifyUrl;

	/**
	 * 是否删除(1是 0否)
	 */
	@ApiModelProperty(value = "是否删除(1是 0否)")
	private Integer delFlag;

}
