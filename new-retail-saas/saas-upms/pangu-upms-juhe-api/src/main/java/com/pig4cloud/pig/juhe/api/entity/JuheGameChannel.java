/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.pig4cloud.pig.juhe.api.config.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 聚合游戏渠道关系表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Data
@TableName("juhe_game_channel")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "聚合游戏渠道关系表")
public class JuheGameChannel extends BaseEntity {

	/**
	 * 主键ID
	 */
	@TableId(value = "record_id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键ID")
	private Long recordId;

	/**
	 * 渠道ID
	 */
	@ApiModelProperty(value = "渠道ID")
	private Long channelId;

	/**
	 * 游戏ID
	 */
	@ApiModelProperty(value = "游戏ID")
	private Long gameId;

	/**
	 * 渠道图标
	 */
	@ApiModelProperty(value = "渠道图标")
	private String iconPath;

	/**
	 * 隐私协议地址
	 */
	@ApiModelProperty(value = "隐私协议地址")
	private String privacyUrl;

	/**
	 * 游戏发货地址
	 */
	@ApiModelProperty(value = "游戏发货地址")
	@TableField(updateStrategy = FieldStrategy.IGNORED)
	private String payCallbackUrl;

	/**
	 * 隐私开关(1开 0关)
	 */
	@ApiModelProperty(value = "隐私开关(1开 0关)")
	private Integer privacyOff;

	/**
	 * 是否删除(1是 0否)
	 */
	@ApiModelProperty(value = "是否删除(1是 0否)")
	private Integer delFlag;
}
