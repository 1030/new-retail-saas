package com.pig4cloud.pig.juhe.api.util;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: apk包信息实体
 * @date 2021/10/8 15:19
 */
@Data
public class ApkInfo implements Serializable {
	/**
	 * apk内部版本号
	 */
	private String versionCode = null;
	/**
	 * apk外部版本号
	 */
	private String versionName = null;
	/**
	 * apk的包名
	 */
	private String packageName = null;
	/**
	 * 支持的SDK版本。
	 */
	private String sdkVersion;
	/**
	 * 建议的SDK版本
	 */
	private String targetSdkVersion;
	/**
	 * 应用程序名
	 */
	private String applicationLable;
	/**
	 * 程序的图标。
	 */
	private String applicationIcon;
	/**
	 * 启动界面
	 */
	private String launchableActivity;

	private String applicationIcon120;
	private String applicationIcon160;
	private String applicationIcon240;
	private String applicationIcon320;
	private String applicationIcon480;
	private String applicationIcon640;

	public String getSuitableApplicationIcon() {
		if (StringUtils.isNotBlank(applicationIcon640)) {
			return applicationIcon640;
		} else if (StringUtils.isNotBlank(applicationIcon480)) {
			return applicationIcon480;
		} else if (StringUtils.isNotBlank(applicationIcon320)) {
			return applicationIcon320;
		} else if (StringUtils.isNotBlank(applicationIcon240)) {
			return applicationIcon240;
		} else if (StringUtils.isNotBlank(applicationIcon160)) {
			return applicationIcon160;
		} else if (StringUtils.isNotBlank(applicationIcon120)) {
			return applicationIcon120;
		} else {
			return applicationIcon;
		}
	}
}
