package com.pig4cloud.pig.juhe.api.util;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 打包工具定时任务
 * @date 2021/9/28 19:07
 */
@Data
public class TaskInfo implements Serializable {

	/**
	 * 记录ID
	 */
	private Long recordId;

	/**
	 * 1读包 2分包
	 */
	private Integer jobType;

}
