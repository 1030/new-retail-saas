package com.pig4cloud.pig.juhe.api.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.UUID;

/**
 * @author yuwenfeng
 * @description: 游戏APPID和KEY生成工具栏
 * @date 2021/9/6 15:12
 */
public class AppUtils {

	//生成 app_secret 密钥
	private final static String SERVER_NAME = "panggu_juhe";
	private final static String[] chars = new String[]{"a", "b", "c", "d", "e", "f",
			"g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
			"t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
			"J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
			"W", "X", "Y", "Z"};

	public static String getAppId() {
		StringBuffer shortBuffer = new StringBuffer();
		String uuid = UUID.randomUUID().toString().replace("-", "");
		for (int i = 0; i < 8; i++) {
			String str = uuid.substring(i * 4, i * 4 + 4);
			int x = Integer.parseInt(str, 16);
			shortBuffer.append(chars[x % 0x3E]);
		}
		return shortBuffer.toString();

	}

	public static String getAppSecret(String appId) {
		try {
			String[] array = new String[]{appId, SERVER_NAME};
			StringBuffer sb = new StringBuffer();
			// 字符串排序
			Arrays.sort(array);
			for (int i = 0; i < array.length; i++) {
				sb.append(array[i]);
			}
			String str = sb.toString();
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(str.getBytes());
			byte[] digest = md.digest();

			StringBuffer hexstr = new StringBuffer();
			String shaHex = "";
			for (int i = 0; i < digest.length; i++) {
				shaHex = Integer.toHexString(digest[i] & 0xFF);
				if (shaHex.length() < 2) {
					hexstr.append(0);
				}
				hexstr.append(shaHex);
			}
			return hexstr.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	public static void main(String[] args) {
		String appId = getAppId();
		System.out.println(appId);
		/*String appSecret = getAppSecret(appId);
		System.out.println("appId: "+appId);
		System.out.println("appSecret: "+appSecret);
		String path="http://wqwasd.com/auto/backup/zipfile/res/drawable-hdpi/ic_launcher.png";
		System.out.println(path.substring(path.lastIndexOf("/")+1,path.length()));*/
	}
}
