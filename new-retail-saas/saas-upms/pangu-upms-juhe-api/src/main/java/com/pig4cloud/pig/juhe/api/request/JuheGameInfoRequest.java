package com.pig4cloud.pig.juhe.api.request;

import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author yuwenfeng
 * @description: 聚合游戏，新增、编辑
 * @date 2021/9/6 14:21
 */
@Data
@ApiModel(value = "聚合游戏")
public class JuheGameInfoRequest implements Serializable {

	@ApiModelProperty(value = "游戏ID")
	@NotNull(groups = Update.class, message = "游戏ID不能为空")
	private Long gameId;

	@ApiModelProperty(value = "游戏名称")
	@NotBlank(message = "游戏名称不能为空")
	@Size(max = 50, message = "游戏名称长度不合法")
	private String gameName;

	@ApiModelProperty(value = "支付回调地址(发货地址)")
	@NotBlank(message = "回调地址不能为空")
	@Size(max = 255, message = "回调地址长度不合法")
	@Pattern(regexp = "^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+$", message = "发货地址不合法")
	private String payNotifyUrl;

}
