package com.pig4cloud.pig.juhe.api.request;

import com.pig4cloud.pig.juhe.api.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author yuwenfeng
 * @description: 安卓渠道打包
 * @date 2021/9/7 14:28
 */
@Data
@ApiModel(value = "聚合渠道打包")
public class JuhePackageRequest implements Serializable {

	@ApiModelProperty(value="母包ID")
	@NotNull(message = "母包ID不能为空")
	private Long pageId;

	@ApiModelProperty(value = "渠道ID")
	@NotBlank(message = "渠道ID串不能为空，以逗号隔开")
	private String channelIds;

	@ApiModelProperty(value = "游戏ID")
	@NotNull(message = "游戏ID不能为空")
	private Long gameId;


}
