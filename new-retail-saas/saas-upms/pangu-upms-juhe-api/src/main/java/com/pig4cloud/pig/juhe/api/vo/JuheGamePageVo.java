package com.pig4cloud.pig.juhe.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yuwenfeng
 * @description: 聚合游戏母包
 * @date 2021/9/8 14:06
 */
@Data
@ApiModel(value = "聚合游戏母包")
public class JuheGamePageVo implements Serializable {

	@ApiModelProperty(value = "母包ID")
	private Long pageId;

	@ApiModelProperty(value = "游戏ID")
	private Long gameId;

	@ApiModelProperty(value = "包名")
	private String pageName;

	@ApiModelProperty(value = "版本号")
	private String pageVersion;

	@ApiModelProperty(value = "更新说明")
	private String pageContext;

	@ApiModelProperty(value = "母包地址")
	private String pageUrl;

	@ApiModelProperty(value = "游戏名称")
	private String gameName;

	@ApiModelProperty(value = "创建者")
	private String createBy;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value="状态(1解析中 2解析成功 3解析失败)")
	private Integer pageStatus;
}
