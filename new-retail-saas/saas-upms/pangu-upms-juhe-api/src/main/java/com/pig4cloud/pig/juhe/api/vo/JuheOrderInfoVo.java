package com.pig4cloud.pig.juhe.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yuwenfeng
 * @description: 聚合订单信息
 * @date 2021/9/3 17:09
 */
@Data
@ApiModel(value = "聚合订单信息")
public class JuheOrderInfoVo implements Serializable {

	@ApiModelProperty(value="聚合订单id")
	private Long orderId;

	@ApiModelProperty(value="订单类型 1商品 2计费点")
	private Integer orderType;

	@ApiModelProperty(value="渠道ID")
	private Long channelId;

	@ApiModelProperty(value="游戏ID")
	private Long gameId;

	@ApiModelProperty(value="订单流水号")
	private String orderSerial;

	@ApiModelProperty(value="聚合用户ID")
	private Long openId;

	@ApiModelProperty(value="订单付费金额")
	private Long orderFee;

	@ApiModelProperty(value="商品编码")
	private String goodsCode;

	@ApiModelProperty(value="商品名称")
	private String goodsName;

	@ApiModelProperty(value="商品描述")
	private String goodsDesc;

	@ApiModelProperty(value="游戏订单号")
	private String gameOrderSerial;

	@ApiModelProperty(value="渠道订单号")
	private String channelOrderSerial;

	@ApiModelProperty(value="订单状态 0：预订单 1：未支付 2：支付成功 3：支付失败 4：异常订单 5：退款 6：操作中")
	private Integer orderStatus;

	@ApiModelProperty(value="用户余额")
	private Double userBalance;

	@ApiModelProperty(value="vip等级")
	private String vipLevel;

	@ApiModelProperty(value="角色等级")
	private String roleLevel;

	@ApiModelProperty(value="工会，帮派")
	private String gangsName;

	@ApiModelProperty(value="角色名称")
	private String roleName;

	@ApiModelProperty(value="角色id")
	private String roleId;

	@ApiModelProperty(value="服务器ID")
	private String serverId;

	@ApiModelProperty(value="所在服务器")
	private String serverName;

	@ApiModelProperty(value="透传信息")
	private String traInfo;

	@ApiModelProperty(value="订单支付时间")
	private Date payTime;

	@ApiModelProperty(value="订单创建时间")
	private Date createTime;

	@ApiModelProperty(value="游戏回调状态 1待回调 2：回调成功 3：回调失败")
	private Integer gameStatus;

	@ApiModelProperty(value="发货时间")
	private Date deliverTime;

	@ApiModelProperty(value = "剩余回调次数")
	private Integer noticeNum;

	@ApiModelProperty(value="渠道名称")
	private String channelName;

	@ApiModelProperty(value="游戏名称")
	private String gameName;
}
