/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.admin.api.feign;

import com.pig4cloud.pig.admin.api.entity.SysDeptGroup;
import com.pig4cloud.pig.admin.api.feign.factory.RemoteDeptGroupServiceFallbackFactory;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/28
 */
@FeignClient(contextId = "remoteDeptGroupService", value = ServiceNameConstants.UPMS_SERVICE, fallbackFactory = RemoteDeptGroupServiceFallbackFactory.class)
public interface RemoteDeptGroupService {


	@GetMapping("/sysdeptgroup/getByIds")
	R<List<SysDeptGroup>> getByIds(@RequestHeader(SecurityConstants.FROM) String from,  @RequestBody List<Integer> ids);


}
