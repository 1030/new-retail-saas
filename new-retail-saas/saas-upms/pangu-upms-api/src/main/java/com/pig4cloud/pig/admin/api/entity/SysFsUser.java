package com.pig4cloud.pig.admin.api.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: sys_fs_user
 * @Author: hma
 * @Date:   2023-03-21
 * @Version: V1.0
 */
@Data
@TableName("sys_fs_user")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="sys_fs_user对象", description="sys_fs_user")
public class SysFsUser  extends Model<SysFile>  implements Serializable{

	/**主键id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "主键id")
    private Integer id;
	/**用户ID*/
    @ApiModelProperty(value = "用户ID")
    private Integer userId;
	/**租户内用户的唯一标识，ID值与查询参数中的user_id_type 对应。*/
    @ApiModelProperty(value = "租户内用户的唯一标识，ID值与查询参数中的user_id_type 对应。")
    private String fsUserId;
	/**用户的open_id*/
    @ApiModelProperty(value = "用户的open_id")
    private String openId;
	/**用户名*/
    @ApiModelProperty(value = "用户名")
    private String realName;
	/**是否删除  0否 1是*/
    @ApiModelProperty(value = "是否删除  0否 1是")
    private Integer deleted;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**修改时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private Integer createId;
	/**修改人*/
    @ApiModelProperty(value = "修改人")
    private Integer updateId;
}
