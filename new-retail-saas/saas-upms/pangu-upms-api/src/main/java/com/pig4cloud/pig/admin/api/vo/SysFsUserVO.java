package com.pig4cloud.pig.admin.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 系统飞书用户
 * @Author: hma
 * @Date:   2023-03-17
 * @Version: V1.0
 */
@Data
@ApiModel(value="飞书系统用户vo", description="飞书系统用户vo")
public class SysFsUserVO implements Serializable {

	private static final long serialVersionUID = -4715460048201218070L;

	/**用户ID*/
    @ApiModelProperty(value = "用户ID")
    private Integer userId;
	/**租户内用户的唯一标识，ID值与查询参数中的user_id_type 对应。*/
    @ApiModelProperty(value = "租户内用户的唯一标识，ID值与查询参数中的user_id_type 对应。")
    private String fsUserId;

	/**用户名*/
    @ApiModelProperty(value = "用户名")
    private String realName;

}
