/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.admin.api.dto;

import com.pig4cloud.pig.admin.api.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author lengleng
 * @date 2017/11/5
 */
@Data
@ApiModel(value = "系统用户传输对象")
@EqualsAndHashCode(callSuper = true)
public class UserDTO extends SysUser {
	/**
	 * 角色ID
	 */
	@ApiModelProperty(value = "角色id集合")
	private List<Integer> role;
	/**
	 * 部门id
	 */
	@ApiModelProperty(value = "部门id")
	private Integer deptId;
	/**
	 * 新密码
	 */
	@ApiModelProperty(value = "新密码")
	private String newpassword1;


	/**
	 * 确认密码
	 */
	@ApiModelProperty(value = "确认密码")
	private String newpassword2;
	/**
	 * 状态 ：0 未激活 ， 1 已激活
	 */
	@ApiModelProperty(value = "状态")
	private Integer tenantStatus;

	/**
	 * 排序字段
	 */
	@ApiModelProperty(value = "排序字段")
	private String orderColumn;

	/**
	 * 排序顺序，asc-升序；desc-降序；
	 */
	@ApiModelProperty(value = "排序字段")
	private String orderAsc;


	@ApiModelProperty(value = "租户范畴：1 非CPS租户   2    CPS租户")
	private Integer tenantScope;

	@ApiModelProperty(value = "CPS租户id")
	private Integer cpsTenantId;

}
