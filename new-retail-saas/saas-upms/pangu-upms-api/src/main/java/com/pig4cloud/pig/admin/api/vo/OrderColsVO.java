package com.pig4cloud.pig.admin.api.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author sunyq
 * @date 2022/9/2 13:48
 */
@Data
public class OrderColsVO implements Serializable {
	/**
	 * 表头别名
	 */
	@NotBlank(message = "表头别名不能为空")
	private String alias;

	/**
	 * 表头隐藏列，以逗号隔开
	 */
	private String orderCols;

}
