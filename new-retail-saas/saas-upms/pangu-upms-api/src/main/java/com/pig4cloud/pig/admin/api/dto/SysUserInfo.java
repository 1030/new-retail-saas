package com.pig4cloud.pig.admin.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author chengang
 * @Date 2021/10/11
 */
@Data
public class SysUserInfo implements Serializable {

	private Integer userId;
	private String userName;
	private Integer deptId;
	private String deptName;
	private Integer deptGroupId;
	private String deptGroupName;

}
