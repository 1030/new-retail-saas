/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.admin.api.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author zhuxm
 * @date 2021/3/1
 */
@Data
public class HideColsPlanAttrVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 表头别名
	 */
	@NotBlank(message = "查询类别不能为空")
	private String queryColumn;
	/**
	 * 渠道类别
	 */
	@NotBlank(message = "渠道类型不能为空")
	private String ctype;
	/**
	 * 表头隐藏列，以逗号隔开
	 */
	private String hideCols;

}
