package com.pig4cloud.pig.admin.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author cx
 * @version 1.0.0
 * @ClassName SysTenant.java
 * @createTime 2021年09月15日 15:02:00
 */
@Data
@ApiModel(value = "租户")
@TableName("sys_tenant")
public class SysTenant  extends Model<SysTenant> {
	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value="主键")
	private Integer id;
	/**
	 * 名称
	 */
	@ApiModelProperty(value="名称")
	private String name;
	/**
	 * 编码
	 */
	@ApiModelProperty(value="编码")
	private String code;
	/**
	 * 默认首页地址
	 */
	@ApiModelProperty(value="默认首页地址")
	private String indexUrl;
	/**
	 * 开始时间
	 */
	@ApiModelProperty(value="开始时间")
	private Date startTime;
	/**
	 * 结束时间
	 */
	@ApiModelProperty(value="结束时间")
	private Date endTime;
	/**
	 * 状态
	 */
	@ApiModelProperty(value="状态")
	private Integer status;
	/**
	 * 删除标识
	 */
	@ApiModelProperty(value="删除标识")
	private Integer delFlag;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value="创建时间")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value="修改时间")
	private Date updateTime;

	@TableField(exist = false)
	@ApiModelProperty(value="是否选中，登录外选中最新的租户")
	private Boolean selected = false;
}
