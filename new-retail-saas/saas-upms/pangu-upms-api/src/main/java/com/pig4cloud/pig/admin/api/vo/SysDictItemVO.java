package com.pig4cloud.pig.admin.api.vo;

import com.pig4cloud.pig.admin.api.entity.SysDictItem;
import lombok.Data;

import java.util.List;


/**
 * 字典项VO
 *
 * @author lengleng
 * @date 2019/03/19
 */
@Data
public class SysDictItemVO extends SysDictItem {

	private static final long serialVersionUID = 1L;
	private List<SysDictItem> children;



}
