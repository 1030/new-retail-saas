package com.pig4cloud.pig.admin.api.vo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
public class SysTabColField implements Serializable {

	private static final long serialVersionUID = -6959843837641947018L;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String slot;

	private JSONObject attrs;

	private JSONArray operations;
}
