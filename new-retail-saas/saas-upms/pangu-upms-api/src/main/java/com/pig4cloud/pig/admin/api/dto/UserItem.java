package com.pig4cloud.pig.admin.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description
 * @Author chengang
 * @Date 2021/9/30
 */
@Data
public class UserItem implements Serializable {

	private static final long serialVersionUID = -7764068721937289101L;

	private Integer id;
	private String name;

	private List<UserItem> userList;


	public UserItem(Integer id, String name){
		this.id = id;
		this.name = name;
	}

}
