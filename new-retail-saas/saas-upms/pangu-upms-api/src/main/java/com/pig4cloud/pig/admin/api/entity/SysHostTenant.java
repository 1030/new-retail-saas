package com.pig4cloud.pig.admin.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 *
 */
@Data
@ApiModel(value = "域名租户匹配表")
@TableName("sys_host_tenant")
public class SysHostTenant extends Model<SysHostTenant> {
	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value="主键")
	private Integer id;
	/**
	 * 域名
	 */
	@ApiModelProperty(value="域名")
	private String host;

	/**
	 * 域名对应的租户列表，以,隔开
	 */
	@ApiModelProperty(value="域名对应的租户列表，以,隔开")
	@TableField(value = "tenant_id")
	private String tenantId;

	/**
	 * 删除标识
	 */
	@ApiModelProperty(value="删除标识")
	@TableLogic
	private Integer delFlag;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value="创建时间")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@ApiModelProperty(value="修改时间")
	private Date updateTime;
}
