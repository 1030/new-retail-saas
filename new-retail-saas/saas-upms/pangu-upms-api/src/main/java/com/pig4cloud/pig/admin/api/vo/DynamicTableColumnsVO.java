package com.pig4cloud.pig.admin.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author sunyq
 * @date 2022/9/2 13:41
 */
@Data
public class DynamicTableColumnsVO implements Serializable {
	/**
	 * 基础列信息
	 */
	private List<SysTabColField> baseTabColFiledList;
	/**
	 * 排序列信息
	 */
	private List<SysTabColField> orderTabColFiledList;
}
