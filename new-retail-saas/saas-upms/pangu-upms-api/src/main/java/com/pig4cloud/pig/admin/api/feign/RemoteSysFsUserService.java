/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.admin.api.feign;

import com.pig4cloud.pig.admin.api.feign.factory.RemoteSysFsUserServiceFallbackFactory;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Map;

/**
 * @author lengleng
 * @date 2018/6/28
 */
@FeignClient(contextId = "remoteSysFsUserService", value = ServiceNameConstants.UPMS_SERVICE, fallbackFactory = RemoteSysFsUserServiceFallbackFactory.class)
public interface RemoteSysFsUserService {
	/**
	 * 根据用户列表查询绑定的的广告账号列表
	 *
	 * @param
	 * @return
	 */
	@GetMapping("/fs/getSysFsUserList")
	R<List<Map<String,Object>>> getSysFsUserList(@RequestHeader(SecurityConstants.FROM) String from);



}
