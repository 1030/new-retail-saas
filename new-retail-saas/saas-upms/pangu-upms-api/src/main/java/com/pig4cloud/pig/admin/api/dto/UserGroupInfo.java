package com.pig4cloud.pig.admin.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description
 * @Author chengang
 * @Date 2021/10/11
 */
@Data
public class UserGroupInfo implements Serializable {

	private static final long serialVersionUID = 718109405853084132L;

	/**
	 * 是否为设计主管  1：是  0：不是
	 */
	private Integer isManager = 0;

	private List<UserItem> list;

}
