/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.admin.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 动态表头
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="sys_tab_col")
public class SysTabCol extends Model<SysTabCol> {

	private static final long serialVersionUID = 1L;

	/**
	 * 菜单ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "表头id")
	private Integer id;

	/**
	 * 菜单名称
	 */
	@ApiModelProperty(value = "表头名称")
	private String name;

	/**
	 * 菜单权限标识
	 */
	@ApiModelProperty(value = "表头标识")
	private String alias;

	/**
	 * 表头内容
	 */
	@ApiModelProperty(value = "表头内容")
	private String content;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;

	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "更新时间")
	private LocalDateTime updateTime;

	/**
	 * 0--正常 1--删除
	 */
	@TableLogic
	private String delFlag;

	/**
	 * 环境标识
	 */
	@ApiModelProperty(value = "环境标识")
	private String env;

}
