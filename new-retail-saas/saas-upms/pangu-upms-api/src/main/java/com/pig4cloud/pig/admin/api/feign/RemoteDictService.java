/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.admin.api.feign;

import com.pig4cloud.pig.admin.api.entity.SysDictItem;
import com.pig4cloud.pig.admin.api.feign.factory.RemoteDictServiceFallbackFactory;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/22
 */
@FeignClient(contextId = "remoteDictService", value = ServiceNameConstants.UPMS_SERVICE, fallbackFactory = RemoteDictServiceFallbackFactory.class)
public interface RemoteDictService {

	/**
	 * 通过字典类型查找字典
	 *
	 * @param type 类型
	 * @return 同类型字典
	 */
	@GetMapping("/dict/typeInner/{type}")
	R<List<SysDictItem>> getDictByType4Inner(@RequestHeader(SecurityConstants.FROM) String from, @PathVariable(value = "type") String type);


	/**
	 * 通过字典类型查找字典
	 * 定时器调用没有租户和token
	 *
	 * @param type 类型
	 * @return 同类型字典
	 */
	@GetMapping("/dict/typeInner/{type}")
	R<List<SysDictItem>> getDictByType4Inner(@RequestHeader(SecurityConstants.TENANT_ID) Integer tenantId,@RequestHeader(SecurityConstants.FROM) String from, @PathVariable(value = "type") String type);


}
