/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.pig4cloud.pig.admin.api.feign;

import com.pig4cloud.pig.admin.api.dto.SysUserInfo;
import com.pig4cloud.pig.admin.api.dto.UserInfo;
import com.pig4cloud.pig.admin.api.entity.SysUser;
import com.pig4cloud.pig.admin.api.entity.SysUserTenant;
import com.pig4cloud.pig.admin.api.feign.factory.RemoteUserServiceFallbackFactory;
import com.pig4cloud.pig.admin.api.vo.UserSelectVO;
import com.pig4cloud.pig.admin.api.vo.UserVO;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/22
 */
@FeignClient(contextId = "remoteUserService", value = ServiceNameConstants.UPMS_SERVICE,fallbackFactory = RemoteUserServiceFallbackFactory.class)
public interface RemoteUserService {
	/**
	 * 通过用户名查询用户、角色信息
	 *
	 * @param username 用户名
	 * @param from     调用标志
	 * @return R
	 */
	@GetMapping("/user/info/{username}")
	R<UserInfo> info(@PathVariable("username") String username
			, @RequestHeader(SecurityConstants.FROM) String from);


	/**
	 * 根据用户id查询用户信息
	 *
	 * @param userId
	 * @param from
	 * @return
	 */
	@GetMapping("/user/findUserByUserId/{userId}")
	R<UserInfo> findUserByUserId(@PathVariable("userId") Integer userId, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 通过社交账号或手机号查询用户、角色信息
	 *
	 * @param inStr appid@code
	 * @param from  调用标志
	 * @return
	 */
	@GetMapping("/social/info/{inStr}")
	R<UserInfo> social(@PathVariable("inStr") String inStr
			, @RequestHeader(SecurityConstants.FROM) String from);

	/**
	 * 查询上级部门的用户信息
	 *
	 * @param username 用户名
	 * @return R
	 */
	@GetMapping("/user/ancestor/{username}")
	R<List<SysUser>> ancestorUsers(@PathVariable("username") String username);

	/**
	 * 所有用户信息
	 *
	 * @return
	 */
	@GetMapping("/user/getUserList")
	R<List<SysUser>> getUserList(@RequestHeader(SecurityConstants.FROM) String from);


	/**
	 * 所有用户信息--不带删除的
	 *
	 * @return
	 */
	@GetMapping("/user/getUserSelectList")
	R<List<UserSelectVO>> getUserSelectList();

	/**
	 * 所有用户信息--带删除的
	 *
	 * @return
	 */
	@GetMapping("/user/getUserSelectList4All")
	R<List<UserSelectVO>> getUserSelectList4All();

	/**
	 * 根据用户ID集合获取用户信息
	 *
	 * @return
	 */
	@PostMapping("/user/getUserListByUserIds")
	R<List<SysUser>> getUserListByUserIds(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody List<Integer> ids);


	/**
	 * 根据用户ID集合获取用户信息-含部门和部门组别信息
	 *
	 * @return
	 */
	@PostMapping("/user/getUserInfoByIds")
	R<List<SysUserInfo>> getUserInfoByIds(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody List<Integer> ids);


	/**
	 * 根据用户ID集合获取用户信息
	 *
	 * @return
	 */
	@PostMapping("/user/getDeptIdByIds")
	R<List<Integer>> getDeptIdByIds(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody List<Integer> ids);

	/**
	 * 获取组别下的所有的用户
	 *
	 * @return
	 */
	@PostMapping("/user/getUserListByGroupId")
	R<List<SysUserTenant>> getUserListByGroupId(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody Integer deptGroupId);

	/**
	 * 获取组别下的所有的用户 - 带租户ID -- xxlJob执行用
	 *
	 * @return
	 */
	@PostMapping("/user/getUserListByGroupIdAndTId")
	R<List<SysUserTenant>> getUserListByGroupIdAndTId(@RequestHeader(SecurityConstants.FROM) String from,
													  @RequestParam(value = "deptGroupId") Integer deptGroupId,
													  @RequestParam(value = "tenantId") Integer tenantId);

	/**
	 * 根据用户ID列表获取用户姓名
	 *
	 * @param from
	 * @param userIds
	 * @return
	 */
	@PostMapping("/user/getUserIdAndNameByIds")
	R<List<SysUser>> getUserIdAndNameByIds(@RequestHeader(SecurityConstants.FROM) String from, @RequestBody List<Integer> userIds);

	/**
	 * 查询租户下的所有用户
	 * @param from
	 * @param tenantId
	 * @return
	 */
	@GetMapping("/user/getUserSelectListByTenantId")
	R<List<SysUser>> getUserSelectListByTenantId(@RequestHeader(SecurityConstants.FROM) String from, @RequestParam(value = "tenantId") Integer tenantId);
}
