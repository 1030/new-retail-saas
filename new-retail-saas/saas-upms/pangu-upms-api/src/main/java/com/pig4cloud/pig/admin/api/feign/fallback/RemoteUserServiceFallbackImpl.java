/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.admin.api.feign.fallback;

import com.pig4cloud.pig.admin.api.dto.SysUserInfo;
import com.pig4cloud.pig.admin.api.dto.UserInfo;
import com.pig4cloud.pig.admin.api.entity.SysUser;
import com.pig4cloud.pig.admin.api.entity.SysUserTenant;
import com.pig4cloud.pig.admin.api.feign.RemoteUserService;
import com.pig4cloud.pig.admin.api.vo.UserSelectVO;
import com.pig4cloud.pig.admin.api.vo.UserVO;
import com.pig4cloud.pig.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * @author lengleng
 * @date 2019/2/1
 */
@Slf4j
@Component
public class RemoteUserServiceFallbackImpl implements RemoteUserService {

	@Setter
	private Throwable cause;

	@Override
	public R<UserInfo> info(String username, String from) {
		return null;
	}

	@Override
	public R<UserInfo> findUserByUserId(Integer userId, String from) {
		return null;
	}

	@Override
	public R<UserInfo> social(String inStr, String from) {
		return null;
	}

	@Override
	public R<List<SysUser>> ancestorUsers(String username) {
		return null;
	}

	@Override
	public R<List<SysUser>> getUserList(String from) {
		return null;
	}

	@Override
	public R<List<UserSelectVO>> getUserSelectList() {
		return null;
	}

	@Override
	public R<List<UserSelectVO>> getUserSelectList4All() {
		cause.printStackTrace();
		return R.ok();
	}

	@Override
	public R<List<SysUser>> getUserListByUserIds(String from, List<Integer> ids) {
		return R.ok();
	}

	@Override
	public R<List<SysUserInfo>> getUserInfoByIds(String from, List<Integer> ids) {
		return R.ok();
	}

	@Override
	public R<List<Integer>> getDeptIdByIds(String from, List<Integer> ids) {
		log.error("服务已熔断");
		return R.ok();
	}

	@Override
	public R<List<SysUserTenant>> getUserListByGroupId(String from, Integer deptGroupId) {
		return R.ok();
	}

	@Override
	public R<List<SysUserTenant>> getUserListByGroupIdAndTId(String from, Integer deptGroupId, Integer tenantId) {
		return R.ok();
	}

	@Override
	public R<List<SysUser>> getUserIdAndNameByIds(String from, List<Integer> userIds) {
		log.error("服务已熔断", cause);
		return R.ok(Collections.emptyList());
	}

	@Override
	public R getUserSelectListByTenantId(String from, Integer tenantId) {
		cause.printStackTrace();
		return R.ok();
	}


}
