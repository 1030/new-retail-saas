/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.admin.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 字典项
 *
 * @author lengleng
 * @date 2019/03/19
 */
@Data
@ApiModel(value = "字典项")
public class SysDictItemAll implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 编号
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "字典项id")
	private Integer id;
	/**
	 * 所属字典类id
	 */
	@ApiModelProperty(value = "所属字典类id")
	private Integer dictId;
	/**
	 * 数据值
	 */
	@ApiModelProperty(value = "数据值")
	private String value;
	/**
	 * 标签名
	 */
	@ApiModelProperty(value = "标签名")
	private String label;
	/**
	 * 类型
	 */
	@ApiModelProperty(value = "类型")
	private String type;
	/**
	 * 描述
	 */
	@ApiModelProperty(value = "描述")
	private String description;
	/**
	 * 排序（升序）
	 */
	@ApiModelProperty(value = "排序值，默认升序")
	private Integer sort;

}
