package com.pig4cloud.pig.admin.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户租户表
 * @author  chengang
 * @version  2021-09-15 19:28:51
 * table: sys_user_tenant
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_user_tenant")
public class SysUserTenant extends Model<SysUserTenant>{

	/**
	 * 用户ID
	 */
	@TableField(value = "user_id")
	private Integer userId;

	/**
	 * 所属租户
	 */
	@TableField(value = "tenant_id")
	private Integer tenantId;

	/**
	 * 部门ID
	 */
	@TableField(value = "dept_id")
	private Integer deptId;

	/**
	 * 部门组别
	 */
	@TableField(value = "dept_group_id")
	private Integer deptGroupId;

	/**
	 * 项目状态
	 */
	@TableField(value = "status")
	private Integer status;

}

	

	
	

