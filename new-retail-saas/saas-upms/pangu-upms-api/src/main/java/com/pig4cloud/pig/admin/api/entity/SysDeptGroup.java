/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.admin.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.time.LocalDateTime;

/**
 *
 *
 * @author pigx code generator
 * @date 2021-07-08 13:49:11
 */
@Data
@TableName("sys_dept_group")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "")
public class SysDeptGroup extends Model<SysDeptGroup> {
private static final long serialVersionUID = 1L;

    /**
     *
     */
	@TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="")
    private Integer id;
    /**
     * 组别名称
     */
    @ApiModelProperty(value="组别名称")
		private String name;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value="修改时间")
    private LocalDateTime updateTime;
    /**
     * 创建人id
     */
    @ApiModelProperty(value="创建人id")
    private Integer createUserId;
    /**
     * 更新人ID
     */
    @ApiModelProperty(value="更新人ID")
    private Integer updateUserId;
    /**
     * 部门ID
     */
    @ApiModelProperty(value="部门ID")
    private Integer deptId;
    }
