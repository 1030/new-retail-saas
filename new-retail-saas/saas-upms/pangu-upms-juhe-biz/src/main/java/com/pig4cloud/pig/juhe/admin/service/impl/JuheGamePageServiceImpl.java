/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.admin.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.admin.api.dto.UserInfo;
import com.pig4cloud.pig.admin.api.entity.SysUser;
import com.pig4cloud.pig.admin.api.feign.RemoteUserService;
import com.pig4cloud.pig.common.core.constant.SecurityConstants;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import com.pig4cloud.pig.juhe.admin.mapper.JuheGameInfoMapper;
import com.pig4cloud.pig.juhe.admin.mapper.JuheGamePageMapper;
import com.pig4cloud.pig.juhe.admin.service.AsyncService;
import com.pig4cloud.pig.juhe.api.entity.JuheGameInfo;
import com.pig4cloud.pig.juhe.api.entity.JuheGamePage;
import com.pig4cloud.pig.juhe.admin.service.JuheGamePageService;
import com.pig4cloud.pig.juhe.api.request.JuheGamePageRequest;
import com.pig4cloud.pig.juhe.api.util.Constant;
import com.pig4cloud.pig.juhe.api.vo.JuheGamePageVo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 聚合游戏母包表
 *
 * @author yuwenfeng
 * @date 2021-09-08 14:00:01
 */
@Slf4j
@Service
@AllArgsConstructor
public class JuheGamePageServiceImpl extends ServiceImpl<JuheGamePageMapper, JuheGamePage> implements JuheGamePageService {

	private final JuheGameInfoMapper juheGameInfoMapper;

	private final RemoteUserService remoteUserService;

	private final AsyncService asyncService;

	@Override
	public Page<JuheGamePageVo> selectJuheGamePageVoPage(Page<JuheGamePage> page, QueryWrapper<JuheGamePage> query) {
		return baseMapper.selectJuheGamePageVoPage(page, query);
	}

	@Override
	public R addGamePage(JuheGamePageRequest juheGamePage) {
		SysUser sysUser = null;
		R<UserInfo> userInfo = remoteUserService.findUserByUserId(SecurityUtils.getUser().getId(), SecurityConstants.FROM_IN);
		if (null != userInfo.getData()) {
			sysUser = userInfo.getData().getSysUser();
		}
		if (null == sysUser) {
			return R.failed("无法获取登录账号信息");
		}
		JuheGameInfo gameInfo = juheGameInfoMapper.selectById(juheGamePage.getGameId());
		if (null == gameInfo) {
			return R.failed("游戏不存在");
		}
		//根据路径地址解析母包
		String extension = juheGamePage.getPageUrl().substring(juheGamePage.getPageUrl().lastIndexOf(StrUtil.DOT) + 1);
		if (!Constant.APK.equalsIgnoreCase(extension)) {
			return R.failed("文件不合法,必须为" + Constant.APK + "文件");
		}
		JuheGamePage info = JSON.parseObject(JSON.toJSONString(juheGamePage), JuheGamePage.class);
		info.setCreateBy(sysUser.getRealName());
		info.setPageStatus(Constant.PAGE_STATUS_WAIT);
		int statusNum = baseMapper.insert(info);
		if (statusNum > 0) {
			asyncService.fetchApkInfo(info.getPageId());
			return R.ok();
		} else {
			return R.failed("新增操作异常");
		}
	}

	@Override
	public R<List<JuheGamePage>> getPackageListByGameId(Long gameId) {
		return R.ok(baseMapper.selectList(new QueryWrapper<JuheGamePage>().eq("game_id", gameId).eq("page_status", Constant.PAGE_STATUS_SUCC).orderByDesc("create_time")));
	}
}
