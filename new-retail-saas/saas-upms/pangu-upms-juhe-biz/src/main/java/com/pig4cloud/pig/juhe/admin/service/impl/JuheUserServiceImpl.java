/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.juhe.api.entity.JuheUser;
import com.pig4cloud.pig.juhe.admin.mapper.JuheUserMapper;
import com.pig4cloud.pig.juhe.admin.service.JuheUserService;
import com.pig4cloud.pig.juhe.api.vo.JuheUserVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 聚合用户表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Service
@AllArgsConstructor
public class JuheUserServiceImpl extends ServiceImpl<JuheUserMapper, JuheUser> implements JuheUserService {

	@Override
	public Page<JuheUserVo> getJuheUserVoPage(Page<JuheUser> page, QueryWrapper<JuheUser> queryWrapper) {
		return baseMapper.selectJuheUserVoPage(page,queryWrapper);
	}
}
