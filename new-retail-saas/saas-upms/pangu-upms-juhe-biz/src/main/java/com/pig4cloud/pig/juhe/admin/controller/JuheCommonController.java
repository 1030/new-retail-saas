package com.pig4cloud.pig.juhe.admin.controller;

import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.juhe.admin.service.JuhePackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author yuwenfeng
 * @description: 通用接口
 * @date 2021/9/8 14:36
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/common")
@Api(value = "common", tags = "通用接口管理")
public class JuheCommonController {

	private static final Logger logger = LoggerFactory.getLogger(JuheCommonController.class);

	private final JuhePackService juhePackService;

	@ApiOperation(value = "聚合文件上传", notes = "聚合文件上传")
	@PostMapping("/upload")
	public R<String> upload(@RequestParam("file") MultipartFile file) {
		try {
			if (file.isEmpty()) {
				return R.failed("文件为空");
			}
			return juhePackService.uploadFile(file);
		} catch (Exception e) {
			logger.error("通用接口-聚合文件上传-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

}
