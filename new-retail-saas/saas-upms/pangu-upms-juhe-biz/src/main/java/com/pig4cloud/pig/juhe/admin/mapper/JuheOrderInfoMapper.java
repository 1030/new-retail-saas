/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.juhe.api.entity.JuheOrderInfo;
import com.pig4cloud.pig.juhe.api.vo.JuheOrderInfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 聚合订单表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Mapper
public interface JuheOrderInfoMapper extends BaseMapper<JuheOrderInfo> {

    Page<JuheOrderInfoVo> selectJuheOrderInfoVoPage(Page<JuheOrderInfo> page, @Param(Constants.WRAPPER) QueryWrapper<JuheOrderInfo> query);
}
