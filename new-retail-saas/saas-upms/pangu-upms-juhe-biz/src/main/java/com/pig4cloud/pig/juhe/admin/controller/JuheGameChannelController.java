/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.juhe.api.entity.JuheGameChannel;
import com.pig4cloud.pig.juhe.admin.service.JuheGameChannelService;
import com.pig4cloud.pig.juhe.api.groups.Update;
import com.pig4cloud.pig.juhe.api.request.JuheGameChannelRequest;
import com.pig4cloud.pig.juhe.api.request.SelectGameChannelRequest;
import com.pig4cloud.pig.juhe.api.util.Constant;
import com.pig4cloud.pig.juhe.api.vo.JuheGameChannelVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

/**
 * 聚合游戏渠道配置表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/juhegamechannel")
@Api(value = "juhegamechannel", tags = "聚合游戏渠道配置管理")
public class JuheGameChannelController {

	private static final Logger logger = LoggerFactory.getLogger(JuheGameChannelController.class);

	private final JuheGameChannelService juheGameChannelService;

	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('juhe_juhegamechannel_get')")
	public R<Page<JuheGameChannelVo>> getJuheGameChannelPage(Page<JuheGameChannel> page, SelectGameChannelRequest juheGameChannel) {
		try {
			QueryWrapper<JuheGameChannel> query = new QueryWrapper<JuheGameChannel>();
			if (null != juheGameChannel.getGameId()) {
				query.eq("jhgc.game_id", juheGameChannel.getGameId());
			}
			if (null != juheGameChannel.getChannelId()) {
				query.eq("jhgc.channel_id", juheGameChannel.getChannelId());
			}
			query.eq("jhgc.del_flag", Constant.DEL_NO);
			query.orderByDesc("jhgc.create_time");
			return R.ok(juheGameChannelService.selectGameChannelVoPage(page, query));
		} catch (Exception e) {
			logger.error("聚合游戏渠道配置-分页查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "通过id查询", notes = "通过id查询")
	@GetMapping("/getById")
	@PreAuthorize("@pms.hasPermission('juhe_juhegamechannel_get')")
	public R<JuheGameChannelVo> getById(Long recordId) {
		try {
			return R.ok(juheGameChannelService.getGameChannelVoById(recordId));
		} catch (Exception e) {
			logger.error("聚合游戏渠道配置-通过id查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "新增聚合游戏渠道配置", notes = "新增聚合游戏渠道配置")
	@SysLog("新增聚合游戏渠道配置表")
	@PostMapping("/add")
	@PreAuthorize("@pms.hasPermission('juhe_juhegamechannel_add')")
	public R add(@Valid @RequestBody JuheGameChannelRequest juheGameChannel) {
		try {
			return juheGameChannelService.addGameChannel(juheGameChannel);
		} catch (Exception e) {
			logger.error("聚合游戏渠道配置-新增聚合游戏渠道配置-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "修改聚合游戏渠道配置", notes = "修改聚合游戏渠道配置")
	@SysLog("修改聚合游戏渠道配置表")
	@PostMapping("/edit")
	@PreAuthorize("@pms.hasPermission('juhe_juhegamechannel_edit')")
	public R edit(@Validated(Update.class) @RequestBody JuheGameChannelRequest juheGameChannel) {
		try {
			return juheGameChannelService.editGameChannel(juheGameChannel);
		} catch (Exception e) {
			logger.error("聚合游戏渠道配置-修改聚合游戏渠道配置-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "通过id删除聚合游戏渠道配置", notes = "通过id删除聚合游戏渠道配置")
	@SysLog("通过id删除聚合游戏渠道配置表")
	@GetMapping("/del")
	@PreAuthorize("@pms.hasPermission('juhe_juhegamechannel_del')")
	public R del(Long recordId) {
		try {
			return juheGameChannelService.delGameChannelById(recordId);
		} catch (Exception e) {
			logger.error("聚合游戏渠道配置-通过id删除聚合游戏渠道配置-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

}
