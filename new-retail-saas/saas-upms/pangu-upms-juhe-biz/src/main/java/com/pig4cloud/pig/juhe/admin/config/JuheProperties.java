package com.pig4cloud.pig.juhe.admin.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @author yuwenfeng
 * @description: 动态配置
 * @date 2022/7/4 14:18
 */
@RefreshScope
@Configuration
@Data
public class JuheProperties {

	/**
	 * 文件访问地址
	 */
	@Value(value = "${juhe.file.path:http://test.apk.3399.58yx.cn/upload/}")
	private String juheFilePath;

	/**
	 * 文件路径
	 */
	@Value(value = "${juhe.file.dir:/data/wwwroot/3399/ad-platform/upload/}")
	private String juheFileDir;

	/**
	 * 聚合接口服务地址
	 */
	@Value(value = "${juhe.package.url:https://juhe.dogame.com}")
	private String juhePackageUrl;

	/**
	 * 分包服务地址
	 */
	@Value(value = "${juhe.pack.pythonFile:/data/project/pythonWork/batch_package/auto/JavaCallMultiBuildApk.py}")
	private String juhePackagePythonPath;

	/**
	 * 渠道配置文件地址
	 */
	@Value(value = "${channel.config.path:/data/project/pythonWork/batch_package/auto/config/local_channel_info.xml}")
	private String channelConfigPath;

	/**
	 * 渠道配置文件路径
	 */
	@Value(value = "${channel.config.file:/data/project/pythonWork/batch_package/auto/config/}")
	private String channelConfigFile;

	/**
	 * python环境命令
	 */
	@Value(value = "${juhe.python.cmd:python}")
	private String juhePythonCmd;

	/**
	 * SCP服务器IP
	 */
	@Value(value = "${juhe.scp.client.ip:58.49.58.62}")
	private String juheScpClientIP;

	/**
	 * SCP服务器端口
	 */
	@Value(value = "${juhe.scp.client.port:38422}")
	private int juheScpClientPort;

	/**
	 * SCP服务器账号
	 */
	@Value(value = "${juhe.scp.client.name:wytxscp}")
	private String juheScpClientName;

	/**
	 * SCP服务器密码
	 */
	@Value(value = "${juhe.scp.client.password:CimISwgqOjOFqj0brEqjjg==}")
	private String juheScpClientPassword;

	/**
	 * SCP服务器目录路径
	 */
	@Value(value = "${juhe.scp.client.remote.dir:/data/wwwroot/3399/pack/tmp/}")
	private String juheScpClientRemoteDir;

	/**
	 * 运营分包服务地址
	 */
	@Value(value = "${juhe.apk.push.url:http://test.apk.3399.58yx.cn/pack-api/parentGame/uploadByLocalFile}")
	private String juheApkPushUrl;

	/**
	 * 推送母包方式1：同服务器部署 2：非同服务器部署
	 */
	@Value(value = "${juhe.apk.push.type:1}")
	private int juheApkPushType;
}
