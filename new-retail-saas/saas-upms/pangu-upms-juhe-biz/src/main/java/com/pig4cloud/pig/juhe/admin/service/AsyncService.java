package com.pig4cloud.pig.juhe.admin.service;

import com.pig4cloud.pig.juhe.api.entity.JuheGamePage;
import com.pig4cloud.pig.juhe.api.vo.AsyncPackInfoData;

import java.util.List;

/**
 * @description: 异步服务
 * @author yuwenfeng
 * @date 2021/10/18 10:54
 */
public interface AsyncService {

	void  fetchApkInfo(Long pageId);

	void  packChannelApk(List<AsyncPackInfoData> packDataList);
}
