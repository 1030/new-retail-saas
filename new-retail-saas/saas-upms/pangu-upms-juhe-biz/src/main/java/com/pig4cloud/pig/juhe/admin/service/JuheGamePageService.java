/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.juhe.api.entity.JuheGamePage;
import com.pig4cloud.pig.juhe.api.request.JuheGamePageRequest;
import com.pig4cloud.pig.juhe.api.vo.JuheGamePageVo;

import java.util.List;

/**
 * 聚合游戏母包表
 *
 * @author yuwenfeng
 * @date 2021-09-08 14:00:01
 */
public interface JuheGamePageService extends IService<JuheGamePage> {

	Page<JuheGamePageVo> selectJuheGamePageVoPage(Page<JuheGamePage> page, QueryWrapper<JuheGamePage> query);

	R addGamePage(JuheGamePageRequest juheGamePage);

    R<List<JuheGamePage>> getPackageListByGameId(Long gameId);
}
