package com.pig4cloud.pig.juhe.admin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author yuwenfeng
 * @description: 线程初始化
 * @date 2021/10/11 14:27
 */
@Component
public class JuheConstant {
	public static String FILE_PATH;

	public static String FILE_DIR;

	public static String PACKAGE_URL;

	public static String PACKAGE_PYTHON_PATH;

	public static String CHANNEL_CONFIG_PATH;

	public static String CHANNEL_CONFIG_FILE;

	@Value("${juhe.file.path}")
	private void setFilePath(String filePath) {
		FILE_PATH = filePath;
	}

	@Value("${juhe.file.dir}")
	private void setFileDir(String fileDir) {
		FILE_DIR = fileDir;
	}

	@Value("${juhe.package.url}")
	public static void setPackageUrl(String packageUrl) {
		PACKAGE_URL = packageUrl;
	}

	@Value("${juhe.pack.pythonFile}")
	private void setPackagePythonPath(String packagePythonPath) {
		PACKAGE_PYTHON_PATH = packagePythonPath;
	}

	@Value("${channel.config.path}")
	private void setChannelConfigPath(String channelConfigPath) {
		CHANNEL_CONFIG_PATH = channelConfigPath;
	}

	@Value("${channel.config.file}")
	private void setChannelConfigFile(String channelConfigFile) {
		CHANNEL_CONFIG_FILE = channelConfigFile;
	}

}
