/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.juhe.admin.service.JuheChannelAttrService;
import com.pig4cloud.pig.juhe.api.entity.JuheChannelAttr;
import com.pig4cloud.pig.juhe.api.entity.JuheChannelInfo;
import com.pig4cloud.pig.juhe.admin.service.JuheChannelInfoService;
import com.pig4cloud.pig.juhe.api.groups.Update;
import com.pig4cloud.pig.juhe.api.request.JuheChannelRequest;
import com.pig4cloud.pig.juhe.api.request.SelectChannelRequest;
import com.pig4cloud.pig.juhe.api.vo.JuheChannelAttrVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 聚合渠道商表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/juhechannelinfo")
@Api(value = "juhechannelinfo", tags = "聚合渠道商管理")
public class JuheChannelInfoController {

	private static final Logger logger = LoggerFactory.getLogger(JuheChannelInfoController.class);

	private final JuheChannelInfoService juheChannelInfoService;

	private final JuheChannelAttrService juheChannelAttrService;

	@ApiOperation(value = "下拉框查询", notes = "下拉框查询")
	@GetMapping("/list")
	@PreAuthorize("@pms.hasPermission('juhe_juhechannelinfo_get')")
	public R<List<JuheChannelInfo>> list(SelectChannelRequest request) {
		try {
			QueryWrapper<JuheChannelInfo> query = new QueryWrapper<JuheChannelInfo>();
			if (StringUtils.isNotBlank(request.getChannelSdkName())) {
				query.like("channel_sdk_name", request.getChannelSdkName());
			}
			return R.ok(juheChannelInfoService.list(query));
		} catch (Exception e) {
			logger.error("聚合渠道商-下拉框查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('juhe_juhechannelinfo_get')")
	public R<Page<JuheChannelInfo>> getJuheChannelInfoPage(Page<JuheChannelInfo> page, JuheChannelInfo juheChannelInfo) {
		try {
			QueryWrapper<JuheChannelInfo> query = new QueryWrapper<JuheChannelInfo>();
			if (StringUtils.isNotBlank(juheChannelInfo.getChannelName())) {
				query.like("channel_name", juheChannelInfo.getChannelName());
			}
			if (StringUtils.isNotBlank(juheChannelInfo.getChannelSdkName())) {
				query.like("channel_sdk_name", juheChannelInfo.getChannelSdkName());
			}
			return R.ok(juheChannelInfoService.page(page, query));
		} catch (Exception e) {
			logger.error("聚合渠道商-分页查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "通过id查询", notes = "通过id查询")
	@GetMapping("/getById")
	@PreAuthorize("@pms.hasPermission('juhe_juhechannelinfo_get')")
	public R<JuheChannelAttrVo> getById(Long channelId) {
		try {
			return juheChannelInfoService.getChannelAttrVoById(channelId);
		} catch (Exception e) {
			logger.error("聚合渠道商-通过id查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "新增聚合渠道商", notes = "新增聚合渠道商")
	@SysLog("新增聚合渠道商")
	@PostMapping("/addChl")
	@PreAuthorize("@pms.hasPermission('juhe_juhechannelinfo_add')")
	public R addChl(@Valid @RequestBody JuheChannelRequest juheChannelInfo) {
		try {
			return juheChannelInfoService.addChannelInfo(juheChannelInfo);
		} catch (Exception e) {
			logger.error("聚合渠道商-修改聚合渠道商-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "修改聚合渠道商", notes = "修改聚合渠道商")
	@SysLog("修改聚合渠道商表")
	@PostMapping("/editChl")
	@PreAuthorize("@pms.hasPermission('juhe_juhechannelinfo_edit')")
	public R editChl(@Validated(Update.class) @RequestBody JuheChannelRequest juheChannelInfo) {
		try {
			return juheChannelInfoService.editChannelInfo(juheChannelInfo);
		} catch (Exception e) {
			logger.error("聚合渠道商-修改聚合渠道商-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "通过id删除聚合渠道商", notes = "通过id删除聚合渠道商")
	@SysLog("通过id删除聚合渠道商表")
	@GetMapping("/delChl")
	@PreAuthorize("@pms.hasPermission('juhe_juhechannelinfo_del')")
	public R delChl(Long channelId) {
		try {
			return juheChannelInfoService.removeChannelById(channelId);
		} catch (Exception e) {
			logger.error("聚合渠道商-通过id删除聚合渠道商-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "通过渠道id查询属性集合", notes = "通过id查询")
	@GetMapping("/getAttrListByChlId")
	@PreAuthorize("@pms.hasPermission('juhe_juhechannelinfo_get')")
	public R<List<JuheChannelAttr>> getAttrListByChlId(Long channelId) {
		try {
			return juheChannelAttrService.getAttrListByChlId(channelId);
		} catch (Exception e) {
			logger.error("聚合渠道商-通过渠道id查询属性集合-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "通过id删除聚合渠道商属性", notes = "通过id删除聚合渠道商属性")
	@SysLog("通过id删除聚合渠道商属性表")
	@GetMapping("/delAttr")
	@PreAuthorize("@pms.hasPermission('juhe_juhechannelattr_del')")
	public R delAttr(Long attrId) {
		try {
			return juheChannelAttrService.delAttr(attrId);
		} catch (Exception e) {
			logger.error("聚合渠道商-通过id删除聚合渠道商属性-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "读取打包工具渠道配置", notes = "读取打包工具渠道配置")
	@SysLog("读取打包工具渠道配置")
	@GetMapping("/getPythonConfig")
	@PreAuthorize("@pms.hasPermission('juhe_juhechannelinfo_add')")
	public R getPythonConfig() {
		try {
			return juheChannelInfoService.getPythonConfig();
		} catch (Exception e) {
			logger.error("聚合渠道商-读取打包工具渠道配置-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}
}
