/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.juhe.api.entity.JuheChannelInfo;
import com.pig4cloud.pig.juhe.api.request.JuheChannelRequest;
import com.pig4cloud.pig.juhe.api.vo.JuheChannelAttrVo;
import org.dom4j.DocumentException;

import java.io.IOException;

/**
 * 聚合渠道商表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
public interface JuheChannelInfoService extends IService<JuheChannelInfo> {

	R addChannelInfo(JuheChannelRequest juheChannelInfo);

	R editChannelInfo(JuheChannelRequest juheChannelInfo);

	R removeChannelById(Long channelId);

	R<JuheChannelAttrVo> getChannelAttrVoById(Long channelId);

    R getPythonConfig() throws DocumentException, IOException;
}
