package com.pig4cloud.pig.juhe.admin;

import com.pig4cloud.pig.common.security.annotation.EnablePigFeignClients;
import com.pig4cloud.pig.common.security.annotation.EnablePigResourceServer;
import com.pig4cloud.pig.common.swagger.annotation.EnablePigSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author pig 聚合后台管理
 * <p>
 * 项目启动类
 */
@EnableConfigurationProperties
@EnablePigSwagger2
@EnableDiscoveryClient
@EnableCircuitBreaker
@SpringCloudApplication
@EnablePigFeignClients(basePackages = {"com.pig4cloud.pig", "com.dy.yunying"})
@EnablePigResourceServer
public class PanguJuheApplication {

	public static void main(String[] args) {
		SpringApplication.run(PanguJuheApplication.class, args);
	}

}
