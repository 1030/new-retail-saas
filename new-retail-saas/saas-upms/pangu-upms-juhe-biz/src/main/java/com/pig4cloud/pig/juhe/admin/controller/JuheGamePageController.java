/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.juhe.api.entity.JuheGamePage;
import com.pig4cloud.pig.juhe.admin.service.JuheGamePageService;
import com.pig4cloud.pig.juhe.api.request.JuheGamePageRequest;
import com.pig4cloud.pig.juhe.api.util.Constant;
import com.pig4cloud.pig.juhe.api.vo.JuheGamePageVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 聚合游戏母包表
 *
 * @author yuwenfeng
 * @date 2021-09-08 14:00:01
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/juhegamepage")
@Api(value = "juhegamepage", tags = "聚合游戏母包管理")
public class JuheGamePageController {

	private static final Logger logger = LoggerFactory.getLogger(JuheGamePageController.class);

	private final JuheGamePageService juheGamePageService;

	/**
	 * 分页查询
	 *
	 * @param page         分页对象
	 * @param juheGamePage 聚合游戏母包表
	 * @return
	 */
	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('juhe_juhegamepage_get')")
	public R<Page<JuheGamePageVo>> getJuheGamePagePage(Page<JuheGamePage> page, JuheGamePage juheGamePage) {
		try {
			QueryWrapper<JuheGamePage> query = new QueryWrapper<JuheGamePage>();
			if (null != juheGamePage.getGameId()) {
				query.eq("jhgp.game_id", juheGamePage.getGameId());
			}
			query.eq("jhgi.del_flag", Constant.DEL_NO);
			query.orderByDesc("jhgp.page_id");
			return R.ok(juheGamePageService.selectJuheGamePageVoPage(page, query));
		} catch (Exception e) {
			logger.error("聚合游戏母包管理-分页查询-接口:", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	/**
	 * 新增聚合游戏母包表
	 *
	 * @param juheGamePage 聚合游戏母包表
	 * @return R
	 */
	@ApiOperation(value = "新增聚合游戏母包", notes = "新增聚合游戏母包")
	@SysLog("新增聚合游戏母包表")
	@PostMapping("/add")
	@PreAuthorize("@pms.hasPermission('juhe_juhegamepage_add')")
	public R add(@Valid @RequestBody JuheGamePageRequest juheGamePage) {
		try {
			return juheGamePageService.addGamePage(juheGamePage);
		} catch (Exception e) {
			logger.error("聚合游戏母包管理-新增聚合游戏母包-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "根据游戏ID获取母包列表", notes = "根据游戏ID获取母包列表")
	@GetMapping("/getPackageListByGameId")
	@PreAuthorize("@pms.hasPermission('juhe_juhegamepage_get')")
	public R<List<JuheGamePage>> getPackageListByGameId(Long gameId) {
		try {
			return juheGamePageService.getPackageListByGameId(gameId);
		} catch (Exception e) {
			logger.error("聚合游戏母包管理-根据游戏ID获取母包列表-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}
}
