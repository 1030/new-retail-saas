/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.juhe.admin.service.JuhePackService;
import com.pig4cloud.pig.juhe.api.entity.JuheChannelInfo;
import com.pig4cloud.pig.juhe.api.entity.JuhePack;
import com.pig4cloud.pig.juhe.api.request.JuhePackageRequest;
import com.pig4cloud.pig.juhe.api.request.PushPackageRequest;
import com.pig4cloud.pig.juhe.api.request.SelectAttrValueRequest;
import com.pig4cloud.pig.juhe.api.request.SelectPackChannelRequest;
import com.pig4cloud.pig.juhe.api.util.Constant;
import com.pig4cloud.pig.juhe.api.vo.JuheChannelAttrValueVo;
import com.pig4cloud.pig.juhe.api.vo.JuhePackVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 聚合打包记录表
 *
 * @author yuwenfeng
 * @date 2021-09-15 13:47:28
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/juhepack")
@Api(value = "juhepack", tags = "聚合打包记录表管理")
public class JuhePackController {

	private final JuhePackService juhePackService;

	private static final Logger logger = LoggerFactory.getLogger(JuhePackController.class);

	/**
	 * 分页查询
	 *
	 * @param page     分页对象
	 * @param juhePack 聚合打包记录表
	 * @return
	 */
	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('juhe_juhepack_get')")
	public R<Page<JuhePackVo>> getJuhePackPage(Page<JuhePack> page, JuhePack juhePack) {
		try {
			QueryWrapper<JuhePack> query = new QueryWrapper<JuhePack>();
			if (null != juhePack.getGameId() && juhePack.getGameId() > 0) {
				query.eq("jhp.game_id", juhePack.getGameId());
			}
			if (null != juhePack.getChannelId() && juhePack.getChannelId() > 0) {
				query.eq("jhp.channel_id", juhePack.getChannelId());
			}
			if (null != juhePack.getPackStatus() && juhePack.getPackStatus() > 0) {
				query.eq("jhp.pack_status", juhePack.getPackStatus());
			}
			if (StringUtils.isNotBlank(juhePack.getCreateBy())) {
				query.like("jhp.create_by", juhePack.getCreateBy());
			}
			query.eq("jhgi.del_flag", Constant.DEL_NO);
			query.orderByDesc("jhp.create_time");
			return R.ok(juhePackService.selectJuhePackVoPage(page, query));
		} catch (Exception e) {
			logger.error("聚合打包记录管理-分页查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "根据游戏ID和渠道ID查询配置属性值", notes = "根据游戏ID和渠道ID查询配置属性值")
	@GetMapping("/selectAttrValueList")
	@PreAuthorize("@pms.hasPermission('juhe_juhepack_get')")
	public R<List<JuheChannelAttrValueVo>> selectAttrValueList(@Valid SelectAttrValueRequest request) {
		try {
			return R.ok(juhePackService.selectAttrValueList(request.getChannelId(), request.getGameId()));
		} catch (Exception e) {
			logger.error("聚合打包记录管理-根据游戏ID和渠道ID查询配置属性值-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "获取游戏所配置渠道下拉框查询", notes = "获取游戏所配置渠道下拉框查询")
	@GetMapping("/selectChannelList")
	@PreAuthorize("@pms.hasPermission('juhe_juhepack_get')")
	public R<List<JuheChannelInfo>> selectChannelList(@Valid SelectPackChannelRequest request) {
		try {
			QueryWrapper<JuheChannelInfo> query = new QueryWrapper<JuheChannelInfo>();
			if (StringUtils.isNotBlank(request.getChannelSdkName())) {
				query.like("jhcl.channel_sdk_name", request.getChannelSdkName());
			}
			query.eq("jhgc.game_id", request.getGameId());
			query.eq("jhgc.del_flag", Constant.DEL_NO);
			query.orderByAsc("jhgc.create_time");
			return R.ok(juhePackService.selectChannelList(query));
		} catch (Exception e) {
			logger.error("聚合打包记录管理-获取游戏所配置渠道下拉框查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "根据游戏渠道打包", notes = "根据游戏渠道打包")
	@SysLog("根据游戏渠道打包")
	@PostMapping("/juhePackage")
	@PreAuthorize("@pms.hasPermission('juhe_juhepack_package')")
	public R juhePackage(@Valid @RequestBody JuhePackageRequest request) {
		try {
			return juhePackService.juhePackage(request);
		} catch (Exception e) {
			logger.error("聚合打包记录管理-根据游戏渠道打包-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "推送母包至运营平台", notes = "推送母包至运营平台")
	@SysLog("推送母包至运营平台")
	@PostMapping("/pushPackage")
	public R pushPackage(@Valid @RequestBody PushPackageRequest request) {
		try {
			return juhePackService.pushPackage(request);
		} catch (Exception e) {
			logger.error("聚合打包记录管理-推送母包至运营平台-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}
}
