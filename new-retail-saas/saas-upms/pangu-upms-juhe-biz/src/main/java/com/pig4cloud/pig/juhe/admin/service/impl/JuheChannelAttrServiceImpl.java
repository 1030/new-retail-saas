/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.juhe.admin.mapper.JuheChannelAttrValueMapper;
import com.pig4cloud.pig.juhe.api.entity.JuheChannelAttr;
import com.pig4cloud.pig.juhe.admin.mapper.JuheChannelAttrMapper;
import com.pig4cloud.pig.juhe.admin.service.JuheChannelAttrService;
import com.pig4cloud.pig.juhe.api.entity.JuheChannelAttrValue;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 聚合渠道商属性表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Service
@AllArgsConstructor
public class JuheChannelAttrServiceImpl extends ServiceImpl<JuheChannelAttrMapper, JuheChannelAttr> implements JuheChannelAttrService {

	private final JuheChannelAttrValueMapper juheChannelAttrValueMapper;

	@Override
	public R<List<JuheChannelAttr>> getAttrListByChlId(Long channelId) {
		return R.ok(baseMapper.selectList(new QueryWrapper<JuheChannelAttr>().eq("channel_id", channelId)));
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R delAttr(Long attrId) {
		JuheChannelAttr info = baseMapper.selectById(attrId);
		if (null == info) {
			return R.failed("属性不存在");
		}
		int num = juheChannelAttrValueMapper.selectCount(new QueryWrapper<JuheChannelAttrValue>().eq("attr_id", info.getAttrId()));
		if (num > 0) {
			return R.failed("该属性已被使用，暂无法删除");
		}
		int statusNum = baseMapper.deleteById(info.getAttrId());
		if (statusNum > 0) {
			return R.ok();
		} else {
			return R.failed("删除操作异常");
		}
	}
}
