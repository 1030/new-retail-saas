/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.juhe.admin.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.juhe.api.entity.JuheGameInfo;
import com.pig4cloud.pig.juhe.admin.mapper.JuheGameInfoMapper;
import com.pig4cloud.pig.juhe.admin.service.JuheGameInfoService;
import com.pig4cloud.pig.juhe.api.request.JuheGameInfoRequest;
import com.pig4cloud.pig.juhe.api.util.AppUtils;
import com.pig4cloud.pig.juhe.api.util.Constant;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 聚合游戏表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@Service
@AllArgsConstructor
public class JuheGameInfoServiceImpl extends ServiceImpl<JuheGameInfoMapper, JuheGameInfo> implements JuheGameInfoService {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R saveJuheGameInfo(JuheGameInfoRequest juheGameInfo) {
		long num = baseMapper.selectCount(new QueryWrapper<JuheGameInfo>().eq("game_name", juheGameInfo.getGameName()).eq("del_flag",Constant.DEL_NO));
		if (num > 0) {
			return R.failed("该游戏名称已存在");
		}
		String appId = generateAppId();
		String appKey = AppUtils.getAppSecret(appId);
		String appSecret = AppUtils.getAppSecret(appKey);
		JuheGameInfo info = JSON.parseObject(JSON.toJSONString(juheGameInfo), JuheGameInfo.class);
		info.setAppId(appId);
		info.setAppKey(appKey);
		info.setAppSecret(appSecret);
		int statusNum = baseMapper.insert(info);
		if (statusNum > 0) {
			return R.ok();
		} else {
			return R.failed("新增操作异常");
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R updateJuheGameInfo(JuheGameInfoRequest juheGameInfo) {
		List<JuheGameInfo> list = baseMapper.selectList(new QueryWrapper<JuheGameInfo>().eq("game_name", juheGameInfo.getGameName()).eq("del_flag",Constant.DEL_NO));
		if (CollectionUtils.isNotEmpty(list)) {
			for (JuheGameInfo info : list) {
				if (!juheGameInfo.getGameId().equals(info.getGameId())) {
					return R.failed("游戏名称已存在");
				}
			}
		}
		JuheGameInfo info = baseMapper.selectById(juheGameInfo.getGameId());
		if (null == info) {
			return R.failed("游戏不存在");
		}
		info = JSON.parseObject(JSON.toJSONString(juheGameInfo), JuheGameInfo.class);
		int statusNum = baseMapper.updateById(info);
		if (statusNum > 0) {
			return R.ok();
		} else {
			return R.failed("更新操作异常");
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public R removeInfoById(Long gameId) {
		JuheGameInfo info = baseMapper.selectById(gameId);
		if (null == info) {
			return R.failed("游戏不存在");
		}
		info.setDelFlag(Constant.DEL_YES);
		int statusNum = baseMapper.updateById(info);
		if (statusNum > 0) {
			return R.ok();
		} else {
			return R.failed("删除操作异常");
		}
	}

	/**
	 * @description: 唯一值appkey
	 * @author yuwenfeng
	 * @date 2021/9/6 15:20
	 */
	private String generateAppId() {
		String appId = AppUtils.getAppId();
		long num = baseMapper.selectCount(new QueryWrapper<JuheGameInfo>().eq("app_id", appId));
		if (num > 0) {
			return generateAppId();
		}
		return appId;
	}
}
