/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.juhe.api.entity.JuheOrderInfo;
import com.pig4cloud.pig.juhe.admin.service.JuheOrderInfoService;
import com.pig4cloud.pig.juhe.api.request.SelectJuheOrderRequest;
import com.pig4cloud.pig.juhe.api.util.Constant;
import com.pig4cloud.pig.juhe.api.vo.JuheOrderInfoVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 聚合订单表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/juheorderinfo")
@Api(value = "juheorderinfo", tags = "聚合订单管理")
public class JuheOrderInfoController {

	private static final Logger logger = LoggerFactory.getLogger(JuheOrderInfoController.class);

	private final JuheOrderInfoService juheOrderInfoService;

	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('juhe_juheorderinfo_get')")
	public R<Page<JuheOrderInfoVo>> page(Page<JuheOrderInfo> page, SelectJuheOrderRequest juheOrderInfo) {
		try {
			QueryWrapper<JuheOrderInfo> query = new QueryWrapper<JuheOrderInfo>();
			if (StringUtils.isNotBlank(juheOrderInfo.getOrderSerial())) {
				query.like("jhoi.order_serial", juheOrderInfo.getOrderSerial());
			}
			if (null != juheOrderInfo.getGameId()) {
				query.eq("jhoi.game_id", juheOrderInfo.getGameId());
			}
			if (null != juheOrderInfo.getChannelId()) {
				query.eq("jhoi.channel_id", juheOrderInfo.getChannelId());
			}
			if (StringUtils.isNotBlank(juheOrderInfo.getGameOrderSerial())) {
				query.like("jhoi.game_order_serial", juheOrderInfo.getGameOrderSerial());
			}
			if (StringUtils.isNotBlank(juheOrderInfo.getChannelOrderSerial())) {
				query.like("jhoi.channel_order_serial", juheOrderInfo.getChannelOrderSerial());
			}
			if (null != juheOrderInfo.getOrderStatus() && juheOrderInfo.getOrderStatus() >= 0) {
				query.eq("jhoi.order_status", juheOrderInfo.getOrderStatus());
			}
			if (null != juheOrderInfo.getOpenId()) {
				query.eq("jhoi.open_id", juheOrderInfo.getOpenId());
			}
			if (StringUtils.isNotBlank(juheOrderInfo.getRoleId())) {
				query.eq("jhoi.role_id", juheOrderInfo.getRoleId());
			}
			if (StringUtils.isNotBlank(juheOrderInfo.getRoleName())) {
				query.like("jhoi.role_name", juheOrderInfo.getRoleName());
			}
			if (StringUtils.isNotBlank(juheOrderInfo.getServerId())) {
				query.eq("jhoi.server_id", juheOrderInfo.getServerId());
			}
			if (StringUtils.isNotBlank(juheOrderInfo.getServerName())) {
				query.like("jhoi.server_name", juheOrderInfo.getServerName());
			}
			if (StringUtils.isNotBlank(juheOrderInfo.getSTime())) {
				query.ge("DATE_FORMAT(jhoi.create_time,'%Y-%m-%d')", DateUtil.format(DateUtil.parse(juheOrderInfo.getSTime(), "yyyyMMdd"), "yyyy-MM-dd"));
			}
			if (StringUtils.isNotBlank(juheOrderInfo.getETime())) {
				query.le("DATE_FORMAT(jhoi.create_time,'%Y-%m-%d')", DateUtil.format(DateUtil.parse(juheOrderInfo.getETime(), "yyyyMMdd"), "yyyy-MM-dd"));
			}
			query.eq("jhgi.del_flag", Constant.DEL_NO);
			query.orderByDesc("jhoi.create_time");
			return R.ok(juheOrderInfoService.getJuheOrderInfoVoPage(page, query));
		} catch (Exception e) {
			logger.error("聚合订单-分页查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "手动补单", notes = "手动补单")
	@PostMapping("/compensateOrder")
	@PreAuthorize("@pms.hasPermission('juhe_juheorderinfo_compensate')")
	public R compensateOrder(Long orderId) {
		try {
			if (null == orderId) {
				return R.failed("orderId不能为空");
			}
			return juheOrderInfoService.compensateOrder(orderId);
		} catch (Exception e) {
			logger.error("聚合订单-手动补单-接口：", e);
			return R.failed("补单失败，回调地址请求错误，错误码：1005，错误信息：游戏发货地址返回值无法解析");
		}
	}
}
