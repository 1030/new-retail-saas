/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.juhe.api.entity.JuheChannelInfo;
import com.pig4cloud.pig.juhe.api.entity.JuhePack;
import com.pig4cloud.pig.juhe.api.request.JuhePackageRequest;
import com.pig4cloud.pig.juhe.api.request.PushPackageRequest;
import com.pig4cloud.pig.juhe.api.vo.JuheChannelAttrValueVo;
import com.pig4cloud.pig.juhe.api.vo.JuhePackVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 聚合打包记录表
 *
 * @author yuwenfeng
 * @date 2021-09-15 13:47:28
 */
public interface JuhePackService extends IService<JuhePack> {

	Page<JuhePackVo> selectJuhePackVoPage(Page<JuhePack> page, QueryWrapper<JuhePack> query);

	List<JuheChannelAttrValueVo> selectAttrValueList(Long channelId, Long gameId);

	R juhePackage(JuhePackageRequest request);

	R<String> uploadFile(MultipartFile file) throws Exception;

	List<JuheChannelInfo> selectChannelList(QueryWrapper<JuheChannelInfo> query);

    R pushPackage(PushPackageRequest request) throws IOException;
}
