/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.juhe.api.entity.JuheUser;
import com.pig4cloud.pig.juhe.admin.service.JuheUserService;
import com.pig4cloud.pig.juhe.api.request.SelectJuheUserRequest;
import com.pig4cloud.pig.juhe.api.util.Constant;
import com.pig4cloud.pig.juhe.api.vo.JuheUserVo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * 聚合用户表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/juheuser")
@Api(value = "juheuser", tags = "聚合用户管理")
public class JuheUserController {

	private static final Logger logger = LoggerFactory.getLogger(JuheUserController.class);

	private final JuheUserService juheUserService;

	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/page")
	public R<Page<JuheUserVo>> page(Page<JuheUser> page, SelectJuheUserRequest request) {
		try {
			QueryWrapper<JuheUser> query = new QueryWrapper<JuheUser>();
			if (null != request.getGameId()) {
				query.eq("jhu.game_id", request.getGameId());
			}
			if (null != request.getChannelId()) {
				query.eq("jhu.channel_id", request.getChannelId());
			}
			if (null != request.getOpenId() && request.getOpenId() > 0) {
				query.like("jhu.open_id", request.getOpenId());
			}
			if (StringUtils.isNotBlank(request.getUserId())) {
				query.like("jhu.user_id", request.getUserId());
			}
			if (StringUtils.isNotBlank(request.getSTime())) {
				query.ge("DATE_FORMAT(jhu.reg_time,'%Y-%m-%d')", DateUtil.format(DateUtil.parse(request.getSTime(), "yyyyMMdd"), "yyyy-MM-dd"));
			}
			if (StringUtils.isNotBlank(request.getETime())) {
				query.le("DATE_FORMAT(jhu.reg_time,'%Y-%m-%d')", DateUtil.format(DateUtil.parse(request.getETime(), "yyyyMMdd"), "yyyy-MM-dd"));
			}
			query.eq("jhgi.del_flag", Constant.DEL_NO);
			query.orderByDesc("jhu.reg_time");
			return R.ok(juheUserService.getJuheUserVoPage(page, query));
		} catch (Exception e) {
			logger.error("聚合用户-分页查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

}
