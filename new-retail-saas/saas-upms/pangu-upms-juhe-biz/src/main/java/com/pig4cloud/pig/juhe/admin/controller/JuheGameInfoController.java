/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.juhe.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.juhe.api.entity.JuheGameInfo;
import com.pig4cloud.pig.juhe.admin.service.JuheGameInfoService;
import com.pig4cloud.pig.juhe.api.groups.Update;
import com.pig4cloud.pig.juhe.api.request.JuheGameInfoRequest;
import com.pig4cloud.pig.juhe.api.util.Constant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 聚合游戏表
 *
 * @author yuwenfeng
 * @date 2021-09-02 13:56:17
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/juhegameinfo")
@Api(value = "juhegameinfo", tags = "聚合游戏管理")
public class JuheGameInfoController {

	private static final Logger logger = LoggerFactory.getLogger(JuheGameInfoController.class);

	private final JuheGameInfoService juheGameInfoService;

	@ApiOperation(value = "下拉框查询", notes = "下拉框查询")
	@GetMapping("/list")
	@PreAuthorize("@pms.hasPermission('juhe_juhegameinfo_get')")
	public R<List<JuheGameInfo>> list() {
		try {
			return R.ok(juheGameInfoService.list(new QueryWrapper<JuheGameInfo>().eq("del_flag", Constant.DEL_NO)));
		} catch (Exception e) {
			logger.error("聚合游戏-下拉框-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('juhe_juhegameinfo_get')")
	public R<Page<JuheGameInfo>> page(Page<JuheGameInfo> page, JuheGameInfo juheGameInfo) {
		try {
			QueryWrapper<JuheGameInfo> query = new QueryWrapper<JuheGameInfo>();
			if (StringUtils.isNotBlank(juheGameInfo.getGameName())) {
				query.like("game_name", juheGameInfo.getGameName());
			}
			if (StringUtils.isNotBlank(juheGameInfo.getAppId())) {
				query.eq("app_id", juheGameInfo.getAppId());
			}
			query.eq("del_flag", Constant.DEL_NO);
			query.orderByDesc("create_time");
			return R.ok(juheGameInfoService.page(page, query));
		} catch (Exception e) {
			logger.error("聚合游戏-分页查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "通过id查询", notes = "通过id查询")
	@GetMapping("/getById")
	@PreAuthorize("@pms.hasPermission('juhe_juhegameinfo_get')")
	public R<JuheGameInfo> getById(Long gameId) {
		try {
			return R.ok(juheGameInfoService.getById(gameId));
		} catch (Exception e) {
			logger.error("聚合游戏-通过id查询-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "新增聚合游戏", notes = "新增聚合游戏")
	@SysLog("新增聚合游戏表")
	@PostMapping("/add")
	@PreAuthorize("@pms.hasPermission('juhe_juhegameinfo_add')")
	public R add(@Valid @RequestBody JuheGameInfoRequest juheGameInfo) {
		try {
			return juheGameInfoService.saveJuheGameInfo(juheGameInfo);
		} catch (Exception e) {
			logger.error("聚合游戏-新增聚合游戏-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "修改聚合游戏", notes = "修改聚合游戏")
	@SysLog("修改聚合游戏表")
	@PostMapping("/edit")
	@PreAuthorize("@pms.hasPermission('juhe_juhegameinfo_edit')")
	public R edit(@Validated(Update.class) @RequestBody JuheGameInfoRequest juheGameInfo) {
		try {
			return juheGameInfoService.updateJuheGameInfo(juheGameInfo);
		} catch (Exception e) {
			logger.error("聚合游戏-修改聚合游戏-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

	@ApiOperation(value = "通过id删除聚合游戏", notes = "通过id删除聚合游戏")
	@SysLog("通过id删除聚合游戏表")
	@GetMapping("/del")
	@PreAuthorize("@pms.hasPermission('juhe_juhegameinfo_del')")
	public R del(Long gameId) {
		try {
			return juheGameInfoService.removeInfoById(gameId);
		} catch (Exception e) {
			logger.error("聚合游戏-通过id删除聚合游戏-接口：", e);
			return R.failed("接口异常，请联系管理员");
		}
	}

}
