package com.dy.yandi.api.client.requirement.model.dto;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


/**
 * 需求制作者表
 * @author  chengang
 * @version  2021-09-28 13:44:36
 * table: design_requirement_producer
 */
@Data
public class DesignRequirementProducerDto implements Serializable {
	
	//columns START
		//主键id
		private Long id;
		//需求ID
		private Long requirementId;
		//制作人
		private Long producerId;
		//制作类型 1:默认(AE) 2:状态 3:脚本 4:动作 5:场景 6:特效
		private Integer producerType;
		//制作类型值
		private String producerValue;
		// 创意标记 1:是 0:否
		private Integer creativeRemark;
		// 突出标记 1:是 0:否
		private Integer prominentRemark;
		//是否删除  0否 1是
		private Integer isDeleted;
		//创建时间
		private Date createTime;
		//修改时间
		private Date updateTime;
		//创建人
		private Long createId;
		//修改人
		private Long updateId;
	//columns END 数据库字段结束
		// 用户名
		private String userName;
		// 用户昵称
		private String realName;

	
}


