package com.dy.yandi.api.client.demo.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Title null.java
 * @Package com.dy.yandi.api.client.demo.model.entity
 * @Author 马嘉祺
 * @Date 2021/10/8 11:28
 * @Description
 */
@Data
public class DataChartDO {

	private String period;

	private Long materialId;

	private String materialName;

	private Long sellingPointId;

	private String sellingPointName;

	private Long makerId;

	private String makerName;

	private Integer creatorId;

	private String creatorName;

	private List<Maker> makers;

	private Integer adCount;

	private Integer adDays;

	private BigDecimal cost;

	private Integer materialCount;

	private Integer sellingPointCount;

	private String attrs;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Maker {

		private Long makerId;

		private String makerName;

	}

}
