package com.dy.yandi.api.client.requirement.model.dto;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 需求表
 * @author  chengang
 * @version  2021-09-28 13:44:24
 * table: design_requirement
 */
@Data
public class DesignRequirementDto implements Serializable {
	
	//columns START
	//主键id
	private Long id;
	//需求类型 1:AE 2:UE4 3:平面
	private Integer type;
	//优先级 1:紧急 2:较急 3:普通 4:低
	private Integer priority;
	//标题
	private String title;
	//游戏ID
	private Long gameId;
	//标签(字典的值，多个逗号分割)
	private String tag;
	//详情
	private String detail;
	//工期
	private String duration;
	//截止时间
	private Date endTime;
	//发布时间
	private Date publishTime;
	//卖点ID
	private Long sellingPointId;
	//文件类型 1:文件 2:链接
	private Integer fileType;
	//文件地址
	private String fileLink;
	//文件封面地址
	private String fileCoverLink;
	//成果物文件地址
	private String resultLink;
	//成果物文件封面地址
	private String resultCoverLink;
	//完成时间
	private Date completeTime;
	//备注
	private String remark;
	//状态 1:待审核 2:被驳回 3:未开始 4:进行中 5:已完成
	private Integer status;
	//标记状态 1:待标记 2:已标记
	private Integer markStatus;
	//成果物提交者
	private Long resultProducerId;
	//盘古素材唯一ID
	private Long panguMaterialId;
	//拒绝原因
	private String forbidReason;
	//是否删除  0否 1是
	private Integer isDeleted;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	//创建人
	private Long createId;
	//修改人
	private Long updateId;
//columns END 数据库字段结束
	// 游戏名称
	private String gameName;
	// 标签ID
	private String labelId;
	// 标签名称
	private String labelName;
	// 卖点名称
	private String sellingPointName;
	// 需求者名称
	private String needRealName;
	// 制作者ID
	private String producerId;
	// 提交者名称
	private String resultRealName;
	// 制作人名称
	private String producerRealName;
	//创建人部门名称
	private String createDeptName;
	/**
	 * 制作人列表
	 */
	private List<DesignRequirementProducerDto> producerList;

}


