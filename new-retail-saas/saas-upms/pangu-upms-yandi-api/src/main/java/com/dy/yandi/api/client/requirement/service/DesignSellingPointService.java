

package com.dy.yandi.api.client.requirement.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yandi.api.client.requirement.model.DesignSellingPoint;
import com.dy.yandi.api.client.requirement.model.vo.DesignSellingPointVo;
import com.pig4cloud.pig.common.core.util.R;


/**
 * 需求卖点表
 * @author  chengang
 * @version  2021-09-28 13:44:43
 * table: design_selling_point
 */
public interface DesignSellingPointService extends IService<DesignSellingPoint> {
	/**
	 * 卖点列表
	 * @param vo
	 * @return
	 */
	R getList(DesignSellingPointVo vo);

	
}


