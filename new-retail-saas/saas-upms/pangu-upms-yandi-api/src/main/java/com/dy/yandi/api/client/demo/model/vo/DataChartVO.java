package com.dy.yandi.api.client.demo.model.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.yandi.api.client.demo.model.entity.DataChartDO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collection;

/**
 * @Title null.java
 * @Package com.dy.yandi.api.client.demo.model.vo
 * @Author 马嘉祺
 * @Date 2021/10/8 14:38
 * @Description
 */
@Data
public class DataChartVO extends Page<DataChartDO> {

	/**
	 * 开始时间
	 */
	@NotNull(message = "开始时间不能为空", groups = {GetKpiCost.class, ChartsQuery1.class, ChartsQuery2.class})
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate startDate;

	/**
	 * 结束时间
	 */
	@NotNull(message = "结束时间不能为空", groups = {GetKpiCost.class, ChartsQuery1.class, ChartsQuery2.class})
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate endDate;

	/**
	 * 素材ID集合
	 */
	@NotEmpty(message = "素材ID不能为空", groups = {GetKpiCost.class})
	private Collection<Long> materialIds;

	/**
	 * 主游戏ID
	 */
	private Collection<Long> pgids;

	/**
	 * 制作者ID
	 */
	@NotNull(message = "制作者ID不能为空", groups = {ChartsQuery2.class})
	private Integer makerId;

	private Integer topn;

	public interface GetKpiCost {
	}

	public interface ChartsQuery1 {
	}

	public interface ChartsQuery2 {
	}

}
