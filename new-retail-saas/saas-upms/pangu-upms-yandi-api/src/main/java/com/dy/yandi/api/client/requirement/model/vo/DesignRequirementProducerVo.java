package com.dy.yandi.api.client.requirement.model.vo;


import com.dy.yandi.api.client.requirement.model.DesignRequirementProducer;
import lombok.Data;

/**
 * 需求制作者表
 * @author  chengang
 * @version  2021-09-28 13:44:36
 * table: design_requirement_producer
 */
@Data
public class DesignRequirementProducerVo {
	/**
	 * 需求ID
	 */
	private String requirementId;
	
	
	
}
