package com.dy.yandi.api.client.demo.model.vo;

import com.dy.yandi.api.client.demo.model.Demo;
import lombok.Data;

/**
 * @Description //TODO
 * @Author chengang
 * @Date 2021/9/3
 */
@Data
public class DemoVo extends Demo {

	private String enName;

}
