package com.dy.yandi.api.client.requirement.model.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;

/**
 * 需求表
 * @author  chengang
 * @version  2021-09-28 13:44:24
 * table: design_requirement
 */
@Data
public class DesignRequirementVo extends Page {
	// 需求ID
	private String id;
	// 类型
	private String type;
	// 需求标题
	private String title;
	// 状态
	private String status;
	// 标记
	private String markStatus;
	// 开始时间
	private String startTime;
	// 结束时间
	private String endTime;
	// 完成开始时间
	private String okStartTime;
	// 完成结束时间
	private String okEndTime;
	// 工期
	private String duration;
	// 描述
	private String detail;
	// 优先级
	private String priority;
	// 卖点
	private String sellingPointId;
	// 文件类型
	private String fileType;
	// 文件地址
	private String fileLink;
	// 备注
	private String remark;
	// 标签Ids
	private String labelIds;
	// 标签名称
	private String labelName;
	// 游戏ID
	private String gameId;
	// 需求人ID
	private String createId;
	// 制作人ID
	private String producerId;
	// 制作人json
	private JSONArray producerJson;
	// 驳回原因
	private String forbidReason;
	// 用户ID
	private String userId;
	// 租户ID
	private String tenantId;
	// 排序字段
	private String orderField;
	// 排序类型
	private String orderSort;
	// 参考文件
	private MultipartFile referenceFile;
}
