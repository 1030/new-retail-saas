package com.dy.yandi.api.client.requirement.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;




/**
 * 需求表
 * @author  chengang
 * @version  2021-09-28 13:44:24
 * table: design_requirement
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "design_requirement")
public class DesignRequirement extends Model<DesignRequirement>{

	//columns START
			//主键id
			@TableId(value="id",type= IdType.AUTO)
			private Long id;

			//需求类型 1:AE 2:UE4 3:平面
			@TableField(value = "type")
			private Integer type;

			//优先级 1:紧急 2:较急 3:普通 4:低
			@TableField(value = "priority")
			private Integer priority;

			//标题
			@TableField(value = "title")
			private String title;

			//游戏ID
			@TableField(value = "game_id")
			private Long gameId;

			//标签(字典的值，多个逗号分割)
			@TableField(value = "tag")
			private String tag;

			//详情
			@TableField(value = "detail")
			private String detail;

			//工期
			@TableField(value = "duration")
			private String duration;

			//截止时间
			@TableField(value = "end_time")
			private Date endTime;

			//发布时间
			@TableField(value = "publish_time")
			private Date publishTime;

			//卖点ID
			@TableField(value = "selling_point_id")
			private Long sellingPointId;

			//文件类型 1:文件 2:链接
			@TableField(value = "file_type")
			private Integer fileType;

			//文件地址
			@TableField(value = "file_link")
			private String fileLink;
			//文件封面地址
			@TableField(value = "file_cover_link")
			private String fileCoverLink;

			//成果物文件地址
			@TableField(value = "result_link")
			private String resultLink;
			//成果物文件封面地址
			@TableField(value = "result_cover_link")
			private String resultCoveLink;

			//完成时间
			@TableField(value = "complete_time")
			private Date completeTime;

			//备注
			@TableField(value = "remark")
			private String remark;

			//状态 1:待审核 2:被驳回 3:未开始 4:进行中 5:已完成
			@TableField(value = "status")
			private Integer status;

			//标记状态 1:待标记 2:已标记
			@TableField(value = "mark_status")
			private Integer markStatus;

			//成果物提交者
			@TableField(value = "result_producer_id")
			private Long resultProducerId;

			//盘古素材唯一ID
			@TableField(value = "pangu_material_id")
			private Long panguMaterialId;

			//拒绝原因
			@TableField(value = "forbid_reason")
			private String forbidReason;

			//是否删除  0否 1是
			@TableField(value = "is_deleted")
			private Integer isDeleted;

			//创建时间
			@TableField(value = "create_time")
			private Date createTime;

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;

			//创建人
			@TableField(value = "create_id")
			private Long createId;

			//修改人
			@TableField(value = "update_id")
			private Long updateId;

	//columns END 数据库字段结束
	

	
}

	

	
	

