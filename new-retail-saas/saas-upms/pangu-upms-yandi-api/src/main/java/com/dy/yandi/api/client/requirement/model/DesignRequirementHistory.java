package com.dy.yandi.api.client.requirement.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;




/**
 * 需求历史记录表
 * @author  chengang
 * @version  2021-09-28 13:44:30
 * table: design_requirement_history
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "design_requirement_history")
public class DesignRequirementHistory extends Model<DesignRequirementHistory>{

	//columns START
			//主键id
			@TableId(value="id",type= IdType.AUTO)
			private Long id;

			//需求ID
			@TableField(value = "requirement_id")
			private Long requirementId;

			//类型 1:历史记录 2:评论
			@TableField(value = "type")
			private Integer type;

			//备注或评论
			@TableField(value = "remark")
			private String remark;

			//备注或评论时间
			@TableField(value = "remark_time")
			private Date remarkTime;

			//父评论ID,一级评论的父ID为0
			@TableField(value = "p_id")
			private Long pId;

			//操作者
			@TableField(value = "opt_id")
			private Long optId;

			//评论人名称
			@TableField(value = "opt_name")
			private String optName;

			//是否删除  0否 1是
			@TableField(value = "is_deleted")
			private Integer isDeleted;

			//创建时间
			@TableField(value = "create_time")
			private Date createTime;

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;

			//创建人
			@TableField(value = "create_id")
			private Long createId;

			//修改人
			@TableField(value = "update_id")
			private Long updateId;

	//columns END 数据库字段结束
	

	
}

	

	
	

