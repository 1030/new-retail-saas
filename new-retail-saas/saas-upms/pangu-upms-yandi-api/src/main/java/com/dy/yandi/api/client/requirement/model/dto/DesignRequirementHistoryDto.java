package com.dy.yandi.api.client.requirement.model.dto;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


/**
 * 需求历史记录表
 * @author  chengang
 * @version  2021-09-28 13:44:30
 * table: design_requirement_history
 */
@Data
public class DesignRequirementHistoryDto implements Serializable {
	
	//columns START
		//主键id
		private Long id;
		//需求ID
		private Long requirementId;
		//类型 1:历史记录 2:评论
		private Integer type;
		//备注或评论
		private String remark;
		//备注或评论时间
		private Date remarkTime;
		//父评论ID,一级评论的父ID为0
		private Long pId;
		//操作者
		private Long optId;
		//评论人名称
		private String optName;
		//是否删除  0否 1是
		private Integer isDeleted;
		//创建时间
		private Date createTime;
		//修改时间
		private Date updateTime;
		//创建人
		private Long createId;
		//修改人
		private Long updateId;
	//columns END 数据库字段结束
	
	
}


