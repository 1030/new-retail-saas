package com.dy.yandi.api.client.requirement.model.vo;


import com.dy.yandi.api.client.requirement.model.DesignSellingPoint;
import lombok.Data;

/**
 * 需求卖点表
 * @author  chengang
 * @version  2021-09-28 13:44:43
 * table: design_selling_point
 */
@Data
public class DesignSellingPointVo  extends DesignSellingPoint {
	
	private String name;
	
}
