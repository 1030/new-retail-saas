package com.dy.yandi.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;


@Getter
@RequiredArgsConstructor
public enum DesignEnum {
	DESIGN_TYPE_1(1,  "AE"),
	DESIGN_TYPE_2(2,  "UE4"),
	DESIGN_TYPE_3(3,  "平面");



	private final Integer key;
	private final String name;

	public static String getNameByKey(String key){
		if (StringUtils.isBlank(key)){
			return null;
		}
		for (DesignEnum item : DesignEnum.values()) {
			if (key.equals(item.getKey())) {
				return item.getName();
			}
		}
		return null;

	}

	public static List<Integer> getAllKey(){
		List<Integer> result = new ArrayList<>();
		for (DesignEnum item : DesignEnum.values()) {
			result.add(item.getKey());
		}
		return result;
	}

}
