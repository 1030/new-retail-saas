package com.dy.yandi.api.client.requirement.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yandi.api.client.demo.model.entity.DataChartDO;
import com.dy.yandi.api.client.requirement.model.DesignBonus;
import com.dy.yandi.api.client.requirement.model.dto.PerformanceDto;
import com.dy.yandi.api.client.requirement.model.dto.PerformanceVo;
import com.pig4cloud.pig.common.core.util.R;

import java.math.BigDecimal;
import java.util.List;

/**
 * 绩效奖金表
 * @author  chengang
 * @version  2021-09-28 13:43:52
 * table: design_bonus
 */
public interface DesignBonusService extends IService<DesignBonus> {

	IPage<PerformanceVo> getPerformance(PerformanceDto dto);

	List<PerformanceVo> getPerformanceNoPage(PerformanceDto dto);

	List<PerformanceVo> getCalcResult(List<PerformanceVo> list, List<DataChartDO> costList, boolean isTeam,Integer tenantId);

	List<DataChartDO> getAllMaterialCost(PerformanceDto req, List<PerformanceVo> list);

	Integer calcEvaluate(Integer type,BigDecimal totalCore);

	R getPerformance4Task(Integer period, String param);

	
}


