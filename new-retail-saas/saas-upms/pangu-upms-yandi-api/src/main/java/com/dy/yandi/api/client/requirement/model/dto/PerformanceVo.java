package com.dy.yandi.api.client.requirement.model.dto;

import com.pig4cloud.pig.common.core.util.BigDecimalUtils;
import lombok.Data;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description
 * @Author chengang
 * @Date 2021/10/11
 */
@Data
public class PerformanceVo implements Serializable {

	private static final long serialVersionUID = 6404133161275714995L;

	private String ids;

	/**
	 * 返回查询的开始时间(已经格式为yyyy-MM-dd),防止计算奖金时修改了查询时间段而导致的结果不正确
	 */
	private String searchStartDate;

	/**
	 * 返回查询的开始时间(已经格式为yyyy-MM-dd),防止计算奖金时修改了查询时间段而导致的结果不正确
	 */
	private String searchEndDate;

	/**
	 * 返回查询的组别ID,防止计算奖金时修改了组别而导致的结果不正确
	 */
	private Integer searchDeptGroupId;


	/**
	 * 盘古素材ID集合-逗号分割
	 * 存在之前完成的需求后续一直有消耗即：张三10月没有做任何需求但10月前做的需求10月有消耗故列表也需要展示 张三10月份的绩效
	 */
	private String materialIds;

	/**
	 * 创意标集合-逗号分割
	 */
	private String creativeRemarks;

	/**
	 * 突出标集合-逗号分割
	 */
	private String prominentRemarks;

	/**
	 * AE类型需求--月产量--用户对应的需求个数
	 */
	private Integer outPutNum = 0;

	/**
	 * 需求类型
	 */
	private Integer type;
	/**
	 * 周期
	 */
	private Integer period;
	private String periodValue;
	private Integer userId;
	private String userName;
	private Integer deptGroupId;
	private String deptGroupName;
	/**
	 * 消耗分
	 */
	private Double costScore = 0.0;
	/**
	 * 产量分
	 */
	private Double outPutScore = 0.0;
	/**
	 * 标记分
	 */
	private Double creativeScore = 0.0;
	/**
	 * 团队分
	 */
	private Double teamScore = 0.0;
	/**
	 * 综合分
	 */
	private Double comprehensiveScore = 0.0;
	/**
	 * 总分
	 */
	private Double totalScore = 0.0;
	/**
	 * 奖金
	 * -1 表示 未计算
	 */
	private Double bonus = -1d;

	/**
	 * 奖金总额
	 */
	private Double totalBonus;

	/**
	 * 评价显示
	 */
	private String evaluateName;

	/**
	 * 评价占比
	 */
	private Integer evaluate;

	/**
	 * 计算时间
	 */
	private String calcTime;

	/**
	 * 消耗分+创意分+产量分-综合表现扣分
	 */
	private Double middleScore;

	public Double getTotalScore() {
		//UE4 团队分默认0 可以统一按此计算
		return BigDecimalUtils.sub(BigDecimalUtils.add(BigDecimalUtils.add(this.creativeScore,BigDecimalUtils.add(this.costScore,this.outPutScore)),this.teamScore),this.comprehensiveScore);
	}

	public Double getMiddleScore() {
		return BigDecimalUtils.sub(BigDecimalUtils.add(this.creativeScore,BigDecimalUtils.add(this.costScore,this.outPutScore)),this.comprehensiveScore);
	}

}
