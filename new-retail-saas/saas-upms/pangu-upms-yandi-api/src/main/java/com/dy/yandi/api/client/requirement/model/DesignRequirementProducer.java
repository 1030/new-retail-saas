package com.dy.yandi.api.client.requirement.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;




/**
 * 需求制作者表
 * @author  chengang
 * @version  2021-09-28 13:44:36
 * table: design_requirement_producer
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "design_requirement_producer")
public class DesignRequirementProducer extends Model<DesignRequirementProducer>{

	//columns START
			//主键id
			@TableId(value="id",type= IdType.AUTO)
			private Long id;

			//需求ID
			@TableField(value = "requirement_id")
			private Long requirementId;

			//制作人
			@TableField(value = "producer_id")
			private Long producerId;

			//制作类型 1:默认(AE) 2:状态 3:脚本 4:动作 5:场景 6:特效
			@TableField(value = "producer_type")
			private Integer producerType;

			//制作类型值
			@TableField(value = "producer_value")
			private String producerValue;

			// 创意标记 1:是 0:否
			@TableField(value = "creative_remark")
			private Integer creativeRemark;

			// 突出标记 1:是 0:否
			@TableField(value = "prominent_remark")
			private Integer prominentRemark;

			//是否删除  0否 1是
			@TableField(value = "is_deleted")
			private Integer isDeleted;

			//创建时间
			@TableField(value = "create_time")
			private Date createTime;

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;

			//创建人
			@TableField(value = "create_id")
			private Long createId;

			//修改人
			@TableField(value = "update_id")
			private Long updateId;

	//columns END 数据库字段结束
	

	
}

	

	
	

