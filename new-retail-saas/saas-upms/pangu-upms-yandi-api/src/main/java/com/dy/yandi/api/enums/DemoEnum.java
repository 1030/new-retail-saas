/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dy.yandi.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;


@Getter
@RequiredArgsConstructor
public enum DemoEnum {
	PENDING("PENDING",  "待处理"),
	PROCESSING("PROCESSING",  "处理中"),
	SUCCESS("SUCCESS",  "成功可用"),
	ERROR("ERROR",  "错误");


	private final String key;
	private final String name;

	public static String getNameByKey(String key){
		if (StringUtils.isBlank(key)){
			return null;
		}
		for (DemoEnum item : DemoEnum.values()) {
			if (key.equals(item.getKey())) {
				return item.getName();
			}
		}
		return null;

	}

}
