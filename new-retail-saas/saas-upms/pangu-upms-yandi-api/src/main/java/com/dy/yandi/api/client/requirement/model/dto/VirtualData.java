package com.dy.yandi.api.client.requirement.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description 奖金评价数据
 * @Author chengang
 * @Date 2021/9/30
 */
@Data
public class VirtualData implements Serializable {

	private static final long serialVersionUID = 7721048846573568697L;

	/**
	 * 总分 UE4类型需求可以为负数
	 */
	private BigDecimal core;

	/**
	 * 绩效评价
	 */
	private String name;

	/**
	 * 奖金比例 如：80，90
	 */
	private Integer percentage;

	public VirtualData (BigDecimal core, String name, Integer percentage) {
		this.core = core;
		this.name = name;
		this.percentage = percentage;
	}

}
