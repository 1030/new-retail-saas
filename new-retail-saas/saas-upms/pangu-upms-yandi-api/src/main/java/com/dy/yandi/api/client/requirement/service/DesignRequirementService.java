

package com.dy.yandi.api.client.requirement.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yandi.api.client.requirement.model.DesignRequirement;
import com.dy.yandi.api.client.requirement.model.vo.DesignMaterialVo;
import com.dy.yandi.api.client.requirement.model.vo.DesignRequirementVo;
import com.pig4cloud.pig.common.core.util.R;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 需求表
 * @author  chengang
 * @version  2021-09-28 13:44:24
 * table: design_requirement
 */
public interface DesignRequirementService extends IService<DesignRequirement> {
	/**
	 * 需求列表 - 分页
	 * @param vo
	 * @return
	 */
	R getPage(DesignRequirementVo vo);
	/**
	 * 需求操作記錄
	 * @param vo
	 * @return
	 */
	R getRecordList(DesignRequirementVo vo);
	/**
	 * 我的需求 - 分页
	 * @param vo
	 * @return
	 */
	R getMyNeedPage(DesignRequirementVo vo);
	/**
	 * 已完成 - 分页
	 * @param vo
	 * @return
	 */
	R getComplete(DesignRequirementVo vo);
	/**
	 * 待审核 - 分页
	 * @param vo
	 * @return
	 */
	R getAudit(DesignRequirementVo vo);
	/**
	 * 发布需求
	 * @param vo
	 * @return
	 */
	R addEditNeed(DesignRequirementVo vo);
	/**
	 * 更新需求状态
	 * @param vo
	 * @return
	 */
	R editNeedStatus(DesignRequirementVo vo);
	/**
	 * 提交成果
	 * @param vo
	 * @return
	 */
	R submitResult(DesignMaterialVo vo);
	/**
	 * 标记
	 * @param vo
	 * @return
	 */
	R mark(DesignRequirementVo vo);
	/**
	 * 根据需求类型查询对应制作人列表
	 * @param vo
	 * @return
	 */
	R getProducerListByType(DesignRequirementVo vo);
	/**
	 * 待审核需求总数
	 * @return
	 */
	R auditTotal();
}


