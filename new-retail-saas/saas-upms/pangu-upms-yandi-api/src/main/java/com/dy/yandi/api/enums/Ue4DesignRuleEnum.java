package com.dy.yandi.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;


@Getter
@RequiredArgsConstructor
public enum Ue4DesignRuleEnum {
	cost1("cost1",  "500000"),
	cost2("cost2",  "1000000"),
	mark("mark",  "2"),
	out1("out1",  "5"),
	out2("out2",  "3");



	private final String label;
	private final String defaultValue;

	public static String getValueByLabel(String label){
		if (StringUtils.isBlank(label)){
			return null;
		}
		for (Ue4DesignRuleEnum item : Ue4DesignRuleEnum.values()) {
			if (label.equals(item.getLabel())) {
				return item.getDefaultValue();
			}
		}
		return null;

	}

}
