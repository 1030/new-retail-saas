package com.dy.yandi.api.client.requirement.model.dto;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


/**
 * 需求卖点表
 * @author  chengang
 * @version  2021-09-28 13:44:43
 * table: design_selling_point
 */
@Data
public class DesignSellingPointDto implements Serializable {
	
	//columns START
		//主键id
		private Long id;
		//类型 1:默认
		private Integer type;
		//名称
		private String name;
		//状态 1:启用 2:禁用
		private Integer status;
		//描述
		private String remark;
		//是否删除  0否 1是
		private Integer isDeleted;
		//创建时间
		private Date createTime;
		//修改时间
		private Date updateTime;
		//创建人
		private Long createId;
		//修改人
		private Long updateId;
	//columns END 数据库字段结束
	
	
}


