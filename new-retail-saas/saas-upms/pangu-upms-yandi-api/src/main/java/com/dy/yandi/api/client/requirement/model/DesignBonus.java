package com.dy.yandi.api.client.requirement.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;




/**
 * 绩效奖金表
 * @author  chengang
 * @version  2021-09-28 13:43:52
 * table: design_bonus
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "design_bonus")
public class DesignBonus extends Model<DesignBonus>{

	//columns START
			//主键id
			@TableId(value="id",type= IdType.AUTO)
			private Long id;

			//制作人
			@TableField(value = "producer_id")
			private Long producerId;

			//周期类型 1:按月 2:按季度 3:按年
			@TableField(value = "period_type")
			private Integer periodType;

			//周期值 格式:202109
			@TableField(value = "period_value")
			private String periodValue;

			//综合得分
			@TableField(value = "comprehensive_score")
			private BigDecimal comprehensiveScore;

			//消耗分
			@TableField(value = "cost_score")
			private BigDecimal costScore;

			//产量分
			@TableField(value = "outPut_score")
			private BigDecimal outPutScore;

			//标记分
			@TableField(value = "creative_score")
			private BigDecimal creativeScore;

			//团队分
			@TableField(value = "team_score")
			private BigDecimal teamScore;

			//总分
			@TableField(value = "total_score")
			private BigDecimal totalScore;

			//奖金
			@TableField(value = "bonus")
			private BigDecimal bonus;

			//最后一次奖金总额
			@TableField(value = "total_bonus")
			private BigDecimal totalBonus;

			//计算时间
			@TableField(value = "calc_time")
			private Date calcTime;

			//是否删除  0否 1是
			@TableField(value = "is_deleted")
			private Integer isDeleted;

			//创建时间
			@TableField(value = "create_time")
			private Date createTime;

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;

			//创建人
			@TableField(value = "create_id")
			private Long createId;

			//修改人
			@TableField(value = "update_id")
			private Long updateId;

	//columns END 数据库字段结束
	

	
}

	

	
	

