package com.dy.yandi.api.client.requirement.model.dto;

import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 绩效奖金表
 * @author  chengang
 * @version  2021-09-28 13:43:52
 * table: design_bonus
 */
@Data
public class DesignBonusDto implements Serializable {
	
	//columns START
		//主键id
		private Long id;
		//制作人
		@NotNull(message = "制作人不能为空")
		private Long userId;
		//周期类型 1:按月 2:按季度 3:按年
		@NotNull(message = "周期类型不能为空")
		@Min(value = 1,message = "最小值不能小于1")
		@Max(value = 3,message = "最大值不能大于3")
		private Integer period = 1;
		/**
		 * 按月 202109  按季度 {年份-季度值}  2021-3   按年  2021
		 */
		@NotNull(message = "周期值不能为空")
		private String periodValue;

		/**
		 * 返回查询的开始时间(已经格式为yyyy-MM-dd),防止计算奖金时修改了查询时间段而导致的结果不正确
		 */
		private String searchStartDate;

		/**
		 * 返回查询的开始时间(已经格式为yyyy-MM-dd),防止计算奖金时修改了查询时间段而导致的结果不正确
		 */
		private String searchEndDate;

		/**
		 * 返回查询的组别ID,防止计算奖金时修改了组别而导致的结果不正确
		 */
		private Integer searchDeptGroupId;
		private Integer type;

		//综合得分
		private BigDecimal comprehensiveScore = BigDecimal.ZERO;
		//总分 UE4 类型需求总分可能为负数
//		@NotNull(message = "总分不能为空")
		private BigDecimal totalScore;
		//最后一次奖金总额
		private BigDecimal totalBonus;
		//是否删除  0否 1是
		private Integer isDeleted;
		//创建时间
		private Date createTime;
		//修改时间
		private Date updateTime;
		//创建人
		private Long createId;
		//修改人
		private Long updateId;
	//columns END 数据库字段结束
	
	
}


