package com.dy.yandi.api.client.requirement.model.vo;

import com.pig4cloud.pig.common.core.mybatis.Page;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Collection;

/**
 * 需求表
 * @author  chengang
 * @version  2021-09-28 13:44:24
 * table: design_requirement
 */
@Data
public class DesignMaterialVo extends Page implements Serializable {

	private static final long serialVersionUID = 1L;

	// 需求ID
	private String id;
	// 完成成果
	private Collection<MultipartFile> resultFile;
	// 文件名称
	private String fileName;
}
