

package com.dy.yandi.api.client.requirement.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yandi.api.client.requirement.model.DesignRequirementHistory;


/**
 * 需求历史记录表
 * @author  chengang
 * @version  2021-09-28 13:44:30
 * table: design_requirement_history
 */
public interface DesignRequirementHistoryService extends IService<DesignRequirementHistory> {

	
}


