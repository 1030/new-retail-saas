package com.dy.yandi.api.client.requirement.model.vo;


import com.dy.yandi.api.client.requirement.model.DesignBonus;
import lombok.Data;

/**
 * 绩效奖金表
 * @author  chengang
 * @version  2021-09-28 13:43:52
 * table: design_bonus
 */
@Data
public class DesignBonusVo  extends DesignBonus {
	
	
	
}
