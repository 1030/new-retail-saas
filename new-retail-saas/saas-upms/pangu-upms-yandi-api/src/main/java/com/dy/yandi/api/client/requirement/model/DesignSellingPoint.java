package com.dy.yandi.api.client.requirement.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;




/**
 * 需求卖点表
 * @author  chengang
 * @version  2021-09-28 13:44:43
 * table: design_selling_point
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "design_selling_point")
public class DesignSellingPoint extends Model<DesignSellingPoint>{

	//columns START
			//主键id
			@TableId(value="id",type= IdType.AUTO)
			private Long id;

			//类型 1:默认
			@TableField(value = "type")
			private Integer type;

			//名称
			@TableField(value = "name")
			private String name;

			//状态 1:启用 2:禁用
			@TableField(value = "status")
			private Integer status;

			//描述
			@TableField(value = "remark")
			private String remark;

			//是否删除  0否 1是
			@TableField(value = "is_deleted")
			private Integer isDeleted;

			//创建时间
			@TableField(value = "create_time")
			private Date createTime;

			//修改时间
			@TableField(value = "update_time")
			private Date updateTime;

			//创建人
			@TableField(value = "create_id")
			private Long createId;

			//修改人
			@TableField(value = "update_id")
			private Long updateId;

	//columns END 数据库字段结束
	

	
}

	

	
	

