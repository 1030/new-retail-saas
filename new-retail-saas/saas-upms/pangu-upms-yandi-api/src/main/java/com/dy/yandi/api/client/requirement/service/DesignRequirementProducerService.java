

package com.dy.yandi.api.client.requirement.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.yandi.api.client.requirement.model.DesignRequirementProducer;

/**
 * 需求制作者表
 * @author  chengang
 * @version  2021-09-28 13:44:36
 * table: design_requirement_producer
 */
public interface DesignRequirementProducerService extends IService<DesignRequirementProducer> {

	
}


