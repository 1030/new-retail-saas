package com.dy.yandi.api.constant;

/**
 * @Description
 * @Author chengang
 * @Date 2021/9/3
 */
public class DesignConstant {

	public static final String APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";

	public static final String APPLICATION_JSON = "application/json";

	public static final String CHARSET_ENCODING = "UTF-8";

	public static final String YYYYMM = "yyyyMM";

	public static final String AE_DESIGN_cost = "cost";
	public static final String AE_DESIGN_markCost1 = "markCost1";
	public static final String AE_DESIGN_markCost2 = "markCost2";
	public static final String AE_DESIGN_out1 = "out1";
	public static final String AE_DESIGN_out2 = "out2";

	public static final String UE4_DESIGN_cost1 = "cost1";
	public static final String UE4_DESIGN_cost2 = "cost2";
	public static final String UE4_DESIGN_mark = "mark";
	public static final String UE4_DESIGN_out1 = "out1";
	public static final String UE4_DESIGN_out2 = "out2";

	public static final Integer PERIOD_MONTH = 1;
	public static final Integer PERIOD_QUARTERS = 2;
	public static final Integer PERIOD_YEARS = 3;





}
