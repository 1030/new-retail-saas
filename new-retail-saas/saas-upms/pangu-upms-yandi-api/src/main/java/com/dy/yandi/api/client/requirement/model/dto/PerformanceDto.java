package com.dy.yandi.api.client.requirement.model.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @Description 绩效管理查询DTO
 * @Author chengang
 * @Date 2021/9/28
 */
@Data
public class PerformanceDto extends Page implements Serializable {

	private static final long serialVersionUID = 133557192561610246L;

	@NotNull(message = "开始时间不能为空")
	private String startDate;

	@NotNull(message = "结束时间不能为空")
	private String endDate;

	/**
	 * 1：按月 2：按季度 3：按年
	 */
	private Integer period = 1;

	private Integer deptGroupId;

	private Integer userId;

	private List<Integer> searchUserList;

	/**
	 * 当前组别下所有用户--用于计算团队分
	 */
	private List<Integer> allUserList;

	/**
	 * 根据组别和需求类型的字典配置映射关系设置值
	 * 设计师取所属组别，所属组别为空则不显示绩效
	 */
	private Integer type;

	/**
	 * 根据筛选时间和用户拼接的SQL
	 */
	private String dictSql;




}
