package com.dy.yandi.api.client.requirement.model.vo;


import com.dy.yandi.api.client.requirement.model.DesignRequirementHistory;
import lombok.Data;

/**
 * 需求历史记录表
 * @author  chengang
 * @version  2021-09-28 13:44:30
 * table: design_requirement_history
 */
@Data
public class DesignRequirementHistoryVo  extends DesignRequirementHistory {
	
	
	
}
