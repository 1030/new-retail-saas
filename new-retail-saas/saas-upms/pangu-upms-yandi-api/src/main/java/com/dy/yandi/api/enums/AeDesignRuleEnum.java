package com.dy.yandi.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;


@Getter
@RequiredArgsConstructor
public enum AeDesignRuleEnum {
	cost("cost",  "1000000"),
	markCost1("markCost1",  "100000"),
	markCost2("markCost2",  "500000"),
	out1("out1",  "15"),
	out2("out2",  "23");



	private final String label;
	private final String defaultValue;

	public static String getValueByLabel(String label){
		if (StringUtils.isBlank(label)){
			return null;
		}
		for (AeDesignRuleEnum item : AeDesignRuleEnum.values()) {
			if (label.equals(item.getLabel())) {
				return item.getDefaultValue();
			}
		}
		return null;

	}

}
